<?php
if( ! defined("SF_VERSION") ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit;
}

class SF_Admin {

	private $code_version = 1;
	
	public function __construct() {

		add_action( 'admin_init', array( $this, 'register_settings' ) );
		add_action( 'admin_menu', array( $this, 'add_menu_item' ) );
		
		add_filter( "plugin_action_links_wp-social-into-video-files/index.php", array( $this, 'add_settings_link' ) );

		if ( isset( $_GET['page'] ) && $_GET['page'] === 'wp-social-into-video-files' ) {
			add_action( 'admin_enqueue_scripts', array( $this, 'load_css' ) );
		}
	}
	
	function wss_plugin_activation_action(){
		$defaults = array(
				'auto_add_post_types' => array( 'post' ),
				'social_options'=>array('facebook','twitter','googleplus', 'linkedin','dig','snaptcha'),
				'load_static'=>array('load_css','load_js'),
				'show_icons'=>'0',
				'text_position'=>'left'
		);
		update_option( 'wp_social_into_video_files', $defaults );
		update_option( 'wss_wp_social_into_video_files','f,t,g,d,s');
		update_option( 'wss_plugin_version ',SF_VERSION);
	}
	
	public function load_css() {
		wp_enqueue_style ( 'wp-social-into-video-files', SF_PLUGIN_URL . 'static/admin-styles.css' );
		wp_enqueue_media();
	}

	public function register_settings() {
		register_setting( 'wp_social_into_video_files', 'wp_social_into_video_files', array($this, 'sanitize_settings') );
	}

	public function sanitize_settings( $settings ) {
		$settings['twitter_username'] = trim( strip_tags( $settings['twitter_username'] ) );
		$settings['auto_add_post_types'] = ( isset( $settings['auto_add_post_types'] ) ) ? $settings['auto_add_post_types'] : array();
		return $settings;
	}

	public function add_settings_link( $links ) {
		$settings_link = '<a href="options-general.php?page=wp-social-into-video-files">'. __('Settings') . '</a>';
		array_unshift( $links, $settings_link );
		return $links;
	}

	public function add_menu_item() {
		add_options_page( 'WP Social Into Videos and Files', 'WP Social Into Videos and Files', 'manage_options', 'wp-social-into-video-files', array( $this, 'show_settings_page' ) );
	}

	public function show_settings_page() {
		$opts = ss_get_options();
		$post_types = get_post_types( array( 'public' => true ), 'objects' );
		include SF_PLUGIN_DIR . 'includes/settings-page.php';
	}
}
