<?php
if( ! defined( "SF_VERSION" ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit;
}

class SF_Public {
	
	public function __construct() {
		add_filter( 'the_content', array( $this, 'add_links_after_content' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'load_assets' ), 99 );
		add_shortcode( 'wp_social_into_video_files',array($this,'social_sharing'));
	}
	
	public function add_links_after_content( $content ){
		$opts = ss_get_options();
		$show_buttons = false;
		
		if( ! empty( $opts['auto_add_post_types'] ) && in_array( get_post_type(), $opts['auto_add_post_types'] ) && is_singular( $opts['auto_add_post_types'] ) ) {
			$show_buttons = true;
		}
			
		$show_buttons = apply_filters( 'ss_display', $show_buttons );
		if( ! $show_buttons ) {
			return $content;
		}
		$opts['icon_order']=get_option('wss_wp_social_into_video_files');
		
		if($opts['social_icon_position'] == 'before' ){
			return $this->social_sharing($opts).$content;
		}
		else{
			return $content . $this->social_sharing($opts);			
		}
	}
	
	public function load_assets() 
	{		
		wp_enqueue_style( 'wp-social-into-video-files', SF_PLUGIN_URL . 'static/socialshare.css', array(), SF_VERSION );
		wp_enqueue_style( 'wp-social-into-video-files_icons', SF_PLUGIN_URL . 'static/social_share_icons_lib.css', array(), SF_VERSION );
		wp_enqueue_script( 'wp-social-into-video-files', SF_PLUGIN_URL . 'static/socialshare.js', array(), SF_VERSION, true );	

	}

	public function social_sharing( $atts=array() ) {
		extract(shortcode_atts(array(
				'social_options' => 'twitter, facebook, googleplus, linkedin, dig, snaptcha',
				'twitter_username' => ''
		),$atts));

		if(!is_array($social_options))
			$social_options = array_filter( array_map( 'trim', explode( ',',$social_options ) ) );
		
		remove_filter('the_title','wptexturize');
		
		$title = urlencode(html_entity_decode(get_the_title()));
		add_filter('the_title','wptexturize');		
		$url = urlencode( get_permalink() );
		ob_start();
		if(isset($social_options) && !empty($social_options)){
			$count_social_option = count($social_options);
			
			?>
			<div class="social-sharing-video-files">
				<div class="block-general">
					<div id="option-social">					
						<?php 
						for ($i=0; $i < $count_social_option; $i++) { 
							if($social_options[$i] == 'facebook'){
								_e('<a class="opt-facebook" href= "https://www.facebook.com/sharer/sharer.php?u='.$url.'&t='.$title.'" target="_blank"><span class="socicon-facebook"></span>'.$social_options[$i].'</a>');
							}elseif ($social_options[$i] == 'twitter'){
								_e('<a class="opt-twitter" href= "http://twitter.com/home?status='.$title.'&nbsp;'.$url.'" target="_blank"><span class="socicon-twitter"></span>'.$social_options[$i].'</a>');
							}elseif ($social_options[$i] == 'googleplus') {
								_e('<a class="opt-googleplus" href= "https://plus.google.com/share?url='.$url.'" target="_blank"><span class="socicon-googleplus"></span>Google Plus</a>');
							}elseif($social_options[$i] == 'linkedin'){
								_e('<a class="opt-linkedin" href="https://www.linkedin.com/shareArticle?mini=true&url='.$url.'&title='.$title.'" target="_blank"><span class="socicon-linkedin"></span>'.$social_options[$i].'</a>');
							}elseif($social_options[$i] == 'dig'){
								_e('<a class="opt-dig" href= "http://digg.com/submit?url='.$url.'&title='.$title.'" target="_blank"><span class="socicon-digg"></span>'.$social_options[$i].'</a>');
							}elseif($social_options[$i] == 'snaptcha'){
								_e('<a class="opt-snaptcha" href= "https://www.facebook.com/sharer/sharer.php?u='.$url.'&t='.$title.'" target="_blank"><span class="socicon-snapchat"></span>'.$social_options[$i].'</a>');
							}

						}
					?>
					</div>
				</div>
			</div>
	    	<?php
		}	
	  	$output = ob_get_contents();
		ob_end_clean();
		return $output;
	}
}