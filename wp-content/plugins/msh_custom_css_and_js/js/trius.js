jQuery(document).ready(function(){
	
	var $ = jQuery.noConflict();
	
	// Category edit from frontend
	
	$('#category-edit').click(function(e){
		e.preventDefault();
		
		$('#cat-div').hide();
		$('#cat-editdiv').show();
	})
	
	$('#cat-save').click(function(e){
		e.preventDefault();
		
		$('#cat-editdiv').hide();
		$('#cat-div').show();
		
		var catval = [];
		var post_id = jQuery('input[name="post_id"]').val();
		
		$('input[name="cat_c[]"]:checked').each(function(i){
          catval[i] = $(this).val();
        });
		
		$.ajax({
			url: admin_ajax,
			type: "POST",
			data: {action : 'user_category_edits',catval : catval,post_id : post_id},
			success : function(data){
				location.reload();
				//alert('ok');
			}
		})
		
		
	})
	
	$('#cat-cncl').click(function(e){
		e.preventDefault();
		
		$('#cat-editdiv').hide();
		$('#cat-div').show();
	})
	
	// Title edit from frontend
	
	$('#title-edit').click(function(e){
		e.preventDefault();
		
		$('.video-title').hide();
		$('#title-div').show();
	})
	
	$('#title-cncl').click(function(e){
		e.preventDefault();
		
		$('#title-div').hide();
		$('.video-title').show();
	})
	
	$('#title-save').click(function(e){
		e.preventDefault();
		
		
		$('#title-div').hide();
		$('.video-title').show();
		
		var post_title = jQuery('input[name="title-val"]').val();
		var post_id = jQuery('input[name="post_id"]').val();
		
		$.ajax({
			url: admin_ajax,
			type: "POST",
			data: {action : 'user_title_edits',post_title : post_title,post_id : post_id},
			success : function(data){
				location.reload();
				//alert('ok');
			}
		})
	})
	
	$('#tags-edit').click(function(e){
		e.preventDefault();
		$('#tags-val').hide();
		$('.tags-forminput').show();
	})
	
	$('#tagsave').click(function(e){
		e.preventDefault();
		
		var tags_postid = $('#tags_postid').val();
		var tagsinput = $('#tagsinput').val();
		
		$.ajax({
			url: admin_ajax,
			type: "POST",
			data: {action : 'user_tags_edits',tags_postid : tags_postid,tagsinput : tagsinput}
			}).done(function(data) {
				location.reload();
		});
		
		$('.tags-forminput').hide();
		$('#tags-val').show();
		
	})
	
	$('.del-video').click(function(e){
		e.preventDefault();
		
		var confrm = confirm("Are you sure you want to continue !");
		var file_id = $(this).attr('data-id');
		//var profile_id = '<?= $profile_id;?>';
		
		if(confrm){
			$.ajax({
				type: "POST",  
				url: admin_ajax,  
				data: { action:'upload_video_delete',file_id: file_id,profile_id:profile_id},
				success : function(data){
					var res = $.parseJSON(data);
					if(res.msg_type == 'success'){
						$('.v-d').show();
						$('#post-'+res.file_id).remove();
					}
					else{
						alert('Not authorised');
					}
				}
			});
		}
	});
	
	$('.addflag-anchor2').click(function(e){
		e.preventDefault();
		//var url = $(this).attr('data-url');	
		$('#furl').val($(this).attr('data-url'));
		$('#aurl').val($(this).attr('data-aurl'));
		$('#ftitle').val($(this).attr('data-title'));
		$('#model_flag2').modal();
	});
	
	/*$('#addflag-submit2').live('click',function(e){
		var title = $('#ftitle').val();
		var hre = $('#aurl').val();
		var reason = $('#reason').val()+'<br> Title = '+title+'<br>Url = <a href="'+hre+'?tabo=file">'+hre+'</a>';
		
		
		var url = $('#furl').val();
		
		$.ajax({
			type: "POST",  
			url: admin_ajax,  
			data: {action:'user_mark_flag',reason:reason,url:url},
			success : function(data){
				var modelhtml = $('#model_flag2 .modal-content').html();
				$('#model_flag2 .modal-content').html(data);
				
				setTimeout(function(){
					$('#model_flag2').modal('hide');
					$('#model_flag2 .modal-content').html(modelhtml);
				},3000)
			}
		});
	});*/
	
})