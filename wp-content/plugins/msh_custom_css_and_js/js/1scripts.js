jQuery(document).ready(function(){

var ajax_site_base = "/doityourselfnation.org";

// wrap body in new wrapper
jQuery( "body" ).wrapInner( "<div class='main_wrapper'></div>");

// change logo on video player
setInterval(function(){
	jQuery('#VideoPlayer_logo').attr('src','data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADgAAAAiCAYAAAAKyxrjAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAB5FJREFUeNrUWQlwU9UaPje5WUrSpk3SPYVHQctSWpZXFpcqVBTHZRC0KJti9bk9RhQYhT4XeD71+RxxxifoOKDI4Iajo6MwjiNuo1jEjRbtOPrY2tJSStMmTXNzc+9930n/aIxJmtDg4D/z5W7nnHu+86/nRqja1MjSLZqqrBjmKms4uW/XngP/nMcEgTGd0WTRVLVfkWW1dMk6VrJgbX7f4e9nCzrddt6FnSbRnYYx1+rN1g2y+3jfoW33h2auM5mfwMELMq/gaG3duYn5jjZ3iNbsjbh+ip1GSTfB6cC/jPYi1vHR9nbvoaaZotHgZ5p2Fz2/RjQZPVJX++2dn+5gxuy8Nty7A6j9sxD8d2hQUZSlzhYLTtcxnd6E42IAhsoWDhzYvXLvCTTUd1O//wK2M53gOKA65IOahrmHhnbgIojjdmrzMjdVfl+n1/OG4b65wLIzneDI8Ikg6JgSVPgp1KSJuLGUHi1GwLHy+0owGGoXIVPOdIJy+CTo7TbYJ11kNVqzrg1KgWOKJG0VBEFVg/I22S+paHKzbcxUFuz35EX07zvTCe4DPPxEOtHCsitr9k/e2Di1eE5dkSBot8h+v6Aq6kN5My7XT96w54Bj+tz/SZ1HSiP6v386CAppzoOPAyt5WtMUhRmy85nJWfht196dl7XverbNee787LyapVsUr/sq//HDAyYqhILOd8DEPwNBLm8BVw5kfI1TRTrI7zdk5e5VfD1Tpa7WDFQCePMvxsMjzQygIa2miXULKFqI4Nm4NgBKvEWgIwIG60xy/LtIm7rfDvO7guUjYAnQAowCMrgL0zMR8AM/D1LpmKgvb6PBIIpVjZW09Qa+wisbRfKdzCQmzYPDYeAr4G1gB00glmwAtgL3ALcCWVFz3MVzIbCfrpEz2CdAUYyxbgSeTzCvMVi6bxHEWK+ksNbegE8vsPsmFVka11QXhTTYmyTBaPEB/wEeilj1WPIScF3EdRMwIaqNSItXFGeMieSnvxNRJ1R1eOW9bZ7A56Md5lXzxzn2XFZmY38tti4H8VpxCGY+DHgAuAm4gEwpYX4kcZJLyJH1+SBmyGvY8rAbmUSBHfPIrKUnwGRVa5nmso5dX+NqvnxMDrOZ9I+hyWrq5xHT4M/FpJVJQHOMNOSKumcbMFnWlcI7xugFYXdA1S442iOxTrfEFkzOZY9dMpxBe8fmjbOfzLcadqPdTBbDNNIhZuBVoDLqfiEhUjJCJVySBHXwLZ+ssoPd/dXDDLqKJZW5+43Q4KzSLDZ7lC3SSmbGNOE0RuYKMtl1EfdcFECiJSdh7hqo91h3fxBBQ2oqsBrrQGzvsim59qpiSxWefpnspOIR5BXJGoqA5SmQvIPSQ7jsKonTLisWL64tf1BlR+FbOD45Pi+jftlkl2/JRCdzZRnvRJsnyOzjBp1kCfKo+ibwNLCJiCYjfFcwN2L3EI+g85fogtCi1wkGhHjLIbe/I89iuPPq8fZX55xtY3POyh6ZIerW03YrUl4nn/eeKsHwKvMN6W3AI8BrwLQkSNZEEBwRb+zQTh/G2OELIFAEDKU55vPWzyppqp3gYMNtxr+Rtixx+o+miqlmKAQj5Qjt1ldQEk8kZVER9jflkwpS7Z5Abo8/yCRFZetmlTCEeU+Jzdg0ym5+Bs1uSXJOsygPr05QhaUcZJ4EthHRNXECiD3ivCgcCfu5b7mlNknRtl5RlrNx6SQnk1Ernjs8kzktYtgtFqY4n1VUVTWkiyCj8P4uUD9YJET5NPIkNNXilhoKMo23w/y+XgATnDky60JanA+SeB+vgb8BZsd5vhm4GzgZtbinRNBGVcWceBU8fEtu6ZV5FMxRVe2lUrv58Xuri9tvQGKGb12KZg9TFPxHkgSdFJ15XVsX4/lZwDPxuKRC8HoayByLGKyNHUF490jKO7XlDq7B7rLcjFUrzilwGHTCA7TKkenBkMK7/VQSXhQncBUPxUT5Cr0RKx+GqwyUT0Gc3ldVbH20bkoeqy238y3Yj8D5tEsYqnAtHgW4BTTG8f2UCbppr/ZitLYQL9gJn8w6++R9hZnG5dDYF4smOlj1iCxuep/Rduhq4C9p3kz/QF/fXhwqQZ5AefE69teAgZuBkLYCJlH3QkX+sLXLpxd0LaxwssJMw82UKx3Uf0sSZmg+RZI8il8CLBoKQf5pb6xAe/nOviBr9waaR9hMN9RNyW+onWBn011W/h30vRif+7j2dibx7rwhaHIx+eJ5KRMU6McHbWETyfdbGyvyLfXQlvt65C57hriKIqAtzq5/JftjZD5wILLsS0gwVGUgErZ7ZfhX8NCIbOOD2GdtnTfezi4ebSvEfuw58qtEMo/85I+Q42zgP43dCQnqqco4go2komqbp7kyl9eX2/tR8HJtraa8NVi0DVII/ziFCXrTQPJD4O9s4L+N2AQPuv1dCPdPzR1rr19U6WA1pbZzaBeR7HfK7ZSIexJ97Y4hHWnS5NNUdF8V21t3/KQ/2M3/4dJmA83arxIEegi9QB+d/wx8BtwPFPA/WhKgEPgEaAe8hA6gCZgQ1TYTaKV39xJkuq4c5D0m4Luovlxa/y/AAMjkC7Zn/q5GAAAAAElFTkSuQmCC');
}, 600);

// controls header height behind videos in homepage header
function header_height(){
	jQuery('.homepage-header').height( jQuery('.homepage_main_video').find('video').height() );	
}
setTimeout(function(){
	header_height();
}, 600);
jQuery(window).resize(function(){
	header_height();
});
// end header height

// first footer widget fix width
//jQuery('.footer-container').children('.container').children('.grid-4').eq(0).css('width','32%');

// animate block under videos on hover
jQuery('.title-bar').mouseover(function(){
  jQuery(this).nextAll('.spacer-dark').animate({
    width: '+=100%',
    backgroundColor: '#3498DB'
  },400);
});
jQuery('.title-bar').mouseout(function(){
  jQuery(this).nextAll('.spacer-dark').animate({
    width: '-=100%',
    backgroundColor: '#c9c9c9'
  },400);
});


// slick
/*

jQuery('.homepage-video-holder').slick({
            dots: false,
            infinite: false,
            speed: 300,
            slidesToShow: 5,
            slidesToScroll: 1,
            arrows: true,
			lazyLoad: 'progressive',
            responsive: [
                {
                    breakpoint: 1112,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 800,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 650,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
});
*/
//

// fix bigger naviagtion buttons to act on slick nav
jQuery(document).on('click','.scroll-left',function(){
  jQuery(this).prevAll('.layout-3').find('.slick-prev').click();
});
jQuery(document).on('click','.scroll-right',function(){
  jQuery(this).prevAll('.layout-3').find('.slick-next').click();  
});

// working on moving homepage elements for mobile
var sidebar = jQuery('.sidebar-wrapper');
var before_sidebar = jQuery('#categories_container');

if (jQuery(window).width() < 1116){
  jQuery('#categories_container').after(sidebar);
}
// end working on moving homepage elements for mobile

// add category icons to the 4 random categories on the home page
jQuery('#categories_container').children('a').each(function(){
  var main_href = jQuery(this);
  var icon = '';
  
  jQuery('.sidebar').children('a').each(function(){
    if (jQuery(this).attr('href') == main_href.attr('href')){
      icon = jQuery(this).children('h4').children('span');
      console.log(icon);
    }
  });
    
    if (icon != ''){
      jQuery(this).children('h5').prepend(icon);
    }
});


});