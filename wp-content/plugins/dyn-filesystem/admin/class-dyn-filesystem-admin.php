<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://www.doityourselfnation.org
 * @since      1.0.0
 *
 * @package    Dyn_Filesystem
 * @subpackage Dyn_Filesystem/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Dyn_Filesystem
 * @subpackage Dyn_Filesystem/admin
 * @author     papul <28papul@gmail.com>
 */
class Dyn_Filesystem_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

		require_once dirname(__FILE__) . '/../includes/class-dyn-filesystem-helper.php';
	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/dyn-filesystem-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/dyn-filesystem-admin.js', array( 'jquery' ), $this->version, false );

	}

	public function add_file_upload_metabox(){
		add_meta_box('custom_file_upload_to_cpt', 'File Upload', array($this, 'file_upload_html'), 'dyn_file', 'normal', 'high');
	}

	public function save_file_upload_metabox($post_id){
		/*if ( !wp_verify_nonce( $_POST["file_upload_nonce_for_post_{$post_id}"], "file_upload_post_{$post_id}" )) {
        	return $post_id;

    	}*/
    	// verify if this is an auto save routine. If it is our form has not been submitted, so we dont want to do anything
    	if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
        	return $post_id;
     
    	// Check permissions
		if ( !current_user_can( 'edit_post') )
		    return $post_id;

		//If not dyn_file post type, return
		$post = get_post($post_id);
    	if ($post->post_type != 'dyn_file')
    		return $post_id;

		$file_upload = $_POST['dyn-file-upload'];
		$type = 'dyn-file-upload';
		$value = $file_upload['upload'];

		update_post_meta( $post_id, 'dyn_upload_type', $type );
		update_post_meta( $post_id, 'dyn_upload_value', $value );

		return $post_id;

	}

	public function file_upload_html($post){
		$uploaded_value = get_post_meta( $post->ID, 'dyn_upload_value', true);
		ob_start();
		?>
		<div class="dyn-file-upload">
			<input type="hidden" name="file_upload_nonce_for_post_<?php echo $post->ID; ?>" class="file_upload_nonce" value="<?php echo wp_create_nonce( 'file_upload_post_'.$post->ID );?>">
			<?php
				$value = '';
				$css_class = '';
				$mime_class = '';
				if($uploaded_value){
					$css_class = 'dyn-display';
					$value = wp_get_attachment_url($uploaded_value);
					$hidden_value = $uploaded_value;
					$mime_class = "dyn-file-sprite " . Dyn_Filesystem_Helper::class_by_mime( get_post_mime_type($uploaded_value) );
				}
			?>
			<div class="dyn-upload-option display-dyn-value display-dyn-file-upload dyn-display">
				<div class="label">
					Upload File:
				</div>

				<div class="description">
					<input type="text" name="dyn-file-upload-url" class="dyn-file-upload-url" value="<?php echo $value; ?>" readonly>
					<input type="hidden" name="dyn-file-upload[upload]" class="dyn-file-upload-value" value="<?php echo $hidden_value; ?>">
					<a href="#" class="upload-dyn-file button">Add Media</a>
					<a href="#" class="remove-dyn-file button <?php echo $css_class; ?>">Remove Media</a>
				</div>

				<div class="dyn-uploaded-file <?php echo $mime_class; ?>">

				</div>
			</div>
		</div>
		<?php
		$output = ob_get_clean();
		echo $output;

	}

}
