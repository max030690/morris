<?php

/**
 * Fired during plugin activation
 *
 * @link       http://www.doityourselfnation.org
 * @since      1.0.0
 *
 * @package    Dyn_Filesystem
 * @subpackage Dyn_Filesystem/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Dyn_Filesystem
 * @subpackage Dyn_Filesystem/includes
 * @author     papul <28papul@gmail.com>
 */
class Dyn_Filesystem_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
