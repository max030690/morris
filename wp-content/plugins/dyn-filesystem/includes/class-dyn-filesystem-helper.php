<?php
class Dyn_Filesystem_Helper{
	function __construct(){

	}

	public static function file_type_by_mime($mime, $ext=''){
		switch($mime){
			case "application/vnd.ms-powerpoint":
			case "application/vnd.ms-powerpoint.presentation.macroenabled.12":
			case "application/vnd.ms-powerpoint.slideshow.macroenabled.12":
			case "application/vnd.openxmlformats-officedocument.presentationml.presentation":
			case "application/vnd.openxmlformats-officedocument.presentationml.slideshow":
                        case "application/vnd.openxmlformats-officedocument.presentationml.slide":
                        case "application/vnd.openxmlformats-officedocument.presentationml.template":
                        case "application/vnd.ms-powerpoint.template.macroenabled.12":    
				return "Powerpoint";
				break;
                        case "audio/3gpp":
                        case "video/3gpp" :
                                return "3GP";
                                break;
                        case "video/avi":
                        case "video/msvideo":   
                        case "video/x-msvideo":
                        case "image/avi":    
                        case "video/xmpg2":
                        case "application/x-troff-msvideo":
                        case "audio/aiff":
                        case "audio/avi":
                                return "AVI";
                                break;
                        case "image/gif":
                        case "image/x-xbitmap":       
                        case "image/gi_":    
                                return "GIF";
                                break;   
                        case "video/x-flv":
                                return "FLV";
                                break;    
                            
                        case "video/x-ms-wmv":
                                return "WMV";
                                break;
                            
			case "application/x-msdownload":
				return "application";
				break;
			case "application/zip":
			case "application/x-compressed":
			case "application/x-zip-compressed":
			case "multipart/x-zip":
				return "Zip";
				break;
			case "image/jpeg":
			case "image/gif":
			case "image/x-icon":
				return "Image";
				break;
			case "image/png":
				return "Image";
				break;
			case "audio/mpeg3":
			case "audio/mpeg":
			case "audio/x-mpeg-3":
			case "video/mpeg":
			case "video/x-mpeg":
				return "Audio";
				break;
			case "video/mp4":
                        case "video/divx": 
                        case "video/dvd":
                        case "video/mpeg":    
			case "video/x-ms-wmv":
                                return "video/audio";
				break;
                        case "image/x-icon":
                                return "Icon";
                                break;
                        case "application/octet-stream":
                                return "Photoshop";
                                break;    
                        case "application/vnd.rn-realplayer":
                                return "Real Player";
                                break;   
                        case "application/x-mspublisher":
                                return "Publisher";
                                break;
                           
                        case "text/plain":
                        case "application/txt":
                        case "browser/internal":
                        case "text/anytext":
                        case "widetext/plain":
                        case "widetext/paragraph":
                                return "Text File";
                                break;
                        case "application/excel":
			case "application/vnd.ms-excel":
                        case "application/x-excel":
			case "application/x-msexcel":    
                        case "application/vnd.ms-excel.sheet.binary.macroenabled.12":
                        case "application/vnd.ms-excel.template.macroenabled.12":
                        case "application/vnd.ms-excel.sheet.macroenabled.12":
                        case "application/vnd.openxmlformats-officedocument.spreadsheetml.template":                            
			case "application/x-excel":
			case "application/x-msexcel":    
				return "Excel";
				break;
			case "application/msword":
                        case "application/dot":
                        case "application/x-dot":    
			case "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
                        case "application/vnd.openxmlformats-officedocument.wordprocessingml.template":  
                        case "application/vnd.ms-word.template.macroenabled.12":    
			case "application/rtf":
			case "application/x-rtf":
			case "text/richtext":
				return "Document";
				break;
			case "application/pdf":
				return "PDF";
				break;
			case "application/octet-stream":
				if($ext){
					if($ext == "exe")
						return "Application";
					else
						return "ISO";
				}
				return "ISO";
				break;
			default:
				return "Unknown";
				break;
		}
	}

	public static function class_by_mime($mime, $ext=''){
		switch($mime){
                        case "application/vnd.ms-powerpoint":
			case "application/vnd.ms-powerpoint.presentation.macroenabled.12":
			case "application/vnd.ms-powerpoint.slideshow.macroenabled.12":
			case "application/vnd.openxmlformats-officedocument.presentationml.presentation":
			case "application/vnd.openxmlformats-officedocument.presentationml.slideshow":
                        case "application/vnd.openxmlformats-officedocument.presentationml.slide":
                        case "application/vnd.openxmlformats-officedocument.presentationml.template":
                        case "application/vnd.ms-powerpoint.template.macroenabled.12":     
				return "dyn-ppt";
				break;
                        case "audio/3gpp":
                        case "video/3gpp" :    
                                return "dyn-3gp";
                                break;                        
                        case "video/avi":
                        case "video/msvideo":   
                        case "video/x-msvideo":
                        case "image/avi":    
                        case "video/xmpg2":
                        case "application/x-troff-msvideo":
                        case "audio/aiff":
                        case "audio/avi":
                                return "dyn-avi";
                                break;
                        
                        case "image/gif":
                        case "image/x-xbitmap":       
                        case "image/gi_":    
                                return "dyn-gif";
                                break;
                        
                        case "video/x-flv":
                                return "dyn-flv";
                                break;
                        
                        case "video/x-ms-wmv":
                                return "dyn-wmv";
                                break;                                 
                            
			case "application/x-msdownload":
				return "dyn-exe";
				break;
			case "application/zip":
			case "application/x-compressed":
			case "application/x-zip-compressed":
			case "multipart/x-zip":
				return "dyn-zip";
				break;
			case "image/jpeg":
			case "image/gif":
			case "image/x-icon":
				return "dyn-jpg";
				break;
			case "image/png":
				return "dyn-png";
				break;
			case "audio/mpeg3":
			case "audio/mpeg":
			case "audio/x-mpeg-3":
			case "video/mpeg":
			case "video/x-mpeg":
				return "dyn-mp3";
				break;
			case "video/mp4":
				return "dyn-mp4";
				break;
                        case "video/x-ms-wmv":
                                return "dyn-wmv";
                                break;
                        case "video/divx":
                                return "dyn-mkv";
                                break;
                        case "image/x-icon":
                                return "dyn-xicon";
                                break; 
                        case "video/dvd":
                        case "video/mpeg":
                                return "dyn-vob";
                                break;
                        case "application/octet-stream":
                                return "dyn-psd";
                                break;
                        case "application/vnd.rn-realplayer":
                                return "dyn-rnx";
                                break;
                        case "application/x-mspublisher":
                                return "dyn-pub";
                                break;    
                        
                        case "text/plain":
                        case "application/txt":
                        case "browser/internal":
                        case "text/anytext":
                        case "widetext/plain":
                        case "widetext/paragraph":
                                return "dyn-txt";
                                break;    
                            
			case "application/excel":
			case "application/vnd.ms-excel":
                        case "application/vnd.ms-excel.sheet.binary.macroenabled.12":
                        case "application/vnd.ms-excel.template.macroenabled.12":
                        case "application/vnd.ms-excel.sheet.macroenabled.12":
                        case "application/vnd.openxmlformats-officedocument.spreadsheetml.template":                            
			case "application/x-excel":
			case "application/x-msexcel":
				return "dyn-xls";
				break;
                       
                        case "application/msword":
                        case "application/dot":
                        case "application/x-dot":    
			case "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
                        case "application/vnd.openxmlformats-officedocument.wordprocessingml.template":  
                        case "application/vnd.ms-word.template.macroenabled.12":    
			case "application/rtf":
			case "application/x-rtf":
			case "text/richtext":    
				return "dyn-doc";
				break;
			case "application/pdf":
				return "dyn-pdf";
				break;
			case "application/octet-stream":
				if($ext){
					if($ext == "exe")
						return "dyn-exe";
					else
						return "dyn-iso";
				}
				return "dyn-iso";
				break;
			default:
				return "dyn-unknown";
				break;
		}
	}
};