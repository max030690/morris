<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://www.doityourselfnation.org
 * @since      1.0.0
 *
 * @package    Dyn_Filesystem
 * @subpackage Dyn_Filesystem/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Dyn_Filesystem
 * @subpackage Dyn_Filesystem/public
 * @author     papul <28papul@gmail.com>
 */
class Dyn_Filesystem_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

		require_once dirname(__FILE__) . '/../includes/class-dyn-filesystem-helper.php';

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/dyn-filesystem-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/dyn-filesystem-public.js', array( 'jquery' ), $this->version, false );
	}

	public function get_file_type($post_id){
		$uploaded_value = get_post_meta( get_the_id(), 'dyn_upload_value', true );
		return Dyn_Filesystem_Helper::file_type_by_mime(get_post_mime_type($uploaded_value), pathinfo(get_attached_file($uploaded_value), PATHINFO_EXTENSION));
	}

	public function get_class($post_id){
		$uploaded_value = get_post_meta( $post_id, 'dyn_upload_value', true );
		return Dyn_Filesystem_Helper::class_by_mime(get_post_mime_type($uploaded_value), pathinfo(get_attached_file($uploaded_value), PATHINFO_EXTENSION));
	}

	public function file_upload_shortcode(){

		if(!is_user_logged_in())
			return;
		ob_start();
		if( isset($_POST['dyn-submit'])
			&& wp_verify_nonce( $_POST['dyn_file_upload_nonce'], 'dyn_file_upload' )
		){
			require_once( ABSPATH . 'wp-admin/includes/image.php' );
			require_once( ABSPATH . 'wp-admin/includes/file.php' );
			require_once( ABSPATH . 'wp-admin/includes/media.php' );

			$attachment_id = media_handle_upload( 'dyn-select-file', 0);
			if ( is_wp_error( $attachment_id ) ) {
			?>
				<div class="alert alert-danger">
					<span>Something went wrong</span>
				</div>
			<?php
			} else {
				$privacyOption = $_REQUEST['privacy-option'];
		        $select_users_list = implode(",", $_REQUEST['select_users_list']);
				$title = $_POST['dyn-select-title'];
				$description = $_POST['dyn-select-description'];
				$tags = trim($_POST['dyn-select-tags']);
				$reference = trim($_POST['dyn-select-reference']);
				$typeupload = $_POST['typeupload'];


				$dyn_post_id = wp_insert_post(
					array(
						'post_title' => $title,
						'post_content' => $description,
						'post_author' => $profile_id,
						'post_type' => 'dyn_file',
						'post_status' => 'publish'
					)
				);

				if(!empty($tags)){
					$tags = explode(' ', $tags);
					wp_set_post_tags( $dyn_post_id, $tags );
				}
				$post_tmp = get_post( $dyn_post_id );
				$author_id = $post_tmp->post_author;

				if( isset( $_POST['dyn_background'] ) ){
					if( $_POST['dyn_background'] == 'background_no' ){
						if ( ! delete_post_meta( $dyn_post_id, 'dyn_background_src' ) ) {}
						//echo '<script>alert("background_no");</script>';
					}
					if( $_POST['dyn_background'] == 'background_user' ){
						if( get_user_meta( $author_id, 'background_image', true ) ){
							$img = get_user_meta( $author_id, 'background_image' );
							update_post_meta( $dyn_post_id, 'dyn_background_src', $img[0] );
							//echo '<script>alert("background_user - '.$img[0].'");</script>';
						}
					}
					if( $_POST['dyn_background'] == 'background_new' ){
						if(($_FILES['dyn_bg']['type'] == 'image/jpg')
							|| ($_FILES['dyn_bg']['type'] == 'image/jpeg')
							|| ($_FILES['dyn_bg']['type'] == 'image/png')
							|| ($_FILES['dyn_bg']['type'] == 'image/gif')){
							$file = 'wp-content/themes/videopress/images/'.rand($author_id, 99999999).$_FILES['dyn_bg']['name'];
							move_uploaded_file($_FILES['dyn_bg']['tmp_name'], $file);
							update_post_meta( $dyn_post_id, 'dyn_background_src', $file);
							//echo '<script>alert("background_new");</script>';
						}else{
							echo '<div class="alert alert-danger">
									<span>Background image failed its type is different then jpeg, jpg, gif or png</span>
								</div>';
						}
					}
				}

				update_post_meta( $dyn_post_id, 'dyn_upload_type', 'dyn-file-upload' );
				update_post_meta( $dyn_post_id, 'dyn_upload_value', $attachment_id );
				update_post_meta( $dyn_post_id, 'video_options_refr', $reference );
				if ($typeupload=="cfiles"){
					global $current_user;  get_currentuserinfo();
					$user_ID = $current_user->ID;
					$body["message"] = "New file has been uploaded by ".$current_user->display_name;
					$body["message"] .= "<h3><a href='".get_site_url()."/file/".slug_title($title)."' target='_blank' > ".$title."</a></h3>";
					$body["message"] .= "<br />".$description;
					$body["message"] .= "<br />".$reference;
					$body["message"] .= "<hr />";
					$body["subject"] = "New file has been uploaded ";
					notification_subscribers($user_ID,$body);
				}

				update_post_meta( $dyn_post_id, 'privacy-option', $privacyOption);
		        update_post_meta( $dyn_post_id, 'select_users_list', $select_users_list);

				?>
				<div class="alert alert-success">
					<span>File Uploaded successfully. <a href="<?php echo get_permalink($dyn_post_id); ?>">View File</a></span>
				</div>
				<?php
			}

		}
		?>
		<div class="dyn-file-upload-section">
			<div class="dyn-click-upload">
				<div>Max Upload Size: 2GB</div>
				<div>Supported File Type: PPT, EXCEL, ISO, MP3, MP4, PDF, Word Document, ZIP</div>
			</div>

			<div class="dyn-upload-details">
				<form role="form" method="post" action="" enctype="multipart/form-data">
					<?php wp_nonce_field( 'dyn_file_upload', 'dyn_file_upload_nonce' ); ?>
					<div class="form-group">
						<label for="dyn-select-file">Select File:</label><br>
						<input type="file" id="file" name="dyn-select-file" class="dyn-select-file" class="form-control">
						<input type="hidden" name="typeupload" value="cfiles">
					</div>
					<div class="form-group">
						<br />
						<progress id="progressBar" value="0" max="100" style="width:100%;"></progress>
						<h3 id="status"></h3>
						<p id="loaded_n_total"></p>
						<br />
					</div>

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/jquery.tokenize.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/jquery.tokenize.css" />
<script>
	function _(el){
		return document.getElementById(el);
	}

	function uploadFile(){
		var file = _("file").files[0];
		var formdata = new FormData();
		formdata.append("file", file);
		var ajax = new XMLHttpRequest();
		ajax.upload.addEventListener("progress", progressHandler, false);
		ajax.addEventListener("load", completeHandler, false);
		ajax.addEventListener("error", errorHandler, false);
		ajax.addEventListener("abort", abortHandler, false);
		ajax.open("POST", "");
		ajax.send(formdata);
	}

	function progressHandler(event){
		_("loaded_n_total").innerHTML = "Uploaded "+event.loaded+" bytes of "+event.total;
		var percent = (event.loaded / event.total) * 100;
		_("progressBar").value = Math.round(percent);
		_("status").innerHTML = Math.round(percent)+"% uploaded... please wait";
	}

	function completeHandler(event){
		_("status").innerHTML = event.target.responseText;
		_("progressBar").value = 0;
	}

	function errorHandler(event){
		_("status").innerHTML = "Upload Failed";
	}
	function abortHandler(event){
		_("status").innerHTML = "Upload Aborted";
	}
</script>

					<div class="form-group">
						<label for="dyn-select-title">Title:</label>
						<input type="text" name="dyn-select-title" id="dyn-select-title" class="form-control">
					</div>
					<div class="form-group">
						<label for="dyn-select-description">Description:</label>
						<textarea name="dyn-select-description" id="dyn-select-description" class="form-control" rows="3"></textarea>
					</div>
					<div class="form-group">
						<label for="dyn-select-reference">Reference:</label>
						<textarea name="dyn-select-reference" class="form-control" id="dyn-select-reference" rows="2"></textarea>
					</div>
					<div class="form-group">
						<label for="dyn-tags">Tags:</label>
						<input type="text" name="dyn-select-tags" class="dyn-select-tags">
						<span>Enter tags separated by space</span>
					</div>
					<div class="form-group">
						<label for="dyn-tags">Add Background:</label>
						<div class="checkbox">
							<label>White Background
							<input type="radio" name="dyn_background" class="dyn_checkbox" value="background_no" checked="checked" ></label>
							<label>Profile Background
							<input type="radio" name="dyn_background" class="dyn_checkbox" value="background_user" ></label>
							<label>New Background
							<input type="radio" name="dyn_background" class="dyn_checkbox" value="background_new" ></label>
						</div>
					</div>
					<div class="form-group">
			<label for="dyn-tags"><br/>Privacy Options:</label>
			<div class="checkbox">
				<label>Private
				  <input type="radio" id="privacy-radio" name="privacy-option" class="dyn-select-file" class="form-control" value="private">
				</label>
				<label>Public
				 <input type="radio" id="privacy-radio1" name="privacy-option" class="dyn-select-file" class="form-control" value="public">
				</label>
			</div>

			<div style="display:none;" class="select-box">
				<?php
				  echo '<label for="dyn-tags">Please select the User you want to give access</label><br>
				      <select id="tokenize" class="tokenize-sample" name="select_users_list[]" style="width: 350px; height: 150px;" multiple="multiple">';
					  $args1 = array(
						  'role' => 'free_user',
						  'orderby' => 'id',
						  'order' => 'desc'
					   );
					  $subscribers = get_users($args1);
					  foreach ($subscribers as $user) {
					      if(get_current_user_id() == $user->id)
						  { }
						  else
						  {
							   echo '<option value="'.$user->id.'">' . $user->display_name.'</option>';
						  }
					  }
				  echo '</select>';
				?>
			</div>
			<script type="text/javascript">
				  $(document).ready(function(){
					  $("#privacy-radio").click(function(){
						 $(".select-box").slideDown();
					  });
					  $("#privacy-radio1").click(function(){
						 $(".select-box").slideUp();
						 //$('select#select_users_list option').removeAttr("selected");
					  });
				  });
				$('#tokenize').tokenize();
				 datas: "bower.json.php"
			  </script>
			  <style>.tokenize-sample { width: 300px }</style>
		</div>

					<style>
                      #privacy-radio1, #privacy-radio {
                        padding: 5px;
                        text-align: center;
                      }

                     #select-box {
                       padding: 50px;
                       display: none;
                     }
                    </style>
					<div id="upload_container"></div>
					<script type="text/javascript">
						jQuery(document).ready(function() {
							jQuery("#upload_container").html('');
							jQuery(".dyn_checkbox").click( function() {
								var test = $(this).val();
								if( test == 'background_new' ){
									jQuery("#upload_container").html('<div class="form-group"><label for="dyn_bg">Upload File:</label><input type="file" id="dyn_bg" name="dyn_bg" class="dyn_bg" accept=".jpeg,.jpg,.gif,.png,.JPEG,.JPG,.GIF,.PNG"></div>');
								}else{
									jQuery("#upload_container").html('');
								}
							});
						});
					</script>
					<input class="btn btn-default" type="submit" id="submit" value="Submit" name="dyn-submit" onclick="uploadFile()">
				</form>
			</div>
		</div>
		<?php
		$output = ob_get_contents();
		ob_end_clean();
		return $output;
	}

	public function file_upload($profile_id){

		if(!is_user_logged_in() && (get_current_user_id() != $profile_id))
			return;
		ob_start();
		if( isset($_POST['dyn-submit'])
			&& wp_verify_nonce( $_POST['dyn_file_upload_nonce'], 'dyn_file_upload' )
		){
			require_once( ABSPATH . 'wp-admin/includes/image.php' );
			require_once( ABSPATH . 'wp-admin/includes/file.php' );
			require_once( ABSPATH . 'wp-admin/includes/media.php' );

			$attachment_id = media_handle_upload( 'dyn-select-file', 0);
			if ( is_wp_error( $attachment_id ) ) {
			?>
				<div class="alert alert-danger">
					<span>Something went wrong</span>
				</div>
			<?php
			} else {
				$title = $_POST['dyn-select-title'];
				$description = $_POST['dyn-select-description'];
				$tags = trim($_POST['dyn-select-tags']);

				$dyn_post_id = wp_insert_post(
					array(
						'post_title' => $title,
						'post_content' => $description,
						'post_author' => $profile_id,
						'post_type' => 'dyn_file',
						'post_status' => 'publish'
					)
				);

				if(!empty($tags)){
					$tags = explode(' ', $tags);
					wp_set_post_tags( $dyn_post_id, $tags );
				}


				update_post_meta( $dyn_post_id, 'dyn_upload_type', 'dyn-file-upload' );
				update_post_meta( $dyn_post_id, 'dyn_upload_value', $attachment_id );
				?>
				<div class="alert alert-success">
					<span>File Updated</span>
				</div>
				<?php
			}

		}
		$url = add_query_arg('display_page', 'file');
		?>
		<div class="dyn-file-upload-section">
			<div class="dyn-click-upload">
				<button class="btn btn-success file-upload-button">Upload File</button>
				<div>Max Upload Size: 1GB</div>
				<div>Supported File Type: PPT, EXCEL, ISO, MP3, MP4, PDF, Word Document, ZIP</div>
			</div>

			<div class="dyn-upload-details" style="display: none;">
				<form role="form" method="post" action="<?= $url; ?>" enctype="multipart/form-data">
					<?php wp_nonce_field( 'dyn_file_upload', 'dyn_file_upload_nonce' ); ?>
					<div class="form-group">
						<label for="dyn-select-file">Select File:</label>
						<input type="file" id="file" name="dyn-select-file" class="dyn-select-file" class="form-control">
					</div>
					<div class="form-group">
						<br />
						<progress id="progressBar" value="0" max="100" style="width:100%;"></progress>
						<h3 id="status"></h3>
						<p id="loaded_n_total"></p>
						<br />
					</div>
<script>
	function _(el){
		return document.getElementById(el);
	}

	function uploadFile(){
		var file = _("file").files[0];
		var formdata = new FormData();
		formdata.append("file", file);
		var ajax = new XMLHttpRequest();
		ajax.upload.addEventListener("progress", progressHandler, false);
		ajax.addEventListener("load", completeHandler, false);
		ajax.addEventListener("error", errorHandler, false);
		ajax.addEventListener("abort", abortHandler, false);
		ajax.open("POST", "");
		ajax.send(formdata);
	}

	function progressHandler(event){
		_("loaded_n_total").innerHTML = "Uploaded "+event.loaded+" bytes of "+event.total;
		var percent = (event.loaded / event.total) * 100;
		_("progressBar").value = Math.round(percent);
		_("status").innerHTML = Math.round(percent)+"% uploaded... please wait";
	}

	function completeHandler(event){
		_("status").innerHTML = event.target.responseText;
		_("progressBar").value = 0;
	}

	function errorHandler(event){
		_("status").innerHTML = "Upload Failed";
	}
	function abortHandler(event){
		_("status").innerHTML = "Upload Aborted";
	}
</script>

					<div class="form-group">
						<label for="dyn-select-title">Title:</label>
						<input type="text" name="dyn-select-title" id="dyn-select-title" class="form-control">
					</div>
					<div class="form-group">
						<label for="dyn-select-description">Description:</label>
						<textarea name="dyn-select-description" id="dyn-select-description" class="form-control" rows="3"></textarea>
					</div>
					<div class="form-group">
						<label for="dyn-tags">Tags:</label>
						<input type="text" name="dyn-select-tags" class="dyn-select-tags">
						<span>Enter tags separated by space</span>
					</div>
					<input class="btn btn-default" type="submit" id="submit" value="Submit" name="dyn-submit" onclick="uploadFile()">
				</form>
			</div>
		</div>
		<?php
		$output = ob_get_contents();
		ob_end_clean();
		return $output;
	}

	public function sort_box($profile_id){
		$sort_type = array(
			"recent" => "Recent",
			"views"	=> "Views",
			"comment" => "Comment",
			"download" => "Download"
		);
		$url = add_query_arg('display_page', 'file');

		if(isset($_GET['file-sort']))
			$sort = $_GET['file-sort'];
		else
			$sort = 'recent';
		ob_start();
		?>
		<div class="cover-twoblocks">
			<div class="sortvideo-div">
			<div class="dyn-file-sort">
				<label>Sort By123: </label>
				<select name="dyn-file-sort-select" class="dyn-file-sort-select">
				<?php foreach ($sort_type as $key => $value): ?>
					<option data-url="<?php echo add_query_arg('file-sort', $key, $url); ?>" value="<?php echo $key; ?>" <?php selected( $sort, $key ) ?>><?php echo $value; ?></option>
				<?php endforeach; ?>
				</select>
			</div>

			</div>
			<div class="well well-sm">
				<strong>Views</strong>
				<div class="btn-group">
					<a href="#" id="list2" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th-list">
					</span>List</a> <a href="#" id="grid2" class="btn btn-default btn-sm"><span
						class="glyphicon glyphicon-th"></span>Grid</a>
				</div>
			</div>
		</div>
		<script type="text/javascript">
		$(document).ready(function() {
			$('#products2 .item2').addClass('grid-group-item');
			$('#list2').click(function(event){
				event.preventDefault();
				$('#products2 .item2').addClass('list-group-item');
				$("#new_files .list-group .list-group-item").removeClass("changeStyleGrid").addClass("changeStyleList");
			});
			$('#grid2').click(function(event){event.preventDefault();$('#products2 .item2').removeClass('list-group-item');
				$('#products2 .item2').addClass('grid-group-item');
				$("#new_files .list-group .grid-group-item").removeClass("changeStyleList").addClass("changeStyleGrid");
			});
		});
		</script>
		<?php
		return ob_get_clean();

	}

	public function files_list($profile_id){
		global $wpdb, $wp_query;
		ob_start();
		echo $this->sort_box($profile_id);
		//FA: NOTE - get-query_var('page') does not work for files in the channels page - switched to $_GET
		$paged = ( isset($_GET['page']) ) ? $_GET['page'] : 1;
		if(isset($_GET['file-sort']))
			$sort = $_GET['file-sort'];
		else
			$sort = 'recent';
		$args = array(
			'post_type' => 'dyn_file',
			'post_author' => $profile_id,
			'paged' => $paged,
			'post_status' => 'publish',
			'posts_per_page' => 12,
		);
		if( $sort == 'comment' ){
			$args['orderby'] = 'comment_count';
		}
		if($sort == 'download' || $sort == 'views'){
			if( $sort == 'download' )
				$key = 'dyn_download_count';
			else
				$key = 'popularity_count';

			$args['orderby'] = array('meta_value_num' => 'DESC');
			$args['meta_query'] = array(
			        'relation' => 'OR',
			        array(
			            'key'=> $key,
			            'compare' => 'EXISTS'
			        ),
			        array(
			            'key'=> $key,
			            'compare' => 'NOT EXISTS'
			        )
			    );
		}
		if($_GET['file-sort'] == ""){
			if($paged = 1){
				$args = array(
					'posts_per_page' => 12,
					'post_type' => 'dyn_file',
					'post_author' => $profile_id,
					'paged' => $paged,
					'post_status' => 'publish',
					//'meta_key' => 'privacy-option',
					//'meta_value_num' => 'public',
					//'meta_compare' => '=',
				);
			}else{
				$args = array(
					'posts_per_page' => 12,
					'post_type' => 'dyn_file',
					'post_author' => $profile_id,
					'paged' => $paged,
					'post_status' => 'publish',
				);
			}
		}
		// if(!is_user_logged_in())
		//'meta_key' => 'number',
		//'meta_value_num' => 10,
		//'meta_compare' => '<',
		$my_query = new WP_Query( $args );
		$temp_query = $wp_query;
		$wp_query   = NULL;
		$wp_query   = $my_query;
		$ri=0;
		?>
		<div id="products2" class="list-group xia-files">
		<div class="row">
		<?php
		/*echo ("<pre>");
		//print_r($my_query);
		foreach($my_query->posts as $datas){
			print_r($datas);
			echo $datas->ID.'<br />';
	}
		echo ("</pre>");
		exit;*/
		while ( $my_query->have_posts() ) : $my_query->the_post(); ?>

			<?php $uploaded_value = get_post_meta( get_the_id(), 'dyn_upload_value', true ); ?>
			<!-- Start Layout Wrapper -->
			<?php
			   $postID = get_the_ID();
			   $privacyOption   = get_post_meta( $postID, 'privacy-option' );
			   $selectUsersList = get_post_meta( $postID, 'select_users_list' );
			   $post_author     = get_the_author_id();
			   $user_ID         = get_current_user_id();
			   $selectUsersList = explode( ",", $selectUsersList[0] );

			   //print_r($privacyOption);
			    if(!is_user_logged_in())
				{  // case where user is not logged in
						 if(isset($privacyOption[0]) && ($privacyOption[0] != "private"))
						  {
						  	//if( ($ri % 6 && $ri != 0) == 0){echo '</div><div class="row">'; }
								    ?>

			<div class="item2 col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 post-<?php the_ID(); ?>">
				<div class="thumbnail layout-2-wrapper solid-bg">

					<div class="grid-6 first">
						<div class="image-holder filesz dyn-file-sprite <?php echo apply_filters('dyn_class_by_mime', get_the_id()); ?>">
							<a href="<?php echo get_permalink(); ?>">
								<div class="hover-item dyn-file">
			                        <i class="fa fa-download"></i>
			                    </div>
							</a>
						</div>
						<div class="layout-title-box layout-title-box-<?php the_ID(); ?>" style="display:block;">
							<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php echo ctpreview( $content = get_the_title() , $length = 39 , $message = 'No Title available' , $html = ' [...]' ); ?></a></h6>
							<ul class="bottom-detailsul">
							<?php if($profile_id == get_current_user_id()){ ?>
							<li><p><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>" ><i class="fa fa-trash-o"></i> Delete</a></p></li>
							<?php } ?>
							<li><a class="detailsblock-btn" data-modal-id="myModal-<?php the_ID(); ?>"><i class="fa fa-info-circle" aria-hidden="true"></i> Details</a></li>
							</ul>
						</div>
					</div>

					<script type="text/javascript">
					$(function(){

					var appendthis =  ("<div class='modal-overlayblockdiv'></div>");

						$('a[data-modal-id]').click(function(e) {
							e.preventDefault();
					    $("body").append(appendthis);
					    $(".modal-overlay").fadeTo(500, 0.7);
					    //$(".js-modalbox").fadeIn(500);
							var modalBox = $(this).attr('data-modal-id');
							$('#'+modalBox).fadeIn($(this).data());
						});


					$(".js-modal-close, .modal-overlay").click(function() {
					    $(".modal-box, .modal-overlay").fadeOut(500, function() {
					        $(".modal-overlay").remove();
					    });
					});

					$(window).resize(function() {
					    $(".modal-box").css({
					        top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
					        left: ($(window).width() - $(".modal-box").outerWidth()) / 2
					    });
					});

					$(window).resize();

					});
					</script>
					<div class="modal-box" id="myModal-<?php the_ID(); ?>">
					 <div class="modal-body">
					 <a class="js-modal-close close">×</a>
					<div class="layout-2-details layout-2-details-<?php the_ID(); ?>">
						<div class="image-holder filesz dyn-file-sprite <?php echo apply_filters('dyn_class_by_mime', get_the_id()); ?>">
							<a href="<?php echo apply_filters('dyn_file_download_link', get_the_id()); ?>">
								<div class="hover-item dyn-file">
			                        <i class="fa fa-download"></i>
			                    </div>
							</a>
						</div>
						<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title();?></a></h6>
						<div>File Type: <?php echo apply_filters( 'dyn_file_type_by_mime', get_the_ID() ); ?></div>
						<?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
						<div class="video_excerpt_text one"><h4 class="tittle-h4">Description 3</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = '' , $message = 'No Description available' , $html = ' [...]' );?></div></div>
						<div class="video_excerpt_text two"><h4 class="tittle-h4">Reference</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_post_meta(get_the_ID(),'video_options_refr',true) , $length = '' , $message = 'No referance given' , $html = ' [...]' );?></div></div>
						<ul class="stats">
							<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
							<li><?php videopress_countviews( get_the_ID() ); ?></li>
							<li><i class="fa fa-wrench"></i>
							<?php
								$result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
								echo count($result);
							?> Endorsments</li>
							<li><i class="fa  fa-heart"></i>
							<?php
							  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
							  $result1 = $wpdb->get_results($sql_aux);
							  echo count($result1);
							?> Favorites</li>
							<!--end favorite-->
							<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
							<li><?php comments_number() ?></li>
							<li><i class="fa fa-download"></i> <?php echo apply_filters( 'dyn_number_of_download', get_the_id()); ?></li>
						</ul>
						<div class="clear"></div>
						<p><?php echo videopress_content('240'); ?></p>
						<?php if($profile_id == get_current_user_id()){ ?>
						<p><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>" ><i class="fa fa-trash-o"></i> Delete</a></p>
						<?php } ?>
					</div></div></div>

					<div class="layout-2-details layout-2-details-<?php the_ID(); ?>" style="display:none;">
						<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title();?></a></h6>
						<div>File Type: <?php echo apply_filters( 'dyn_file_type_by_mime', get_the_ID() ); ?></div>
						<?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
						<div class="video_excerpt_text one"><h4 class="tittle-h4">Description</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = 100 , $message = 'No Description available' , $html = '<a data-modal-id="myModal-'.get_the_ID().'"> [...]</a>' ); ?></div></div>
						<ul class="stats">
							<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
							<li><?php videopress_countviews( get_the_ID() ); ?></li>
							<li><i class="fa fa-wrench"></i>
							<?php
								$result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
								echo count($result);
							?> Endorsments</li>
							<li><i class="fa  fa-heart"></i>
							<?php
							  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
							  $result1 = $wpdb->get_results($sql_aux);
							  echo count($result1);
							?> Favorites</li>
							<!--end favorite-->
							<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
							<li><?php comments_number() ?></li>
							<li><i class="fa fa-download"></i> <?php echo apply_filters( 'dyn_number_of_download', get_the_id()); ?></li>
						</ul>
						<div class="clear"></div>
						<p><?php echo videopress_content('240'); ?></p>
						<?php if($profile_id == get_current_user_id()){ ?>
						<p><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>" ><i class="fa fa-trash-o"></i> Delete</a></p>
						<?php } ?>
					</div>

					<div class="clear"></div>
				</div>
			</div>
			<!-- End Layout Wrapper -->
		<?php
		$ri++;
						  }
						  else
						  {
								if(!isset($privacyOption[0]))
								{
									//if( ($ri % 6) == 0 && $ri != 0){echo '</div><div class="row">'; }
									 ?>
			<div class="item2 col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 post-<?php the_ID(); ?>">
				<div class="thumbnail layout-2-wrapper solid-bg">

					<div class="grid-6 first">
						<div class="image-holder filesz dyn-file-sprite <?php echo apply_filters('dyn_class_by_mime', get_the_id()); ?>">
							<a href="<?php echo get_permalink(); ?>">
								<div class="hover-item dyn-file">
			                        <i class="fa fa-download"></i>
			                    </div>
							</a>
						</div>
						<div class="layout-title-box layout-title-box-<?php the_ID(); ?>" style="display:block;">
							<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php echo ctpreview( $content = get_the_title() , $length = 39 , $message = 'No Title available' , $html = ' [...]' ); ?></a></h6>
							<ul class="bottom-detailsul">
							<?php if($profile_id == get_current_user_id()){ ?>
							<li><p><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>" ><i class="fa fa-trash-o"></i> Delete</a></p></li>
							<?php } ?>
							<li><a class="detailsblock-btn" data-modal-id="myModal-<?php the_ID(); ?>"><i class="fa fa-info-circle" aria-hidden="true"></i> Details</a></li>
							</ul>
						</div>
					</div>

					<script type="text/javascript">
					$(function(){

					var appendthis =  ("<div class='modal-overlayblockdiv'></div>");

						$('a[data-modal-id]').click(function(e) {
							e.preventDefault();
					    $("body").append(appendthis);
					    $(".modal-overlay").fadeTo(500, 0.7);
					    //$(".js-modalbox").fadeIn(500);
							var modalBox = $(this).attr('data-modal-id');
							$('#'+modalBox).fadeIn($(this).data());
						});


					$(".js-modal-close, .modal-overlay").click(function() {
					    $(".modal-box, .modal-overlay").fadeOut(500, function() {
					        $(".modal-overlay").remove();
					    });
					});

					$(window).resize(function() {
					    $(".modal-box").css({
					        top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
					        left: ($(window).width() - $(".modal-box").outerWidth()) / 2
					    });
					});

					$(window).resize();

					});
					</script>
					<div class="modal-box" id="myModal-<?php the_ID(); ?>">
					 <div class="modal-body">
					 <a class="js-modal-close close">×</a>
					<div class="layout-2-details layout-2-details-<?php the_ID(); ?>">
						<div class="image-holder filesz dyn-file-sprite <?php echo apply_filters('dyn_class_by_mime', get_the_id()); ?>">
							<a href="<?php echo apply_filters('dyn_file_download_link', get_the_id()); ?>">
								<div class="hover-item dyn-file">
			                        <i class="fa fa-download"></i>
			                    </div>
							</a>
						</div>
						<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title();?></a></h6>
						<div>File Type: <?php echo apply_filters( 'dyn_file_type_by_mime', get_the_ID() ); ?></div>
						<?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
						<div class="video_excerpt_text one"><h4 class="tittle-h4">Description 3</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = '' , $message = 'No Description available' , $html = ' [...]' );?></div></div>
						<div class="video_excerpt_text two"><h4 class="tittle-h4">Reference</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_post_meta(get_the_ID(),'video_options_refr',true) , $length = '' , $message = 'No referance given' , $html = ' [...]' );?></div></div>
						<ul class="stats">
							<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
							<li><?php videopress_countviews( get_the_ID() ); ?></li>
							<li><i class="fa fa-wrench"></i>
							<?php
								$result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
								echo count($result);
							?> Endorsments</li>
							<li><i class="fa  fa-heart"></i>
							<?php
							  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
							  $result1 = $wpdb->get_results($sql_aux);
							  echo count($result1);
							?> Favorites</li>
							<!--end favorite-->
							<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
							<li><?php comments_number() ?></li>
							<li><i class="fa fa-download"></i> <?php echo apply_filters( 'dyn_number_of_download', get_the_id()); ?></li>
						</ul>
						<div class="clear"></div>
						<p><?php echo videopress_content('240'); ?></p>
						<?php if($profile_id == get_current_user_id()){ ?>
						<p><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>" ><i class="fa fa-trash-o"></i> Delete</a></p>
						<?php } ?>
					</div></div></div>



					<div class="layout-2-details layout-2-details-<?php the_ID(); ?>" style="display:none;">
						<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title();?></a></h6>
						<div>File Type: <?php echo apply_filters( 'dyn_file_type_by_mime', get_the_ID() ); ?></div>
						<?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
						<div class="video_excerpt_text one"><h4 class="tittle-h4">Description</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = 100 , $message = 'No Description available' , $html = '<a data-modal-id="myModal-'.get_the_ID().'"> [...]</a>' ); ?></div></div>
						<ul class="stats">
							<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
							<li><?php videopress_countviews( get_the_ID() ); ?></li>
							<li><i class="fa fa-wrench"></i>
							<?php
								$result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
								echo count($result);
							?> Endorsments</li>
							<li><i class="fa  fa-heart"></i>
							<?php
							  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
							  $result1 = $wpdb->get_results($sql_aux);
							  echo count($result1);
							?> Favorites</li>
							<!--end favorite-->
							<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
							<li><?php comments_number() ?></li>
							<li><i class="fa fa-download"></i> <?php echo apply_filters( 'dyn_number_of_download', get_the_id()); ?></li>
						</ul>
						<div class="clear"></div>
						<p><?php echo videopress_content('240'); ?></p>
						<?php if($profile_id == get_current_user_id()){ ?>
						<p><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>" ><i class="fa fa-trash-o"></i> Delete</a></p>
						<?php } ?>
					</div>

					<div class="clear"></div>
				</div>
			</div>
			<!-- End Layout Wrapper -->
		<?php
		$ri++;
								}
						  }
				}
				else
				{  // case where user is logged in
					 if($post_author == $user_ID)
					 {    // Case where logged in User is same as Video Author User
					 	//if( ($ri % 6) == 0 && $ri != 0){echo '</div><div class="row">'; }
						   ?>
			<div class="item2 col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 post-<?php the_ID(); ?>">
				<div class="thumbnail layout-2-wrapper solid-bg">

					<div class="grid-6 first">
						<div class="image-holder filesz dyn-file-sprite <?php echo apply_filters('dyn_class_by_mime', get_the_id()); ?>">
							<a href="<?php echo apply_filters('dyn_file_download_link', get_the_id()); ?>">
								<div class="hover-item dyn-file">
			                        <i class="fa fa-download"></i>
			                    </div>
							</a>
						</div>
						<div class="layout-title-box layout-title-box-<?php the_ID(); ?>" style="display:block;">
							<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php echo ctpreview( $content = get_the_title() , $length = 39 , $message = 'No Title available' , $html = ' [...]' ); ?></a></h6>
							<ul class="bottom-detailsul">
							<?php if($profile_id == get_current_user_id()){ ?>
							<li><p><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>" ><i class="fa fa-trash-o"></i> Delete</a></p></li>
							<?php } ?>
							<li><a class="detailsblock-btn" data-modal-id="myModal-<?php the_ID(); ?>"><i class="fa fa-info-circle" aria-hidden="true"></i> Details</a></li>
							<li><a class="changesettingsblock-btn" data-modal-id="myModalPrivacy-<?php the_ID(); ?>"><i class="fa fa-info-circle" aria-hidden="true"></i>Privacy</a></li>
							</ul>
						</div>
					</div>

					<script type="text/javascript">
					$(function(){

					var appendthis =  ("<div class='modal-overlayblockdiv'></div>");

						$('a[data-modal-id]').click(function(e) {
							e.preventDefault();
					    $("body").append(appendthis);
					    $(".modal-overlay").fadeTo(500, 0.7);
					    //$(".js-modalbox").fadeIn(500);
							var modalBox = $(this).attr('data-modal-id');
							$('#'+modalBox).fadeIn($(this).data());
						});


					$(".js-modal-close, .modal-overlay").click(function() {
					    $(".modal-box, .modal-overlay").fadeOut(500, function() {
					        $(".modal-overlay").remove();
					    });
					});

					$(window).resize(function() {
					    $(".modal-box").css({
					        top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
					        left: ($(window).width() - $(".modal-box").outerWidth()) / 2
					    });
					});

					$(window).resize();

					});
					</script>
		<div class="modal-box" id="myModalPrivacy-<?php the_ID(); ?>">
			<div class="modal-body">
				<a class="js-modal-close close privacyCloseI-<?php the_ID(); ?>">×</a>
				<div class="layout-2-details layout-2-details-<?php the_ID(); ?>">

						<div class="form-group">
							<label for="dyn-tags"><br/>Privacy Options:</label>
							<div class="checkbox">
								  <?php
								  //$selectUsersListArray = explode(",", $selectUsersList[0]);
                                      $selectUsersListArray = $selectUsersList;
								     if(isset($privacyOption[0]) && $privacyOption[0] != "")
									 {
								       //echo $privacyOption[0]."<br>";
										 ?>
										    <label>Private
											  <?php
											     if($privacyOption[0] == "private")
												 {
											        ?>
													   <input type="radio" id="privacy-radio-<?php the_ID(); ?>" name="privacy-option-<?php the_ID(); ?>" class="dyn-select-file" class="form-control" value="private" checked="checked">
													<?php
												 }
												 else
												 {
													 ?>
													   <input type="radio" id="privacy-radio-<?php the_ID(); ?>" name="privacy-option-<?php the_ID(); ?>" class="dyn-select-file" class="form-control" value="private">
													<?php
												 }
											  ?>
											</label>
											<label>Public
											<input type="radio" id="privacy-radio1-<?php the_ID(); ?>" class="dyn-select-file" class="form-control" value="public" <?php if (isset($privacyOption[0]) && $privacyOption[0]=="public") echo "checked";?> name="privacy-option2-<?php the_ID(); ?>">
											</label>
										 <?php
									 }
									 else
									 {
										 ?>
										    <label>Private
											  <input type="radio" id="privacy-radio-<?php the_ID(); ?>" name="privacy-option-<?php the_ID(); ?>" class="dyn-select-file" class="form-control" value="private">
											</label>
											<label>Public
											 <input type="radio" id="privacy-radio1" name="privacy-option2-<?php the_ID(); ?>" class="dyn-select-file" class="form-control" value="public">
											</label>
										 <?php
									 }
								  ?>
							</div>
							 <?php
								 if(isset($privacyOption[0]) && $privacyOption[0] != "")
								 {

									   if($privacyOption[0] == "private")
									   {
										       $args1 = array(
												  'role' => 'free_user',
												  'orderby' => 'id',
												  'order' => 'desc'
											   );
											  $subscribers = get_users($args1);
										    ?>
											<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/jquery.tokenize.js"></script>
												<div class="select-box-<?php the_ID(); ?>">
													<?php
													  echo '<select class="tokenize-sample" id="tokenize-'.get_the_ID().'" name="select_users_list[]" style="width: 350px; height: 150px;" multiple="multiple">';
														  foreach ($subscribers as $user) {
															  if(get_current_user_id() == $user->id)
															  { }
															  else
															  {
														          if( in_array($user->id, $selectUsersListArray) )
																  {
																	   echo '<option value="'.$user->id.'" selected="selected">' . $user->display_name.'</option>';
																  }
                                                                  else{
																	   echo '<option value="'.$user->id.'">' . $user->display_name.'</option>';
																  }
															  }
														  }
													  echo '</select>';
													?>

													<script type="text/javascript">
																		  $(document).ready(function(){
												  $("#privacy-radio").click(function(){
													 $(".select-box").slideDown();
												  });
												  $("#privacy-radio1").click(function(){
													 $(".select-box").slideUp();
													 $('select#select_users_list option').removeAttr("selected");
												  });

											  });
											  $('#tokenize-<?php echo get_the_ID(); ?>').tokenize();
				                                datas: "bower.json.php"
										  </script>
										  <style> .tokenize-sample { width: 350px;; }</style>
												</div>

										   <?php
									   }
									   else
									   {
                                            ?>

												<div style="display:none;" class="select-box-<?php the_ID(); ?>">
													<?php
													  echo '<select class="tokenize-sample" id="tokenize-'.get_the_ID().'" name="select_users_list[]" style="width: 350px; height: 150px;" multiple="multiple">';
														  $args1 = array(
															  'role' => 'free_user',
															  'orderby' => 'id',
															  'order' => 'desc'
														   );
														  $subscribers = get_users($args1);
														  foreach ($subscribers as $user) {
															  if(get_current_user_id() == $user->id)
															  { }
															  else
															  {
																  echo '<option value="'.$user->id.'">' . $user->display_name.'</option>';
															  }
														  }
													  echo '</select>';
													?>

													<script type="text/javascript">
																		  $(document).ready(function(){
												  $("#privacy-radio").click(function(){
													 $(".select-box").slideDown();
												  });
												  $("#privacy-radio1").click(function(){
													 $(".select-box").slideUp();
													 //$('select#select_users_list option').removeAttr("selected");
												  });

											  });
											      $('#tokenize-<?php echo get_the_ID(); ?>').tokenize();
				                                         datas: "bower.json.php"
										  </script>
										  <style> .tokenize-sample { width: 350px; }</style>
												</div>

										   <?php
									   }
								 }
								 else
								 {
									   ?>
									        <div style="display:none;" class="select-box-<?php the_ID(); ?>">
												<?php
												  echo '<select class="tokenize-sample" id="tokenize-'.get_the_ID().'" name="select_users_list[]" style="width: 350px; height: 150px;" multiple="multiple">';
													  $args1 = array(
														  'role' => 'free_user',
														  'orderby' => 'id',
														  'order' => 'desc'
													   );
													  $subscribers = get_users($args1);
													  foreach ($subscribers as $user) {
														  if(get_current_user_id() == $user->id)
														  { }
														  else
														  {
															  echo '<option value="'.$user->id.'">' . $user->display_name.'</option>';
														  }
													  }
												  echo '</select>';
												?>

												<script type="text/javascript">
													 $(document).ready(function(){
												  $("#privacy-radio").click(function(){
													 $(".select-box").slideDown();
												  });
												  $("#privacy-radio1").click(function(){
													 $(".select-box").slideUp();
													 //$('select#select_users_list option').removeAttr("selected");
												  });

											  });
											     $('#tokenize-<?php echo get_the_ID(); ?>').tokenize();
				                                         datas: "bower.json.php"
										  </script>
										  <style> .tokenize-sample { width: 350px; }</style>
											</div>

									   <?php
								 }
							 ?>
							 <br>
							 <div id="msgLoader-<?php the_ID(); ?>"></div>
                             <input type="button" id="save-Privacy-Option-<?php the_ID(); ?>" name="save-privacy-option-<?php the_ID(); ?>" value="Save Privacy Option" currentUserID="<?php echo get_current_user_id(); ?>" postID="<?php echo get_the_ID(); ?>">
						</div>


					<script>
                      $(document).ready(function(){
						  $("#privacy-radio-<?php the_ID(); ?>").click(function(){
							 $("#privacy-radio1-<?php the_ID(); ?>").prop( "checked", false );
						     $(".select-box-<?php the_ID(); ?>").slideDown();
						  });
						  $("#privacy-radio1-<?php the_ID(); ?>").click(function(){
							 $("#privacy-radio-<?php the_ID(); ?>").prop( "checked", false );
						     $(".select-box-<?php the_ID(); ?>").slideUp();
							 $('select#select_users_list-<?php the_ID(); ?> option').removeAttr("selected");
						  });
						  $(".privacyCloseI-<?php the_ID(); ?>").click(function(){
							 $('#msgLoader-<?php the_ID(); ?>').html('');
						  });
						  $("#save-Privacy-Option-<?php the_ID(); ?>").click(function(){
							   var userID = $(this).attr('currentUserID');
							   var postID = $(this).attr('postID');
							   var select_users_list = $('#tokenize-'+postID).val();
							   if ( typeof(select_users_list) !== "undefined" && select_users_list !== null )
							   { }
						       else
							   {
								   select_users_list = '';
							   }
							   var privacyOptionV = "";
							   if($('#privacy-radio-<?php the_ID(); ?>').is(':checked'))
							   {
								   var privateChecked = "yes";
							   }
							   else
							   {
								   var privateChecked = "no";
							   }
							   if($('#privacy-radio1-<?php the_ID(); ?>').is(':checked'))
							   {
								   var publicChecked = "yes";
							   }
							   else
							   {
								   var publicChecked = "no";
							   }
							   if(privateChecked == "yes")
							   {
								   privacyOptionV = "private";
							   }
							   else if(publicChecked == "yes")
							   {
								   privacyOptionV = "public";
							   }
							   else
							   {
								   privacyOptionV = "";
							   }
                               $.ajax({
									type: 'POST',
									url: "<?php echo site_url(); ?>/update-Privacy.php",
									data: { userID: userID, postID: postID, privacyOptionV : privacyOptionV,
									select_users_list : select_users_list},
									beforeSend: function(){
									  $('#msgLoader-<?php the_ID(); ?>').html('<img src="<?php echo site_url(); ?>/wp-content/themes/videopress/images/loading.gif" />');
									},
									success: function(data){
										 $('#msgLoader-<?php the_ID(); ?>').html(data);

									}

								});
						  });


                      });

					 $('#tokenize').tokenize();
                    </script>
					<style> .tokenize-sample { width: 350px; }</style>
					<style>
                      #privacy-radio1-<?php the_ID(); ?>, #privacy-radio-<?php the_ID(); ?> {
                        padding: 5px;
                        text-align: center;
                      }

                     #select-box-<?php the_ID(); ?> {
                       padding: 50px;
                       display: none;
                     }
                    </style>
				</div>
			</div>
</div>
					<div class="modal-box" id="myModal-<?php the_ID(); ?>">
					 <div class="modal-body">
					 <a class="js-modal-close close">×</a>
					<div class="layout-2-details layout-2-details-<?php the_ID(); ?>">
						<div class="image-holder filesz dyn-file-sprite <?php echo apply_filters('dyn_class_by_mime', get_the_id()); ?>">
							<a href="<?php echo apply_filters('dyn_file_download_link', get_the_id()); ?>">
								<div class="hover-item dyn-file">
			                        <i class="fa fa-download"></i>
			                    </div>
							</a>
						</div>
						<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title();?></a></h6>
						<div>File Type: <?php echo apply_filters( 'dyn_file_type_by_mime', get_the_ID() ); ?></div>
						<?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
						<div class="video_excerpt_text one"><h4 class="tittle-h4">Description 3</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = '' , $message = 'No Description available' , $html = ' [...]' );?></div></div>
						<div class="video_excerpt_text two"><h4 class="tittle-h4">Reference</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_post_meta(get_the_ID(),'video_options_refr',true) , $length = '' , $message = 'No referance given' , $html = ' [...]' );?></div></div>
						<ul class="stats">
							<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
							<li><?php videopress_countviews( get_the_ID() ); ?></li>
							<li><i class="fa fa-wrench"></i>
							<?php
								$result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
								echo count($result);
							?> Endorsments</li>
							<li><i class="fa  fa-heart"></i>
							<?php
							  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
							  $result1 = $wpdb->get_results($sql_aux);
							  echo count($result1);
							?> Favorites</li>
							<!--end favorite-->
							<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
							<li><?php comments_number() ?></li>
							<li><i class="fa fa-download"></i> <?php echo apply_filters( 'dyn_number_of_download', get_the_id()); ?></li>
						</ul>
						<div class="clear"></div>
						<p><?php echo videopress_content('240'); ?></p>
						<?php if($profile_id == get_current_user_id()){ ?>
						<p><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>" ><i class="fa fa-trash-o"></i> Delete</a></p>
						<?php } ?>
					</div></div></div>



					<div class="layout-2-details layout-2-details-<?php the_ID(); ?>" style="display:none;">
						<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title();?></a></h6>
						<div>File Type: <?php echo apply_filters( 'dyn_file_type_by_mime', get_the_ID() ); ?></div>
						<?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
						<div class="video_excerpt_text one"><h4 class="tittle-h4">Description</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = 100 , $message = 'No Description available' , $html = '<a data-modal-id="myModal-'.get_the_ID().'"> [...]</a>' ); ?></div></div>
						<ul class="stats">
							<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
							<li><?php videopress_countviews( get_the_ID() ); ?></li>
							<li><i class="fa fa-wrench"></i>
							<?php
								$result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
								echo count($result);
							?> Endorsments</li>
							<li><i class="fa  fa-heart"></i>
							<?php
							  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
							  $result1 = $wpdb->get_results($sql_aux);
							  echo count($result1);
							?> Favorites</li>
							<!--end favorite-->
							<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
							<li><?php comments_number() ?></li>
							<li><i class="fa fa-download"></i> <?php echo apply_filters( 'dyn_number_of_download', get_the_id()); ?></li>
						</ul>
						<div class="clear"></div>
						<p><?php echo videopress_content('240'); ?>
						<?php if($profile_id == get_current_user_id()){ ?>
						        <a href="#" class="del-video" data-id="<?= get_the_ID(); ?>" ><i class="fa fa-trash-o"></i> Delete</a>
						<?php } ?>
						        <a class="privacy-video" data-modal-id="myModalPrivacy-<?php the_ID(); ?>"><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;Privacy</a>
						</p>
					</div>

					<div class="clear"></div>
				</div>
			</div>
			<!-- End Layout Wrapper -->
		<?php
		$ri++;
					 }
					 else
					 {    // Case where logged in User is not same as Video Author User
						  if(isset($privacyOption[0]) && $privacyOption[0] != "private")
						  {
						  //	if( ($ri % 6) == 0){echo '</div><div class="row">'; }
								?>
			<div class="item2 col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 post-<?php the_ID(); ?>">
				<div class="thumbnail layout-2-wrapper solid-bg">

					<div class="grid-6 first">
						<div class="image-holder filesz dyn-file-sprite <?php echo apply_filters('dyn_class_by_mime', get_the_id()); ?>">
							<a href="<?php echo apply_filters('dyn_file_download_link', get_the_id()); ?>">
								<div class="hover-item dyn-file">
			                        <i class="fa fa-download"></i>
			                    </div>
							</a>
						</div>
						<div class="layout-title-box layout-title-box-<?php the_ID(); ?>" style="display:block;">
							<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php echo ctpreview( $content = get_the_title() , $length = 39 , $message = 'No Title available' , $html = ' [...]' ); ?></a></h6>
							<ul class="bottom-detailsul">
							<?php if($profile_id == get_current_user_id()){ ?>
							<li><p><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>" ><i class="fa fa-trash-o"></i> Delete</a></p></li>
							<?php } ?>
							<li><a class="detailsblock-btn" data-modal-id="myModal-<?php the_ID(); ?>"><i class="fa fa-info-circle" aria-hidden="true"></i> Details</a></li>
							</ul>
						</div>
					</div>

<script type="text/javascript">
$(function(){

var appendthis =  ("<div class='modal-overlayblockdiv'></div>");

	$('a[data-modal-id]').click(function(e) {
		e.preventDefault();
    $("body").append(appendthis);
    $(".modal-overlay").fadeTo(500, 0.7);
    //$(".js-modalbox").fadeIn(500);
		var modalBox = $(this).attr('data-modal-id');
		$('#'+modalBox).fadeIn($(this).data());
	});


$(".js-modal-close, .modal-overlay").click(function() {
    $(".modal-box, .modal-overlay").fadeOut(500, function() {
        $(".modal-overlay").remove();
    });
});

$(window).resize(function() {
    $(".modal-box").css({
        top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
        left: ($(window).width() - $(".modal-box").outerWidth()) / 2
    });
});

$(window).resize();

});
</script>
					<div class="modal-box" id="myModal-<?php the_ID(); ?>">
					 <div class="modal-body">
					 <a class="js-modal-close close">×</a>
					<div class="layout-2-details layout-2-details-<?php the_ID(); ?>">
						<div class="image-holder filesz dyn-file-sprite <?php echo apply_filters('dyn_class_by_mime', get_the_id()); ?>">
							<a href="<?php echo apply_filters('dyn_file_download_link', get_the_id()); ?>">
								<div class="hover-item dyn-file">
			                        <i class="fa fa-download"></i>
			                    </div>
							</a>
						</div>
						<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title();?></a></h6>
						<div>File Type: <?php echo apply_filters( 'dyn_file_type_by_mime', get_the_ID() ); ?></div>
						<?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
						<div class="video_excerpt_text one"><h4 class="tittle-h4">Description 3</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = '' , $message = 'No Description available' , $html = ' [...]' );?></div></div>
						<div class="video_excerpt_text two"><h4 class="tittle-h4">Reference</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_post_meta(get_the_ID(),'video_options_refr',true) , $length = '' , $message = 'No referance given' , $html = ' [...]' );?></div></div>
						<ul class="stats">
							<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
							<li><?php videopress_countviews( get_the_ID() ); ?></li>
							<li><i class="fa fa-wrench"></i>
							<?php
								$result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
								echo count($result);
							?> Endorsments</li>
							<li><i class="fa  fa-heart"></i>
							<?php
							  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
							  $result1 = $wpdb->get_results($sql_aux);
							  echo count($result1);
							?> Favorites</li>
							<!--end favorite-->
							<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
							<li><?php comments_number() ?></li>
							<li><i class="fa fa-download"></i> <?php echo apply_filters( 'dyn_number_of_download', get_the_id()); ?></li>
						</ul>
						<div class="clear"></div>
						<p><?php echo videopress_content('240'); ?></p>
						<?php if($profile_id == get_current_user_id()){ ?>
						<p><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>" ><i class="fa fa-trash-o"></i> Delete</a></p>
						<?php } ?>
					</div></div></div>



					<div class="layout-2-details layout-2-details-<?php the_ID(); ?>" style="display:none;">
						<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title();?></a></h6>
						<div>File Type: <?php echo apply_filters( 'dyn_file_type_by_mime', get_the_ID() ); ?></div>
						<?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
						<div class="video_excerpt_text one"><h4 class="tittle-h4">Description</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = 100 , $message = 'No Description available' , $html = '<a data-modal-id="myModal-'.get_the_ID().'"> [...]</a>' ); ?></div></div>
						<ul class="stats">
							<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
							<li><?php videopress_countviews( get_the_ID() ); ?></li>
							<li><i class="fa fa-wrench"></i>
							<?php
								$result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
								echo count($result);
							?> Endorsments</li>
							<li><i class="fa  fa-heart"></i>
							<?php
							  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
							  $result1 = $wpdb->get_results($sql_aux);
							  echo count($result1);
							?> Favorites</li>
							<!--end favorite-->
							<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
							<li><?php comments_number() ?></li>
							<li><i class="fa fa-download"></i> <?php echo apply_filters( 'dyn_number_of_download', get_the_id()); ?></li>
						</ul>
						<div class="clear"></div>
						<p><?php echo videopress_content('240'); ?></p>
						<?php if($profile_id == get_current_user_id()){ ?>
						<p><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>" ><i class="fa fa-trash-o"></i> Delete</a></p>
						<?php } ?>
					</div>

					<div class="clear"></div>
				</div>
			</div>
			<!-- End Layout Wrapper -->
		<?php
		$ri++;
						  }
						  else
						  {
							  if(!isset($privacyOption[0]))
							  {
							  	//if( ($ri % 6) == 0 && $ri != 0){echo '</div><div class="row">'; }
									?>
			<div class="item2 col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 post-<?php the_ID(); ?>">
				<div class="thumbnail layout-2-wrapper solid-bg">

					<div class="grid-6 first">
						<div class="image-holder filesz dyn-file-sprite <?php echo apply_filters('dyn_class_by_mime', get_the_id()); ?>">
							<a href="<?php echo apply_filters('dyn_file_download_link', get_the_id()); ?>">
								<div class="hover-item dyn-file">
			                        <i class="fa fa-download"></i>
			                    </div>
							</a>
						</div>
						<div class="layout-title-box layout-title-box-<?php the_ID(); ?>" style="display:block;">
							<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php echo ctpreview( $content = get_the_title() , $length = 39 , $message = 'No Title available' , $html = ' [...]' ); ?></a></h6>
							<ul class="bottom-detailsul">
							<?php if($profile_id == get_current_user_id()){ ?>
							<li><p><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>" ><i class="fa fa-trash-o"></i> Delete</a></p></li>
							<?php } ?>
							<li><a class="detailsblock-btn" data-modal-id="myModal-<?php the_ID(); ?>"><i class="fa fa-info-circle" aria-hidden="true"></i> Details</a></li>
							</ul>
						</div>
					</div>

<script type="text/javascript">
$(function(){

var appendthis =  ("<div class='modal-overlayblockdiv'></div>");

	$('a[data-modal-id]').click(function(e) {
		e.preventDefault();
    $("body").append(appendthis);
    $(".modal-overlay").fadeTo(500, 0.7);
    //$(".js-modalbox").fadeIn(500);
		var modalBox = $(this).attr('data-modal-id');
		$('#'+modalBox).fadeIn($(this).data());
	});


$(".js-modal-close, .modal-overlay").click(function() {
    $(".modal-box, .modal-overlay").fadeOut(500, function() {
        $(".modal-overlay").remove();
    });
});

$(window).resize(function() {
    $(".modal-box").css({
        top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
        left: ($(window).width() - $(".modal-box").outerWidth()) / 2
    });
});

$(window).resize();

});
</script>
					<div class="modal-box" id="myModal-<?php the_ID(); ?>">
					 <div class="modal-body">
					 <a class="js-modal-close close">×</a>
					<div class="layout-2-details layout-2-details-<?php the_ID(); ?>">
						<div class="image-holder filesz dyn-file-sprite <?php echo apply_filters('dyn_class_by_mime', get_the_id()); ?>">
							<a href="<?php echo apply_filters('dyn_file_download_link', get_the_id()); ?>">
								<div class="hover-item dyn-file">
			                        <i class="fa fa-download"></i>
			                    </div>
							</a>
						</div>
						<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title();?></a></h6>
						<div>File Type: <?php echo apply_filters( 'dyn_file_type_by_mime', get_the_ID() ); ?></div>
						<?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
						<div class="video_excerpt_text one"><h4 class="tittle-h4">Description 3</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = '' , $message = 'No Description available' , $html = ' [...]' );?></div></div>
						<div class="video_excerpt_text two"><h4 class="tittle-h4">Reference</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_post_meta(get_the_ID(),'video_options_refr',true) , $length = '' , $message = 'No referance given' , $html = ' [...]' );?></div></div>
						<ul class="stats">
							<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
							<li><?php videopress_countviews( get_the_ID() ); ?></li>
							<li><i class="fa fa-wrench"></i>
							<?php
								$result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
								echo count($result);
							?> Endorsments</li>
							<li><i class="fa  fa-heart"></i>
							<?php
							  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
							  $result1 = $wpdb->get_results($sql_aux);
							  echo count($result1);
							?> Favorites</li>
							<!--end favorite-->
							<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
							<li><?php comments_number() ?></li>
							<li><i class="fa fa-download"></i> <?php echo apply_filters( 'dyn_number_of_download', get_the_id()); ?></li>
						</ul>
						<div class="clear"></div>
						<p><?php echo videopress_content('240'); ?></p>
						<?php if($profile_id == get_current_user_id()){ ?>
						<p><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>" ><i class="fa fa-trash-o"></i> Delete</a></p>
						<?php } ?>
					</div></div></div>



					<div class="layout-2-details layout-2-details-<?php the_ID(); ?>" style="display:none;">
						<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title();?></a></h6>
						<div>File Type: <?php echo apply_filters( 'dyn_file_type_by_mime', get_the_ID() ); ?></div>
						<?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
						<div class="video_excerpt_text one"><h4 class="tittle-h4">Description</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = 100 , $message = 'No Description available' , $html = '<a data-modal-id="myModal-'.get_the_ID().'"> [...]</a>' ); ?></div></div>
						<ul class="stats">
							<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
							<li><?php videopress_countviews( get_the_ID() ); ?></li>
							<li><i class="fa fa-wrench"></i>
							<?php
								$result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
								echo count($result);
							?> Endorsments</li>
							<li><i class="fa  fa-heart"></i>
							<?php
							  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
							  $result1 = $wpdb->get_results($sql_aux);
							  echo count($result1);
							?> Favorites</li>
							<!--end favorite-->
							<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
							<li><?php comments_number() ?></li>
							<li><i class="fa fa-download"></i> <?php echo apply_filters( 'dyn_number_of_download', get_the_id()); ?></li>
						</ul>
						<div class="clear"></div>
						<p><?php echo videopress_content('240'); ?></p>
						<?php if($profile_id == get_current_user_id()){ ?>
						<p><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>" ><i class="fa fa-trash-o"></i> Delete</a></p>
						<?php } ?>
					</div>

					<div class="clear"></div>
				</div>
			</div>
			<!-- End Layout Wrapper -->
		<?php
			$ri++;
							  }
							  else
							  {
									if( is_array($selectUsersList) and count($selectUsersList) > 0 )
									   {   // case where user access list is available
											 if( in_array($user_ID, $selectUsersList) )
											 {
											 	//if( ($ri % 6) == 0 && $ri != 0){echo '</div><div class="row">'; }
												   ?>
			<div class="item2 col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 post-<?php the_ID(); ?>">
				<div class="thumbnail layout-2-wrapper solid-bg">

					<div class="grid-6 first">
						<div class="image-holder filesz dyn-file-sprite <?php echo apply_filters('dyn_class_by_mime', get_the_id()); ?>">
							<a href="<?php echo apply_filters('dyn_file_download_link', get_the_id()); ?>">
								<div class="hover-item dyn-file">
			                        <i class="fa fa-download"></i>
			                    </div>
							</a>
						</div>
						<div class="layout-title-box layout-title-box-<?php the_ID(); ?>" style="display:block;">
							<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php echo ctpreview( $content = get_the_title() , $length = 39 , $message = 'No Title available' , $html = ' [...]' ); ?></a></h6>
							<ul class="bottom-detailsul">
							<?php if($profile_id == get_current_user_id()){ ?>
							<li><p><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>" ><i class="fa fa-trash-o"></i> Delete</a></p></li>
							<?php } ?>
							<li><a class="detailsblock-btn" data-modal-id="myModal-<?php the_ID(); ?>"><i class="fa fa-info-circle" aria-hidden="true"></i> Details</a></li>
							</ul>
						</div>
					</div>

<script type="text/javascript">
$(function(){

var appendthis =  ("<div class='modal-overlayblockdiv'></div>");

	$('a[data-modal-id]').click(function(e) {
		e.preventDefault();
    $("body").append(appendthis);
    $(".modal-overlay").fadeTo(500, 0.7);
    //$(".js-modalbox").fadeIn(500);
		var modalBox = $(this).attr('data-modal-id');
		$('#'+modalBox).fadeIn($(this).data());
	});


$(".js-modal-close, .modal-overlay").click(function() {
    $(".modal-box, .modal-overlay").fadeOut(500, function() {
        $(".modal-overlay").remove();
    });
});

$(window).resize(function() {
    $(".modal-box").css({
        top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
        left: ($(window).width() - $(".modal-box").outerWidth()) / 2
    });
});

$(window).resize();

});
</script>
					<div class="modal-box" id="myModal-<?php the_ID(); ?>">
					 <div class="modal-body">
					 <a class="js-modal-close close">×</a>
					<div class="layout-2-details layout-2-details-<?php the_ID(); ?>">
						<div class="image-holder filesz dyn-file-sprite <?php echo apply_filters('dyn_class_by_mime', get_the_id()); ?>">
							<a href="<?php echo apply_filters('dyn_file_download_link', get_the_id()); ?>">
								<div class="hover-item dyn-file">
			                        <i class="fa fa-download"></i>
			                    </div>
							</a>
						</div>
						<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title();?></a></h6>
						<div>File Type: <?php echo apply_filters( 'dyn_file_type_by_mime', get_the_ID() ); ?></div>
						<?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
						<div class="video_excerpt_text one"><h4 class="tittle-h4">Description 3</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = '' , $message = 'No Description available' , $html = ' [...]' );?></div></div>
						<div class="video_excerpt_text two"><h4 class="tittle-h4">Reference</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_post_meta(get_the_ID(),'video_options_refr',true) , $length = '' , $message = 'No referance given' , $html = ' [...]' );?></div></div>
						<ul class="stats">
							<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
							<li><?php videopress_countviews( get_the_ID() ); ?></li>
							<li><i class="fa fa-wrench"></i>
							<?php
								$result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
								echo count($result);
							?> Endorsments</li>
							<li><i class="fa  fa-heart"></i>
							<?php
							  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
							  $result1 = $wpdb->get_results($sql_aux);
							  echo count($result1);
							?> Favorites</li>
							<!--end favorite-->
							<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
							<li><?php comments_number() ?></li>
							<li><i class="fa fa-download"></i> <?php echo apply_filters( 'dyn_number_of_download', get_the_id()); ?></li>
						</ul>
						<div class="clear"></div>
						<p><?php echo videopress_content('240'); ?></p>
						<?php if($profile_id == get_current_user_id()){ ?>
						<p><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>" ><i class="fa fa-trash-o"></i> Delete</a></p>
						<?php } ?>
					</div></div></div>


					<div class="layout-2-details layout-2-details-<?php the_ID(); ?>" style="display:none;">
						<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title();?></a></h6>
						<div>File Type: <?php echo apply_filters( 'dyn_file_type_by_mime', get_the_ID() ); ?></div>
						<?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
						<div class="video_excerpt_text one"><h4 class="tittle-h4">Description</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = 100 , $message = 'No Description available' , $html = '<a data-modal-id="myModal-'.get_the_ID().'"> [...]</a>' ); ?></div></div>
						<ul class="stats">
							<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
							<li><?php videopress_countviews( get_the_ID() ); ?></li>
							<li><i class="fa fa-wrench"></i>
							<?php
								$result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
								echo count($result);
							?> Endorsments</li>
							<li><i class="fa  fa-heart"></i>
							<?php
							  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
							  $result1 = $wpdb->get_results($sql_aux);
							  echo count($result1);
							?> Favorites</li>
							<!--end favorite-->
							<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
							<li><?php comments_number() ?></li>
							<li><i class="fa fa-download"></i> <?php echo apply_filters( 'dyn_number_of_download', get_the_id()); ?></li>
						</ul>
						<div class="clear"></div>
						<p><?php echo videopress_content('240'); ?></p>
						<?php if($profile_id == get_current_user_id()){ ?>
						<p><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>" ><i class="fa fa-trash-o"></i> Delete</a></p>
						<?php } ?>
					</div>

					<div class="clear"></div>
				</div>
			</div>
			<!-- End Layout Wrapper -->
		<?php
		$ri++;
											 }
											 else
											 {
											 }
									   }
									   else
									   {  }
							  }
						  }
					 }
				}
		endwhile;
		echo '</div>';
		echo '</div>';

		/* FA replaced
		  echo paginate_links(array(
		  'show_all'  => false,
		  'total' => 3,
		  'add_args' => array(
		  'display_page' => 'file'

		   )
	     ));
		*/

		$display_file_p=isset($_GET['display_page']) ? $_GET['display_page'] : '';

        $ftotal=$wp_query->found_posts;

         if($display_file_p=='file'){
          $fcurrent=max( 1, $paged );
         }
         else{
          $fcurrent=1;
        }


        $fpaginate = paginate_links( array(
		'format' => '?page/%#%/',
		'current' => $fcurrent,
		'type'=>'array',
		'end_size'=>1,
		'mid_size'=>4,
		'total' => ceil($ftotal / 12)//FA: total amount of pages
           ) );

    $fcount=count($fpaginate);


     $maxf=ceil(($ftotal/12)/2);

    if( $fcurrent<= ceil($ftotal / 12)  ) {

     if( ceil($ftotal / 12) <5 ) {
       foreach($fpaginate as $flnk){echo $flnk.'&nbsp;';}
      }

     else if( ceil($ftotal / 12) >= 5 ){

    if ($fcurrent<=6)
 	{
 	if ($fcurrent>2)
 	{echo $fpaginate[0].'&nbsp;';}
       if($fpaginate[$fcurrent-2]){
 	echo $fpaginate[$fcurrent-2].'&nbsp;';
      }
 	echo $fpaginate[$fcurrent-1].'&nbsp;';
 	echo $fpaginate[$fcurrent].'&nbsp;';
	if( isset($fpaginate[$fcurrent+1]) && (($fcount-1) > $fcurrent+1) ){echo $fpaginate[$fcurrent+1].'&nbsp;';}
	if( isset($fpaginate[$fcurrent+2]) && (($fcount-1) > $fcurrent+2) ){echo $fpaginate[$fcurrent+2].'&nbsp;';}

 	}
	else if ($fcurrent>=7)
 	{
 	echo $fpaginate[0].'&nbsp;';
 	echo $fpaginate[5].'&nbsp;';
 	echo $fpaginate[6].'&nbsp;';
 	echo $fpaginate[7].'&nbsp;';
	if( isset($fpaginate[8]) && (($fcount-1) > 8) ){echo $fpaginate[8].'&nbsp;';}
	if( isset($fpaginate[9]) && (($fcount-1) > 9) ){echo $fpaginate[9].'&nbsp;';}
 	}


	if( $fcurrent <= (ceil($ftotal/12)-1) ){
	echo $fpaginate[$fcount-1];
 	 }

 	}

  }

      //FA: end of paginate replace

		$wp_query = NULL;
		$wp_query = $temp_query;
	}

	public function number_of_download($post_id){
		$count = get_post_meta( $post_id, 'dyn_download_count', true );

		if(!$count)
			$count = 0;
		if($count == 1)
			return "1 Download";
		else
			return "$count Downloads";
	}

	public function add_download_query_var($qvars){
		$qvars[] = 'dyn_file_download_id';
		return $qvars;
	}

	public function update_file_download_count(){
		global $wpdb;
		if(!isset($_GET['dyn_file_download_id']))
			return;
		$post_id = intval($_GET['dyn_file_download_id']);
		if(!$post_id || get_post_type( $post_id ) != 'dyn_file')
			return;

		$download_id = get_post_meta( $post_id, 'dyn_upload_value', true);
		if(!$download_id)
			return;
		$file = get_attached_file($download_id);
		if (file_exists($file)) {
			$download_count = get_post_meta( $post_id, 'dyn_download_count', true);
			if(add_post_meta($post_id, 'ip_download', get_ip_address(), true)){
				if(!$download_count)
					$download_count = 1;
				else
					$download_count += 1;
				update_post_meta( $post_id, 'dyn_download_count', $download_count );
			}
			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename="'.basename($file).'"');
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header('Content-Length: ' . filesize($file));
			readfile($file);
		    exit;
		}
		else{
			echo "File unavailable";
		}
		die();
	}

	public function dyn_file_download_link($post_id){
		$uploaded_value = get_post_meta( $post_id, 'dyn_upload_value', true );

		$url = home_url( '/custom-file-download-link/' );
		$url = add_query_arg('dyn_file_download_id', $post_id, $url);

		return $url;
	}

	public function file_image_div($output, $post_id){
		$uploaded_value = get_post_meta( $post_id, 'dyn_upload_value', true );
		ob_start();
		?>
		<div class="dyn-file-sprite <?php echo Dyn_Filesystem_Helper::class_by_mime(get_post_mime_type($uploaded_value), pathinfo(get_attached_file($uploaded_value), PATHINFO_EXTENSION)); ?>">
			<i class="fa fa-download fa-3x download-icon"></i>
		</div>
		<?php
		$output = ob_get_contents();
		ob_end_clean();
		return $output;
	}

	public function register_filesystem_post_type(){
		$labels = array(
			'name'               => _x( 'DYN File', 'Genral name for post type', 'dyn-filesystem' ),
			'singular_name'      => _x( 'File', 'post type singular name', 'dyn-filesystem' ),
			'menu_name'          => _x( 'Files', 'admin menu', 'dyn-filesystem' ),
			'name_admin_bar'     => _x( 'Files', 'add new on admin bar', 'dyn-filesystem' ),
			'add_new'            => _x( 'Add New', 'file', 'dyn-filesystem' ),
			'add_new_item'       => __( 'Add New File', 'dyn-filesystem' ),
			'new_item'           => __( 'New File', 'dyn-filesystem' ),
			'edit_item'          => __( 'Edit File', 'dyn-filesystem' ),
			'view_item'          => __( 'View File', 'dyn-filesystem' ),
			'all_items'          => __( 'All Files', 'dyn-filesystem' ),
			'search_items'       => __( 'Search Files', 'dyn-filesystem' ),
			'parent_item_colon'  => __( 'Parent Files:', 'dyn-filesystem' ),
			'not_found'          => __( 'No Files found.', 'dyn-filesystem' ),
			'not_found_in_trash' => __( 'No Files found in Trash.', 'dyn-filesystem' )
		);

		$args = array(
			'labels'             => $labels,
			'description'        => __( 'This post type store all the files.', 'dyn-filesystem' ),
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'file' ),
			'capability_type'    => 'post',
			'taxonomies' 		 => array('category', 'post_tag'),
			'has_archive'        => true,
			'hierarchical'       => false,
			'menu_position'      => null,
			'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
		);

		register_post_type( 'dyn_file', $args );
	}

}
