/**
 * yith-infs.js
 *
 * @author Your Inspiration Themes
 * @package YITH Infinite Scrolling
 * @version 1.0.0
 */

jQuery(document).ready( function($) {
    "use strict";

    if( typeof yith_infs_premium !== 'undefined' && yith_infs_premium.options ) {

        $.init_infinitescroll = function() {
            $.each( yith_infs_premium.options, function (key, value) {

                $.yit_infinitescroll(value);
            });
        };

        $.init_infinitescroll();

        $(document).on( 'yith-wcan-ajax-loading', function(){
            $( '.yith-infs-button-wrapper' ).remove();
        });

        $(document).on( 'yith-wcan-ajax-filtered', function(){
            $.init_infinitescroll();
        });

    }
});