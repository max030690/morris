<?php
/**
 * Main class
 *
 * @author Yithemes
 * @package YITH Infinite Scrolling
 * @version 1.0.0
 */


if ( ! defined( 'YITH_INFS' ) ) {
	exit;
} // Exit if accessed directly

if ( ! class_exists( 'YITH_INFS' ) ) {
	/**
	 * YITH Infinite Scrolling
	 *
	 * @since 1.0.0
	 */
	class YITH_INFS {

		/**
		 * Single instance of the class
		 *
		 * @var \YITH_INFS
		 * @since 1.0.0
		 */
		protected static $instance;

		/**
		 * Plugin version
		 *
		 * @var string
		 * @since 1.0.0
		 */
		public $version = YITH_INFS_VERSION;

		/**
		 * Plugin object
		 *
		 * @var string
		 * @since 1.0.0
		 */
		public $obj = null;

		/**
		 * Returns single instance of the class
		 *
		 * @return \YITH_INFS
		 * @since 1.0.0
		 */
		public static function get_instance() {
			if ( is_null( self::$instance ) ) {
				self::$instance = new self();
			}

			return self::$instance;
		}

		/**
		 * Constructor
		 *
		 * @return mixed YITH_INFS_Admin | YITH_INFS_Frontend
		 * @since 1.0.0
		 */
		public function __construct() {

			// Load Plugin Framework
			add_action('after_setup_theme', array($this, 'plugin_fw_loader'), 1);

			$enable = YITH_INFS_Admin::get_option( 'yith-infs-enable', 'yes' ) == 'yes';
			$active_mobile = YITH_INFS_Admin::get_option('yith-infs-enable-mobile', 'yes') == 'yes';

			// Class admin
			if ( is_admin() ) {
				YITH_INFS_Admin_Premium();
			}
			// Frontend class
			if ( $enable && ( ! wp_is_mobile() || ( wp_is_mobile() && $active_mobile ) ) ){
				YITH_INFS_Frontend_Premium();
			}
		}

		/**
		 * Load Plugin Framework
		 *
		 * @since  1.0.0
		 * @access public
		 * @return void
		 * @author Andrea Grillo <andrea.grillo@yithemes.com>
		 */
		public function plugin_fw_loader() {

			if ( ! defined( 'YIT' ) || ! defined( 'YIT_CORE_PLUGIN' ) ) {
				require_once( YITH_INFS_DIR . '/plugin-fw/yit-plugin.php' );
			}

		}
	}
}

/**
 * Unique access to instance of YITH_INFS class
 *
 * @return \YITH_INFS
 * @since 1.0.0
 */
function YITH_INFS(){
	return YITH_INFS::get_instance();
}