<?php
/*
 Plugin Name: WC Vendors Affiliates
 Description: WC Vendors Affiliates
 Version: 1.0.0
 Requires at least: 4.0
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( !defined( 'wcv_a_plugin_dir' ) ) 		define( 'wcv_a_plugin_dir', trailingslashit( dirname( __FILE__ ) ) . '/' );
register_activation_hook( __FILE__, array( 'WC_Vendors_Affiliates', 'install' ) );
register_uninstall_hook( __FILE__, array( 'WC_Vendors_Affiliates', 'uninstall' ) );

class WC_Vendors_Affiliates
{

    public static function getTableName()
    {
        global $wpdb;
        return $wpdb->prefix . 'vendor_affiliates';
    }

    public function __construct()
    {
        add_action( 'wcvendors_settings_after_shop_name', array( $this, 'add_affiliates_rate_input' ) );
        add_action( 'wcvendors_shop_settings_saved', array( $this, 'save_affiliates_rate_settings' ) );
        add_action( 'wcv_pro_store_settings_saved', array( $this, 'save_affiliates_rate_settings' ) );
        add_action( 'become_affiliate', array( $this, 'generate_become_affiliate_button' ));
        add_action( 'init', array( $this, 'submit_become_affiliate_button' ));
        add_action('affwp_register_user', array($this, 'add_registered_affiliate_to_vendor'), 10, 3);
        add_filter('affwp_calc_referral_amount', array($this, 'get_affiliate_rate'), 10, 5);
        add_filter('wcv_commission_rate', array($this, 'pull_affiliate_commission_from_vendor'), 90, 6);
        add_filter('wcv_order_table_columns', array($this, 'wcv_order_table_columns'), 90);
        add_filter('wcv_orders_table_rows', array($this, 'wcv_orders_table_rows'), 90);
        add_filter( 'manage_shop_order_posts_columns', array( $this, 'shop_order_columns' ), 90);
        add_action( 'manage_shop_order_posts_custom_column', array( $this, 'render_shop_order_columns' ), 2 );
    }

    public function shop_order_columns($columns){
        $columns['total_without']      = __( 'Total without commissions', 'woocommerce' );
        $columns['order_total']      = __( 'Total', 'woocommerce' );
        $columns['affiliate_commission']    = __( 'Affiliate Commission' );
        $columns['system_commission']    = __( 'System Commission', 'woocommerce' );
        return $columns;
    }

    public function render_shop_order_columns($column){
        global $post, $woocommerce, $the_order,$wpdb;
        switch ( $column ) {
            case 'total_without' :
                echo $the_order->get_formatted_order_total();

                if ( $the_order->payment_method_title ) {
                    echo '<small class="meta">' . __( 'Via', 'woocommerce' ) . ' ' . esc_html( $the_order->payment_method_title ) . '</small>';
                }
                break;

            case 'affiliate_commission' :
                $order = wc_get_order( $post->ID );
                $items = $order->get_items();
                foreach ( $items as $item ) {
                    $order = new WC_Order($the_order->id);
                    $product_price = $order->get_total();
                    $item_id = $item['product_id'];
                    $rate = $this->get_affilate_rate($item_id);
                    $affiliate_commission = $product_price * ($rate / 100);
                    echo wc_price($affiliate_commission);
                }
                break;

            case 'order_total' :
                $commission = $wpdb->get_results( "
								SELECT * FROM {$wpdb->prefix}pv_commission
								WHERE     order_id = '" . $post->ID . "'
								ORDER BY time DESC
							" );
                foreach($commission as $row){
                    $total=woocommerce_price( $row->total_due + $row->total_shipping + $row->tax );
                    echo $total;
                }
                break;

            case 'system_commission' :
                $order = wc_get_order( $post->ID );
                $product_price = $order->get_total();
                $commission = $wpdb->get_results( "
								SELECT * FROM {$wpdb->prefix}pv_commission
								WHERE     order_id = '" . $post->ID . "'
								ORDER BY time DESC
							" );
                foreach($commission as $row){
                    $total=woocommerce_price( $row->total_due + $row->total_shipping + $row->tax );
                    echo  wc_price($product_price - doubleval(strip_tags($total)) );
                }
                break;

        }
    }





    public function wcv_order_table_columns($columns)
    {
        $columns['total_without_commissions'] = 'Total without commissions';
        $columns['affiliate_commission'] = 'Affiliate Commission';
        $columns['system_commission'] = 'System Commission';
        return $columns;
    }

    public function wcv_orders_table_rows($rows)
    {
        global $woocommerce, $post;
        global $wpdb;
        foreach($rows as &$row) {
            $order = new WC_Order($row->ID);
            $total = $order->get_total();
            $row->total_without_commissions = wc_price($total);
            $row->system_commission = wc_price($total - doubleval(strip_tags($row->total)) );
            $user_id = get_current_user_id();
            if($wpdb->get_var("SELECT affiliate_id FROM {$wpdb->prefix}affiliate_wp_referrals WHERE reference={$row->ID}") ){
                $rate = abs(doubleval(get_user_meta( $user_id, 'pv_shop_affiliates_rate', true )));
                $row->affiliate_commission = wc_price($total * ($rate / 100));
            }
        }
        return $rows;
    }


    public function get_affilate_rate($product_id){
        $_pf = new WC_Product_Factory();
        $product = $_pf->get_product($product_id);
        $user_id = $product->post->post_author;
        $rate = abs(doubleval(get_user_meta( $user_id, 'pv_shop_affiliates_rate', true )));
        if($rate > 100) {
            $rate = 100;
        }
        return $rate;
    }

    public function pull_affiliate_commission_from_vendor($commission, $product_id, $product_price, $order, $qty)
    {
        global $wpdb;
        if($wpdb->get_var("SELECT affiliate_id FROM {$wpdb->prefix}affiliate_wp_referrals WHERE reference={$order->id}") ) {

            $rate = $this->get_affilate_rate($product_id);
            $commission -= $product_price * ($rate / 100);
            $commission      = round( $commission, 2 );
        }
        return $commission;
    }

    public function get_affiliate_rate($referral_amount, $affiliate_id, $amount, $reference, $product_id)
    {
        $rate = $this->get_affilate_rate($product_id);
        //$type = affwp_get_affiliate_rate_type( $affiliate_id );
        $decimals = affwp_get_decimal_count();

        $referral_amount = round( $amount * $rate / 100, $decimals );
        if ( $referral_amount < 0 ){
            $referral_amount = 0;
        }
        return $referral_amount;
    }


    public static function install()
    {
        global $wpdb;
        $table_name = self::getTableName();
        $sql = "CREATE TABLE IF NOT EXISTS $table_name (
            id bigint NOT NULL AUTO_INCREMENT,
            vendor_id bigint NOT NULL,
            affiliate_id bigint(20) NOT NULL,
            added_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
            PRIMARY KEY (id)
	    )";
        $wpdb->query($sql);
    }

    public static function uninstall()
    {
        global $wpdb;
        $sql = "DROP TABLE IF EXISTS ".self::getTableName();
        $wpdb->query($sql);
    }


    public function add_affiliates_rate_input()
    {
        if ( !WCV_Vendor_Dashboard::can_view_vendor_page() ) {
            return false;
        }

        $user_id = get_current_user_id();

        wc_get_template( 'settings.php', array(
            'user_id'          => $user_id,
        ), 'wc-vendors/dashboard/settings/', wcv_a_plugin_dir . 'templates/settings/' );

    }

    public function save_affiliates_rate_settings($user_id) {
        //var_dump($user_id); die;
        if ( isset( $_POST[ 'pv_shop_affiliates_rate' ] ) ) {
            $rate = abs(doubleval($_POST[ 'pv_shop_affiliates_rate' ]));
            if($rate && $rate > 100) {
                $rate = 100;
            }

            update_user_meta( $user_id, 'pv_shop_affiliates_rate', $rate );
        }
    }

    public function is_vendor_affiliate($vendor_id){
        global $wpdb;
        $affiliate_id = affwp_get_affiliate_id();
        if($affiliate_id) {
            $user_count = $wpdb->get_var( "SELECT COUNT(*) FROM ".self::getTableName()." WHERE vendor_id=".$vendor_id." and affiliate_id=".$affiliate_id." " );
            //$sql = "SELECT *FROM ".self::getTableName()." WHERE vendor_id=".$vendor_id." and affiliate_id=".$affiliate_id." ";
            // $result=$wpdb->get_results($sql);
            if($user_count) {
                return true;
            }
        }
        return false;
    }







    public function generate_become_affiliate_button($vendor_id)
    {
        if($this->is_vendor_affiliate($vendor_id)) {
            global $wp;
            $current_url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
            echo '<div class="container affiliate-link">
                  <a href="#" id="affiliate-referral-link" data-toggle="tooltip" title="You will receive '.abs(doubleval(get_user_meta( $vendor_id, 'pv_shop_affiliates_rate', true ))).'% from your referral payments">
                  Your referral link '.affwp_get_affiliate_referral_url( array( 'base_url' => $current_url )).'
                  </a>
                  </div>';


        } else {
            wc_get_template( 'become-affiliate.php', array(
                'vendor_id'          => $vendor_id,
            ), 'wc-vendors/dashboard/settings/', wcv_a_plugin_dir . 'templates/settings/' );
        }
    }

    public function add_user_as_affiliate_to_vendor($user_id, $vendor_id) {
        global $wpdb;
        $affiliate_id = affwp_get_affiliate_id();
        if(!$affiliate_id) {
            $affiliate_id = affwp_add_affiliate( array( 'user_id' => $user_id ) );
        }
        $wpdb->insert(
            self::getTableName(),
            array(
                'vendor_id' => $vendor_id,
                'affiliate_id' => $affiliate_id,
            )
        );
    }

    public function submit_become_affiliate_button() {
        if ( isset( $_POST[ 'pv_become_vendor_id' ] ) ) {
            $vendor_id = intval($_POST[ 'pv_become_vendor_id' ]);
            $user_id = get_current_user_id();
            if(!WCV_Vendors::is_vendor( $vendor_id ) || $vendor_id == $user_id){
                die('Wrong Vendor');
            }

            if($this->is_vendor_affiliate($vendor_id)) {
                return wp_redirect($_SERVER['HTTP_REFERER'].'?store=recent');
            }

            if($user_id) {
                $this->add_user_as_affiliate_to_vendor($user_id, $vendor_id);
            } else {
                global $wp;
                return wp_redirect(site_url('affiliate-area?vendor='.$vendor_id.'&return=http://'."$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"));
            }
            wp_redirect($_SERVER['HTTP_REFERER'].'?store=recent');
        }
    }


    public function add_registered_affiliate_to_vendor($affiliate_id, $status, $args){

        if(isset($_GET['vendor']) && $vendor_id = abs(intval($_GET['vendor']))) {
            if(!$this->is_vendor_affiliate($vendor_id)){
                $this->add_user_as_affiliate_to_vendor(get_current_user_id(), $vendor_id);
                if(isset($_GET['return'])) {
                    return wp_redirect($_GET['return']);
                }
            }
        }
    }


}

new WC_Vendors_Affiliates();