<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://www.doityourselfnation.org
 * @since      1.0.0
 *
 * @package    Dyn_Review
 * @subpackage Dyn_Review/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Dyn_Review
 * @subpackage Dyn_Review/public
 * @author     papul <28papul@gmail.com>
 */
class Dyn_Review_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Display single value or all categories for rating
	 *
	 * @since 	1.0.0
	 * @access 	private
	 * @var 	string 	$single 
	 */
	private $single;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->single = true;
		require_once dirname(__FILE__) . '/../includes/class-dyn-review-db.php';

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Dyn_Review_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Dyn_Review_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/dyn-review-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Dyn_Review_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Dyn_Review_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/dyn-review-public.js', array( 'jquery' ), $this->version, false );
		wp_localize_script( $this->plugin_name, 'ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
	}

	public function submit_review(){
		$db = new Dyn_Review_DB();
		$permission = get_option($this->plugin_name . "_permission");
		if( $permission == 'only' ){
			if( !is_user_logged_in() )
				return;
		}
		$author_id = get_post_field( 'post_author', $review_data['post_id'] );
		if( $author_id == get_current_user_id() )
			return;

		$review_data = $_POST['review_data'];
		$review_data['review_status'] = "published";
		if($review_data['user_id'] != 0){
			$review_data['user_name'] = "";
		}
		if($this->single){
			$review_data['ratings'] = array();
			$categories = $db->get_categories();
			foreach($categories as $category){
				$review_data['ratings'][] = array(
					'cat_name' => $category->name,
					'rating' => $review_data['rating']
				);
			}
		}
		$db->save_review($review_data);
		wp_die();
	}

	public function add_review_button($output, $post_id, $type){
		$db = new Dyn_Review_DB();
		$permission = get_option($this->plugin_name . "_permission");
		if( $permission == 'only' ){
			if( !is_user_logged_in() )
				return;
		}
		if( is_user_logged_in() ) {
			if( $type == 'post' ){
				$post = get_post( $post_id );
				$profile_id = $post->post_author;
			}
			else{	
				$file = $db->get_file($post_id);
				$profile_id = $file->user_id;
			}
			if( $profile_id == get_current_user_id() ){
				return;
			}
			if($db->user_already_reviewed($post_id, $type, get_current_user_id()) ){
				return;
			}
		}

		$output .= '<div class="add_dyn_review review_button_wrapper '.$type.' custom_btn">';
		$output .= '<span>Review</span>';
		$output .= '</div>';

		$categories = $db->get_categories();
		$output .= '<div class="review_box" data-id="' . $post_id . '" data-type="' . $type . '" style="display:none;">';
		$output .= '<div class="review_error alert alert-danger" style="display:none;"></div>';
		if(is_user_logged_in()){
			$user_id = get_current_user_id();
		}
		else{
			$user_id = 0;
		}
		$output .= '<div data-user_id="' . $user_id . '" class="review_user_info">';
		if($user_id == 0){
			$output .= '<div class="form-group">';
			$output .= '<label for="review_user_name">Enter your name:</label>';
			$output .= '<input type="text" name="review_user_name" class="review_user_name form-control">';
			$output .= '</div>';
		}
		$output .= '</div>';
		
		ob_start();
		if(!$this->single):
			$count = 1;
			foreach($categories as $category):
			?>
				<div class="review_category">
					<div class="category_name">
						<span><?php echo $category->name; ?></span>
					</div>

					<div class="category_rating">
						<div data-rating="0" data-category="<?php echo $category->name; ?>" class='dyn_rating_bar'>
							<div class='dyn_rating' style='width:0%'>
							</div>
						</div>
					</div>
				</div>
			<?php
				$count++;
			endforeach;
		else:
		?>
			<div class="review_single">
				<div data-rating="0"  class='dyn_rating_bar'>
					<div class='dyn_rating' style='width:0%'>
					</div>
				</div>
			</div>
		<?php
		endif;
		$output .= ob_get_contents();
		ob_end_clean();
		$output .= '<div class="form-group">';
		$output .= '<label for="review">Enter Review</label>';
		$output .= '<textarea name="review" class="review_text form-control"></textarea>';
		$output .= '</div>';
		$output .= '<div class="review_submit btn btn-success">Submit</div>';
		$output .= '<div class="review_close btn btn-danger">Close</div>';
		$output .= '</div>';
		return $output;
	}

	public function display_review($output, $post_id, $rating_type){
		
		$db = new Dyn_Review_DB();

		$rating = $db->get_rating_for_post($post_id, $rating_type);
		$output = "<div class='dyn_rating_bar dyn_open_review_box' data-post_id='{$post_id}' data-rating_type='{$rating_type}'>";
		$output .= "<div class='dyn_rating' style='width:" . $rating . "%'>";
		$output .= "</div>";
		$output .= "</div>";

		$per_page = 5;
		$offset = 0;
		$sort = "recent";
		$next_offset = $offset + $per_page;

		$reviews = $db->get_reviews($post_id, $rating_type, $sort, $per_page, $offset);
		$total_reviews = $db->get_review_count($post_id, $rating_type);

		$output .= '<div class="reviews review_box dyn_all_reviews class-dyn-revew-public.php" style="display:none;">';
		$output .= $this->display_review_box($reviews);
		
		if($total_reviews > $per_page) {
			$output .= '<div class="btn btn-info dyn_load_more" data-offset="' . $next_offset . '" data-post_id="' . $post_id . '" data-rating_type="' . $rating_type . '">';
			$output .= 'Load More';
			$output .= '</div>';
		}
		$output .= '<div class="btn btn-danger dyn_close_review_box">';
		$output .= 'Close';
		$output .= '</div>';
		$output .= '</div>';
		$output .= "<span>(clickable)</span>";
		return $output;
	}

	public function load_more_reviews() {
		$db = new Dyn_Review_DB();

		$offset = (int)$_POST['offset'];
		$post_id = (int)$_POST['post_id'];
		$rating_type = $_POST['rating_type'];
		$total_reviews = $db->get_review_count($post_id, $rating_type);
		$per_page = 5;
		$sort = "recent";
		$output = array();
		$next_offset = $offset + $per_page;

		$reviews = $db->get_reviews($post_id, $rating_type, $sort, $per_page, $offset);
		if($total_reviews > $next_offset){
			$output['have_more'] = "true";
			$output['offset'] = $next_offset;
		}
		else{
			$output['have_more'] = "false";
		}

		$output['reviews'] = $this->display_review_box($reviews);

		echo json_encode($output);
		wp_die();
	}

	private function display_review_box($reviews) {
		$db = new Dyn_Review_DB();
		$permission = get_option($this->plugin_name . "_permission");
		ob_start();
		?>
		
		<?php
		foreach($reviews as $review):
		?>
			<div class="dyn_review" 
					data-review_id ="<?php echo $review->review_id; ?>" 
					data-review_type="<?php echo $review->rating_type; ?>">
				<div class="review_file">
					<?php echo $this->get_post_details($review); ?>
				</div>
				<?php if($this->single): ?>
					<div class="ratings">
						<div class='dyn_rating_bar'>
							<div class='dyn_rating' style='width:<?php echo $review->total_rating; ?>%'>
							</div>
						</div>
					</div>
				<?php else: ?>
				<?php $ratings = $db->get_ratings($review->review_id); ?>
					<div class="ratings">
					<?php foreach($ratings as $rating): ?>
						<div class="review_field">
							<div class="review_title">
								<?php echo $rating->name; ?>
							</div>

							<div class="review_value">
								<div class='dyn_rating_bar'>
									<div class='dyn_rating' style='width:<?php echo $rating->rating; ?>%'>
									</div>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
					</div>
				<?php endif; ?>

				<?php 
				if( $review->user_id == 0){
					$author = $review->user_name;
				} else {
					$userdata = get_userdata( $review->user_id );
					$author = '<a href="' . get_author_posts_url( (int)$review->user_id ) . '">' . $userdata->user_nicename . '</a>';
				}

				?>
				<div class="review_author">
					<span class="author_left_text">
						By:
					</span>
					<span class="author_name">
						<?php echo $author; ?>
					</span>

					<span class="review_date">
						on <?php echo date('F j, Y', strtotime($review->date) ); ?>
					</span>
				</div>
				<?php
					$positive_count = $db->count_positive_helpful($review->review_id);
					$negative_count = $db->count_negative_helpful($review->review_id);
					$disabled = '';
					if( $permission == 'only' ){
						if( !is_user_logged_in() )
							$disabled = ' disabled';
					}
					if( is_user_logged_in() ){
						$id = $db->review_helpful_exist($review->review_id, get_current_user_id());
						if($id)
							$disabled = ' disabled';
						$author_id = get_post_field( 'post_author', $review->post_id );
						if( $author_id == get_current_user_id() )
							$disabled = ' disabled';
					}
				?>
				<div class="review_helpful">
					<span class="helpful_text">
						Was this helpful?
					</span>

					<button type="button" class="btn btn-success dyn_review_positive"<?php echo $disabled; ?>>
						<i class="fa fa-thumbs-up fa-lg"></i>
						<span class="helpful-count" data-count="<?php echo $positive_count; ?>">(<?php echo $positive_count; ?>)</span>
					</button>

					<button type="button" class="btn btn-danger dyn_review_negative"<?php echo $disabled; ?>>
						<i class="fa fa-thumbs-down fa-lg"></i>
						<span class="helpful-count" data-count="<?php echo $negative_count; ?>">(<?php echo $negative_count; ?>)</span>
					</button>
				</div>
			</div>
		<?php endforeach; ?>
			
		<?php
		$output = ob_get_contents();
		ob_end_clean();
		return $output;
	}

	public function get_post_details($review){
		$db = new Dyn_Review_DB();
		if( $review->rating_type == 1) {
			return "<a href='" . get_the_permalink($review->post_id) . "'>" . get_the_title($review->post_id) . "</a>";
		}
		else{
			$file = $db->get_file($review->post_id);
			$link = $upload_dir['url'].'/files/'.$file->file;
			return ("<a href='$link'>" . $file->file_name . "." . $file->file_type . "</a>");
		}
	}

	public function review_query_var($qvars) {
		$qvars[] = 'review_paged';
		$qvars[] = 'display_page';
		$qvars[] = 'review_sort_type';

		return $qvars;
	}

	public function review_tab($output, $profile_id, $rating_type) {
		ob_start();
		$sort_array = array(
			'recent' => 'Recent',
			'top_rated' => 'Top Rated'
		);
		$sort = "recent";
		$sort_query = get_query_var('review_sort_type');
		
		if( isset($_POST['review_sort_type'] ) ){
			if( $_POST['review_sort_type'] == 'recent' )
				$sort = "recent";
			else
				$sort = "top_rated";
		}
		elseif( $sort_query ) {
			if( $sort_query == 'recent' )
				$sort = "recent";
			else
				$sort = "top_rated";
		}
		$db = new Dyn_Review_DB();
		
		$total_review = $db->get_rating_count($profile_id);
		$per_page = 5;
		$num_of_page = ceil($total_review/$per_page);
		$page_num = max( 1, get_query_var('review_paged') );
		$offset = $per_page * ($page_num - 1);

		$reviews = $db->get_sort_reviews($profile_id, $sort, $per_page, $offset);
		$url = add_query_arg('display_page', 'review');
		$url = add_query_arg('review_sort_type', $sort, $url);

		?>
		<div class="reviews <?php echo $rating_type; ?>">
			<div class="review_sort">
				<form class="review_sort_form" method="post">
					<div class="form-group">
						<label for="review_sort_type">Sort By</label>
						<select class="review_sort_type form-control" name="review_sort_type">
							<?php foreach($sort_array as $key => $value): ?>
								<option value="<?php echo $key; ?>"<?php echo selected($key, $sort); ?>><?php echo $value; ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</form>
			</div>
		<?php
			echo paginate_links( array(
				'base' => add_query_arg('review_paged', '%#%', $url),
				'format' => '',
				'current' => $page_num,
				'total' => $num_of_page
			) );
		echo $this->display_review_box($reviews); ?>
		</div>
		<?php
		echo paginate_links( array(
			'base' => add_query_arg('review_paged', '%#%', $url),
			'format' => '',
			'current' => $page_num,
			'total' => $num_of_page,
		) );
		$output = ob_get_contents();
		ob_end_clean();
		return $output;
	}

	public function number_of_post_review($post_id, $rating_type){

		$db = new Dyn_Review_DB();

		$output = $db->number_of_post_review($post_id, $rating_type);
		
		if($output == 1){
			$output .= " Review";
		}
		else{
			$output .= " Reviews";
		}

		return $output;
	}

	public function channel_home_content($profile_id) {
		$db = new Dyn_Review_DB();
		
		//$total_review = $db->get_rating_count($profile_id);

		$sort = "recent";
		$all_reviews = $db->get_sort_reviews($profile_id, $sort, 99999999, $offset);
		$count = 0;
		$sum = 0;
		foreach($all_reviews as $review){
			$sum += $review->total_rating;
			$count++;
		}

		if($count == 0)
			$rating = 0;
		else
			$rating = ceil($sum/$count);

		$logo_url = "http://www.doityourselfnation.org/bit_bucket/wp-content/uploads/DYN-logo-white-small.png";
		?>
		<div class="col-md-3">
			<p>Total Reviews</p>
			<div class="icon-inner">
				<?php //echo "<img style='width:50%; background:#3498db;' src='$logo_url'>"; ?>
				<!--<span style="font-weight:bold; font-size:17px;">Rating</span>-->
				<div class='dyn_rating_bar center-rating'>
					<div class='dyn_rating' style='width:<?php echo $rating; ?>%'>
					</div>
				</div>
				<div class="dyn_rating_summary">
					<p><?php echo number_format(($rating/20), 1); ?> out of 5</p>
				</div>
				<div>
					<p><?php echo $count; ?></p>
				</div>
			</div>
		</div>
		<?php
	}

	public function save_review_helpful(){
		$db = new Dyn_Review_DB();
		$permission = get_option($this->plugin_name . "_permission");
		$review_id = $_POST['review_id'];
		$helpful = $_POST['helpful'];
		$user_id = get_current_user_id();
		$output = array();
		if( $permission == 'only' ){
			if( !is_user_logged_in() ){
				$output['message'] = 'You need to login to review this';
				$output['status'] = 'false';
			}
		}
		if( is_user_logged_in() ){
			$id = $db->review_helpful_exist($review_id, get_current_user_id());
			if($id){
				$output['message'] = 'You have already reviewed this';
				$output['status'] = 'false';
			}
			$author_id = get_post_field( 'post_author', $db->get_post_id_from_review_id($review_id) );
			if( $author_id == get_current_user_id() )
				return;
		}

		if(empty($output)){
			$id = $db->save_review_helpful($review_id, $user_id, $helpful);
			$output['id'] = $id;
			if( $id ){
				$output['status'] = 'success';
			}else{
				$output['status'] = 'false';
			}
		}
		echo json_encode($output);
		wp_die();
	}

	public function single_last_review($post_id, $rating_type){
		include_once dirname(__FILE__) . '/../includes/class-dyn-review-helper.php';
		$db = new Dyn_Review_DB();
		$reviews = $db->get_reviews($post_id, $rating_type, 'recent', 1, 0);
		ob_start();
		foreach ($reviews as $review): ?>
			<div class="dyn_single_review">
				<div class="dyn_author_pic pull-left">
					<?php echo get_avatar( $review->user_id); ?>
				</div>

				<div class="dyn_single_right pull-left">
					<div class="dyn_author_name">
						<?php echo $review->user_name; ?>
					</div>
					<div class='dyn_rating_bar'>
						<div class='dyn_rating' style='width:<?php echo $review->total_rating; ?>%'>
						</div>
					</div>
					<div class="dyn_review_time">
						<?php echo DYN_REVIEW_HELPER::humanTiming(strtotime($review->date)) . ' ago'; ?>
					</div>
					<div class="dyn_review_text">
						<?php echo $review->review; ?>
					</div>
				</div>
			</div>
		<?php
		endforeach;
		$output = ob_get_contents();
		ob_end_clean();
		return $output;
	}

	public function dyn_review_display(){
		ob_start();
		echo "abcd";
		$output = ob_get_contents();
		ob_end_clean();
		return $output;
	}

}
