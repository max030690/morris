<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://www.doityourselfnation.org
 * @since      1.0.0
 *
 * @package    Dyn_Review
 * @subpackage Dyn_Review/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Dyn_Review
 * @subpackage Dyn_Review/admin
 * @author     papul <28papul@gmail.com>
 */
class Dyn_Review_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Dyn_Review_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Dyn_Review_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/dyn-review-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Dyn_Review_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Dyn_Review_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/dyn-review-admin.js', array( 'jquery' ), $this->version, false );

	}

	public function add_plugin_page(){
		add_menu_page(
			'Dyn Review',
			'Dyn Review',
			'manage_options',
			$this->plugin_name,
			array($this, 'menu_page',)
		);
		add_submenu_page(
			$this->plugin_name,
			'Reviews',
			'Reviews',
			'manage_options',
			'dyn-all-reviews',
			array($this, 'all_reviews')
		);
	}

	public function all_reviews() {
		include_once 'partials/display-all-reviews.php';
	}

	public function menu_page(){
		include_once 'partials/dyn-review-admin-display.php';
	}

	public function register_settings(){
		//Add setting section
		add_settings_section(
			$this->plugin_name . '_general',
			__('General', 'dyn-review'),
			array($this, 'general_cb'),
			$this->plugin_name
		);

		//Add Settings field
		add_settings_field(
			$this->plugin_name . '_rating_type',
			__( 'Rating Type', 'dyn-review' ),
			array( $this,  'rating_type_cb' ),
			$this->plugin_name,
			$this->plugin_name. '_general',
			array( 'label_for' => $this->plugin_name . '_rating_type' )
		);

		add_settings_field(
			$this->plugin_name . '_permission',
			__( 'Who can Review', 'dyn-review' ),
			array( $this,  'permission_cb' ),
			$this->plugin_name,
			$this->plugin_name. '_general',
			array( 'label_for' => $this->plugin_name . '_permission' )
		);

		add_settings_field(
			$this->plugin_name . '_cat',
			__( 'Review Category', 'dyn-review' ),
			array( $this,  'cat_cb' ),
			$this->plugin_name,
			$this->plugin_name. '_general',
			array( 'label_for' => $this->plugin_name . '_cat' )
		);

		//register settings
		register_setting( $this->plugin_name, $this->plugin_name . '_rating_type', array( $this, 'sanitize_rating_type' ) );
		register_setting( $this->plugin_name, $this->plugin_name . '_permission', array( $this, 'sanitize_permission' ) );
	}

	public function general_cb(){
		echo '<p>' . __( 'Please select all the options.', 'dyn-review' ) . '</p>';
	}

	public function cat_cb(){
		?>
			<code>Visaul Quality</code>, <code>Easy to Follow</code>, 
			<code>Duplication</code>, <code>Content Quality</code>,
			<code>Description</code>
		<?php
	}

	public function rating_type_cb(){
		$option = get_option($this->plugin_name . '_rating_type');
		$types = array(
			'five' => '5 Star',
			'hundred' => 'percentage'
		);
		?>
			<select name="<?php echo $this->plugin_name; ?>_rating_type">
			<?php foreach( $types as $key => $value ): ?>
				<?php $css = ($key==$option) ? ' selected="selected"' : ''; ?>
				<option value="<?php echo $key;?>"<?php echo $css; ?>><?php echo $value; ?></option>
			<?php endforeach; ?>
			</select>
		<?php
	}

	public function sanitize_rating_type($option){
		return $option;
	}

	public function permission_cb(){
		$option = get_option($this->plugin_name . '_permission');
		$types = array(
			'all' => 'Everyone',
			'only' => 'Only Registered Users'
		);
		?>
			<select name="<?php echo $this->plugin_name; ?>_permission">
			<?php foreach( $types as $key => $value ): ?>
				<?php $css = ($key==$option) ? ' selected="selected"' : ''; ?>
				<option value="<?php echo $key;?>"<?php echo $css; ?>><?php echo $value; ?></option>
			<?php endforeach; ?>
			</select>
		<?php
	}

	public function sanitize_permission($option){
		return $option;
	}


}
