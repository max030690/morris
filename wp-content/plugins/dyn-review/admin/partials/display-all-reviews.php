<?php
/**
 * Display all Reviews in the admin panel
 */
require_once dirname(__FILE__) . '/../class-review-list-table.php';
$review_list_table = new Review_List_Table();
$review_list_table->prepare_items();
?>

<div class="wrap">
	<h2>All Reviews</h2>
	<form method="post">
		<?php $review_list_table->display(); ?>
	</form>
</div>