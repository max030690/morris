<?php if($item->misc['error'] && !isset($_POST['suppress_errors'])) { ?>
<table align="center" border="0" style="margin-top:30px;">
	<tr>
		<td align="left">
			<ul style="margin:0; padding:0;">
			<?php echo $item->misc['info']; ?>
			</ul>
		</td>
	</tr>
</table>
<?php } ?>
 <form name="type" id="type" action="<?php echo $oiopub_set->request_uri; ?>" method="get">
	<input type="hidden" name="do" value="video" />

</form>
<?php if($item->item_type > 0) { ?>
<form name="process" id="process" action="<?php echo $oiopub_set->request_uri; ?>" method="post" enctype="multipart/form-data">
<input type="hidden" name="process" value="yes" />
<input type="hidden" name="oio_type" value="<?php echo $item->item_type; ?>" />
<table align="center" width="550" class="start" border="0" cellspacing="4" cellpadding="4">
	<tr>
		<td width="120"><b><?php echo __oio("Your Name"); ?>:</b></td>
		<td><input tabindex="2" type="text" name="oio_name" size="40" value="<?php echo $item->adv_name; ?>" />&nbsp;[<a href="javascript://" title="<?php echo __oio("Enter your full name"); ?>"><b>?</b></a>]</td>
	</tr>
	<tr>
		<td><b><?php echo __oio("Email Address"); ?>:</b></td>
		<td><input tabindex="3" type="text" name="oio_email" size="40" value="<?php echo $item->adv_email; ?>" />&nbsp;[<a href="javascript://" title="<?php echo __oio("Enter your email address"); ?>"><b>?</b></a>]</td>
	</tr>
	<tr>
		<td colspan="2" height="20"></td>
	</tr>
	<tr>
		<td><b><?php echo __oio("Payment"); ?>:</b></td>
		<?php if($item->payment_status == 1) { ?>
		<td><?php echo $item->payment_processor; ?></td>
		<?php } else { ?>
		<td><?php echo oiopub_dropmenu_kv($oiopub_set->arr_payment, "oio_paymethod", $item->payment_processor, 200, "add_field(\"process\", \"hidden\", \"suppress_errors\", \"suppress\", \"1\"); document.process.submit();", 0, 4); ?>&nbsp;[<a href="javascript://" title="<?php echo __oio("Select your preferred payment method"); ?>"><b>?</b></a>]</td>
		<?php } ?>
	</tr>
	<tr>









	</tr>
<?php if($oiopub_set->general_set['subscription'] == 1 && $oiopub_set->{$bz}['model'] == 'days' && $item->payment_status != 1) { ?>
	<tr>
		<td><b><?php echo __oio("Subscription"); ?>:</b></td>
		<td><?php echo oiopub_dropmenu_kv($oiopub_set->arr_yesno, "oio_subscription", $item->item_subscription, 200, "", 0, 6); ?>&nbsp;[<a href="javascript://" title="<?php echo __oio("If subscribing, your purchase will be automatically renewed"); ?>"><b>?</b></a>]</td>
	</tr>
<?php } ?>
<?php if(!empty($oiopub_set->{$bz}['link_exchange']) && is_numeric($item->payment_amount) && $item->payment_amount == 0) { ?>
	<tr>
		<td colspan="2" height="20"></td>
	</tr>
	<tr>
		<td colspan="2" align="justify"><?php echo __oio("To participate, place a link to 'my link' on your website. Then enter the URL of the page you placed it on, in the 'your page' section"); ?>:</td>
	</tr>
	<tr>
		<td colspan="2" height="20"></td>
	</tr>
	<tr>
		<td><b><?php echo __oio("My Link"); ?>:</b></td>
		<td><?php echo $oiopub_set->{$bz}['link_exchange']; ?></td>
	</tr>
	<tr>
		<td><b><?php echo __oio("Your Page"); ?>:</b></td>
		<td><input tabindex="7" type="text" name="oio_exchange" size="40" value="<?php echo $item->link_exchange; ?>" />&nbsp;[<a href="javascript://" title="<?php echo __oio("Enter the URL of the page on your website where you placed 'my link'"); ?>"><b>?</b></a>]</td>
	</tr>
	<tr>
		<td colspan="2" height="20"></td>
	</tr>
	<tr>
		<td colspan="2" align="justify"><?php echo __oio("Now enter details of the ad you'd like to exchange below"); ?>:</td>
	</tr>
<?php } ?>
	<tr>
		<td colspan="2" height="20"></td>
	</tr>
<?php if($oiopub_set->general_set['upload'] == 1) { ?>
	<tr>
		<td><b><?php echo __oio("Video Upload"); ?>:</b></td>
		<td>
			<div id="input-upload-file" class="box-shadow">
			<input tabindex="8" type="file" name="oio_url"  class="upload" id="fileUp"   onchange="setFileInfo(this.files)" size="40" />[<a href="javascript://" title="<?php echo __oio("The banner image file to upload"); ?>"><b>?</b></a>]
			</div>


			<input type="hidden" name="videoduration" id="videoduration">
			<script type="text/javascript">
				var 	duration ;
				var myVideos = [];
				window.URL = window.URL || window.webkitURL;
				function setFileInfo(files) {
					myVideos.push(files[0]);
					var video = document.createElement('video');
					video.preload = 'metadata';
					video.onloadedmetadata = function() {
						window.URL.revokeObjectURL(this.src)
						var duration = video.duration;
						myVideos[myVideos.length-1].duration = duration;
						document.getElementById('videoduration').value = duration;
						newdur = document.getElementById('videoduration').value;

					}
					video.src = URL.createObjectURL(files[0]);;
				}

			</script>






</tr><tr><input type="hidden" name="videosssss" value="okk">



		<br><td><b><?php echo __oio("Views"); ?>:</b></td>
		<?php if($item->payment_status == 1) { ?>
		<td><?php echo $item->payment_amount . " " . $item->payment_currency; ?> (<?php echo $item->item_duration > 0 ? $item->item_duration . " " . "Views": "permanent"; ?>)</td>
		<?php } else { ?>
		<td><?php echo "<input type='text' name='price' id='pprice'> "; ?>&nbsp;[<a href="javascript://" title="<?php echo __oio("Select your preferred price"); ?>"><b>?</b></a>]<div id="price"></div></td>
		<input type="hidden" name="dur" id="dur" >
		<input type="hidden" name="views" id="views" >
			<script type="text/javascript">

			function price(){
			dur = 	document.getElementById('videoduration').value;
			views = document.getElementById('pprice').value;
			newpr = 1;
				if(views !=='') {views = parseInt(views);

				if(views <100) {document.getElementById('price').innerHTML = '<span style="color:red"><b style="color:red">Min value of Views must be 100 </b></span>';


				$('.oiopaymentbutton').attr('disabled','disabled');
				} else {document.getElementById('price').innerHTML = '<span></span>';
					    $(".oiopaymentbutton").removeAttr("disabled");


					    newpr =  dur * 0.01 * views;

				newpr = newpr.toFixed(2);

				document.getElementById('price').innerHTML = 'Second : '+dur + ' Price :' +newpr ;
				document.getElementById('dur').value = dur;
				document.getElementById('views').value =views;
			}



					    }

				}

	setInterval(price, 300);
 		</script>












		<?php } ?>
















			<?php if($item->item_status != 0) { ?>
			<br />
			<a href="<?php echo $item->item_url; ?>" target="_blank"><?php echo __oio("View existing banner"); ?></a>
			<?php } ?>
		</td>
	</tr>

<?php } else { ?>
	<tr>
		<td><b><?php echo __oio("Banner URL"); ?>:</b></td>
		<td><input tabindex="9" type="text" name="oio_url" size="40" value="<?php echo $item->item_url; ?>" />&nbsp;[<a href="javascript://" title="<?php echo __oio("The URL of the banner image to display"); ?>"><b>?</b></a>]</td>
	</tr>
<?php } ?>
	 <tr>
		<td><b><?php echo __oio("Website URL"); ?>:</b></td>
		<td><input tabindex="10" type="text" name="oio_page" size="40" value="<?php echo $item->item_page; ?>" />&nbsp;[<a href="javascript://" title="<?php echo __oio("The target website your ad will point to"); ?>"><b>?</b></a>]</td>
	</tr>
	 
<?php if($oiopub_set->{$bz}['cats'] == 1 && function_exists('oiopub_category_list')) { ?>
	<tr>
		<td><b><?php echo __oio("Category"); ?>:</b></td>
		<td><?php echo oiopub_dropmenu_kv(oiopub_category_list(), "oio_category", $item->category_id, 200, "", 0, 12); ?>&nbsp;[<a href="javascript://" title="<?php echo __oio("Select the category which best fits your ad"); ?>"><b>?</b></a>]</td>
	</tr>
<?php } ?>
<?php if($oiopub_set->{$bz}['nofollow'] == 2 && $item->payment_status != 1) { ?>
	<tr>
		<td><b><?php echo __oio("Use Nofollow"); ?>:</b></td>
		<td><?php echo oiopub_dropmenu_kv($oiopub_set->arr_yesno, "oio_nofollow", $item->item_nofollow, 90, "", 0, 13); ?>&nbsp;[<a href="javascript://" title="<?php echo __oio("Select whether to use the nofollow attribute"); ?>"><b>?</b></a>]</td>
	</tr>
	<tr>
		<td></td>
		<td>(<?php echo __oio("%s added to price if nofollow tag not used", array($oiopub_set->{$bz}['nfboost'] . "%")); ?>)</td>
	</tr>
<?php } ?>
	<tr>
		<td colspan="2" height="20"></td>
	</tr>
<?php if($oiopub_set->general_set['security_question'] == 1) { ?>
	<tr>
		<td><b><?php echo __oio("Security"); ?>:</b></td>
		<td><?php echo $item->captcha['question'] ?>&nbsp;&nbsp;<input tabindex="14" type="text" name="oio_security" size="10" />&nbsp;&nbsp;[<a href="javascript://" title="<?php echo __oio("The security question is used to stop spam bots"); ?>"><b>?</b></a>]</td>
	</tr>
	<tr>
		<td colspan="2" height="10"></td>
	</tr>
	<tr>
		<td><b><?php echo __oio("Countries"); ?>:</b></td>
		<td><br>
			<select name="country[]" id="multiselect" multiple>
				<?php foreach(oiopub_settings::$countries as $key => $country) { ?>
					<option value="<?php echo $key ?>"><?php echo $country ?></option>
				<?php } ?>
			</select><br><br>
		</td>
	</tr>
	<tr>
		<td><b>Tags :</b></td>
		<td>
			<div class="tags well">
				<div class="tagging-js" data-tags-input-name="taggone"></div> <span style=" float: right;  margin-top: -54px; margin-right: -23px;">&nbsp;[<a href="javascript://" title="<?php echo __oio("Place tags on your advertisement so that video's, books, and files that have this tag will have your advertisement be shown on them."); ?>"><b>?</b></a>]</span>
			</div>
		</td>
	</tr>
	<tr>
		<td><b>Preventative Tags :</b></td>
		<td>
			<div class="tags well">
				<div class="tagging-js" data-tags-input-name="taggonea"></div> <span style=" float: right;  margin-top: -54px; margin-right: -23px;">&nbsp;[<a href="javascript://" title="<?php echo __oio("Place tags on your advertisement so that video's, books, and files that have this tag will have your advertisement not be shown on them regardless of what there other tags should be."); ?>"><b>?</b></a>]</span>
			</div>
		</td>
	</tr>
	<script  type="text/javascript">

	var tags = [];

		$('input[name="taggone[]"]').each(function () {
			tags.push($(this).val())
		})
		$('input[name="taggonea[]"]').each(function () {
			tags.push($(this).val())
		})

		function checkIfArrayIsUnique(myArray)
		{
			for (var i = 0; i < myArray.length; i++)
			{
				for (var j = 0; j < myArray.length; j++)
				{
					if (i != j)
					{
						if (myArray[i] == myArray[j])
						{
							alert('Dublicate tags : myArray[i]') // means there are duplicate values
							return false ;
						}
					}
				}
			}
			return true; // means there are no duplicate values.
		}



		if(jQuery('.oiopaymentbutton').click) {
			checkIfArrayIsUnique('tags[]');
		}





	</script>
<?php } ?>
	<tr>
		<td></td>
		<td><input tabindex="15"   type="submit" value="<?php echo __oio("Continue to Checkout"); ?>" class="oiopaymentbutton" /></td>
	</tr>
</table>
</form>
<?php } else { ?>
<table align="center" border="0" width="550" class="start">
	<tr>
		<td>
			<h3><?php echo __oio("Video Ad Pricing"); ?></h3>
			<table id="banner-chart" class="chart" width="100%" border="0" cellspacing="2" cellpadding="2">
				<tr style="background:#E0EEEE;"><td><a href="purchase.php?do=video&amp;zone=1">Video  Ads</a><br><i></i></td><td></td></tr>
			</table>
		</td>
	</tr>
</table>
<?php } ?>
<?php $oiopub_hook->fire('purchase_form_footer'); ?>
<script type="text/javascript">
	/*jQuery(function(){
		$("#multiselect").multiselect();
	});*/
	jQuery(document).ready(function(){
		jQuery('#multiselect').multiselect();
	})
</script>