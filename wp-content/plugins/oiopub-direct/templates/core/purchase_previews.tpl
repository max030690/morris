<script type="text/javascript">
<!--
function typechange(box) {
	<?php
	echo "	if(box.value == 4) {\n";
	echo "		showdiv('inline-link', 'block');\n";
	echo "		showdiv('inline-ad', 'none');\n";
	echo "	} else {\n";
	echo "		showdiv('inline-ad', 'block');\n";
	echo "		showdiv('inline-link', 'none');\n";	
	echo "	}\n";
	?>
}
function setChargingModel(el) {
	document.getElementById('chargingModel').innerHTML = el.value.charAt(0).toUpperCase() + el.value.slice(1);
}
//-->
</script>

<?php
if(!empty($message)) {
	echo "<br />\n";
	echo "<center>" . $message . "</center>\n";
	echo "<br /><br />\n";
}
?>
<form action="<?php echo $oiopub_set->request_uri; ?>" method="post" enctype="multipart/form-data">
<input type="hidden" name="csrf" value="<?php echo oiopub_csrf_token(); ?>" />
<input type="hidden" name="process" value="yes" />
<table align="center" class="start" width="550" cellspacing="4" cellpadding="4" style="margin-top:0px;">
	<tr>
		<td width="140"><b>Purchase Code:</b></td>
		<td><?php echo (empty($item->rand_id) ? "N/A" : $item->rand_id); ?></td>
	</tr>
	<tr>
		<td><b>Advertiser Name:</b></td>
		<td><?php echo $item->adv_name; ?></td>
	</tr>
	<tr>
		<td><b>Advertiser Email:</b></td>
		<td><?php echo $item->adv_email; ?></td>
	</tr>
	<tr>
		<td><b>Purchase Type:</b></td>
		<?php if($item_type == "video") { ?>
		<td><?php if($item->item_type == 1) echo "HTML5 (self-hosted)"; else echo $item->item_type; ?></td>
		<?php } else {?>
		<td><?php echo $adtype_array[$item->item_type]; ?></td>
		<?php } ?>
		 
	</tr>
	<tr>
		<td><b>Purchase Status:</b></td> 
			   <td><?php echo $adstatus_array[$item->item_status]; ?></td>-
     
		<!--<td><?php echo $item->item_status; ?></td>-->
	</tr>
<?php if($nofollow) { ?>
	<tr>
		<td><b>Use Nofollow?</b></td>
		<td><?php echo oiopub_dropmenu_kv($oiopub_set->arr_yesno, "adnofollow", $item->item_nofollow, 200); ?></td>
	</tr>
<?php } ?>
<?php if($oiopub_module->tracker == 1 && $oiopub_set->tracker['enabled'] == 1) { ?>
	<tr>
		<td><b>Stats Tracking?</b></td>
		<td><?php  if( $item->direct_link == 0) echo "Yes"; else echo "No"; ?></td>
	</tr>
<?php } ?>
	<tr>
		<td colspan="2" height="20"></td>
	</tr>
	<tr>
		<td><b>Price:</b></td>
		<td><?php echo $item->payment_amount; ?> </td>
	</tr>
	<tr>
		<td><b>Currency:</b></td>
		<td><?php echo $item->payment_currency; ?></td>
	</tr>
	<tr>
		<td><b>Payment Method:</b></td>
		<td><?php echo $item->payment_processor;  ?></td>
	</tr>
	<tr>
		<td><b>Payment Status:</b></td>
		<td><?php echo  $paystatus_array[$item->payment_status]; ?></td>
	</tr>
<?php if($item_id > 0) { ?>
	<tr>
		<td><b>Transaction ID:</b></td>
		<td><?php echo $item->payment_txid; ?> &nbsp;[<a href="javascript://" title="Contains the transaction ID from the payment processor. Leave blank if no processor used."><b>?</b></a>]</td>
	</tr>
<?php } ?>
	<tr>
		<td><b>Subscription?</b></td>
		<?php if($item_type == "video") { ?>
		<td><?php if($item->item_subscription == 0) echo "Yes"; else echo "No";  ?></td>
		<?php } else {?>
		<td><?php echo $item->item_subscription;  ?></td>
		<?php } ?>
	</tr>
	<tr>
		<td colspan="2" height="20"></td>
	</tr>
	<tr>
		<td><b>Charging Model:</b></td>
		<?php if($item_type == "video") { ?>
		<td><?php if($item->item_model == "impressions") echo "Views"; else echo $item->item_model; ?></td>
		<?php } else {?>
		<td><?php echo $item->item_model; ?></td>
		<?php } ?>
	</tr>
	<tr>
		<td><b>Start date:</b></td>
		<td><?php echo ($item->payment_time > 0 ? strftime("%m/%d/%Y  %T", $item->payment_time) : ""); ?>&nbsp;[<a href="javascript://" title="Can schedule future start date, as long as purchase status is 'Approved' and payment status is 'Paid'. Date format is mm/dd/yy"><b>?</b></a>]</td>
	</tr>
	<tr>
	     
		<td><b><span id="chargingModel">
		<?php if($item_type == "video") { ?>
		Views
		<?php } else { ?>
		<?php echo $item->item_model ? ucfirst($item->item_model) : "Days"; ?>
		<?php } ?>
		</span> purchased</b></td>
		<td><?php echo  $item->item_duration; ?> &nbsp;[<a href="javascript://" title="Set to zero if ad is a subscription, or should last forever"><b>?</b></a>]</td>
	</tr>
<?php if($item->item_model && $item->item_model != 'days') { ?>
	<tr>
		<td><b>
		<?php if($item_type == "video") { ?>
		Views
		<?php } else { ?>
		<?php echo ucfirst($item->item_model); ?>
         <?php } ?>
		left</b></td>
		<td><?php echo $item->item_duration_left > 0 ? $item->item_duration_left : 0; ?>&nbsp;[<a href="javascript://" title="The number of <?php echo $item->item_model; ?> left to use up"><b>?</b></a>]</td>
	</tr>
<?php } ?>
	<tr>
		<td colspan="2" height="20"></td>
	</tr>
<?php if($item_type == "post") { ?>
<?php if($item_id <= 0) { ?>
<?php if(!empty($cats_array)) { ?>
	<tr>
		<td><b>Post Category:</b></td>
		<td><?php echo  $item->item_type; ?></td>
	</tr>
<?php } ?>
	<tr>
		<td><b>Post Title:</b></td>
		<td><?php echo $item->post_title; ?></td>
	</tr>
	<tr>
		<td colspan="2" style="padding-top:15px;"><b>Post Content:</b><br /> <?php echo $item->post_content; ?> </td>
	</tr>
<?php } else { ?>
	<tr>
		<td></td><td style="padding-top:15px;"><i><a href="<?php echo oiopub_post_admin_edit($item->post_id); ?>" target="_parent">Click Here</a> to edit the post itself</i></td>
	</tr>
<?php } ?>
<?php } elseif($item_type == "link") { ?>
	<tr>
		<td><b>Link URL:</b></td>
		<td> <?php echo $item->item_url; ?> &nbsp;[<a href="javascript://" title="The url that your link goes to when clicked on"><b>?</b></a>]</td>
	</tr>
	<tr>
		<td><b>Link Text:</b></td>
		<td id="anchor1" > <?php echo $item->item_page; ?> &nbsp;[<a href="javascript://" title="The text that is linked to, make it short and snappy!"><b>?</b></a>]</td>
	</tr>
	<tr>
		<td><b>Link Tooltip:</b></td>
		<td> <?php echo $item->item_tooltip; ?> &nbsp;[<a href="javascript://" title="A description (like this one!) that a user will see when hovering over your link"><b>?</b></a>]</td>
	</tr>
<?php if(!empty($cats_array)) { ?>
	<tr>
		<td><b>Category:</b></td>
		<td><?php echo $item->category_id; ?>&nbsp;[<a href="javascript://" title="Select a category to associate the ad with"><b>?</b></a>]</td>
	</tr>
<?php } ?>
	<tr>
		<td valign="top"><b>Description:</b>&nbsp;[<a href="javascript://" title="You can add some descriptive text here that will sit below the link itself (like adsense)"><b>?</b></a>]</td>
		<td> <?php echo stripslashes($item->item_notes); ?> </td>
	</tr>
<?php } elseif($item_type == "video") {  ?> 
 
<tr><td colspan="2">
	<table id="inline-ad" width="100%" cellpadding="4" cellspacing="0">
	 
	 
	<tr>
		<td><b><?php echo __oio("video Upload"); ?>:</b></td>
		<td>
			 
			  
			  <video id="v0" style="width: 271px;border-radius: 5px;" controls tabindex="0" autobuffer preload>
    
    <source type="video/mp4;  codecs=&quot;avc1.42E01E, mp4a.40.2&quot;" src=" <?php echo $item->item_url; ?>"></source>
    <p>Sorry, your browser does not support the &lt;video&gt; element.</p>
</video>
		</td>
	</tr>
	 
	 
	 
	 
	</table></tr></td>
<?php }elseif($item_type == "inline") { ?>
	<tr><td colspan="2">
	<table id="inline-ad" width="100%" cellpadding="4" cellspacing="0">
	<?php if($oiopub_set->inline_ads['selection'] == 1) { ?>
	<tr>
		<td width="140"><b>YouTube URL:</b></td>
		<td><?php echo $item->item_url; ?> &nbsp;[<a href="javascript://" title="The URL of the youtube video that will be displayed"><b>?</b></a>]</td>
	</tr>
	<?php } elseif($oiopub_set->inline_ads['selection'] == 2) { ?>
	<?php if($oiopub_set->general_set['upload'] == 1) { ?>
	<tr>
		<td width="140" valign="top"><b>Banner Upload:</b>&nbsp;[<a href="javascript://" title="The banner image to upload"><b>?</b></a>]</td>
		<td>
			 
			<?php
			if(!empty($item->item_url)) {
				echo "<br />\n";
				echo "[<a href=\"" . $item->item_url . "\" target=\"_target\">view current banner</a>]\n";
			}
			?>
		</td>
	</tr>
	<?php } else { ?>
	<tr>
		<td><b>Banner URL:</b></td>
		<td> <?php echo $item->item_url; ?> &nbsp;[<a href="javascript://" title="The url of the banner image that will be displayed"><b>?</b></a>]</td>
	</tr>
	<?php } ?>
	<tr>
		<td><b>Website URL:</b></td>
		<td> <?php echo $item->item_page; ?> &nbsp;[<a href="javascript://" title="The URL of the website that the image will be linked to"><b>?</b></a>]</td>
	</tr>
	<tr>
		<td><b>Alt Text:</b></td>
		<td> <?php echo $item->item_tooltip; ?> &nbsp;[<a href="javascript://" title="A description of the image to use in the 'alt' image tag"><b>?</b></a>]</td>
	</tr>
	<?php } elseif($oiopub_set->inline_ads['selection'] == 3) { ?>
	<tr>
		<td><b>RSS Feed URL:</b></td>
		<td> <?php echo $item->item_url; ?> &nbsp;[<a href="javascript://" title="The URL of the feed that will be displayed"><b>?</b></a>]</td>
	</tr>
	<?php } ?>
	<tr>
		<td colspan="2" height="20"></td>
	</tr>
	<tr>
		<td valign="top"><b>Ad Code:</b>&nbsp;[<a href="javascript://" title="You can add code such as adsense here, to display the ads in a banner zone. You must include all script tags also."><b>?</b></a>]</td>
		<td> <?php echo stripslashes($item->item_notes); ?> </td>
	</tr>
	</table>
	<table id="inline-link" width="100%" cellpadding="4" cellspacing="0">
	<tr>
		<td width="140"><b>Link URL:</b></td>
		<td> <?php echo $item->item_url; ?> &nbsp;[<a href="javascript://" title="The URL of the website that the post text will be linked to"><b>?</b></a>]</td>
	</tr>
	<tr>
		<td><b>Link Tooltip:</b></td>
		<td> <?php echo $item->item_tooltip; ?> &nbsp;[<a href="javascript://" title="A description (like this one!) that a user will see when hovering over your link"><b>?</b></a>]</td>
	</tr>
	<tr>
		<td><b>Post ID:</b></td>
		<?php if(empty($item->post_phrase)) $item->post_phrase = "keyword(s) goes here"; ?>
		<td> <?php echo intval($item->post_id); ?> &nbsp;&nbsp;<input type="text" name="postphrase" size="31" value="<?php echo $item->post_phrase; ?>" />&nbsp;[<a href="javascript://" title="Pick a keyword(s) from your chosen page that will contain your link"><b>?</b></a>]</td>
	</tr>
	</table>
	</td></tr>
<?php } elseif($item_type == "banner") { ?>
	<?php if($oiopub_set->general_set['upload'] == 1) { ?>
	<tr>
		<td valign="top"><b>Banner Upload:</b>&nbsp;[<a href="javascript://" title="The banner image to upload"><b>?</b></a>]</td>
		<td>
			 
			<?php
			if(!empty($item->item_url)) {
				echo "<br />\n";
				echo "[<a href=\"" . $item->item_url . "\" target=\"_target\">view current banner</a>]\n";
			}
			?>
		</td>
	</tr>
	<?php } else { ?>
	<tr>
		<td><b>Banner URL:</b></td>
		<td> <?php echo $item->item_url; ?> &nbsp;[<a href="javascript://" title="The url of the banner image that will be displayed"><b>?</b></a>]</td>
	</tr>
	<?php } ?>
	<tr>
		<td><b>Website URL:</b></td>
		<td> <?php echo $item->item_page; ?> &nbsp;[<a href="javascript://" title="The URL of the website that the image will be linked to"><b>?</b></a>]</td>
	</tr>
	<tr>
		<td><b>Alt Text:</b></td>
		<td> <?php echo $item->item_tooltip; ?> &nbsp;[<a href="javascript://" title="A description of the image to use in the 'alt' image tag"><b>?</b></a>]</td>
	</tr>
	<tr>
		<td colspan="2" height="20"></td>
	</tr>
<?php if(!empty($cats_array)) { ?>
	<tr>
		<td><b>Category:</b></td>
		<td><?php echo   $item->category_id; ?>[<a href="javascript://" title="Select a category to associate the ad with"><b>?</b></a>]</td>
	</tr>
<?php } ?>
	<tr>
		<td><b>SubID:</b></td>
		<td> <?php echo $item->item_subid; ?> &nbsp;[<a href="http://forum.oiopublisher.com/discussion/1100/different-ads-on-each-page-ie-buddypresswpmu/#Item_2" title="Click to find out more" target="_blank"><b>?</b></a>]</td>
	</tr>
	<tr>
		<td colspan="2" height="20"></td>
	</tr>
	<tr>
		<td valign="top"><b>Ad Code:</b>&nbsp;[<a href="javascript://" title="You can add code such as adsense here, to display the ads in a banner zone. You must include all script tags also."><b>?</b></a>]</td>
		<td> <?php echo $item->item_notes; ?> </td>
	</tr>
<?php } ?>
	 
</table>
</form>
<br />
 
<script type="text/javascript">
<!--
<?php
if($item->item_type > 0) {
	$item_type = $item->item_type;
} elseif(isset($_POST['adtype']) && $_POST['adtype'] > 0) {
	$item_type = intval($_POST['adtype']);
} else {
	$item_type = $oiopub_set->inline_ads['selection'];
}
if($item_type == 4) {
	echo "showdiv('inline-ad', 'none');\n";
	echo "showdiv('inline-link', 'block');\n";
} else {
	echo "showdiv('inline-link', 'none');\n";
	echo "showdiv('inline-ad', 'block');\n";
}
?>
//-->
</script>