<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title><?php echo isset($templates['title_head']) ? $templates['title_head'] : $templates['title']; ?> | <?php echo $oiopub_set->site_name; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo $oiopub_set->plugin_url; ?>/templates/<?php echo $oiopub_set->template; ?>/style.css" />
	<script type='text/javascript' src='http://www.oiopublisher.com/libs/bubble/bubble.js'></script>
	<?php echo isset($_GET['rand']) ? '<meta name="robots" content="noindex,nofollow" />' . "\n" : ''; ?>
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.js"></script>
	<script type="text/javascript">
$(document).ready(function() {	

		var id = '#dialog';
	
		//Get the screen height and width
		var maskHeight = $(document).height();
		var maskWidth = $(window).width();
	
		//Set heigth and width to mask to fill up the whole screen
		$('#mask').css({'width':maskWidth,'height':maskHeight});
		
		//transition effect		
		$('#mask').fadeIn(1000);	
		$('#mask').fadeTo("slow",0.8);	
	
		//Get the window height and width
		var winH = $(window).height();
		var winW = $(window).width();
              
		//Set the popup window to center
		$(id).css('top',  winH/2-$(id).height()/2);
		$(id).css('left', winW/2-$(id).width()/2);
	
		//transition effect
		$(id).fadeIn(2000); 	
	
	//if close button is clicked
	$('.window .close').click(function (e) {
		//Cancel the link behavior
		e.preventDefault();
		
		$('#mask').hide();
		$('.window').hide();
	});		
	
	//if mask is clicked
	$('#mask').click(function () {
		$(this).hide();
		$('.window').hide();
	});		
	
});

</script>
<style type='text/css'>
.oio-overlay { display:block; width:100%; height:100%; z-index:1000; position:fixed; top:0px; left:0px; background:#000; opacity:0.3; filter:alpha(opacity=30); -ms-filter:"alpha(opacity=30)"; }
.oio-box { display:block; z-index:1000; width:700px; height:450px; position:fixed; top:70px; left:50%; margin-left:-350px; padding:15px; background:#FFF; border:1px solid #000; text-align:left; }
* html { overflow-y:hidden; }
* html body { overflow-y:auto; height:100%; }
* html .oio-overlay { position:absolute; }
* html .oio-box { position:absolute; }
</style>
</head>

<body onload='enableTooltip("sitecontainer");'>
<script type="text/javascript" src="http://www.oiopublisher.com/js/core.js"></script>
	<div id="wrap">
		<div id="header">
			<img src="<?php echo $oiopub_set->plugin_url; ?>/templates/<?php echo $oiopub_set->template; ?>/images/DYN-logo-white-small.png" alt="Logo" />
			<h1><?php echo $templates['title']; ?></h1>
		</div>
		<div id="content">
