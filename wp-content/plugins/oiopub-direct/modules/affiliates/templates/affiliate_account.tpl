<?php if($_GET['do'] == '' && $affiliate_id <= 0) { ?>
	<table class="start" align="center" width="500">
		<tr>
			<td>
				<h2 style="text-align:center;"><?php echo __oio("Affiliate Login"); ?></h2>
				<?php echo $templates['failed_login']; ?>
				<form action="<?php echo $oiopub_set->request_uri; ?>" method="post">
					<input type="hidden" name="process" value="login" />
						<table width="100%" align="center" cellpadding="4" cellspacing="4">
							<tr>
								<td width="100">
									<b><?php echo __oio("Email"); ?>:</b>
								</td>
								<td>
									<input type="text" size="40" name="email" />
								</td>
							</tr>
							<tr>
								<td>
									<b><?php echo __oio("Password"); ?>:</b>
								</td>
								<td>
									<input type="password" size="40" name="password" />
								</td>
							</tr>
							<tr>
								<td></td>
								<td>
									<a href="account.php?do=lost">
										<b><?php echo __oio("Forgotten Password?"); ?></b>
									</a>
								</td>
							</tr>
							<tr>
								<td></td>
								<td>
									<input type="submit" value="<?php echo __oio("Login"); ?>" />
								</td>
							</tr>
						</table>
				</form>
			</td>
		</tr>
		<tr>
			<td>
				<h2 id="reg" style="text-align:center; margin-top:60px;">
					<?php echo __oio("Create an affiliate account"); ?>
				</h2>
				<?php echo $templates['error']; ?>
				<form action="<?php echo $oiopub_set->request_uri; ?>#reg" method="post">
					<input type="hidden" name="process" value="register" />
					<table width="100%" align="center" cellpadding="4" cellspacing="4">
						<tr>
							<td width="100">
								<b><?php echo __oio("Name"); ?>:</b>
							</td>
							<td>
								<input type="text" size="40" name="name" value="<?php echo $name; ?>" />
							</td>
						</tr>
						<tr>
							<td>
								<b><?php echo __oio("Email"); ?>:</b>
							</td>
							<td>
								<input type="text" size="40" name="email" value="<?php echo $email; ?>" />
							</td>
						</tr>
						<tr>
							<td>
								<b><?php echo __oio("Password"); ?>:</b>
							</td>
							<td>
								<input type="password" size="40" name="password" value="" />
							</td>
						</tr>
						<tr>
							<td>
								<b><?php echo __oio("PayPal"); ?>:</b>
							</td>
							<td>
								<input type="text" size="40" name="paypal" value="<?php echo $paypal; ?>" />
							</td>
						</tr>
					<?php if($oiopub_set->general_set['security_question'] == 1) { ?>
						<tr>
							<td>
								<b><?php echo __oio("Security"); ?>:</b>
							</td>
							<td>
								<?php echo $captcha['question'] ?>&nbsp;&nbsp;&nbsp;<input type="text" size="5" name="security" />
							</td>
						</tr>
					<?php } if($oiopub_set->affiliates['terms']) { ?>
						<tr>
							<td></td>
							<td style="font-size:11px;">
								<?php echo __oio("By registering, you accept our <a href='account.php?do=terms' target='_blank'>Terms and Conditions</a>"); ?>
							</td>
						</tr>
					<?php } ?>
						<tr>
							<td></td>
							<td>
								<input type="submit" value="<?php echo __oio("Register"); ?>" />
							</td>
						</tr>
					</table>
				</form>
			</td>
		</tr>
</table>
<?php } ?>
<?php if($_GET['do'] == '' && $affiliate_id > 0) { ?>
	<?php if($oiopub_set->affiliates['help']) { ?>
		<table align="center" width="90%">
			<tr>
				<td align="right">
					&raquo; <a href="account.php?do=help" style="color:red;"><?php echo __oio("Affiliate Help & FAQs"); ?></a>
				</td>
			</tr>
		</table>
	<?php } ?>
	<div id="oiopub-container" style="padding:20px 0;">
		<div id="sitecontainer"><!-- Site Container -->
			<div class="sidebar">
				<h4>Marketplace Search</h4>
				<div style="margin-bottom:20px;">
					<form method="post" action="http://www.oiopublisher.com/search.php">
						<input type="hidden" name="process" value="search">
						<input type="hidden" name="csrf" value="da663403501ce2e6f19ee018f3d9049a">
						<input type="hidden" name="type" value="1">
						<p>
							<input type="text" name="search" id="Search" value=""> <input type="submit" value="Go">
						</p>
						<!--
						<p>Filter: &nbsp;
							<select size="1" name="brand" style="width:145px;">
								<option value="0" selected="selected"> All Sites</option>
								<option value="entrecard"> EntreCard</option>
							</select>
						</p>-->
					</form>
				</div>
				<h4>My Account</h4>
				<ul style="margin-bottom:20px;">
					<li><a href="account.php?do=logout"><?php echo __oio("Logout"); ?></a></li>
					<li><a href="#">Submit</a></li>
					<li><a href="#">Support</a></li>
				</ul>
			</div>

			<div class="sitecontent"><!-- Sitecontent -->
				<style type="text/css">.this { color:red; } .last{color:red;} .all{color: red;}</style>
				<h3 style="margin:0 0 -20px 0;">My Affiliate Stats</h3>
				<p>
					You have <b>$0.00</b> due to be paid out this week, and a further <b>$0.00</b> that has yet to <a href="faq.php#affiliate" target="_blank">mature</a>.
				</p>
				<p style="color:red;">
					<b>» Please enter your <a href="#settings">payment details</a> below, or you cannot receive payment!</b>
				</p>
				<table width="96%" style="background:#CCFFCC; border:1px solid #66FF66; padding:10px; margin:20px 3px 10px 3px;">
					<tbody>
						<tr>
							<td width="110"><b>Affiliate ID:</b></td>
							<td colspan="2"><?php echo $affiliate_id; ?></td>
						</tr>
						<tr>
							<td colspan="3" height="5"></td>
						</tr>
						<tr>
							<td width="110"><b>Referral Link:</b></td>
							<td width="300"><?php echo "http://" . $templates['aff_url']; ?></td>
							<td>[<a href="account.php?show=link">customise</a>]</td>
						</tr>
						<tr>
							<td colspan="3" height="5"></td>
						</tr>
						<tr>
							<td><b>My Coupon:</b></td>
							<td>
							<a href="account.php?show=coupon"><i>Click here to setup your code</i></a>
							</td>
							<td>[<a href="account.php?show=coupon">more info</a>]</td>
						</tr>
					</tbody>
				</table>
				<?php
					if($_GET['show']=="link") {
				?>
						<div class="oio-overlay"></div>
						<div class="oio-box">
							<table style="width:100%; border:0px; margin-bottom:20px;" cellspacing="0" cellpadding="0">
								<tbody>
									<tr>
										<td valign="top">
											<h3 style="background:none; margin:0px; padding:0px;">
												<font color="red">Affiliate Stats: link</font>
											</h3>
										</td>
										<td valign="top" style="text-align:right;">
											[<a href="account.php"><b>Close</b></a>]
										</td>
									</tr>
								</tbody>
							</table>
							<div style="height:400px; overflow-y:scroll;">
								<p style="border-top:1px dashed #999; border-bottom:1px dashed #999; padding:10px 0 10px 0;">
									<b>Basic Link:</b><?php echo "http://" . $templates['aff_url']; ?>
								</p>
								<p style="margin-top:30px; color:red;">
									<b>Extra Link Parameters</b>
								</p>
								<p style="line-height:24px;">*
									<b>subID</b> - any string of your choice, can be used to track different campaigns<br>* <b>page</b> - redirect users to a page other than the home page upon clicking
								</p>
								<p>eg.
									<a target="blank" href="<?php echo "http://" . $templates['aff_url']; ?>&amp;subID=campaign1&amp;page=buynow.php"><?php echo "http://" . $templates['aff_url']; ?>&amp;subID=campaign1&amp;page=buynow.php</a>
								</p>
								<p style="margin-top:30px; color:red;">
									<b>Cloaking your Links</b>
								</p>
								<p>As you can see from the example link above, referral urls can get quite long. You can make the links shorter using a couple of techniques:</p>
								<p style="line-height:24px;">* use a link shortening service, such as
									<a target="_blank" href="http://tinyurl.com">tinyurl.com</a>
									<br>* if you host your own website, you can use an
									<a target="_blank" href="http://www.toasteggme.com/index.php/earning-step-by-step/16th-hide-your-affiliate-links-using-htaccess-file/">htaccess file</a> to shorten the link
								</p>
							</div>
						</div>
				<?php
					}
				?>
				<?php
					if($_GET['show']=="coupon") {
				?>
						<div class="oio-overlay"></div>
						<div class="oio-box">
							<table style="width:100%; border:0px; margin-bottom:20px;" cellspacing="0" cellpadding="0">
							<tbody><tr>
							<td valign="top"><h3 style="background:none; margin:0px; padding:0px;"><font color="red">Affiliate Stats: coupon</font></h3></td>
							<td valign="top" style="text-align:right;">[<a href="account.php"><b>Close</b></a>]</td>
							</tr>
							</tbody></table>
							<div style="height:400px; overflow-y:scroll;">
							<p style="border-top:1px dashed #999; border-bottom:1px dashed #999; padding:10px 0 10px 0;"><b><?php echo date('F'); ?> Coupon Code:</b> &nbsp;&nbsp; <i>code not setup yet</i></p>
							<p style="margin-top:30px; color:red;"><b>Set Coupon Code</b></p>

							<form method="post" action="">
							<input type="hidden" name="process" value="coupon">
							<input type="hidden" name="csrf" value="67182a98ac26c6305b495532538448ce">
							<b>Code:</b> &nbsp;&nbsp;&nbsp;FALL16-<input type="text" name="coupon" size="14" maxlength="8" value=""> &nbsp;<input type="submit" value="Go">
							</form>
							<p style="margin-top:30px; color:red;"><b>How it Works</b></p>
							<p>Your coupon entitles referrals to a discount on their purchase, and ensures that you will be credited with the sale. The code will change on the 1st of every month, at which time you will be notified by email.</p>
							<p style="margin-top:30px; color:red;"><b>October Restrictions</b></p>
							<ul style="line-height:24px; margin-left:-10px;">
							<li>your affiliates will receive a <b>$10 discount</b> when using this coupon</li>
							<li>this coupon is valid during the <b>month of October</b> only</li>
							<li>as you have purchased OIO, only half of the discount ($5.00) will be taken from your commission</li>
							</ul>
							</div>
							</div>
				<?php
					}
				?>
				<?php
					if($_GET['show']=="level") {
				?>
						<div class="oio-overlay"></div>
						<div class="oio-box">
							<table style="width:100%; border:0px; margin-bottom:20px;" cellspacing="0" cellpadding="0">
							<tbody><tr>
						<td valign="top"><h3 style="background:none; margin:0px; padding:0px;"><font color="red">Affiliate Stats: level</font></h3></td>
						<td valign="top" style="text-align:right;">[<a href="account.php"><b>Close</b></a>]</td>
						</tr>
						</tbody></table>
						<div style="height:400px; overflow-y:scroll;">
						<p style="border-top:1px dashed #999; border-bottom:1px dashed #999; padding:10px 0 10px 0;"><b>Commission level for <?php echo date("F Y") ?>:</b> &nbsp;&nbsp;<?php echo $aff->level; ?>%</p>
						<p style="padding-top:20px;">If you are able to refer a large number of affiliates to OIO and would like to discuss increasing your commission level, please <a href="/contact.php?s=3">contact us</a>.</p>
						</div>
						</div>
				<?php
					}
				?>
				<?php
					if($_GET['show']=="clicks") {
				?>
						<div class="oio-overlay"></div>
						<div class="oio-box">
							<table style="width:100%; border:0px; margin-bottom:20px;" cellspacing="0" cellpadding="0">
								<tbody>
									<tr>
										<td valign="top">
											<h3 style="background:none; margin:0px; padding:0px;">
												<font color="red">Affiliate Stats: clicks</font>
											</h3>
										</td>
										<td valign="top" style="text-align:right;">
											[<a href="account.php"><b>Close</b></a>]
										</td>
									</tr>
								</tbody>
							</table>
							<div style="height:400px; overflow-y:scroll;">
								<p>
									<a class="<?php if($_GET['show']=='clicks' && !$_GET['month']) echo 'this' ?>" href="account.php?show=clicks">This Month</a> |
									<a class="<?php if($_GET['show']=='clicks' && $_GET['month']=='last') echo 'last' ?>" href="account.php?show=clicks&amp;month=last">Last Month</a> |
									<a class="<?php if($_GET['show']=='clicks' && $_GET['month']=='all') echo 'all' ?>" href="account.php?show=clicks&amp;month=all">All Time</a>
								</p>
								<table width="100%" border="0" cellspacing="4" cellpadding="4">
									<tbody>
										<tr>
											<td>
												<b>Referer</b>
											</td>
											<td>
												<b>Time (Europe/London)</b>
											</td>
											<!-- <td>
												<b>SubID</b>
											</td> -->
										</tr>
										<?php
											if($_GET['show']=='clicks' && !$_GET['month']){
												foreach($clicks as $clk){
												?>
													<tr>
														<td>
															<a href="<?php echo 'http://'.$clk->page ?>" target="_blank"><?php echo 'http://'.$clk->page ?></a>
														</td>
														<td><?php echo date("d M", $clk->date) ?>, <?php echo date('h:i:s',strtotime($clk->time)) ?></td>
														<!-- <td>N/A</td> -->
													</tr>
												<?php
												}
											}
											if($_GET['show']=='clicks' && $_GET['month']=="last"){
												foreach($clicks as $clk){
												?>
													<tr>
														<td>
															<a href="<?php echo 'http://'.$clk->page ?>" target="_blank"><?php echo 'http://'.$clk->page ?></a>
														</td>
														<td><?php echo date("d M", $clk->date) ?>, <?php echo date('H:s',strtotime($clk->time)) ?></td>
														<!-- <td>N/A</td> -->
													</tr>
												<?php
												}
											}
											if($_GET['show']=='clicks' && $_GET['month']=="all"){
												foreach($clicks as $clk){
												?>
													<tr>
														<td>
															<a href="<?php echo 'http://'.$clk->page ?>" target="_blank"><?php echo 'http://'.$clk->page ?></a>
														</td>
														<td><?php echo date("d M", $clk->date) ?>, <?php echo date('H:s',strtotime($clk->time)) ?></td>
														<!-- <td>N/A</td> -->
													</tr>
												<?php
												}
											}
										?>


									</tbody>
								</table>
							</div>
						</div>
				<?php
					}
				?>
				<?php
					if($_GET['show']=="sales"){
					?>
						<div class="oio-overlay"></div>
						<div class="oio-box">
							<table style="width:100%; border:0px; margin-bottom:20px;" cellspacing="0" cellpadding="0">
								<tbody>
									<tr>
										<td valign="top">
											<h3 style="background:none; margin:0px; padding:0px;"><font color="red">Affiliate Stats: sales</font></h3>
										</td>
										<td valign="top" style="text-align:right;">
											[<a href="account.php"><b>Close</b></a>]
										</td>
									</tr>
								</tbody>
							</table>
							<div style="height:400px; overflow-y:scroll;">
								<p>
									<a class="<?php if($_GET['show']=='sales' && !$_GET['month']) echo 'this' ?>" href="account.php?show=sales">This Month</a> |
									<a class="<?php if($_GET['show']=='sales' && $_GET['month']=='last') echo 'last' ?>" href="account.php?show=sales&amp;month=last">Last Month</a> |
									<a class="<?php if($_GET['show']=='sales' && $_GET['month']=='all') echo 'all' ?>" href="account.php?show=sales&amp;month=all">All Time</a>
								</p>
								<table width="100%" border="0" cellspacing="4" cellpadding="4">
									<tbody>
										<tr>
											<td><b>User</b></td>
											<td><b>Time (Europe/London)</b></td>
											<td><b>Commission</b></td>
											<td><b>Status</b></td>
											<td><b>Referer</b></td>
											<td><b>SubID</b></td>
										</tr>
										<?php
											if(count($sales)<1){
										?>
												<tr>
													<td align="center" colspan="5" height="100">No sales found within this time frame</td>
												</tr>
										<?php
											}
										?>

									</tbody>
								</table>
							</div>
						</div>

				<?php
					}
				?>
				<?php
					if($_GET['show']=="commission"){
				?>
						<div class="oio-box">
							<table style="width:100%; border:0px; margin-bottom:20px;" cellspacing="0" cellpadding="0">
								<tbody>
									<tr>
										<td valign="top">
											<h3 style="background:none; margin:0px; padding:0px;"><font color="red">Affiliate Stats: commission</font></h3>
										</td>
										<td valign="top" style="text-align:right;">
											[<a href="account.php"><b>Close</b></a>]
										</td>
									</tr>
								</tbody>
							</table>
							<div style="height:400px; overflow-y:scroll;">
								<p style="margin-bottom:30px;">
									<a class="<?php if($_GET['show']=='commission' && !$_GET['month']) echo 'this' ?>" href="account.php?show=commission">This Month</a> |
									<a class="<?php if($_GET['show']=='commission' && $_GET['month']=='last') echo 'last' ?>" href="account.php?show=commission&amp;month=last">Last Month</a> |
									<a class="<?php if($_GET['show']=='commission' && $_GET['month']=='all') echo 'all' ?>" href="account.php?show=commission&amp;month=all">All Time</a></p>
									<?php
										if($_GET['month']=="this" || !$_GET['month']){
									?>
									<p>
										<b>Pending Commission:</b>&nbsp; <?php echo $symbol.number_format($verified_commission,2) ?><br><i>this amount represents the commission that has yet to reach the 14 day maturation period</i>
									</p>
									<p style="margin-top:30px;">
										<b>Matured Commission:</b>&nbsp; <?php echo $symbol.number_format($unverified_commission,2) ?><br><i>this amount represents the commission that is due to be paid out to you at the end of the week</i>
									</p>
									<p style="margin-top:30px;">
										<b>Paid Commission:</b>&nbsp; <?php echo $symbol.number_format($paid_commission,2) ?><br><i>this amount represents the commission that has already been paid out to you</i>
									</p>
									<?php
										}
									?>
									<?php
										if($_GET['month']=="last"){
									?>
									<p>
										<b>Pending Commission:</b>&nbsp; <?php echo $symbol.number_format($verified_commission,2) ?><br><i>this amount represents the commission that has yet to reach the 14 day maturation period</i>
									</p>
									<p style="margin-top:30px;">
										<b>Matured Commission:</b>&nbsp; <?php echo $symbol.number_format($unverified_commission,2) ?><br><i>this amount represents the commission that is due to be paid out to you at the end of the week</i>
									</p>
									<p style="margin-top:30px;">
										<b>Paid Commission:</b>&nbsp; <?php echo $symbol.number_format($paid_commission,2) ?><br><i>this amount represents the commission that has already been paid out to you</i>
									</p>
									<?php
										}
									?>
									<?php
										if($_GET['month']=="all"){
									?>
									<p>
										<b>Pending Commission:</b>&nbsp; <?php echo $symbol.number_format($verified_commission,2) ?><br><i>this amount represents the commission that has yet to reach the 14 day maturation period</i>
									</p>
									<p style="margin-top:30px;">
										<b>Matured Commission:</b>&nbsp; <?php echo $symbol.number_format($unverified_commission,2) ?><br><i>this amount represents the commission that is due to be paid out to you at the end of the week</i>
									</p>
									<p style="margin-top:30px;">
										<b>Paid Commission:</b>&nbsp; <?php echo $symbol.number_format($paid_commission,2) ?><br><i>this amount represents the commission that has already been paid out to you</i>
									</p>
									<?php
										}
									?>
							</div>
						</div>
				<?php
					}
				?>
				<table width="100%" border="0" style="margin-top:10px;">
					<tbody>
						<tr>
							<td valign="top" width="420">
							<?php if($_GET['month']=="this" || !$_GET['month']){ ?>
								<table width="100%" border="0" style="background:#F1F1F1; border:1px solid #D3D3D3; padding:10px; line-height:24px;">
									<tbody>
										<tr>
										<td colspan="3" align="right">
											<p style="margin:-13px -5px 0px 0px; font-size:10px;"><a class="this" href="account.php">This Month</a> | <a href="account.php?month=last">Last Month</a> | <a href="account.php?month=all">All Time</a></p>
										</td>
									</tr>
									<tr>
										<td width="140"><b>Monthly Level:</b></td>
										<td width="60"><?php echo $aff->level; ?>%</td>
										<td>[<a href="account.php?show=level">increase level</a>]</td>
									</tr><tr>
										<td width="140"><b>Total Clicks:</b></td>
										<td><?php echo $total_hits; ?></td>
										<td>[<a href="account.php?show=clicks">clicks breakdown</a>]</td>
									</tr>
									<tr>
										<td><b>Total Sales:</b></td>
										<td><?php echo $total_sales; ?></td>
										<td>[<a href="account.php?show=sales">sales breakdown</a>]</td>
									</tr>
									<tr>
										<td><b>Total Earned:</b></td>
										<td>
											<?php echo $symbol; ?>
											<?php echo number_format($total_earn,2) ?>
										</td>
										<td>[<a href="account.php?show=commission">commission breakdown</a>]</td>
									</tr>
								</tbody>
								</table>
								<?php } ?>
							<?php if($_GET['month']=="last"){ ?>
								<table width="100%" border="0" style="background:#F1F1F1; border:1px solid #D3D3D3; padding:10px; line-height:24px;">
									<tbody>
										<tr>
										<td colspan="3" align="right">
											<p style="margin:-13px -5px 0px 0px; font-size:10px;"><a href="account.php">This Month</a> | <a class="last" href="account.php?month=last">Last Month</a> | <a href="account.php?month=all">All Time</a></p>
										</td>
									</tr>
									<tr>
										<td width="140"><b>Monthly Level:</b></td>
										<td width="60"><?php echo $aff->level; ?>%</td>
										<td>[<a href="account.php?show=level">increase level</a>]</td>
									</tr><tr>
										<td width="140"><b>Total Clicks:</b></td>
										<td><?php echo $total_hits; ?></td>
										<td>[<a href="account.php?show=clicks">clicks breakdown</a>]</td>
									</tr>
									<tr>
										<td><b>Total Sales:</b></td>
										<td><?php echo $total_sales; ?></td>
										<td>[<a href="account.php?show=sales">sales breakdown</a>]</td>
									</tr>
									<tr>
										<td><b>Total Earned:</b></td>
										<td>
											<?php echo $symbol; ?>
											<?php echo number_format($total_earn,2) ?>
										</td>
										<td>[<a href="account.php?show=commission">commission breakdown</a>]</td>
									</tr>
								</tbody>
								</table>
								<?php } ?>
								<?php if($_GET['month']=="all"){ ?>
								<table width="100%" border="0" style="background:#F1F1F1; border:1px solid #D3D3D3; padding:10px; line-height:24px;">
									<tbody>
										<tr>
										<td colspan="3" align="right">
											<p style="margin:-13px -5px 0px 0px; font-size:10px;"><a href="account.php">This Month</a> | <a href="account.php?month=last">Last Month</a> | <a class="all" href="account.php?month=all">All Time</a></p>
										</td>
									</tr>
									<tr>
										<td width="140"><b>Monthly Level:</b></td>
										<td width="60"><?php echo $aff->level; ?>%</td>
										<td>[<a href="account.php?show=level">increase level</a>]</td>
									</tr><tr>
										<td width="140"><b>Total Clicks:</b></td>
										<td><?php echo $total_hits ?></td>
										<td>[<a href="account.php?show=clicks">clicks breakdown</a>]</td>
									</tr>
									<tr>
										<td><b>Total Sales:</b></td>
										<td><?php echo $total_sales; ?></td>
										<td>[<a href="account.php?show=sales">sales breakdown</a>]</td>
									</tr>
									<tr>
										<td><b>Total Earned:</b></td>
										<td>
											<?php echo $symbol; ?>
											<?php echo number_format($total_earn,2) ?>
										</td>
										<td>[<a href="account.php?show=commission">commission breakdown</a>]</td>
									</tr>
								</tbody>
								</table>
								<?php } ?>
							</td>
							<td width="20"></td>
							<td valign="top" align="center">
							<?php
								foreach ($banners as $banner) {
										$bannerurl = $banner->item_url;
										$bannerid = $banner->item_id;
									}
									if($banners){
			?>
								<img src="<?php echo $bannerurl; ?>" style="width:125px; height:125px; border:0px;" alt="">

								<div style="margin-top:7px;">[<a href="#banners">More Banners</a>]</div>
								<?php } ?>
							</td>
						</tr>
						<tr></tr>
					</tbody>
				</table>
				<script type="text/javascript">
					<!--
					function update_size() {
						var id = document.banners.banners.value;
						document.getElementById('banner_size').innerHTML = id;
						document.getElementById('banner_preview').href = "http://www.oiopublisher.com/banners.php?u=16434&b=" + id;
					}
				//-->
				</script>
				<h3 id="banners" style="margin:40px 0 -20px 0;">Banner Code Generator</h3>
				<form name="banners">
					<p>This tool lets you instantly create custom affiliate banner code to use on your websites!</p>
					<p style="margin-top:20px;">
						<b>Banner Size:</b>
						&nbsp;
						<select size="1" name="banners" style="width:150px;" onchange="update_size();;">
							<option value="125x125"> 125x125</option>
							<option value="468x60"> 468x60</option>
							<option value="728x90"> 728x90</option>
							<option value="250x250"> 250x250</option>
							<option value="120x240"> 120x240</option>
							<option value="120x600"> 120x600</option>
						</select>
						&nbsp;&nbsp;&nbsp;[
							<a id="banner_preview" href="banners.php?u=16434&amp;b=125x125" target="_blank">Preview Banner</a>
					]</p>
					<div style="background:#FAFAD2; border:1px solid #F0E68C; padding:10px;">
						&lt;a href="<?php echo 'http://' . $templates['aff_url']?>"&gt;&lt;img src="http://www.doityourselfnation.org/bit_bucket/images/banners/<span id="banner_size">125x125</span>_1.gif" alt="Do It Yourself Naiton" /&gt;&lt;/a&gt;
					</div>
				</form>
				<h3 id="settings" style="margin:40px 0 0 0;">My Payment Details</h3>
					<form method="post" action="/affiliate.php#settings">
						<input type="hidden" name="csrf" value="da663403501ce2e6f19ee018f3d9049a">
						<input type="hidden" name="process" value="settings">
						<table width="100%" border="0" cellspacing="4" cellpadding="4">
							<tbody>
								<tr>
									<td width="130"><b>Processor:</b></td>
									<td>
										<select size="1" name="pay_method" style="width:216px;">
											<option value="0" selected="selected"> -- select --</option>
											<option value="PayPal"> PayPal</option>
										</select>
									</td>
								</tr>
								<tr>
									<td>
										<b>Account:</b>
									</td>
									<td>
										<input type="text" name="pay_account" size="30" value="">
									</td>
								</tr>
								<tr>
									<td>
										<b>Monthly Reports:</b>
									</td>
									<td>
										<select size="1" name="affiliate_reports" style="width:216px;">
											<option value="0"> Don't receive reports</option>
											<option value="1" selected="selected"> Receive reports by email</option>
										</select>
									</td>
								</tr>
								<tr>
									<td></td>
									<td>
										<input type="submit" value="Update">
									</td>
								</tr>
							</tbody>
						</table>
					</form>
					<br>
				</div><!-- Sitecontent Ends -->
			<div class="clear"></div>
		</div>
	</div>
	<!-- <table class="start" align="center" width="90%">
		<tr>
			<td>
				<h3 style='margin-bottom:2px;'>
					<?php echo __oio("Affiliate Link"); ?> &nbsp;
					<small>
						[<a href="account.php?do=logout"><?php echo __oio("Logout"); ?></a>]
					</small>
				</h3>
				<?php echo "http://" . $templates['aff_url']; ?>
			</td>
		</tr>
	</table> -->
	<!-- <table class="start" align="center" width="90%">
		<tr>
			<td>
				<h3 style='margin-bottom:2px;'><?php echo __oio("Account Settings"); ?></h3>
				<?php echo __oio("You can update your email address or paypal address below."); ?>
				<br /><br /><br />
				<form action="<?php echo $oiopub_set->request_uri; ?>" method="post">
					<input type="hidden" name="process" value="update_settings" />
					<table width="60%" align="center" cellpadding="4" cellspacing="4" style="background:#F0FFFF; padding:15px;">
						<tr>
							<td colspan="2" align="center">
								<?php echo $templates['error']; ?>
							</td>
						</tr>
						<tr>
							<td>
								<b><?php echo __oio("Email"); ?>:</b>
							</td>
							<td>
								<input type="text" size="40" name="email" value="<?php echo $aff->email; ?>" />
							</td>
						</tr>
						<tr>
							<td>
								<b><?php echo __oio("PayPal"); ?>:</b>
							</td>
							<td>
								<input type="text" size="40" name="paypal" value="<?php echo $aff->paypal; ?>" />
							</td>
						</tr>
						<tr>
							<td>
								<b><?php echo __oio("Password"); ?>:</b>
							</td>
							<td><input type="text" size="40" name="password" value="" /></td>
						</tr>
						<tr>
							<td></td>
							<td>
								<input type="submit" value="<?php echo __oio("Update"); ?>" />
							</td>
						</tr>
					</table>
				</form>
			</td>
		</tr>
	</table>
	<table class="start" align="center" width="90%">
		<tr>
			<td>
				<h3 style='margin-bottom:2px;'><?php echo __oio("Discount Settings"); ?></h3>
				<?php echo __oio("The Affiliate Discount will allow you to offer customers a better deal, by transferring some of the money you would make as an affiliate to a customer discount. If you earn 20% per purchase for example, setting the discount value to 5 would give the customer a 5% discount, while you earn 15%."); ?>
				<br /><br /><br />
				<form action="<?php echo $oiopub_set->request_uri; ?>" method="post">
					<input type="hidden" name="process" value="update_discount" />
					<table align="center" cellpadding="4" cellspacing="4" style="background:#F0FFFF; padding:15px;">
						<tr>
							<td>
								<b><?php echo __oio("My Earnings"); ?>:</b>
							</td>
							<td><?php echo $templates['discount_text']; ?></td>
						</tr>
						<tr>
							<td>
								<b><?php echo __oio("Dicsount Value"); ?>:</b>
							</td>
							<td>
								<input type="text" size="30" name="coupon" value="<?php echo $aff->coupon; ?>" />
								<?php echo $templates['discount_type']; ?>
							</td>
						</tr>
						<tr>
							<td></td>
							<td>
								<input type="submit" value="<?php echo __oio("Update"); ?>" />
							</td>
						</tr>
					</table>
				</form>
			</td>
		</tr>
	</table>
	<table class="start" align="center" width="90%">
		<tr>
			<td>
				<h3 style='margin-bottom:2px;'><?php echo __oio("My Sales Stats"); ?></h3>
				<?php echo __oio("Below are your affiliate sales stats. There is a %s day waiting period before sales commission can be paid out, which is displayed as unverified commission. The verified commission is eligible to be paid out, but has yet to be processed.", array( $oiopub_set->affiliates['maturity'] )); ?>
				<br /><br /><br />
				<table width="60%" align="center" cellpadding="4" cellspacing="4" style="background:#F0FFFF; padding:15px;">
					<tr>
						<td colspan="2" style="padding-bottom:15px;">
							<b><?php echo __oio("Conversion Ratio"); ?>:</b>
						</td>
					</tr>
					<tr>
						<td>
							<?php echo __oio("Total Hits Received"); ?>:
						</td>
						<td>
							<?php echo $total_hits; ?>
						</td>
					</tr>
					<tr>
						<td>
							<?php echo __oio("Total Sales Generated"); ?>:
						</td>
						<td>
							<?php echo $total_sales; ?>
						</td>
					</tr>
					<tr>
						<td>
							<?php echo __oio("Conversion Rate"); ?>:
						</td>
						<td>
							<?php echo number_format($conversion_ratio, 2); ?>%
						</td>
					</tr>
					<tr>
						<td colspan="2" height="30"></td>
					</tr>
					<tr>
						<td colspan="2" style="padding-bottom:15px;">
							<b><?php echo __oio("Sales Commission"); ?>:</b>
						</td>
					</tr>
					<tr>
						<td>
							<?php echo __oio("Unverified Commission"); ?>:
						</td>
						<td>
							<?php echo number_format($unverified_commission, 2); ?>
							<?php echo $oiopub_set->general_set['currency']; ?>
						</td>
					</tr>
					<tr>
						<td>
							<?php echo __oio("Verified Commission"); ?>:
						</td>
						<td>
							<?php echo number_format($verified_commission, 2); ?> <?php echo $oiopub_set->general_set['currency']; ?>
						</td>
					</tr>
					<tr>
						<td>
							<?php echo __oio("Paid Commission"); ?>:
						</td>
						<td>
							<?php echo number_format($paid_commission, 2); ?> <?php echo $oiopub_set->general_set['currency']; ?>
						</td>
					</tr>
				</table>
				<?php if($fraud_commission > 0) { ?>
					<br />
					<table align="center">
						<tr>
							<td align="center">
								<?php echo __oio("Commission removed due to sales discrepency"); ?>:
								<?php echo number_format($fraud_commission, 2); ?>
								<?php echo $oiopub_set->general_set['currency']; ?>
							</td>
						</tr>
					</table>
				<?php } ?>
			</td>
		</tr>
	</table> -->
<?php } ?>
<?php if($_GET['do'] == 'lost') { ?>
	<table class="start" align="center" width="500">
		<tr>
			<td>
				<h2 style="text-align:center;"><?php echo __oio("Resend Password"); ?></h2>
				<?php echo $templates['error']; ?>
				<form action="<?php echo $oiopub_set->request_uri; ?>" method="post">
					<input type="hidden" name="process" value="lost_password" />
					<table align="center" cellpadding="4" cellspacing="4">
						<tr>
							<td width="100">
								<b><?php echo __oio("Email"); ?>:</b>
							</td>
							<td>
								<input type="text" size="40" name="email" />
							</td>
						</tr>
					<?php if($oiopub_set->general_set['security_question'] == 1) { ?>
						<tr>
							<td>
								<b><?php echo __oio("Security"); ?>:</b>
							</td>
							<td>
								<?php echo $captcha['question'] ?>&nbsp;&nbsp;&nbsp;
								<input type="text" size="5" name="security" />
							</td>
						</tr>
					<?php } ?>
						<tr>
							<td></td>
							<td>
								<input type="submit" value="<?php echo __oio("Send Password"); ?>" />
							</td>
						</tr>
					</table>
				</form>
				<br /><br />
				<center>
					<a href="account.php">
						<b><?php echo __oio("Back to Affiliate Page"); ?></b>
					</a>
				</center>
			</td>
		</tr>
	</table>
<?php } ?>
<?php if($_GET['do'] == 'terms') { ?>
	<table align="center" width="90%">
		<tr>
			<td align="right">
				&raquo; <a href="javascript:history.go(-1);" style="color:red;"><?php echo __oio("Back"); ?></a>
			</td>
		</tr>
	</table>
	<h2>
		<?php echo __oio("Affiliate Terms and Conditions"); ?>
	</h2>
	<table class="start" align="center" width="500">
		<tr>
			<td align="left">
				<?php echo str_replace(array("\r\n", "\n"), "<br />", stripslashes($oiopub_set->affiliates['terms'])); ?>
			</td>
		</tr>
	</table>
<?php } ?>
<?php if($_GET['do'] == 'help') { ?>
	<table align="center" width="90%">
		<tr>
			<td align="right">
				&raquo; <a href="javascript:history.go(-1);" style="color:red;"><?php echo __oio("Back"); ?></a>
			</td>
		</tr>
	</table>
	<h2>
		<?php echo __oio("Affiliate help & FAQs"); ?>
	</h2>
	<table class="start" align="center" width="500">
		<tr>
			<td align="left">
				<?php echo str_replace(array("\r\n", "\n"), "<br />", stripslashes($oiopub_set->affiliates['help'])); ?>
			</td>
		</tr>
	</table>
<?php } ?>