<?php

/**

Plugin Name: Easy Print - Responsive FlipBook WP Extension
Plugin URI: http://codecanyon.net/user/mpc
Description: This plugin extend Responsive Flip Book Wordpress Plugin with print functionality for users.
Version: 1.1
Author: MassivePixelCreation
Author URI: http://codecanyon.net/user/mpc
Text Domain: rfbwp-print
Domain Path: /languages/

**/

define( 'RFB_PRINT_PLUGIN_ROOT', plugin_dir_url( __FILE__ ) );

add_action( 'plugins_loaded', 'rfb_print_localization' );
function rfb_print_localization() {
	load_plugin_textdomain( 'rfbwp-print', FALSE, basename( dirname( __FILE__ ) ) . '/languages/' );
}

add_action( 'admin_init', 'rfb_print_admin_init', 99999 );
function rfb_print_admin_init() {
	if( !defined( 'MPC_PLUGIN_FILE' ) )
		return;

	$plugin_path = plugin_basename( MPC_PLUGIN_FILE );

	if( is_plugin_inactive( $plugin_path ) )
		return;

	add_action( 'rfbwp/scripts', 'rfb_print_admin_enqueue_scripts' );
	
	add_filter( 'rfbwp/options/navigationButtons',	'rfb_print_nav_options' );
	add_filter( 'rfbwp/options/showAllPages',		'rfb_print_options' );
}

function rfb_print_admin_enqueue_scripts() {
	wp_localize_script( 'flipbook-js', 'mpcthPrintLocalize', array(
		'print' => __( 'Print?', 'rfbwp-print' )
	) );
}

function rfb_print_nav_options( $options ) {
	global $rfbwp_shortname;
	
	$print_options = array(
		array(
			"name" => __( "Print", 'rfbwp-print' ), // this defines the heading of the option
			"desc" => __( "Print:", 'rfbwp-print' ), // this is the field/option description
			"desc-pos" => "top",
			"id"   => $rfbwp_shortname."_fb_nav_pt", // the id must be unique, it is used to call back the propertie inside the them
			"type" => "checkbox",
			"std" => "1",
			"help" => "true", // should the help icon be displayed (not working yet, better add this to your settings)
			"help-desc"  => __( 'Enable/Disable Print button.', 'rfbwp-print' ), // text for the help tool tip
			"help-pos" => "top",
			"stack" => "begin"
		),
		array(
			"name" => __( "Menu Order", 'rfbwp-print' ), // this defines the heading of the option
			"desc" => __( "Menu order:", 'rfbwp-print' ), // this is the field/option description
			"desc-pos" => "top",
			"id"   => $rfbwp_shortname."_fb_nav_pt_order", // the id must be unique, it is used to call back the propertie inside the them
			"std"  => "6", // deafult value of the text
			"validation" => "numeric", /* Each text field can be specialy validated, if the text wont be using HTML tags you can choose here 'nohtml' ect. Choose Between: numeric, multinumeric, nohtml, url, email or dont set it for standard  validation*/
			"type" => "text-small"
		),
		array(
			"name" => __( "Icon", 'rfbwp-print' ), // this defines the heading of the option
			"desc" => __( "Icon:", 'rfbwp-print' ), // this is the field/option description
			"desc-pos" => "top",
			"id"   => $rfbwp_shortname."_fb_nav_pt_icon", // the id must be unique, it is used to call back the propertie inside the them
			"std"  => "fa fa-print", // deafult value of the text
			"type" => "icon",
			"stack" => "end",
			"validation" => ''
		)
	);
	
	return apply_filters( 'rfbwp/print/navOptions', array_merge( $options, $print_options ) );
}

function rfb_print_options( $options ) {
	global $rfbwp_shortname;
	
	$print_options = array(
		array(
			"name" => __( "Combine pages", 'rfbwp-print' ),
			"desc" => __( "Combine First & Last page: ", 'rfbwp-print' ),
			"desc-pos" => "top",
			"id" => $rfbwp_shortname."_fb_pt_combine",
			"std" => "1",
			"type" => "checkbox",
			"help" => "true", // should the help icon be displayed (not working yet, better add this to your settings)
			"help-desc"  => __( 'Enable this option to print First & Last page near to each other.', 'rfbwp-print' ), // text for the help tool tip
			"help-pos" => "top",
			"toggle" => "begin",
			"toggle-name" => __( "Print Settings", 'rfbwp-print' )
		),	
		array(
			"name" => __( "Hard Covers", 'rfbwp-print' ),
			"desc" => __( "Print Hard Covers?: ", 'rfbwp-print' ),
			"desc-pos" => "top",
			"id" => $rfbwp_shortname."_fb_pt_hc",
			"std" => "1",
			"type" => "checkbox",
			"help" => "true", // should the help icon be displayed (not working yet, better add this to your settings)
			"help-desc"  => __( 'Enable this option to add Hard Covers to print view.', 'rfbwp-print' ), // text for the help tool tip
			"help-pos" => "top",
		),	
		array(
			"name" => __( "User Choice", 'rfbwp-print' ),
			"desc" => __( "Enable User Choice: ", 'rfbwp-print' ),
			"desc-pos" => "top",
			"id" => $rfbwp_shortname."_fb_pt_choice",
			"std" => "1",
			"type" => "checkbox",
			"help" => "true", // should the help icon be displayed (not working yet, better add this to your settings)
			"help-desc"  => __( 'Enable this option to allow users select which page should be printed.', 'rfbwp-print' ), // text for the help tool tip
			"help-pos" => "top",
			"toggle" => "end",
		),
	);
		
	return apply_filters( 'rfbwp/print/options', array_merge( $options, $print_options ) );
}

/* 
 * Frontend
 */
add_action( 'init', 'rfb_print_init' );
function rfb_print_init() {	
	if( !defined( 'MPC_PLUGIN_FILE' ) )
		return;
	
	add_filter( 'rfbwp/navButtonsCount',	'rfb_print_registerButton', '', 2 );
	add_filter( 'rfbwp/navRender',			'rfb_print_renderButton', '', 4 );	
}

add_action( 'rfbwp/flipbook/scripts', 'rfb_print_enqueue_scripts' );
function rfb_print_enqueue_scripts() { 
	wp_enqueue_style( 'rfbwp-print', RFB_PRINT_PLUGIN_ROOT . 'css/print.min.css' );
	wp_enqueue_script( 'rfbwp-print', RFB_PRINT_PLUGIN_ROOT . 'js/print.min.js', array( 'jquery', 'flipbook-js' ) );
}

function rfb_print_registerButton( $buttonsCount, $data ) {
	if( isset( $data['rfbwp_fb_nav_pt'] ) && $data['rfbwp_fb_nav_pt'] == '1')
		$buttonsCount++;
		
	return $buttonsCount;
}

function rfb_print_renderButton( $buttons, $data, $order, $textMenu ) {
	if( isset( $data['rfbwp_fb_nav_pt'] ) && $data['rfbwp_fb_nav_pt'] == '1' && $data['rfbwp_fb_nav_pt_order'] == $order ) {
		$icons = $print_data = '';
		$icons .= 'data-icon="' . esc_attr( $data['rfbwp_fb_nav_pt_icon'] ) . '" ';

		$icons = $textMenu ? 'data-text="' .  __( 'print', 'rfbwp-print' ) . '"' : $icons ;

		if( isset( $data[ 'rfbwp_fb_pt_combine'] ) && $data[ 'rfbwp_fb_pt_combine' ] == '1' )
			$print_data .= ' data-combine-fl="1"';
		
		if( isset( $data[ 'rfbwp_fb_pt_hc'] ) && $data[ 'rfbwp_fb_pt_hc' ] == '1' )
			$print_data .= ' data-print-hc="1"';
		
		if( isset( $data[ 'rfbwp_fb_pt_choice'] ) && $data[ 'rfbwp_fb_pt_choice' ] == '1' )
			$print_data .= ' data-print-choice="1"';
				
		$buttons .= '<li class="print" ' .  $icons . $print_data . '></li>';
	}
	
	return $buttons;
}