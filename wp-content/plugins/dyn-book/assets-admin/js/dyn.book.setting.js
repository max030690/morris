(function($){
	
	
		
	// Load Preset 
	
	
})(jQuery);

( function () {
	jQuery(document).ready(function($) {
		
		//color
		$('.mp-color').wpColorPicker();
		
		 // Hide Show siblings on checkbox
		$('.checkbox-of-toggle').each( function() {
			$(this).nextAll().toggle(this.checked);
		});
		$('.checkbox-of-toggle').on('click',function(){
			$(this).nextAll().toggle(this.checked);
		});
		
		//toggle header 
		$(document).on('click', '.group.settings div.mp-toggle-header', function(e) {
			e.preventDefault();
			//var start = performance.now();
			var $this = $(this),
				$section = $this.next('div.mp-toggle-content');

			if($this.hasClass('open'))
				$this.removeClass('open');
			else
				$this.addClass('open');
			$this.next('div.mp-toggle-content').slideToggle('slow');
		});
		
		
		// Load Pre Style
		function rfbwp_import_preset( $section, preset ) {
				var i;

					info = [];
					info[0] = Math.floor((Math.random() * 1000) + 1);
					info[1] = Math.floor((Math.random() * 1000) + 1); 
					$.ajax({
						 method: "GET",
						 url: DYN_BOOK.presetsURL + preset + '.json',
						 data: {info:info},
						 dataType: 'json',
						 beforeSend: function(xhr){ 
							
						},
						success : function( data ) {
							data = data.preset;
							for( i = 0; i < data.length - 1; i++ ) {
								rfbwp_set_value( $section.find( '#' + data[ i ].field_id ), data[ i ].value, data[ i ].type );
							}
						},
						fail : function( data ) {
							
						}
					});
				
		}
		function rfbwp_set_value( $field, value, type ) {
				if( value == undefined )
					return;

				if( type == 'text' ) {
					$field.val( value );
				} else if( type == 'color') {
					$field.val( value ).trigger( 'change' );
				} else if( type == 'select' ) {
					$field.val( value ).trigger( 'change' );
				} else if( type == 'checkbox' ) {
					$field.prop( 'checked', value );
				} else if( type == 'icon' ) {
					$field.find( 'input' ).val( value );
					$field.find( '.mpc-icon-select i' ).attr( 'class', value );
					if(  value != '' )
						$field.find( '.mpc-icon-select' ).removeClass( 'mpc-icon-select-empty' );
				} else if( type == 'font-select' ) {
					$field.siblings( 'input' ).val( value ).trigger( 'change' );
				}
			}
		$('#rfbwp_fb_pre_style').on( 'change',function() {
				var $this = $( this ),
					$section = $this.parents( '.group.settings' ),
					preset = $this.val();
				
				if( preset !== '' && preset !== null ) {
					rfbwp_import_preset( $section, preset );
					
				}
		});
		
		
		
		/* ---------------------------------------------------------------- */
		/* Google Webfonts
		/* ---------------------------------------------------------------- */
		var googleFonts = '',
		googleFontsList = [];
		
		if(DYN_BOOK.googleFonts == false) {
			info = [];
			info[0] = Math.floor((Math.random() * 1000) + 1);
			info[1] = Math.floor((Math.random() * 1000) + 1); 
			var dt;
			$.ajax({
				 method: "GET",
				 url: 'https://www.googleapis.com/webfonts/v1/webfonts?callback=?&key=' + DYN_BOOK.googleAPIKey,
				 data: {info:info},
				 dataType: 'json',
				 beforeSend: function(xhr){ 
					
				},
				success : function( data ) {
					dt = data;
					if(data.error != undefined) {
						$('#mpcth_menu_font').after('<div class="mpcth-of-error">' + DYN_BOOK.googleAPIErrorMsg + '</div>');
					} else {
						
						
						rfbwpAddGoogleFonts(data);
					}
				},
				fail : function( data ) {
					
				},
				complete:function(){
					var googleFontsData = { items: [] };

					for(var i = 0; i < dt.items.length; i++) {
						googleFontsData.items[i] = {};
						googleFontsData.items[i].family = dt.items[i].family;
						googleFontsData.items[i].variants = dt.items[i].variants;
					}
					
					$.ajax({
						 method: "POST",
						 url: DYN_BOOK.ajaxurl,
						 data: {'action':'dyn_rfbwp_cache_google_webfonts','google_webfonts':JSON.stringify(googleFontsData),'info':info},
						 beforeSend: function(xhr){ 
							
						},
						success : function( data ) {
						
						},
						fail : function( data ) {
							
						},
						complete:function(){
							
						}
					});
					
				}
			});
			
		} else {
			rfbwpAddGoogleFonts(JSON.parse(DYN_BOOK.googleFonts));
		}
		
		function rfbwpAddGoogleFonts(data) {
			if(data.items != undefined) {
				var fontsCount = data.items.length;
				googleFontsList = data.items;

				googleFonts = '';
				for(var i = 0; i < fontsCount; i++) {
					var family = googleFontsList[i].family;

					googleFonts += '<option class="mpcth-option-google" data-index="' + i + '" value="' + family + '">' + family + '</option>';
				}
				$('.rfbwp-of-input-font').html(googleFonts);
			}
		}
		
		
		/* Icon Picker */
		function initIconPicker() {
			var $icons_modal = $( '#mpc_icon_select_grid_modal' );

			$('.mp-toggle-content').each( function() {
				var $elem = $( this );

				$elem.find( '.mpc-icon-select' ).each( function() {
					var $icon_wrap = $( this ),
						$icon_clear = $icon_wrap.siblings( '.mpc-icon-select-clear' ),
						$icon_val = $icon_wrap.siblings( '.mpc-icon-select-value' ),
						$icon = $icon_wrap.children( 'i' );

					$icon_wrap.on( 'click', function( event ) {
						if ( $icons_modal.length ) {
							
							$icons_modal.find('#mpc_icon_select_temp').val($(this).attr('id'));
							$icons_modal.dialog({
								maxHeight: 400,
								minWidth:800
							});
						}

						event.preventDefault();
					} );
				} );
			} );
			
			$( '#mpc_icon_select_grid_modal i').each( function() {
				$(this).on('click',function(){
					var temp_id;
					temp_id = $('#mpc_icon_select_grid_modal #mpc_icon_select_temp').val();
					$('#'+temp_id).find('i').attr('class',$(this).attr('class'));
					
					$('#'+temp_id.replace('_temp','')).val($(this).attr('class'));
					$icons_modal.dialog('close');
					
				});
			});
		}
		
		initIconPicker();

		
	});
	
	
})();