// Used for Shortcode [dyn-book-upload]
/*
Books Settings
*/


/*
Add Pages
*/
// Remove Field Reepater
function dyn_field_repeater_remove(sectionsSelector,removeClass,AYSMsg){
	(function($){
	 //hide show
	  $(document).on("click",removeClass,function(event) {
		  event.preventDefault();
		  
		  var theContainer = $(this).parents(sectionsSelector);
		  if(confirm(AYSMsg)){

				   // Making fade out, hide and remove element a sequence
				   // to provide a nice UX when removing element. 
		
				$(theContainer).fadeOut('normal', 
					  function( ){
						 $(theContainer).hide('fast', 
							function(){ $(theContainer).remove();  } 
						 );
					  } 
				   );
		   
		
		   }
	  });
	})(jQuery);
}
dyn_field_repeater_remove('.group-dbu-repeated-sections','.dbu-remove','Are you sure you want to remove this section?');
	
// Hide Show Repeater Field
function dyn_field_repeater_hideshow(sectionsSelector,idleShowHide,classToggle){
    (function($){
	 //hide show
	  $(document).on("click",idleShowHide,function(event) {
		  event.preventDefault();
		  isVisible = $(this).siblings(classToggle).is(":visible"); 
		  $(sectionsSelector).find(classToggle).hide();
		  if (isVisible) {
			$(this).siblings(classToggle).show();
		  } else {
			$(this).siblings(classToggle).hide();
		  }
		  $(this).siblings(classToggle).toggle();
	  });
	})(jQuery);
}

dyn_field_repeater_hideshow('.group-dbu-repeated-sections','.dbu-heading-show-hide','.dbu-repeated-item');

//fill input when images are not selected yet
function dyn_field_input_image_temp_reset(){
	(function($) {
		$('.dynbookimage-attachment-list-temp').html('');
	})(jQuery);
}
function dyn_field_input_image_temp() {
	(function($) {
		
		$(".dynbookimage-fields").each(function() {
			var temp = $(this).find('.dynbookimage-attachment-list > li').length;
			if (temp==0) {
				var myTemp = $(this).find('.dynbookimage-attachment-list-temp');
				myTemp.html('<input type="hidden" name="'+ myTemp.data('field-1') +'" value=""><input type="hidden" name="'+ myTemp.data('field-2') +'" value="">'
				);
			}
		});

	})(jQuery);
}



(function($) {
	
	// HTML Editor
	function initHTMLeditor( ) {
		
		tinymce.init({ selector:'.html-editor',
					mode: 'exact',
					relative_urls: false,
					skin: 'mpc-flipbook',
					height: '100',
					force_br_newlines: true,
					force_p_newlines: false,
					entity_encoding: 'raw',
					verify_html: false,
					plugins: [
						"autolink lists link image hr anchor",
						"code media nonbreaking paste textcolor colorpicker textpattern"
					],
					image_advtab: true,
					menubar : false,
					toolbar1: "undo redo | styleselect | bold italic | forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media | code", });


	}
	//initHTMLeditor();
	
	// Add Field Repeater Section using ajax
	$('.dbu-add-item').on("click", function(event){
         // Avoiding the link to do the default behavior.
        event.preventDefault();
		console.log("dyn_book_section_repeater");
		
		// hide all repeater
		$('.dbu-repeated-item').hide();
		
		var stuff ={'action':'dyn_book_section_repeater'}; 
		 
		$.ajax({
			 method: "POST",
			 url: DYN_BOOK.ajaxurl,
			 data: stuff,
			 beforeSend: function(xhr){ 
				
			},
			success : function( response ) {
				console.log( response );
				if ($('.group-dbu-repeated-sections:last').length>0) {
					$(response).insertAfter('.group-dbu-repeated-sections:last');
				} else {
					$(response).insertAfter('#group-dbu');
				}
				//tinymce.remove();
				//initHTMLeditor();
			},
			fail : function( response ) {
				console.log( response );
				alert(response);
			}
		});
		
	});
	
	$(document).on("click",'.dbu-add', function(event){
		event.preventDefault();
		$('.dbu-add-item').trigger( "click" );
	});
	
	// Submit Form CREATE
	$( '#dny-book-form-create' ).on( 'submit', function(e) {
        e.preventDefault();
		var data = $('#dny-book-form-create').serialize();
		$.ajax({
			 method: "POST",
			 url: DYN_BOOK.ajaxurl,
			 data: data,
			 dataType: 'json',
			 beforeSend: function(xhr){ 
				dyn_field_input_image_temp_reset();
				dyn_field_input_image_temp();
				$('#dny-book-form-create').hide();
				//$('#dny-book-form-create-message').html('<span> Book Uploading is processing ....</span>');
				//$('#dny-book-form-create-message').show();
			},
			success : function( response ) {
				console.log( response );
				if (response.success) {
					//$('#dny-book-form-create-message').html(response.message);
					window.location = response.redirect_to;
					 //alert(response.succes);
				} else {
					$('#dny-book-form-create-message').html('There is server error');
				}
				$('#dny-book-form-create').show();
			},
			fail : function( response ) {
				console.log( response );
				$('#dny-book-form-create-message').html('Book is failed to be created');
				$('#dny-book-form-create').show();
			}
		});
	});
	// Submit Form CREATE Ends
	
	
	// Submit Form EDIT
	$( '#dny-book-form-edit' ).on( 'submit', function(e) {
        e.preventDefault();
		var data = $('#dny-book-form-edit').serialize();
		$.ajax({
			 method: "POST",
			 url: DYN_BOOK.ajaxurl,
			 data: data,
			 dataType: 'json',
			 beforeSend: function(xhr){ 
				dyn_field_input_image_temp_reset();
				dyn_field_input_image_temp();
				$('#dny-book-form-create').hide();
				//$('#dny-book-form-create-message').html('<span> Book Uploading is processing ....</span>');
				//$('#dny-book-form-create-message').show();
			},
			success : function( response ) {
				console.log( response );
				if (response.success) {
					//$('#dny-book-form-create-message').html(response.message);
					window.location = response.redirect_to;
					 //alert(response.succes);
				} else {
					$('#dny-book-form-create-message').html('There is server error');
				}
				$('#dny-book-form-create').show();
			},
			fail : function( response ) {
				console.log( response );
				$('#dny-book-form-create-message').html('Book is failed to be created');
				$('#dny-book-form-create').show();
			}
		});
	});
	// Submit Form EDIT Ends
	
	
	
	
})(jQuery);