<?php
	$post_id = $_REQUEST['post_id'];
?>
<div id="dny-book-form-create-message" class="alert alert-success">
	<span>Book Uploaded successfully. <a href="">View File</a></span>
</div>
<form id="dny-book-form-edit" method="post">	
	<input type="hidden" name="post_id" value="<?php echo $post_id; ?>" />
	<div class="form-group">
		<label>Title </label>
		<div>
			<input class="form-control" type="text" name="post-title" value="<?php echo get_the_title($post_id);  ?>" required>
			<input class="form-control" type="hidden" name="profile_id" value="<?php  echo get_current_user_id();  ?>">
			
		</div>
	</div>
	<!--
	<div>
		<a href="#" class="dbu-add-item">ADD PAGES</a>
	</div>
	<div id="group-dbu">		
		<?php
		$path_file = DYN_BOOK_PLUGIN_DIR . 'inc/form-upload/field-repeater.php';
		//require_once($path_file);
		?>
	</div>
	-->
	<?php
		$path_file = DYN_BOOK_PLUGIN_DIR . 'inc/form-upload/field-setting.php';
		require_once($path_file);
	?>
	<div class="form-group">
		<label for="dyn-select-description">Description:</label>
		<textarea name="dyn-select-description" id="dyn-select-description" class="form-control" rows="3"><?php $my_postid = $post_id;$content_post = get_post($my_postid);echo $content = $content_post->post_content;?></textarea>
	</div>
	<div class="form-group">
		<label>Reference:</label>
		<textarea class="form-control" rows="2" class="form-control" name="dyn-select-reference"><?php echo get_post_meta($post_id,'video_options_refr',true);?></textarea>
	</div>
	<div class="form-group">
		<label for="dyn-tags">Tags:</label>
       
		<input type="text" class="dyn-select-tags" name="dyn-select-tags" value="<?php
$posttags = get_the_tags($post_id);
if ($posttags) {
  foreach($posttags as $tag) {
    echo $tag->name . ' ';
  }
}
?>" />
		<span>Enter tags separated by space</span>
	</div>
	<div class="form-group">
		<label for="dyn-tags">Add Background:</label>					
		<div class="checkbox">
			<label>White Background 
			<input type="radio" checked="checked" value="background_no" class="dyn_checkbox" name="dyn_background"></label>
			<label>Profile Background 
			<input type="radio" value="background_user" class="dyn_checkbox" name="dyn_background"></label>
			<label>New Background 
			<input type="radio" value="background_new" class="dyn_checkbox" name="dyn_background"></label>
		</div>
	</div>
	<div id="upload_container">
		<div class="form-group">
		<label for="dyn_bg">Upload File:</label>
			<?php
				$random = rand(1,100000);
			?>

			<div class="dynbookimage-fields">
				<div id="dynbookimage-featured_image-upload-container-<?php echo $random; ?>">
					<div class="dynbookimage-attachment-upload-filelist-<?php echo $random; ?>" data-type="file" data-required="yes">
						<a id="dynbookimage-featured_image-pickfiles-<?php echo $random; ?>" data-form_id="18" class="button file-selector  dynbookimage_featured_image_18" href="#">Select Image</a>

						<ul class="dynbookimage-attachment-list thumbnails">
						</ul>
						<div class="dynbookimage-attachment-list-temp" data-field-1="dyn_bg" data-field-2="dyn_bg_id">
						</div>
					</div>
				</div><!-- .container -->

				<span class="dynbookimage-help"></span>

			</div> <!-- .dynbookimage-fields -->

        <script type="text/javascript">
            jQuery(function($) {
                new DYNIMAGE_Uploader('dynbookimage-featured_image-pickfiles-<?php echo $random; ?>', 'dynbookimage-featured_image-upload-container-<?php echo $random; ?>', 1, 'featured_image', 'jpg,jpeg,gif,png,bmp', 1024,'dyn_bg','dyn_bg_id');
            });
        </script>
			
			
		</div>
	</div>
	
	<script type="text/javascript">
		jQuery(document).ready(function() {
			
			jQuery("#upload_container").hide();
			jQuery(".dyn_checkbox").click( function() {
				var test = jQuery(this).val();
				if( test == 'background_new' ){
					jQuery("#upload_container").show();
				}else{
					jQuery("#upload_container").hide();
				}
			});
		});
	</script>
	<div>
		<input type="hidden" name="action" value="dyn_book_edit_post" />
       <?php if ( get_post_status($post_id) == 'draft' ) { ?>
		<input type="submit" value="PREVIEW">
        <?php } else { ?>
			<input type="submit" value="SAVE BOOK">
		<?php } ?>
	</div>
</form>
<?php
		$path_file = DYN_BOOK_PLUGIN_DIR . 'inc/form-upload/panel/mpc_icon/icon_select/icon_grid.php';
		require_once($path_file);
?>