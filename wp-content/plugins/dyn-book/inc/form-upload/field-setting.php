<h4>Books Settings</h4>
<div id="dyn-book-group-setting">
<div class="group settings" id="mp-option-settings_0">
<?php
$rfbwp_shortname = 'rfbwp';

/*  Main Start */
/*  -------------------------------------------------------------------------------------------------------- */
$options[] = array(
		'type' => 'heading',
		'name' => 'Main'
	);

$options[] = array(
		'type' => 'sub-start'
);
	$options[] = array(
		"name" => __( "Name", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Book Name: ", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"help" => "true", // should the help icon be displayed (not working yet, better add this to your settings)
		"help-desc"  => __( 'Flip book name is used to generate a unique shortcode for the book (NOTE: Please use only laters, numbers and spaces), width and height are used to specify books dimantions.', 'rfbwp' ), // text for the help tool tip
		"help-pos" => "bottom",
		"id"   => $rfbwp_shortname."_fb_name", // the id must be unique, it is used to call back the propertie inside the theme
		"std"  => "", // deafult value of the text
		"validation" => "nohtml", /* Each text field can be specialy validated, if the text wont be using HTML tags you can choose here 'nohtml' ect. Choose Between: numeric, multinumeric, nohtml, url, email or dont set it for standard  validation*/
		"type" => "text-medium",
		"toggle" => "begin",
		"toggle-name" => __( "Main", 'rfbwp' )
	);

	$options[] = array(
		"name" => __( "Page Width:", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Page Width:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_width", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "960", // deafult value of the text
		"validation" => "numeric", /* Each text field can be specialy validated, if the text wont be using HTML tags you can choose here 'nohtml' ect. Choose Between: numeric, multinumeric, nohtml, url, email or dont set it for standard  validation*/
		"type" => "text-small",
		"unit" => __( "px", 'rfbwp' ),
		"stack" => "begin"
	);

	$options[] = array(
		"name" => __( "Book Height:", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Page Height:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_height", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "600", // deafult value of the text
		"validation" => "numeric", /* Each text field can be specialy validated, if the text wont be using HTML tags you can choose here 'nohtml' ect. Choose Between: numeric, multinumeric, nohtml, url, email or dont set it for standard  validation*/
		"type" => "text-small",
		"unit" => __( "px", 'rfbwp' ),
		"stack" => "end"
	);

	$options[] = array(
		'name'        => __( 'Always open', 'rfbwp' ),
		'desc'        => __( 'Always open:', 'rfbwp' ),
		'desc-pos'    => 'top',
		'id'          => $rfbwp_shortname . '_fb_force_open',
		'std'         => '0',
		'type'        => 'checkbox',
		'help'        => 'true',
		'help-desc'   => __( 'Select this option to prevent the book from closing.', 'rfbwp' ),
		'help-pos'    => 'top',
	);

	$options[] = array(
		'name'        => __( 'Enable Page Turn Sound', 'rfbwp' ),
		'desc'        => __( 'Enable Page Turn Sound:', 'rfbwp' ),
		'desc-pos'    => 'top',
		'id'          => $rfbwp_shortname . '_fb_enable_sound',
		'std'         => '0',
		'type'        => 'checkbox',
		'help'        => 'true',
		'help-desc'   => __( 'Select this option to enable page turn sound effect.', 'rfbwp' ),
		'help-pos'    => 'top',
	);

	$options[] = array(
		'name'        => __( 'Display in RTL', 'rfbwp' ),
		'desc'        => __( 'Display in RTL:', 'rfbwp' ),
		'desc-pos'    => 'top',
		'id'          => $rfbwp_shortname . '_fb_is_rtl',
		'std'         => '0',
		'type'        => 'checkbox',
		'help'        => 'true',
		'help-desc'   => __( 'Select this option to enable RTL display.', 'rfbwp' ),
		'help-pos'    => 'top',
	);

//	$options[] = array(
//		'name'        => __( 'Generate Preset', 'rfbwp' ),
//		'desc'        => __( 'Generate Preset:', 'rfbwp' ),
//		'desc-pos'    => 'top',
//		'id'          => $rfbwp_shortname . '_fb_down_preset',
//		'std'         => '0',
//		'type'        => 'checkbox',
//		'class'		  => 'export_preset',
//		'help'        => 'true',
//		'help-desc'   => __( 'Click to download preset file.', 'rfbwp' ),
//		'help-pos'    => 'top',
//	);

	$options[] = array(
		"name" => __( "Load Predefined Book Style", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Load Predefined Book Style:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_pre_style", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "1", // deafult value of the text
		"type" => "select",
		"options" => array(
			"" => "",
			"alice" => __( "Alice", 'rfbwp' ),
			"brochure" => __( "Brochure", 'rfbwp' ),
			"catalogue" => __( "Catalogue", 'rfbwp' ),
			"comic_book" => __( "Comic Book", 'rfbwp' ),
			"company_brochure" => __( "Company Brochure", 'rfbwp' ),
			"flyer" => __( "Flyer", 'rfbwp' ),
			"lookbook" => __( "Lookbook", 'rfbwp' ),
			"magazine" => __( "Magazine", 'rfbwp' ),
			"main_preview" => __( "Main Preview", 'rfbwp' ),
			"menu" => __( "Menu", 'rfbwp' ),
			"portfolio" => __( "Portfolio", 'rfbwp' )
		),
		"help" => "true", // should the help icon be displayed (not working yet, better add this to your settings)
		"help-desc"  => __( 'Select predefined style for your Book. You can modify each of the settings after.', 'rfbwp' ), // text for the help tool tip
		"help-pos" => "top",
		'toggle'      => 'end',
	);

$options[] = array(
		'type' => 'sub-end'
);	


/*  Decoration Start */
/*  -------------------------------------------------------------------------------------------------------- */
$options[] = array(
		'type' => 'heading',
		'name' => 'Decoration'
	);

$options[] = array(
		'type' => 'sub-start'
);	

$options[] = array(
		"name" => __( "Border size", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Border size:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_border_size", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "0", // deafult value of the text
		"validation" => "numeric", /* Each text field can be specialy validated, if the text wont be using HTML tags you can choose here 'nohtml' ect. Choose Between: numeric, multinumeric, nohtml, url, email or dont set it for standard  validation*/
		"type" => "text-small",
		"unit" => __( "px", 'rfbwp' ),
		"help" => "true", // should the help icon be displayed (not working yet, better add this to your settings)
		"help-desc"  => __( 'Set up books border, it\'s color and radius.','rfbwp'), // text for the help tool tip
		"help-pos" => "top",
		"stack" => "begin",
		"toggle" => "begin",
		"toggle-name" => __( "Decorations", 'rfbwp' )
	);

	$options[] = array(
		"name" => __( "Border color:", 'rfbwp' ),
		"desc" => __( "Border color: ", 'rfbwp' ),
		"desc-pos" => "top",
		"id" => $rfbwp_shortname."_fb_border_color",
		"std" => "",
		"type" => "color",
		"validation" => ''
	);

	$options[] = array(
		"name" => __( "Book Border Radius", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Border radius: ", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_border_radius", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "0", // deafult value of the text
		"validation" => "numeric", /* Each text field can be specialy validated, if the text wont be using HTML tags you can choose here 'nohtml' ect. Choose Between: numeric, multinumeric, nohtml, url, email or dont set it for standard  validation*/
		"unit" => __( "px", 'rfbwp' ),
		"type" => "text-small",
		"stack" => "end"
	);

	$options[] = array(
		"name" => __( "Book Outline", 'rfbwp' ),
		"desc" => __( "Book outline: ", 'rfbwp' ),
		"desc-pos" => "top",
		"id" => $rfbwp_shortname."_fb_outline",
		"std" => "0",
		"type" => "checkbox",
		"help" => "true", // should the help icon be displayed (not working yet, better add this to your settings)
		"help-desc"  => __( 'Set up books outline - line displayed outside the border.', 'rfbwp' ), // text for the help tool tip
		"help-pos" => "top",
		"stack" => "begin"
	);

	$options[] = array(
		"name" => __( "Outline Color", 'rfbwp' ),
		"desc" => __( "Outline color:", 'rfbwp' ),
		"desc-pos" => "top",
		"id" => $rfbwp_shortname."_fb_outline_color",
		"std" => "",
		"type" => "color",
		"stack" => "end",
		"validation" => ''
	);

	$options[] = array(
		"name" => __( "Inner Page Shadows", 'rfbwp' ),
		"desc" => __( "Inner page shadow", 'rfbwp' ),
		"desc-pos" => "top",
		"id" => $rfbwp_shortname."_fb_inner_shadows",
		"std" => "1",
		"help" => "true", // should the help icon be displayed (not working yet, better add this to your settings)
		"help-desc"  => __( 'Enable/Disable book inner shadow', 'rfbwp' ), // text for the help tool tip
		"help-pos" => "top",
		"type" => "checkbox"
	);

	$options[] = array(
		"name" => __( "Edge Page Outline", 'rfbwp' ),
		"desc" => __( "Edge page outline", 'rfbwp' ),
		"desc-pos" => "top",
		"id" => $rfbwp_shortname."_fb_edge_outline",
		"std" => "0",
		"help" => "true", // should the help icon be displayed (not working yet, better add this to your settings)
		"help-desc"  => __( 'Enable/Disable page edge outline - it is used to create 3D like feeling. If you don\'t have border around your book we suggest disabling this feature.', 'rfbwp' ), // text for the help tool tip
		"help-pos" => "top",
		"type" => "checkbox",
		"stack" => "begin",
	);

	$options[] = array(
		"name" => __( "Outline Color", 'rfbwp' ),
		"desc" => __( "Outline color:", 'rfbwp' ),
		"desc-pos" => "top",
		"id" => $rfbwp_shortname."_fb_edge_outline_color",
		"std" => "",
		"type" => "color",
		"stack" => "end",
		"validation" => ''
	);

	$options[] = array(
		"name" => __( "Fullscreen", 'rfbwp' ),
		"desc" => __( "Fullscreen Overlay", 'rfbwp' ),
		"desc-pos" => "top",
		"id" => $rfbwp_shortname."_fb_fs_color",
		"std" => "1",
		"help" => "true", // should the help icon be displayed (not working yet, better add this to your settings)
		"help-desc"  => __( 'Specify the fullscreen overlay opacity, color and color of the \'X\' (close) icon.', 'rfbwp' ), // text for the help tool tip
		"help-pos" => "top",
		"std" => "#ededed",
		"type" => "color",
		"stack" => "begin",
		"validation" => ''
	);

	$options[] = array(
		"name" => __( "Opacity", 'rfbwp' ),
		"desc" => __( "Opacity:", 'rfbwp' ),
		"desc-pos" => "top",
		"id" => $rfbwp_shortname."_fb_fs_opacity",
		"std" => "95",
		"type" => "text-small",
		"validation" => ''
	);

	$options[] = array(
		"name" => __( "Reverse 'X' Color", 'rfbwp' ),
		"desc" => __( "Reverse 'X' Color", 'rfbwp' ),
		"desc-pos" => "top",
		"id" => $rfbwp_shortname."_fb_fs_icon_color",
		"std" => "1",
		"type" => "checkbox",
		"stack" => "end",
		"validation" => ''
	);

	$options[] = array(
		"name" => __( "New Table of Content style", 'rfbwp' ),
		"desc" => __( "New Table of Content style", 'rfbwp' ),
		"desc-pos" => "top",
		"id" => $rfbwp_shortname."_fb_toc_display_style",
		"std" => "0",
		"help" => "true", // should the help icon be displayed (not working yet, better add this to your settings)
		"help-desc"  => __( 'Enable this option to use new Table of Content style.', 'rfbwp' ), // text for the help tool tip
		"help-pos" => "top",
		"type" => "checkbox",
		"toggle" => "end",
	);
$options[] = array(
		'type' => 'sub-end'
);	

	/*-----------------------------------------------------------------------------------*/
	/*	Fonts Settings
	/*-----------------------------------------------------------------------------------*/

$options[] = array(
		'type' => 'heading',
		'name' => 'Font Settings'
	);

$options[] = array(
		'type' => 'sub-start'
);	

	$options[] = array(
		"name" => __( "Heading Font", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Heading Font:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_heading_font", // the id must be unique, it is used to call back the propertie inside the them
		"type" => "checkbox",
		"std" => "1",
		"help" => "true", // should the help icon be displayed (not working yet, better add this to your settings)
		"help-desc"  => __( 'Specify the font size, font family for headings.', 'rfbwp' ), // text for the help tool tip
		"help-pos" => "top",
		"stack" => "begin",
		"toggle" => "begin",
		"toggle-name" => __( "Fonts Settings", 'rfbwp' )
	);

	$options[] = array(
		"name" => __( "Family", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Family:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_heading_family", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "Open Sans", // deafult value of the text
		"type" => "font_select",
		"validation" => ''
	);

	$options[] = array(
		"name" => __( "Style", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Style:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_heading_fontstyle", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "regular", // deafult value of the text
		"type" => "select",
		"validation" => '',
		"options" => array( 'regular' => __( 'Regular', 'rfbwp'), 'bold' => __( 'Bold', 'rfbwp'), 'italic' => __( 'Italic', 'rfbwp'), 'bold-italic' => __( 'Bold Italic', 'rfbwp'), )
	);

	$options[] = array(
		"name" => __( "Size", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Size:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_heading_size", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "24", // deafult value of the text
		"type" => "text-small",
		"unit" => __( "px", 'rfbwp' ),
		"validation" => ''
	);

	$options[] = array(
		"name" => __( "Line Height:", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Line Height:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_heading_line", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "", // deafult value of the text
		"type" => "text-small",
		"unit" => __( "px", 'rfbwp' ),
		"validation" => ''
	);

	$options[] = array(
		"name" => __( "Color", 'rfbwp' ),
		"desc" => __( "Color:", 'rfbwp' ),
		"desc-pos" => "top",
		"id" => $rfbwp_shortname."_fb_heading_color",
		"std" => "#2b2b2b",
		"type" => "color",
		"stack" => "end",
		"validation" => ''
	);

	$options[] = array(
		"name" => __( "Content Font", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Content Font:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_content_font", // the id must be unique, it is used to call back the propertie inside the them
		"type" => "checkbox",
		"std" => "1",
		"help" => "true", // should the help icon be displayed (not working yet, better add this to your settings)
		"help-desc"  => __( 'Specify the font size, font family for content.', 'rfbwp' ), // text for the help tool tip
		"help-pos" => "top",
		"stack" => "begin",
	);

	$options[] = array(
		"name" => __( "Family", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Family:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_content_family", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "Open Sans", // deafult value of the text
		"type" => "font_select",
		"unit" => __( "px", 'rfbwp' ),
		"validation" => ''
	);

	$options[] = array(
		"name" => __( "Style", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Style:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_content_fontstyle", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "regular", // deafult value of the text
		"type" => "select",
		"validation" => '',
		"options" => array( 'regular' => __( 'Regular', 'rfbwp'), 'bold' => __( 'Bold', 'rfbwp'), 'italic' => __( 'Italic', 'rfbwp'), 'bold-italic' => __( 'Bold Italic', 'rfbwp'), )
	);

	$options[] = array(
		"name" => __( "Size", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Size:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_content_size", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "15", // deafult value of the text
		"type" => "text-small",
		"unit" => __( "px", 'rfbwp' ) ,
		"validation" => ''
	);

	$options[] = array(
		"name" => __( "Line Height:", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Line Height:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_content_line", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "25", // deafult value of the text
		"type" => "text-small",
		"unit" => __( "px", 'rfbwp' ),
		"validation" => ''
	);

	$options[] = array(
		"name" => __( "Color", 'rfbwp' ),
		"desc" => __( "Color:", 'rfbwp' ),
		"desc-pos" => "top",
		"id" => $rfbwp_shortname."_fb_content_color",
		"std" => "#2b2b2b",
		"type" => "color",
		"stack" => "end",
		"validation" => ''
	);

	$options[] = array(
		"name" => __( "Pagination Font", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Pagination Font:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_num_font", // the id must be unique, it is used to call back the propertie inside the them
		"type" => "checkbox",
		"std" => "0",
		"help" => "true", // should the help icon be displayed (not working yet, better add this to your settings)
		"help-desc"  => __( 'Specify the font size, font family for pagination.', 'rfbwp' ), // text for the help tool tip
		"help-pos" => "top",
		"stack" => "begin"
	);

	$options[] = array(
		"name" => __( "Family", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Family:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_num_family", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "Open Sans", // deafult value of the text
		"type" => "font_select",
		"validation" => ''
	);

	$options[] = array(
		"name" => __( "Style", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Style:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_num_fontstyle", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "", // deafult value of the text
		"type" => "select",
		"validation" => '',
		"options" => array( 'regular' => __( 'Regular', 'rfbwp'), 'bold' => __( 'Bold', 'rfbwp'), 'italic' => __( 'Italic', 'rfbwp'), 'bold-italic' => __( 'Bold Italic', 'rfbwp'), )
	);

	$options[] = array(
		"name" => __( "Size", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Size:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_num_size", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "", // deafult value of the text
		"type" => "text-small",
		"unit" => __( "px", 'rfbwp' ) ,
		"validation" => ''
	);

	$options[] = array(
		"name" => __( "Line Height:", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Line Height:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_num_line", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "", // deafult value of the text
		"type" => "text-small",
		"unit" => __( "px", 'rfbwp' ),
		"validation" => ''
	);

	$options[] = array(
		"name" => __( "Color", 'rfbwp' ),
		"desc" => __( "Color:", 'rfbwp' ),
		"desc-pos" => "top",
		"id" => $rfbwp_shortname."_fb_num_color",
		"std" => "",
		"type" => "color",
		"stack" => "end",
		"validation" => ''
	);

	$options[] = array(
		"name" => __( "Table of Contents Font", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Table of Contents Font:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_toc_font", // the id must be unique, it is used to call back the propertie inside the them
		"type" => "checkbox",
		"std" => "0",
		"help" => "true", // should the help icon be displayed (not working yet, better add this to your settings)
		"help-desc"  => __( 'Specify the font size, font family for Table of Contents.', 'rfbwp' ), // text for the help tool tip
		"help-pos" => "top",
		"stack" => "begin"
	);

	$options[] = array(
		"name" => __( "Family", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Family:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_toc_family", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "Open Sans", // deafult value of the text
		"type" => "font_select",
		"validation" => ''
	);

	$options[] = array(
		"name" => __( "Style", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Style:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_toc_fontstyle", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "", // deafult value of the text
		"type" => "select",
		"validation" => '',
		"options" => array( 'regular' => __( 'Regular', 'rfbwp'), 'bold' => __( 'Bold', 'rfbwp'), 'italic' => __( 'Italic', 'rfbwp'), 'bold-italic' => __( 'Bold Italic', 'rfbwp'), )
	);

	$options[] = array(
		"name" => __( "Size", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Size:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_toc_size", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "", // deafult value of the text
		"type" => "text-small",
		"unit" => __( "px", 'rfbwp' ) ,
		"validation" => ''
	);

	$options[] = array(
		"name" => __( "Line Height:", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Line Height:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_toc_line", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "", // deafult value of the text
		"type" => "text-small",
		"unit" => __( "px", 'rfbwp' ),
		"validation" => ''
	);

	$options[] = array(
		"name" => __( "Color", 'rfbwp' ),
		"desc" => __( "Color:", 'rfbwp' ),
		"desc-pos" => "top",
		"id" => $rfbwp_shortname."_fb_toc_color",
		"std" => "",
		"type" => "color",
		"validation" => ''
	);

	$options[] = array(
		"name" => __( "Hover", 'rfbwp' ),
		"desc" => __( "Hover:", 'rfbwp' ),
		"desc-pos" => "top",
		"id" => $rfbwp_shortname."_fb_toc_colorhover",
		"std" => "",
		"type" => "color",
		"stack" => "end",
		"toggle" => "end",
		"validation" => ''
	);

$options[] = array(
		'type' => 'sub-end'
);	

	/*-----------------------------------------------------------------------------------*/
	/*	Zoom Settings
	/*-----------------------------------------------------------------------------------*/
	
$options[] = array(
		'type' => 'heading',
		'name' => 'Zoom Settings'
	);

$options[] = array(
		'type' => 'sub-start'
);	

$options[] = array(
		"name" => __( "Zoom Border Size", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Border size: ", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_zoom_border_size", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "10", // deafult value of the text
		"validation" => "numeric", /* Each text field can be specialy validated, if the text wont be using HTML tags you can choose here 'nohtml' ect. Choose Between: numeric, multinumeric, nohtml, url, email or dont set it for standard  validation*/
		"type" => "text-small",
		"unit" => __( "px", 'rfbwp' ),
		"help" => "true", // should the help icon be displayed (not working yet, better add this to your settings)
		"help-desc"  => __( 'Specify parameters for the border that is displayed around the zoomed page.', 'rfbwp' ), // text for the help tool tip
		"help-pos" => "top",
		"stack" => "begin",
		"toggle" => "begin",
		"toggle-name" => __( "Zoom Settings", 'rfbwp' )
	);

	$options[] = array(
		"name" => __( "Zoom Border Color", 'rfbwp' ),
		"desc" => __( "Border color:", 'rfbwp' ),
		"desc-pos" => "top",
		"id" => $rfbwp_shortname."_fb_zoom_border_color",
		"std" => "#ECECEC",
		"type" => "color",
		"validation" => ''
	);

	$options[] = array(
		"name" => __( "Zoom Border Radius", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Border radius:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_zoom_border_radius", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "10", // deafult value of the text
		"validation" => "numeric", /* Each text field can be specialy validated, if the text wont be using HTML tags you can choose here 'nohtml' ect. Choose Between: numeric, multinumeric, nohtml, url, email or dont set it for standard  validation*/
		"type" => "text-small",
		"unit" => __( "px", 'rfbwp' ),
		"stack" => "end"
	);

	$options[] = array(
		"name" => __( "Zoom Outline", 'rfbwp' ),
		"desc" => __( "Outline: ", 'rfbwp' ),
		"desc-pos" => "top",
		"id" => $rfbwp_shortname."_fb_zoom_outline",
		"std" => "1",
		"type" => "checkbox",
		"help" => "true", // should the help icon be displayed (not working yet, better add this to your settings)
		"help-desc"  => __( 'Enable/Disable zoom panel outline, specify it\'s color.', 'rfbwp' ), // text for the help tool tip
		"help-pos" => "top",
		"stack" => "begin"
	);

	$options[] = array(
		"name" => __( "Zoom Outline Color", 'rfbwp' ),
		"desc" => __( "Outline color: ", 'rfbwp' ),
		"desc-pos" => "top",
		"id" => $rfbwp_shortname."_fb_zoom_outline_color",
		"std" => "#D0D0D0",
		"type" => "color",
		"stack" => "end",
		"validation" => ''
	);

	$options[] = array(
		"name" => __( "Force Mobile Zoom", 'rfbwp' ),
		"desc" => __( "Force Mobile Zoom: ", 'rfbwp' ),
		"desc-pos" => "top",
		"id" => $rfbwp_shortname."_fb_zoom_force",
		"std" => "1",
		"type" => "checkbox",
		"toggle" => "end",
		"help" => "true", // should the help icon be displayed (not working yet, better add this to your settings)
		"help-desc"  => __( 'Enable/Disable forced zoom on mobile. Be default mobile zoom will display FlipBook in it\'s default size or double size for small books. By enabling forced zoom it will always display in double size.', 'rfbwp' ), // text for the help tool tip
		"help-pos" => "top",
	);


$options[] = array(
		'type' => 'sub-end'
);	

	/*-----------------------------------------------------------------------------------*/
	/*	Show All Pages
	/*-----------------------------------------------------------------------------------*/
	
$options[] = array(
	'type' => 'heading',
	'name' => 'Show All Pages'
);

$options[] = array(
		'type' => 'sub-start'
);	

$options[] = array(
		"name" => __( "Thumbnail Columns", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Thumbnail Columns:"), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_sa_thumb_cols", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "3", // deafult value of the text
		"validation" => "numeric", /* Each text field can be specialy validated, if the text wont be using HTML tags you can choose here 'nohtml' ect. Choose Between: numeric, multinumeric, nohtml, url, email or dont set it for standard  validation*/
		"type" => "text-small",
		"help" => "true", // should the help icon be displayed (not working yet, better add this to your settings)
		"help-desc"  => __( 'Show all pages panel contains thumbnails of each page, here you can specify number of columns of those thumbnails.', 'rfbwp' ), // text for the help tool tip
		"help-pos" => "top",
		"toggle" => "begin",
		"toggle-name" => __( "Show all pages settings", 'rfbwp' )
	);

	$options[] = array(
		"name" => __( "Thumbnail Border Size", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Thumbnail border size:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_sa_thumb_border_size", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "1", // deafult value of the text
		"validation" => "numeric", /* Each text field can be specialy validated, if the text wont be using HTML tags you can choose here 'nohtml' ect. Choose Between: numeric, multinumeric, nohtml, url, email or dont set it for standard  validation*/
		"type" => "text-small",
		"unit" => __( "px", 'rfbwp' ),
		"help" => "true", // should the help icon be displayed (not working yet, better add this to your settings)
		"help-desc"  => __( 'Border Size - displayed around all thumbnails.', 'rfbwp' ), // text for the help tool tip
		"help-pos" => "top",
		"stack" => "begin"
	);

	$options[] = array(
		"name" => __( "Thumbnail Border Color", 'rfbwp' ),
		"desc" => __( "Border color:", 'rfbwp' ),
		"desc-pos" => "top",
		"id" => $rfbwp_shortname."_fb_sa_thumb_border_color",
		"std" => "#878787",
		"type" => "color",
		"stack" => "end",
		"validation" => ''
	);

	$options[] = array(
		"name" => __( "Vertical Padding", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Vertical padding: ", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_sa_vertical_padding", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "10", // deafult value of the text
		"validation" => "numeric", /* Each text field can be specialy validated, if the text wont be using HTML tags you can choose here 'nohtml' ect. Choose Between: numeric, multinumeric, nohtml, url, email or dont set it for standard  validation*/
		"type" => "text-small",
		"unit" => __( "px", 'rfbwp' ),
		"help" => "true", // should the help icon be displayed (not working yet, better add this to your settings)
		"help-desc"  => __( 'Specify thumbnails vertical and horizontal padding.', 'rfbwp' ), // text for the help tool tip
		"help-pos" => "top",
		"stack" => "begin"
	);

	$options[] = array(
		"name" => __( "Horizontal Padding", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Horizontal padding:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_sa_horizontal_padding", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "10", // deafult value of the text
		"validation" => "numeric", /* Each text field can be specialy validated, if the text wont be using HTML tags you can choose here 'nohtml' ect. Choose Between: numeric, multinumeric, nohtml, url, email or dont set it for standard  validation*/
		"type" => "text-small",
		"unit" => __( "px", 'rfbwp' ),
		"stack" => "end"
	);


	$options[] = array(
		"name" => __( "Panel Border Size", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Border size:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_sa_border_size", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "10", // deafult value of the text
		"validation" => "numeric", /* Each text field can be specialy validated, if the text wont be using HTML tags you can choose here 'nohtml' ect. Choose Between: numeric, multinumeric, nohtml, url, email or dont set it for standard  validation*/
		"type" => "text-small",
		"unit" => __( "px", 'rfbwp' ),
		"help" => "true", // should the help icon be displayed (not working yet, better add this to your settings)
		"help-desc"  => __( 'Specify parameters for the show all pages panel border. This border is displayed around all of the thumbnails.', 'rfbwp' ), // text for the help tool tip
		"help-pos" => "top",
		"stack" => "begin"
	);

	$options[] = array(
		"name" => __( "Panel Border Color", 'rfbwp' ),
		"desc" => __( "Border color:", 'rfbwp' ),
		"desc-pos" => "top",
		"id" => $rfbwp_shortname."_fb_sa_border_color",
		"std" => "#F6F6F6",
		"type" => "color",
		"validation" => ''
	);

	$options[] = array(
		"name" => __( "Panel Border Radius", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Border radius:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_sa_border_radius", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "10", // deafult value of the text
		"validation" => "numeric", /* Each text field can be specialy validated, if the text wont be using HTML tags you can choose here 'nohtml' ect. Choose Between: numeric, multinumeric, nohtml, url, email or dont set it for standard  validation*/
		"type" => "text-small",
		"unit" => __( "px", 'rfbwp' ),
		"stack" => "end"
	);

	$options[] = array(
		"name" => __( "Panel Outline", 'rfbwp' ),
		"desc" => __( "Panel outline: ", 'rfbwp' ),
		"desc-pos" => "top",
		"id" => $rfbwp_shortname."_fb_sa_outline",
		"std" => "1",
		"type" => "checkbox",
		"help" => "true", // should the help icon be displayed (not working yet, better add this to your settings)
		"help-desc"  => __( 'Specify show all pages panel outline - outline is a 1px line displayed around the border.', 'rfbwp' ), // text for the help tool tip
		"help-pos" => "top",
		"stack" => "begin"
	);

	$options[] = array(
		"name" => __( "Panel Outline Color", 'rfbwp' ),
		"desc" => __( "Outline color:", 'rfbwp' ),
		"desc-pos" => "top",
		"id" => $rfbwp_shortname."_fb_sa_outline_color",
		"std" => "#D6D6D6",
		"type" => "color",
		"stack" => "end",
		"toggle" => "end",
		"validation" => ''
	);
$options[] = array(
		'type' => 'sub-end'
);
	
	/*-----------------------------------------------------------------------------------*/
	/*	Navigation Settings
	/*-----------------------------------------------------------------------------------*/
	
$options[] = array(
	'type' => 'heading',
	'name' => 'Navigation Settings'
);

$options[] = array(
		'type' => 'sub-start'
);	

$options[] = array(
		"name" => __( "Compact Mode", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Compact Mode:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_nav_	", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "0", // deafult value of the text
		"type" => "checkbox",
		//"options" => array("Compact" => __( "Compact", 'rfbwp' ), "Spread" => __( "Spread", 'rfbwp' ) ),
		"help" => "true", // should the help icon be displayed (not working yet, better add this to your settings)
		"help-desc"  => __( 'Enable Compact mode to display navigation sticking to book. '
				. '<br /><br />Enable Stack buttons if you want to display the radius border only on first and last buttons.'
				. '<br /><br />Choose the position of the menu from Menu Alignment options'
				. '<br /><br />Enable Text buttons to display predefined text instead of icons.'), // text for the help tool tip
		"help-pos" => "top",
		"stack" => "begin",
		"toggle" => "begin",
		"toggle-name" => __( "Navigation Settings", 'rfbwp' )
	);

	$options[] = array(
		"name" => __( "Menu alignment", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Menu alignment:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_nav_menu_position", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "bottom", // deafult value of the text
		"type" => "select",
		"options" => array("bottom" => __( "Bottom", 'rfbwp' ), "top" => __( "Top", 'rfbwp' ), "aside left" => __( "Aside Left", 'rfbwp' ), "aside right" => __( "Aside Right", 'rfbwp' ) )
	);

	$options[] = array(
		"name" => __( "Stack buttons", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Stack buttons:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_nav_stack", // the id must be unique, it is used to call back the propertie inside the them
		"type" => "checkbox",
		"std" => "0",
		"help" => "false", // should the help icon be displayed (not working yet, better add this to your settings)
		"help-desc"  => __( 'Enable/Disable buttons stack.', 'rfbwp' ), // text for the help tool tip
		"help-pos" => "top",
	);

	$options[] = array(
		"name" => __( "Text buttons", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Text buttons:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_nav_text", // the id must be unique, it is used to call back the propertie inside the them
		"type" => "checkbox",
		"std" => "0",
		"help" => "false", // should the help icon be displayed (not working yet, better add this to your settings)
		"help-desc"  => __( 'Enable/Disable buttons with only texts.', 'rfbwp' ), // text for the help tool tip
		"help-pos" => "top",
		"stack" => "end"
	);

	$options[] = array(
		"name" => __( "Table of Contents", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Table of contents:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_nav_toc", // the id must be unique, it is used to call back the propertie inside the them
		"type" => "checkbox",
		"std" => "1",
		"help" => "true", // should the help icon be displayed (not working yet, better add this to your settings)
		"help-desc"  => __( 'Enable/Disable Table of Contents button.', 'rfbwp' ), // text for the help tool tip
		"help-pos" => "top",
		"stack" => "begin"
	);

	$options[] = array(
		"name" => __( "Menu Order", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Menu order:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_nav_toc_order", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "1", // deafult value of the text
		"validation" => "numeric", /* Each text field can be specialy validated, if the text wont be using HTML tags you can choose here 'nohtml' ect. Choose Between: numeric, multinumeric, nohtml, url, email or dont set it for standard  validation*/
		"type" => "text-small"
	);

	$options[] = array(
		"name" => __( "Page Index", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Page index:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_nav_toc_index", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "2", // deafult value of the text
		"validation" => "numeric", /* Each text field can be specialy validated, if the text wont be using HTML tags you can choose here 'nohtml' ect. Choose Between: numeric, multinumeric, nohtml, url, email or dont set it for standard  validation*/
		"type" => "text-small"
	);

	$options[] = array(
		"name" => __( "Icon", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Icon:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_nav_toc_icon", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "fa fa-th-list", // deafult value of the text
		"type" => "icon",
		"stack" => "end",
		"validation" => ''
	);

	$options[] = array(
		"name" => __( "Zoom", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Zoom:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_nav_zoom", // the id must be unique, it is used to call back the propertie inside the them
		"type" => "checkbox",
		"std" => "1",
		"help" => "true", // should the help icon be displayed (not working yet, better add this to your settings)
		"help-desc"  => __( 'Enable/Disable Zoom button.', 'rfbwp' ), // text for the help tool tip
		"help-pos" => "top",
		"stack" => "begin"
	);

	$options[] = array(
		"name" => __( "Menu Order", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Menu order:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_nav_zoom_order", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "2", // deafult value of the text
		"validation" => "numeric", /* Each text field can be specialy validated, if the text wont be using HTML tags you can choose here 'nohtml' ect. Choose Between: numeric, multinumeric, nohtml, url, email or dont set it for standard  validation*/
		"type" => "text-small"
	);

	$options[] = array(
		"name" => __( "Zoom In Icon", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Zoom Icon:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_nav_zoom_icon", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "fa fa-search-plus", // deafult value of the text
		"type" => "icon",
		"validation" => ''
	);

	$options[] = array(
		"name" => __( "Zoom Out Icon", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Zoon Out Icon:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_nav_zoom_out_icon", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "fa fa-search-minus", // deafult value of the text
		"type" => "icon",
		"stack" => "end",
		"validation" => ''
	);

	$options[] = array(
		"name" => __( "Slide show", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Slide show:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_nav_ss", // the id must be unique, it is used to call back the propertie inside the them
		"type" => "checkbox",
		"std" => "1",
		"help" => "true", // should the help icon be displayed (not working yet, better add this to your settings)
		"help-desc"  => __( 'Enable/Disable Slide Show button and delay between the slides.', 'rfbwp' ), // text for the help tool tip
		"help-pos" => "top",
		"stack" => "begin"
	);

	$options[] = array(
		"name" => __( "Menu Order", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Menu order:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_nav_ss_order", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "3", // deafult value of the text
		"validation" => "numeric", /* Each text field can be specialy validated, if the text wont be using HTML tags you can choose here 'nohtml' ect. Choose Between: numeric, multinumeric, nohtml, url, email or dont set it for standard  validation*/
		"type" => "text-small"
	);

	$options[] = array(
		"name" => __( "Play Icon", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Play Icon:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_nav_ss_icon", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "fa fa-play", // deafult value of the text
		"type" => "icon",
		"validation" => ''
	);

	$options[] = array(
		"name" => __( "Stop Icon", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Stop Icon:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_nav_ss_stop_icon", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "fa fa-pause", // deafult value of the text
		"type" => "icon",
		"validation" => ''
	);

	$options[] = array(
		"name" => __( "Delay", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Delay:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_nav_ss_delay", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "2000", // deafult value of the text
		"type" => "text-small",
		"unit" => __( "ms", 'rfbwp' ),
		"stack" => "end",
		"validation" => ''
	);

	$options[] = array(
		"name" => __( "Show all pages", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Show all pages:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_nav_sap", // the id must be unique, it is used to call back the propertie inside the them
		"type" => "checkbox",
		"std" => "1",
		"help" => "true", // should the help icon be displayed (not working yet, better add this to your settings)
		"help-desc"  => __( 'Enable/Disable Show All Pages button.', 'rfbwp' ), // text for the help tool tip
		"help-pos" => "top",
		"stack" => "begin"
	);

	$options[] = array(
		"name" => __( "Menu Order", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Menu order:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_nav_sap_order", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "4", // deafult value of the text
		"validation" => "numeric", /* Each text field can be specialy validated, if the text wont be using HTML tags you can choose here 'nohtml' ect. Choose Between: numeric, multinumeric, nohtml, url, email or dont set it for standard  validation*/
		"type" => "text-small"
	);

	$options[] = array(
		"name" => __( "Prev", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Prev:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_nav_sap_icon_prev", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "fa fa-chevron-up", // deafult value of the text
		"type" => "icon",
		"validation" => ''
	);

	$options[] = array(
		"name" => __( "Next", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Next:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_nav_sap_icon_next", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "fa fa-chevron-down", // deafult value of the text
		"type" => "icon",
		"validation" => ''
	);

	$options[] = array(
		"name" => __( "Icon", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Icon:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_nav_sap_icon", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "fa fa-th", // deafult value of the text
		"type" => "icon",
		"validation" => ''
	);

	$options[] = array(
		"name" => __( "Close Icon", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Close Icon:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_nav_sap_icon_close", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "fa fa-times", // deafult value of the text
		"type" => "icon",
		"stack" => "end",
		"validation" => ''
	);

	$options[] = array(
		"name" => __( "Fullscreen", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Fullscreen:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_nav_fs", // the id must be unique, it is used to call back the propertie inside the them
		"type" => "checkbox",
		"std" => "1",
		"help" => "true", // should the help icon be displayed (not working yet, better add this to your settings)
		"help-desc"  => __( 'Enable/Disable Fullscreen button.', 'rfbwp' ), // text for the help tool tip
		"help-pos" => "top",
		"stack" => "begin"
	);

	$options[] = array(
		"name" => __( "Menu Order", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Menu order:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_nav_fs_order", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "5", // deafult value of the text
		"validation" => "numeric", /* Each text field can be specialy validated, if the text wont be using HTML tags you can choose here 'nohtml' ect. Choose Between: numeric, multinumeric, nohtml, url, email or dont set it for standard  validation*/
		"type" => "text-small"
	);

	$options[] = array(
		"name" => __( "Icon", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Icon:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_nav_fs_icon", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "fa fa-expand", // deafult value of the text
		"type" => "icon",
		"validation" => ''
	);

	$options[] = array(
		"name" => __( "Close Icon", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Close Icon:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_nav_fs_close_icon", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "fa fa-compress", // deafult value of the text
		"type" => "icon",
		"stack" => "end",
		"validation" => ''
	);

	$options[] = array(
		"name" => __( "Download", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Download:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_nav_dl", // the id must be unique, it is used to call back the propertie inside the them
		"type" => "checkbox",
		"std" => "1",
		"help" => "true", // should the help icon be displayed (not working yet, better add this to your settings)
		"help-desc"  => __( 'Enable/Disable Download button.', 'rfbwp' ), // text for the help tool tip
		"help-pos" => "top",
		"stack" => "begin"
	);

	$options[] = array(
		"name" => __( "Menu Order", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Menu order:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_nav_dl_order", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "6", // deafult value of the text
		"validation" => "numeric", /* Each text field can be specialy validated, if the text wont be using HTML tags you can choose here 'nohtml' ect. Choose Between: numeric, multinumeric, nohtml, url, email or dont set it for standard  validation*/
		"type" => "text-small"
	);

	$options[] = array(
		"name" => __( "File", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "File:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_nav_dl_file", // the id must be unique, it is used to call back the propertie inside the them
		"validation" => "", /* Each text field can be specialy validated, if the text wont be using HTML tags you can choose here 'nohtml' ect. Choose Between: numeric, multinumeric, nohtml, url, email or dont set it for standard  validation*/
		"token"	=> $rfbwp_shortname.'_0',
		"type" => "upload-file"
	);

	$options[] = array(
		"name" => __( "Icon", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Icon:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_nav_dl_icon", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "fa fa-download", // deafult value of the text
		"type" => "icon",
		"validation" => '',
		"stack" => "end"
	);
	$options[] = array(
		"name" => __( "Arrows", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Arrows:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_nav_arrows", // the id must be unique, it is used to call back the propertie inside the them
		"type" => "checkbox",
		"std" => "1",
		"help" => "true", // should the help icon be displayed (not working yet, better add this to your settings)
		"help-desc"  => __( 'Enable/Disable Next/Previous navigation buttons.', 'rfbwp' ), // text for the help tool tip
		"help-pos" => "top",
		"stack" => "begin"
	);

	$options[] = array(
		"name" => __( "Group with Navbar", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Group with Navbar:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_nav_arrows_toolbar", // the id must be unique, it is used to call back the propertie inside the them
		"type" => "checkbox",
		"std" => "0"
	);

	$options[] = array(
		"name" => __( "Prev Icon", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Prev Icon:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_nav_prev_icon", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "fa fa-chevron-left", // deafult value of the text
		"type" => "icon",
		"validation" => ''
	);

	$options[] = array(
		"name" => __( "Next Icon", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Next Icon:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_nav_next_icon", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "fa fa-chevron-right", // deafult value of the text
		"type" => "icon",
		"validation" => '',
		"stack" => "end",
		"toggle" => "end",
	);

$options[] = array(
		'type' => 'sub-end'
);	

/*-----------------------------------------------------------------------------------*/
	/*	Navigation Style
	/*-----------------------------------------------------------------------------------*/
	
$options[] = array(
	'type' => 'heading',
	'name' => 'Navigation Style'
);

$options[] = array(
		'type' => 'sub-start'
);

$options[] = array(
		"name" => __( "Enable General Style", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Enable General Style:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_nav_general", // the id must be unique, it is used to call back the propertie inside the them
		"type" => "checkbox",
		"std" => "1",
		"help" => "true", // should the help icon be displayed (not working yet, better add this to your settings)
		"help-desc"  => __( 'Specify the general style (padding, margin, font size, bottom border) nav button.', 'rfbwp' ), // text for the help tool tip
		"help-pos" => "top",
		"stack" => "begin",
		"toggle" => "begin",
		"toggle-name" => __( "Navigation Style", 'rfbwp' )
	);

	$options[] = array(
		"name" => __( "Vertical Padding", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Vertical Padding:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_nav_general_v_padding", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "15", // deafult value of the text
		"type" => "text-small",
		"unit" => __( "px", 'rfbwp' ) ,
		"validation" => ''
	);

	$options[] = array(
		"name" => __( "Horizontal Padding", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Horizontal Padding:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_nav_general_h_padding", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "15", // deafult value of the text
		"type" => "text-small",
		"unit" => __( "px", 'rfbwp' ),
		"validation" => ''
	);

	$options[] = array(
		"name" => __( "Margin ", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Margin:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_nav_general_margin", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "20", // deafult value of the text
		"type" => "text-small",
		"unit" => __( "px", 'rfbwp' ) ,
		"validation" => ''
	);

	$options[] = array(
		"name" => __( "Font size", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Font Size:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_nav_general_fontsize", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "22", // deafult value of the text
		"type" => "text-small",
		"unit" => __( "px", 'rfbwp' ),
		"validation" => ''
	);

	$options[] = array(
		"name" => __( "Border Size", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Border Size:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_nav_general_bordersize", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "0", // deafult value of the text
		"type" => "text-small",
		"unit" => __( "px", 'rfbwp' ),
		"validation" => ''
	);

	$options[] = array(
		"name" => __( "Buttons shadow", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Buttons shadow:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_nav_general_shadow", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "0", // deafult value of the text
		"type" => "checkbox",
		"stack" => "end"
	);

	$options[] = array(
		"name" => __( "Regular State", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Regular State:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_nav_default", // the id must be unique, it is used to call back the propertie inside the them
		"type" => "checkbox",
		"std" => "1",
		"help" => "true", // should the help icon be displayed (not working yet, better add this to your settings)
		"help-desc"  => __( 'Specify the color, background color, font size, buttons border radius for default (unactive) button.', 'rfbwp' ), // text for the help tool tip
		"help-pos" => "top",
		"stack" => "begin"
	);

	$options[] = array(
		"name" => __( "Color", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Color:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_nav_default_color", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "#2b2b2b", // deafult value of the text
		"type" => "color",
		"validation" => ''
	);

	$options[] = array(
		"name" => __( "Background", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Background:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_nav_default_background", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "", // deafult value of the text
		"type" => "color",
		"stack" => "end",
		"validation" => ''
	);

	$options[] = array(
		"name" => __( "Hover State", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Hover State:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_nav_hover", // the id must be unique, it is used to call back the propertie inside the them
		"type" => "checkbox",
		"std" => "1",
		"help" => "true", // should the help icon be displayed (not working yet, better add this to your settings)
		"help-desc"  => __( 'Specify the color, background color, font size, buttons border radius for button hover.', 'rfbwp' ), // text for the help tool tip
		"help-pos" => "top",
		"stack" => "begin"
	);

	$options[] = array(
		"name" => __( "Color", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Color:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_nav_hover_color", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "#22b4d8", // deafult value of the text
		"type" => "color",
		"validation" => ''
	);

	$options[] = array(
		"name" => __( "Background", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Background:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_nav_hover_background", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "", // deafult value of the text
		"type" => "color",
		"stack" => "end",
		"validation" => ''
	);

	$options[] = array(
		"name" => __( "Regular Border State", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Regular Border State:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_nav_border_default", // the id must be unique, it is used to call back the propertie inside the them
		"type" => "checkbox",
		"std" => "1",
		"help" => "true", // should the help icon be displayed (not working yet, better add this to your settings)
		"help-desc"  => __( 'Enable border styles for buttons.', 'rfbwp' ), // text for the help tool tip
		"help-pos" => "top",
		"stack" => "begin"
	);

	$options[] = array(
		"name" => __( "Color", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Color:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_nav_border_color", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "", // deafult value of the text
		"type" => "color"
	);

	$options[] = array(
		"name" => __( "Radius", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Radius:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_nav_border_radius", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "2", // deafult value of the text
		"type" => "text-small",
		"unit" => __( "px", 'rfbwp' ),
		"stack" => "end",
		"validation" => ''
	);

	$options[] = array(
		"name" => __( "Hover Border State", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Hover Border State:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_nav_border_hover", // the id must be unique, it is used to call back the propertie inside the them
		"type" => "checkbox",
		"std" => "1",
		"help" => "true", // should the help icon be displayed (not working yet, better add this to your settings)
		"help-desc"  => __( 'Enable border styles for hover buttons.', 'rfbwp' ), // text for the help tool tip
		"help-pos" => "top",
		"stack" => "begin"
	);

	$options[] = array(
		"name" => __( "Color", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Color:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_nav_border_hover_color", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "", // deafult value of the text
		"type" => "color",
		"validation" => ''
	);

	$options[] = array(
		"name" => __( "Radius", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Radius:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_nav_border_hover_radius", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "2", // deafult value of the text
		"type" => "text-small",
		"unit" => __( "px", 'rfbwp' ),
		"stack" => "end",
		"toggle" => "end",
		"validation" => ''
	);	
$options[] = array(
		'type' => 'sub-end'
);	



	/*-----------------------------------------------------------------------------------*/
	/*	Pagination Display
	/*-----------------------------------------------------------------------------------*/
$options[] = array(
		'type' => 'heading',
		'name' => 'Pagination Display'
	);

$options[] = array(
		'type' => 'sub-start'
);	

$options[] = array(
		"name" => __( "Enable Pagination display", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Enable Pagination display:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_num", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "0", // deafult value of the text
		"type" => "checkbox",
		"help" => "true", // should the help icon be displayed (not working yet, better add this to your settings)
		"help-desc"  => __( 'Select this option to enable automatic pagination display.', 'rfbwp' ), // text for the help tool tip
		"help-pos" => "top",
		"toggle" => "begin",
		"toggle-name" => __( "Pagination Settings", 'rfbwp' ),
		"stack" => "begin"
	);

	$options[] = array(
		"name" => __( "Hide on first & last page", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Hide on first & last page:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_num_hide", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "1", // deafult value of the text
		"type" => "checkbox",
		"help" => "false", // should the help icon be displayed (not working yet, better add this to your settings)
		//"help-desc"  => __( 'Hide numeration on first and last page of book.', 'rfbwp' ), // text for the help tool tip
		"help-pos" => "top",
		"stack" => "end"
	);

	$options[] = array(
		"name" => __( "Style", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Style:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_num_style", // the id must be unique, it is used to call back the propertie inside the them
		"type" => "checkbox",
		"std" => "0",
		"help" => "true", // should the help icon be displayed (not working yet, better add this to your settings)
		"help-desc"  => __( 'Specify background color.', 'rfbwp' ), // text for the help tool tip
		"help-pos" => "top",
		"stack" => "begin"
	);

	$options[] = array(
		"name" => __( "Background", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Background:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_num_background", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "", // deafult value of the text
		"type" => "color",
		"stack" => "end",
		"validation" => ''
	);

	$options[] = array(
		"name" => __( "Border", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Border:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_num_border", // the id must be unique, it is used to call back the propertie inside the them
		"type" => "checkbox",
		"std" => "0",
		"help" => "true", // should the help icon be displayed (not working yet, better add this to your settings)
		"help-desc"  => __( 'Enable border styles for pagination.', 'rfbwp' ), // text for the help tool tip
		"help-pos" => "top",
		"stack" => "begin",
		"validation" => ''
	);

	$options[] = array(
		"name" => __( "Color", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Color:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_num_border_color", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "", // deafult value of the text
		"type" => "color",
		"validation" => ''
	);

	$options[] = array(
		"name" => __( "Size", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Size:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_num_border_size", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "2", // deafult value of the text
		"type" => "text-small",
		"unit" => __( "px", 'rfbwp' ),
		"validation" => ''
	);

	$options[] = array(
		"name" => __( "Radius", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Radius:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_num_border_radius", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "2", // deafult value of the text
		"type" => "text-small",
		"unit" => __( "px", 'rfbwp' ),
		"stack" => "end" ,
		"validation" => ''
	);

	$options[] = array(
		"name" => __( "Vertical Position", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Vertical Position:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_num_v_position", // the id must be unique, it is used to call back the propertie inside the them
		"type" => "select",
		"std" => "1",
		"options" => array( "top" => _( "Top"), "bottom" => __( "Bottom", 'rfbwp' ) ),
		"help" => "true", // should the help icon be displayed (not working yet, better add this to your settings)
		"help-desc"  => __( 'Select Vertical position for pagination.', 'rfbwp' ), // text for the help tool tip
		"help-pos" => "top",
		"stack" => "begin",
		"validation" => ''
	);

	$options[] = array(
		"name" => __( "Horizontal Position", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Horizontal Position:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_num_h_position", // the id must be unique, it is used to call back the propertie inside the them
		"type" => "select",
		"std" => "1",
		"options" => array( "center" => _( "Center"), "aside" => __( "Aside", 'rfbwp' ) ),
		"help" => "true", // should the help icon be displayed (not working yet, better add this to your settings)
		"help-desc"  => __( 'Select Horizontical position for pagination.', 'rfbwp' ), // text for the help tool tip
		"help-pos" => "top",
		"stack" => "end",
		"validation" => ''
	);

	$options[] = array(
		"name" => __( "Vertical Padding", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Vertical padding: ", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_num_v_padding", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "12", // deafult value of the text
		"validation" => "numeric", /* Each text field can be specialy validated, if the text wont be using HTML tags you can choose here 'nohtml' ect. Choose Between: numeric, multinumeric, nohtml, url, email or dont set it for standard  validation*/
		"type" => "text-small",
		"unit" => __( "px", 'rfbwp' ),
		"help" => "true", // should the help icon be displayed (not working yet, better add this to your settings)
		"help-desc"  => __( 'Specify pagination vertical and horizontal padding.', 'rfbwp' ), // text for the help tool tip
		"help-pos" => "top",
		"stack" => "begin"
	);

	$options[] = array(
		"name" => __( "Horizontal Padding", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Horizontal padding:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_num_h_padding", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "10", // deafult value of the text
		"validation" => "numeric", /* Each text field can be specialy validated, if the text wont be using HTML tags you can choose here 'nohtml' ect. Choose Between: numeric, multinumeric, nohtml, url, email or dont set it for standard  validation*/
		"type" => "text-small",
		"unit" => __( "px", 'rfbwp' ),
		"stack" => "end"
	);

	$options[] = array(
		"name" => __( "Vertical Margin", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Vertical Margin: ", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_num_v_margin", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "12", // deafult value of the text
		"validation" => "numeric", /* Each text field can be specialy validated, if the text wont be using HTML tags you can choose here 'nohtml' ect. Choose Between: numeric, multinumeric, nohtml, url, email or dont set it for standard  validation*/
		"type" => "text-small",
		"unit" => __( "px", 'rfbwp' ),
		"help" => "true", // should the help icon be displayed (not working yet, better add this to your settings)
		"help-desc"  => __( 'Specify pagination vertical and horizontal margin.', 'rfbwp' ), // text for the help tool tip
		"help-pos" => "top",
		"stack" => "begin"
	);

	$options[] = array(
		"name" => __( "Horizontal Margin", 'rfbwp' ), // this defines the heading of the option
		"desc" => __( "Horizontal Margin:", 'rfbwp' ), // this is the field/option description
		"desc-pos" => "top",
		"id"   => $rfbwp_shortname."_fb_num_h_margin", // the id must be unique, it is used to call back the propertie inside the them
		"std"  => "10", // deafult value of the text
		"validation" => "numeric", /* Each text field can be specialy validated, if the text wont be using HTML tags you can choose here 'nohtml' ect. Choose Between: numeric, multinumeric, nohtml, url, email or dont set it for standard  validation*/
		"type" => "text-small",
		"unit" => __( "px", 'rfbwp' ),
		"stack" => "end",
		"toggle" => "end"
	);
$options[] = array(
		'type' => 'sub-end'
);	
	/*----------------------------------------------------------------------------*\
		HARD COVER - Settings
	\*----------------------------------------------------------------------------*/

$options[] = array(
		'type' => 'heading',
		'name' => 'Hard Cover Settings'
	);

$options[] = array(
		'type' => 'sub-start'
);	
$options[ ] = array(
		'name'        => __( 'Enable hard cover', 'rfbwp' ),
		'desc'        => __( 'Enable hard cover:', 'rfbwp' ),
		'desc-pos'    => 'top',
		'id'          => $rfbwp_shortname . '_fb_hc',
		'std'         => '0',
		'type'        => 'checkbox',
		'help'        => 'true',
		'help-desc'   => __( 'Select this option to enable hard covers.', 'rfbwp' ),
		'help-pos'    => 'top',
		'toggle'      => 'begin',
		'toggle-name' => __( 'Hard Cover', 'rfbwp' ),
	);

	$options[ ] = array(
		'name'       => __( 'Choose front cover outside image', 'rfbwp' ),
		'desc'       => __( 'Choose front cover outside image:', 'rfbwp' ),
		'id'         => $rfbwp_shortname . '_fb_hc_fco',
		'help'       => 'true',
		'help-desc'  => __( 'Specify images for front cover.', 'rfbwp' ),
		'help-pos'   => 'left',
		'class'      => /*$rfbwp_shortname . '-hard-cover-left ' .*/ $rfbwp_shortname . '-page-bg-image',
		'token'      => $rfbwp_shortname . '_0',
		'stack'      => 'begin',
//		'float'      => 'left',
		'std'        => '',
		'validation' => '',
		'type'       => 'upload',
	);
	$options[ ] = array(
		'name'       => __( 'Choose front cover inside image', 'rfbwp' ),
		'desc'       => __( 'Choose front cover inside image:', 'rfbwp' ),
		'id'         => $rfbwp_shortname . '_fb_hc_fci',
//		'help'       => 'true',
//		'help-desc'  => __( 'Specify pages background image.', 'rfbwp' ),
//		'help-pos'   => 'left',
		'class'      => /*$rfbwp_shortname . '-hard-cover-right ' .*/ $rfbwp_shortname . '-page-bg-image',
		'token'      => $rfbwp_shortname . '_0',
		'stack'      => 'end',
//		'float'      => 'right',
		'std'        => '',
		'validation' => '',
		'type'       => 'upload',
	);
	$options[] = array(
		'name'     => __( 'Front cover side', 'rfbwp' ),
		'desc'     => __( 'Front cover side:', 'rfbwp' ),
		'desc-pos' => 'top',
		'id'       => $rfbwp_shortname . '_fb_hc_fcc',
		'std'      => '#dddddd',
		'type'     => 'color',
	);

	$options[ ] = array(
		'name'       => __( 'Choose back cover outside image', 'rfbwp' ),
		'desc'       => __( 'Choose back cover outside image:', 'rfbwp' ),
		'id'         => $rfbwp_shortname . '_fb_hc_bco',
		'help'       => 'true',
		'help-desc'  => __( 'Specify images for back cover.', 'rfbwp' ),
		'help-pos'   => 'left',
		'class'      => /*$rfbwp_shortname . '-hard-cover-left ' .*/ $rfbwp_shortname . '-page-bg-image',
		'token'      => $rfbwp_shortname . '_0',
		'stack'      => 'begin',
//		'float'      => 'left',
		'std'        => '',
		'validation' => '',
		'type'       => 'upload',
	);
	$options[ ] = array(
		'name'       => __( 'Choose back cover inside image', 'rfbwp' ),
		'desc'       => __( 'Choose back cover inside image:', 'rfbwp' ),
		'id'         => $rfbwp_shortname . '_fb_hc_bci',
//		'help'       => 'true',
//		'help-desc'  => __( 'Specify pages background image.', 'rfbwp' ),
//		'help-pos'   => 'left',
		'class'      => /*$rfbwp_shortname . '-hard-cover-right ' .*/ $rfbwp_shortname . '-page-bg-image',
		'token'      => $rfbwp_shortname . '_0',
		'stack'      => 'end',
//		'float'      => 'right',
		'std'        => '',
		'validation' => '',
		'type'       => 'upload',
	);
	$options[] = array(
		'name'     => __( 'Back cover side', 'rfbwp' ),
		'desc'     => __( 'Back cover side:', 'rfbwp' ),
		'desc-pos' => 'top',
		'id'       => $rfbwp_shortname . '_fb_hc_bcc',
		'std'      => '#dddddd',
		'type'     => 'color',
		'toggle'   => 'end',
	);

$options[] = array(
		'type' => 'sub-end'
);		
	
	
$path_prefix = 'rfbwp_options[books][0]';	

$output = '';
$is_stack = false;
foreach($options as $value) {
	
	//VAR
	
	$val_id = '';
	$id = " ";
	$temp_id = " ";
	if (isset($value['id'])) {
		$val_id = $path_prefix .   '[' . $value['id'] . ']';
		$id = ' id="'. $value['id'] .'" ';
		$temp_id = ' id="'. $value['id']  .'_temp" ';
	}
	
	$std = '';
	if (isset($value['std'])) {
		$std = $value['std'];
	}
	
	$class = '';
	if(isset($value['class']))
		$class = ' ' . $value['class'] . ' ';
				

	
	if (!$is_stack) {
		switch ($value['type']) {
			case 'text-small' :
			case 'text-medium' :
			case 'text-big' :
			case 'checkbox' :
			case 'select' :
			case 'color' :
			case 'textarea' :
			case 'upload' :
			case 'icon' :
			case 'font_select':
			case 'upload-file' :
					$output .= '<div class="option"><div class="controls">';
					
				break;
			default:
				break;
		}
	}
	
	
	
	switch ($value['type']) {
		case 'text-small' :
		case 'text-medium' :
		case 'text-big' :
		case 'checkbox' :
		case 'select' :
		case 'color' :
		case 'textarea' :
		case 'upload' :
		case 'icon' :
		case 'font_select':
		case 'upload-file' :
				
				$output .= '<div class="description-top">' . $value['name'] .'</div>';
			break;
		default:
			break;
	}
	
	
	switch ($value['type']) {
		case 'heading' :
				$output .= '<div class="mp-toggle-header">';
				$output .= '<span class="toggle-name">'. $value["name"] .'</span><span class="toggle-arrow"></span>';
				$output .= '</div>';
			break;
		case 'sub-start' :
				$output .= '<div class="mp-toggle-content">';
			break;
		case 'sub-end' :
				$output .= '</div>';
			break;
		case 'text-small':
			
				$output .= '<input '. $id .' class="mp-input-small mp-input-border '. $class .'" type="text" name="'. $val_id .'" value="'. $std .'"/>';
				if(isset($value['unit']))
						$output .= '<span class="mp-unit">'.$value['unit'].'</span>';
			break;
		case 'text-medium':
				
				$output .= '<input '. $id .' class="mp-input-medium mp-input-border'. $class .'" type="text" name="'. $val_id .'" value="'. $std .'"/>';
				if(isset($value['unit']))
						$output .= '<span class="mp-unit">'.$value['unit'].'</span>';
			break;
		case 'text-big':
				$output .= '<input '. $id .' class="mp-input-big mp-input-border'. $class .'" type="text" name="'. $val_id .'" value="'. $std .'"/>';
				if(isset($value['unit']))
						$output .= '<span class="mp-unit">'.$value['unit'].'</span>';
			break;
		case 'textarea' :
				$cols = '35';
				$output .= '<textarea '. $id .' class="mp-textarea mp-input-border displayall'. $class .'" name="' . $val_id . '" cols="'. $cols. '" rows="8">'. $std .'</textarea>';
			break;
		case 'checkbox' :
				$toggle_class ='';
				if (isset($value['stack'])) {
					$temp = $value['stack'];
					if ($temp=='begin') { 
						$toggle_class=' checkbox-of-toggle'; 
					}
				}
				// hack to show 0 or 1 checkbox
				$output .= '<input type="hidden" name="'. $val_id .'" value="0" />';
				$output .= '<input '. $id .' class="'. $class .'checkbox of-input'. $toggle_class .'" type="checkbox" name="'. $val_id .'"'. checked($std, 1, false) .' value="1" />';
			break;
		case 'select' :
				$output .= '<select '. $id .'  class="mp-dropdown'. $class .'" name="' . $val_id . '">';
				foreach ($value['options'] as $key => $option ) {
					$selected = '';
					if( $std != '' ) {
						 if ( $std == $key) {
						 	$selected = ' selected';
						 }
					}
					$output .= '<option'. $selected .' value="' . ( $key ) . '">' . ( $option ) . '</option>';
				}
				$output .= '</select>';
			break;
		case 'color' :
				$output .= '<input '. $id .' class="mp-color mp-input-border'. $class .'" type="text" name="'. $val_id .'" value="'. $std .'"/>';
			break;
		case 'upload-file' :
		case 'upload' :
				$val_id_id = '';
				if (isset($value['id'])) {
					$val_id_id = $path_prefix .   '[' . $value['id'] . '_id]';
				}
				//$output .= '<input '. $id .' class="mp-input-medium mp-input-border'. $class .'" type="text" name="'. $val_id .'" value="'. $std .'"/>';
				$random = rand(1,100000);
				$output .= '<div class="dynbookimage-fields">
				<div id="dynbookimage-featured_image-upload-container-'. $random .'">
					<div class="dynbookimage-attachment-upload-filelist-'.  $random .'" data-type="file" data-required="yes">
						<a id="dynbookimage-featured_image-pickfiles-'. $random .'" data-form_id="18" class="button file-selector  dynbookimage_featured_image_18" href="#">Select Image</a>

						<ul class="dynbookimage-attachment-list thumbnails">
						</ul>
						<div class="dynbookimage-attachment-list-temp" data-field-1="'. $val_id .'" data-field-2="'. $val_id_id .'">
						</div>
					</div>
				</div><!-- .container -->

            <span class="dynbookimage-help"></span>

        </div> <!-- .dynbookimage-fields -->

        <script type="text/javascript">
            jQuery(function($) {
                new DYNIMAGE_Uploader(\'dynbookimage-featured_image-pickfiles-'.$random .'\', \'dynbookimage-featured_image-upload-container-'. $random .'\', 1, \'featured_image\', \'jpg,jpeg,gif,png,bmp\', 1024,\''. $val_id .'\',\''. $val_id_id .'\');
            });
        </script>';
			break;
		case 'icon' :
				$output .=  '<a '. $temp_id .' href="#" class="mpc-icon-select ' . $class . ' mp-input-border"><i class="' . $std . '"></i></a>';
				$output .= '<input '. $id .' class="mp-input-icon mp-input-medium mp-input-border'. $class .'" type="hidden" name="'. $val_id .'" value="'. $std .'"/>';
				
			break;
		case 'font_select' :
				$output .= '<select '. $id .'  class="of-input rfbwp-of-input-font mp-dropdown'. $class .'" name="' . $val_id . '">';
				$output .= '<option value="'. $std .'">' . $std . '</option>';
				
				$output .= '</select>';
			break;
		case 'upload-file' :
				//$output .= '<input '. $id .' class="mp-input-medium mp-input-border'. $class .'" type="text" name="'. $val_id .'" value="'. $std .'"/>';
				
			break;
		default : break;
	}
	
	if (isset($value['stack'])) {
		$stack = $value['stack'];
		if ($stack=='begin') {
			$is_stack = true;
			
		}
		if ($stack=='end') {
			$is_stack = false;
			
		}
		
	}
	
	
	if (!$is_stack) {
		switch ($value['type']) {
			case 'text-small' :
			case 'text-medium' :
			case 'text-big' :
			case 'checkbox' :
			case 'select' :
			case 'color' :
			case 'textarea' :
			case 'upload' :
			case 'icon' :
			case 'font_select':
			case 'upload-file' :
					$output .= '</div></div>';
				break;
			default:
				break;
		}
	}
}

echo $output;
?>
</div>
</div>