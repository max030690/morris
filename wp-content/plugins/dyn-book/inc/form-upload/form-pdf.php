<div id="mp-option-books" class="group books" style="display: block;"><div class="breadcrumbs" style="display: none;"><span class="breadcrumb-0 breadcrumb">Books Shelf</span><span class="breadcrumb-1 breadcrumb">Books Settings</span><span class="breadcrumb-2 breadcrumb">Add Pages</span><a href="#" class="edit-button-alt" style="display: none;">Save Settings</a></div><div class="field  field-books" id="field-rfbwp_books" style="display: block;">
<div class="option">
<div class="controls">
<div class="add-new-book-wrap">



<div class="mpc-buttons-wrap add-book-buttons">

	

<div class="rfbwp-pdf-wizard-wrap" id="rfbwp_pdf_wizard">
		<!--
		<div data-error="" class="rfbwp-error-box" id="rfbwp_error_box" style="">
			<div class="rfbwp-error-server rfbwp-error">
				<h3>Server connection issue.</h3>
				<p>Server didn't respond to request.</p>
				<p>Please <a class="rfbwp-restart" href="#">try again</a>. If the issue would repeat contact our support team :)</p>
			</div>
			<div class="rfbwp-error-size rfbwp-error">
				<h3>PDF size issue.</h3>
				<p>PDF size is larger then 50MB.</p>
				<p>Please <a class="rfbwp-restart" href="#">try again</a>. If the issue would repeat contact our support team :)</p>
			</div>
			<div class="rfbwp-error-upload rfbwp-error">
				<h3>PDF upload issue.</h3>
				<p>PDF couldn't be uploaded to the server.</p>
				<p>Please <a class="rfbwp-restart" href="#">try again</a>. If the issue would repeat contact our support team :)</p>
			</div>
			<div class="rfbwp-error-pages rfbwp-error">
				<h3>PDF pages issue.</h3>
				<p>PDF have too few pages. It should have at least 3 pages.</p>
				<p>Please <a class="rfbwp-restart" href="#">try again</a>. If the issue would repeat contact our support team :)</p>
			</div>
			<div class="rfbwp-error-info rfbwp-error">
				<h3>PDF data issue.</h3>
				<p>Server couldn't extract PDF data for convertion.</p>
				<p>Please <a class="rfbwp-restart" href="#">try again</a>. If the issue would repeat contact our support team :)</p>
			</div>
			<div class="rfbwp-error-convertion rfbwp-error">
				<h3>PDF convertion issue.</h3>
				<p>Server couldn't convert PDF to images.</p>
				<p>Please <a class="rfbwp-restart" href="#">try again</a>. If the issue would repeat contact our support team :)</p>
			</div>
			<div class="rfbwp-error-images rfbwp-error">
				<h3>Images list issue.</h3>
				<p>Server couldn't list converted images.</p>
				<p>Please <a class="rfbwp-restart" href="#">try again</a>. If the issue would repeat contact our support team :)</p>
			</div>
			<div class="rfbwp-error-import rfbwp-error">
				<h3>Images import issue.</h3>
				<p>Server couldn't import converted images.</p>
				<p>Please <a class="rfbwp-restart" href="#">try again</a>. If the issue would repeat contact our support team :)</p>
			</div>
			<div class="rfbwp-error-generate rfbwp-error">
				<h3>Imported images issue.</h3>
				<p>Too few images imported. Flipbook should have at least 3 pages.</p>
				<p>Please <a class="rfbwp-restart" href="#">try again</a>. If the issue would repeat contact our support team :)</p>
			</div>
		</div>
		
		-->
		<div class="rfbwp-step rfbwp-active" id="rfbwp_step_options">
			<h3>Step 1: <em>Set Flipbook options</em></h3>
			<label for="rfbwp_name">Flipbook name:</label>
			<input type="text" placeholder="Flipbook name" name="rfbwp_name" id="rfbwp_name">

			<div class="rfbwp-col-left">
				<h5>Normal page</h5>
				<label for="rfbwp_width">
					Max width:					<input type="text" data-max="1000" data-min="100" data-default="500" value="500" placeholder="Width" name="rfbwp_width" class="rfbwp-validate-option" id="rfbwp_width">
					<span>px</span>
				</label>
				<label for="rfbwp_height">
					Max height:					<input type="text" data-max="1000" data-min="100" data-default="500" value="500" placeholder="Height" name="rfbwp_height" class="rfbwp-validate-option" id="rfbwp_height">
					<span>px</span>
				</label>
				<label for="rfbwp_quality">
					Quality:					<input type="text" data-max="100" data-min="1" data-default="50" value="50" placeholder="Quality" name="rfbwp_quality" class="rfbwp-validate-option" id="rfbwp_quality">
					<span>%</span>
				</label>
			</div>

			<div class="rfbwp-col-right" id="rfbwp_zoom_wrap">
				<h5>Zoom page</h5>
				<label class="rfbwp-zoom" for="rfbwp_zoom">
					<input type="checkbox" checked="checked" name="rfbwp_zoom" id="rfbwp_zoom">
					Enabled				</label>
				<label for="rfbwp_zoom_width">
					Max width:					<input type="text" data-max="2000" data-min="100" data-default="1000" value="1000" placeholder="Width" name="rfbwp_zoom_width" class="rfbwp-validate-option" id="rfbwp_zoom_width">
					<span>px</span>
				</label>
				<label for="rfbwp_zoom_height">
					Max height:					<input type="text" data-max="2000" data-min="100" data-default="1000" value="1000" placeholder="Height" name="rfbwp_zoom_height" class="rfbwp-validate-option" id="rfbwp_zoom_height">
					<span>px</span>
				</label>
				<label for="rfbwp_zoom_quality">
					Quality:					<input type="text" data-max="100" data-min="1" data-default="80" value="80" placeholder="Quality" name="rfbwp_zoom_quality" class="rfbwp-validate-option" id="rfbwp_zoom_quality">
					<span>%</span>
				</label>
			</div>
		</div>
		<div class="rfbwp-step rfbwp-active" id="rfbwp_step_pdf">
			<h3>Step 2: <em>Select PDF</em></h3>
			<div class="rfbwp-link">
				<input type="file" name="files[]" id="rfbwp_pdf">
				<span data-text="Click to select file" class="rfbwp-file-name" id="rfbwp_pdf_overlay">Click to select file</span>
			</div>
		</div>
		<div class="rfbwp-step" id="rfbwp_step_convert">
			<h3>Step 3: <em>Start convertion</em></h3>
			<a class="rfbwp-link" id="rfbwp_convert" href="#">Convert PDF</a>
		</div>
		<div class="rfbwp-step" id="rfbwp_step_info">
			<h3>Convertion in progress</h3>
			<div class="rfbwp-progress" id="rfbwp_uploading">
				<span>Uploading: </span>
				<span class="rfbwp-value">0%</span>
				<span data-text="( calculating... )" class="rfbwp-time">( calculating... )</span>
				<span class="rfbwp-line" style="width: 0%;"></span>
			</div>
			<div class="rfbwp-progress" id="rfbwp_converting">
				<span>Converting: </span>
				<span class="rfbwp-value">0%</span>
				<span data-text="( calculating... )" class="rfbwp-time">( calculating... )</span>
				<span class="rfbwp-line" style="width: 0%;"></span>
			</div>
			<div class="rfbwp-progress" id="rfbwp_importing">
				<span>Importing: </span>
				<span class="rfbwp-value">0%</span>
				<span data-text="( calculating... )" class="rfbwp-time">( calculating... )</span>
				<span class="rfbwp-line" style="width: 0%;"></span>
			</div>
		</div>
	</div>
	<!--
	<div class="add-new-book-wrap"><a href="#" class="convert-book mpc-button revert"><i class="dashicons dashicons-book"></i> Convert PDF</a><a href="#" class="add-book mpc-button revert"><i class="dashicons dashicons-book-alt"></i> Create New Book</a><a href="#" class="cancel-convert-book mpc-button revert close"><i class="dashicons dashicons-no"></i> Cancel</a></div></div><div class="description"></div>
	-->
	<div class="clear"></div></div></div></div>