<?php
class Dyn_Book_Filter_Theme {
	function __construct() {
		add_filter('dyn/book/includes/user/profile_new_book_list',array($this,'profile_new_book_list'),10,1);

	}

	public function sort_box($profile_id){
		$sort_type = array(
			"recent" => "Recent",
			"views"	=> "Views",
			"comment" => "Comment"
		);
		$url = add_query_arg('display_page', 'book');

		if(isset($_GET['book-sort']))
			$sort = $_GET['book-sort'];
		else
			$sort = 'recent';
		ob_start();
		?>
		<div style="" class="alert alert-success v-d-book-message">
			<a aria-label="close" data-dismiss="alert" class="close" href="#">×</a>
			<strong>Success!</strong> Book is Deleted.
		</div>
		<div class="cover-twoblocks">
			<div class="sortvideo-div">
				<div class="dyn-book-sort">
					<label>Sort By: </label>
					<select name="dyn-book-sort-select" class="dyn-book-sort-select">
					<?php foreach ($sort_type as $key => $value): ?>
						<option data-url="<?php echo add_query_arg('book-sort', $key, $url); ?>" value="<?php echo $key; ?>" <?php selected( $sort, $key ) ?>><?php echo $value; ?></option>
					<?php endforeach; ?>
					</select>
				</div>
			</div>

			<div class="well well-sm">
				<strong>Views</strong>
				<div class="btn-group">
					<a href="#" id="list-book" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th-list">
					</span>List</a> <a href="#" id="grid-book" class="btn btn-default btn-sm"><span
						class="glyphicon glyphicon-th"></span>Grid</a>
				</div>
			</div>

		</div>

		<script type="text/javascript">
		$(document).ready(function() {
			$('#products-book .item-book').addClass('grid-group-item');
			$('#list-book').click(function(event){event.preventDefault();$('#products-book .item-book').addClass('list-group-item');});
			$('#grid-book').click(function(event){event.preventDefault();$('#products-book .item-book').removeClass('list-group-item');
			$('#products-book .item-book').addClass('grid-group-item');});
		});
		</script>
		<?php
		return ob_get_clean();

	}

	function profile_new_book_list($profile_id) {
		global $wpdb, $wp_query;
		ob_start();
		echo $this->sort_box($profile_id);

		$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

		if(isset($_GET['book-sort']))
			$sort = $_GET['book-sort'];
		else
			$sort = 'recent';


		$args = array(
			'post_type' => 'dyn_book',
			'author' => $profile_id,
			'paged' => $paged,
			'post_status' => 'publish',
			'posts_per_page' => 10,
		);
		if( $sort == 'comment' ){
			$args['orderby'] = 'comment_count';
		}
		if ($sort == 'views'){
			$key = 'popularity_count';

			$args['orderby'] = array('meta_value_num' => 'DESC');
			$args['meta_query'] = array(
			        'relation' => 'OR',
			        array(
			            'key'=> $key,
			            'compare' => 'EXISTS'
			        ),
			        array(
			            'key'=> $key,
			            'compare' => 'NOT EXISTS'
			        )
			    );
		}
		$my_query = new WP_Query( $args );
		$temp_query = $wp_query;
		$wp_query   = NULL;
		$wp_query   = $my_query;
		?>

<?php wp_enqueue_style( 'dyn-book-file-css',DYN_BOOK_PLUGIN_URL.'assets-public/css/user-profile-book.css' ); ?>
<script type="text/javascript">

$(function(){


	$(".dyn-book-sort-select").change(function(){
		window.location.href = $(this).find(':selected').data("url");
	});

	$('.del-book').click(function(e){
		e.preventDefault();

		var confrm = confirm("Are you sure you want to continue !");
		var file_id = $(this).attr('data-id');
		//var profile_id = '<?= $profile_id;?>';

		if(confrm){
			$.ajax({
				type: "POST",
				url: admin_ajax,
				data: { action:'upload_video_delete',file_id: file_id,profile_id:profile_id},
				success : function(data){
					var res = $.parseJSON(data);
					if(res.msg_type == 'success'){
						$('.v-d-book-message').show();
						$('.post-'+res.file_id).remove();
					}
					else{
						alert('Not authorised');
					}
				}
			});
		}
	});
});

</script>

		<div id="products-book" class="list-group">



		<?php
		while ( $my_query->have_posts() ) : $my_query->the_post(); ?>
			<?php $uploaded_value = get_post_meta( get_the_id(), 'dyn_upload_value', true ); ?>
			<!-- Start Layout Wrapper -->
			<div class="item-book col-xs-6 col-lg-6 post-<?php the_ID(); ?>">
				<div class="thumbnail layout-2-wrapper solid-bg">

					<div class="grid-6 first">


						<div class="image-holder">
							<a href="<?php the_permalink(); ?>">
							<div class="hover-item"></div>
							</a>
							<?php
								if (has_post_thumbnail()) {
									the_post_thumbnail('small-thumb', array( 'class' => 'layout-2-thumb' ) );
								} else {
									echo '<img class="layout-2-thumb wp-post-image" src="'. get_template_directory_uri() . '/images/no-image.png' .'" alt="" />';
								}
							?>

						</div>
						<div class="layout-title-box layout-title-box-<?php the_ID(); ?>" style="display:block;">
							<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php echo ctpreview( $content = get_the_title() , $length = 39 , $message = 'No Title available' , $html = ' [...]' ); ?></a></h6>
							<ul class="bottom-detailsul">
							<?php if($profile_id == get_current_user_id()){ ?>
							<li><p><a href="#" class="del-book" data-id="<?= get_the_ID(); ?>" ><i class="fa fa-trash-o"></i> Delete</a></p></li>
							<?php } ?>
							<li><a class="detailsblock-btn" data-modal-id="myModal-<?php the_ID(); ?>"><i class="fa fa-info-circle" aria-hidden="true"></i> Details</a></li>
							</ul>
						</div>
					</div>



					<div class="modal-box" id="myModal-<?php the_ID(); ?>">
					 <div class="modal-body">
					 <a class="js-modal-close close">×</a>
					<div class="layout-2-details layout-2-details-<?php the_ID(); ?>">
						<div class="image-holder dyn-book-file-sprite dyn-book-file-image">
							<a href="<?php the_permalink() ?>">
								<div class="hover-item">
			                        <i class="fa fa-download"></i>
			                    </div>
							</a>
						</div>
						<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title();?></a></h6>
						<div>File Type: Book</div>
						<?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
						<div class="video_excerpt_text one"><h4 class="tittle-h4">Description 4</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = '' , $message = 'No Description available' , $html = ' [...]' );?></div></div>
						<div class="video_excerpt_text two"><h4 class="tittle-h4">Reference</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_post_meta(get_the_ID(),'video_options_refr',true) , $length = '' , $message = 'No referance given' , $html = ' [...]' );?></div></div>
						<ul class="stats">
							<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
							<li><?php videopress_countviews( get_the_ID() ); ?></li>
							<li><i class="fa fa-wrench"></i>
							<?php
								$result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
								echo count($result);
							?> Endorsments</li>
							<li><i class="fa  fa-heart"></i>
							<?php
							  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
							  $result1 = $wpdb->get_results($sql_aux);
							  echo count($result1);
							?> Favorites</li>
							<!--end favorite-->
							<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
							<li><?php comments_number() ?></li>

						</ul>
						<div class="clear"></div>
						<p><?php echo videopress_content('240'); ?></p>
						<?php if($profile_id == get_current_user_id()){ ?>
						<p><a href="#" class="del-book" data-id="<?= get_the_ID(); ?>" ><i class="fa fa-trash-o"></i> Delete</a></p>
						<?php } ?>
					</div></div></div>



					<div class="layout-2-details layout-2-details-<?php the_ID(); ?>" style="display:none;">
						<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title();?></a></h6>
						<div>File Type: Book</div>
						<?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
						<div class="video_excerpt_text one"><h4 class="tittle-h4">Description</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = 100 , $message = 'No Description available' , $html = '<a data-modal-id="myModal-'.get_the_ID().'"> [...]</a>' ); ?></div></div>
						<ul class="stats">
							<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
							<li><?php videopress_countviews( get_the_ID() ); ?></li>
							<li><i class="fa fa-wrench"></i>
							<?php
								$result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
								echo count($result);
							?> Endorsments</li>
							<li><i class="fa  fa-heart"></i>
							<?php
							  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
							  $result1 = $wpdb->get_results($sql_aux);
							  echo count($result1);
							?> Favorites</li>
							<!--end favorite-->
							<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
							<li><?php comments_number() ?></li>

						</ul>
						<div class="clear"></div>
						<p><?php echo videopress_content('240'); ?></p>
						<?php if($profile_id == get_current_user_id()){ ?>
						<p><a href="#" class="del-book" data-id="<?= get_the_ID(); ?>" ><i class="fa fa-trash-o"></i> Delete</a></p>
						<?php } ?>
					</div>

					<div class="clear"></div>
				</div>
			</div>
			<!-- End Layout Wrapper -->




		<?php
		endwhile;
		echo '</div>';

		echo paginate_links(array('add_args' => array('display_page' => 'book')));

		$wp_query = NULL;
		$wp_query = $temp_query;
	}


}

new Dyn_Book_Filter_Theme();
?>