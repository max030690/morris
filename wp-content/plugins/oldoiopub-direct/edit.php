<?php

/*
Copyright (C) 2008  Simon Emery

This file is part of OIOpublisher Direct.
*/

//define vars
define('OIOPUB_EDIT', 1);
define('OIOPUB_ADMIN', 1);

//init
include_once(str_replace("\\", "/", dirname(__FILE__)) . "/index.php");

//don't show errors
@ini_set('display_errors', 0);

//is plugin enabled?
if($oiopub_set->enabled != 1) {
	die("Plugin Currently Offline");
}

//check access
/*
if(!oiopub_auth_check()) {
	die("Access Denied");
}
*/

//get vars
$message = '';
$nofollow = true;
$item = new oiopub_std;
$item_id = intval($_GET['id']);
$item_type = oiopub_clean($_GET['type']);

//copy
if($_GET['do'] == "copy") {
	oiopub_copy_ad($item_id, $_SERVER['HTTP_REFERER']);
}

//disable upload?
if(isset($_GET['upload']) && $_GET['upload']) {
	$oiopub_set->general_set['upload'] = $_GET['upload'] == 'false' ? 0 : 1;
}

//edit?
if($item_id > 0) {
	$item = $oiopub_db->GetRow("SELECT * FROM $oiopub_set->dbtable_purchases WHERE item_id='$item_id'");
	if(empty($item)) {
		die("This Purchase ID does not exist in the database!");
	} elseif($item->submit_api > 0) {
		die("You cannot edit purchases made via OIOpublisher.com");
	} else {
		$pid = $item->post_id;
	}
}

//already uploaded?
if(strpos($item->item_url, "/uploads/") !== false) {
	$is_uploaded = true;
	$org_item_url = $item->item_url;
} else {
	$is_uploaded = false;
	$org_item_url = $item->item_url;
}

//get type
if($item_type == "post") {
	$prefix = "p";
	$nofollow = false;
	$item->item_channel = 1;
	$adtype_array = array( 1 => "Blogger", "Advertiser" );
	$title = __oio("Paid Review Purchase");
} elseif($item_type == "link") {
	$prefix = "l";
	$item->item_channel = 2;
	for($z=1; $z <= $oiopub_set->links_zones; $z++) {
		$lz = "links_" . $z;
		if(!empty($oiopub_set->{$lz}['title'])) {
			$adtype_array[$z] = $oiopub_set->{$lz}['title'] . " (zone $z)";
		}
		if(isset($item->item_type) && $item->item_type == $z) {
			$nofollow = $oiopub_set->{$lz}['nofollow'] == 2 ? true : false;
		}
	}
	$title = __oio("Text Ad Purchase");
} elseif($item_type == "inline") {
	$prefix = "v";
	$item->item_channel = 3;
	if($oiopub_set->inline_ads['selection'] == 1) $inline_select = "Video Ad";
	if($oiopub_set->inline_ads['selection'] == 2) $inline_select = "Banner Ad";
	if($oiopub_set->inline_ads['selection'] == 3) $inline_select = "RSS Feed Ad";
	$adtype_array = array( $oiopub_set->inline_ads['selection'] => $inline_select, 4 => "Intext Link" );
	$title = __oio("Inline Ad Purchase");
	//$nofollow = $oiopub_set->inline_ads['nofollow'] == 2 ? true : false;
} elseif($item_type == "custom") {
	$prefix = "s";
	$nofollow = false;
	$item->item_channel = 4;
	for($z=1; $z <= $oiopub_set->custom_num; $z++) {
		$cn = "custom_" . $z;
		if(!empty($oiopub_set->{$cn}['title'])) {
			$adtype_array[$z] = $oiopub_set->{$cn}['title'] . " (item $z)";
		}
	}
	$title = __oio("Custom Purchase");
} elseif($item_type == "banner") {
	$prefix = "b";
	$item->item_channel = 5;
	for($z=1; $z <= $oiopub_set->banners_zones; $z++) {
		$bz = "banners_" . $z;
		if(!empty($oiopub_set->{$bz}['title'])) {
			$adtype_array[$z] = $oiopub_set->{$bz}['title'] . " (zone $z)";
		}
		if(isset($item->item_type) && $item->item_type == $z) {
			$nofollow = $oiopub_set->{$bz}['nofollow'] == 2 ? true : false;
		}
	}
	$title = __oio("Banner Ad Purchase");
}
elseif($item_type == "video") {
	$prefix = "p";
	$nofollow = false;
	$item->item_channel = 6;
	$adtype_array = array( 1 => "HTML5 (self-hosted)");
	$title = __oio("Paid Review Purchase");
}

//post vars
if(isset($_POST['process']) && $_POST['process'] == "yes") {
	//set vars
	$item->adv_name = oiopub_clean($_POST['name']);
	$item->adv_email = strtolower(oiopub_clean($_POST['email']));
	$item->item_status = intval($_POST['adstatus']);
	$item->item_nofollow = intval($_POST['adnofollow']);
	$item->direct_link = intval($_POST['adtracking']);
	$item->payment_processor = oiopub_clean($_POST['paymethod']);
	$item->payment_status = intval($_POST['paystatus']);
	$item->item_subscription = intval($_POST['subscription']);
	$item->payment_txid = oiopub_clean($_POST['txn_id']);
	$item->payment_amount = floatval($_POST['adprice']);
	$item->item_model = oiopub_clean($_POST['admodel']);
	$item->item_duration = intval($_POST['adduration']);
	$item->item_duration_left = intval($_POST['adduration_left']);
	$item->item_url = oiopub_clean($_POST['adurl']);
	$item->item_url = oiopub_clean($_POST['mpfour_url']);
	
	$item->item_page = oiopub_clean($_POST['adpage']);
	$item->item_tooltip = oiopub_clean($_POST['adtooltip']);
	$item->item_notes = oiopub_clean($_POST['adnotes'], 0, 1, 0);
	$item->item_subid = oiopub_clean($_POST['subid']);
	$item->category_id = intval($_POST['cats']);
	//set currency?
	if(isset($_POST['adcurrency'])) {
		$item->payment_currency = oiopub_clean($_POST['adcurrency']);
	}
	//post data?
	if($item_type == "post") {
		$item->post_author = intval($_POST['adtype']);
		$item->item_type = 0;
	} else {
		$item->post_author = 0;
		$item->item_type = intval($_POST['adtype']);
	}
	//payment start
	if(isset($_POST['adstart']) && !empty($_POST['adstart'])) {
		$item->payment_time = strtotime($_POST['adstart']);
	} elseif($item->payment_time == 0 && $item->payment_status == 1) {
		$item->payment_time = time();
	} else {
		$item->payment_time = 0;
	}
	//payment next
	if($item->payment_time > 0 && $item->item_duration > 0 && $item->item_subscription == 1 && $item->item_model == 'days') {
		$item->payment_next = $item->payment_time + ($item->item_duration * 86400);
	} else {
		$item->payment_next = 0;
	}
	//post items
	if(isset($_POST['postid'])) {
		$item->post_id = intval($_POST['postid']);
	}
	if(isset($_POST['postphrase'])) {
		$item->post_phrase = oiopub_clean($_POST['postphrase']);
	}
	//inline hack
	if($item->item_channel == 3 && $item->item_type == 4) {
		$item->item_url = oiopub_clean($_POST['adurl2']);
		$item->item_tooltip = oiopub_clean($_POST['adtooltip2']);
	}
	//image upload
	if($oiopub_set->general_set['upload'] == 1 && isset($_FILES['adurl']['name'])) {
		if(!empty($_FILES['adurl']['name'])) {
			include_once($oiopub_set->folder_dir . "/include/upload.php");
			$rand = oiopub_rand(6) . "_";
			$upload = new oiopub_upload();
			$upload->name = $rand . $_FILES['adurl']['name'];
			$upload->size = $_FILES['adurl']['size'];
			$upload->temp_name = $_FILES['adurl']['tmp_name'];
			$upload->upload_dir = $oiopub_set->folder_dir . "/uploads/";
			$upload->is_image = true;
			if($upload->upload()) {
				$item->item_url = $oiopub_set->plugin_url . "/uploads/" . $rand . $_FILES['adurl']['name'];
			} else {
				$message = "<font color='red'><b>File Upload Attempt Failed!</b></font>\n";
			}
		} elseif($is_uploaded) {
			$item->item_url = $org_item_url;
		}
	}
	if(empty($message)) {
		if($item_id > 0) {
			//set data
			$set = array();
			foreach($item as $k => $v) {
				$set[] = "$k='$v'";
			}
			//update
			$oiopub_db->query("UPDATE $oiopub_set->dbtable_purchases SET " . implode(',', $set) . " WHERE item_id='$item_id'");
			$message = "<font color='green'><b>Purchase Data Updated</b></font>\n";
			oiopub_flush_cache();
		} else {
			//post data
			if($item_type == "post") {
				$post = array();
				$post['category'] = intval($_POST['postcat']);
				$post['title'] = oiopub_clean($_POST['posttitle']);
				$post['content'] = oiopub_clean($_POST['postcontent'], 0, 1);
				if(function_exists('oiopub_insert_post')) {
					$item->post_id = oiopub_insert_post($post);
				} else {
					die("The platform you are using OIOpublisher on cannot handle post data!");
				}
			}
			// Video data post of elite plugin
				if($item_type == "video") {
				/*
				 $elite_players = get_option("elite_players");
						if (isset($_GET['playerId']) )
							{
							$current_id = $_GET['playerId'];
							$elite_player = $elite_players[$current_id];
							$videos = $elite_player["videos"];
						}
					$new_id = 0;
					$highest_id = 0;
					foreach ($elite_players as $elite_player) {
					$player_id = $elite_player["id"];
					if($player_id > $highest_id) {
						$highest_id = $player_id;
					}
				  }
				$current_id = $highest_id + 1;*/
				//create new elite player 
			//	$videotitle =  $_POST['posttitle'];
				//$discription_video = $_POST['discription_video'];
				//$info_video = $_POST['info_video'];
				//$thumbImg_video = $_POST['thumbImg_video'];
				$mpfour_url = $_POST['adurl'];
				/*$prerollAD_video = array('no','yes');
				$prerollGotoLink = $_POST['prerollGotoLink'];
				$preroll_mp4 = $_POST['preroll_mp4'];
				$prerollSkipTimer = $_POST['prerollSkipTimer'];
				$midrollAD = array('no','yes');
				$midrollAD_displayTime = $_POST['midrollAD_displayTime'];
				$midrollGotoLink = $_POST['midrollGotoLink'];
				$midroll_mp4 = $_POST['midroll_mp4'];
				$midrollSkipTimer = $_POST['midrollSkipTimer'];
				$midrollGotoLink = $_POST['midrollGotoLink'];
				$postrollAD = array('no','yes');
				$postrollGotoLink = $_POST['postrollGotoLink'];
				$postroll_mp4 = $_POST['postroll_mp4'];
				$popupAdShow = array('no','yes');
				$popupImg = $_POST['popupImg'];
				$popupAdEndTime = $_POST['popupAdEndTime'];
				$popupAdGoToLink = $_POST['popupAdGoToLink'];
				//$popupImg = $_POST['popupImg'];
				
				$new_elite_player = array(	'id' 					=> $current_id, 
										"instanceName" 				=> "Player " . $current_id,
										'videoType'					=>'HTML5 (self-hosted)',
										"videos" 					=> array( 0 => array(
																				"title" 		=>$videotitle,
																				"description" 	=> $discription_video,
																				"info" 			=> $info_video,
																				"thumbImg" 		=> $thumbImg_video,
																				"mp4" 					=> $mpfour_url,
																				"prerollAD" 			=>  $prerollAD_video,
																				"prerollGotoLink" 		=> $prerollGotoLink,
																				"preroll_mp4" 			=> $preroll_mp4,
																				"prerollSkipTimer" 		=> $prerollSkipTimer,
																				"midrollAD"			 	=> $midrollAD,
																				"midrollAD_displayTime" => $midrollAD_displayTime,
																				"midrollGotoLink" 		=> $midrollGotoLink,
																				"midroll_mp4"			=> $midroll_mp4,
																				"midrollSkipTimer" 		=> $midrollSkipTimer,
																				"postrollAD" 			=> $postrollAD,
																				"postrollGotoLink" 		=> $postrollGotoLink,
																				"postroll_mp4" 			=> $postroll_mp4,
																				"postrollSkipTimer"  	=> $postrollSkipTimer,
																				"popupAdShow" 			=> $popupAdShow,
																				"popupImg" 				=> $popupImg,
																				"popupAdStartTime" 		=> $popupAdEndTime,
																				"popupAdGoToLink" 		=> $popupAdGoToLink,
																				),
																			),
										 "videoPlayerWidth" 			=> $videoPlayerWidth,
										"videoPlayerHeight" 		=> $videoPlayerHeight,
										"responsive" 				=> $responsive,
										"videoPlayerShadow" 		=> $videoPlayerShadow,
										"colorAccent" 				=> $colorAccent,
										"posterImg" 				=> $posterImg,
										"logoShow" 					=> $logoShow,
										"logoPath" 					=> $logoPath,
										"logoPosition" 				=> $logoPosition,
										"logoClickable" 			=> $logoClickable,
										"logoGoToLink" 				=> $logoGoToLink,
										"allowSkipAd" 				=> $allowSkipAd,
										"onFinish" 					=> $onFinish,
										"autoplay" 					=> $autoplay,
										"loadRandomVideoOnStart" 	=> $loadRandomVideoOnStart,
										"shuffle" 					=> $shuffle,
										"playlistBehaviourOnPageload" => $playlistBehaviourOnPageload,
										"playlist" 					=> $playlist,
										"playlistScrollType"  		=> $playlistScrollType,
										"hideVideoSource" 			=> $playlistScrollType,
										"showAllControls" 			=> $showAllControls,
										"rightClickMenu" 			=> $rightClickMenu,
										"autohideControls" 			=> $autohideControls,
										"hideControlsOnMouseOut" 	=> $hideControlsOnMouseOut,
										"fullscreen" 				=> $fullscreen,
										"nowPlayingText" 			=> $nowPlayingText,
										"infoShow" 					=> $infoShow,
										"shareShow" 				=> $shareShow,
										"facebookShow" 				=> $facebookShow,
										"twitterShow" 				=> $twitterShow,
										"mailShow" 					=> $mailShow,
										"facebookShareName" 		=> $facebookShareName,
										"facebookShareLink" 		=> $facebookShareLink,
										"facebookShareDescription" 	=> $facebookShareDescription,
										"facebookSharePicture" 		=> $facebookSharePicture,
										"twitterText" 				=>  $twitterText,
										"twitterLink" 				=> $twitterLink,
										"twitterHashtags" 			=> $twitterHashtags,
										"twitterVia" 				=> $twitterVia,
										"googlePlus" 				=> $googlePlus,
										"embedShow" 				=> $embedShow,
										"embedCodeSrc" 				=> $embedCodeSrc,
										"embedCodeW" 				=> $embedCodeW,
										"embedCodeH" 				=> $embedCodeH,
										"embedShareLink" 			=> $embedShareLink,
										"youtubeControls" 			=> $youtubeControls,
										"youtubeSkin" 				=> $youtubeSkin,
										"youtubeColor" 				=> $youtubeColor,
										"youtubeQuality" 			=> $youtubeQuality,
										"youtubeShowRelatedVideos" 	=> $youtubeShowRelatedVideos,
										"vimeoColor" 				=> $vimeoColor,
										//"videoType" =>  $videoType, 
										 
						);
				$elite_players[$current_id] = $new_elite_player;*/
				update_option("elite_players", $upload->name);
				/*$wpdb->insert('wp_oiopub_purchases', array(
                   'item_url' => "Player " . $current_id,
                   //  'email' => 'kumkum@gmail.com',
                     // 'phone' => '3456734567', // ... and so on
                      ));*/
					  $item->item_url = oiopub_clean($upload->name);
			}
			//insert
			$item->submit_time = time();
			$item->rand_id = $prefix . "-" . oiopub_rand(10);
			$item->post_id = intval($item->post_id);
			$oiopub_db->query("INSERT INTO " . $oiopub_set->dbtable_purchases . " (" . implode(',', array_keys((array) $item)) . ") VALUES ('" . implode("','", array_values((array) $item)) . "')");
			$item_id = intval($oiopub_db->insert_id);
			$oiopub_set->request_uri .= '&id=' . $item_id;
			if($item_id > 0) {
				$message = "<font color='green'><b>New Purchase Inserted</b></font>\n";
				oiopub_flush_cache();
			} else {
				$db_error = $oiopub_db->LastError();
				$message = "<font color='red'><b>An error has occurred: " . ($db_error ? $db_error : 'cause unknown') . "</b></font>\n";
			}
		}
	}
	//reset item notes (hack)
	$item->item_notes = stripslashes($item->item_notes);
} elseif(isset($_POST['delete']) && $_POST['delete'] == "yes") {
	//delete
	$item = "";
	$item_id = intval($_GET['id']);
	$oiopub_db->query("DELETE FROM " . $oiopub_set->dbtable_purchases . " WHERE item_id='$item_id'");
	$message = "<font color='green'><b>Item Deleted</b></font>\n";
	oiopub_flush_cache();
	//remove image?
	if($is_uploaded) {
		$exp = explode("/uploads/", $org_item_url, 2);
		@unlink($oiopub_set->folder_dir . "/uploads/" . $exp[1]);
	}
}

//other arrays
$adstatus_array = array( 0 => "Pending", 1 => "Approved", 2 => "Rejected", 3 => "Expired", -1 => "Queued", -2 => "Queued (pending)" );
$paystatus_array = array( 0 => "Not Paid", 1 => "Paid", 2 => "Invalid" );

//get categories
if(function_exists('oiopub_category_list')) {
	$cats_array = oiopub_category_list();
}

//template vars
$templates = array();
$templates['page'] = "purchase_edit";
$templates['title'] = $title;

//load template
include_once($oiopub_set->folder_dir . "/templates/core/main.tpl");

?>