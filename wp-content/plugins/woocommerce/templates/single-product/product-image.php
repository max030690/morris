<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<?php
$postId=get_the_ID();
 
$uploaded_type = get_post_meta( get_the_ID(), 'dyn_upload_type', true );
$uploaded_value = get_post_meta( get_the_ID(), 'dyn_upload_value', true );
?>

<?php if(!is_user_logged_in()){ ?>
	<style>
		.scroll-left_3{
			display:none !important;
		}

		.scroll-right_3{
			display:none !important;
		}
	</style>
	<?php

}

$arg = get_current_user_id();
$path = "http://ec2-54-200-193-165.us-west-2.compute.amazonaws.com/app/video_prediction?user_id=".$arg;
$response = file_get_contents($path);


global $wpdb;
$postdataval = false;
if(isset($_POST['postdataval']) && $_POST['postdataval']!=''){
	if($_POST['postdataname'] == 'desc'){
		$postdataval = $_POST['postdataval'];
		$my_post = array(
				'ID'           => $_POST['postid'],
				'post_content' => $postdataval,
		);
		// Update the post into the database
		wp_update_post( $my_post );
	}
	elseif($_POST['postdataname'] == 'refe'){
		update_post_meta($_POST['postid'], 'video_options_refr', $_POST['postdataval']);
	}
}

?>

<?php
/**
 * Single Product Image
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-image.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.14
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post, $woocommerce, $product;
///echo get_the_ID();
?>


<div class="container-fluid grid-3-4 centre-block">
	<div class="row" id="product-first">
		<div class="col-md-8  col-sm-12 col-lg-8 col-xs-12" id="images">
			<div class="row">
				<div class="col-md-12 col-lg-8 col-sm-12">
					<?php
					if ( has_post_thumbnail() ) {
						$image_caption = get_post( get_post_thumbnail_id() )->post_excerpt;
						$image_link    = wp_get_attachment_url( get_post_thumbnail_id() );
						$image         = get_the_post_thumbnail( $post->ID, apply_filters( 'single_product_large_thumbnail_size', 'shop_single' ), array(
								'title'	=> get_the_title( get_post_thumbnail_id() )
						) );

						$attachment_count = count( $product->get_gallery_attachment_ids() );

						if ( $attachment_count > 0 ) {
							$gallery = '[product-gallery]';
						} else {
							$gallery = '';
						}

						echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<div class="img-product-page"><a href="%s" itemprop="image" class="woocommerce-main-image zoom" title="%s" data-rel="prettyPhoto' . $gallery . '">%s</a></div>', $image_link, $image_caption, $image ), $post->ID );

					} else {

						echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<div class="img-product-page"><img src="%s" alt="%s" /></div>', wc_placeholder_img_src(), __( 'Placeholder', 'woocommerce' ) ), $post->ID );

					}
					?>
					<?php
					/**
					 * Single Product Thumbnails
					 *
					 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-thumbnails.php.
					 *
					 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
					 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
					 * as little as possible, but it does happen. When this occurs the version of the template file will.
					 * be bumped and the readme will list any important changes.
					 *
					 * @see 	    http://docs.woothemes.com/document/template-structure/
					 * @author 		WooThemes
					 * @package 	WooCommerce/Templates
					 * @version     2.3.0
					 */

					if ( ! defined( 'ABSPATH' ) ) {
						exit; // Exit if accessed directly
					}
					global $post, $product, $woocommerce;

					$attachment_ids = $product->get_gallery_attachment_ids();

					if ( $attachment_ids ) {
						$loop 		= 0;
						$columns 	= apply_filters( 'woocommerce_product_thumbnails_columns', 3 );
						?>
						<div class="thumbnails <?php echo 'columns-' . $columns; ?>  " style="width: 268px"><?php

							foreach ( $attachment_ids as $attachment_id ) {

								$classes = array( 'zoom' );

								if ( $loop === 0 || $loop % $columns === 0 )
									$classes[] = 'first';

								if ( ( $loop + 1 ) % $columns === 0 )
									$classes[] = 'last';

								$image_link = wp_get_attachment_url( $attachment_id );

								if ( ! $image_link )
									continue;

								$image_title 	= esc_attr( get_the_title( $attachment_id ) );
								$image_caption 	= esc_attr( get_post_field( 'post_excerpt', $attachment_id ) );

								$image       = wp_get_attachment_image( $attachment_id, apply_filters( 'single_product_small_thumbnail_size', 'shop_thumbnail' ), 0, $attr = array(
										'title'	=> $image_title,
										'alt'	=> $image_title,
										'width' => '65px'
								) );

								$image_class = esc_attr( implode( ' ', $classes ) );

								echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', sprintf( '<a href="%s" class="%s" title="%s"  data-rel="prettyPhoto[product-gallery]">%s</a>', $image_link, $image_class, $image_caption, $image ), $attachment_id, $post->ID, $image_class );

								$loop++;
							}

							?></div>
						<?php
					}
					$country_id = oiopub_settings::get_user_country_index();

			if(isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] !== '127.0.0.1' && $country_id !== false){
				$queryyy = " AND (`country` LIKE '%,$country_id,%' OR `country` LIKE '$country_id,%' OR `country` LIKE '$country_id' OR `country` LIKE '%,$country_id' OR country ='all')  ";

			} else {
				$queryyy = " ";
			}
			$tagSelected = wp_get_post_tags($post->ID);
			$tagsNameArray = array();
			$tagsSlugArray = array();
			$tagsIDArray = array();
			$tags_str = '';
			foreach($tagSelected as $tag)
			{
				$tags_str .= "'".$tag->name."',";
			}
			$tags_str = substr($tags_str, 0, -1);
			$bagfhfa = ''; $bagfhf = '';
			global $wpdb;
			$ptags  = $tags_str ? $wpdb->get_results("SELECT *  FROM `wp_oiopub_purchases_preventative_tags` WHERE  `tag`  IN ($tags_str) ") : null;
			if($ptags) {
				$ptagname = '';
				$ptagpurchaseid = '';foreach ($ptags as $ptag) {

					$ptagname = $ptag->tag;
					$ptagpurchaseid = $ptag->purchase_id;
				}
				if($ptags)	{$bagfhfa = "AND id != " .  $ptagpurchaseid ;} else {$bagfhfa = '';


					global $wpdb;
					$tags  = $wpdb->get_results("SELECT *  FROM `wp_oiopub_purchases_tags` WHERE  `tag`  IN ($tags_str) ");
					$tagname = '';
					$tagpurchaseid = '';
					foreach ($tags as $tag) {

						$tagname = $tag->tag;
						$tagpurchaseid = $tag->purchase_id;
					}

					if($tags && $tagpurchaseid !=='' && $tagname !==''){ $bagfhf = "AND id = " .  $tagpurchaseid ; }  else {$bagfhf = ''; }
				}
			} ?>


				</div>
				<div class="col-md-12 col-lg-4 col-sm-12" id="addproduct">
					<div class="product-group">
						<!--Donate Popup-->
						<?php
						include 'sale-flash2.php';
						do_action( 'woocommerce_single_product_summary' );
						?>
						<?php
						$post_id = get_the_ID();
						$author_id=$post->post_author;
						if(is_user_logged_in() && $author_id == get_current_user_id())
						{

							echo '<div  id="tools-edit"><strong>Tools :<a href="#" id="title-edit"><i class="fa fa-pencil-square-o"></i>EDIT</a></strong>';
							echo '<a id="dublicate-product" href="http://www.doityourselfnation.org/bit_bucket/dashboard/product/duplicate/'.get_the_id().'">Dublicate</a></div>';

						}

						?>

						<div id="title-div" style="display:none;">
							<input type="text" name="title-val" value="<?= the_title();?>">
							<input type="hidden" name="post_id" value="<?= get_the_id();?>">

							<button type="button" class="btn btn-info" id="title-save">Save</button>
							<button type="button" class="btn btn-primary" id="title-cncl">Cancel</button>
							<br>
						</div>



					</div>
					<div class="ships-from"></div>
				</div>

			</div>

			<script>
				jQuery(document).ready(function($) {
					$('#title-edit').click(function(){
						$('#tools-edit').hide();
					});

					$('#title-cncl').click(function(e){
						e.preventDefault();

						$('#title-div').hide();
						$('#tools-edit').show();
					});


					$('#title-save').click(function(e){
						e.preventDefault();


						$('#title-div').hide();
						$('#tools-edit').show();

						var post_title = jQuery('input[name="title-val"]').val();
						var post_id = jQuery('input[name="post_id"]').val();
						$.ajax({
							url: admin_ajax,
							type: "POST",
							data: {action : 'user_title_edits',post_title : post_title,post_id : post_id},
							success : function(data){
							 ocation.reload();
								//alert('ok');
 							}
						})
					});
 					$('.sold-by .wcvendors_ships_from span').prependTo(".ships-from");
					$('<i class="fa fa-smile-o" aria-hidden="true"></i>').prependTo('.stock.in-stock');
				});

			</script>

			<div class="col-md-12 col-lg-12 col-sm-12 product-attributes-all">

				<?php


				$user = wp_get_current_user();
				if(!(in_array( 'adfree_user', (array) $user->roles ))) {
					$banners  = $wpdb->get_results("SELECT item_id,item_url FROM `wp_oiopub_purchases` WHERE   item_channel= 7 and payment_status = 1 and country !=''  $queryyy $bagfhf $bagfhfa ORDER BY RAND() LIMIT 1");


			$bannerurl = '';
			$bannerid = '';
			$countryinbase = '';
			foreach ($banners as $banner) {

				$bannerurl = $banner->item_url;
				$bannerid = $banner->item_id;
			}

					?>

					<?php
					if ($banners) {

						?>
						<a href="http://www.doityourselfnation.org/bit_bucket/wp-content/plugins/oiopub-direct/modules/tracker/go.php?id=<?php echo $bannerid; ?>"><img class="oio-click" style="width: 100%; height: 150px;margin-top: 9px;
    margin-bottom: -12px;"src="<?php echo $bannerurl; ?>"/></a>
					<?php } } ?>


				<?php
				if ( ! defined( 'ABSPATH' ) ) {
					exit; // Exit if accessed directly
				}
				global $post, $product;
				$cat_count = sizeof( get_the_terms( $post->ID, 'product_cat' ) );
				$tag_count = sizeof( get_the_terms( $post->ID, 'product_tag' ) );
				?>



				<!-- Button trigger modal -->





                <div class="entry grey-background group-sort-tags-des">
<?php
$author_id=$post->post_author;
$naxlogin = get_userdata($author_id);
?>
					<div class="sold-by"> 	<p>Sold By <a href="<?php echo home_url().'/user/'.get_the_author_meta( 'user_login' , get_the_author_meta('ID') ); ?>">
								<?php echo get_the_author_meta( 'display_name' , get_the_author_meta('ID') ); ?>
							</a>
						</p>
					</div>




				<div class="video-heading">

					<!--<h6 class="video-title"><?php //the_title(); ?></h6>-->
					<div class="flag_post custom_btn"><span id="addflag-anchor">Flag</span></div>

					<?php
					$postiddd = get_the_ID();
					$result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . $postiddd . ' AND user_id = ' . get_current_user_id());
					//print_r($result);
					if (empty($result)){
						?>
						<div id="endorse_wrapper"><div id="endorse_button" class="endorse_video_no custom_btn" style="background-color: #000 !important;"><span>Endorse</span></div></div>
					<?php }else{ ?>
						<div id="endorse_wrapper"><div id="endorse_button" class="endorse_video_no custom_btn" style="background-color: #0F9D58 !important;"><span>Endorsed!</span></div></div>
					<?php } ?>

					<!-- Section for the favorite button -->
					<?php
					$result = $wpdb->get_results( 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . $postiddd . ' AND user_id = ' . get_current_user_id());
					if (empty($result)){
						?>
						<div id="favorite_wrapper"><div id="favorite_button" class="endorse_video_no custom_btn" style="background-color:#ff8a8a"><span>Favorite</span></div></div>
					<?php }else{ ?>
						<div id="favorite_wrapper"><div id="favorite_button" class="endorse_video_no custom_btn" style="background-color:#0F9D58"><span>Un-favorite</span></div></div>
					<?php } ?>
					<!-- End favorite -->

					<?php
					//Adding a filter for adding Review button
					//This will be added from dyn-review plugin
					$output = "";
					echo apply_filters( 'dyn_review_button', $output, $postiddd, "post" );
					?>

					<?php //Start of Section for the Donate Button get_current_user_id()
					$return_url = current_page_url()."?done=1";
					$cancel_url = current_page_url()."?done=0";
					$datapaypal = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."users_merchant_accounts WHERE user_id =".get_the_author_meta('ID')." and status=1");
					$paypalacc='';

					if($datapaypal){
						foreach ($datapaypal as $reg_datagen){
							$paypalacc=$reg_datagen->paypal_email;
						}
					}
					if($paypalacc!==''){ ?>

						<!--Donate Popup-->
						<div class="modal fade" id="model_donate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
										<h4 class="modal-title" id="myModalLabel">Donate This User</h4>
									</div>
									<div class="modal-body">
										<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top" class="form-horizontal" role="form">
											<input type="hidden" name="cmd" value="_xclick">
											<input type="hidden" name="business" value="<?php echo $paypalacc ?>">
											<input type="hidden" name="address_override" value="<?php echo $paypalacc ?>">
											<input type="hidden" name="return" value="<?php echo $return_url; ?>">
											<input type="hidden" name="return_url" value="<?php echo $return_url; ?>">
											<input type="hidden" name="cancel" value="<?php echo $cancel_url; ?>">
											<input type="hidden" name="cancel_url" value="<?php echo $cancel_url; ?>">
											<input type="hidden" name="lc" value="US">
											<input type="hidden" name="item_name" value="Do It Yourself Nation">
											<input type="hidden" name="item_number" value="<?php echo get_the_author_meta('nickname', $author->ID ); ?>">
											<input type="hidden" name="currency_code" value="USD">
											<input type="hidden" name="button_subtype" value="services">
											<input type="hidden" name="bn" value="PP-BuyNowBF:btn_paynowCC_LG.gif:NonHosted">
											<div class="form-group">
												<span for="amount" class="control-label col-sm-4">Amount:</span>
												<div class="col-sm-8">
													<input type="text" class="form-control" name="amount" required >
												</div>
											</div>
											<input type="hidden" name="on0" value="Email" required >
											<div class="form-group">
												<span for="os0" class="control-label col-sm-4">Your Paypal email:</span>
												<div class="col-sm-8">
													<input type="text" class="form-control" name="os0" maxlength="300" size="40" required>
												</div>
											</div>
											<input type="hidden" name="on1" value="Message" required>
											<div class="form-group">
												<span for="os1" class="control-label col-sm-4"	>Message to this author:</span>
												<div class="col-sm-8">
													<input type="text" class="form-control" name="os1" maxlength="300" size="40">
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-offset-4 col-sm-8">
													<button type="submit" class="btn btn-primary donate-button">DONATE WITH PAYPAL</button>
												</div>
											</div>

										</form>
									</div>
								</div>
							</div>
						</div>
					<?php }
					?>


					<?php  //do_action( 'woocommerce_after_single_product_summary'); ?>



					<!--
                                    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
                                    <script>
                   /*
                                        $( function() {
                                            $( "#dialog" ).dialog({
                                                autoOpen: false,
                                                width:900,
                                                show: {
                                                    effect: "blind",
                                                    duration: 1000
                                                },
                                                hide: {
                                                    effect: "explode",
                                                    duration: 1000
                                                }
                                            });

                                            $( "#opener" ).on( "click", function() {
                                                $( "#dialog" ).dialog( "open" );
                                            });

                                        } );
                                        */
                                    </script>
                -->
                                    <!--

                                                <div id="dialog" title="Review">
                                                    <div class="dialog-content-open"></div>
                                                </div>
                            -->




                                <script>
                                /*	jQuery(document).ready(function($) {
                                        $('#tab-reviews').prependTo(".dialog-content-open");
                                    });
                                 */
                                </script>
                                <!--
                                <div id="favorite_wrapper" class="review">
                                    <button type="submit" id="opener" class="btn btn-primary">Review</button>
                                </div>
                     -->




					<!--add to cart-->
					<div id="favorite_wrapper" class="form-add"><div>
					<span>
					<?php
					if ( ! defined( 'ABSPATH' ) ) {
						exit; // Exit if accessed directly
					}

					global $product;

					if ( ! $product->is_purchasable() ) {
						return;
					}

					?>

						<?php
						// Availability
						$availability      = $product->get_availability();
						$availability_html = empty( $availability['availability'] ) ? '' : '<p class="stock ' . esc_attr( $availability['class'] ) . '">' . esc_html( $availability['availability'] ) . '</p>';

						echo apply_filters( 'woocommerce_stock_html', $availability_html, $availability['availability'], $product );
						?>

						<?php if ( $product->is_in_stock() ) : ?>

							<?php do_action( 'woocommerce_before_add_to_cart_form' ); ?>

							<form class="cart" method="post" enctype='multipart/form-data'>
								<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>

								<button type="submit" class="single_add_to_cart_button button alt"><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>

								<?php
								if ( ! $product->is_sold_individually() ) {
									woocommerce_quantity_input( array(
											'min_value'   => apply_filters( 'woocommerce_quantity_input_min', 1, $product ),
											'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->backorders_allowed() ? '' : $product->get_stock_quantity(), $product ),
											'input_value' => ( isset( $_POST['quantity'] ) ? wc_stock_amount( $_POST['quantity'] ) : 1 )
									) );
								}
								?>

								<input type="hidden" name="add-to-cart" value="<?php echo esc_attr( $product->id ); ?>" />



								<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>
							</form>

							<?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>

						<?php endif; ?>

					</span>
						</div></div>

					<div class="clear"></div>
				</div>

				<?php get_template_part('includes/sharer'); ?>
				<!-- End Sharer -->
				<?php
				$output = "";
				//echo apply_filters( 'dyn_display_review', $output, $postiddd, "post" );
				?>
				<script>
					var check_id = "<?php echo get_current_user_id(); ?>";

					// endorse video / un-endorse
					jQuery('#endorse_button').on('click',function(){
						if (check_id == 0){
							alert("You must be logged in to endorse a product.");
						}else{
							jQuery.ajax({
								url: "<?php echo get_home_url() ?>/endorse_product.php?post_id=" + <?php echo get_the_ID(); ?> + '&v=' + Date.now(),
								type: "GET",
							}).done(function(data) {
								//console.log(data);
								if (data == 1){
									jQuery('#endorse_button').html('<span>Endorsed!</span>');
									jQuery('#endorse_button').css('background-color','#0F9D58');
								}
								if (data == 2){
									jQuery('#endorse_button').html('<span>Endorse</span>');
									jQuery('#endorse_button').css('background-color','#000');
								}
							});

						}
					});


					// favorite video / non-favorite
					jQuery('#favorite_button').on('click',function(){
						if (check_id == 0){
							alert("You must be logged in to set a product as favorite.");
						}else{
							jQuery.ajax({
								url: "<?php echo get_home_url() ?>/favorite_product.php?post_id=" + <?php echo get_the_ID(); ?> + '&v=' + Date.now(),
								type: "GET",
							}).done(function(data) {
								if (data == 1) {
									jQuery('#favorite_button').html('<span>Un-favorite!</span>');
									jQuery('#favorite_button').css('background-color','#0F9D58');
								} else if (data = 2) {
									jQuery('#favorite_button').html('<span>Favorite</span>');
									jQuery('#favorite_button').css('background-color','#ff8a8a');
								}
							});

						}
					});
               //
					// flag inappropriate posts
					function flag_post(){
						//if (  localStorage["logged_in"] == 1 ){
						if (  1 == 1 ){
							// flag ajax code goes here
							jQuery.ajax({
								url: "<?php echo get_home_url() ?>/flag_email.php",
								type: "POST",
								data: {flagged_page : window.location.pathname}
							}).done(function(data) {
								console.log(data);
							});
							alert('This post has been flagged for moderation. Thank you.');
						}else{
							alert('Please log in to flag this as inappropriate. Thank you.');
						}
					}

					/* jQuery('.flag_post').on('click',function(){
					 if (check_id == 0){
					 alert("You must be logged in to flag a video.");
					 }else{
					 //flag_post();
					 }
					 });
					 */


				</script>

				<!-- Modal -->



				<?php if ( wc_product_sku_enabled() && ( $product->get_sku() || $product->is_type( 'variable' ) ) ) : ?>
					<span class="sku_wrapper">
			<?php _e( 'SKU:', 'woocommerce' ); ?>
						<span class="sku" itemprop="sku"><?php echo ( $sku = $product->get_sku() ) ? $sku : __( 'N/A', 'woocommerce' ); ?></span>
		</span>

				<?php endif; ?>

				<?php  //do_action( 'woocommerce_after_single_product_summary' );
				?>

				<?php if(WC()->cart->get_cart_contents_count()){ ?>
				<a class="cart-contents" href="<?php echo wc_get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>">
					<i class="fa fa-shopping-cart" aria-hidden="true"><sup>
							<?php echo sprintf ( _n( '%d', '%d', WC()->cart->get_cart_contents_count() ), WC()->cart->get_cart_contents_count() ); ?>
						</sup></i> <bdi> - </bdi> <?php echo WC()->cart->get_cart_total(); ?>
				</a>
               <?php } ?>

				<script>
					jQuery(document).ready(function($) {
						$('.cart-contents').prependTo(".nav-container-wrapper .nav-container");
					});

				</script>

				<div class="panel-group">
					<!-- START About us home page -->
					<div class="panel panel-default">
						<div class="panel-heading">
							<a data-toggle="collapse" href="#paneldes">
								<h4 class="panel-title">Description <span class="pull-right panel-btn"><span class="glyphicon glyphicon-collapse-down"></span></span></h4>
							</a>
						</div>
						<div id="paneldes" class="panel-collapse collapse">
							<div class="panel-body">
								<?php if(get_the_author_id() == get_current_user_id()){ ?>
									<span class="pull-right sec-form" id="edit-des"><i class="fa fa-pencil-square-o"></i></span>
								<?php } ?>
								<div id="desf-c">
									<?php
									if($postdataval){
										echo $postdataval;
									}
									else{
										if($post->post_content==""){
											echo '<div class="content-empty">No Description Provided</div>';
										}else{
											the_content();
										}
									}

									?>
								</div>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<a data-toggle="collapse" href="#panelref">
								<h4 class="panel-title">Additional info <span class="pull-right panel-btn"><span class="glyphicon glyphicon-collapse-down"></span></span></h4>
							</a>
						</div>
						<div id="panelref" class="panel-collapse collapse">
							<div class="panel-body">
								<?php if(get_the_author_id() == get_current_user_id()){ ?>
									<span class="pull-right sec-form" id="edit-ref"><i class="fa fa-pencil-square-o"></i></span>
								<?php } ?>
								<div id="ref-c">
									<?php
									$ref = get_post_meta(get_the_ID(),'video_options_refr',true);
									if($ref){
										echo $ref;
									}else{
										echo 'No additional info given';
									}
									?>
								</div>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<a data-toggle="collapse" href="#panelshare">
								<h4 class="panel-title">Share <span class="pull-right panel-btn"><span class="glyphicon glyphicon-collapse-down"></span></span></h4>
							</a>
						</div>
						<div id="panelshare" class="panel-collapse collapse">
							<div class="panel-body">
								<div class="block-general">
									<div id="option-social">
										<div class="social-share-left"><a class="opt-facebook" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fbit_bucket.loc%2Ffile%2F4th-test%2F&amp;t=4th+Test" target="_blank"><span class="socicon-facebook"></span>facebook</a></div>
										<div class="social-share-left"><a class="opt-twitter" href="http://twitter.com/home?status=4th+Test�http%3A%2F%2Fbit_bucket.loc%2Ffile%2F4th-test%2F" target="_blank"><span class="socicon-twitter"></span>twitter</a></div>
										<div class="social-share-left"><a class="opt-googleplus" href="https://plus.google.com/share?url=http%3A%2F%2Fbit_bucket.loc%2Ffile%2F4th-test%2F" target="_blank"><span class="socicon-googleplus"></span>Google Plus</a></div>
										<div class="social-share-left"><a class="opt-linkedin" href="https://www.linkedin.com/shareArticle?mini=true&amp;url=http%3A%2F%2Fbit_bucket.loc%2Ffile%2F4th-test%2F&amp;title=4th+Test" target="_blank"><span class="socicon-linkedin"></span>linkedin</a></div>
										<div class="social-share-left"><a class="opt-dig" href="http://digg.com/submit?url=http%3A%2F%2Fbit_bucket.loc%2Ffile%2F4th-test%2F&amp;title=4th+Test" target="_blank"><span class="socicon-digg"></span>dig</a></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				</div>

				<ul class="stats sicon grey-background">
					<?php global $wpdb ; ?>
					<li><i class="fa fa-eye"></i> <?php videopress_countviews( get_the_ID() ); ?></li>
					<li><i class="fa  fa-wrench"></i> <?php $result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
						echo count($result); ?> Endorsments</li>
					<!--favourite section -->
					<li><i class="fa  fa-heart"></i>
						<?php
						$sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
						$result1 = $wpdb->get_results($sql_aux);
						echo count($result1);
						?> Favorites</li>
					<!--end favorite -->
					<li><i class="fa fa-calendar"></i> <?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
					<li><i class="fa fa-star-o"></i><?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
					<li><i class="fa fa-shopping-bag" aria-hidden="true"></i><span id="units_sold-sapn"></span></li>
				</ul>
				<script>
					jQuery(document).ready(function($) {
						$('.units_sold').prependTo("#units_sold-sapn");
						//$('.units_sold').css({'display':'inline-block'});
					});

				</script>



                <div class="entry grey-background group-sort-tags-des">
				<div id="category-tags">
					<?php //echo $product->get_categories( ', ', '<span class="posted_in">' . _n( 'Category:', 'Categories:', $cat_count, 'woocommerce' ) . ' ', '</span>' ); ?>

					<?php //echo $product->get_tags( ', ', '<i class="fa fa-tags" aria-hidden="true"></i><span class="tagged_as">' . _n( 'Tag:', 'Tags:', $tag_count, 'woocommerce' ) . ' ', ' </span>' ); ?>
					<?php
					// Link Pages Navigation
					$args = array( 'before' => '<div class="wp_link_pages">Pages:&nbsp; ', 'after' => '</div><div class="clear"></div>' );
					wp_link_pages( $args );


					// Display the Tags
					$tagval = '';?>
					<?php $all_tags = array();?>
					<?php $all_tags = get_the_tags();?>
					<?php if($all_tags){
						$before1 = '<span><i class="fa fa-tags"></i>Tags</span>';
					}

					if(get_the_author_id() == get_current_user_id()){
						$before1 = '<span><i class="fa fa-tags"></i>Tags';
						$before1 .= '&nbsp;&nbsp;<a href="#" id="tags-edit"><i class="fa fa-pencil-square-o"></i>Edit</a></span>';
					}
					$before = '<span id="tags-val">';

					$after = '</span>';


					?>

					<?php if( is_user_logged_in() ){
						$profile_id = get_current_user_id();
						$data = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."user_playlists WHERE user_id = $profile_id  ORDER BY id DESC");
						//if($data){ ?>
							<!--<div class="addto_playlist grey-background padding5_10">
								<a herf="#" id="videostest" ><strong><i class="fa fa-plus"></i> Add To Playlist</strong></a>
							</div> -->
						<?php // }
					} ?>
					<div class="post-tags grey-background padding5_10">
						<?= $before1;?>
						<?php
						$tagSelected = wp_get_post_tags($post->ID);
						$tagsNameArray = array();
						$tagsSlugArray = array();
						$tagsIDArray = array();
						foreach($tagSelected as $key=>$val)
						{
							$name = $val->name;
							$slug = $val->slug;
							$term_id = $val->term_id;
							$tagsNameArray[] = $name;
							$tagsSlugArray[] = $slug;
							$tagsIDArray[] = $term_id;
						}
						the_tags( $before,', ',$after ); ?>
					</div>


					<?php

					if($all_tags){
						foreach($all_tags as $tag){
							$tagval .= $tag->name.',';
						};
					}

					rtrim($tagval, ",");

					if(get_the_author_id() == get_current_user_id()){
						?>

						<div class="tags-forminput" style="display:none;">
							<input name="tagsinput" class="tagsinput" id="tagsinput" value="<?= $tagval;?>">
							<input name="userid" type="hidden" id="tags_postid" value="<?= get_the_id();?>">
							<button type="button" class="btn btn-success" id="tagsave">Save</button>
							<button type="button" class="btn btn-primary" id="tagcancel">Cancel</button>
						</div>

					<?php }  ?>


				</div>
				<?php comments_template(); ?>
				<?php  do_action( 'woocommerce_product_meta_end' ); ?>
				</div>
			</div>

		</div>

		<div class="modal fade" id="modeldesref_section" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title" id="myModalLabel">Add Description</h4>
					</div>
					<div class="modal-body">
						<form method="post" id="desref-form">
							<input type="hidden" name="postdataname" id="postdataname">

							<input type="hidden" name="postid" id="postid" value="<?= get_the_ID();?>">
							<div id="">
								<textarea rows="10" id="postdataval" name="postdataval"></textarea>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="button" id="desref-submit" class="btn btn-primary">Submit changes</button>
					</div>
				</div>
			</div>
		</div>
		<!-- Lightbox popup -->



<script>

	$(document).ready(function(){
		var $ = jQuery.noConflict();

		CKEDITOR.inline( 'postdataval' ,{font_defaultLabel : 'Arial',fontSize_defaultLabel : '44px'});
		CKEDITOR.editorConfig = function( config ) {
			config.font_defaultLabel = 'Arial';
			config.fontSize_defaultLabel = '44px';
		};

		var $ = jQuery.noConflict();
		$('input#tagsinput').tagsinput({
			confirmKeys: [32]
		});

		$('#tagcancel').click(function(e){
			e.preventDefault();

			$('.tags-forminput').hide();
			$('#tags-val').show();
			$('#tagsinput').tagsinput('removeAll');
			$('#tagsinput').tagsinput('add', '<?= $tagval;?>', {preventPost: true});
		})

	})

	var $ = jQuery.noConflict();


	$('#tags-edit').click(function(e){
		e.preventDefault();
		$('#tags-val').hide();
		$('.tags-forminput').show();
	});

	$('#tagcancel').click(function(e){
		e.preventDefault();
		$('.tags-forminput').hide();
	})

	$('#tagsave').click(function(e){
		e.preventDefault();

		var tags_postid = $('#tags_postid').val();
		var tagsinput = $('#tagsinput').val();

		$.ajax({
			url: admin_ajax,
			type: "POST",
			data: {action : 'user_tags_edits',tags_postid : tags_postid,tagsinput : tagsinput}
		}).done(function(data) {
			location.reload();
		});

		$('.tags-forminput').hide();
		$('#tags-val').show();

	});



	$('#edit-des').click(function(e){
		var $ = jQuery.noConflict();

		e.preventDefault();

		$('#myModalLabel').html('Add Description');
		$('#postdataname').val('desc');

		$('.cke_textarea_inline').html($('#desf-c').html());

		$('#modeldesref_section').modal();
	})


	$('#edit-ref').click(function(e){
		e.preventDefault();

		var $ = jQuery.noConflict();

		$('#myModalLabel').html('Add Reference');
		$('#postdataname').val('refe');

		$('.cke_textarea_inline').html($('#ref-c').html());

		$('#modeldesref_section').modal();
	})

	$('#desref-submit').click(function(e){
		var $ = jQuery.noConflict();
		$('#desref-form').submit();
	})

</script>

		<?php do_action( 'woocommerce_product_thumbnails' ); ?>


	</div>
