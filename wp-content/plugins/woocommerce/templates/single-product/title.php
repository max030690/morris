<?php
/**
 * Single Product title
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/title.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<h1 itemprop="name" class="product_title entry-title"><?php the_title(); ?></h1>
<?php get_template_part('rating'); ?>

<div class="dyn_rating_bar dyn_open_review_box" data-post_id="" data-rating_type="post"><div class="dyn_rating" style="width:0%"></div></div>
<div class="reviews review_box dyn_all_reviews title.php" style="display:none;">

	<div class="btn btn-danger dyn_close_review_box">Close</div></div><span>(clickable)</span>
<?php
 //include 'product-playlist-template.php'; ?>

