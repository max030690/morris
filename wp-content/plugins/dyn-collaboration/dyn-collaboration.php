<?php
/*
Plugin name: DYN Collaboration 
Plugin URI: http://www.wpamanuke.com
Description: This is a custom plugin for Collaboration section to upload file , book and video with share to other user.
Version: 1.0
Author: DYN UPWORK PROGRAMMER 
Author URI: http://www.doityourselfnation.org
*/

add_action('init', function()
{
	
	$singular	= "Collaboration";
	$plural		= "Collaborations";
	$name 		= "collaboration";
	
    $labels = array(
        "name" 					=> $plural,
        "singular_name" 		=> $singular,
        "menu_name" 			=> $plural,
        "all_items" 			=> "All ".$plural,
        "add_new" 				=> "Add New",
        "add_new_item" 			=> "Add New ".$singular,
        "edit" 					=> "Edit",
        "edit_item" 			=> "Edit ".$singular,
        "new_item" 				=> "New ".$singular,
        "view" 					=> "View",
        "view_item" 			=> "View ".$singular,
        "search_items" 			=> "Search ".$singular,
        "not_found" 			=> "No ".$singular." Found",
        "not_found_in_trash" 	=> "No ".$singular." Found in Trash",
        "parent" 				=> "Parent ".$singular
    );
    $args = array(
        "labels" 				=> $labels,
        "description" 			=> "Custom ".$plural,
        "public" 				=> true,
        "show_ui" 				=> true,
        "has_archive" 			=> true,
        "show_in_menu" 			=> true,
        "exclude_from_search" 	=> false,
        "capability_type" 		=> "post",
        "map_meta_cap" 			=> true,
        "hierarchical" 			=> true,
		"taxonomies" 			=> array( "post_tag" ),
        "rewrite" 				=> array( "slug" => $name, "with_front" => true ),
        "query_var" 			=> true,
		"show_in_nav_menus" 	=> true,
		"publicly_queryable" 	=> true,
		"can_export" 			=> true,
        "supports" 				=> array( "title", "editor", "excerpt", "author", "thumbnail", "custom-fields" )
    );
	register_post_type( $name, $args );
	
});

class Collaboration {
	
	public function __construct()
    {
        add_shortcode('add_collaboration', array($this, 'add_collaboration'));
        add_shortcode('view_pending_collaboration', array($this, 'view_pending_collaboration'));
        add_shortcode('view_collaboration', array($this, 'view_collaboration'));
        add_shortcode('add_video_collaboration', array($this, 'add_video_collaboration'));
        add_shortcode('add_file_collaboration', array($this, 'add_file_collaboration'));
        add_shortcode('count_view_collaboration', array($this, 'count_view_collaboration'));
    }	
	
	public function add_collaboration()
	{			
		
		$user_ID = get_current_user_id();
		
		$return = '<div class="background_uploader col-md-12 col-sm-full" style="margin-bottom:20px">
			<h4>Add New Collaboration.</h4>
			<div class="form-group">
				<form class="form-horizontals form-horizontalstwo" method="post" enctype="multipart/form-data">	
					<small class="control-label " for="bgfile"></small>
					<div class="col-md-3 label-class">
					<label>Collaboration Name</label>
						<input type="text" style="border: 1px solid grey; border-radius: 5px; padding: 5px;" name="collaboration_name">
					</div>
					<div class="col-md-3 tmesutker-clsahss">
						<label>Upload Featured Image</label>
						<input type="file" style="border: 1px solid grey; border-radius: 5px; padding: 5px;" name="insert_featured_image">
						<input type="hidden" name="collaboration_author" value="'.$user_ID.'">
					</div>
					<div class="col-md-3 right-colsm">
						<input class="btn btn-success custom_btn_style" name="collaboration_add" id="collaboration_add" value="Add Collaboration" type="submit">
					</div>
				</form>
			</div>';
		
		if(isset($_POST['collaboration_add']) && $_POST['collaboration_add'] == 'Add Collaboration'){
			
			if(isset($_POST['collaboration_name'])){
			
				$post = array(
					'post_title'     => $_POST['collaboration_name'] ,
					'post_status'    => 'publish' ,
					'post_type'      => 'collaboration',
					'post_author'    => $_POST['collaboration_author']
				);
				$post_id = wp_insert_post( $post );
				
				if($post_id > 0){
					$this->insert_featured_image($fileName='insert_featured_image', $post_id);
					$return .= '<br><p style="clear:both;width:100%"><span>"'.$_POST['collaboration_name'].'" successfuly added!.</span></p>';
				}
				
			}
			$_POST['collaboration_add'] = $_POST['collaboration_name'] = "";
			
		}
		
		$return .= '</div>';
		return $return;
	}
	
	
	public function view_pending_collaboration()
	{
		global $wpdb;
		$user_ID = get_current_user_id();
		
		if(!empty($_POST['add_collaboration_request_submit']) and $_POST['add_collaboration_request_submit'] == "Accept"){		
			$to_collaboration_id		= $_POST['to_collaboration_id'];
			$from_collaboration_id		= $_POST['from_collaboration_id'];			
			
			$wpdb->get_results("UPDATE ".$wpdb->prefix."authors_collaboration_list SET status=2 WHERE from_collaboration_id='".$from_collaboration_id."' AND to_collaboration_id='".$to_collaboration_id."'");

			$_POST['add_collaboration_request_submit'] = "";
		}
		
		if(!empty($_POST['remove_collaboration_submit']) and ($_POST['remove_collaboration_submit'] == "Reject" OR $_POST['remove_collaboration_submit'] == "Remove Collaboration")){		
			$from_collaboration_id		= $_POST['from_collaboration_id'];
			$to_collaboration_id		= $_POST['to_collaboration_id'];
			
			$wpdb->get_results("DELETE FROM ".$wpdb->prefix."authors_collaboration_list WHERE (from_collaboration_id='".$from_collaboration_id."' AND to_collaboration_id='".$to_collaboration_id."') OR (from_collaboration_id='".$to_collaboration_id."' AND to_collaboration_id='".$from_collaboration_id."')");
			
			$_POST['remove_collaboration_submit'] = "";
		}		
		
		$result = $wpdb->get_results("SELECT *FROM ".$wpdb->prefix."authors_collaboration_list WHERE (to_collaboration_id='".$user_ID."' AND status=1)");
		
		echo '<div style="display: table;width: 100%;table-layout: fixed;border-spacing: 10px;clear:both;">';
		echo "<h4>Pending collaboration requests.</h4>";
		if(count($result) > 0){
			
			echo '<div class="view_collaboration_class" style="display: table;width: 100%;table-layout: fixed;border-spacing: 10px;clear:both;">';
			foreach($result as $row){
				
				if($row->to_collaboration_id == $user_ID){
					$post		= @get_post($row->collaboration_post_id);
					
					if($post->post_type == 'collaboration'){
					
					$meta_key = 'profile_pic_meta';	
					$fileName = get_user_meta($row->from_collaboration, $meta_key, true );
					$userdata = get_userdata($row->from_collaboration_id);
					$linkprof = site_url().'/user/'.$userdata->user_nicename;
					
					if(strlen($userdata->display_name)>25) $dsplayna = substr($userdata->display_name,0,22).'...';
					else $dsplayna = $userdata->display_name;	
					if($fileName != ''){
						$fileNameImg = site_url().'/profile_pic/'.$fileName;	
					}else{
						$fileNameImg = get_stylesheet_directory_uri().'/images/no_avatar.jpg';
					}
						
					$post_title = $post->post_title;
					
					$post_link	= get_permalink($row->collaboration_post_id);
					
					$post_image = get_the_post_thumbnail( $row->collaboration_post_id, 'full' ); 
					
					echo '<div style="display:inline-block;text-align:center;" class="live-stream-block" >
						<a class="user_img" href="'.$post_link.'">';
					
					if ( $post_image != "" ){
						echo $post_image;
					}else{
						echo '<img src="'.get_bloginfo('template_url').'/images/collaborationsample.jpg" alt="'.get_the_title().'" height="100px" width="100px" />';
					}
					
					echo '</a><a class="user_tile" href="'.$post_link.'">"'.$post_title.'"</a><a class="user_name" href="'.$linkprof.'"><small>'.$dsplayna.'</small></a><a class="buton_anchor">';
					
					echo '<form method="post" class="subscribers-inner-form">
						<input type="hidden" name="collaboration_post_id" value="'.$row->collaboration_post_id.'">
						<input type="hidden" name="to_collaboration_id" value="'.$row->to_collaboration_id.'">
						<input type="hidden" name="from_collaboration_id" value="'.$row->from_collaboration_id.'">
						<input type="submit" name="add_collaboration_request_submit" value="Accept" class="btn btn-success btn-xs">
						</form>';
					
					echo '<form method="post" class="subscribers-inner-form">
						<input type="hidden" name="collaboration_post_id" value="'.$row->collaboration_post_id.'">
						<input type="hidden" name="from_collaboration_id" value="'.$row->to_collaboration_id.'">
						<input type="hidden" name="to_collaboration_id" value="'.$row->from_collaboration_id.'">
						<input type="submit" name="remove_collaboration_submit" value="Reject" class="btn btn-success btn-xs">
						</form>';
					
					echo '</a></div>';
					}
				}
			}
			echo '</div>';
		}else{
			echo "<h4><small>No collaboration requests.</small></h4>";
		}
		echo '</div>';
		
	}
	public function count_view_collaboration()
	{
		global $wpdb;
		
		$user_ID = get_current_user_id();
		$view_collaboration = array();
		
		$postsArray = get_posts(
			array(
				'posts_per_page' 	=> -1,
				'post_type' 		=> 'collaboration',
				'post_status' 		=> 'publish',
				'author' 			=> $user_ID
		)	);
		if(!empty($postsArray) and is_array($postsArray) and count($postsArray)>0){
			foreach($postsArray as $key=>$val){
				global $post; $post = $val; setup_postdata( $post );
				$view_collaboration [] = get_the_ID();
		}	}
		return count($view_collaboration);
	}
	
	public function view_collaboration()
	{
		global $wpdb;
		
		$user_ID = get_current_user_id();
		$view_collaboration = array();
		
		$postsArray = get_posts(
			array(
				'posts_per_page' 	=> -1,
				'post_type' 		=> 'collaboration',
				'post_status' 		=> 'publish',
				'author' 			=> $user_ID
		)	);
		if(!empty($postsArray) and is_array($postsArray) and count($postsArray)>0){
			foreach($postsArray as $key=>$val){
				global $post; $post = $val; setup_postdata( $post );
				$view_collaboration [] = get_the_ID();
		}	}		
		
		$result = $wpdb->get_results("SELECT *FROM ".$wpdb->prefix."authors_collaboration_list WHERE to_collaboration_id='".$user_ID."' AND status=2");
		if(count($result) > 0){			
			foreach($result as $row){
				$view_collaboration [] = $row->collaboration_post_id;
			}
		}
		
		sort($view_collaboration);
		$view_collaboration = array_unique($view_collaboration);
		
		echo '<div style="display: table;width: 100%;table-layout: fixed;border-spacing: 10px;clear:both;">
			<h4>Current collaborations.</h4>';
		
		if(count($view_collaboration) > 0){
			$postArray = get_posts(
				array(
					'posts_per_page' 	=> -1,
					'post_type' 		=> 'collaboration',
					'post_status' 		=> 'publish',
					'post__in' 			=> $view_collaboration
			)	);
			if(!empty($postArray) and is_array($postArray) and count($postArray)>0){
				
				echo '<script type="text/javascript">
						jQuery(document).ready(function($){
							$(".remove_collaboration").live("click",function(){
								var confirms = confirm("Are you sure you want to continue!");
								if(confirms){
								var collaboration_id = $(this).attr("collaboration_id");
								$.ajax({
									type: "POST",
									url: "'.plugins_url().'/dyn-collaboration/dyn-collaboration-ajax.php",
									data: {
										remove_collaboration 	: "remove_collaboration",
										collaboration_id 		: collaboration_id
									},
									success: function(data){
										setTimeout(function(){ window.location.assign(window.location.href); }, 1000);
									},
									error: function(xhr, textStatus, error){
										$(".remove_collaboration").trigger("click");
									}
								});
								}
							});
						});
					</script>';
				
				echo '<div class="view_collaboration_class" style="display: table;width: 100%;table-layout: fixed;border-spacing: 10px;clear:both;">';
				foreach($postArray as $key=>$val){
					global $post; $post = $val; setup_postdata( $post );					
					
					echo '<div style="display:inline-block;text-align:center;float:left;height:300px;margin:0 9px;" class="live-stream-block" >
						<a href="'.get_the_permalink().'">';
					
					if ( has_post_thumbnail() ){
						the_post_thumbnail('full');
					}else{
						echo '<img src="'.get_bloginfo('template_url').'/images/collaborationsample.jpg" alt="'.get_the_title().'" height="100px" width="100px" />';
					}
					
					echo '</a>
						<a href="'.get_the_permalink().'">'.get_the_title().'</a>
						<a><small>'.human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ).' ago</small></a><br>
						<input type="button" collaboration_id="'.get_the_ID().'" value="Delete" class="btn btn-success btn-sm remove_collaboration"></div>';
				}
				echo '</div>';
			}
		}else{
			echo '<h4><small>No collaborations.</small></h4>';
		}
		echo '</div>';
	}
	
	public function insert_featured_image($fileName, $post_id)
	{
		require_once(ABSPATH . "wp-admin" . '/includes/image.php');
		require_once(ABSPATH . "wp-admin" . '/includes/file.php');
		require_once(ABSPATH . "wp-admin" . '/includes/media.php');
		$attach_id = media_handle_upload( $fileName, $post_id );
		$attach_data = wp_generate_attachment_metadata( $attach_id, $file );
		wp_update_attachment_metadata( $attach_id, $attach_data );
		set_post_thumbnail( $post_id, $attach_id );
	}
	
	public function add_file_collaboration( $attr = '' ){
		//print_r($attr);
		$collaboration_id = '';
		if($attr != '' && count($attr) > 0){
			$collaboration_id = "<input type='hidden' name='collaboration_id' value='".$attr['collaboration_id']."' />";
		}
		if( isset($_POST['dyn-submit']) 
			&& wp_verify_nonce( $_POST['dyn_file_upload_nonce'], 'dyn_file_upload' )
		){
			
			
			require_once( ABSPATH . 'wp-admin/includes/image.php' );
			require_once( ABSPATH . 'wp-admin/includes/file.php' );
			require_once( ABSPATH . 'wp-admin/includes/media.php' );

			$attachment_id = media_handle_upload( 'dyn-select-file', 0);
			if ( is_wp_error( $attachment_id ) ) {
			?>
				<div class="alert alert-danger">
					<span>Something went wrong</span>
				</div>
			<?php
			} else {
				$privacyOption = $_REQUEST['privacy-option'];
		        $select_users_list = implode(",", $_REQUEST['select_users_list']); 
				$title = $_POST['dyn-select-title'];
				$description = $_POST['dyn-select-description'];
				$tags = trim($_POST['dyn-select-tags']);
				$reference = trim($_POST['dyn-select-reference']);
				$typeupload = $_POST['typeupload'];
				$collaboration_post_id   = $_POST['collaboration_id'];
				
				$dyn_post_id = wp_insert_post(
					array(
						'post_title' => $title,
						'post_content' => $description,
						'post_author' => $profile_id,
						'post_type' => 'dyn_file',
						'post_status' => 'publish'
					)
				);
				
				if(!empty($tags)){
					$tags = explode(' ', $tags);
					wp_set_post_tags( $dyn_post_id, $tags );
				}
				$post_tmp = get_post( $dyn_post_id );
				$author_id = $post_tmp->post_author;
					
				if( isset( $_POST['dyn_background'] ) ){
					if( $_POST['dyn_background'] == 'background_no' ){						
						if ( ! delete_post_meta( $dyn_post_id, 'dyn_background_src' ) ) {}
						//echo '<script>alert("background_no");</script>';
					}
					if( $_POST['dyn_background'] == 'background_user' ){
						if( get_user_meta( $author_id, 'background_image', true ) ){
							$img = get_user_meta( $author_id, 'background_image' );
							update_post_meta( $dyn_post_id, 'dyn_background_src', $img[0] );					
							//echo '<script>alert("background_user - '.$img[0].'");</script>';
						}
					}
					if( $_POST['dyn_background'] == 'background_new' ){
						if(($_FILES['dyn_bg']['type'] == 'image/jpg') 
							|| ($_FILES['dyn_bg']['type'] == 'image/jpeg') 
							|| ($_FILES['dyn_bg']['type'] == 'image/png') 
							|| ($_FILES['dyn_bg']['type'] == 'image/gif')){
							$file = 'wp-content/themes/videopress/images/'.rand($author_id, 99999999).$_FILES['dyn_bg']['name'];
							move_uploaded_file($_FILES['dyn_bg']['tmp_name'], $file);	
							update_post_meta( $dyn_post_id, 'dyn_background_src', $file);
							//echo '<script>alert("background_new");</script>';
						}else{
							echo '<div class="alert alert-danger">
									<span>Background image failed its type is different then jpeg, jpg, gif or png</span>
								</div>';
						}
					}
				}
				
				update_post_meta( $dyn_post_id, 'dyn_upload_type', 'dyn-file-upload' );
				update_post_meta( $dyn_post_id, 'dyn_upload_value', $attachment_id );
				update_post_meta( $dyn_post_id, 'video_options_refr', $reference );
				update_post_meta( $dyn_post_id, 'collaboration_id', $collaboration_post_id);
				if ($typeupload=="cfiles"){
					global $current_user;  get_currentuserinfo();
					$user_ID = $current_user->ID;
					$body["message"] = "New file has been uploaded by ".$current_user->display_name;
					$body["message"] .= "<h3><a href='".get_site_url()."/file/".slug_title($title)."' target='_blank' > ".$title."</a></h3>";
					$body["message"] .= "<br />".$description;
					$body["message"] .= "<br />".$reference;
					$body["message"] .= "<hr />";
					$body["subject"] = "New file has been uploaded ";
					notification_subscribers($user_ID,$body);
				}
				
				update_post_meta( $dyn_post_id, 'privacy-option', $privacyOption);
		        update_post_meta( $dyn_post_id, 'select_users_list', $select_users_list);
				
				?>
				<div class="alert alert-success">
					<span>File Uploaded successfully. <a href="<?php echo get_permalink($dyn_post_id); ?>">View File</a></span>
				</div>
				<?php
			}

		}
		?>
		
		<div class="dyn-file-upload-section">
			<div class="dyn-click-upload" style="text-align:left;">
				<div>Max Upload Size: 2GB</div>
				<div>Supported File Type: PPT, EXCEL, ISO, MP3, MP4, PDF, Word Document, ZIP</div>
			</div>

			<div class="dyn-upload-details">
			<div class="top_pop">
				<form role="form" method="post" action="" enctype="multipart/form-data">
					<?php wp_nonce_field( 'dyn_file_upload', 'dyn_file_upload_nonce' ); ?>
					<?php echo $collaboration_id; ?>
					<div class="form-group">
						<label for="dyn-select-file">Select File:</label><br>
						<input type="file" id="file" name="dyn-select-file" class="dyn-select-file" class="form-control">
						<input type="hidden" name="typeupload" value="cfiles">
					</div> 
					<div class="form-group">
						
						<progress id="progressBar" value="0" max="100" style="width:100%;"></progress>
						<h3 id="status"></h3>
						<p id="loaded_n_total"></p>	
						
					</div>
					 
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/jquery.tokenize.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/jquery.tokenize.css" />
<script>
	function _(el){
		return document.getElementById(el);
	}
	
	function uploadFile(){
		var file = _("file").files[0];		
		var formdata = new FormData();
		formdata.append("file", file);
		var ajax = new XMLHttpRequest();
		ajax.upload.addEventListener("progress", progressHandler, false);
		ajax.addEventListener("load", completeHandler, false);
		ajax.addEventListener("error", errorHandler, false);
		ajax.addEventListener("abort", abortHandler, false);
		ajax.open("POST", "");
		ajax.send(formdata);
	}
	
	function progressHandler(event){
		_("loaded_n_total").innerHTML = "Uploaded "+event.loaded+" bytes of "+event.total;
		var percent = (event.loaded / event.total) * 100;
		_("progressBar").value = Math.round(percent);
		_("status").innerHTML = Math.round(percent)+"% uploaded... please wait";
	}
	
	function completeHandler(event){
		_("status").innerHTML = event.target.responseText;
		_("progressBar").value = 0;
	}
	
	function errorHandler(event){
		_("status").innerHTML = "Upload Failed";
	}
	function abortHandler(event){
		_("status").innerHTML = "Upload Aborted";
	}
</script>
					
					<div class="form-group">
						<label for="dyn-select-title">Title:</label>
						<input type="text" name="dyn-select-title" id="dyn-select-title" class="form-control">
					</div>
					<div class="form-group">
						<label for="dyn-select-description">Description:</label>
						<textarea name="dyn-select-description" id="dyn-select-description" class="form-control" rows="3"></textarea>
					</div>
					<div class="form-group">
						<label for="dyn-select-reference">Reference:</label>
						<textarea name="dyn-select-reference" class="form-control" id="dyn-select-reference" rows="2"></textarea>
					</div>
					<div class="form-group">
						<label for="dyn-tags">Tags:</label>
						<input type="text" name="dyn-select-tags" class="dyn-select-tags">
						<span>Enter tags separated by space</span>
					</div>
					
					<div class="form-group form_bottom col-sm-4 col-sm-4 col-xs-12">
						<label class="radio_head" for="dyn-tags">Add Background:</label>					
						<div class="checkbox">
							<label>
							<input type="radio" name="dyn_background" class="dyn_checkbox" value="background_no" checked="checked" >White Background </label>
							<label>
							<input type="radio" name="dyn_background" class="dyn_checkbox" value="background_user" >Profile Background </label>
							<label> 
							<input type="radio" name="dyn_background" class="dyn_checkbox" value="background_new" >New Background</label>
						</div>
					</div>
					<div class="form-group form_bottom col-sm-4 col-sm-4 col-xs-12">  
			<label class="radio_head" for="dyn-tags"> Privacy Options:</label>	 		
			<div class="checkbox">
				<label>
				  <input type="radio" id="privacy-radio" name="privacy-option" class="dyn-select-file" class="form-control" value="private">Private
				</label>
				<label>
				 <input type="radio" id="privacy-radio1" name="privacy-option" class="dyn-select-file" class="form-control" value="public">Public
				</label> 
			</div>
			
			<div style="display:none;" class="select-box">
				<?php
				  echo '<label for="dyn-tags">Please select the User you want to give access</label><br>
				      <select id="tokenize" class="tokenize-sample" name="select_users_list[]" style="width: 350px; height: 150px;" multiple="multiple">';
					  $args1 = array(
						  'role' => 'free_user',
						  'orderby' => 'id',
						  'order' => 'desc'
					   );
					  $subscribers = get_users($args1);
					  foreach ($subscribers as $user) { 
					      if(get_current_user_id() == $user->id)
						  { }
						  else
						  {
							   echo '<option value="'.$user->id.'">' . $user->display_name.'</option>';
						  } 
					  }
				  echo '</select>';
				?>
			</div><!--End-select-box--->
			<script type="text/javascript">
				  $(document).ready(function(){
					  $("#privacy-radio").click(function(){
						 $(".select-box").slideDown();
					  });
					  $("#privacy-radio1").click(function(){
						 $(".select-box").slideUp();
						 //$('select#select_users_list option').removeAttr("selected");
					  });
				  });
				$('#tokenize').tokenize();
				 datas: "bower.json.php"
			</script>
			<style>.tokenize-sample { width: 300px }</style>
		</div> <!--End-form-group-->
					
					<style> 
                      #privacy-radio1, #privacy-radio {
                        padding: 5px;
                        text-align: center;
                      }

                     #select-box {
                       padding: 50px;
                       display: none;
                     }
                    </style>
					<!--<div id="upload_container"></div>-->
					<script type="text/javascript">
						jQuery(document).ready(function() {
							jQuery("#upload_container").html('');
							jQuery(".dyn_checkbox").click( function() {
								var test = $(this).val();
								if( test == 'background_new' ){
									jQuery("#upload_container").html('<div class="form-group"><label for="dyn_bg">Upload File:</label><input type="file" id="dyn_bg" name="dyn_bg" class="dyn_bg" accept=".jpeg,.jpg,.gif,.png,.JPEG,.JPG,.GIF,.PNG"></div>');
								}else{
									jQuery("#upload_container").html('');
								}
							});
						});
					</script>
					<div class="publish_cnt"><input class="btn btn-default" type="submit" id="submit" value="Submit" name="dyn-submit" onclick="uploadFile()"></div>
				</form><!--End-Forms-->
				</div>
			</div><!--End-dyn-upload-details-->
		</div><!--End-dyn-file-upload-section-->
		<?php
	}
	
	public function add_video_collaboration( $attr = '' )
	{
	//print_r($attr);
	$collaboration_id = '';
	if($attr != '' && count($attr) > 0){
		$collaboration_id = "<input type='hidden' name='collaboration_id' value='".$attr['collaboration_id']."' />";
	}
		
	if ( isset( $_POST['upload_video'] ) && isset( $_POST['action'] ) ) {

		global $current_user;  get_currentuserinfo();
		
		if($_FILES['file']){

			$path = $upload_dir['path'];  
			$privacyOption = $_REQUEST['privacy-option'];
			$select_users_list = implode(",", $_REQUEST['select_users_list']);
			$xmlFile = pathinfo($_FILES['file']['name']);
			$randValue = random_string(); 
			$extension = $xmlFile['extension'];

			$fileNameVal = $randValue.'.'.$extension; 
			$imageName = $randValue; 

			$path=$path.'/'.$fileNameVal; 
			
			if(move_uploaded_file($_FILES['file']['tmp_name'],$path))
			{
				$title     = $_POST['post_title'];
				$content   = $_POST['post_content'];
				$collaboration_post_id   = $_POST['collaboration_id'];
				$video_options = $upload_dir['url'].'/'.$fileNameVal;
				//exec("ffmpeg -i ".$path." -vf fps=1 ".$upload_dir['path']."/".$imageName.".png");
				exec("ffmpeg -y -i ".$path." -ss 00:00:14 -vframes 1 -an -vcodec png -f rawvideo -s 500x270 ".$upload_dir['path']."/".$imageName.".png", $output); 			 
					
					require_once(ABSPATH . "wp-admin" . '/includes/image.php');
					require_once(ABSPATH . "wp-admin" . '/includes/file.php');
					require_once(ABSPATH . "wp-admin" . '/includes/media.php');

				$title     = $_POST['post_title'];
				$content   = $_POST['post_content'];
							

				$data= 	 array('video_type'      => 'upload',
							   'video_upload' => $video_options,
							   'youtube_url' => '',
							   'embed_code'=>'');
							   
				$post = array(
					'post_title' => $title,
					'post_content' => $content,
					'post_status' => 'publish'
				);

				$pid = wp_insert_post($post,true);
				$video_options_refr = $_POST['ref'];
				add_post_meta($pid, 'video_options', $data);
				add_post_meta($pid, 'video_options_refr', $video_options_refr);				
				add_post_meta($pid, 'collaboration_id', $collaboration_post_id);

				$post_tmp = get_post( $pid );
				$author_id = $post_tmp->post_author;

				if(isset($_POST['dyn_thumbnail'])){
					if($_POST['dyn_thumbnail'] == 'thumbnail_custom'){
						if(($_FILES['dyn_thumb_file']['type'] == 'image/jpg') 
							|| ($_FILES['dyn_thumb_file']['type'] == 'image/jpeg') 
							|| ($_FILES['dyn_thumb_file']['type'] == 'image/png') 
							|| ($_FILES['dyn_thumb_file']['type'] == 'image/gif')){
								$attach_id = media_handle_upload('dyn_thumb_file', 0);
								set_post_thumbnail( $pid, $attach_id );
						}
					}
					else{
						$imagetouse = $upload_dir['path']."/".$imageName.".png";
							
						// Check image file type 
						$wp_filetype = wp_check_filetype( $imagetouse, null );
						// Set attachment data 
						$attachment = array( 
							'post_mime_type' => $wp_filetype['type'], 
							'post_title'     => sanitize_file_name( $imagetouse ), 
							'post_content'   => '', 
							'post_status'    => 'inherit' 
						); 
						// Create the attachment 
						$attach_id = wp_insert_attachment( $attachment, $imagetouse, $pid );
						// Define attachment metadata 
						$attach_data = wp_generate_attachment_metadata( $attach_id, $imagetouse );
					 
						// Assign metadata to attachment 
						wp_update_attachment_metadata( $attach_id, $attach_data );
					 
						// And finally assign featured image to post 
						set_post_thumbnail( $pid, $attach_id );	
					}
				}

				if(isset($_POST['dyn_background'])){
					if( $_POST['dyn_background'] == 'background_no' ){						
						if ( ! delete_post_meta( $pid, 'dyn_background_src' ) ) {}
						//echo '<script>alert("background_no");</script>';
					}
					if( $_POST['dyn_background'] == 'background_user' ){
						if( get_user_meta( $author_id, 'background_image', true ) ){
							$img = get_user_meta( $author_id, 'background_image' );
							update_post_meta( $pid, 'dyn_background_src', $img[0] );					
							//echo '<script>alert("background_user - '.$img[0].'");</script>';
						}
					}
					if( $_POST['dyn_background'] == 'background_new' ){
						if(($_FILES['dyn_bg']['type'] == 'image/jpg') 
							|| ($_FILES['dyn_bg']['type'] == 'image/jpeg') 
							|| ($_FILES['dyn_bg']['type'] == 'image/png') 
							|| ($_FILES['dyn_bg']['type'] == 'image/gif')){
							$file = 'wp-content/themes/videopress/images/'.rand($author_id, 99999999).$_FILES['dyn_bg']['name'];
							move_uploaded_file($_FILES['dyn_bg']['tmp_name'], $file);	
							update_post_meta( $pid, 'dyn_background_src', $file);
						}else{
							echo '<div class="alert alert-danger">
									<span>Background image failed its type is different then jpeg, jpg, gif or png</span>
								</div>';
						}
					}
				}	

						
				if(isset($_POST['tagsinput']) && !empty($_POST['tagsinput'])){
					
					$tagsinput = $_POST['tagsinput'];
					wp_set_post_tags( $pid, $tagsinput, true );
					
				}
				the_alert(get_site_url()."/".slug_title($title));
					  
				$user_ID = $current_user->ID;
				$body["message"] = "New video has been uploaded by ".$current_user->display_name.".";
				$body["message"] .= "<h3><a href='".get_site_url()."/".slug_title($title)."' target='_blank' > ".$title."</a></h3>";
				$body["message"] .= "<br />".$content;
				$body["message"] .= "<hr />";
				$body["subject"] = "New video has been uploaded ";
				notification_subscribers($user_ID,$body);			
			}
			update_post_meta( $pid, 'privacy-option', $privacyOption);
			update_post_meta( $pid, 'select_users_list', $select_users_list);
		}
	}
		
		
		
		?>
		<form method="post" enctype="multipart/form-data" class="custom-post-type popform" id="uploadForm" >
			<?php echo $collaboration_id; ?>
		   <div class="top_pop">
		   <div class="top_input">
			  <label>Post Title</label>
			  <input type="text" id="title" class=" span12" size="40" name="post_title" required />
			  <label>Description</label>
			  <textarea id="description" tabindex="3" name="post_content" cols="50" rows="6"></textarea>
		   </div>
		   <div class="tabs-panel bottom_sel">
			  <label>Video Reference</label>
			  <textarea name="ref" id="ref" rows="10" cols="30" placeholder="video reference text"></textarea>
			  <label>Add Tags</label> 
		   <input name="tagsinput" class="tagsinput" id="tagsinput" > 
		   <div id="tabs-1">
			  <input class="vp-js-upload vp-button button" name="file" id="file" type="file" value="Choose File" required>
			  <h3 id="status"></h3>
			  <p id="loaded_n_total"></p>
		   </div>
		   </div>
		   </div>
		   
		   
		   
		   <script>
			  function _(el){
				return document.getElementById(el);
			  }
			  
			  function uploadFile(){
				var file = _("file").files[0];
				var filetype = file.type;
				type = filetype.split('/');
				if(type[0] == "video"){
					var formdata = new FormData();
					formdata.append("file", file);
					var ajax = new XMLHttpRequest();
					ajax.upload.addEventListener("progress", progressHandler, false);
					ajax.addEventListener("load", completeHandler, false);
					ajax.addEventListener("error", errorHandler, false);
					ajax.addEventListener("abort", abortHandler, false);
					ajax.open("POST", "");
					ajax.send(formdata);
				}	
				
			  }
			  
			  function progressHandler(event){
				_("loaded_n_total").innerHTML = "Uploaded "+event.loaded+" bytes of "+event.total;
				var percent = (event.loaded / event.total) * 100;
				_("progressBar").value = Math.round(percent);
				_("status").innerHTML = Math.round(percent)+"% uploaded... please wait";
			  }
			  
			  function completeHandler(event){
				_("status").innerHTML = event.target.responseText;
				_("progressBar").value = 0;
			  }
			  
			  function errorHandler(event){
				_("status").innerHTML = "Upload Failed";
			  }
			  function abortHandler(event){
				_("status").innerHTML = "Upload Aborted";
			  }
		   </script>
		   
		   <div class="form-group form_bottom col-sm-4 col-sm-4 col-xs-12">
			  <label class="radio_head" for="dyn-thumbnail">Thumbnail:</label>
			  <div class="checkbox">
				 <label>
				 <input type="radio" name="dyn_thumbnail" class="dyn_thumbnail_checkbox" value="thumbnail_default" checked="checked" >Default</label>
				 <label>
				 <input type="radio" name="dyn_thumbnail" class="dyn_thumbnail_checkbox" value="thumbnail_custom" >Custom Thumbnail </label>
			  </div>
		   </div>
		  <!-- <div id="thumbnail_upload"></div>-->
		   <div class="form-group form_bottom col-sm-4 col-sm-4 col-xs-12">
			  <label class="radio_head" for="dyn-tags"> Add Background:</label>					
			  <div class="checkbox">
				 <label>
				 <input type="radio" name="dyn_background" class="dyn_checkbox" value="background_no" checked="checked" >White Background </label>
				 <label>
				 <input type="radio" name="dyn_background" class="dyn_checkbox" value="background_user" >Profile Background </label>
				 <label>
				 <input type="radio" name="dyn_background" class="dyn_checkbox" value="background_new" >New Background </label>
			  </div>
		   </div>
		   <!--<div id="upload_container"></div>-->
		   <script type="text/javascript">
			  jQuery(document).ready(function() {
				jQuery("#upload_container").html('');
				jQuery("#thumbnail_upload").html('');
				jQuery(".dyn_checkbox").click( function() {
					var test = $(this).val();
					if( test == 'background_new' ){
						jQuery("#upload_container").html('<div class="form-group"><label for="dyn_bg">Upload File:</label><input type="file" id="dyn_bg" name="dyn_bg" class="dyn_bg" accept=".jpeg,.jpg,.gif,.png,.JPEG,.JPG,.GIF,.PNG"></div>');
					}else{
						jQuery("#upload_container").html('');
					}
				});
				
				jQuery(".dyn_thumbnail_checkbox").click( function() {
					var thumbval = $(this).val();
					if( thumbval == 'thumbnail_custom' ){
						jQuery("#thumbnail_upload").html('<div class="form-group"><label for="dyn_thumb_file">Upload Thumbnail:</label><input type="file" id="dyn_thumb_file" name="dyn_thumb_file" class="dyn_thumb_file" accept="image/*"></div>');
					}else{
						jQuery("#thumbnail_upload").html('');
					}
				});
			  });
		   </script>
		   <div class="form-group form_bottom col-sm-4 col-sm-4 col-xs-12">
			  <label class="radio_head" for="dyn-tags"> Privacy Options:</label>	 		
			  <div class="checkbox">
				 <label>
				 <input type="radio" id="privacy-radio" name="privacy-option" class="dyn-select-file" class="form-control" value="private">Private
				 </label>
				 <label>
				 <input type="radio" id="privacy-radio1" name="privacy-option" class="dyn-select-file" class="form-control" value="public">Public
				 </label> 
			  </div>
			  <div style="display:none;" class="select-box">
				 <?php
					echo '<label for="dyn-tags">Please select the User you want to give access</label><br><select id="tokenize" class="tokenize-sample" name="select_users_list[]" style="width: 350px; height: 150px;" multiple="multiple">';
						$args1 = array(
							'role'		=> 'free_user',
							'orderby'	=> 'id',
							'order'		=> 'desc'
						);
					 $subscribers = get_users($args1);
					 foreach ($subscribers as $user) { 
						if(get_current_user_id() == $user->id)
						{ }
						else
						{
						  echo '<option value="'.$user->id.'">' . $user->display_name.'</option>';
						} 
					 }
					echo '</select>';
					?> 
			  </div>
			  <script type="text/javascript">
				 jQuery(document).ready(function($){
				  $("#privacy-radio").click(function(){
				  $(".select-box").slideDown();
				  });
				  $("#privacy-radio1").click(function(){
				  $(".select-box").slideUp();
				  //$('select#select_users_list option').removeAttr("selected");
				  });
				 });
				 jQuery('#tokenize').tokenize();
				 datas: "bower.json.php"
			  </script>
			  <style>.tokenize-sample { width: 300px }</style>
		   </div>
		  <div class="publish_cnt"> <input type="submit" value="Publish" tabindex="8" id="submit" name="upload_video" onclick="uploadFile()" /></div>
		   <input type="hidden" name="action" value="my_post_type" />
		</form>
		<?php
		
	}
}
$Collaboration = new Collaboration();



