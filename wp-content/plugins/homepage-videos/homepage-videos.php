<?php
/*
Plugin Name: MSH - Homepage Videos for Do It Yourself Nation
Plugin URI: http://www.mainstreehost.com
Description: This plugin adds controls the homepage videos in the header. 
Version: 1.0
Author: Matthew Backlas for Mainstreethost
Author URI: http://www.mainstreehost.com
License: GPL2
*/

add_action( 'admin_menu', 'register_msh_homepagevideos_admin' );

function register_msh_homepagevideos_admin()
		{
			add_menu_page( 'Homepage Videos', 'Homepage Videos', 'manage_options', '/homepage-videos/homepage-videos-admin.php', '', '', '99.1989' );
		}

?>