<?php

 function show_fb() {
  $current_user = wp_get_current_user();
  if(!is_user_logged_in())
	wp_redirect(home_url());
        error_reporting(E_ALL & ~ E_NOTICE);
        require_once(plugin_dir_path( __FILE__ ).'../../../../wp-admin/includes/plugin.php');
        require_once(plugin_dir_path( __FILE__ ).'../massive-panel/theme-settings.php');
		require_once(plugin_dir_path( __FILE__ ).'../massive-panel/theme-options.php');
		//theme-options-font
        mp_register_settings();
     	$settings_output = mp_get_settings();
	$content = mp_display_front_content();
        $plugin_data = get_plugin_data( MPC_PLUGIN_FILE );
 	wp_enqueue_style('mp_theme_et_icons', MPC_PLUGIN_ROOT.'/assets/fonts/et-icons.css');
	wp_enqueue_style('mp_theme_et_line', MPC_PLUGIN_ROOT.'/assets/fonts/et-line.css');
	wp_enqueue_style('mp_theme_settings_css', MPC_PLUGIN_ROOT.'/massive-panel/css/mp-styles.css');
	wp_enqueue_style('mp_theme_select2_css', MPC_PLUGIN_ROOT.'/massive-panel/select2/select2.css');
	wp_enqueue_style('flipbook_styles', MPC_PLUGIN_ROOT.'/assets/css/style.min.css');
	wp_enqueue_style('wp-color-picker1', MPC_PLUGIN_ROOT.'/front/css/color-picker.css');
        wp_enqueue_style('wp-jquery-ui-dialog');
        wp_enqueue_script('mp_theme_select2_js', MPC_PLUGIN_ROOT.'/massive-panel/select2/select2.min.js', array('jquery'), false, true);
        wp_enqueue_style( 'wp-color-picker' );
        wp_enqueue_script(
			'iris',
			admin_url( 'js/iris.min.js' ),
			array( 'jquery-ui-draggable', 'jquery-ui-slider', 'jquery-touch-punch' ),
			false,
			1
		);
        wp_enqueue_script(
			'wp-color-picker',
			admin_url( 'js/color-picker.min.js' ),
			array( 'iris' ),
			false,
			1
		);
        $colorpicker_l10n = array(
			'clear' => __( 'Clear' ),
			'defaultString' => __( 'Default' ),
			'pick' => __( 'Select Color' ),
			'current' => __( 'Current Color' ),
		);
        wp_localize_script( 'wp-color-picker', 'wpColorPickerL10n', $colorpicker_l10n ); 
		
        /* UPLOAD IMAGE */
        wp_enqueue_script( 'plupload-handlers' );
        wp_enqueue_script('mp_theme_icon_select_js', MPC_PLUGIN_ROOT.'/massive-panel/mpc_icon/icon_select/field_icon_select.js', array('jquery', 'jquery-ui-dialog'), false, true );
	wp_enqueue_script('mp_theme_toc_generator_js', MPC_PLUGIN_ROOT.'/massive-panel/mpc_toc/field_toc.js', array('jquery', 'jquery-ui-dialog'), false, true );
                        
	wp_enqueue_script('webfonts', '//ajax.googleapis.com/ajax/libs/webfont/1.1.2/webfont.js');
        wp_enqueue_style('rfbwp-styles2', MPC_PLUGIN_ROOT.'/massive-panel/css/mp-styles.css');
       wp_enqueue_script('mp_theme_settings_js', MPC_PLUGIN_ROOT.'/front/js/fbs.js', array('jquery', 'jquery-ui-core', 'jquery-ui-dialog'), false, true);
        wp_enqueue_media();
                        
        wp_enqueue_style('rfbwp-fontawesome', MPC_PLUGIN_ROOT.'/assets/fonts/font-awesome.css');
         // JS
	wp_enqueue_script('ion-sound', MPC_PLUGIN_ROOT.'/assets/js/ion.sound.min.js', array('jquery'));
	wp_enqueue_script('jquery-doubletab', MPC_PLUGIN_ROOT.'/assets/js/jquery.doubletap.js', array('jquery'));
wp_enqueue_script('jquery-migrate', "/wp-includes/js/jquery/jquery-migrate.js", array(), '1.2.1' );
	wp_localize_script( 'ion-sound', 'mpcthLocalize', array(
		'soundsPath' => MPC_PLUGIN_ROOT . '/assets/sounds/',
		'downloadPath' => MPC_PLUGIN_ROOT . '/includes/download.php?file='
	) );
	wp_enqueue_script('mp_theme_tinymce_js', MPC_PLUGIN_ROOT.'/massive-panel/tinymce/tinymce.min.js', array('jquery'), false, true);
	wp_enqueue_script('mp_theme_ace_js', MPC_PLUGIN_ROOT.'/massive-panel/ace/ace.js', array('jquery'), false, true);
	wp_enqueue_script('mp_theme_ace_css_mode', MPC_PLUGIN_ROOT.'/massive-panel/ace/mode-css.js', array('jquery', 'mp_theme_ace_js'), false, true);

	wp_localize_script('mp_theme_settings_js', 'mpcthLocalize', array(
		'optionsName'		=> $rfbwp_shortname,
		'googleAPIErrorMsg' => __('There is problem with access to Google Webfonts. Please try again later. If this message keeps appearing please contact our support at <a href="http://mpc.ticksy.com/">mpc.ticksy.com</a>.', 'mpcth'),
		'googleAPIKey'		=> MPC_RFBWP_GOOGLE_FONTS_API_ID,
		'googleFonts'		=> stripslashes( $google_webfonts ),
		'addNewPage'		=> __('Add New Page', 'rfbwp' ),
		'editPage'			=> __('Edit Page', 'rfbwp' ),
		'previewPage'		=> __('Preview Page', 'rfbwp' ),
		'deletePage'		=> __('Delete Page', 'rfbwp' ),
		'addaudio'		=> __('Add Audio', 'rfbwp' ),
		'cancelNewPage'		=> __('Cancel', 'rfbwp'),
		'presetsURL'		=> plugin_dir_url( __FILE__ ) . 'presets/',
		'messages'			=> array(
			'errors' => array(
				'lastPage'		=> __( 'last page must be single', 'rfbwp' ),
				'firstPage'		=> __( 'first page must be single', 'rfbwp' ),
				'minPages'		=> __( 'book needs at least 4 pages', 'rfbwp' ),
				'evenPages'		=> __( 'number of pages must be even', 'rfbwp' ),
				'error'			=> __( '<span>ERROR: </span>', 'rfbwp' ),
			),
			'dialogs' => array(
				'maxInputVars'	=> __( 'We are sorry but your changes weren\'t saved. Please increase "max_input_vars" value in your "php.ini" file.', 'rfbwp' ),
				'bottomPage'	=> __( 'Oops! It looks like this page is already at the bottom.', 'rfbwp' ),
				'topPage'		=> __( 'Oops! It looks like this page is already at the top.', 'rfbwp' ),
				'bookSaved'		=> __( 'Book settings saved successfully.', 'rfbwp' ),
				'importFinished'=> __( 'Import has been successfully finished.', 'rfbwp' ),
				'normalLarge'	=> __( "Number of normal and large images don't match.", 'rfbwp' ),
				'noImages'		=> __( 'No images selected to upload.', 'rfbwp' ),
				'bookID'		=> __( 'Wrong book ID.', 'rfbwp' ),
				'selectImage'	=> __( 'Select Image', 'rfbwp' ),
				'insertImage'	=> __( 'Insert Image', 'rfbwp' ),
				'selectImages'	=> __( 'Select Images', 'rfbwp' ),
				'insertImages'	=> __( 'Insert Images', 'rfbwp' ),
				'presetLoaded'	=> __( 'Preset has been loaded.', 'rfbwp' ),
				'deleteBook'	=> __( 'Are you sure you want to delete: ', 'rfbwp' ),
			),
		),
	) );
                    
        

        ?> 
<style>
    .wp-color-result:hover,
                .wp-color-result{
                    background-color:#fff;
                }
    .entry{
        border: none !important;
        box-shadow: none !important;
    }
    div#field-rfbwp_pages {
    border: none;
     padding: 0 !important;
}
.entry table {
margin: 0 !important;
}
.entry table td{
    
}
.media-modal{
    z-index: 22222222222 !important;
}
.fw-fsel{
    font-weight: normal;
}  
.fc-fcol{
    width: 74.35%;
}
.breadcrumbs,.tab-group,.section-group,.group,#top-bar,#top-nav,#top{
    display: none;
} 
#mp-option-books{
    display:block;
}
.select2-chosen{
    font-weight:normal
}
.wp-picker-container{
    width: 74.35%;
    font-weight: normal;
}
#field-rfbwp_fb_hc_fcc  .wp-picker-container,#field-rfbwp_fb_hc_bcc .wp-picker-container {
    width:16.9%;
    font-weight: normal;
}
#field-rfbwp_fb_border_color .wp-picker-container{
    width:58.3%;
    font-weight: normal;
}
.wrap1{
    display: block;
}

#upload_rfbwp_fb_page_bg_image,#upload_rfbwp_fb_page_bg_image_zoom,.rfbwp-page-save{
    text-shadow: none;
}
#rfbwp_tools{
/*    display: none;*/
}
.entry table{
    text-shadow: none !important;
}
.dsg{dispaly:none;}


.breadcrumb-0{    display: none;}
.disnst{    display: none;}
.dshidd{    display: none;}
span.breadcrumb:after {
    position: absolute;
    left: 83px;
    content: '';
    width: 0;
    height: 0;
    border-style: solid;
    border-width: 0px 35px 35px 0;
    border-color: transparent #505050 transparent transparent;
    -webkit-transition: all .25s;
    -moz-transition: all .25s;
    -o-transition: all .25s;
    transition: all .25s;
}
span.breadcrumb {
    background: #52b3d9;
}
.sd{color: #d24d57;
    position: absolute;
    margin-left: -14px;
    font-size: 16px;
	cursor: pointer;
}
.edit-button-back{
	background: #52b3d9;  
    padding: 0 32px 0 15px;
    float: left;
    height: 35px;
    line-height: 35px;
    font-weight: bold;
    text-transform: uppercase;
    color: #fff;
   }
.edit-button-back:after {
    position: absolute;
    left: 50px;
    content: '';
    width: 0;
    height: 0;
    border-style: solid;
    border-width: 0px 35px 35px 0;
    border-color: transparent #505050 transparent transparent;
    -webkit-transition: all .25s;
    -moz-transition: all .25s;
    -o-transition: all .25s;
    transition: all .25s;
} 
.edit-buttonsnow{
text-decoration: none;
    padding: 0 15px 0 15px;
    float: right;
    height: 35px;
    line-height: 35px;
    font-weight: bold;
    text-transform: uppercase;
	background: #505050;
    color: #ffffff;
}  
.dst{display:none;}
.edit-buttonsnow a{color: #ffffff;}
#rfbwp_fb_descript{padding-top:20px;}
#rfbwp_fb_descript .description-top {width: 100%;
    float: left;}
#rfbwp_fb_descript .controls  input[type=radio]{width: 5%;float: left;}
.controls  .texts{width: 20%;float: left;}
.discriptionfied .description-top {width: 100%;
    float: left;}
.discriptionfied input[type=radio]{width: 2%;float: left;height: 18px;}
a.add-audio-page {
    display: inline-block;
    margin: 0;
    margin-right: 10px;
    background: #00b16a;
    color: #fff;
}


.edit-button-page{
	background: #52b3d9;  
    padding: 0 32px 0 15px;
    float: left;
    height: 35px;
    line-height: 35px;
    font-weight: bold;
    text-transform: uppercase;
    color: #fff;
   }
.edit-button-page:after {
    position: absolute;
    left: 50px;
    content: '';
    width: 0;
    height: 0;
    border-style: solid;
    border-width: 0px 35px 35px 0;
    border-color: transparent #505050 transparent transparent;
    -webkit-transition: all .25s;
    -moz-transition: all .25s;
    -o-transition: all .25s;
    transition: all .25s;
} 
.discriptionfied{padding-top: 36px;}
.controls input[type="checkbox"], #mpc_toc_generator input[type=checkbox] {
    height: 27px;
    line-height: 24px;
    top: 0;
    float: left;
    width: 18px;
}
div.description-top {
    height: 35px;
    margin: 0px 5px 0px 1px;
    line-height: 35px;
    clear: both;
    font-size: 14px;
    display: inline-block;
    color: #333333;
    text-transform: capitalize;
    float: left;
	width: 19%;
}
input.mp-input-medium {
    width: 246px;
}
 select {
    text-transform: none;
    width: 246px;
}
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>

<script>
var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
jQuery(document).ready(function($) {
  // $('.wrap1').show(); rfbwp_tools settings  add-book books edit-button-back add-book
  //   $("link[rel=stylesheet]").attr({href : "<?php //echo get_template_directory_uri();?>/css/entry.css"}).remove();
   $('a.edit-button-back').attr('style','display:none');
   $('span.breadcrumb-3').attr('style','display:none');
    $('a.edit-button-back-tool').attr('style','display:none');
  $('.wrap1').on('click', 'a.edit-button', function( e ) {
	  $('div#rfbwp_tools_toggle_content').removeClass('disnst');
	 $('div.mp-toggle-content').attr('style','display:none');
	  $('div#rfbwp_tools').attr('style','display:block');
	 $('div#rfbwp_tools_toggle_content').attr('style','display:block');
	 $('a.edit-button-back').attr('style','display:block');
	 $('div.settings').attr('style','display:none');
	 $('a.edit-buttonsnow').attr('style','display:none');
	  $('a.edit-button').attr('style','display:none');
	  
	 
  }); 
  $('.wrap1').on('click', 'a.edit-button-page', function( e ) {
	  $('div.discriptionfied').addClass('dst');
	  //  $('div#discriptionfied_publish').attr('style','display:none');
		 //$('div#discriptionfied_publish').attr('style','display:none');
		 var book_id = $("#rfbwp_tools").data("book-id");
	     var rootbook = '#mp-option-pages_' + book_id;
	    $(rootbook).attr('style','display:block'); 
		 $('a.edit-buttonsnow').attr('style','display:block');
		  $('a.edit-button-page').attr('style','display:none');
		  // $('div.discriptionfied').removeClass('dst');
	   // $('div.discriptionfied').attr('style','display:none');
	});   
  $('.wrap1').on('click', 'a.edit-buttonsnow', function( e ) {
	  $('div.discriptionfied').removeClass('dst');
	    $('div.discriptionfied').attr('style','display:block');
		
		 var book_id = $("#rfbwp_tools").data("book-id");
	     var rootbook = '#mp-option-pages_' + book_id;
	    $(rootbook).attr('style','display:none');
		 $('a.edit-buttonsnow').attr('style','display:none');
		  $('a.edit-button-page').attr('style','display:block');
		//edit-buttonsnow 
		/*var desc = $('<textarea cols="80" rows="12" id="rfbwp_fb_descript" class="mp-input-medium mp-input-border "" name="rfbwp_fb_descript"  value=""></textarea>');
        var referance = $('<textarea cols="80" rows="12" id="rfbwp_fb_refernce" class="mp-input-medium mp-input-border " name="rfbwp_fb_refernce"  value=""></textarea>');
		var tags = $(' <input id="rfbwp_fb_tag" class="mp-input-medium mp-input-border " name="rfbwp_fb_tag" type="text" value="">');
		var bgras = $('<input id="rfbwp_fb_background" class="mp-input-medium mp-input-border " name="rfbwp_fb_background" type="radio" value="whitebacground"><span class="texts">White Background</span><input id="rfbwp_fb_background" class="mp-input-medium mp-input-border " name="rfbwp_fb_background" type="radio" value="profilebackground"><span class="texts">Profile Background</span><input id="rfbwp_fb_background" class="mp-input-medium mp-input-border " name="rfbwp_fb_background" type="radio" value="newbackground"><span class="texts">New Background</span>');

		var privacys = (' <input id="rfbwp_fb_privacy" class="mp-input-medium mp-input-border " name="rfbwp_fb_privacy" type="radio" value="private"><span class="texts">Private</span><input id="rfbwp_fb_privacy" class="mp-input-medium mp-input-border " name="rfbwp_fb_privacy" type="radio" value="public"><span class="texts">Public</span>');

		$('#desc').append(desc);
		$('#referance').append(referance);
		$('#tags').append(tags);
		$('#bgras').append(bgras);
		$('#privacys').append(privacys);*/
	});   
  $('.wrap1').on('click', 'span.breadcrumb-3', function( e ) {
	    $('div.group').attr('style','display:block');
	   $('div.settings').attr('style','display:none');
	  $('div.group').attr('style','display:block');
	  
  });
  $('.wrap1').on('click', 'span.breadcrumb-5', function( e ) {
	   var book_id = $("#rfbwp_tools").data("book-id");
	   var rootbook = '#mp-option-pages_' + book_id;
	   //mp-option-pages_
	    $('div#rfbwp_tools').attr('style','display:block');
		 $('a.edit-button-back').attr('style','display:block');
		 $('div.bottom-nav').attr('style','display:block');
	  //  $('div.group').attr('style','display:block'); 
	   //$('div.settings').attr('style','display:none');
	 // $('div.group').attr('style','display:block');
	  $(rootbook).attr('style','display:none');
	  $('span.breadcrumb-5').attr('style','display:none');
	   $('a.edit-buttonsnow').attr('style','display:none');
	  $('a.edit-button-page').attr('style','display:block');
	  $('div#discriptionfied_publish').attr('style','display:none');
	  $('div.bottom-nav a.edit-button-page').fadeOut();
	  
  });
   $('.wrap1').on('click', 'a.add-book', function( e ) {
	    
	  $('.settings').find('.mp-toggle-header').trigger('click');
  });
  $('.wrap1').on('click', 'span.breadcrumb-3', function( e ) {
			e.preventDefault();
           setup_footer('books');
			 
   });
		
		
    $('.wrap1').on('click', 'a.edit-button-back', function( e ) {
	  $('div#rfbwp_tools_toggle_content').removeClass('disnst');
	 var book_id = $("#rfbwp_tools").data("book-id");
	 var rootbook = '#mp-option-settings_' + book_id;
	 //var contebt = $("#rfbwp_tools").data("toggle-section");data-toggle-section
	 var rightnow = rootbook +'.mp-toggle-content';
	 $(rightnow).attr('style','display:block');
	  $('div#rfbwp_tools').attr('style','display:none');
	 $('div#rfbwp_tools_toggle_content').attr('style','display:none');
	 $('a.edit-button-back-tool').attr('style','display:none');
	 $('a.edit-button-back').attr('style','display:none');
	 $(rootbook).attr('style','display:block');
	 $('div#rfbwp_tools_toggle_content .bottom-nav').attr('style','display:none');
	 $('.settings').find('.mp-toggle-header').trigger('click');
	  $('div.breadcrumbs span.breadcrumb-2').attr('style','display:none');
	  $('a.edit-button').attr('style','display:block');
		//alert(book_id);
	 
    });	//breadcrumb-0 
	/*$('#rfbwp_flipbook_batch_import').on('click', function(e) {
		$('div#rfbwp_tools_toggle_content').attr('style','display:none');
		$('span.breadcrumb-0').attr('style','display:none');
		$('span.breadcrumb-1').attr('style','display:none');
		$('div#rfbwp_tools').attr('style','display:none');
		 $('a.edit-button-back-tool').attr('style','display:block');
		 $('a.edit-button-back').attr('style','display:none');
		$('div.breadcrumbs').find('span.breadcrumb-0').attr('value', 'Back');
	});	*/
	$('.wrap1').on('click', 'a.edit-button-page', function( e ) {
		
	    var book_id = $("#rfbwp_tools").data("book-id");
	   var rootbook = '#mp-option-pages_' + book_id;
	   $(rootbook).attr('style','display:block');
	  $('a.edit-button-page').attr('style','display:none');
	   $('div.discriptionfied').attr('style','display:block');
    });
	$('.wrap1').on('click', 'a.edit-button-back-tool', function( e ) {
		
	  //$('div#rfbwp_tools_toggle_content').removeClass('disnst');
	 $('div.mp-toggle-content').attr('style','display:none');
	  $('div#rfbwp_tools').attr('style','display:block');
	 $('div#rfbwp_tools_toggle_content').attr('style','display:block');
	 $('a.edit-button-back-tool').attr('style','display:none');
	 $('a.edit-button-back').attr('style','display:block');
	 $('div.settings').attr('style','display:none');
	// $('div#rfbwp_tools_toggle_content .bottom-nav').attr('style','display:none');
	 
    });
	$('.wrap1').on('click', 'div#rfbwp_fb_background', function( e ) {
      var book_id = $("#rfbwp_tools").data("book-id");		
      var dfs = $('input[name=rfbwp_fb_background]:checked').val();
	  if(dfs == 'newbackground')
	  { 
       $('div#rfbwp_fb_background_img').attr('style','display:block');
      }
	  else{
		$('div#rfbwp_fb_background_img').attr('style','display:none');  
	  }
     });
});

$(document).on('click','i.sd',function(e){
    e.preventDefault();
    var rel = this.rel;
     //or using attr()
    var rel=$(this).attr('rel'); 
    // alert(rel);  
	var removeid = $('img#img' + rel).data("bk");
	 // alert(removeid);
	  var oladid = $('#rfbwp_flipbook_batch_ids').val();
	 if(rel == 0)
	 {
		 var res = oladid.replace(removeid + ',' ,'');
	  $('#rfbwp_flipbook_batch_ids').val(res);
	 }
	 else{
		 var res = oladid.replace(',' + removeid  ,'');
	  $('#rfbwp_flipbook_batch_ids').val(res); 
	 }
	 $('img#img' + rel).remove();
	 $('i#sda' + rel).remove();
	
	  
  });

</script>

<div class="wrap1" id="wpcontent" style="min-height: 280px;">
		<div class="icon32" id="icon-options-general"></div>
		<h2><?php echo $settings_output['mp_page_title']; ?></h2>
 		<div id="top">
			<div id="top-nav">
                    		<!--<div class="mpc-logo"></div>-->
				<?php echo $content[3]; ?>
				<?php echo $content[2]; ?>
			</div>
                        <!-- end topnav -->
		</div> 
                <!-- end top -->
		<div id="bg-content">
                    <div id="sidebar"><?php echo $content[1]; ?></div>
                    <form action="/" id="options-form" name="options-form" method="post">
                            <?php
                                    settings_fields($settings_output[$rfbwp_shortname.'_option_name']);
                                    echo $content[0];
                            ?>
                            <div class="bottom-nav">
                                    <div class="mp-line">
                                            <div class="mp-line-around">
                                                    <input type="hidden" name="action" value="rfbwp_save_settings" />
                                                    <input type="hidden" name="security" value="<?php echo wp_create_nonce('rfbwp-theme-data'); ?>" />
                                                    <input type="hidden" name="uid" id="uid"  value="<?php echo $current_user->ID; ?>" />

                                                    <input name="mp-submit" class="save-button" type="submit" value="<?php esc_attr_e('Next Step', 'rfbwp'); ?>" />

                                                    <a class="edit-button" href="#"><?php _e('Next Step', 'rfbwp'); ?></a>
													<a class="edit-buttonsnow" href="#"><?php _e('Next Step', 'rfbwp'); ?></a>
													<a class="edit-button-back" href="#"><?php _e('Back', 'rfbwp'); ?></a>
													<a class="edit-button-back-tool" href="#"><?php _e('Back', 'rfbwp'); ?></a>
													<a class="edit-button-page" style="display:none" href="#"><?php _e('Back', 'rfbwp'); ?></a>
                                            </div>
                                    </div>
                            </div>
                    </form>
					 
                                 
                    <div id="rfbwp_tools">
                            <header id="rfbwp_tools_toggle_header">
                                    <a id="rfbwp_tools_toggle_title" href="#"><?php _e('Batch Upload', 'rfbwp'); ?><span class="toggle-arrow"></span></a>
                            </header>

                        <div id="rfbwp_tools_toggle_content">
                               <div class="field">
                                        <form action="<?php echo admin_url('admin-ajax.php'); ?>" enctype="multipart/form-data" method="post">
                                               <!-- <div class="description-top"><?php _e('Import Flipbook settings:', 'rfbwp'); ?> </div>-->
                                                <input type="hidden" name="action" value="import_flipbooks">
                                                <input type="hidden" name="back_url" value="" id="rfbwp_import_back_url">
                                                <input type="hidden" name="mp-settings" value="Save Page">
                                                <input type="hidden" name="book_id" id="rfbwp_import_id">
                                              <input type="file" class="dshidd" name="import_flipbooks_file" id="rfbwp_import_file" style="display: none;">
                                                <input type="submit" id="rfbwp_import">
                                             <a id="rfbwp_flipbook_import" class="dshidd" class="mpc-button revert" href="#">
                                                        <i class="dashicons dashicons-upload"></i>
                                                        <?php _e('Import', 'rfbwp'); ?>
                                                </a> 
                                              <!--<div class="help-icon">
                                                        <span class="mp-tooltip top">
                                                                <?php _e('Use this field to import all Flipbooks and pages settings from previously created backup. <br /><br /> (NOTE: File must have .rfb extension).', 'rfbwp'); ?>
                                                        </span>
                                                </div>-->
                                        </form>

                                </div>
                                <div class="field dshidd">
                                        <div class="description-top"><?php _e('Export Flipbook settings:', 'rfbwp'); ?> </div>
                                        <a id="rfbwp_flipbook_export" class="mpc-button revert" href="#">
                                                <i class="dashicons dashicons-migrate"></i>
                                                <?php _e('Export', 'rfbwp'); ?>
                                        </a>
                                        <div class="help-icon">
                                                <span class="mp-tooltip top">
                                                        <?php _e('Use this field to export all Flipbooks and pages settings to a file. <br /><br /> (NOTE: This exports only settings. To export used images you will have to use other plugin).', 'rfbwp'); ?>
                                                </span>
                                        </div>
                                </div> 

                                <div class="field">
                                        <div class="description-top"><?php _e('Batch Images upload:', 'rfbwp'); ?> </div>
                                        <div class="help-icon">
                                                <span class="mp-tooltip top">
                                                        <?php _e('Use this feature to import multiple images at once. Please notice that the order and amount of Normal and Large images must be exactly the same. <br /><br />If you do not wish to use zoomed (large) images you can left this field empty.', 'rfbwp'); ?>
                                                </span>
                                        </div>
                                        <div class="select-section">
                                                <input type="hidden" id="rfbwp_flipbook_batch_ids" name="rfbwp_flipbook_batch_ids" value="">
                                                <a id="rfbwp_flipbook_batch_select" class="mpc-button revert" href="#">
                                                        <i class="dashicons dashicons-format-gallery"></i>
                                                        <?php _e('Select Batch Image', 'rfbwp'); ?>
                                                </a>
                                                <a id="rfbwp_flipbook_batch_clear" class="mpc-button delete-page" href="#0">
                                                        <i class="dashicons dashicons-trash"></i>
                                                        <?php _e('Delete', 'rfbwp'); ?>
                                                </a>
                                                <div id="rfbwp_flipbook_batch_images_wrap"></div>
                                        </div>
                                     <div class="select-section dshidd">
                                                <input type="hidden" id="rfbwp_flipbook_batch_ids_large" name="rfbwp_flipbook_batch_ids_large" value="">
                                                <a id="rfbwp_flipbook_batch_select_large" class="mpc-button revert" href="#">
                                                        <i class="dashicons dashicons-format-gallery"></i>
                                                        <?php _e('Select Large', 'rfbwp'); ?>
                                                </a>
                                                <a id="rfbwp_flipbook_batch_clear_large" class="mpc-button delete-page" href="#0">
                                                        <i class="dashicons dashicons-trash"></i>
                                                        <?php _e('Delete', 'rfbwp'); ?>
                                                </a>
                                                <div id="rfbwp_flipbook_batch_images_wrap_large"></div>
                                        </div> 
                                        <a id="rfbwp_flipbook_batch_import" class="mpc-button revert" href="#">
                                                <i class="dashicons dashicons-upload"></i>
                                                <?php _e('Upload', 'rfbwp'); ?>
                                        </a>
                                       <!-- <div class="description-top batch-double"><?php _e('Double Pages:', 'rfbwp'); ?> </div>
                                        <input id="rfbwp_flipbook_batch_double" class="checkbox of-input" type="checkbox" name="rfbwp_flipbook_batch_double" />
                                     -->
                                </div>
                        </div>
						
						 <div class="bottom-nav">
                                    <div class="mp-line">
                                            <div class="mp-line-around">
                                                    
                                                    <a class="edit-button" href="#"><?php _e('Next Step', 'rfbwp'); ?></a>
                                            </div>
                                    </div>
                            </div>
                    </div>
					<div id="discriptionfied_publish" class="discriptionfied dst">
					 <form action="/" id="options-forms" name="options-forms" method="post">
					       <input type="hidden" name="action" value="rfbwp_publish_book_finially" />
						     <input name="mp-publish" style="display:none" class="publish-button" type="submit" value="<?php esc_attr_e('Publish', 'rfbwp'); ?>" />
					 
					<div id="rfbwp_fb_descript" class="field  field-text-medium">
					

                                                
                          <div class="option">
                             <div class="controls fcontrols">
                                   <div class="description-top ">Description: </div>
                                 <div id="desc"></div>
								<textarea cols="80" rows="12" id="rfbwp_fb_descript" class="mp-input-medium mp-input-border " name="rfbwp_fb_descript"  value=""></textarea> 
       
								  <div class="help-icon">
								  <span class="mp-tooltip bottom">Flip book Description
								  </span>
								</div>
							</div>
						<div class="clear"></div>
						</div>
					  </div>
					  <div id="rfbwp_fb_refernce" class="field  field-text-medium">
                          <div class="option">
                             <div class="controls fcontrols">
                                   <div class="description-top ">Referance: </div>
                                  <textarea cols="80" rows="12" id="rfbwp_fb_refernce" class="mp-input-medium mp-input-border " name="rfbwp_fb_refernce"  value=""></textarea> 
		
								  <div class="help-icon">
								  <span class="mp-tooltip bottom">Flip book Referance
								  </span>
								</div>
							</div>
						<div class="clear"></div>
						</div>
					  </div> 
					 <div id="rfbwp_fb_tag" class="field  field-text-medium">
                          <div class="option">
                             <div class="controls fcontrols">
                                   <div class="description-top ">Add Tags: </div>
                                <input id="rfbwp_fb_tag" class="mp-input-medium mp-input-border " name="rfbwp_fb_tag" type="text" value=""> 
		
								  <div class="help-icon">
								  <span class="mp-tooltip bottom">Flip book name tag
								  </span>
								</div>
							</div>
						<div class="clear"></div>
						</div>      
					  </div>
					  <div id="rfbwp_fb_background" class="field  field-text-medium" >
                          <div class="option">
                             <div class="controls fcontrols">
                                   <div class="description-top ">Add Background: </div>

                                 <input id="rfbwp_fb_background" class="mp-input-medium mp-input-border " name="rfbwp_fb_background" type="radio" value="whitebacground"><span class="texts">White Background</span>
								 <input id="rfbwp_fb_background" class="mp-input-medium mp-input-border " name="rfbwp_fb_background" type="radio" value="profilebackground"><span class="texts">Profile Background</span>
								 <input id="rfbwp_fb_background" class="mp-input-medium mp-input-border " name="rfbwp_fb_background" type="radio" value="newbackground"><span class="texts">New Background</span> 

						  <div class="help-icon">
								  <span class="mp-tooltip bottom">Flip book Background
								  </span>
								</div>
							</div>
						<div class="clear"></div>
						</div>
						
						 
					  </div>
					  <div id="rfbwp_fb_background_img" class="field  field-upload rfbwp-page-bg-image" style="display:none">
                                <div class="option">
									<div class="controls fcontrols">
										<span class="of_metabox_desc">Choose background image:</span>
                                            <div class="screenshot" id="rfbwp_fb_background_img">
											       
												<img src="http://www.doityourselfnation.org/bit_bucket/wp-content/plugins/newresponsive-flipbook/massive-panel/images/no-image.png" class="default">
											</div>
										<input id="rfbwp_fb_background_img" class="upload mp-input mp-input-border rfbwp-page-bg-image" type="text" name="rfbwp_fb_background_img" value="">
                                      <a id="upload_rfbwp_fb_background_img" class="upload_button mpc-button rfbwp-page-bg-image" ><i class="dashicons dashicons-format-image"></i> Upload Image</a>
									</div>
							   </div>
							   <div class="clear"></div>
						</div> 
					  <div id="rfbwp_fb_privacy" class="field  field-text-medium">
                          <div class="option">
                             <div class="controls fcontrols">
                                   <div class="description-top ">Privacy Options: </div>
                                 <input id="rfbwp_fb_privacy" class="mp-input-medium mp-input-border " name="rfbwp_fb_privacy" type="radio" value="private"><span class="texts">Private</span>
								 <input id="rfbwp_fb_privacy" class="mp-input-medium mp-input-border " name="rfbwp_fb_privacy" type="radio" value="public"><span class="texts">Public</span>

				
                                   <div class="help-icon">
								  <span class="mp-tooltip bottom">Flip book privacy
								  </span>
								</div>
							</div>
						<div class="clear"></div>
						</div>
					  </div>
					  
					  <div id="field-rfbwp_fb_book_publish" class="field  field-button rfbwp-book-publish revert">
                             <div class="option">
                                  <div class="controls fcontrols">
                                        <a class="rfbwp-book-publish revert mpc-button" href="#Publish">
                                       <i class="dashicons dashicons-edit"></i> Publish</a></div><div class="clear">
									   </div>
									   </div>
						</div>
					  </form>
                 </div>
		</div> <!-- end bg-content -->

		<div id="rfbwp_page_preview">
			<div id="rfbwp_page_preview_wrap"></div>
			<div id="rfbwp_page_preview_close"><?php _e('Click anywhere to close.', 'rfbwp') ?></div>
		</div>

	</div>
        <!-- end wrap -->
  <?php      //error_reporting(E_ALL && ~ E_NOTICE);
	
  
  
}
                        
function mp_display_front_content($update = false, $get_page_form = false) {
	// variables
	global $allowedtags;
	global $rfbwp_shortname;
	global $settings;

	$option_name = rfbwp_get_option_name();
   $settings = rfbwp_get_settings();
	$options = mpcrf_options_newfornt();
        //mp_settings_scripts();
        mp_notices_style();
	if(isset($settings['books']) && count($settings['books']) > 0)
		$options = mp_duplicate_options($options, 'true', 'false');

	$rfbwp_page_form = '';
	$book_id = -1;
	$form_id = -1;
	$page_id = -1;
	$counter = 0;
	$menu = '<ul>';
	$tabs = '';
	$output = '';
	$header = '';
	$section_name = '';
	$begin_tabs = true;
	$begin_page_form = false;
	$create_page_form = false;
	$desc = '';
	$hide = 'false';
	$type = '';
	$path_prefix = '';
	$add_button = false;
	$separator = false;
	$stacked = false;
	$toggle = false;

	foreach($options as $value) {
		$counter++;
		$val = ''; // used to store save value of a field;
		$select_value = '';
		$checked = '';
		$desc = 'right';
		$hide = 'false';

		if( isset($value['sub']) && $value['sub'] == 'settings' )
			$type = 'books';
		elseif( isset($value['sub']) && $value['sub'] == 'pages' )
			$type = 'pages';
		elseif( $value['type'] == 'section' )
			$type = '';

		if($type == 'books' && $value['type'] == 'heading'  && isset($value['sub']) && $value['sub'] == 'settings') {
			$book_id ++;
			$page_id = -1;
			$add_button = true;
			$begin_page_form = false;
		}

		if($type == 'pages' && $begin_page_form && !isset($settings['books'][$book_id]['pages']) ||
			$type == 'pages' && $begin_page_form && isset($settings['books'][$book_id]['pages']) && count($settings['books'][$book_id]['pages']) < 1 ) {

			if($add_button) {
				$output .= '<img class="rfbwp-first-page" src="'.MPC_PLUGIN_ROOT.'/massive-panel/images/first_page.png" />';
				$add_button = false;
			}

			if($form_id == -1 || $form_id == $book_id) {
				$create_page_form = true;
				$form_id = $book_id;
			} else {
				continue;
			}

		} else {
			$create_page_form = false;
                        
		}

		if($type == 'pages' && $value['type'] == 'separator') {
			$begin_page_form = true;
			$page_id++;
		}


		if($value['type'] == 'separator') {
			if($separator){
				$output .= '</div>';
				$output .= '<div id="ps_'.$page_id.'" class="page-settings">';
			} else {
				$output .= '<div id="ps_'.$page_id.'" class="page-settings">';
				$separator = true;
			}
		} elseif($separator && $value['type'] == 'section' || $separator && $value['type'] == 'heading') {
			$separator = false;
			$output .= '</div>';

		}
                

		if($type == 'books')
			$path_prefix = $option_name.'[books]['.$book_id.']';
		elseif($type == 'pages')
			$path_prefix = $option_name.'[books]['.$book_id.'][pages]['.$page_id.']';

		if (isset($value['desc-pos']))
			$desc = $value['desc-pos'];

		// Wrap all options
		if (($value['type'] != "heading") && ($value['type'] != "section")  &&
			($value['type'] != "top-header") && ($value['type'] != "top-socials")) {

			// convert ids to lowercase with no spaces
			$value['id'] = preg_replace('/\W/', '', strtolower($value['id']) );
			$id = 'field-' . $value['id'];
			$class = 'field ';

			if(isset($value['float']))
				$class .= $value['float'].' ';

			if ( isset($value['type']) )
				$class .= ' field-'.$value['type'];

			if ( isset($value['class']) )
				$class .= ' '.$value['class'];
			 
			if(!$create_page_form) {//open
                    
				if(isset($value['toggle']) && $value['toggle'] == 'begin' ) {
					if($value['togglebs'] == 'no'){

					$toggle = true;
					$output .= '<div class="mp-toggle-header " style="display:none;"><span class="toggle-name">'.$value['toggle-name'].'</span><span class="toggle-arrow"></span></div><div class="mp-toggle-content" data-toggle-section="' . $id . '" >';
					}
					else{
					  $toggle = true;
					$output .= '<div class="mp-toggle-header"><span class="toggle-name">'.$value['toggle-name'].'</span><span class="toggle-arrow"></span></div><div class="mp-toggle-content" data-toggle-section="' . $id . '" >';
					
					}
				}

				if(isset($value['stack']) && $value['stack'] == 'begin') {
					$stacked = true;

					if($value['id'] == 'rfbwp_fb_page_bg_image')
						$output .= '<div class="stacked-fields no-border">';
					else
						$output .= '<div class="stacked-fields" data-section-id="' . $id . '">';

					if(isset($value['help']) && $value['help'] == 'true')
						$output .= '<div class="help-icon"><span class="mp-tooltip '.(isset($value['help-pos']) ? $value['help-pos'] : 'top').'">'.$value['help-desc'].'</span></div>';

					$output .= '<div id="' . esc_attr( $id ) .'" class="' . esc_attr( $class ) . '">'."\n";
				} else {
					$output .= '<div id="' . esc_attr( $id ) .'" class="' . esc_attr( $class ) . '">'."\n";
				}
			} else {

				if(isset($value['toggle']) && $value['toggle'] == 'begin' ) {
					if($value['toggletest'] == 'ntok')
					{
					
					$toggle = true;
					$rfbwp_page_form .= '<div class="mp-toggle-header" style="dispaly:none;"><span class="toggle-name">'.$value['toggle-name'].'</span><span class="toggle-arrow"></span></div><div class="mp-toggle-content">';
					}
					else{
					  $toggle = true;
					$rfbwp_page_form .= '<div class="mp-toggle-header"><span class="toggle-name">'.$value['toggle-name'].'</span><span class="toggle-arrow"></span></div><div class="mp-toggle-content" >';
						
					}
				}

				if(isset($value['stack']) && $value['stack'] == 'begin') {
					$stacked = true;

					if($value['id'] == 'rfbwp_fb_page_bg_image')
						$rfbwp_page_form .= '<div class="stacked-fields no-border">';
					else
						$rfbwp_page_form .= '<div class="stacked-fields">';

					if(isset($value['help']) && $value['help'] == 'true')
						$rfbwp_page_form .= '<div class="help-icon"><span class="mp-tooltip '.(isset($value['help-pos']) ? $value['help-pos'] : 'top').'">'.$value['help-desc'].'</span></div>';
					$rfbwp_page_form .= '<div id="' . esc_attr( $id ) .'" class="' . esc_attr( $class ) . '">'."\n";
				} else {
					$rfbwp_page_form .= '<div id="' . esc_attr( $id ) .'" class="' . esc_attr( $class ) . '">'."\n";
				}
			}

			if($value['type'] == "choose-sidebar") {
				$output .= '<div class="option">' . "\n" . '<div class="controls controls-sidebar">' . "\n";
			} elseif ($value['type'] == "choose-portfolio") {
				$output .= '<div class="option">' . "\n" . '<div class="controls controls-portfolio">' . "\n";
			} else {
				if(!$create_page_form)
					$output .= '<div class="option">' . "\n" . '<div class="controls fcontrols">' . "\n";
				else
					$rfbwp_page_form .= '<div class="option">' . "\n" . '<div class="controls">' . "\n";
			}

		 }

		 // Set default value to $val
		if (isset($value['std']))
			$val = $value['std'];

		// If the option is already saved, ovveride $val
		if (($value['type'] != 'heading') && ($value['type'] != "section") &&
			($value['type'] != 'info') && ($value['type'] != "top-header") &&
			($value['type'] != "top-socials") && $value['type'] != "separator") {

			if ( $type == 'books' && isset($settings['books'][$book_id][$value['id']]) ) {
				$val = $settings['books'][$book_id][($value['id'])];
				// Striping slashes of non-array options
				if (!is_array($val)) $val = stripslashes($val);
			} elseif ( $type == 'pages' && isset($settings['books'][$book_id]['pages'][$page_id][$value['id']])) {
				$val = $settings['books'][$book_id]['pages'][$page_id][$value['id']];
				// Striping slashes of non-array options
				if (!is_array($val)) $val = stripslashes($val);
			}
		}

		$description = '';
		if ( isset($value['desc'])) $description = $value['desc'];

		if($desc == 'top' && !isset($value['class'])) {
			if(!$create_page_form)
				$output .= '<div class="description-top ">'.$description.'</div>'."\n";
			else
				$rfbwp_page_form .= '<div class="description-top">'.$description.'</div>'."\n";

			if($value['id'] == 'rfbwp_page_html') {
				if(!$create_page_form)
					$output .= '<div class="help-icon"><span class="mp-tooltip '.(isset($value['help-pos']) ? $value['help-pos'] : 'top').'">'.$value['help-desc'].'</span></div>';
				else
					$rfbwp_page_form .= '<div class="help-icon"><span class="mp-tooltip '.(isset($value['help-pos']) ? $value['help-pos'] : 'top').'">'.$value['help-desc'].'</span></div>';
			}
		}

		switch ($value['type']) {
			// Basic text input
			case 'text-small':
				if(isset($value['class']))
					$class = $value['class'];
				else
					$class = '';

				if(!$create_page_form) {
					if($value['id'] == 'rfbwp_fb_margin_top')
						$output .= '<span class="mp-fb-margins"></span>';

					$output .= '<input id="' . esc_attr( $value['id'] ) . '" class="mp-input-small mp-input-border '.$class.'" name="' . esc_attr( $path_prefix.'[' . $value['id'] . ']' ) . '" type="text" value="' . esc_attr( $val ) . '" />';

					if(isset($value['unit']))
						$output .= '<span class="mp-unit">'.$value['unit'].'</span>';

				} else {

					if($value['id'] == 'rfbwp_fb_margin_top')
						$rfbwp_page_form .= '<span class="mp-fb-margins"></span>';

					$rfbwp_page_form .= '<input id="' . esc_attr( $value['id'] ) . '" class="mp-input-small mp-input-border '.$class.'" name="' . esc_attr( $path_prefix.'[' . $value['id'] . ']' ) . '" type="text" value="' . esc_attr( $val ) . '" />';

					if(isset($value['unit']))
						$rfbwp_page_form .= '<span class="mp-unit">'.$value['unit'].'</span>';
				}
			break;

			case 'text-medium':
				if(isset($value['class']))
					$class = $value['class'];
				else
					$class = '';

				if(!$create_page_form)
					$output .= '<input id="' . esc_attr( $value['id'] ) . '" class="mp-input-medium mp-input-border '.$class.'" name="' . esc_attr( $path_prefix.'[' . $value['id'] . ']' ) . '" type="text" value="' . esc_attr( $val ) . '" />';
				else
					$rfbwp_page_form .= '<input id="' . esc_attr( $value['id'] ) . '" class="mp-input-medium mp-input-border '.$class.'" name="' . esc_attr( $path_prefix.'[' . $value['id'] . ']' ) . '" type="text" value="' . esc_attr( $val ) . '" />';
			break;

			case 'text-big':
				if(!$create_page_form)
					$output .= '<input id="' . esc_attr( $value['id'] ) . '" class="mp-input-big mp-input-border" name="' . esc_attr( $path_prefix.'[' . $value['id'] . ']' ) . '" type="text" value="' . esc_attr( $val ) . '" />';
				else
					$rfbwp_page_form .= '<input id="' . esc_attr( $value['id'] ) . '" class="mp-input-big mp-input-border" name="' . esc_attr( $path_prefix.'[' . $value['id'] . ']' ) . '" type="text" value="' . esc_attr( $val ) . '" />';
			break;

			// Textarea
			case 'textarea':
				$cols = '35';
				$ta_value = '';
				$val = stripslashes($val);

				if(isset($value['class']))
					$class = $value['class'];
				else
					$class = '';

				if(!$create_page_form) {
					$output .= '<textarea id="' . $value['id'] . '" class="mp-textarea mp-input-border displayall '.$class.'" name="' . $path_prefix.'[' . $value['id'] . ']' . '" cols="'. $cols. '" rows="8">' . $val . '</textarea>';

				} else {
					$rfbwp_page_form .= '<textarea id="' . $value['id'] . '" class="mp-textarea mp-input-border displayall '.$class.'" name="' . $path_prefix.'[' . $value['id'] . ']' . '" cols="'. $cols. '" rows="8">' . $val . '</textarea>';

				}
			break;

			// Textarea Big
			case 'textarea-big':
				$cols = '86';
				$ta_value = '';
				$val = stripslashes($val);

				if(isset($value['class']))
					$class = $value['class'];
				else
					$class = '';

				if($value['wp-editor'])
					$class .= ' html-editor';

				$field = '<textarea id="' . $value['id'] . '" class="mp-textarea mp-input-border displayall '.$class.'" name="' . $path_prefix.'[' . $value['id'] . ']' . '" cols="'. $cols. '" rows="15">' . $val . '</textarea>';

				/*if($value['wp-editor'])
					$field = '<div class="editors-wrapper">' . $field . '</div>';*/

				if(!$create_page_form)
						$output .=  $field;
				else
					$rfbwp_page_form .= $field;

			break;

			// Font Select Box
			case 'font_select':
				if( isset( $val )) {
					$family = $val;

					if(!is_array($family) ) $family = stripslashes( $family );
				}
				else {
					$family = 'default';
				}
                                
                                

				$output .= '<input type="hidden"  name="' . $path_prefix.'[' . $value['id'] . ']' . '" value="' . $family . '" class="font-handler" />';
				$output .= '<select data-font="' . $family . '" class="of-input rfbwp-of-input-font mp-dropdown" id="' . $value['id'] . '">';

				if( !empty( $family ) )
					$output .= '<option class="mpcth-option-default" value="default">' . __('default', 'mpcth') . '</option>';

				$output .= '</select>';
                                
                               

			break;

			// Select Box
			case "select":
				if(isset($value['class']))
					$class = $value['class'];
				else
					$class = '';

				if(!$create_page_form) {
					$output .= '<select class="mp-dropdown '.$class.'" name="' . esc_attr( $path_prefix.'[' . $value['id'] . ']' ) . '" id="' . esc_attr( $value['id'] ) . '">';
				} else {
					$rfbwp_page_form .= '<select class="mp-dropdown '.$class.'" name="' . esc_attr( $path_prefix.'[' . $value['id'] . ']' ) . '" id="' . esc_attr( $value['id'] ) . '">';
				}

				foreach ($value['options'] as $key => $option ) {
					$selected = '';
					 if( $val != '' ) {
						 if ( $val == $key) {
						 	$selected = ' selected';
						 }
			     }
			     if(!$create_page_form)
					 $output .= '<option'. $selected .' value="' . esc_attr( $key ) . '">' . esc_html( $option ) . '</option>';
				 else
					$rfbwp_page_form .= '<option'. $selected .' value="' . esc_attr( $key ) . '">' . esc_html( $option ) . '</option>';
			 }
			 	if(!$create_page_form)
					$output .= '</select>';
				else
					$rfbwp_page_form .= '</select>';

				if($value['id'] == 'rfbwp_fb_page_type') {
					if(!$create_page_form)
						$output .= '<div class="help-icon"><span class="mp-tooltip '.(isset($value['help-pos']) ? $value['help-pos'] : 'top').'">'.$value['help-desc'].'</span></div>';
					else
						$rfbwp_page_form .= '<div class="help-icon"><span class="mp-tooltip '.(isset($value['help-pos']) ? $value['help-pos'] : 'top').'">'.$value['help-desc'].'</span></div>';
				}

			break;

			// Checkbox
			case "checkbox":
                            if($val=='on'){$val=1;}
				if(!$create_page_form)
					$output .= '<input id="' . esc_attr( $value['id'] ) . '" class="checkbox of-input" type="checkbox" name="' . esc_attr( $path_prefix.'[' . $value['id'] . ']' ) . '" '. checked($val, 1, false) .' />';
				else
					$rfbwp_page_form .= '<input id="' . esc_attr( $value['id'] ) . '" class="checkbox of-input" type="checkbox" name="' . esc_attr( $path_prefix.'[' . $value['id'] . ']' ) . '" '. checked($val, 1, false) .' />';
			break;
//                        case "radio":
//                            if($val=='on'){$val=1;}
//				if(!$create_page_form)
//					$output .= '<input id="' . esc_attr( $value['id'] ) . '" class="checkbox of-input" type="radio" name="' . esc_attr( $path_prefix.'[' . $value['id'] . ']' ) . '" '. checked($val, 1, false) .' />';
//				else
//					$rfbwp_page_form .= '<input id="' . esc_attr( $value['id'] ) . '" class="checkbox of-input" type="radio" name="' . esc_attr( $path_prefix.'[' . $value['id'] . ']' ) . '" '. checked($val, 1, false) .' />';
//			break;
			// Uploader
			case "upload":
				$value['help-desc'] = isset($value['help-desc']) ? $value['help-desc'] : '';
				$value['help-pos'] = isset($value['help-pos']) ? $value['help-pos'] : 'top';

				if(isset($value['class']))
					$class = $value['class'];
				else
					$class = '';

				if(!$create_page_form)
					//$output .='';
					$output .= mp_medialibrary_uploader($value['id'], $class, $value['token'], $book_id, $page_id, $val, null, $value['desc'], $value['help-desc'], 0, '', $value['help-pos']); // New AJAX Uploader using Media Library
				else
					$rfbwp_page_form .= mp_medialibrary_uploader($value['id'], $class, $value['token'], $book_id, $page_id, $val, null, $value['desc'], $value['help-desc'], 0, '', $value['help-pos']);
			break;

			// PDF Uploader
			case "upload-file":
				$value['help-desc'] = isset($value['help-desc']) ? $value['help-desc'] : '';
				$value['help-pos'] = isset($value['help-pos']) ? $value['help-pos'] : 'top';

				if(isset($value['class']))
					$class = $value['class'];
				else
					$class = '';

				$output .= mp_medialibrary_file_uploader($value['id'], $class, $value['token'], $book_id, $page_id, $val, null, $value['desc'], $value['help-desc'], 0, '', $value['help-pos']); // New AJAX Uploader using Media Library
				break;
			// audio Uploader
			case "upload-audio":
				$value['help-desc'] = isset($value['help-desc']) ? $value['help-desc'] : '';
				$value['help-pos'] = isset($value['help-pos']) ? $value['help-pos'] : 'top';

				if(isset($value['class']))
					$class = $value['class'];
				else
					$class = '';

				$output .= mp_medialibrary_audio_uploader($value['id'], $class, $value['token'], $book_id, $page_id, $val, null, $value['desc'], $value['help-desc'], 0, '', $value['help-pos']); // New AJAX Uploader using Media Library
				break;
	

			// Button Grey Preview mp_medialibrary_audio_uploader
			case "button" :

				$tooltip = isset( $value['tooltip'] ) ? '<span class="tooltip">' . $value['tooltip'] . '</span>' : '';

				if(!$create_page_form) {
					$output .= '<a class="'.$value['class'].' mpc-button" href="#'.$page_id.'"><i class="dashicons '.(isset($value['icon']) ? $value['icon'] : '').'"></i> '.$value['name'] . $tooltip . '</a>';
				} else {
					$rfbwp_page_form .= '<a class="'.$value['class'].' mpc-button" href="#'.$page_id.'"><i class="dashicons '.(isset($value['icon']) ? $value['icon'] : '').'"></i> '.$value['name'] . $tooltip . '</a>';
				}

			break;

			// Color picker
			case "color":
				if(!$create_page_form)
					$output .= '<input class="mp-color mp-input-border" name="' . esc_attr( $path_prefix.'[' . $value['id'] . ']' ) . '" id="' . esc_attr( $value['id'] ) . '" type="text" value="' . esc_attr( $val ) . '" />';
				else
					$rfbwp_page_form .= '<input class="mp-color mp-input-border" name="' . esc_attr( $path_prefix.'[' . $value['id'] . ']' ) . '" id="' . esc_attr( $value['id'] ) . '" type="text" value="' . esc_attr( $val ) . '" />';

			break;

			// Info
			case "info":
				if(!$create_page_form)
					$output .= '<span id="' .esc_attr( $value['id']). '" class="info box-' .$value['color']. '">' .$value['desc']. '</span>';
				else
					$rfbwp_page_form .=	'<span id="' .esc_attr( $value['id']). '" class="info box-' .$value['color']. '">' .$value['desc']. '</span>';

			break;

			// Books (modul for flip book plugin)
			case "books":
				// display books in a table on front page
				$output .= rfbwp_get_front_books_table();
			break;

			case "pages":
				// dispaly pages
				$output .= get_books_front_pages_table($book_id);
			break;

			// Heading for Tabs
			case "heading":
				if($counter >= 2){
					if(!$create_page_form)
			  			$output .= '</div>'."\n";
			  		else
			  			$rfbwp_page_form .= '</div>'."\n";
				}

				$jquery_click_hook = preg_replace('/\W/', '', strtolower($value['name']) );
				$jquery_click_hook = "mp-option-" . $jquery_click_hook;

				if($begin_tabs){
					$tabs .= '<ul class="tab-group" id="' .$section_name. '-tab">';
					$begin_tabs = false;
				}

				$class = preg_split('/_/', esc_attr($value['name']));
				$class = strtolower($class[0]);

				$tabs .= '<li class="button-tab"><a id="'.  esc_attr( $jquery_click_hook ) . '-tab" class="'.$class.'" title="' . esc_attr( $value['name'] ) . '" href="' . esc_attr( '#'.  $jquery_click_hook ) . '"><span class="tab-bg-left"></span><span class="tab-bg-center"><span class="tab-text">' . esc_html( $value['name'] ) . '</span></span><span class="tab-bg-right"></span></a></li>';

				if(!$create_page_form) {
					$output .= '<div class="group '.$class.'" id="' . esc_attr( $jquery_click_hook ) . '">';
					$output .= '<div class="breadcrumbs">'
							. '<span class="breadcrumb-0 breadcrumb">' . __( 'Back', 'rfbwp' ) . '</span>' 
							. '<span class="breadcrumb-1 breadcrumb">' . __( 'Back', 'rfbwp' ) . '</span>'
							. '<span class="breadcrumb-5 breadcrumb">' . __( 'Back', 'rfbwp' ) . '</span>'
							. '<span class="breadcrumb-2 breadcrumb dsg" style="display: none;">' . __( 'All Page', 'rfbwp' ) . '</span>'
							. '<a class="edit-button-alt" href="#">' . __( 'Next Step', 'rfbwp' ) . '</a></div>';
				} else {
			  		$rfbwp_page_form .= '<div class="group '.$class.'" id="' . esc_attr( $jquery_click_hook ) . '">';
			  		$rfbwp_page_form .= '<div class="breadcrumbs">'
							//. '<span class="breadcrumb-0 breadcrumb">' . __( 'Books Shelf', 'rfbwp' ) . '</span>'
							. '<span class="breadcrumb-1 breadcrumb">' . __( 'Books Settings', 'rfbwp' ) . '</span>'
							. '<span class="breadcrumb-2 breadcrumb">' . __( 'Add Pages', 'rfbwp' ) . '</span>'
						 . '<a class="edit-button-alt" href="#">' . __( 'Next Step', 'rfbwp' ) . '</a></div>';
			  	}

				break;

			// Sidebar navigation
			case "section":
				if($counter >= 2) {
			  		$output .= '</div>'."\n";
			  		$tabs .= '</ul>'; // end tabs;
			  		$begin_tabs = true;
				}

				$jquery_click_hook = preg_replace('/\W/', '', strtolower($value['name']) );
				$jquery_click_hook = "mp-section-" . $jquery_click_hook;
				$section_name = $jquery_click_hook;
				$menu .= '<li class="button-sidebar"><a id="'.  esc_attr( $jquery_click_hook ) . '-button" title="' . esc_attr( $value['name'] ) . '" href="' . esc_attr( '#'.  $jquery_click_hook ) . '"></a></li>';
				$output .= '<div class="section-group" id="' . esc_attr( $jquery_click_hook ) . '">';
			break;

			case "top-socials":
//				$header .= '<ul class="socials">';
//				foreach($value['options'] as $key => $val) {
//					$header .= '<li class="social">'
//							. '<a class="mpc-button" href="' .($key == 'email' ? 'mailto:' : '').$val[2]. '">'
//							. '<i class="dashicons ' . $val[0] . '"></i></a></li>';
//				}
//
//				$header .= '</ul>';
			break;


			case "top-header":
				//$header .= '<h2 class="main-header">' . esc_attr( $value['name'] ) . '</h2>';
				//$header .= '<h3 class="main-desc">' . esc_attr( $value['desc'] ) . '</h3>';
			break;

			case "icon":
				$class = empty( $val ) ? 'mpc-icon-select-empty' : '';

				$output .=  '<a href="#" class="mpc-icon-select ' . $class . ' mp-input-border"><i class="' . esc_attr( $val ) . '"></i></a>'
						.'<a href="#" class="mpc-icon-select-clear fa fa-times"></a>'
						.'<input type="hidden" id="' . esc_attr( $path_prefix.'[' . $value['id'] . ']' ) . '" name="' . esc_attr( $path_prefix.'[' . $value['id'] . ']' ) . '" value="' . esc_attr( $val ) . '" class="mpc-text-field mpc-icon-select-value" />';
			break;

		}

		if (($value['type'] != "heading") && ($value['type'] != "section")  && ($value['type'] != "top-header") && ($value['type'] != "top-socials")) {

			// this code is for the descript & help
			if (isset($value['help']) && $value['help'] == "true" && !isset($value['stack'])) {
				if(!$create_page_form)
					$output .= '<div class="help-icon"><span class="mp-tooltip '.(isset($value['help-pos']) ? $value['help-pos'] : 'top').'">'.$value['help-desc'].'</span></div></div>';
				else
					$rfbwp_page_form .= '<div class="help-icon"><span class="mp-tooltip '.(isset($value['help-pos']) ? $value['help-pos'] : 'top').'">'.$value['help-desc'].'</span></div></div>';
			} else {
				if(!$create_page_form)
					$output .= '</div>';
				else
					$rfbwp_page_form .= '</div>';
			}

			$description = '';
			if ( isset( $value['desc'] ) ) {
				$description = $value['desc'];
			}

			if($desc == 'bottom' && ($value['type'] != "info") && !isset($value['class'])) {
				if(!$create_page_form)
					$output .= '<div class="description-bottom">' . wp_kses( $description, $allowedtags) . '</div>'."\n";
				else
					$rfbwp_page_form .= '<div class="description-bottom">' . wp_kses( $description, $allowedtags) . '</div>'."\n";
			} elseif($desc == 'right' && ($value['type'] != "info") && !isset($value['class'])) {
				if(!$create_page_form)
					$output .= '<div class="description">' . wp_kses( $description, $allowedtags) . '</div>'."\n";
				else
					$rfbwp_page_form .= '<div class="description">' . wp_kses( $description, $allowedtags) . '</div>'."\n";
			}
			// the end of description code
			if($hide == 'true') {
				$output .= '</div>';
			}


			if(!$create_page_form) {
				if(isset($value['toggle']) && $value['toggle'] == 'end' && $stacked) {
					$output .= '<div class="clear"></div></div></div></div></div>';
					$toggle = false;
					$stacked = false;
				} else if(isset($value['toggle']) && $value['toggle'] == 'end' && !$stacked) {
					$output .= '<div class="clear"></div></div></div></div>';
					$toggle = false;
				} else if(isset($value['stack']) && $value['stack'] == 'end') {
					$output .= '<div class="clear"></div></div></div></div>';
					$stacked = false;
				} elseif(!$stacked) {
					$output .= '<div class="clear"></div></div></div>';
				} elseif($stacked) {
					$output .= '</div></div>';
				}

			} else {
				if(isset($value['toggle']) && $value['toggle'] == 'end' && $stacked) {
					$rfbwp_page_form .= '<div class="clear"></div></div></div></div></div>';
					$toggle = false;
					$stacked = false;
				} else if(isset($value['toggle']) && $value['toggle'] == 'end' && !$stacked) {
					$output .= '<div class="clear"></div></div></div></div>';
					$toggle = false;
				} else if(isset($value['stack']) && $value['stack'] == 'end') {
					$rfbwp_page_form .= '<div class="clear"></div></div></div></div>';
					$stacked = false;
				} elseif(!$stacked) {
					$rfbwp_page_form .= '<div class="clear"></div></div></div></div>';
				} elseif($stacked) {
					$rfbwp_page_form .= '</div></div>';
				}
			}
		}
	}
	$tabs .= '</ul>';
	$menu .= '</ul>';

	if(!$create_page_form)
   		$output .= '</div>';
   	else
		$rfbwp_page_form .= '</div>';

	$_POST['page_form'] = $rfbwp_page_form;

    return array($output, $menu, $tabs, $header);
}

function rfbwp_get_front_books_table(){
    
        global $settings;
	global $rfbwp_shortname;
        //echo "out";
	$output = '';
	$settings = rfbwp_get_settings();
	$output .= '<div class="add-new-book-wrap">';//<a class="cancel-convert-book mpc-button revert close" href="#"><i class="dashicons dashicons-no"></i> Cancel</a>
       // $output .= apply_filters('rfbwp/addNewBook', 'rfb_pdf_convert_button' );
	
        $output .= apply_filters( 'rfbwp/addNewBook','<a class="add-book mpc-button revert" href="#"><i class="dashicons dashicons-book-alt"></i> ' . __( 'Create New Book', 'rfbwp' ) .'</a> ') ;
	$output .= '</div>';

 	$output .= '<div class="no-books-added"><img class="rfbwp-first-book" src="' . MPC_PLUGIN_ROOT . '/massive-panel/images/add_new_book.png" /></div>';
	 

	//$output .= apply_filters( 'rfbwp/afterTable', '' );

	$output .= '<div class="add-new-book-wrap">';
	$output .= apply_filters( 'rfbwp/addNewBookBottom', '<a class="add-book mpc-button revert" href="#"><i class="dashicons dashicons-book-alt"></i> ' . __( 'Create New Book', 'rfbwp' ) .'</a>' );
        //$output .= '<a class="add-book mpc-button revert" href="#"><i class="dashicons dashicons-book-alt"></i> ' . __( 'Create New Book', 'rfbwp' ) .'</a>' ;
	$output .= '</div>';
                        
//echo "ou2t";
	return $output;
}
function current_user_books($settings,$current_user){
    $count=0;
    if(isset($settings['books'])){
    for($i = 0; $i < count($settings['books']); $i++) {
        if(is_admin()){
            $count++;
        }else{
        if($settings['books'][$i]['fbwp_uid'] == $current_user->ID){$count++;}
        }
    }
    }
    return $count;

}
function get_books_front_pages_table($bookID) {
	global $settings;
	global $rfbwp_shortname;
	$output = '';
	$settings = rfbwp_get_settings();

	$output .= '<table class="fpages-table"><tbody>';

	$page_count = isset($settings['books'][$bookID]['pages']) ? count($settings['books'][$bookID]['pages']) : 0;

	$j = -1;

	for($i = 0; $i < $page_count; $i++) {
		$j++;

		$page_type  = isset($settings['books'][$bookID]['pages'][$i][$rfbwp_shortname.'_fb_page_type']) ? $settings['books'][$bookID]['pages'][$i][$rfbwp_shortname.'_fb_page_type'] : '';
		$page_index = isset($settings['books'][$bookID]['pages'][$i][$rfbwp_shortname.'_fb_page_index']) ? $settings['books'][$bookID]['pages'][$i][$rfbwp_shortname.'_fb_page_index'] : '';

		$output .= '<tr id="page-display_'.$j.'" class="display fpage-row-bg" >'
                        . '<td id="pimg'.$i.'" class="fthumb-preview page-img-td">';

		if(isset($settings['books'][$bookID]['pages'][$i][$rfbwp_shortname.'_fb_page_bg_image']) && $settings['books'][$bookID]['pages'][$i][$rfbwp_shortname.'_fb_page_bg_image'] != '')
			$output .= '<img class="fb-dyn-images" data-src="'.$settings['books'][$bookID]['pages'][$i][$rfbwp_shortname.'_fb_page_bg_image'].'" src="'.MPC_PLUGIN_ROOT.'/massive-panel/images/no-image.png" alt=""/>';
		else
			$output .= '<div class="no-cover"></div>';

		$output .= '</td><td class="page-type-td">'
                        . '<div class="fpage-type">'.$page_type.'</div>';
               

		$output .= '<div class="fbtn-shadow-off">';
		$output .= '<a class="add-page mpc-button fmpc-button" href="#'.$bookID.'"><i class="dashicons dashicons-plus"></i> <span class="tooltip">'. __('Add New Page', 'rfbwp' ) . '</span></a>';
		$output .= '<a class="edit-page mpc-button fmpc-button" href="#'.$bookID.'"><i class="dashicons dashicons-edit"></i> <span class="tooltip">'. __('Edit Page', 'rfbwp' ) . '</span></a>';
		$output .= '<a class="preview-page mpc-button" href="#'.$bookID.'"><i class="dashicons dashicons-visibility"></i> <span class="tooltip">'. __('Page Preview', 'rfbwp' ) . '</span></a>';
		$output .= '<a class="delete-page mpc-button fmpc-button" href="#'.$bookID.'"><i class="dashicons dashicons-trash"></i> <span class="tooltip">'. __('Delete Page', 'rfbwp' ) . '</span></a>';
		
		$output .= '<a class="add-audio-page mpc-button fmpc-button" href="#'.$bookID.'"><i class="fa fa-file-audio-o"></i> <span class="tooltip">'. __('Add Audio', 'rfbwp' ) . '</span></a>';
		
		$output .= '</div>';
                

		$output .= '</td>'
                        . '<td class="navigation page-btn-td">';

		$output .= '<a class="up-page mpc-button" href="#'.$bookID.'"><i class="dashicons dashicons-arrow-up-alt2"></i></a>';
		$output .= '<input type="checkbox" class="page-checkbox" />';
		$output .= '<span class="desc">page</span>';
		if($page_type != 'Double Page')
			$output .= '<span class="page-index">'.$page_index.'</span>';
		else
			$output .= '<span class="page-index">'.$page_index.' - '.(int)($page_index+1).'</span>';
		$output .= '<a class="down-page mpc-button" href="#'.$bookID.'"><i class="dashicons dashicons-arrow-down-alt2"></i></a>';

		$output .= '</td>'
                        . '<td class="mpc-sortable-handle page-arrow-td"><i class="fa fa-arrows-v"></i></td></tr>'
                        . '<tr id="pset_'.$j.'" class="page-set"><td collspan="3"></td></tr>';
	}

	if($page_count == 0) {
		$output .= '<tr id="pset_0" class="page-set"><td collspan="3"></td></tr>';
	}

	$output .= '</tbody></table>';

	return $output;
}


function rfbwp_get_books(){
$cat=isset($_GET['cat_type']) ? $_GET['cat_type'] : '';
global $wpdb, $paged;
$paged = (get_query_var('page')) ? get_query_var('page') : 1;
if($cat=='new-books'){
    $bkstart = ($paged == 1) ? 0 : intval($paged-1) * 12;    
}
else $bkstart=0;
//create custom loop for books
if(!is_user_logged_in()){   
    if(isset($_GET['sortbook']) && $_GET['sortbook'] != ""){
        if($_GET['sortbook'] == 'endorsements'){
            $endorsements = true;
            $bktemp = "SELECT p.*,endocount FROM wp_posts p left join (select *,count(*) as endocount from wp_endorsements group by post_id ) endors on p.ID = endors.post_id where p.post_type = 'dyn_book' AND p.post_status = 'publish'  order by endors.endocount desc ";   
            $bkquery = $bktemp.' LIMIT  ' . $bkstart .', 12'; 
        }elseif ($_GET['sortbook'] == 'views') {
            $views = true;
            $bktemp= 'SELECT *  FROM wp_posts AS p JOIN wp_postmeta AS meta WHERE p.post_type="dyn_book" AND p.post_status="publish" AND p.ID = meta.post_id AND meta.meta_key="popularity_count" ORDER BY meta.meta_value' ;
            $bkquery = $bktemp.' LIMIT  ' . $bkstart .', 12';
        }elseif ($_GET['sortbook'] == 'favourites') {
            $favourites = true;
            $bktemp = "SELECT p.*,favcount FROM wp_posts p left join (select *,count(*) as favcount from wp_favorite group by post_id ) favorite on p.ID = favorite.post_id where p.post_type = 'dyn_book' AND p.post_status = 'publish'  order by favorite.favcount desc";
            $bkquery = $bktemp.' LIMIT  ' . $bkstart .', 12';
        }elseif ($_GET['sortbook'] == 'comments') {
            $comments = true;
            $bktemp = "SELECT * FROM wp_posts where post_type = 'dyn_book' AND post_status = 'publish' order by comment_count ";  
            $bkquery = $bktemp.' LIMIT  ' . $bkstart .', 12'; 
        }    
    }else{
        $bktemp= 'SELECT * FROM ' .$wpdb->prefix.'posts WHERE post_type="dyn_book" AND post_status="publish" AND ID NOT IN (SELECT post_id FROM '.$wpdb->prefix.'postmeta WHERE meta_key="privacy-option" AND meta_value="private") ORDER BY STR_TO_DATE( '.$wpdb->prefix. 'posts.post_date_gmt , "%Y-%m-%d %H:%i:%s")  DESC' ;
        $bkquery = $bktemp.' LIMIT  ' . $bkstart .', 12';
    }
    $bkresult = $wpdb->get_results( $bkquery ); 
    $bktotal=$wpdb->query($bktemp);
}else if(is_user_logged_in()){
    if(isset($_GET['sortbook']) && $_GET['sortbook'] != ""){
        if($_GET['sortbook'] == 'endorsements'){
            $endorsements = true;        
            $bktemp = 'SELECT * FROM wp_posts AS p INNER JOIN (select *,count(*) as endocount from wp_endorsements group by post_id ) endors on p.ID = endors.post_id WHERE p.post_type="dyn_book" AND p.post_status="publish" AND p.ID NOT IN (SELECT post_id FROM wp_postmeta WHERE meta_key="privacy-option" AND meta_value="private")
            UNION ALL
            SELECT * FROM wp_posts AS p INNER JOIN (select *,count(*) as endocount from wp_endorsements group by post_id ) endors on p.ID = endors.post_id WHERE p.post_type="dyn_book" AND p.post_status="publish" AND p.ID IN (SELECT post_id FROM wp_postmeta WHERE meta_key="privacy-option" AND meta_value="private") AND p.post_author='.get_current_user_id().'
            UNION ALL
            SELECT * FROM wp_posts AS p INNER JOIN (select *,count(*) as endocount from wp_endorsements group by post_id ) endors on p.ID = endors.post_id WHERE p.post_type="dyn_book"  AND p.post_status="publish" AND p.ID IN (SELECT post_id
            FROM wp_postmeta WHERE meta_key = "select_users_list" AND LENGTH(meta_value)>0  AND meta_value REGEXP "[[:<:]]'.get_current_user_id().'[[:>:]]") ORDER BY CAST(endocount AS SIGNED INTEGER ) DESC';
            $bkquery = $bktemp.' LIMIT  ' . $bkstart .', 12';
        }elseif ($_GET['sortbook'] == 'views') {
            $views = true;
            $bktemp = 'SELECT * FROM wp_posts AS p INNER JOIN wp_postmeta AS meta ON p.ID=meta.post_id WHERE p.post_type="dyn_book" AND p.post_status="publish" AND p.ID NOT IN (SELECT post_id FROM wp_postmeta WHERE meta_key="privacy-option" AND meta_value="private")
            UNION ALL
            SELECT * FROM wp_posts AS p INNER JOIN wp_postmeta AS meta ON p.ID=meta.post_id WHERE p.post_type="dyn_book" AND p.post_status="publish" AND p.ID IN (SELECT post_id FROM wp_postmeta WHERE meta_key="privacy-option" AND meta_value="private") AND p.post_author='.get_current_user_id().'
            UNION ALL
            SELECT * FROM wp_posts AS p INNER JOIN wp_postmeta AS meta ON p.ID=meta.post_id WHERE p.post_type="dyn_book"  AND p.post_status="publish" AND p.ID IN (SELECT post_id
            FROM wp_postmeta WHERE meta_key = "select_users_list" AND LENGTH(meta_value)>0  AND meta_value REGEXP "[[:<:]]'.get_current_user_id().'[[:>:]]") AND meta.meta_key="popularity_count" ORDER BY CAST(meta_value AS SIGNED INTEGER ) DESC ';
            $bkquery = $bktemp.' LIMIT  ' . $bkstart .', 12';

        }elseif ($_GET['sortbook'] == 'favourites') {
            $favourites = true;
            $bktemp = 'SELECT * FROM wp_posts AS p INNER JOIN (select *,count(*) as favcount from wp_favorite group by post_id ) favorite on p.ID = favorite.post_id WHERE p.post_type="dyn_book" AND p.post_status="publish" AND p.ID NOT IN (SELECT post_id FROM wp_postmeta WHERE meta_key="privacy-option" AND meta_value="private")
            UNION ALL
            SELECT * FROM wp_posts AS p INNER JOIN (select *,count(*) as favcount from wp_favorite group by post_id ) favorite on p.ID = favorite.post_id WHERE p.post_type="dyn_book" AND p.post_status="publish" AND p.ID IN (SELECT post_id FROM wp_postmeta WHERE meta_key="privacy-option" AND meta_value="private") AND p.post_author='.get_current_user_id().'
            UNION ALL
            SELECT * FROM wp_posts AS p INNER JOIN (select *,count(*) as favcount from wp_favorite group by post_id ) favorite on p.ID = favorite.post_id WHERE p.post_type="dyn_book"  AND p.post_status="publish" AND p.ID IN (SELECT post_id
            FROM wp_postmeta WHERE meta_key = "select_users_list" AND LENGTH(meta_value)>0  AND meta_value REGEXP "[[:<:]]'.get_current_user_id().'[[:>:]]") ORDER BY CAST(favcount AS SIGNED INTEGER ) DESC';
            
            $bkquery = $bktemp.' LIMIT  ' . $bkstart .', 12';

        }elseif ($_GET['sortbook'] == 'comments') {
            $comments = true;
            $bktemp = 'SELECT * FROM ' .$wpdb->prefix.'posts WHERE post_type="dyn_book" AND post_status="publish" AND ID NOT IN (SELECT post_id FROM '.$wpdb->prefix.'postmeta WHERE meta_key="privacy-option" AND meta_value="private")
            UNION
            SELECT * FROM '.$wpdb->prefix.'posts WHERE post_type="dyn_book" AND post_status="publish" AND ID IN (SELECT post_id FROM '.$wpdb->prefix.'postmeta WHERE meta_key="privacy-option" AND meta_value="private") AND post_author='.get_current_user_id().'
            UNION
            SELECT * FROM '.$wpdb->prefix.'posts WHERE post_type="dyn_book"  AND post_status="publish" AND ID IN (SELECT post_id
            FROM '.$wpdb->prefix.'postmeta WHERE meta_key = "select_users_list" AND LENGTH(meta_value)>0  AND meta_value REGEXP "[[:<:]]'.get_current_user_id().'[[:>:]]") ORDER BY  comment_count  DESC ';
            $bkquery = $bktemp.' LIMIT  ' . $bkstart .', 12'; 
        }    
    }else{
        $bktemp = 'SELECT * FROM ' .$wpdb->prefix.'posts WHERE post_type="dyn_book" AND post_status="publish" AND ID NOT IN (SELECT post_id FROM '.$wpdb->prefix.'postmeta WHERE meta_key="privacy-option" AND meta_value="private")
    UNION ALL
    SELECT * FROM '.$wpdb->prefix.'posts WHERE post_type="dyn_book" AND post_status="publish" AND ID IN (SELECT post_id FROM '.$wpdb->prefix.'postmeta WHERE meta_key="privacy-option" AND meta_value="private") AND post_author='.get_current_user_id().'
    UNION ALL
    SELECT * FROM '.$wpdb->prefix.'posts WHERE post_type="dyn_book"  AND post_status="publish" AND ID IN (SELECT post_id
    FROM '.$wpdb->prefix.'postmeta WHERE meta_key = "select_users_list" AND LENGTH(meta_value)>0  AND meta_value REGEXP "[[:<:]]'.get_current_user_id().'[[:>:]]") ORDER BY  STR_TO_DATE(post_date_gmt,"%Y-%m-%d %H:%i:%s")  DESC ';
        
        $bkquery = $bktemp.' LIMIT  ' . $bkstart .', 12';
    }
    $bkresult = $wpdb->get_results( $bkquery); 
    $bktotal=$wpdb->query($bktemp);
}

?>

<div class="list-group" id="new-book-tab">
<?php echo '<a href="#new-books" id="new-books"><h5><b>NEW BOOKS</b></h5></a>'; ?>
<!-- Sort block -->	
<div class="cover-twoblocks">
    <div class="sortvideo-div">
        <form method="get" id="sortbook-form">
            <p><label>Currently Sorted By : </label>
                <select class="sortbook" name="sortbook">
                    <option value="recent" <?= ($selected)?'selected':'';?>>Recent</option>
                    <option value="views" <?= ($views)?'selected':'';?>>Views</option>  
                    <option value="endorsements" <?= ($endorsements)?'selected':'';?>>Endorsements</option>
                    <option value="favourites" <?= ($favourites)?'selected':'';?>>Favorites</option>
                    <option value="comments" <?= ($comments)?'selected':'';?>>comments</option>
                </select>
            </p>
        </form>
    </div>
    
    <div class="well well-sm">
        <strong>Views</strong>
        <div class="btn-group">
            <a href="" id="list-book" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th-list"></span>List
            </a>
            <a href="" id="grid-book" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th"></span>Grid</a>
        </div>
    </div>
</div>
<div class="alert alert-success v-d" style="display:none;">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<strong>Success!</strong> book is Deleted.
	</div>
	<div class="row">				 
<?php 
if($bkresult)
{
$bkri=0;
foreach ($bkresult as $bkdata){
    $postID = $bkdata->ID; 
    $post = get_post($postID);
    $privacyOption   = get_post_meta( $postID, 'privacy-option' ); 
    $selectUsersList = get_post_meta( $postID, 'select_users_list' ); 
    $post_author     = $bkdata->post_author;
    $user_ID         = get_current_user_id(); 
    $selectUsersList = explode( ",", $selectUsersList[0] ); 
	$refr = get_post_meta($postID,'video_options_refr',true);
	if($refr){}else{$refr= 'No reference given'; }
    $thumbnail_id = get_post_thumbnail_id($postID);
    $thumbnail_url=wp_get_attachment_image_src($thumbnail_id, array(240,240), true);
    $imgsrc= $thumbnail_url [0];
    $revnum =apply_filters( 'dyn_number_of_post_review', $postID, "post");
    $endors=$wpdb->get_results( 'SELECT user_id FROM '.$wpdb->prefix.'endorsements WHERE post_id = ' . $postID);
    $endorsnum=count($endors);
    $favs=$wpdb->get_results('SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id='. $postID);
    $favsnum=count($favs);
    $book_author=get_the_author_meta('display_name', $bkdata->post_author);
    //echo "privacy".$privacyOption[0].is_user_logged_in();             
        if(!is_user_logged_in()) {  // case where user is not logged in 
           if(isset($privacyOption[0]) and $privacyOption[0] == "public") {            
			  //if( ($bkri % 6) == 0){echo '</div><div class="row">'; }
			    ?>
					<div class="bookitem col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 text-center" style="padding: 5px 5px 20px 5px !important; margin:0px !important; height:330px;">
		                 <div class="thumbnail layout-2-wrapper solid-bg" style="height:100%; width:100% !important;">
					   	   <div class="image-holder">
						   <a href="<?php echo get_permalink($postID); ?>">
						   <div class="hover-item"></div>
						   </a>    	
								<?php							
							    if( has_post_thumbnail($postID) ){
                                    echo get_the_post_thumbnail($postID, 'medium-thumb', array( 'class' => 'layout-3-thumb', 'style'=>"height:140px; max-width:220px;" ) );
                                } else {
                                    echo '<img src="'.get_template_directory_uri().'/images/no-image.png" class="layout-3-thumb" style="height:140px; max-width:220px;">';
                                } ?>											
						   </div>
								<div class="layout-title-box text-center" style="display:block;">
								<h6 class="layout-title" style="max-width:100%; word-wrap: break-word; height:40px;">
								<?php
								preg_match_all('/(\w+\s)/', $bkdata->post_title, $matches);
								if ( count($matches[0]) && count($matches[1]) >4){
								?>
								<a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $matches[1][0] .  $matches[1][1] . $matches[1][2] . '[...]'; ?></a>
								<?php 
									}
								else if(strlen($bkdata->post_title)>30){ 
							   	?>
							    <a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo substr($bkdata->post_title, 0, 30) .' [...]'; ?></a>

							   <?php
							    }
								else {
										?>
								<a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $bkdata->post_title; ?></a>

								<?php } ?>
								</h6>
								<!-- <ul class="stats">
									<li ><?php videopress_displayviews( $postID ); ?></li>
									<li><?php //echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
									<li>Author: <font color="red"><?php //echo get_the_author($postID); ?></font></li>
									
								</ul>
								-->
                                <div class="layout-2-details book-expand-detail-$postID" style="display:none;">
                                    <?php echo apply_filters( 'dyn_display_review', $output, $postID, "post" ); ?>
                                    <div class="video_excerpt_text one">
                                        <h4 class="tittle-h4">Description</h4>
                                        <div class="inline-blockright"><?php 
                                            if($bkdata->post_content ==""){echo "No description available.";}else{
                                                echo $bkdata->post_content;
                                            } 
                                        ?></div>
                                    </div>                  
                                    <ul class="stats">
                                        <li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
                                        <li><?php videopress_countviews($postID); ?></li>
                                        <li><i class="fa fa-wrench"></i> <?php echo $endorsnum; ?> Endorsments</li>
                                        <li><i class="fa fa-heart"></i><?php echo $favsnum; ?> Favorites</li>
                                        <li><i class="fa fa-star-o"></i> <?php echo $revnum; ?></li>
                                        <li><?php comments_number(); ?></li>
                                    </ul>                  
                                </div>

								<ul class="bottom-detailsul" style="vertical-align:baseline;">
								<li><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>"><i class="fa fa-trash-o"></i> Delete</a></li>
								<li><a class="detailsblock-btn" data-modal-id="<?php echo $postID; ?>" data-author="<?php echo $book_author; ?>" data-time="<?php echo human_time_diff(get_the_time('U', $postID), current_time('timestamp') ) . ' ago'; ?>" data-views="<?php videopress_displayviews( $postID ); ?>" data-title="<?php echo $bkdata->post_title; ?>" data-permalink="<?php echo get_permalink($postID); ?>" data-content="<?php echo strip_tags($bkdata->post_content); ?>" data-image="<?php echo $imgsrc; ?>" data-comments="<?php echo $bkdata->comment_count; ?>" data-ref="<?php echo $refr ?>" data-endorse="<?php echo $endorsnum?>" data-favr="<?php echo $favsnum; ?> " data-revs="<?php echo $revnum ?>">
								<i class="fa fa-info-circle" aria-hidden="true"></i> Details
								</a>
								</li>
							   <li><a class="changesettingsblock-btn" data-modal-id="myModalPrivacy-<?php the_ID(); ?>"><i class="fa fa-info-circle" aria-hidden="true"></i>Privacy</a></li>
							
							   </ul>
							   </div>
							   
						 </div>
					   </div>		
			   <?php
			    $bkri++;
		  }
		  else
		  {
			    if(!isset($privacyOption[0]))
				{
					 //if( ($bkri % 6) == 0){echo '</div><div class="row">'; }
			    ?>
					<div class="bookitem col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 text-center" style="padding: 5px 5px 20px 5px !important; margin:0px !important; height:330px;">
		                <div class="thumbnail layout-2-wrapper solid-bg" style="height:100%; width:100% !important;">
					   	   <div class="image-holder">
						   <a href="<?php echo get_permalink($postID); ?>">
						   <div class="hover-item"></div>
						   </a>            
								<?php							
							   if( has_post_thumbnail($postID) ){
									echo get_the_post_thumbnail($postID, 'medium-thumb', array( 'class' => 'layout-3-thumb', 'style'=>"height:140px; max-width:220px;" ) );
								}else{
										echo '<img src="'.get_template_directory_uri().'/images/no-image.png" class="layout-3-thumb" style="height:140px; max-width:220px;">';
								}?>											
						   </div>
								<div class="layout-title-box text-center" style="display:block;">
								<h6 class="layout-title" style="max-width:100%; word-wrap: break-word; height:40px;">
								<?php
								preg_match_all('/(\w+\s)/', $bkdata->post_title, $matches);

								if ( count($matches[0]) && count($matches[1]) >4){;
								?>
								<a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $matches[1][0] .  $matches[1][1] . $matches[1][2] . '[...]'; ?></a>
								<?php 
									}
								else if(strlen($bkdata->post_title)>30){ 
							   	?>
							    <a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo substr($bkdata->post_title, 0, 30) .' [...]'; ?></a>

							   <?php
							    }
								else {
										?>
								<a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $bkdata->post_title; ?></a>

								<?php } ?>
								</h6>
								<!-- <ul class="stats">
									<li ><?php videopress_displayviews( $postID ); ?></li>
									<li><?php //echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
									<li>Author: <font color="red"><?php //echo get_the_author($postID); ?></font></li>
									
								</ul>
								-->
                                <div class="layout-2-details book-expand-detail-$postID" style="display:none;">
                                    <?php echo apply_filters( 'dyn_display_review', $output, $postID, "post" ); ?>
                                    <div class="video_excerpt_text one">
                                        <h4 class="tittle-h4">Description</h4>
                                        <div class="inline-blockright"><?php 
                                            if($bkdata->post_content ==""){echo "No description available.";}else{
                                                echo $bkdata->post_content;
                                            } 
                                        ?></div>
                                    </div>                     
                                    <ul class="stats">
                                        <li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
                                        <li><?php videopress_countviews($postID); ?></li>
                                        <li><i class="fa fa-wrench"></i> <?php echo $endorsnum; ?> Endorsments</li>
                                        <li><i class="fa fa-heart"></i><?php echo $favsnum; ?> Favorites</li>
                                        <li><i class="fa fa-star-o"></i> <?php echo $revnum; ?></li>
                                        <li><?php comments_number(); ?></li>
                                    </ul>                  
                                </div>
								<ul class="bottom-detailsul" style="vertical-align:baseline;">
								<li><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>"><i class="fa fa-trash-o"></i> Delete</a></li>
								
								<li><a class="detailsblock-btn" data-modal-id="<?php echo $postID; ?>" data-author="<?php echo $book_author; ?>" data-time="<?php echo human_time_diff(get_the_time('U', $postID), current_time('timestamp') ) . ' ago'; ?>" data-views="<?php videopress_displayviews( $postID ); ?>" data-title="<?php echo $bkdata->post_title; ?>" data-permalink="<?php echo get_permalink($postID); ?>" data-content="<?php echo strip_tags($bkdata->post_content); ?>" data-image="<?php echo $imgsrc; ?>" data-comments="<?php echo $bkdata->comment_count; ?>" data-ref="<?php echo $refr ?>" data-endorse="<?php echo $endorsnum?>" data-favr="<?php echo $favsnum; ?> " data-revs="<?php echo $revnum ?>">
								<i class="fa fa-info-circle" aria-hidden="true"></i> Details
								</a>
								</li>
							   <li><a class="changesettingsblock-btn" data-modal-id="myModalPrivacy-<?php the_ID(); ?>"><i class="fa fa-info-circle" aria-hidden="true"></i>Privacy</a></li>
							
							   </ul>
							   </div>
							   
						 </div>
					   </div>		
			   <?php
			    $bkri++;
				}
		  }
		} 
		else {  // case where user is logged in
			 if($post_author == $user_ID)	
			 {    // Case where logged in User is same as Video Author User
		         // if( ($bkri % 6) == 0){echo '</div><div class="row">'; }
					    ?>
							<div class="bookitem col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 text-center" style="padding: 5px 5px 20px 5px !important; margin:0px !important; height:330px;">

    			              <div class="thumbnail layout-2-wrapper solid-bg" style="height:100%; width:100% !important;">

								  
							   	   <div class="image-holder">
								   <a href="<?php echo get_permalink($postID); ?>">
								   <div class="hover-item"></div>
								   </a>            
												
											<?php							
										   if( has_post_thumbnail($postID) ){
												echo get_the_post_thumbnail($postID, 'medium-thumb', array( 'class' => 'layout-3-thumb', 'style'=>"height:140px; max-width:220px;" ) );
											}else{
													echo '<img src="'.get_template_directory_uri().'/images/no-image.png" class="layout-3-thumb" style="height:140px; max-width:220px;">';
											}?>

											
								   </div>
										<div class="layout-title-box text-center" style="display:block;">
										<h6 class="layout-title" style="max-width:100%; word-wrap: break-word; height:40px;">
										<?php
										preg_match_all('/(\w+\s)/', $bkdata->post_title, $matches);

										if ( count($matches[0]) && count($matches[1]) >4){;
										?>
										<a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $matches[1][0] .  $matches[1][1] . $matches[1][2] . '[...]'; ?></a>
										<?php 
											}
										else if(strlen($bkdata->post_title)>30){ 
									   	?>
									    <a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo substr($bkdata->post_title, 0, 30) .' [...]'; ?></a>

									   <?php
									    }
										else {
												?>
										<a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $bkdata->post_title; ?></a>

										<?php } ?>
										</h6>
										<!-- <ul class="stats">
											<li ><?php videopress_displayviews( $postID ); ?></li>
											<li><?php //echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
											<li>Author: <font color="red"><?php //echo get_the_author($postID); ?></font></li>
											
										</ul>
										-->
                                        <div class="layout-2-details book-expand-detail-$postID" style="display:none;">
                                            <?php echo apply_filters( 'dyn_display_review', $output, $postID, "post" ); ?>
                                            <div class="video_excerpt_text one">
                                                <h4 class="tittle-h4">Description</h4>
                                                <div class="inline-blockright"><?php 
                                                    if($bkdata->post_content ==""){echo "No description available.";}else{
                                                        echo $bkdata->post_content;
                                                    } 
                                                ?></div>
                                            </div>                    
                                            <ul class="stats">
                                                <li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
                                                <li><?php videopress_countviews($postID); ?></li>
                                                <li><i class="fa fa-wrench"></i> <?php echo $endorsnum; ?> Endorsments</li>
                                                <li><i class="fa fa-heart"></i><?php echo $favsnum; ?> Favorites</li>
                                                <li><i class="fa fa-star-o"></i> <?php echo $revnum; ?></li>
                                                <li><?php comments_number(); ?></li>
                                            </ul>                  
                                        </div>
										<ul class="bottom-detailsul" style="vertical-align:baseline;">
										<li><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>"><i class="fa fa-trash-o"></i> Delete</a></li>
								
										<li><a class="detailsblock-btn" data-modal-id="<?php echo $postID; ?>" data-author="<?php echo $book_author; ?>" data-time="<?php echo human_time_diff(get_the_time('U', $postID), current_time('timestamp') ) . ' ago'; ?>" data-views="<?php videopress_displayviews( $postID ); ?>" data-title="<?php echo $bkdata->post_title; ?>" data-permalink="<?php echo get_permalink($postID); ?>" data-content="<?php echo strip_tags($bkdata->post_content); ?>" data-image="<?php echo $imgsrc; ?>" data-comments="<?php echo $bkdata->comment_count; ?>" data-ref="<?php echo $refr ?>" data-endorse="<?php echo $endorsnum?>" data-favr="<?php echo $favsnum; ?> " data-revs="<?php echo $revnum ?>">
										<i class="fa fa-info-circle" aria-hidden="true"></i> Details
										</a>
										</li>
									   <li><a class="changesettingsblock-btn" data-modal-id="myModalPrivacy-<?php the_ID(); ?>"><i class="fa fa-info-circle" aria-hidden="true"></i>Privacy</a></li>
							
									   </ul>
									   </div>
									   
								 </div>
							   </div>		
					   <?php
					    $bkri++;
			 }	
			 else
			 {    // Case where logged in User is not same as Video Author User                
				  if(isset($privacyOption[0]) and $privacyOption[0] == "public")
				  {

					  //if( ($bkri % 6) == 0){echo '</div><div class="row">'; }
					    ?>
							<div class="bookitem col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 text-center" style="padding: 5px 5px 20px 5px !important; margin:0px !important; height:330px;">

    			              <div class="thumbnail layout-2-wrapper solid-bg" style="height:100%; width:100% !important;">								  
							   	   <div class="image-holder">
								   <a href="<?php echo get_permalink($postID); ?>">
								   <div class="hover-item"></div>
								   </a>   		
										<?php							
									    if( has_post_thumbnail($postID) ){
											echo get_the_post_thumbnail($postID, 'medium-thumb', array( 'class' => 'layout-3-thumb', 'style'=>"height:140px; max-width:220px;" ) );
										}else{
												echo '<img src="'.get_template_directory_uri().'/images/no-image.png" class="layout-3-thumb" style="height:140px; max-width:220px;">';
										}?>

											
								   </div>
										<div class="layout-title-box text-center" style="display:block;">
										<h6 class="layout-title" style="max-width:100%; word-wrap: break-word; height:40px;">
										<?php
										preg_match_all('/(\w+\s)/', $bkdata->post_title, $matches);

										if ( count($matches[0]) && count($matches[1]) >4){;
										?>
										<a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $matches[1][0] .  $matches[1][1] . $matches[1][2] . '[...]'; ?></a>
										<?php 
											}
										else if(strlen($bkdata->post_title)>30){ 
									   	?>
									    <a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo substr($bkdata->post_title, 0, 30) .' [...]'; ?></a>
									   <?php
									    }
										else {
												?>
										<a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $bkdata->post_title; ?></a>

										<?php } ?>
										</h6>
										<!-- <ul class="stats">
											<li ><?php videopress_displayviews( $postID ); ?></li>
											<li><?php //echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
											<li>Author: <font color="red"><?php //echo get_the_author($postID); ?></font></li>											
										</ul>
										-->
                                        <div class="layout-2-details book-expand-detail-$postID" style="display:none;">
                                            <?php echo apply_filters( 'dyn_display_review', $output, $postID, "post" ); ?>
                                            <div class="video_excerpt_text one">
                                                <h4 class="tittle-h4">Description</h4>
                                                <div class="inline-blockright"><?php 
                                                    if($bkdata->post_content ==""){echo "No description available.";}else{
                                                        echo $bkdata->post_content;
                                                    } 
                                                ?></div>
                                            </div>                    
                                            <ul class="stats">
                                                <li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
                                                <li><?php videopress_countviews($postID); ?></li>
                                                <li><i class="fa fa-wrench"></i> <?php echo $endorsnum; ?> Endorsments</li>
                                                <li><i class="fa fa-heart"></i><?php echo $favsnum; ?> Favorites</li>
                                                <li><i class="fa fa-star-o"></i> <?php echo $revnum; ?></li>
                                                <li><?php comments_number(); ?></li>
                                            </ul>                  
                                        </div>
										<ul class="bottom-detailsul" style="vertical-align:baseline;">
										<li><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>"><i class="fa fa-trash-o"></i> Delete</a></li>
								
										<li><a class="detailsblock-btn" data-modal-id="<?php echo $postID; ?>" data-author="<?php echo $book_author; ?>" data-time="<?php echo human_time_diff(get_the_time('U', $postID), current_time('timestamp') ) . ' ago'; ?>" data-views="<?php videopress_displayviews( $postID ); ?>" data-title="<?php echo $bkdata->post_title; ?>" data-permalink="<?php echo get_permalink($postID); ?>" data-content="<?php echo strip_tags($bkdata->post_content); ?>" data-image="<?php echo $imgsrc; ?>" data-comments="<?php echo $bkdata->comment_count; ?>" data-ref="<?php echo $refr ?>" data-endorse="<?php echo $endorsnum?>" data-favr="<?php echo $favsnum; ?> " data-revs="<?php echo $revnum ?>">
										<i class="fa fa-info-circle" aria-hidden="true"></i> Details
										</a>
										</li>
									   <li><a class="changesettingsblock-btn" data-modal-id="myModalPrivacy-<?php the_ID(); ?>"><i class="fa fa-info-circle" aria-hidden="true"></i>Privacy</a></li>
							
									   </ul>
									   </div>
									   
								 </div>
							   </div>		
					   <?php
					    $bkri++;
				  }
				  else
				  {
					  if(!isset($privacyOption[0])) {
						  // if( ($bkri % 6) == 0){echo '</div><div class="row">'; }
					    ?>
							<div class="bookitem col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 text-center" style="padding: 5px 5px 20px 5px !important; margin:0px !important; height:330px;">

    			              <div class="thumbnail layout-2-wrapper solid-bg" style="height:100%; width:100% !important;">

								  
							   	   <div class="image-holder">
								   <a href="<?php echo get_permalink($postID); ?>">
								   <div class="hover-item"></div>
								   </a>            
												
											<?php							
										   if( has_post_thumbnail($postID) ){
												echo get_the_post_thumbnail($postID, 'medium-thumb', array( 'class' => 'layout-3-thumb', 'style'=>"height:140px; max-width:220px;" ) );
											}else{
													echo '<img src="'.get_template_directory_uri().'/images/no-image.png" class="layout-3-thumb" style="height:140px; max-width:220px;">';
											}?>

											
								   </div>
										<div class="layout-title-box text-center" style="display:block;">
										<h6 class="layout-title" style="max-width:100%; word-wrap: break-word; height:40px;">
										<?php
										preg_match_all('/(\w+\s)/', $bkdata->post_title, $matches);

										if ( count($matches[0]) && count($matches[1]) >4){;
										?>
										<a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $matches[1][0] .  $matches[1][1] . $matches[1][2] . '[...]'; ?></a>
										<?php 
											}
										else if(strlen($bkdata->post_title)>30){ 
									   	?>
									    <a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo substr($bkdata->post_title, 0, 30) .' [...]'; ?></a>

									   <?php
									    }
										else {
												?>
										<a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $bkdata->post_title; ?></a>

										<?php } ?>
										</h6>
										<!-- <ul class="stats">
											<li ><?php videopress_displayviews( $postID ); ?></li>
											<li><?php //echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
											<li>Author: <font color="red"><?php //echo get_the_author($postID); ?></font></li>
											
										</ul>
										-->
                                        <div class="layout-2-details book-expand-detail-$postID" style="display:none;">
                                            <?php echo apply_filters( 'dyn_display_review', $output, $postID, "post" ); ?>
                                            <div class="video_excerpt_text one">
                                                <h4 class="tittle-h4">Description</h4>
                                                <div class="inline-blockright"><?php 
                                                    if($bkdata->post_content ==""){echo "No description available.";}else{
                                                        echo $bkdata->post_content;
                                                    } 
                                                ?></div>
                                            </div>                   
                                            <ul class="stats">
                                                <li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
                                                <li><?php videopress_countviews($postID); ?></li>
                                                <li><i class="fa fa-wrench"></i> <?php echo $endorsnum; ?> Endorsments</li>
                                                <li><i class="fa fa-heart"></i><?php echo $favsnum; ?> Favorites</li>
                                                <li><i class="fa fa-star-o"></i> <?php echo $revnum; ?></li>
                                                <li><?php comments_number(); ?></li>
                                            </ul>                  
                                        </div>
										<ul class="bottom-detailsul" style="vertical-align:baseline;">
										<li><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>"><i class="fa fa-trash-o"></i> Delete</a></li>
								
										<li><a class="detailsblock-btn" data-modal-id="<?php echo $postID; ?>" data-author="<?php echo $book_author; ?>" data-time="<?php echo human_time_diff(get_the_time('U', $postID), current_time('timestamp') ) . ' ago'; ?>" data-views="<?php videopress_displayviews( $postID ); ?>" data-title="<?php echo $bkdata->post_title; ?>" data-permalink="<?php echo get_permalink($postID); ?>" data-content="<?php echo strip_tags($bkdata->post_content); ?>" data-image="<?php echo $imgsrc; ?>" data-comments="<?php echo $bkdata->comment_count; ?>" data-ref="<?php echo $refr ?>" data-endorse="<?php echo $endorsnum?>" data-favr="<?php echo $favsnum; ?> " data-revs="<?php echo $revnum ?>">
										<i class="fa fa-info-circle" aria-hidden="true"></i> Details
										</a>
										</li>
									   <li><a class="changesettingsblock-btn" data-modal-id="myModalPrivacy-<?php the_ID(); ?>"><i class="fa fa-info-circle" aria-hidden="true"></i>Privacy</a></li>
							
									   </ul>
									   </div>
									   
								 </div>
							   </div>		
					   <?php
					    $bkri++;
					  } else  {
						    if( is_array($selectUsersList) and count($selectUsersList) > 0 )
							   {   // case where user access list is available
									 if( in_array($user_ID, $selectUsersList) )
									 {
										 //if( ($bkri % 6) == 0){echo '</div><div class="row">'; }
					    ?>
							<div class="bookitem col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 text-center" style="padding: 5px 5px 20px 5px !important; margin:0px !important; height:330px;">
    			              <div class="thumbnail layout-2-wrapper solid-bg" style="height:100%; width:100% !important;">	
							   	   <div class="image-holder">
								   <a href="<?php echo get_permalink($postID); ?>">
								   <div class="hover-item"></div>
								   </a>            
												
											<?php							
										  if( has_post_thumbnail($postID) ){
												echo get_the_post_thumbnail($postID, 'medium-thumb', array( 'class' => 'layout-3-thumb', 'style'=>"height:140px; max-width:220px;" ) );
											}else{
													echo '<img src="'.get_template_directory_uri().'/images/no-image.png" class="layout-3-thumb" style="height:140px; max-width:220px;">';
											}?>

											
								   </div>
										<div class="layout-title-box text-center" style="display:block;">
										<h6 class="layout-title" style="max-width:100%; word-wrap: break-word; height:40px;">
										<?php
										preg_match_all('/(\w+\s)/', $bkdata->post_title, $matches);

										if ( count($matches[0]) && count($matches[1]) >4){;
										?>
										<a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $matches[1][0] .  $matches[1][1] . $matches[1][2] . '[...]'; ?></a>
										<?php 
										}
										else if(strlen($bkdata->post_title)>30){ 
									   	?>
									    <a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo substr($bkdata->post_title, 0, 30) .' [...]'; ?></a>

									   <?php
									    }
										else {
												?>
										<a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $bkdata->post_title; ?></a>

										<?php } ?>
										</h6>
										<!-- <ul class="stats">
											<li ><?php videopress_displayviews( $postID ); ?></li>
											<li><?php //echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
											<li>Author: <font color="red"><?php //echo get_the_author($postID); ?></font></li>
											
										</ul>
										-->
                                        <div class="layout-2-details book-expand-detail-$postID" style="display:none;">
                                            <?php echo apply_filters( 'dyn_display_review', $output, $postID, "post" ); ?>
                                            <div class="video_excerpt_text one">
                                                <h4 class="tittle-h4">Description</h4>
                                                <div class="inline-blockright"><?php 
                                                    if($bkdata->post_content ==""){echo "No description available.";}else{
                                                        echo $bkdata->post_content;
                                                    } 
                                                ?></div>
                                            </div>                     
                                            <ul class="stats">
                                                <li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
                                                <li><?php videopress_countviews($postID); ?></li>
                                                <li><i class="fa fa-wrench"></i> <?php echo $endorsnum; ?> Endorsments</li>
                                                <li><i class="fa fa-heart"></i><?php echo $favsnum; ?> Favorites</li>
                                                <li><i class="fa fa-star-o"></i> <?php echo $revnum; ?></li>
                                                <li><?php comments_number(); ?></li>
                                            </ul>                  
                                        </div>
										<ul class="bottom-detailsul" style="vertical-align:baseline;">
										<li><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>"><i class="fa fa-trash-o"></i> Delete</a></li>
								
										<li><a class="detailsblock-btn" data-modal-id="<?php echo $postID; ?>" data-author="<?php echo $book_author; ?>" data-time="<?php echo human_time_diff(get_the_time('U', $postID), current_time('timestamp') ) . ' ago'; ?>" data-views="<?php videopress_displayviews( $postID ); ?>" data-title="<?php echo $bkdata->post_title; ?>" data-permalink="<?php echo get_permalink($postID); ?>" data-content="<?php echo strip_tags($bkdata->post_content); ?>" data-image="<?php echo $imgsrc; ?>" data-comments="<?php echo $bkdata->comment_count; ?>" data-ref="<?php echo $refr ?>" data-endorse="<?php echo $endorsnum?>" data-favr="<?php echo $favsnum; ?> " data-revs="<?php echo $revnum ?>">
										<i class="fa fa-info-circle" aria-hidden="true"></i> Details
										</a>
										</li>
									   <li><a class="changesettingsblock-btn" data-modal-id="myModalPrivacy-<?php the_ID(); ?>"><i class="fa fa-info-circle" aria-hidden="true"></i>Privacy</a></li>
							
									   </ul>
									   </div>
									   
								 </div>
							   </div>		
					   <?php
					    $bkri++;
									 }
									 else
									 {  // case where user is not in access list
									 	 
									 }
							   }
							   else
							   {  }
					  }	 
				  }
			 }	
		} //end else user not login  
        ?>  
        
        <!-- Modal box detail for books --> 
        <div class="modal-box" id="myModal-<?php echo $postID; ?>">                  
            <div class="modal-body">
                <a class="js-modal-close close">×</a>
                <div class="layout-2-details layout-2-details-<?php echo $postID; ?>">                        
                    <img class="center-block bimg-<?php echo $postID; ?>" width="150"/>    
                    <h6 class="layout-title">  
                    <?php
                        preg_match_all('/(\w+\s)/', $bkdata->post_title, $matches);

                        if ( count($matches[0]) && count($matches[1]) >4){;
                        ?>
                        <a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $matches[1][0] .  $matches[1][1] . $matches[1][2] . '[...]'; ?></a>
                        <?php 
                        }
                        else if(strlen($bkdata->post_title)>30){ 
                        ?>
                        <a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo substr($bkdata->post_title, 0, 30) .' [...]'; ?></a>

                       <?php
                        }
                        else {
                                ?>
                        <a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $bkdata->post_title; ?></a>

                        <?php } ?>              
                    </h6>
                    <?php echo apply_filters( 'dyn_display_review', $output, $postID, "post" ); ?>
                    <div class="video_excerpt_text one"><h4 class="tittle-h4 ">Description</h4>
                        <div class="inline-blockright">
                            <?php if($bkdata->post_content ==""){
                                echo "No description available.";}else{
                                echo $bkdata->post_content;
                            }?>
                        </div>
                    </div>                                          
                    <ul class="stats">
                        <li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
                        <li><?php videopress_countviews($postID); ?></li>
                        <li><i class="fa fa-wrench"></i> <?php echo $endorsnum; ?> Endorsments</li>
                        <li><i class="fa fa-heart"></i><?php echo $favsnum; ?> Favorites</li>
                        <li><i class="fa fa-star-o"></i> <?php echo $revnum; ?></li>
                        <li><?php comments_number(); ?></li>
                    </ul> 
                    <div class="clear"></div>
                </div>
            </div>
        </div>   
        <?php
        }//end for

      }else{
          ?>
          <p class="empty-book">No book available</p>
          <?php  
      }
      //end if      
      ?>
      </div>
      <div class="row" data-anchor="new-books">
        <div class="col-sm-12">
            <nav class="pull-left" id="book-pagination">        
            <?php
            if($cat=='new-books'){
        $pcurrent=max( 1, get_query_var('page') );
        }
        else{
         $pcurrent=1;   
        }
        
        $total = 0;
        $total_round = round($bktotal/12);
        $non_total = $bktotal/12;
        if($total_round < $non_total){
            $total = $non_total + 1;
        }


 $paginate=paginate_links( array(   
    'format' => '?page=%#%',
    'current' => $pcurrent,
    'type' => 'array',
    'end_size' =>1,
    'mid_size'=>4,
    'total' => $total
           ) );

$pcount=count($paginate);


 $maxp=ceil(($bktotal/12)/2);
 
if( $pcurrent<= ceil($bktotal / 12)  )
{ 
 if( ceil($bktotal / 12) <5 ) {
 foreach($paginate as $plnk){echo $plnk.'&nbsp;';}
 }

else if( ceil($bktotal / 12) >= 5 ){


 if ($pcurrent<=6)
 {
    if ($pcurrent>2)
    {echo $paginate[0].'&nbsp;';}
      if($paginate[$pcurrent-2]){
    echo $paginate[$pcurrent-2].'&nbsp;';
       }
    echo $paginate[$pcurrent-1].'&nbsp;';
    echo $paginate[$pcurrent].'&nbsp;';
if( isset($paginate[$pcurrent+1]) && (($pcount-1) > $pcurrent+1) ){echo $paginate[$pcurrent+1].'&nbsp;';}
if( isset($paginate[$pcurrent+2]) && (($pcount-1) > $pcurrent+2) ){echo $paginate[$pcurrent+2].'&nbsp;';}

 }

else if ($pcurrent>=7)
 {   echo $paginate[0].'&nbsp;';
    echo $paginate[5].'&nbsp;';
    echo $paginate[6].'&nbsp;';
    echo $paginate[7].'&nbsp;';
if( isset($paginate[8]) && (($pcount-1) > 8) ){echo $paginate[8].'&nbsp;';}
if( isset($paginate[9]) && (($pcount-1) > 9) ){echo $paginate[9].'&nbsp;';}

 }


if( $pcurrent <= (ceil($bktotal/12)-1) ){
echo $paginate[$pcount-1];
  }

 }

}
                  
            ?>
            </nav>
        </div>
    </div>    
    <script type="text/javascript">
        $("#new_books .page-numbers").each(function(){
            /*FA: replaced this

            var href=$(this).attr("href");
            if(typeof(href) !== "undefined"){
                parts = href.split('/');                
                lastPart = parts.pop() == '' ? parts[parts.length - 1] : parts.pop();
                if(lastPart.length >= 4){                    
                    var pos=href.indexOf("?");       
                    var anchor=$(this).parents(".row").eq(0).data("anchor");                     
                    var url=window.location.href;
                    if(url.indexOf("cat_type") == -1){
                        if(url.indexOf("?") != -1){
                            href += "&cat_type=" + anchor;   
                            var sorturl = url.split("?");
                            if(url.indexOf("sortbook") == -1){
                               href += "&sortbook=" + sorturl[1];
                            }                                                  
                            $(this).attr("href", href);
                        }else{
                            href += "&cat_type=" + anchor;                         
                            $(this).attr("href", href);     
                        }
                    }                    
                }else{
                    href2 = href.substring(0,href.lastIndexOf("/"));
                    $(this).attr("href", href2); 
                }
            }
            */   

         var href=$(this).attr("href");
     if(href){
       var vsorturl='sortbook=';
       var patt = /sortbook/g;
       
       var pos=href.indexOf("?");
       if(pos>0){       
        var vparts=href.split("?")[1].split("&");
          vparts.forEach(function(val,indx){
            if(patt.test(val)) {
                vsorturl=val;               
             }          
          });
        }
       href=href.substring(0,pos);
       //var pgn = "<?php echo get_query_var('page'); ?>";
       var pgn=$("#new_books span.page-numbers.current").text();
      //var anchor=$(this).parents(".row").eq(0).data("anchor"); 
      if($(this).text()=="Next »")        
      $(this).attr("href", href + "?cat_type=new-books&"+vsorturl+"&page=" + eval( Number(pgn) + 1) );
      else if ($(this).text()=="« Previous")
      $(this).attr("href", href + "?cat_type=new-books&"+vsorturl+"&page=" + eval(pgn-1));
       else
      $(this).attr("href", href + "?cat_type=new-books&"+vsorturl+"&page=" + $(this).text());
    }               
 });
       
        
        // show detail popup
        $("a.detailsblock-btn").on("click", function(){
            var id = $(this).attr("data-modal-id");
            if(id){                
                var image = $(this).attr("data-image");                
                $(".bimg-"+id).attr("src",image);
                $("#myModal-"+id).fadeIn(); 
            }
                                
        });
        // list and grid view
        $('#new-book-tab .bookitem').addClass('grid-group-item');   
        $("#grid-book").click(function(event){
            event.preventDefault();
            $('#new-book-tab .bookitem').removeClass('list-group-item changeStyleList').addClass('grid-group-item changeStyleGrid');
            $('.layout-title-box .layout-2-details').fadeOut();
            $('.detailsblock-btn').show();
        });
        $("#list-book").click(function(event){
            event.preventDefault();
            $('#new-book-tab .bookitem').removeClass('grid-group-item changeStyleGrid').addClass('list-group-item changeStyleList');
            $('.layout-title-box .layout-2-details').fadeIn();
            $('.detailsblock-btn').hide();
        });
        //end list/grid view
        // sort book
        $(".sortbook").on("change", function(){
            this.form.submit();
        });
        
    </script>
	<script>
		jQuery(document).ready(function(){
			var $ = jQuery.noConflict();

			$('.del-video').click(function(e){
				e.preventDefault();

				console.log($(this).attr('data-id'));
				var confrm = confirm("Are you sure you want to continue!");
				var book_id = $(this).attr('data-id');
				var user_id = '<?= get_current_user_id;?>';

				if(confrm){
					$.ajax({
						type: "POST",
						url: "<?php echo admin_url('admin-ajax.php'); ?>",
						data: { action:'user_book_delete_newone',book_id: book_id},
						success : function(data){
							//$('#lay-'+playlist_id).remove();
							$('.alert-success').attr('style','display:block');
						}
					});
				}
			})

		})

	</script>
					<script type="text/javascript">
$(function(){

var appendthis =  ("<div class='modal-overlayblockdiv'></div>");

	$('a[data-modal-id]').click(function(e) {
		e.preventDefault();
    $("body").append(appendthis);
    $(".modal-overlay").fadeTo(500, 0.7);
    //$(".js-modalbox").fadeIn(500);
		var modalBox = $(this).attr('data-modal-id');
		$('#'+modalBox).fadeIn($(this).data());
	});


$(".js-modal-close, .modal-overlay").click(function() {
    $(".modal-box, .modal-overlay").fadeOut(500, function() {
        $(".modal-overlay").remove();
    });

});

$(window).resize(function() {
    $(".modal-box").css({
        top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
        left: ($(window).width() - $(".modal-box").outerWidth()) / 2
    });
});

$(window).resize();

});
</script>

<div class="modal-box" id="myModalPrivacy-<?php the_ID(); ?>">
			<div class="modal-body">
				<a class="js-modal-close close privacyCloseI-<?php the_ID(); ?>">×</a>
				<div class="layout-2-details layout-2-details-<?php the_ID(); ?>">

						<div class="form-group">
							<label for="dyn-tags"><br/>Privacy Options:</label>
							<div class="checkbox">
								  <?php
								  //$selectUsersListArray = explode(",", $selectUsersList[0]);
								    $selectUsersListArray = $selectUsersList;
								     if(isset($privacyOption[0]) and $privacyOption[0] != "")
									 {
								       //echo $privacyOption[0]."<br>";
										 ?>
										    <label>Private
											  <?php
											     if($privacyOption[0] == "private")
												 {
											        ?>
													   <input type="radio" id="privacy-radio-<?php the_ID(); ?>" name="privacy-option-<?php the_ID(); ?>" class="dyn-select-file" class="form-control" value="private" checked="checked">
													<?php
												 }
												 else
												 {
													 ?>
													   <input type="radio" id="privacy-radio-<?php the_ID(); ?>" name="privacy-option-<?php the_ID(); ?>" class="dyn-select-file" class="form-control" value="private">
													<?php
												 }
											  ?>
											</label>
											<label>Public
											<input type="radio" id="privacy-radio1-<?php the_ID(); ?>" class="dyn-select-file" class="form-control" value="public" <?php if (isset($privacyOption[0]) && $privacyOption[0]=="public") echo "checked";?> name="privacy-option2-<?php the_ID(); ?>">
											</label>
										 <?php
									 }
									 else
									 {
										 ?>
										    <label>Private
											  <input type="radio" id="privacy-radio-<?php the_ID(); ?>" name="privacy-option-<?php the_ID(); ?>" class="dyn-select-file" class="form-control" value="private">
											</label>
											<label>Public
											 <input type="radio" id="privacy-radio1-<?php the_ID(); ?>" name="privacy-option2-<?php the_ID(); ?>" class="dyn-select-file" class="form-control" value="public">
											</label>
										 <?php
									 }
								  ?>
							</div>
							 <?php
								 if(isset($privacyOption[0]) && $privacyOption[0] != "")
								 {
									   if($privacyOption[0] == "private")
									   {
										    ?>
											<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/jquery.tokenize.js"></script>
												<div class="select-box-<?php the_ID(); ?>">
													<?php
													  echo '<select id="tokenize-'.get_the_ID().'" class="tokenize-sample" name="select_users_list[]" style="width: 350px; height: 150px;" multiple="multiple">';
														  $args1 = array(
															  'role' => 'free_user',
															  'orderby' => 'id',
															  'order' => 'desc'
														   );
														  $subscribers = get_users($args1);
														  foreach ($subscribers as $user) {
															  if(get_current_user_id() == $user->id)
															  { }
															  else
															  {
														          if( in_array($user->id, $selectUsersListArray) )
																  {
																	   echo '<option value="'.$user->id.'" selected="selected">' . $user->display_name.'</option>';
																  }
                                                                  else{
																	   echo '<option value="'.$user->id.'">' . $user->display_name.'</option>';
																  }
															  }
														  }
													  echo '</select>';
													?>
													<script type="text/javascript">
											          $(document).ready(function(){
														  $("#privacy-radio").click(function(){
															 $(".select-box").slideDown();
														  });
														  $("#privacy-radio1").click(function(){
															 $(".select-box").slideUp();
															 $('select#select_users_list option').removeAttr("selected");
														  });

													  });
													  $('#tokenize-<?php echo get_the_ID(); ?>').tokenize();
				                                         datas: "bower.json.php"
												  </script>
												  <style> .tokenize-sample { width: 350px;; }</style>
												</div>
										   <?php
									   }
									   else
									   {
                                            ?>
											<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/jquery.tokenize.js"></script>
												<div style="display:none;" class="select-box-<?php the_ID(); ?>">
													<?php
													  echo '<select id="tokenize-'.get_the_ID().'" class="tokenize-sample" name="select_users_list[]" style="width: 350px; height: 150px;" multiple="multiple">';
														  $args1 = array(
															  'role' => 'free_user',
															  'orderby' => 'id',
															  'order' => 'desc'
														   );
														  $subscribers = get_users($args1);
														  foreach ($subscribers as $user) {
															  if(get_current_user_id() == $user->id)
															  { }
															  else
															  {
																  echo '<option value="'.$user->id.'">' . $user->display_name.'</option>';
															  }
														  }
													  echo '</select>';
													?>
													<script type="text/javascript">
											          $(document).ready(function(){
														  $("#privacy-radio").click(function(){
															 $(".select-box").slideDown();
														  });
														  $("#privacy-radio1").click(function(){
															 $(".select-box").slideUp();
															 $('select#select_users_list option').removeAttr("selected");
														  });

													  });
													  $('#tokenize-<?php echo get_the_ID(); ?>').tokenize();
				                                         datas: "bower.json.php"
												  </script>
												  <style> .tokenize-sample { width: 350px;; }</style>
												</div>
										   <?php
									   }
								 }
								 else
								 {
									   ?>
									   <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/jquery.tokenize.js"></script>
									        <div style="display:none;" class="select-box-<?php the_ID(); ?>">
												<?php
												  echo '<select id="tokenize-'.get_the_ID().'" class="tokenize-sample" name="select_users_list[]" style="width: 350px; height: 150px;" multiple="multiple">';
													  $args1 = array(
														  'role' => 'free_user',
														  'orderby' => 'id',
														  'order' => 'desc'
													   );
													  $subscribers = get_users($args1);
													  foreach ($subscribers as $user) {
														  if(get_current_user_id() == $user->id)
														  { }
														  else
														  {
															  echo '<option value="'.$user->id.'">' . $user->display_name.'</option>';
														  }
													  }
												  echo '</select>';
												?>
												<script type="text/javascript">
											          $(document).ready(function(){
														  $("#privacy-radio").click(function(){
															 $(".select-box").slideDown();
														  });
														  $("#privacy-radio1").click(function(){
															 $(".select-box").slideUp();
															 $('select#select_users_list option').removeAttr("selected");
														  });

													  });
													  $('#tokenize-<?php echo get_the_ID(); ?>').tokenize();
				                                         datas: "bower.json.php"
												  </script>
												  <style> .tokenize-sample { width: 350px;; }</style>
											</div>
									   <?php
								 }
							 ?>
							 <br>
							 <div id="msgLoader-<?php the_ID(); ?>"></div>
                             <input type="button" id="save-Privacy-Option-<?php the_ID(); ?>" name="save-privacy-option-<?php the_ID(); ?>" value="Save Privacy Option" currentUserID="<?php echo get_current_user_id(); ?>" postID="<?php echo get_the_ID(); ?>">
						</div>
					<script>
                      $(document).ready(function(){
						  $("#privacy-radio-<?php the_ID(); ?>").click(function(){
							 $("#privacy-radio1-<?php the_ID(); ?>").prop( "checked", false );
						     $(".select-box-<?php the_ID(); ?>").slideDown();
						  });
						  $("#privacy-radio1-<?php the_ID(); ?>").click(function(){
							 $("#privacy-radio-<?php the_ID(); ?>").prop( "checked", false );
						     $(".select-box-<?php the_ID(); ?>").slideUp();
							 $('select#select_users_list-<?php the_ID(); ?> option').removeAttr("selected");
						  });
						  $(".privacyCloseI-<?php the_ID(); ?>").click(function(){
							 $('#msgLoader-<?php the_ID(); ?>').html('');
						  });
						  $("#save-Privacy-Option-<?php the_ID(); ?>").click(function(){
							   var userID = $(this).attr('currentUserID');
							   var postID = $(this).attr('postID');
							   var select_users_list = $('#tokenize-'+postID).val();
							   if ( typeof(select_users_list) !== "undefined" && select_users_list !== null )
							   { }
						       else
							   {
								   select_users_list = '';
							   }
							   var privacyOptionV = "";
							   if($('#privacy-radio-<?php the_ID(); ?>').is(':checked'))
							   {
								   var privateChecked = "yes";
							   }
							   else
							   {
								   var privateChecked = "no";
							   }
							   if($('#privacy-radio1-<?php the_ID(); ?>').is(':checked'))
							   {
								   var publicChecked = "yes";
							   }
							   else
							   {
								   var publicChecked = "no";
							   }
							   if(privateChecked == "yes")
							   {
								   privacyOptionV = "private";
							   }
							   else if(publicChecked == "yes")
							   {
								   privacyOptionV = "public";
							   }
							   else
							   {
								   privacyOptionV = "";
							   }
                               $.ajax({
									type: 'POST',
									url: "<?php echo site_url(); ?>/update-Privacy.php",
									data: { userID: userID, postID: postID, privacyOptionV : privacyOptionV,
									select_users_list : select_users_list},
									beforeSend: function(){
									  $('#msgLoader-<?php the_ID(); ?>').html('<img src="<?php echo site_url(); ?>/wp-content/themes/videopress/images/loading.gif" />');
									},
									success: function(data){
										 $('#msgLoader-<?php the_ID(); ?>').html(data);
									}
								});
						  });
                      });
					 // $('#tokenize').tokenize();
                    </script>
					<style> .tokenize-sample { width: 350px;; }</style>
					<style>
                      #privacy-radio1-<?php the_ID(); ?>, #privacy-radio-<?php the_ID(); ?> {
                        padding: 5px;
                        text-align: center;
                      }

                     #select-box-<?php the_ID(); ?> {
                       padding: 50px;
                       display: none;
                     }
                    </style>
				</div>
			</div>
</div>
	 
    <?php
    }//end function
    
  
?>