<?php

class CustomLeaderboard
{
	private $wpdb;
	
	public function __construct()
	{
		global $wpdb;
		$this->wpdb = &$wpdb;
		wp_enqueue_script('custom-leader-js', plugins_url('custom-leaderboard.js',__FILE__), '', '', true);
	}
	
	public function getLeaderboardData()
	{
		$users = $this->wpdb->get_results("SELECT * FROM {$this->wpdb->prefix}users");
		$rows = array();
		foreach($users as $user)
		{
			$profile_id = $user->ID;
			$viewCounts = $this->wpdb->get_results('select sum(m.meta_value) as view_count from '.$this->wpdb->prefix.'postmeta m join (SELECT * FROM '.$this->wpdb->prefix.'posts where post_type = "post" AND post_status = "publish" AND post_author = '.$profile_id.' ) p on p.ID = m.post_id where m.meta_key = "popularity_count"');
			$viewCount = isset($viewCounts['0']->view_count) ? $viewCounts['0']->view_count : '0';
		
			$endorsementCounts = $this->wpdb->get_results('SELECT count(*) as counts FROM '.$this->wpdb->prefix.'endorsements endors join wp_posts posts on posts.ID = endors.post_id where posts.post_author = '.$profile_id);
			$endorsementCount = $endorsementCounts['0']->counts ;
		
			$favoriteCounts = $this->wpdb->get_results('SELECT count(*) as counts FROM '.$this->wpdb->prefix.'favorite fav join wp_posts posts on posts.ID = fav.post_id where posts.post_author = '.$profile_id);
			$favoriteCount =  $favoriteCounts['0']->counts;
		
			$subsCounts = $this->wpdb->get_results('SELECT count(author_id) as counts FROM '.$this->wpdb->prefix.'authors_suscribers WHERE author_id = '.$profile_id);
			$subsCount = $subsCounts['0']->counts;
			
			$download = $this->wpdb->get_results('select sum(m.meta_value) as downtotal from  '.$this->wpdb->prefix.'postmeta m join (SELECT * FROM '.$this->wpdb->prefix.'posts where post_type = "dyn_file" AND post_status = "publish" AND post_author = '.$profile_id.' ) p on p.ID = m.post_id where m.meta_key = "dyn_download_count"');
			$downloadCount = isset($download['0']->downtotal) ? $download['0']->downtotal : 0;
		
			$allReviews = $this->wpdb->get_results(
				"(SELECT {$this->wpdb->prefix}dyn_review.* FROM {$this->wpdb->prefix}dyn_review
				LEFT JOIN {$this->wpdb->prefix}user_files ON post_id = id
				WHERE {$this->wpdb->prefix}user_files.user_id = $profile_id
				AND review_status = 'published'
				)
				UNION
				(SELECT {$this->wpdb->prefix}dyn_review.* FROM {$this->wpdb->prefix}dyn_review
				LEFT JOIN {$this->wpdb->posts} ON post_id = {$this->wpdb->posts}.ID
				WHERE post_author = $profile_id
				AND review_status = 'published'
				)
				ORDER BY date desc
				LIMIT 99999999
				OFFSET 0"
			);
			$count = 0;
			$sum = 0;
			foreach($allReviews as $review) {
				$sum += $review->total_rating;
				$count++;
			}
			if($count == 0)
				$rating = 0;
			else
				$rating = ceil($sum/$count);
			$profile_meta = 'profile_pic_meta';
			$profile_img = get_user_meta( $user->ID, $profile_meta, true );
			if($profile_img){
				$img2show = site_url() . '/profile_pic/' . $profile_img;
				$profile_link = "<img src={$img2show} height='55' width='55' / >";
			}
			$avatar = get_avatar($profile_id);
			$avatar = str_replace('&', '%26', $avatar);
			$avatarObj = DOMDocument::loadHTML($avatar);

			$imgObj = $avatarObj->getElementsByTagName('img')->item(0);
			$imgObj->setAttribute('height', 55);
			$imgObj->setAttribute('width', 55);
			$avatar = $imgObj->ownerDocument->saveHTML($imgObj);
			$name = $user->display_name;
			$nicename = $user->user_nicename;
			$currentUser = array();
			$currentUser['views'] = $viewCount;
			$currentUser['endorsements'] = $endorsementCount;
			$currentUser['favorites'] = $favoriteCount;
			$currentUser['reviews'] = $rating;
			$currentUser['revCount'] = $count;
			$currentUser['subscribers'] = $subsCount;
			$currentUser['avatar'] = $avatar;
			$currentUser['name'] = $name;
			$currentUser['nicename'] = $nicename;
			$currentUser['downloads'] = $downloadCount;
			$currentUser['profile_pic'] = $profile_link;
			$rows[] = $currentUser;
		}
		ob_start();
		include __DIR__.'/../templates/leaderboard-table.php';
		$result = ob_get_clean();
		return $result;
	}
}