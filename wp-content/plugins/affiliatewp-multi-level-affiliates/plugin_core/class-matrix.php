<?php

class AffiliateWP_MLA_Matrix extends AffiliateWP_MLA_Common {

	public function __construct( $args ) {
		
		parent::__construct();
		
		$this->args = $args;
		$this->matrix_amount = $args['amount'];
		$this->matrix_affiliate_id = $args['affiliate_id'];
		$this->matrix_products = maybe_unserialize( $args['products'] );
	
	}
	
	// Prepare the matrix data. Complete. Tested
	public function prepare_matrix_data() {
		
		$matrix_data = array();
		
		$affiliate = new AffiliateWP_MLA_Affiliate( $this->matrix_affiliate_id );
		
		$matrix_aff_settings = $affiliate->get_affiliate_matrix_settings();
		
		if( empty($matrix_aff_settings) ) {
			
			return $matrix_data;
			
		} else {
		
			$matrix_level_vars = $affiliate->get_matrix_level_vars();
			$matrix_total_levels = $matrix_level_vars['total_levels'];
			
			$matrix_level_affiliates = $affiliate->get_all_level_affiliates();
			$matrix_order_total = $this->get_order_total();
			
			// Set matrix data
			$matrix_data['matrix_order_total'] = $matrix_order_total;
			if( !empty($this->matrix_products) ) $matrix_data['products'] = $this->matrix_products;
			$matrix_data['matrix_level_vars'] = $matrix_level_vars;
			//$matrix_data['matrix_level_rates'] = $matrix_level_vars['level_rates'];
			$matrix_data['matrix_level_affiliates'] = $matrix_level_affiliates;
			
			// If type is default, get the default Now in settings function
			//if( $matrix_level_vars['type'] == 'default' ) $matrix_level_vars['type'] = affiliate_wp()->settings->get( 'referral_rate_type' );
			
			//$this->store_debug_data( array('level_vars' => $matrix_level_vars, 'order_total' => $matrix_order_total ) );
			//$this->store_debug_data( $matrix_level_vars );
			
			// Loop through the levels
			for( $counter = '1'; $counter <= $matrix_total_levels; $counter++ ) {
				
				$affiliate_id = $matrix_level_affiliates[$counter];
				
				$filter_vars = array(
					'affiliate_id' 					=> $affiliate_id,
					'matrix_order_total'  			=> $matrix_order_total,
					'products' 						=> ( !empty($matrix_data['products']) ) ? $matrix_data['products'] : array(),
					'existing_referral_totals' 		=> ( !empty($matrix_data['referrals']) ) ? $matrix_data['referrals'] : array(),
					'level_variables' 				=> $matrix_data['matrix_level_vars'],
					'level'							=> $counter
				);
				
				$referral_base_amount = apply_filters( 'mla_referral_base_amount', $matrix_order_total, $filter_vars );
				
				// Check if an affiliate exists on the level
				if( !empty($affiliate_id) && affwp_is_active_affiliate($affiliate_id) ) :
					
					if( apply_filters(' mla_award_referral', (bool) TRUE, $filter_vars ) ) :
					
						$referral_amount = AffiliateWP_MLA_Referral::calculate_referral_amount( $matrix_level_vars['type'], $matrix_level_vars['level_rates'][$counter], $referral_base_amount );
					
						if( $referral_amount > 0 ) :
						
							// Set the referral data
							$matrix_data['referrals'][$counter]['affiliate_id'] = $affiliate_id;
							$matrix_data['referrals'][$counter]['referral_total'] = $referral_amount;
						
						endif;
					
					endif;
					
				endif;
				
			}
			
			// Override level 1 when direct referral mode is disabled. Return original referral amount.
			if( $this->matrix_setting( 'direct_referral_mode', $matrix_aff_settings['matrix_setting_id']) != 'mla') {
				$matrix_data['referrals']['1']['referral_total'] = $this->matrix_amount;
			}
			
			$matrix_data['referrals'] = apply_filters( 'mla_referrals', $matrix_data['referrals'], $matrix_data );
			
			return $matrix_data;
		
		}

	}
	
}

?>