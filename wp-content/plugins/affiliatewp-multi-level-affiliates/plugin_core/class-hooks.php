<?php

class AffiliateWP_MLA_Hooks extends AffiliateWP_MLA_Common {

	public function __construct() {
		
		parent::__construct();
		
		// Admin affiliate processes
		//add_action( 'affwp_insert_affiliate', array( $this, 'process_insert_affiliate' ), 1, 1 );
		/*Replaced with the following two*/
		/*Now in the main class with the delete affiliate function*/
		//add_action( 'affwp_register_user', array( $this, 'process_insert_affiliate' ), 1, 1 );
		add_action( 'affwp_auto_register_user', array( $this, 'process_insert_affiliate' ), 1, 1 );
		
		// The 4 step MLA generation process
		add_filter( 'affwp_was_referred', array( $this, 'process_was_referred' ), 11, 2 );
		add_filter( 'affwp_get_referring_affiliate_id', array( $this, 'process_set_referring_affiliate_id' ), 11, 3 );
		// 1. Save the order total
		add_filter( 'affwp_calc_referral_amount', array( $this, 'process_save_order_total' ), 11, 5 );
		// 2. Generate the matrix data and return the direct referral amount (optional)
		add_filter( 'affwp_insert_pending_referral', array( $this, 'process_direct_referral' ), 10, 8 );
		// 3. Process the indirect referrals after the direct referral has been generated
		add_action( 'affwp_insert_referral', array( $this, 'process_indirect_referrals' ) );
		// 4. Complete (mark as unpaid) referrals when the direct referral is completed
		add_action( 'affwp_complete_referral', array( $this, 'process_complete_referrals' ), 10, 3 );
		
		// For rejected referrals etc. To do
		//add_action( 'affwp_post_update_referral', array( $this, 'process_status_changes1' ) ); // $data 'status' => 'rejected'
		add_action( 'affwp_set_referral_status', array( $this, 'process_status_changes' ), 10, 3 ); // $referral_id, $new_status, $old_status
		
		// Add default settings for new affiliates groups. Is also called from MLA settings check
		add_action( 'affiliate_groups_update_settings', array( $this, 'affiliate_groups_update_settings' ) );
		
		// Front end hooks
		if( $this->plugin_setting( 'dashboard_tab_enable' ) == '1' ) :
			add_filter( 'affwp_affiliate_area_tabs', array( $this, 'affiliate_area_tabs_register' ) );
			add_action( 'affwp_affiliate_dashboard_tabs', array( $this, 'affiliate_area_mla_tab' ), 10, 2 );
			add_action( 'affwp_affiliate_dashboard_bottom', array( $this, 'affiliate_area_mla_tab_content' ) );
		endif;
		
		// Admin report tabs
		//add_filter( 'affwp_reports_tabs', array( $this, 'affwp_reports_tabs' ) );
		
		// Tabs content
		//add_action( 'affwp_reports_tab_mla_orders', array( $this, 'affwp_reports_tab_mla_content' ) );
		//add_action( 'affwp_reports_tab_mla_reports', array( $this, 'affwp_reports_tab_mla_content' ) );
		add_action( 'affwp_reports_tab_mla_charts', array( $this, 'affwp_reports_tab_mla_content' ) );
		
		// Reports
		// Generate a report
		add_action( 'wp_loaded', array( $this, 'generate_report' ) );
		
		// Regenerate Referrals Integration
		add_action( 'affwp_rr_after_regenerate', array( $this, 'affwp_rr_after_regenerate' ), 10, 4 );
		// Temporary
		add_shortcode( 'testing_rr', array( $this, 'testing_rr' ) );
		
		// Scripts
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
		
	}
	
	// Add the required scripts and styles
	public function enqueue_scripts() {
		
		if ( affwp_is_admin_page() ) :
			wp_enqueue_script( 'affwp-mla-select2', plugin_dir_url(__FILE__) . '/includes/js/lib/select2/select2.min.js', array( 'jquery' ), '3.5.2' );
			wp_enqueue_script( 'affwp-mla-admin', plugin_dir_url(__FILE__) . '/includes/js/admin.js', array( 'jquery', 'affwp-mla-select2' ), '0.1.0' );
			wp_enqueue_style( 'affwp-mla-select2', plugin_dir_url(__FILE__) . '/includes/js/lib/select2/select2.css', array(), '3.5.2' );
		endif;
	}
	
	// Add the required scripts and styles - Admin
	public function enqueue_scripts_admin() {
		
		// MLA settings page
		/*if( $this->is_mla_settings_page() ) :
			wp_enqueue_script( 'affwp-mla-jquery', '//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js' );
			wp_enqueue_script( 'affwp-mla-settings', plugin_dir_url(__FILE__) . 'includes/js/settings_page.js' );
			
			wp_enqueue_style( 'affwp-mla-css', plugin_dir_url(__FILE__) . 'includes/css/affwp_mla.css' );
		endif;*/
		
		/*if ( affwp_is_admin_page() ) :
			wp_enqueue_script( 'affwp-mla-jquery', '//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js' );
			
			wp_enqueue_script( 'affwp-mla-select2', plugin_dir_url(__FILE__) . 'includes/js/lib/select2/select2.min.js' );
			wp_enqueue_script( 'affwp-mla-admin', plugin_dir_url(__FILE__) . 'includes/js/admin.js' );
			
			wp_enqueue_style( 'affwp-mla-select2-css', plugin_dir_url(__FILE__) . 'includes/js/lib/select2/select2.css' );
		endif;*/	
		
	}
	
	// Processing Actions
	
	// Process after inserting an affiliate. Complete. Tested
	public function process_insert_affiliate($affiliate_id) {
		
		$affiliate = new AffiliateWP_MLA_Affiliate($affiliate_id);
		$affiliate->set_parent_affiliate(); 

	}
	
	// Set was referred to TRUE
	public function process_was_referred( $bool, $tracking_obj) {
		
		return (bool) TRUE;
	}
	
	// Set the parent ID
	public function process_set_referring_affiliate_id( $affiliate_id = '', $reference = '', $context = '' ) {
		
		$user_id = get_current_user_id();
		
		if( !empty($user_id) ) :
		
			$user_affiliate_id = affwp_get_affiliate_id( $user_id );
			if( !empty($user_affiliate_id) ) :
			
				$affiliate = new AffiliateWP_MLA_Affiliate( $user_affiliate_id );
				$parent_id = $affiliate->get_parent_affiliate_id( $user_affiliate_id );
				
				if( !empty($parent_id) ) :
				
					$affiliate_id = $parent_id;
					
				endif;
				
			endif;
		
		endif;
		
		return $affiliate_id;
		
	}

	// Save the order total. Complete. Tested
	public function process_save_order_total( $referral_amount, $affiliate_id = '', $amount, $reference = '', $product_id = '' ) {
		
		if( (isset($this->process_products) && !in_array($product_id, $this->process_products)) || !isset($this->process_products) || empty($product_id) ) :
		
			$this->order_total = (isset($this->order_total)) ? $this->order_total += $amount : $amount;
			
		endif;
		
		if( isset($this->process_products) && !empty($product_id) ) {
			
			array_push($this->process_products, $product_id);
			
		}elseif( !isset($this->process_products) && !empty($product_id) ) {
			
			$this->process_products = array( $product_id );
			
		}
		
		set_transient( 'mla_referral_order_total', $this->order_total );
		
		// Return the original referral amount
		return $referral_amount;
		
	}
	
	// Filter the direct referral amount and save the matrix data in the custom column. Complete. Tested
	public function process_direct_referral( $args, $amount, $reference, $description, $affiliate_id, $visit_id, $data, $context) {
		
		// Get the matrix data
		$matrix = new AffiliateWP_MLA_Matrix( $args );
		
		$affiliate_matrix_data = $matrix->prepare_matrix_data();
		
		if( !empty($affiliate_matrix_data) ) {
			
			$matrix_data = array( 'mla' => $affiliate_matrix_data );
		
			// custom data already exists
			if ( $args['custom'] ) {
				
				$args['custom'] = maybe_unserialize( $args['custom'] );
				$args['custom'] = array_merge( $matrix_data, $args['custom'] );
				
			} else {
				
				$args['custom'] = $matrix_data;
				
			}
	
			
			$args['custom'] = maybe_serialize( $args['custom'] );
			
			$level_1_referral_total = $matrix_data['mla']['referrals']['1']['referral_total'];
			//if( !empty($level_1_referral_total) ) $args['amount'] = $level_1_referral_total;
			$args['amount'] = $level_1_referral_total;
		
		}

		return $args;
		
	}
	
	// Process indirect referrals. Complete. Tested
	public function process_indirect_referrals($referral_id) {
		
		$referral = new AffiliateWP_MLA_Referral($referral_id);
		$referral->process_indirect_referrals();
		
		// Remove the order total transient as no longer required
		delete_transient( 'mla_referral_order_total' );
		
	}
	
	// Process - Complete referrals (mark as unpaid). Complete. Tested
	public function process_complete_referrals( $referral_id, $referral, $reference) {
		
		$referral = new AffiliateWP_MLA_Referral($referral_id);
		$referral->process_complete_referrals();
		
	}


	// Referral Status change. Requires AffiliateWP 1.8. Complete. Tested
	public function process_status_changes($referral_id, $new_status, $old_status ) {
		
		$referral = new AffiliateWP_MLA_Referral($referral_id);
		$referral->process_rejected_referral( $new_status, $old_status );
		
	}
	
	
	// Process after updating an affiliate. Not required currently
	public function process_update_affiliate($data) {}
	
	// Add default Matrix settings for new affiliate group. Is also called from MLA settings check. Complete. Tested
	public function affiliate_groups_update_settings($active_groups) {
		
		$settings = new AffiliateWP_Multi_Level_Affiliates_Settings();
		$settings->set_affiliate_group_settings($active_groups);
		
	}
	
	//////////// Admin Tabs //////////
	
	// Add the tab
	public function affwp_reports_tabs( $tabs ) {
		
		$tabs['mla_charts'] = __( 'MLA Charts', 'affiliate-wp' );
				
		return $tabs;
				
	}
	
	// Add the content
	function affwp_reports_tab_mla_content() {
		
		$template_loader = new AffiliateWP_MLA_Template_Loader( array( 'sub_directory' => 'admin' ) );
		//$template_loader->set_template_data( $data );
		$tab = (!empty($_GET['tab']) ) ? $_GET['tab'] : '';
		if($tab == 'mla_charts' ) {
			echo $template_loader->get_template_object( 'admin-charts' );
		}
		
	}
	
	////////// Front-end ////////////
	
	// Register the dashboard tab
	public function affiliate_area_tabs_register( $tabs ) {
		
		return array_merge( $tabs, array( 'mla-tab' ) );
		
	}
	
	// Add the dashboard tab link
	public function affiliate_area_mla_tab( $affiliate_id, $active_tab ) {
		
		$tab_headings = array(
			'affiliate_network'		=>	'Affiliate Network'
		);
		
		$mla_tab_headings = apply_filters( 'mla_tab_headings', $tab_headings );
		
		?>

    	<li class="affwp-affiliate-dashboard-tab<?php echo $active_tab == 'mla-tab' ? ' active' : ''; ?>">
        <a href="<?php echo esc_url( add_query_arg( 'tab', 'mla-tab' ) ); ?>"><?php echo $mla_tab_headings['affiliate_network'] ;?></a>
    	</li>

    	<?php
		
	}
	
	// Add the dashboard content
	public function affiliate_area_mla_tab_content( $affiliate_id ) {
		
    	if ( isset( $_GET['tab']) && $_GET['tab'] == 'mla-tab' ) {
			$shortcodes = new AffiliateWP_MLA_Shortcodes();
			echo $shortcodes->mla_dashboard();
		}
		
	}
	
	// Generate a report
	public function generate_report() {
		
		if( isset($_REQUEST['mla_report_action']) && !empty($_REQUEST['mla_report_action']) && isset($_REQUEST['report']) && !empty($_REQUEST['report']) ) :
			
			$vars['report'] = $_REQUEST['report'];
			
			/*if( isset($_REQUEST['mla_report_action']) ) :
				$vars['output'] = $_REQUEST['mla_report_action'];
			endif;
			
			if( isset($_REQUEST['mla_stream_type']) ) :
				$vars['stream_type'] = $_REQUEST['mla_stream_type'];
			endif;*/
			
			$reports = new AffiliateWP_MLA_Reports();
			$reports->generate_report( $vars );
		
		endif;
		
	}
	
	// Regenerate Referrals Integration
	public function affwp_rr_after_regenerate( $reference, $context, $referral ='', $amount ='', $order_total='' ) {
	
	$referral = affiliate_wp()->referrals->get_by( 'reference', $reference, $context );
	$referral_id = $referral->referral_id;	
	
	if( $context == 'woocommerce' ) :
		$referral = new AffiliateWP_MLA_Referral($referral->referral_id);
		$referral->regenerate_referrals_woocommerce($reference);
	endif;
	
	}
	// Temporary
	public function testing_rr() {
		$order_id = 4744;
		$this->affwp_rr_after_regenerate( $order_id, 'woocommerce' );
	}
	
	
}  // End of class

?>