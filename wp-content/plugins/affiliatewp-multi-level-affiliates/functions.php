<?php
// Get an affiliates children from specific level. Returns obj results from affiliates_meta table
function get_sub_affiliates_on_level( $affiliate_id, $level ) {
	
	affiliate_management_includes();
	
	$affiliate = new AffiliateWP_MLA_Affiliate($affiliate_id);
	
	if( !empty($level) ) {
		return $affiliate->get_level_affiliates( $affiliate_id, $level );
	}
	
}

// Set affiliate parent
function mla_set_parent_id( $affiliate_id, $parent_id ) {
	$affiliate = new AffiliateWP_MLA_Affiliate($affiliate_id);
	$affiliate->set_parent_affiliate( $affiliate_id, $parent_id );
	
}
?>
