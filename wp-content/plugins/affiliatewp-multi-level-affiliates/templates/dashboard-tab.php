<div id="affwp-tab-content_mla" class="affwp-tab-content"> 
	
    <?php if( $data->display_rates ) : ?>
    
        <div class="affwp-affiliate-dashboard-mla-rates" style=" width:48%;margin-right:4%;float:left">
            <h4><?php _e( 'Your Commission Rates', 'affiliate-wp' ); ?></h4>
            <?php echo do_shortcode('[mla_dashboard_rates]'); ?>
        </div>
    
    <?php endif; ?>
    
    <?php if( $data->display_earnings ) : ?> 
           
        <div class="affwp-affiliate-dashboard-mla-earnings" style=" width:48%;float:left">
            <h4><?php _e( 'Your Earnings', 'affiliate-wp' ); ?></h4>
            <?php echo do_shortcode('[mla_dashboard_earnings]'); ?>
        </div>
    
    <?php endif; ?>
    
    <?php if( $data->display_network_statistics ) : ?> 
       
        <div class="affwp-affiliate-dashboard-mla-statistics">
            <h4 style="clear:both"><?php _e( 'Your Network Statistics', 'affiliate-wp' ); ?></h4>
            <?php echo do_shortcode('[mla_dashboard_statistics]'); ?>
        </div>
    
    <?php endif; ?>
    
    <?php if( $data->display_network_chart ) : ?>  
       
        <div class="affwp-affiliate-dashboard-mla-network">
            <h4 style="clear:both"><?php _e( 'Your Network Chart', 'affiliate-wp' ); ?></h4>
            <?php echo do_shortcode('[mla_dashboard_chart]'); ?>
        </div>
    
    <?php endif; ?>
    
</div>