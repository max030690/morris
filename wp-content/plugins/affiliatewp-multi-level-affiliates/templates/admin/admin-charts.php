<style type="text/css">
#affwp-mla-reports-wrapper {
	padding-bottom: 100px;
}
.affwp-mla-reports {
	background-color: #ffffff;
	margin-top: 16px;
	padding-top: 20px;
	padding-bottom: 20px;
	padding-left: 10px;
	padding-right: 10px;
}

.affwp-mla-reports th {
	text-align: left!important;
}

</style>


<?php 
$template_loader = new AffiliateWP_MLA_Template_Loader( array( 'sub_directory' => 'admin' ) );
//$data = array();
//$template_loader->set_template_data( $data );
	
$chart = ( isset($_POST['chart']) ) ? $_POST['chart'] : 'network';
//$charts_affiliate_id = ( empty( $_POST['affiliate_id'] ) ) ? '' : $_POST['affiliate_id'] ;
?>

<div id="affwp-mla-reports-wrapper"> 
    <?php
	echo $template_loader->get_template_object( 'admin-charts-toolbar' );

	switch ($chart){
	case 'network':

			echo $template_loader->get_template_object( 'admin-charts-network' );
		
		break;
	}
	?>
</div>