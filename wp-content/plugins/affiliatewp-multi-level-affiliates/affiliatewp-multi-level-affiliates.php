<?php
/*
Plugin Name: AffiliateWP - Multi Level Affiliates
Plugin URI: http://clickstudio.com.au
Description: Add multi-level marketing to AffiliateWP
Version: 0.9.9.6
Author: Click Studio
Author URI: http://clickstudio.com.au
License: GPL2
*/

class AffiliateWP_Multi_Level_Affiliates {

	private static $instance = NULL;

	public static function instance() {

		NULL === self::$instance and self::$instance = new self;
		
		self::$instance->plugin_config();
		self::$instance->includes();
		self::$instance->setup_objects_constants();

		return self::$instance;		

	}

	public function __construct() {}
	
	private function plugin_config() {
		$plugin_config = array();
		
		$plugin_config['plugin_file'] = __FILE__;
		$plugin_config['plugin_dir'] = plugin_dir_path( __FILE__ );
		
		// Item name must be identical to the name on Easy Digital Downloads server.
		$plugin_config['plugin_item_name'] = 'AffiliateWP - Multi Level Affiliates';
		$plugin_config['plugin_prefix'] = 'AFFWP_MLA';
		$plugin_config['plugin_version'] = '0.9.9.6';		
		$plugin_config['plugin_updater_url'] = 'http://www.clickstudio.com.au';	
		$plugin_config['plugin_author'] = 'Click Studio';

		$this->plugin_config = $plugin_config;	
		
		update_site_option( $plugin_config['plugin_prefix'].'_version', $plugin_config['plugin_version'] );

	}
	
	// Includes
	private function includes() {
		require_once $this->plugin_config['plugin_dir'] . 'plugin_core/class-settings.php';
		require_once $this->plugin_config['plugin_dir'] . 'plugin_core/class-base.php';	
		if( is_admin() ) {
		require_once $this->plugin_config['plugin_dir'] . 'includes/class-licenses.php';
		require_once $this->plugin_config['plugin_dir'] . 'includes/class-updater.php';
		}
	}
	
	// Set up objects
	private function setup_objects_constants() {
		
		// AFFWP_MLA_PLUGIN_CONFIG
		define( $this->plugin_config['plugin_prefix'].'_PLUGIN_CONFIG', serialize( $this->plugin_config ) );
		
		// AFFWP_MLA_PLUGIN_SETTINGS
		self::$instance->settings = new AffiliateWP_Multi_Level_Affiliates_Settings();
		self::$instance->plugin_settings = self::$instance->settings->plugin_settings;
		define( $this->plugin_config['plugin_prefix'].'_PLUGIN_SETTINGS', serialize( self::$instance->plugin_settings ) );
		
		self::$instance->base = new AffiliateWP_Multi_Level_Affiliates_Base();
		if( is_admin() ) {
		self::$instance->license = new AffiliateWP_Multi_Level_Affiliates_Licenses($this->plugin_config, self::$instance->plugin_settings);
		self::$instance->updater = new AffiliateWP_Multi_Level_Affiliates_Updater($this->plugin_config, self::$instance->license->get_license_option('license_key'));
		}
	}
	
} // End of class


// Dependency check
add_action( 'init', 'AffiliateWP_Multi_Level_Affiliates_Instance');
function AffiliateWP_Multi_Level_Affiliates_Instance() {
	
	$activation_config = array(
		'plugin_name' => 'AffiliateWP - Multi Level Affiliates',
		'plugin_path' => plugin_dir_path( __FILE__ ),
		'plugin_file' => basename( __FILE__ ),
		'plugin_dependencies' => array(
			'Affiliate_WP' => array(
				'name' => 'AffiliateWP',
				'plugin_folder_file' => 'affiliate-wp/affiliate-wp.php',
				'url' => 'https://affiliatewp.com/'
			)
		),
		
	);
	
	require_once 'includes/class-activation.php';
	$activation = new AffiliateWP_Multi_Level_Affiliates_Activation( $activation_config );
	
	// If all dependencies are fine return instance
	if($activation->check_dependencies()) {
		return AffiliateWP_Multi_Level_Affiliates::instance();
	}
	
}

// Activation hook	
register_activation_hook( __FILE__, 'AffiliateWP_Multi_Level_Affiliates_Activate_Plugin' );
function AffiliateWP_Multi_Level_Affiliates_Activate_Plugin() {
	$plugin_version = get_site_option( 'AFFWP_MLA_version', '0.9.5.3' );
}

// Deactivation hook
register_deactivation_hook( __FILE__, 'AffiliateWP_Multi_Level_Affiliates_Deactivate_Plugin' );
function AffiliateWP_Multi_Level_Affiliates_Deactivate_Plugin() {}

////////// Affiliate Management Functions //////////

function affiliate_management_includes() {
	require_once plugin_dir_path( __FILE__ ) . 'plugin_core/class-settings.php';
	require_once plugin_dir_path( __FILE__ ) . 'plugin_core/class-common.php';
	require_once plugin_dir_path( __FILE__ ) . 'plugin_core/class-affiliate.php';
}


// Set the parent ID after a registration
add_action( 'affwp_register_user', 'process_insert_affiliate', 1, 1 );

function process_insert_affiliate( $affiliate_id ) {
	
	affiliate_management_includes();
	
	$affiliate = new AffiliateWP_MLA_Affiliate($affiliate_id);
	$affiliate->set_parent_affiliate();
	
}

// Insert affiliate from admin
add_action( 'affwp_post_insert_affiliate','process_parent_id_new_affiliate_form', 11);

function process_parent_id_new_affiliate_form($add) {

	affiliate_management_includes();
		
		if (current_user_can('edit_users')) :
		
			if(!empty($add)) :
			
				global $_REQUEST;
				
				$user_id = affwp_get_affiliate_user_id($add);
				$affiliate_id = affwp_get_affiliate_id( $user_id );
				
				//$affiliate_id = $add;
				$parent_id = $_REQUEST['parent_affiliate_id'];
				
				if( !empty($affiliate_id) && !empty($parent_id) ) :
					$affiliate = new AffiliateWP_MLA_Affiliate($affiliate_id);
					$affiliate->set_parent_affiliate( '', $parent_id );
				endif;
			
			endif;
		
		endif;
		
}

// Update affiliate from admin
add_action( 'affwp_post_update_affiliate', 'process_parent_id_edit_affiliate_form' );

function process_parent_id_edit_affiliate_form() {

	affiliate_management_includes();
		
		if (current_user_can('edit_users')) :
			
				global $_REQUEST;
				
				$affiliate_id = $_REQUEST['affiliate_id'];
				$parent_id = $_REQUEST['parent_affiliate_id'];
				
				if( !empty($affiliate_id) && !empty($parent_id) ) :
					$affiliate = new AffiliateWP_MLA_Affiliate($affiliate_id);
					$current_parent_id = $affiliate->get_parent_affiliate_id();

					if( $current_parent_id != $parent_id ) :
						$affiliate->restructure_affiliate_parents( '', $parent_id );
					endif;
					
					$affiliate->restructure_affiliate_network_parents( $affiliate_id  );
					
				endif;
		
		endif;
		
}

// Delete affiliate from admin
add_action( 'affwp_affiliate_deleted', 'process_delete_affiliate', 1, 2 );

function process_delete_affiliate( $affiliate_id, $delete_data ) {
		
	affiliate_management_includes();
		
	$affiliate = new AffiliateWP_MLA_Affiliate($affiliate_id);
	$affiliate->delete_affiliate();

	}
	
// Functions and actions required on plugin load
require_once plugin_dir_path( __FILE__ ) . 'functions.php';

?>