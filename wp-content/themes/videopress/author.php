<?php get_header(); ?>

    <!-- Start Content -->
    <div class="container p_90">
    
    <!-- Start Entries -->
    <div class="grid-3-4 centre-block author-container" style='background: #fff;'>
    
    
    <?php
    
	// Handle the URL Parameter to avoid Error Notice
	if( !empty($_GET['activity']) ){
		$page_location = $_GET['activity'];
	}else{
		$page_location = '';
	}
	
	if( $page_location == 'updateprofile' ){
	
		// Show Update Profile
		if( is_user_logged_in() ){
			get_template_part('includes/user/update-account');
		}else{
			echo 'Please login to edit your profile';
		}
		
	}elseif( $page_location == 'uploadvideo' ){
		// Show Upload Form
		if( is_user_logged_in() ){
			get_template_part('includes/user/upload-video');
		}else{
			echo 'Please login to upload a video';
		}
		
	}else{
		// Show Profile
		get_template_part('includes/user/profile');
	}
	
/* if(isset($_GET['sortvideo']) && ($_GET['sortvideo'] == 'endorsements' || $_GET['sortvideo'] == 'favourites')){
echo '</div></div>';
}elseif(isset($_GET['sortvideo']) && $_GET['sortvideo'] == 'recent'){
echo '</div>';
}else{
echo '';
} */
	?>
       </div>     
    
    <!-- End Entries -->
    
    <!-- Widgets -->
    <!-- <?php //get_sidebar(); ?>-->
    <!-- End Widgets -->
    
    <div class="clear"></div>
    </div>
    <div class="spacing-40"></div>
    <!-- End Content -->

    <script>
    $("#videos .page-numbers").each(function(){
    var href=$(this).attr("href");
     if(href){
  	   var vsorturl='sortvideo=recent';
  	   var patt = /sortvideo/g;  	   
       var pos=href.indexOf("?");
       if(pos>0){    	
    	var vparts=href.split("?")[1].split("&");
    	  vparts.forEach(function(val,indx){
    	 	if(patt.test(val)) {
    	 		vsorturl=val;
    	 	 }    	 	
    	  });
    	}
      href=href.substring(0,pos);
     //var pgn = "<?php echo get_query_var('page'); ?>";
    //var anchor=$(this).parents(".row").eq(0).data("anchor"); 
    var pgn=$("#videos span.page-numbers.current").text();

    if($(this).text()=="Next »")    	
      $(this).attr("href", href + "?display_tab=video&"+vsorturl+"&page=" + eval(Number(pgn)+1));
    else if ($(this).text()=="« Previous")
      $(this).attr("href", href + "?display_tab=video&"+vsorturl+"&page=" + eval(Number(pgn)-1));
    else
      $(this).attr("href", href + "?display_tab=video&"+vsorturl+"&page=" + $(this).text());
   }
});


    $("#store .page-numbers").each(function(){
        var href=$(this).attr("href");
        if(href){
            var vsorturl='store=recent';
            var patt = /store/g;
            var pos=href.indexOf("?");
            if(pos>0){
                var vparts=href.split("?")[1].split("&");
                vparts.forEach(function(val,indx){
                    if(patt.test(val)) {
                        vsorturl=val;
                    }
                });
            }
            href=href.substring(0,pos);
            //var pgn = "<?php echo get_query_var('page'); ?>";
            //var anchor=$(this).parents(".row").eq(0).data("anchor");
            var pgn=$("#store span.page-numbers.current").text();

            if($(this).text()=="Next »")
                $(this).attr("href", href + "?display_page=store&"+vsorturl+"&page=" + eval(Number(pgn)+1));
            else if ($(this).text()=="« Previous")
                $(this).attr("href", href + "?display_page=store&"+vsorturl+"&page=" + eval(Number(pgn)-1));
            else
                $(this).attr("href", href + "?display_page=store&"+vsorturl+"&page=" + $(this).text());
        }
    });




 $("#new_files .page-numbers").each(function(){
    var href=$(this).attr("href");
     if(href){
       var vsorturl='file-sort=recent';
       var patt = /file-sort/g;        
       var pos=href.indexOf("?");
       if(pos>0){       
        var vparts=href.split("?")[1].split("&");
          vparts.forEach(function(val,indx){
            if(patt.test(val)) {
                vsorturl=val;
             }          
          });
        }
      href=href.substring(0,pos);
     //var pgn = "<?php echo get_query_var('page'); ?>";
    //var anchor=$(this).parents(".row").eq(0).data("anchor"); 
    var pgn=$("#new_files span.page-numbers.current").text();

    if($(this).text()=="Next »")        
      $(this).attr("href", href + "?display_page=file&"+vsorturl+"&page=" + eval(Number(pgn)+1));
    else if ($(this).text()=="« Previous")
      $(this).attr("href", href + "?display_page=file&"+vsorturl+"&page=" + eval(Number(pgn)-1));
    else
      $(this).attr("href", href + "?display_page=file&"+vsorturl+"&page=" + $(this).text());
   }
});

//FA: pagination for books is handled at: wp-content/plugins/responsive-flipbook/front/manage_fbs.php - function: rfbwp_get_books 

//FA: handle tabls display according to parameters passed with pagination
     var url=window.location.href;        
        var tab = url.split("?");
        var pattB=/new-books/g;
        var pattV=/video/g;
        var pattS=/sortbook/g;
        var pattf=/file/g;
        var patth=/store/g;

        if( pattB.test(url) ){
           var statusExist = "books";
        }
        else if( pattS.test(url) ){
           var statusExist = "books";
        }
        else if( pattV.test(url) ){
          var statusExist = "video";            
        }
        else if( pattf.test(url) ){
          var statusExist = "file";            
        }
        else if( patth.test(url) ){
            var statusExist = "store";
        }
        else{
            statusExist = false;            
        }  
        if(statusExist == "books"){ 
        	$(".nav-tabs li").removeClass("active");
            $(".tab-pane").removeClass("active");
            $("#new_books").addClass("active");
            $(".bookstab").parents("li").eq(0).addClass("active");
            $("#new_books").tab('show');
            $("#home").removeClass("active");
            
        }
        else if(statusExist == "video"){ 
            $(".nav-tabs li").removeClass("active");           
            $(".tab-pane").removeClass("active");
            $("#videos").addClass("active");
            $(".videostab").parents("li").eq(0).addClass("active");
            $("#videos").tab('show');
            $("#home").removeClass("active");

        }
        else if(statusExist == "file"){ 
            $(".nav-tabs li").removeClass("active");           
            $(".tab-pane").removeClass("active");
            $("#new_files").addClass("active");
            $(".filestab").parents("li").eq(0).addClass("active");
            $("#new_files").tab('show');
            $("#home").removeClass("active");

        }
        else if(statusExist == "store"){
            $(".nav-tabs li").removeClass("active");
            $(".tab-pane").removeClass("active");
            $("#store").addClass("active");
            $(".store").parents("li").eq(0).addClass("active");
            $("#store").tab('show');
            $("#home").removeClass("active");

        }
        else{
        	$(".nav-tabs li").removeClass("active");
            $(".tab-pane").removeClass("active");
            $(".hometab").parents("li").eq(0).addClass("active");
            $("#home").addClass("active");
            $("#home").tab('show');
        }


        //end of FA code

    </script>
    
<?php get_footer(); ?>