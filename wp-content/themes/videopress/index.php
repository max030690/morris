<?php get_header(); ?>

    <!-- Start Content -->
    <div class="container p_90">

	<?php global $paged; ?>
    <!-- Start Entries -->
    <div class="grid-3-4 centre-block">

    <!-- Start Layout 2-->
    <div class="layout-2">
    <?php if (strpos($_SERVER['REQUEST_URI'], '/') == FALSE){ ?>
    <h1><?php single_cat_title( '', true ); ?></h1>

	<?php
		$nothing = true;
		$selected = false;
		$current = true;
		$views=false;
		$endorsements=false;
		$favourites=false;
		$comments=false;
		$reviews=false;
		$downloads=false;

		$category = get_category( get_query_var( 'cat' ) );
		$catid = $category->cat_ID;
		if(isset($_GET['sortvideo']) && $_GET['sortvideo']!=''){
			$nothing = false;
			$sortvideo = filter_input(INPUT_GET, 'sortvideo', FILTER_SANITIZE_SPECIAL_CHARS);
			echo '<br>';
			if($sortvideo=='views'):
			$views=true;
			$my_query = new WP_Query( array(
						'posts_per_page' => 10,
						'post_type' => array('post', 'dyn_file','product'),
						'cat'=>$catid,
						'meta_key' => 'popularity_count',
						'orderby'	=> 'meta_value_num',
						'order'		=> 'DESC',
						'meta_query' => array(
							array(
								'key' => 'popularity_count',
							)
						),
						'paged' => $paged,
					));


			elseif($sortvideo=='endorsements'):
				$endorsements=true;

				if($paged){
					$limit = 'LIMIT '.$paged*10 . ',' . $paged*10+10;
				}

				$querystr = "SELECT p.*,endocount FROM wp_posts p join (select * From wp_term_relationships where term_taxonomy_id = ".$catid." ) tr on p.ID=tr.`object_id` left join (select *,count(*) as endocount from wp_endorsements group by post_id ) endors on p.ID = endors.post_id where p.post_type = 'post' OR p.post_type = 'dyn_file' OR p.post_type = 'product' AND p.post_status = 'publish' order by endors.endocount desc $limit";

				$pageposts = $wpdb->get_results($querystr, OBJECT);

			elseif($sortvideo=='favourites'):
				$favourites=true;

				if($paged){
					$limit = 'LIMIT '.$paged*10 . ',' . $paged*10+10;
				}

				$querystr = "SELECT p.*,favcount FROM wp_posts p join (select * From wp_term_relationships where term_taxonomy_id = ".$catid." ) tr on p.ID=tr.`object_id` left join (select *,count(*) as favcount from wp_favorite group by post_id ) favorite on p.ID = favorite.post_id where p.post_type = 'post' OR p.post_type = 'dyn_file' OR p.post_type = 'product' AND p.post_status = 'publish' order by favorite.favcount desc $limit";

				$pageposts = $wpdb->get_results($querystr, OBJECT);

				//$selected = true;
			elseif($sortvideo=='comments'):
				$my_query = new WP_Query( array(
						'posts_per_page' => 10,
						'post_type' => array('post', 'dyn_file','product'),
						'cat'=>$catid,
						'orderby'=> 'comment_count',
						'order'		=> 'DESC',
						'paged' => $paged,
					));
				$comments=true;
			else:
				$selected = true;
			endif;
				$current = false;
		}


	?>

	<form method="get" id="sortvideo-form">
		<p>Currently Sorted1 By :
			<select class="sortvideo" name="sortvideo">
				<option value="recent" <?= ($selected)?'selected':'';?>>Recent</option>
				<option value="views" <?= ($views)?'selected':'';?>>Views</option>
				<option value="endorsements" <?= ($endorsements)?'selected':'';?>>Endorsements</option>
				<option value="favourites" <?= ($favourites)?'selected':'';?>>Favorites</option>
				<option value="reviews" <?= ($reviews)?'selected':'';?>>#Reviews</option>
				<option value="downloads" <?= ($downloads)?'selected':'';?>>#Downloads</option>
			</select>
		</p>
	</form>
	<script>
		jQuery('.sortvideo').change(function(){
			this.form.submit();
		})
	</script>

	<br/>
    <?php } ?>

    <?php

	if($current || $selected || $nothing):

	/* ================================================================== */
	/* Start of loop */
	/* ================================================================== */
	$x = 0;
	if ( have_posts() ) : while ( have_posts() ) : the_post();
	$x++;

    get_template_part( 'content', get_post_type() );

	/* ================================================================== */
	/* End of Loop */
	/* ================================================================== */
	endwhile;
    /* ================================================================== */
    /* Else if Nothing Found */
    /* ================================================================== */
    else :
    ?>

    <h6>NOTHING FOUND!</h6>
    <p>Sorry Nothing found, please try again</p>

    <?php
	/* ================================================================== */
	/* End If */
	/* ================================================================== */
	endif;

	elseif($endorsements || $favourites):
		if($pageposts):
			global $post;

			foreach($pageposts as $post):
				setup_postdata($post);

				get_template_part( 'content', get_post_type() );

			endforeach;
		else:
			echo '<h3>There are no videos in this section</h3>';
		endif;
	else:
		while ( $my_query->have_posts() ) : $my_query->the_post();
			get_template_part( 'content', get_post_type() );
		endwhile;
	endif;
	?>
    </div>
    <!-- End Layout 2 -->

    <!-- Start Pagination -->
    <?php
	//if($current || $selected):
		if( vp_option('vpt_option.pagination') == '1' ){
			videopress_pagination(); // Use Custom Pagination
		}else{
			echo '<div class="post_pagination">';
			posts_nav_link();
			echo'</div>';
		}
	//endif;

	?>
    <!-- End Pagination -->

    </div>
    <!-- End Entries -->

    <!-- Widgets -->
    <!--<?php //get_sidebar(); ?>-->
    <!-- End Widgets -->

    <div class="clear"></div>
    </div>
    <div class="spacing-40"></div>
    <!-- End Content -->

<?php get_footer(); ?>