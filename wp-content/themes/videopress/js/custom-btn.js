	jQuery(document).ready(function($){
	
	$('#endorsement_btn_action').on('click',function(){
		var link=$(this);
		post_id=link.attr("data-post_id");
		userId=link.attr("data-userId");
		author=link.attr("data-author");
		owner=$(this).attr("data-owner");
		type=$(this).attr("data-type");
		ajaxurl=$(this).attr("href");
		link.css("background","#0F9D58"); 
		
	if (userId == 0){
		alert("You must be logged in to endorse a video.");	
	}else{
		
		if (owner== 0){
		jQuery.post(
				ajaxurl, 
				{
					'action': 'endorsement',
					'data': {
						'postID':post_id,
						'userId':userId,
						'type':type,
					}
				}, 
				function(response){
					
					if(response=="endorsed"){
					
						link.text("Endorsed!");
						link.attr("data-type",'remove');
						link.removeClass('endorse_black');
						link.addClass('endorse_green');
					}else if(response=="unendorsed"){
						link.removeClass('endorse_green');
						link.addClass('endorse_black');
						link.attr("data-type",'add');
						link.text("Endorse");
						link.css("background","black"); 
					}
					
				});
			}else{
					alert("You can not endorse yourself.");	
			}
					

				}
	
	return false;
	});
	
	
	$('#favorite_btn_action').on('click',function(){
		var link=$(this);
		post_id=link.attr("data-post_id");
		userId=link.attr("data-userId");
		author=link.attr("data-author");
		owner=$(this).attr("data-owner");
		type=$(this).attr("data-type");
		ajaxurl=$(this).attr("href");
			link.css("background","#0F9D58"); 
	if (userId == 0){
		alert("You must be logged in to favorite a video.");	
	}else{
		
		if (owner== 0){
		jQuery.post(
				ajaxurl, 
				{
					'action': 'favorite',
					'data': {
						'postID':post_id,
						'userId':userId,
						'type':type,
					}
				}, 
				function(response){
					
					if(response=="favorite"){
					
						link.text("Un-Favorite");
						link.attr("data-type",'remove');
						link.removeClass('favorite_black');
						link.addClass('favorite_green');
					}else if(response=="unfavorite"){
						link.removeClass('favorite_green');
						link.addClass('favorite_black');
						link.attr("data-type",'add');
						link.text("Favorite");
							link.css("background","#FF8A8A"); 
					}
					
				});
			}else{
					alert("You can not set favorite yourself.");	
			}
					

				}
	
	return false;
	});
	
	
});



