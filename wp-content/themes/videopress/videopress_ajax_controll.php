<?php
include '../../../wp-load.php';
/*******************Created by Veena*************************/
if(!empty($_POST['user_id'])){
	$user_id = $_POST['user_id'];
	$user_status = get_user_meta($user_id, '_user_status', true);
	if($user_status){
		delete_user_meta($user_id, '_user_status', 'offline' );
		echo "online";
	}else{
		update_user_meta($user_id, '_user_status', 'offline' );
		echo "offline";
	}
}
if(!empty($_POST['group_name'])){
	
	//print_r($_POST);
	$user_ID = get_current_user_id();
	
	$group_name			= $_POST['group_name'];
	$group_member_id	= json_encode($_POST['group_member_id']);

	$record = $wpdb->insert(
		$wpdb->prefix."authors_chat_group_list",
		array(
			'group_name'		=> $group_name ,
			'group_author'		=> $user_ID ,
			'group_member_id'	=> $group_member_id,
			'group_status'		=> 1
		)
	);
	if($record !== false){
		echo 1;
	}else{
		echo 0;
	}
}