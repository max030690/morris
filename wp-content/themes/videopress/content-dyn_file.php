<!-- Start Layout Wrapper -->
<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="layout-2-wrapper<?php if( $x%2 == 0 ){ echo ' solid-bg'; } ?>">

        <div class="grid-6 first" style="width:130px;">

			<div <?php if(!has_post_thumbnail($postID)){?> class="image-holder filesz dyn-file-sprite <?php echo $imgClass?>" style="min-height:140px; max-width:100% !important; "<?php } else{?> class="image-holder" <?php } ?>>
			<a href="<?php echo get_permalink($postID); ?>">
			 <div class="hover-item dyn-file">
				<i class="fa fa-download"></i>
			 </div>
			   </a>
			   <?php if( has_post_thumbnail($postID) ){
							echo get_the_post_thumbnail($postID, 'medium-thumb', array('class' => 'layout-3-thumb', 'style'=>"height:140px; max-width:220px;" ));
				}?>
			</div>

        </div>

        <div class="layout-2-details">
            <h3 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
            <div>File Type: <?php echo apply_filters( 'dyn_file_type_by_mime', get_the_ID() ); ?></div>
            <?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
            <?php if(remove_p(get_the_excerpt()) != ""){ ?>
			<div class="video_excerpt_text one"><h4 class="tittle-h4">Description</h4><div class="inline-blockright"><?php echo remove_p(get_the_excerpt()); ?></div></div>
			<?php }else{ ?>
			<div class="video_excerpt_text one"><h4 class="tittle-h4">Description</h4><div class="inline-blockright">No Description available</div></div>
			<?php } ?>
            <ul class="stats">
                <li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
                <li><?php videopress_countviews( get_the_ID() ); ?></li>
                <li><i class="fa fa-wrench"></i>
                <?php
                    $result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
                    echo count($result);
                ?> Endorsments</li>
                <li><i class="fa  fa-heart"></i>
                <?php
                  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
                  $result1 = $wpdb->get_results($sql_aux);
                  echo count($result1);
                ?> Favorites</li>
                <!--end favorite-->
                <li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
                <li><?php comments_number() ?></li>
                <li><i class="fa fa-download"></i> <?php echo apply_filters( 'dyn_number_of_download', get_the_id()); ?></li>
            </ul>
            <div class="clear"></div>
            <p><?php echo videopress_content('240'); ?></p>
            <?php if($profile_id && $profile_id == get_current_user_id()){ ?>
            <p><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>" ><i class="fa fa-trash-o"></i></a></p>
            <?php } ?>
        </div>

          <?php
                $postID=$post->ID;

                $refr = get_post_meta($postID,'video_options_refr',true);
                if($refr){}
                else{$refr= 'No reference given'; }

                $ftype=apply_filters( 'dyn_file_type_by_mime', $postID );

                $imgClass = apply_filters('dyn_class_by_mime', $postID);

                $download_count=get_post_meta($postID, 'dyn_download_count', true);

                $revnum =apply_filters( 'dyn_number_of_post_review', $postID, "post");
        ?>

        <div class="clear"></div>

        <ul class="bottom-detailsul hidden">
         <li>
            <a class="detailsblock-btn"  data-postID="<?php echo $postID; ?>" data-modal-id="docs" data-author="<?php echo get_the_author_meta( 'display_name', $post->post_author ); ?>" data-time="<?php echo human_time_diff( get_the_time('U', $postID), current_time('timestamp') ) . ' ago'; ?>" data-views="<?php videopress_displayviews( $postID ); ?>" data-title="<?php echo $post->post_title ?>" data-permalink="<?php echo get_permalink($postID); ?>" data-content="<?php echo strip_tags($post->post_content); ?>" data-image="" data-comments="<?php echo $post->comment_count; ?>" data-ftype="<?php echo $ftype; ?>" data-fclass="<?php echo $imgClass; ?>" data-download="<?php echo $download_count; ?>" data-ref="<?php echo $refr ?>" data-endorse="<?php echo count($result); ?>" data-favr="<?php echo count($result1); ?> " data-img-src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) );?>" data-revs="<?php echo $revnum ?>">
            <i class="fa fa-info-circle" aria-hidden="true"></i> Details
           </a>
         </li>
        </ul>
    </div>
</div>
<!-- End Layout Wrapper -->