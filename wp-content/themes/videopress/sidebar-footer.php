<?php 
// If Footer Widget is Enabled
if( vp_option( 'vpt_option.enable_footer_widget' ) == '1'){ 

	// If Widget Column is Style One
	 if( vp_option('vpt_option.widget_columns') == 'style_one' ){
?>

	 <div class="grid-3">
	 <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar(3) ) :  endif; ?>
     <div class="clear"></div>
     </div>
    
     <div class="grid-2-3">
     <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar(4) ) :  endif; ?>
     <div class="clear"></div>
     </div>

<?php }elseif( vp_option('vpt_option.widget_columns') == 'style_two' ){ // Else if Widget Column is Style Two?>
	
     <div class="grid-3">
	 <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar(3) ) :  endif; ?>
     <div class="clear"></div>
     </div>
    
     <div class="grid-6 first-widget">
	 <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar(4) ) :  endif; ?>
     <div class="clear"></div>
     </div>
    
     <div class="grid-6">
	 <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar(5) ) :  endif; ?>
     <div class="clear"></div>
     </div>
    
     <div class="grid-6">
	 <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar(6) ) :  endif; ?>
     <div class="clear"></div>
     </div>
    
     <div class="grid-6 last-widget">
	 <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar(7) ) :  endif; ?>
     <div class="clear"></div>
     </div>
     
<?php }elseif( vp_option('vpt_option.widget_columns') == 'style_three' ){ // Else if Widget Column is Style Three ?>

	 <div class="grid-2">
	 <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar(3) ) :  endif; ?>
     <div class="clear"></div>
     </div>
    
     <div class="grid-4">
     <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar(4) ) :  endif; ?>
     <div class="clear"></div>
     </div>
    
     <div class="grid-4">
     <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar(5) ) :  endif; ?>
     <div class="clear"></div>
     </div>

<?php }elseif( vp_option('vpt_option.widget_columns') == 'one_third' ){ // Else if Widget Column is Style Four ?>

	 <div class="grid-3">
	 <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar(3) ) :  endif; ?>
     <div class="clear"></div>
     </div>
    
     <div class="grid-3">
     <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar(4) ) :  endif; ?>
     <div class="clear"></div>
     </div>
    
     <div class="grid-3">
	 <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar(5) ) :  endif; ?>
     <div class="clear"></div>
     </div>

<?php }else{ // Else if Widget Column is Style Five ?>

	 <div class="grid-4">
	 <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar(3) ) :  endif; ?>
     <div class="clear"></div>
     </div>
    
     <div class="grid-4">
	 <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar(4) ) :  endif; ?>
     <div class="clear"></div>
     </div>
    
     <div class="grid-4">
	 <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar(5) ) :  endif; ?>
     <div class="clear"></div>
     </div>
    
     <div class="grid-4">
	 <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar(6) ) :  endif; ?>
     <div class="clear"></div>
     </div>

<?php
} // End if
} // End if
?>

<div class="clear"></div>