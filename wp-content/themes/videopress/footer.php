	
	</div> <!-- End innermain-container -->
	<!-- Footer -->
    <div class="footer-container <?php if( vp_option( 'vpt_option.enable_footer_widget' ) != '1'){ echo 'nopadding'; } ?>">
    <div class="container p_90">
	
    
    <?php get_sidebar('footer'); ?>
    
    <!-- Footer Copyright -->
    <div class="full-width" id="copyright">
        <div id="copyright-text"><?php echo date('Y') . ' ' . vp_option('vpt_option.copyright'); ?></div> 
        <div class="footermiddle"><a href="<?php echo home_url( '/privacy-policy' ); ?>">Privacy Policy</a>&nbsp;&nbsp;<a href="<?php echo home_url( '/contact-us' ); ?>">Contact Us</a>
		&nbsp;&nbsp;<a href="<?php echo home_url( '/terms-of-service' ); ?>">Terms of Service</a>
		</div>
        <?php if( vp_option('vpt_option.logo_small') != '' ){ ?>
        <div id="footer-logo">
        	<a href="<?php echo home_url( '/' ); ?>">
            <img src="<?php echo vp_option('vpt_option.logo_small'); ?>" alt="small logo" />
            </a>
        </div>
		<?php } ?>
        
    <div class="clear"></div>
    </div>
    <!-- End Footer Copyright -->
    
    <div class="clear"></div>
    </div>
    </div>

<?php if ( is_user_logged_in() ) { ?>
<div class="main_chatter_box"><?php echo do_shortcode('[wise-chat]'); ?></div>
<div class="mainChatBoxArea"><span class="textChatBox" >Chat</span> <span class="close on" title="Open Chat"><i class="fa fa-plus"></i></span><span class="close off" title="Close Chat" style="display:none;" ><i class="fa fa-minus"></i></span></div>
<script>
jQuery(document).ready(function($){
	$('.main_chatter_box').hide();
	$('.wcWindowTitle').html('');
	$('.mainChatBoxArea .close.on').live('click',function(){
		$('.close.on').hide();
		$('.close.off').show();
		$('.textChatBox').hide();
		$('.main_chatter_box').show();
	});
	$('.mainChatBoxArea .close.off').live('click',function(){		
		$('.close.on').show();
		$('.close.off').hide();
		$('.textChatBox').show();
		$('.main_chatter_box').hide();
	});
	
	$('.wcCustomizeButtonGroup').live('click',function(){
		var data = $('.wcCustomizationsPanelGroupData').html();
		$('.wcCustomizationsPanelGroup').html(data);
		$('.wcCustomizationsPanelGroup').slideToggle();
		$('.wcCustomizationsPanel').hide();
		jQuery('#tokenize').tokenize();
	});
	
	$('.wcCustomizeButton').live('click',function(){
		$('.wcCustomizationsPanelGroup').hide();
	});
	
	$(".chat_group_add_request").live('click',function(){

		var group_name = $(".group_name").val();
		var group_member_id = $(".group_member_id").val();
		
		if(typeof group_name === 'undefined' || group_name === null || group_name == ""){
			alert("Group Name is empty.");
			return false;
		}
		if(typeof group_member_id === 'undefined' || group_member_id === null || group_member_id == ""){
			alert("Select Group Members.");
			return false;
		}
		$('.loading-gif-image').css('display','inline-block');
		$.ajax({
			type: "POST",
			url: "<?php echo bloginfo('template_url'); ?>/videopress_ajax_controll.php",
			data: {
				group_name : group_name ,
				group_member_id : group_member_id
			},
			success: function(data){
				//alert(data);
				$('.loading-gif-image').css('display','none');
				if(data == 1){
					$(".authors_chat_group_list_form").trigger("reset");
					$(".wcCustomizeButtonGroup").trigger("click");
				}				
			},
			error: function(xhr, textStatus, error){
				$(".chat_group_add_request").trigger("click");
			}
		});
	});
});
</script>
<div class="wcCustomizationsPanelGroupData" style="display:none;">

	<form method="post" class="authors_chat_group_list_form">
		<h6><b>Create Group</b></h6>
		<div><input type="text" name="group_name" class="group_name" placeholder="Group Name" /></div>
		<div class="select-box">			
			<select id="tokenize" class="tokenize-sample group_member_id" name="group_member_id[]" multiple="multiple">
				<?php
				$args1 = array(
					'role'		=> 'free_user',
					'orderby'	=> 'id',
					'order'		=> 'desc'
				);
				$subscribers = get_users($args1);
				foreach ($subscribers as $user) {
					if(get_current_user_id() != $user->id)
					{
						echo '<option value="'.$user->id.'">' . $user->display_name.'</option>';
					}
				}
				?>
			</select>			
		</div>
		<div><input type="button" name="chat_group_add_request" class="btn btn-success chat_group_add_request" value="Create" />&nbsp;&nbsp;&nbsp;<img class="loading-gif-image" style="display:none;" src="<?php echo bloginfo('template_url'); ?>/images/loading-gif-image.gif" /></div>
	</form>

</div>
<style>
.group_member_id {
    width: 150px !important;
}
.mainChatBoxArea {
	background-color: #dddee0;
    bottom: 0;
    color: #666;
    padding: 4px 6px;
    position: fixed;
	right: 0;
	display: inline-block;
    z-index: 999999;
}
.mainChatBoxArea .textChatBox{
	width: 170px;
	display: inline-block;
}

.wcWindowTitle{
	bottom: 0!important;
    display: inline!important;
    margin: 0!important;
    padding: 0!important;
    width: 0!important;
}
	<?php for($i=0;$i<50;$i++){ ?>
	.wcMessages.wcMessages<?php echo $i; ?>,
	.wcControls.wcControls<?php echo $i; ?>{
		display:none!important;
	}
	<?php } ?>
</style>

<?php } ?>
    <!-- End Footer -->
	<!--</div> End fullmain-container -->
	<!-- Modal -->
<div class="modal fade" id="model_flag2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Write reason for flag</h4>
			</div>
			<div class="modal-body">
				<form method="post" id="addflag-form">
					<input type="hidden" name="action" value="user_mark_flag">
					<input type="hidden" name="url" id="furl" >
					<input type="hidden" name="url" id="ftitle" >
					<input type="hidden" name="url" id="aurl" >
					<textarea rows="10" id="reason" name="reason" style="width:100%"></textarea>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" id="addflag-submit2" class="btn btn-primary">Submit changes</button>
			</div>
		</div>
	</div>
</div>


<?php wp_footer(); ?>
<script>
	function slid(){ 
		jQuery('.homepage-video-holder').slick('unslick');
			jQuery('.homepage-video-holder').slick({
            dots: false,
            infinite: false,
            speed: 300,
            slidesToShow: 5,
            slidesToScroll: 1,
            arrows: true,
			lazyLoad: 'progressive',
            responsive: [
                {
                    breakpoint: 1112,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 800,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 650,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });
		}
</script>

<?php
if( is_user_logged_in() ){

$profile_id = get_current_user_id();
$data = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."user_playlists WHERE user_id = $profile_id  AND type =1 ORDER BY id DESC");
$data_file = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."user_playlists WHERE user_id = $profile_id AND type =2 ORDER BY id DESC");
$data_book = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."user_playlists WHERE user_id = $profile_id AND type =3 ORDER BY id DESC");

if($data){
?>

<div class="modal fade" id="playlistModaladd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" 
               data-dismiss="modal" aria-hidden="false">
                  &times;
            </button>
            <h4 class="modal-title" id="myModalLabel">Add video to playlist</h4>
         </div>
         <div class="modal-body">
			<!--<form method="post" id="form-add-playlist">
				<label>SELECT Playlist to add Video</label>
				<select name="plval">
				<?php foreach($data as $d): ?>
					<option value="<?= $d->id; ?>"><?= $d->title;?></option>
				<?php endforeach; ?>
				</select>
				<input type="hidden" value='' name="songid" id="songid">
				<input type="hidden" name="action" value="save_my_playlist" />
				<input type="submit" name="submit" value="submit" id="aplst"/>
			</form>-->
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" 
               data-dismiss="modal">Close
            </button>
         </div>
      </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<?php } } ?>

<div class="modal fade" id="videostest_mod" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Add video to playlist</h4>
			</div>
			<div class="modal-body">
				<form method="post" id="form-add-playlist">
				<label>SELECT Playlist to add Video</label>
				<select name="plval">
				<?php foreach($data as $d): ?>
					<option value="<?= $d->id; ?>"><?= $d->title;?></option>
				<?php endforeach; ?>
				</select>
				<input type="hidden" value='' name="songid" id="songid">
				<input type="hidden" name="action" value="save_my_playlist" />
				<input type="submit" name="submit" value="submit" id="aplst"/>
			</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
				 
			</div>
		</div>
	</div>
</div>
<?php $_ajax = admin_url('admin-ajax.php'); ?>
 <script>
	jQuery('#form-add-playlist').submit(function(e){
		e.preventDefault();
		jQuery.ajax({
			type : "post",
			dataType : "html",
			url : '<?php echo $_ajax;?>',
			data : jQuery('#form-add-playlist').serialize(),
			success:function (data){	
				jQuery('#videostest_mod .modal-body').html(data);
				//setTimeout(function(){ location.reload(); },1000)
			}	
		});
	});
	
	jQuery('#videostest').click(function(e){
		jQuery('#videostest_mod').modal();
	});
</script> 


<!-- file model -->
<div class="modal fade" id="filetest_mod" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Add Files to playlist</h4>
			</div>
			<div class="modal-body">
				<form method="post" id="form-add-playlist-file">
				<label>SELECT Playlist to add Files</label>
				<select name="plval">
				<?php foreach($data_file as $datafile): ?>
					<option value="<?= $datafile->id; ?>"><?= $datafile->title;?></option>
				<?php endforeach; ?>
				</select>
				<input type="hidden" value='' name="songid" id="songids">
				<input type="hidden" name="action" value="save_my_playlist" />
				<input type="submit" name="submit" value="submit" id="aplst"/>
			</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
				 
			</div>
		</div>
	</div>
</div>
<?php $_ajax = admin_url('admin-ajax.php'); ?>
 <script>
	jQuery('#form-add-playlist-file').submit(function(e){
		e.preventDefault();
		jQuery.ajax({
			type : "post",
			dataType : "html",
			url : '<?php echo $_ajax;?>',
			data : jQuery('#form-add-playlist-file').serialize(),
			success:function (data){	
				jQuery('#filetest_mod .modal-body').html(data);
				//setTimeout(function(){ location.reload(); },1000)
			}	
		});
	});
	
	jQuery('#fileplaytest').click(function(e){
		jQuery('#filetest_mod').modal();
	});
</script> 


<!-- End file model -->
<!-- book model -->
<div class="modal fade" id="booktest_mod" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Add Books to playlist</h4>
			</div>
			<div class="modal-body">
				<form method="post" id="form-add-playlist-book">
				<label>SELECT Playlist to add Books</label>
				<select name="plval">
				<?php foreach($data_book as $databook): ?>
					<option value="<?= $databook->id; ?>"><?= $databook->title;?></option>
				<?php endforeach; ?>
				</select>
				<input type="hidden" value='' name="songid" id="songidss">
				<input type="hidden" name="action" value="save_my_playlist" />
				<input type="submit" name="submit" value="submit" id="aplst"/>
			</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
				 
			</div>
		</div>
	</div>
</div>

<?php $_ajax = admin_url('admin-ajax.php'); ?>
 <script>
	jQuery('#form-add-playlist-book').submit(function(e){
		e.preventDefault();
		jQuery.ajax({
			type : "post",
			dataType : "html",
			url : '<?php echo $_ajax;?>',
			data : jQuery('#form-add-playlist-book').serialize(),
			success:function (data){	
				jQuery('#booktest_mod .modal-body').html(data);
				//setTimeout(function(){ location.reload(); },1000)
			}	
		});
	});
	
	jQuery('#bookplaytest').click(function(e){
		jQuery('#booktest_mod').modal();
	});
	
	jQuery(document).ready(function($){
            jQuery('.wcUsersList').attr("style","height:300px");  
    });
		
</script> 
<!--<img src='<?php echo home_url(); ?>/DYNMockUp04.jpg' />-->
<script src="//cdn.ckeditor.com/4.5.5/basic/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/slick.min.js"></script>
</body>
</html>