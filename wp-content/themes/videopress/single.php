<?php get_header(); ?>
<style>
	.flag_post
	{
		font-size: 14px !important;
	}
	#endorse_wrapper
	{
		font-size: 14px !important;
	}
	#favorite_wrapper
	{
		font-size: 14px !important;
	}
	.spaceabove {
		margin-top: 4px !important;
	}
</style>
<?php if(!is_user_logged_in()){ ?>
	<style>
		.scroll-left_3{
			display:none !important;
		}

		.scroll-right_3{
			display:none !important;
		}
	</style>
	<?php

}

$arg = get_current_user_id();
$path = "http://ec2-54-200-193-165.us-west-2.compute.amazonaws.com/app/video_prediction?user_id=".$arg;
$response = file_get_contents($path);


global $wpdb;
$postdataval = false;
if(isset($_POST['postdataval']) && $_POST['postdataval']!=''){
	if($_POST['postdataname'] == 'desc'){
		$postdataval = $_POST['postdataval'];
		$my_post = array(
			'ID'           => $_POST['postid'],
			'post_content' => $postdataval,
		);
		// Update the post into the database
		wp_update_post( $my_post );
	}
	elseif($_POST['postdataname'] == 'refe'){
		update_post_meta($_POST['postid'], 'video_options_refr', $_POST['postdataval']);
	}
}

?>
<!-- Start Content -->
<div class="container p_90">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/font-awsome/css/font-awesome.min.css">
	<div class="grid-3-4 centre-block">
		<!-- Start Entries -->
		<div class="grid-3-4-sidebar single-page">
			<!-- Video part-->

			<?php
			if( isset($_REQUEST['done']) and $_REQUEST['done'] == 1 )
			{
				?>
				<div style="color: blue; font-size: 14pt; font-weight: bold;">
					Thanks for donating the amount.
				</div>
				<?php
			}
			else if( isset($_REQUEST['done']) and $_REQUEST['done'] == 0 )
			{
				?>
				<div style="color: red; font-size: 14pt; font-weight: bold;">
					There is some error in server..Please contact with site administrator
				</div>
				<?php
			}
			else
			{ }
			/* ================================================================== */
			/* Start of Loop */
			/* ================================================================== */
			while (have_posts()) : the_post();

			// this next bit sets a cookie for related videos
			$first_category = get_the_category();
			?>
			<script>
				jQuery(document).ready(function(){
					jQuery.ajax({
						url: "<?php echo get_home_url() ?>/diynation_related_videos_cookie.php",
						type: "POST",
						data: {'category': '<?php echo $first_category[0]->term_id; ?>'}
					}).done(function(data) {
						//console.log("Current category id: " + data);
					});

				});
			</script>
			<?php // end set cookie for related videos ?>


			<?php if(get_the_author_id() == get_current_user_id()){ ?>
				<h6 class="video-title"><?php the_title(); ?>&nbsp;&nbsp;<a href="#" id="title-edit"><i class="fa fa-pencil-square-o"></i>Edit</a></h6>
			<?php } else {
				?>
				<h6 class="video-title"><?php the_title(); ?></h6>
				<?php
			} ?>
			<div id="title-div" style="display:none;">
				<input type="text" name="title-val" value="<?= the_title();?>">
				<button type="button" class="btn btn-info" id="title-save">Save</button>
				<button type="button" class="btn btn-primary" id="title-cncl">Cancel</button>
				<br>
			</div>


			<!-- Start Video Player -->
			<?php get_template_part('includes/videoplayer'); ?>
			<!-- End Video Player -->



			<!-- Start Content -->
			<div class="entry grey-background">

				<p>By <a href="<?php echo home_url().'/user/'.get_the_author_meta( 'user_login' , get_the_author_meta('ID') ); ?>">
						<?php echo get_the_author_meta( 'display_name' , get_the_author_meta('ID') ); ?>
					</a>
				</p>
				<!-- Start Video Heading -->
				<div class="video-heading">

					<!--<h6 class="video-title"><?php //the_title(); ?></h6>-->
					<div class="flag_post custom_btn"><span id="addflag-anchor">Flag</span></div>

					<?php
					/*

                    $result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID() . ' AND user_id = ' . get_current_user_id());
                    //print_r($result);
                    if (empty($result)){
                    ?>
                    <div id="endorse_wrapper"><div id="endorse_button" class="endorse_video_no custom_btn" style="background-color: #000 !important;"><span>Endorse</span></div></div>
                    <?php }else{ ?>
                    <div id="endorse_wrapper"><div id="endorse_button" class="endorse_video_no custom_btn" style="background-color: #0F9D58 !important;"><span>Endorsed!</span></div></div>
                    <?php }
                        */

					/*
                     * Custom Button by Kbihm
                     */

					$author_id=$post->post_author;
					$postId=get_the_ID();
					echo '<div id="endorse_wrapper">';
					echo endorsement_btn($postId,$author_id);
					echo '</div>';
					//		 echo '<div id="favorite_wrapper">';
					//	 echo favorite_btn($postId,$author_id);
					//	echo '</div>';
					/*------------------end----------------------------*/



					?>

					<!--Section for the favorite button -->
					<?php
					$result = $wpdb->get_results( 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID() . ' AND user_id = ' . get_current_user_id());
					if (empty($result)){
						?>
						<div id="favorite_wrapper"><div id="favorite_button" class="endorse_video_no custom_btn" style="background-color:#ff8a8a"><span>Favorite</span></div></div>
					<?php }else{ ?>
						<div id="favorite_wrapper"><div id="favorite_button" class="endorse_video_no custom_btn" style="background-color:#0F9D58"><span>Un-favorite</span></div></div>
					<?php }
					?>
					<!-- End favorite -->

					<?php
					//Adding a filter for adding Review button
					//This will be added from dyn-review plugin
					$output = "";
					echo apply_filters( 'dyn_review_button', $output, get_the_ID(), "post" );
					?>

					<?php //Start of Section for the Donate Button get_current_user_id()
					$return_url = current_page_url()."?done=1";
					$cancel_url = current_page_url()."?done=0";
					$datapaypal = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."users_merchant_accounts WHERE user_id =".get_the_author_meta('ID')." and status=1");
					$paypalacc='';
					if($datapaypal){
						foreach ($datapaypal as $reg_datagen){
							$paypalacc=$reg_datagen->paypal_email;
						}
					}
					if($paypalacc!==''){ ?>
						&nbsp;&nbsp;
						<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#model_donate">Donate</button>
						<!--Donate Popup-->
						<div class="modal fade" id="model_donate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
										<h4 class="modal-title" id="myModalLabel">Donate This User</h4>
									</div>
									<div class="modal-body">
										<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top" class="form-horizontal" role="form">
											<input type="hidden" name="cmd" value="_xclick">
											<input type="hidden" name="business" value="<?php echo $paypalacc ?>">
											<input type="hidden" name="address_override" value="<?php echo $paypalacc ?>">
											<input type="hidden" name="return" value="<?php echo $return_url; ?>">
											<input type="hidden" name="return_url" value="<?php echo $return_url; ?>">
											<input type="hidden" name="cancel" value="<?php echo $cancel_url; ?>">
											<input type="hidden" name="cancel_url" value="<?php echo $cancel_url; ?>">
											<input type="hidden" name="lc" value="US">
											<input type="hidden" name="item_name" value="Do It Yourself Nation">
											<input type="hidden" name="item_number" value="<?php echo get_the_author_meta('nickname', $author->ID ); ?>">
											<input type="hidden" name="currency_code" value="USD">
											<input type="hidden" name="button_subtype" value="services">
											<input type="hidden" name="bn" value="PP-BuyNowBF:btn_paynowCC_LG.gif:NonHosted">
											<div class="form-group">
												<span for="amount" class="control-label col-sm-4">Amount:</span>
												<div class="col-sm-8">
													<input type="text" class="form-control" name="amount" required >
												</div>
											</div>
											<input type="hidden" name="on0" value="Email" required >
											<div class="form-group">
												<span for="os0" class="control-label col-sm-4">Your Paypal email:</span>
												<div class="col-sm-8">
													<input type="text" class="form-control" name="os0" maxlength="300" size="40" required>
												</div>
											</div>
											<input type="hidden" name="on1" value="Message" required>
											<div class="form-group">
												<span for="os1" class="control-label col-sm-4"	>Message to this author:</span>
												<div class="col-sm-8">
													<input type="text" class="form-control" name="os1" maxlength="300" size="40">
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-offset-4 col-sm-8">
													<button type="submit" class="btn btn-primary donate-button">DONATE WITH PAYPAL</button>
												</div>
											</div>

										</form>
									</div>
								</div>
							</div>
						</div>
					<?php }
					// End Donate Button Section ?>

					<div class="clear"></div>
				</div>
				<!-- End Video Heading -->

				<!-- Start Sharer -->
				<?php get_template_part('includes/sharer'); ?>
				<!-- End Sharer -->
				<?php
				$output = "";
				echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" );
				?>
				<br>
				<div class="panel-group">
					<!-- START About us home page -->
					<div class="panel panel-default">
						<div class="panel-heading">
							<a data-toggle="collapse" href="#paneldes">
								<h4 class="panel-title">Description <span class="pull-right panel-btn">show</span></h4>
							</a>
						</div>
						<div id="paneldes" class="panel-collapse collapse">
							<div class="panel-body">
								<?php if(get_the_author_id() == get_current_user_id()){ ?>
									<span class="pull-right sec-form" id="edit-des"><i class="fa fa-pencil-square-o"></i></span>
								<?php } ?>
								<div id="desf-c">
									<?php
									if($postdataval){
										echo $postdataval;
									}
									else{
										if($post->post_content==""){
											echo '<div class="content-empty">No Description Provided</div>';
										}else{
											the_content();
										}
									}

									?>
								</div>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<a data-toggle="collapse" href="#panelref">
								<h4 class="panel-title">Reference <span class="pull-right panel-btn">show</span></h4>
							</a>
						</div>
						<div id="panelref" class="panel-collapse collapse">
							<div class="panel-body">
								<?php if(get_the_author_id() == get_current_user_id()){ ?>
									<span class="pull-right sec-form" id="edit-ref"><i class="fa fa-pencil-square-o"></i></span>
								<?php } ?>
								<div id="ref-c">
									<?php
									$ref = get_post_meta(get_the_ID(),'video_options_refr',true);
									if($ref){
										echo $ref;
									}else{
										echo 'No referance given';
									}
									?>
								</div>
							</div>
						</div>
					</div>

					<div class="panel panel-default">
						<div class="panel-heading">
							<a data-toggle="collapse" href="#panelshare">
								<h4 class="panel-title">Share <span class="pull-right panel-btn">show</span></h4>
							</a>
						</div>
						<div id="panelshare" class="panel-collapse collapse">
							<div class="panel-body">
							</div>
						</div>
					</div>

				</div>
				<!-- END About us home page -->
			</div>
			<script>
				var $ = jQuery.noConflict();
				$(document).ready(function(){
					var $ = jQuery.noConflict();

					$(".panel-btn").html('<span class="glyphicon glyphicon-collapse-down"></span>');

					$("#panelref,#paneldes").on("hide.bs.collapse", function(){
						$(this).parent().find(".panel-btn").html('<span class="glyphicon glyphicon-collapse-down"></span>');
					});
					$("#panelref,#paneldes").on("show.bs.collapse", function(){
						$(this).parent().find(".panel-btn").html('<span class="glyphicon glyphicon-collapse-up"></span>');
					});
				})
			</script>

			<script>
				var $ = jQuery.noConflict();

				$('.ref-more').click(function(e){
					e.preventDefault();
					$('.content-ref').html('<?= $ref; ?>');
					$(this).hide();
					$(this).next().show();
				});

				$('.ref-less').click(function(e){
					e.preventDefault();
					$('.content-ref').html('<?= $ref_min; ?>');
					$(this).hide();
					$(this).prev().show();
				});

				$('.des-more').click(function(e){
					e.preventDefault();
					$('.content-des').html('<?= $des; ?>');
					$(this).hide();
					$(this).next().show();
				});

				$('.des-less').click(function(e){
					e.preventDefault();
					$('.content-des').html('<?= $des_min; ?>');
					$(this).hide();
					$(this).prev().show();
				});
			</script>

			<div class="clear"></div>
		</div>
		<!-- End Content -->

		<!-- Start Content -->
		<ul class="stats sicon grey-background">
			<li><i class="fa fa-eye"></i> <?php videopress_countviews( get_the_ID() ); ?></li>
			<li><i class="fa  fa-wrench"></i> <?php $result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
				echo count($result); ?> Endorsments</li>
			<!--favourite section -->
			<li><i class="fa  fa-heart"></i>
				<?php
				$sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
				$result1 = $wpdb->get_results($sql_aux);
				echo count($result1);
				?> Favorites</li>
			<!--end favorite -->
			<li><i class="fa fa-calendar"></i> <?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
			<li><i class="fa fa-star-o"></i><?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
		</ul>
		<div class="clear"></div>

		<?php if( vp_option('vpt_option.show_user') == '1' ){ ?>
			<div class="post-by">
				<a href="<?php echo get_author_posts_url( $post->post_author ); ?>"><?php echo get_avatar( $post->post_author, 50 ); ?></a>
				<a href="<?php echo get_author_posts_url( $post->post_author ); ?>" class="post-by-link"><?php the_author(); ?></a>
				<?php echo '<div class="post-by-vid-count">'.count_user_posts( $post->post_author ).' Videos Uploaded</div>'; ?>
				<div class="clear"></div>
			</div>
		<?php } ?>

		<?php
		// Link Pages Navigation
		$args = array( 'before' => '<div class="wp_link_pages">Pages:&nbsp; ', 'after' => '</div><div class="clear"></div>' );
		wp_link_pages( $args );


		// Display the Tags
		$tagval = '';?>
		<?php $all_tags = array();?>
		<?php $all_tags = get_the_tags();?>
		<?php if($all_tags){
			$before1 = '<span><i class="fa fa-tags"></i>Tags</span>';
		}

		if(get_the_author_id() == get_current_user_id()){
			$before1 = '<span><i class="fa fa-tags"></i>Tags';
			$before1 .= '&nbsp;&nbsp;<a href="#" id="tags-edit"><i class="fa fa-pencil-square-o"></i>Edit</a></span>';
		}
		$before = '<span id="tags-val">';

		$after = '</span>';


		?>

		<?php if( is_user_logged_in() ){
			$profile_id = get_current_user_id();
			$data = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."user_playlists WHERE user_id = $profile_id  ORDER BY id DESC");
			if($data){ ?>
				<div class="addto_playlist grey-background padding5_10">
					<a herf="#" id="videostest" ><strong><i class="fa fa-plus"></i> Add To Playlist</strong></a>
				</div>
			<?php } } ?>
		<div class="post-tags grey-background padding5_10">
			<?= $before1;?>
			<?php
			$tagSelected = wp_get_post_tags($post->ID);
			$tagsNameArray = array();
			$tagsSlugArray = array();
			$tagsIDArray = array();
			foreach($tagSelected as $key=>$val)
			{
				$name = $val->name;
				$slug = $val->slug;
				$term_id = $val->term_id;
				$tagsNameArray[] = $name;
				$tagsSlugArray[] = $slug;
				$tagsIDArray[] = $term_id;
			}
			the_tags( $before,', ',$after ); ?>
		</div>


		<?php

		if($all_tags){
			foreach($all_tags as $tag){
				$tagval .= $tag->name.',';
			};
		}

		rtrim($tagval, ",");

		if(get_the_author_id() == get_current_user_id()){
			?>

			<div class="tags-forminput" style="display:none;">
				<input name="tagsinput" class="tagsinput" id="tagsinput" value="<?= $tagval;?>">
				<input name="userid" type="hidden" id="tags_postid" value="<?= get_the_id();?>">
				<button type="button" class="btn btn-success" id="tagsave">Save</button>
				<button type="button" class="btn btn-primary" id="tagcancel">Cancel</button>
			</div>

		<?php }  ?>

		<script>
			jQuery(document).ready(function(){
				jQuery('#songid').val('<?php echo get_the_id();?>')
			});

		</script>
		<!--
    <div class="post-categories">
    <span>
		<i class="fa fa-folder"></i>Categories&nbsp;&nbsp;
		<?php if(get_the_author_id() == get_current_user_id()){ ?>
			<a href="#" id="category-edit"><i class="fa fa-pencil-square-o"></i>Edit</a>
		<?php } ?>
	</span>

	<div id="cat-div">
	    <?php
		$postCats = wp_get_post_categories($post->ID);
		the_category('&nbsp;,&nbsp;');?>
	</div>

	<div id="cat-editdiv" style="display:none;">

	<?php

		$allcate = get_the_category();
		$post_catar = array();
		foreach($allcate as $cate){
			$post_catar[] = $cate->cat_ID;
		}

		//print_r($post_catar);

		$args = array(
			'type'                     => 'post',
			//'child_of'                 => 0,
			//'parent'                   => '',
			'orderby'                  => 'name',
			'order'                    => 'ASC',
			'hide_empty'               => 0,
			'hierarchical'             => 1,
			'exclude'                  => array('1','23'),
			'include'                  => '',
			'number'                   => '',
			'taxonomy'                 => 'category',
			//'pad_counts'               => false

		);

		$categories = get_categories( $args );

		?>


		<ul>
			<?php foreach($categories as $cat ){ ?>
				<li>
					<input type="checkbox" name="cat_c[]" value="<?= $cat->cat_ID; ?>" <?= (in_array($cat->cat_ID, $post_catar))? 'checked':'';?>>
					<?= $cat->name; ?>
				</li>
			<?php } ?>
		</ul>
		<input type="hidden" name="post_id" value="<?php the_ID(); ?>">

		<button type="button" class="btn btn-info" id="cat-save">Save</button>
		<button type="button" class="btn btn-primary" id="cat-cncl">Cancel</button>
	</div>

    </div>
    -->
		<?php comments_template(); ?>
		<div class="clear"></div>
		<!-- End Content -->

		<!-- Start Related Videos -->
		<div class="spacing-40"></div>
		<?php //get_template_part('includes/related-videos'); ?>
		<!-- End Related Videos -->


		<?php
		/* ================================================================== */
		/* End of Loop */
		/* ================================================================== */
		endwhile;
		?>


	</div>
	<!-- End Video Part -->
	<?php
	/*$country_id = oiopub_settings::get_user_country_index();
	if(isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] !== '127.0.0.1' && $country_id !== false){
		$queryyy = " AND (`country` LIKE '%,$country_id,%' OR `country` LIKE '$country_id,%' OR `country` LIKE '$country_id' OR `country` LIKE '%,$country_id' OR country ='all')  ";

	} else {
		$queryyy = " ";
	}

	$tagSelected = wp_get_post_tags($post->ID);
	$tagsNameArray = array();
	$tagsSlugArray = array();
	$tagsIDArray = array();
	$tags_str = '';
	foreach($tagSelected as $tag)
	{
		$tags_str .= "'".$tag->name."',";
	}
	$tags_str = substr($tags_str, 0, -1);
	$bagfhf = ''; $bagfhfa = '';
	$tags_str = substr($tags_str, 0, -1);
	global $wpdb;
	$ptags  = $wpdb->get_results("SELECT *  FROM `wp_oiopub_purchases_preventative_tags` WHERE  `tag`  IN ($tags_str) ");
	if($ptags) {
		$ptagname = '';
		$ptagpurchaseid = '';foreach ($ptags as $ptag) {

			$ptagname = $ptag->tag;
			$ptagpurchaseid = $ptag->purchase_id;
		}
		if($ptags)	{$bagfhfa = "AND id != " .  $ptagpurchaseid ;} else {$bagfhfa = '';


			global $wpdb;
			$tags  = $wpdb->get_results("SELECT *  FROM `wp_oiopub_purchases_tags` WHERE  `tag`  IN ($tags_str) ");
			$tagname = '';
			$tagpurchaseid = '';
			foreach ($tags as $tag) {

				$tagname = $tag->tag;
				$tagpurchaseid = $tag->purchase_id;
			}

			if($tags && $tagpurchaseid !=='' && $tagname !==''){ $bagfhf = "AND id = " .  $tagpurchaseid ; }  else {$bagfhf = ''; }
		}
	}
	$banners  = $wpdb->get_results("SELECT item_id,item_url FROM `wp_oiopub_purchases` WHERE   `item_channel` = 7 and payment_status = 1 and country != '' $queryyy  $bagfhfa $bagfhf   ORDER BY RAND() LIMIT 1");
	$bannerurl = '';
	$bannerid = '';
	$countryinbase = '';
	foreach ($banners as $banner) {

		$bannerurl = $banner->item_url;
		$bannerid = $banner->item_id;
	}*/
	$country_id = oiopub_settings::get_user_country_index();

			if(isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] !== '127.0.0.1' && $country_id !== false){
				$queryyy = " AND (`country` LIKE '%,$country_id,%' OR `country` LIKE '$country_id,%' OR `country` LIKE '$country_id' OR `country` LIKE '%,$country_id' OR country ='all')  ";

			} else {
				$queryyy = " ";
			}
			$tagSelected = wp_get_post_tags($post->ID);
			$tagsNameArray = array();
			$tagsSlugArray = array();
			$tagsIDArray = array();
			$tags_str = '';
			foreach($tagSelected as $tag)
			{
				$tags_str .= "'".$tag->name."',";
			}
			$tags_str = substr($tags_str, 0, -1);
			$bagfhfa = ''; $bagfhf = '';
			global $wpdb;
			$ptags  = $tags_str ? $wpdb->get_results("SELECT *  FROM `wp_oiopub_purchases_preventative_tags` WHERE  `tag`  IN ($tags_str) ") : null;
			if($ptags) {
				$ptagname = '';
				$ptagpurchaseid = '';foreach ($ptags as $ptag) {

					$ptagname = $ptag->tag;
					$ptagpurchaseid = $ptag->purchase_id;
				}
				if($ptags)	{$bagfhfa = "AND id != " .  $ptagpurchaseid ;} else {$bagfhfa = '';


					global $wpdb;
					$tags  = $wpdb->get_results("SELECT *  FROM `wp_oiopub_purchases_tags` WHERE  `tag`  IN ($tags_str) ");
					$tagname = '';
					$tagpurchaseid = '';
					foreach ($tags as $tag) {

						$tagname = $tag->tag;
						$tagpurchaseid = $tag->purchase_id;
					}

					if($tags && $tagpurchaseid !=='' && $tagname !==''){ $bagfhf = "AND id = " .  $tagpurchaseid ; }  else {$bagfhf = ''; }
				}
			}

			$banners  = $wpdb->get_results("SELECT item_id,item_url FROM `wp_oiopub_purchases` WHERE   item_channel= 7 and payment_status = 1 and country !=''  $queryyy $bagfhf $bagfhfa ORDER BY RAND() LIMIT 1");


			$bannerurl = '';
			$bannerid = '';
			$countryinbase = '';
			foreach ($banners as $banner) {

				$bannerurl = $banner->item_url;
				$bannerid = $banner->item_id;
			}
	?>
	<div class="grid-1-4-sidebar sidebar-autoblock grey-background padding5_10">
		<div class="watch-sidebar-section">
			<div class="autoplay-bar"><?php
				if ($banners) {
				?><br><br>
				<div class="adinsidebar" style="border-radius: 10px">
					<?php $user = wp_get_current_user(); if(!(in_array( 'adfree_user', (array) $user->roles ))) {
					?>	  <a href="http://www.doityourselfnation.org/bit_bucket/wp-content/plugins/oiopub-direct/modules/tracker/go.php?id=<?php echo $bannerid; ?>"><img class="oio-click" style="max-width: 100% ; margin-top: 9px;
    margin-bottom: -12px;"src="<?php echo $bannerurl; ?>"/></a>
					<?php } };?> </div> <br><br><br>
				<div class="checkbox-on-off">
					<label for="autoplay-checkbox">Autoplay</label>
					<span class="autoplay-hovercard">
			          	<span class="autoplay-info-icon fa fa-question-circle" data-orientation="vertical" data-position="topright"></span>
						<span class="yt-uix-hovercard-content">
						When autoplay is enabled, a suggested video will automatically play next.
						</span>
					</span>
					<span class="yt-uix-checkbox-on-off">
						<input id="autoplay-checkbox" type="checkbox" checked="checked">
						<label for="autoplay-checkbox" id="autoplay-checkbox-label" class="background_checkbox_change">
							<span class="checked"></span>
							<span class="toggle"></span>
							<span class="unchecked"></span>
						</label>
					</span>
				</div>
				<input type="hidden" class="checked" value=""/>
				<h4 class="watch-sidebar-head">
					Up next
				</h4>
				<?php
				$user_id = get_current_user_id();
				if($user_id == 0){
					$arg = get_current_user_id();
					$path = "http://ec2-54-200-193-165.us-west-2.compute.amazonaws.com/app/video_prediction?user_id=".$arg;
					$response = file_get_contents($path);

					$video_recommendation = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."posts AS p JOIN ".$wpdb->prefix."user_video_recommendation AS video WHERE p.ID = video.post_id AND p.post_type = 'post' GROUP BY p.ID ORDER BY video.interest  DESC LIMIT 20");
				}else{
					$video_recommendation = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."posts AS p JOIN ".$wpdb->prefix."user_video_recommendation AS video WHERE p.ID = video.post_id AND p.post_type = 'post' AND p.post_author = video.user_id ORDER BY video.interest DESC LIMIT 20");
				}

				?>
				<div class="watch-sidebar-body">
					<ul class="video-list">
						<?php
						if(!empty($video_recommendation)){
							foreach ($video_recommendation as $single_video) {
								if($single_video->ID != $postId ){
									?>
									<li class="video video-list-item related-list-item">
										<input type="hidden" class="video-guid" value="<?php echo $single_video->guid; ?>"/>
										<div class="content-wrapper">
											<a href="<?php echo $single_video->guid; ?>" class="video-item-title"><?php echo $single_video->post_title; ?></a>
											<div class="video-item-des">
												<?php
												echo get_the_author_meta( 'display_name' , get_the_author_meta('ID') )
												?>

											</div>
											<a class="stat view-detail" data-id="<?php echo $single_video->ID; ?>">Detail</a>
										</div>
										<div class="thumb-wrapper">
											<a href="<?php echo $single_video->guid; ?>">
					        			<span class="yt-uix-simple-thumb-wrap yt-uix-simple-thumb-related">
					        				<?php
											$video_image = wp_get_attachment_url( get_post_thumbnail_id($single_video->ID));
											if($video_image != null){
												?>
												<img src="<?php echo $video_image; ?>" class="video-image" style="width: 120px;"/>
												<?php
											}else{
												?>
												<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/no-image-thumb.png" class="video-image" style="width: 120px; height: 100%;">
												<?php
											}
											?>
					        			</span>
											</a>
										</div>
									</li>
									<!-- Insert for first <li class="line"></li>-->
									<?php
								}
							}
							$count_videos = count($video_recommendation);
							if($count_videos >= 20){
								?>
								<div class="showmore">
									<a class="show_more" num="20">Show more</a>
								</div>
								<?php
							}
						}else{
							?>
							<h4>There is no available video now</h4>
							<?php
						} ?>
					</ul>
				</div>

			</div>
		</div>


	</div>
</div>
</div>
<!-- End Entries -->


<!-- Widgets -->
<!-- <?php //get_sidebar(); ?>-->
<!-- End Widgets -->

<div class="clear"></div>
</div>
<!--
   <div class="container p_90">
	   	<div class=" grid-3-4 centre-block">
			<div class="grid-3-4-sidebar single-page">
				<?php comments_template(); ?>
			</div>
		</div>
	</div>
	-->
<div class="spacing-40"></div>
<!-- End Content -->
<script>
	var check_id = "<?php echo get_current_user_id(); ?>";

	// endorse video / un-endorse
	jQuery('#endorse_button').on('click',function(){
		if (check_id == 0){
			alert("You must be logged in to endorse a video.");
		}else{

			jQuery.ajax({
				url: "<?php echo get_home_url() ?>/endorse_video.php?post_id=" + <?php echo get_the_ID(); ?> + '&v=' + Date.now(),
				type: "GET",
			}).done(function(data) {
				//console.log(data);
				if (data == "endorsed"){
					jQuery('#endorse_button').html('<span>Endorsed!</span>');
					jQuery('#endorse_button').css('background-color','#0F9D58');
				}
				if (data == "unendorsed"){
					jQuery('#endorse_button').html('<span>Endorse</span>');
					jQuery('#endorse_button').css('background-color','#000');
				}
			});

		}
	});

	// favorite video / non-favorite
	jQuery('#favorite_button').on('click',function(){
		if (check_id == 0){
			alert("You must be logged in to set a video as favorite.");
		}else{
			url = "<?php echo get_home_url() ?>/favorite_video.php?post_id=" + <?php echo get_the_ID(); ?> + '&v=' + Date.now();
			console.log(url);
			jQuery.ajax({
				url: "<?php echo get_home_url() ?>/favorite_video.php?post_id=" + <?php echo get_the_ID(); ?> + '&v=' + Date.now(),
				type: "GET",
			}).done(function(data) {
				if (data == "favorite"){
					jQuery('#favorite_button').html('<span>Un-favorite</span>');
					jQuery('#favorite_button').css('background-color','#0F9D58');
				}
				if (data == "notfavorite"){
					jQuery('#favorite_button').html('<span>Favorite</span>');
					jQuery('#favorite_button').css('background-color','#ff8a8a');
				}
			});
		}
	});

	// flag inappropriate posts
	function flag_post(){
		//if (  localStorage["logged_in"] == 1 ){
		if (  1 == 1 ){
			// flag ajax code goes here
			jQuery.ajax({
				url: "<?php echo get_home_url() ?>/flag_email.php",
				type: "POST",
				data: {flagged_page : window.location.pathname}
			}).done(function(data) {
				console.log(data);
			});
			alert('This post has been flagged for moderation. Thank you.');
		}else{
			alert('Please log in to flag this as inappropriate. Thank you.');
		}
	}

	/* jQuery('.flag_post').on('click',function(){
	 if (check_id == 0){
	 alert("You must be logged in to flag a video.");
	 }else{
	 //flag_post();
	 }
	 });
	 */


</script>

<!-- <script src="//cdn.ckeditor.com/4.5.5/basic/ckeditor.js"></script>-->
<!-- Modal -->
<div class="modal fade" id="modeldesref_section" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">Add Description</h4>
			</div>
			<div class="modal-body">
				<form method="post" id="desref-form">
					<input type="hidden" name="postdataname" id="postdataname">

					<input type="hidden" name="postid" id="postid" value="<?= get_the_ID();?>">
					<div id="">
						<textarea rows="10" id="postdataval" name="postdataval"></textarea>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" id="desref-submit" class="btn btn-primary">Submit changes</button>
			</div>
		</div>
	</div>
</div>
<!-- Lightbox popup -->
<div id="lightbox">
	<?php foreach ($video_recommendation as $single_video) { ?>
		<div id="show_detail_<?php echo $single_video->ID; ?>" class="lightbox" style="display: none;">
			<div class="container">
				<div class="img-represent">
					<?php
					$video_image = wp_get_attachment_url( get_post_thumbnail_id($single_video->ID));
					if($video_image != null){
						?>
						<img src="<?php echo $video_image; ?>" class="video-image" style="width: 120px;"/>
						<?php
					}else{
						?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/no-image-thumb.png" class="video-image" style="width: 120px; height: 100%;">
						<?php
					}
					?>
					<h4><?php echo $single_video->post_title; ?></h4>
				</div>
				<ul class="detail-info">
					<li><label class="detail-title">By:</label> <span><?php echo get_the_author_meta( 'display_name' , get_the_author_meta('ID') ); ?></span></li>
					<li><?php $output = "";echo apply_filters( 'dyn_display_review', $output, $single_video->ID, "post" );?></li>
					<li><label class="detail-title">Description:</label><?php echo wp_trim_words( $single_video->post_content, 15, '[...]' ); ?></li>
					<li><label class="detail-title">Reference:</label> No reference given</li>
				</ul>
				<ul class="stat">
					<li><i class="fa fa-eye"><?php videopress_countviews($single_video->ID); ?></i></li>
					<li><i class="fa fa-wrench"><?php $result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . $single_video->ID);echo count($result); ?> Endorsments</i></li>
					<li><i class="fa fa-heart">
							<?php
							$sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . $single_video->ID;
							$result1 = $wpdb->get_results($sql_aux);
							echo count($result1);
							?> Favorites
						</i></li>
					<li><i class="fa fa-star-o">
							<?php echo apply_filters( 'dyn_number_of_post_review',$single_video->ID, "post"); ?>
						</i></li>
					<li><i class="fa fa-download">
							<?php
							$download = get_post_meta( $single_video->ID, 'dyn_download_count', true);
							if($download == ""){echo '0 Download';}else{echo $download. "Downloads";}
							?>
						</i></li>
				</ul>
				<a class="close-lightbox">Close</a>
			</div>
		</div>
	<?php } ?>
</div>
<!-- End Lightbox popup -->

<script>

	$(document).ready(function(){
		var $ = jQuery.noConflict();

		CKEDITOR.inline( 'postdataval' ,{font_defaultLabel : 'Arial',fontSize_defaultLabel : '44px'});
		CKEDITOR.editorConfig = function( config ) {
			config.font_defaultLabel = 'Arial';
			config.fontSize_defaultLabel = '44px';
		};

		var $ = jQuery.noConflict();
		$('input#tagsinput').tagsinput({
			confirmKeys: [32]
		});

		$('#tagcancel').click(function(e){
			e.preventDefault();

			$('.tags-forminput').hide();
			$('#tags-val').show();
			$('#tagsinput').tagsinput('removeAll');
			$('#tagsinput').tagsinput('add', '<?= $tagval;?>', {preventPost: true});
		})

	})

	var $ = jQuery.noConflict();

	$( ".google-ads" ).css( "opacity", 0.7 );

	$('#edit-des').click(function(e){
		var $ = jQuery.noConflict();

		e.preventDefault();

		$('#myModalLabel').html('Add Description');
		$('#postdataname').val('desc');

		$('.cke_textarea_inline').html($('#desf-c').html());

		$('#modeldesref_section').modal();
	})

	$('#edit-ref').click(function(e){
		e.preventDefault();

		var $ = jQuery.noConflict();

		$('#myModalLabel').html('Add Reference');
		$('#postdataname').val('refe');

		$('.cke_textarea_inline').html($('#ref-c').html());

		$('#modeldesref_section').modal();
	})

	$('#desref-submit').click(function(e){
		var $ = jQuery.noConflict();
		$('#desref-form').submit();
	})

</script>
<script>
	$(document).ready(function(){
		$('.add_dyn_review').addClass('custom_btn');
		$(".sidebar-autoblock .video-list .video-list-item:first-child").after("<li class='line'></li>");
		$(".sidebar-autoblock .video-list .video-list-item:last-child").after("<li class='line'></li>");

		$(".show_more").on("click", function(){
			var num_load = $(this).attr("num");
			var url = window.location.origin +'/bit_bucket/load_video_recommendation.php';
			var html = '';
			var html_popup = '';
			$.ajax({
				url: url,
				type:'get',
				data:{'from':parseInt(num_load)+10},
				success: function (res) {
					results = $.parseJSON(res);
					$.each(results, function (key, data) {
						html += '<li class="video video-list-item related-list-item">';
						html +=  '<input type="hidden" class="video-guid" value="'+data[0].guid+'"/>';
						html += '<div class="content-wrapper">';
						html += '<a href="'+data[0].guid+'" class="video-item-title">'+data[0].post_title+'</a>';
						html += '<div class="video-item-des">'+data.user+'</div><a class="stat view-detail" data-id="'+data[0].ID+'">Detail</a></div>';
						html += '<div class="thumb-wrapper"><a href="'+data[0].guid+'"><span class="yt-uix-simple-thumb-wrap yt-uix-simple-thumb-related">';
						if(data.feature_image != ""){
							html += '<img src="'+data.feature_image+'" class="video-image" style="width: 120px;"/>'
						}else{
							html+='<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/no-image-thumb.png" class="video-image" style="width: 120px; height: 100%;">';
						}
						html += ' </span></a></div></li>';
						// add video to detail pop up
						html_popup += '<div id="show_detail_'+data[0].ID+'" class="lightbox" style="display: none;"><div class="container"><div class="img-represent">';
						if(data.feature_image != ""){
							html_popup += '<img src="'+data.feature_image+'" class="video-image" style="width: 120px;"/>'
						}else{
							html_popup+='<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/no-image-thumb.png" class="video-image" style="width: 120px; height: 100%;">';
						}
						html_popup += '<h4>'+data[0].post_title+'</h4></div>';
						html_popup += '<ul class="detail-info">';
						html_popup += '<li><label class="detail-title">By:</label> <span>'+data.user+'</span></li>';
						html_popup += '<li>'+data.display_review+'</li>';
						if(data.short_description != ""){
							html_popup += '<li><label class="detail-title">Description:</label>'+data.short_description+'</li>';
						}else{
							html_popup += '<li><label class="detail-title">Description:</label>No description given</li>';
						}
						html_popup += '<li><label class="detail-title">Reference:</label> No reference given</li>		</ul><ul class="stat">';
						html_popup += '<li><i class="fa fa-eye">'+data.count_view+'</i></li>';
						html_popup += '<li><i class="fa fa-wrench">'+data.endorsement+' Endorsments</i></li>';
						html_popup += '<li><i class="fa fa-heart">'+data.favorites+' Favorites</i></li>';
						html_popup += '<li><i class="fa fa-star-o">'+data.dyn_number_post_review+'</i></li>';
						html_popup += '<li><i class="fa fa-download">'+data.downloads+'</i></li></ul>';
						html_popup += '<a class="close-lightbox">Close</a></div></div>';
						// hide showmore if there is no videos remaining
						if(data.remain_videos == 1){
							$(".showmore").hide();
						}
					});
					$(".showmore").before(html);
					$("#lightbox").append(html_popup);
					$('.show_more').attr('num',parseInt(num_load)+10);
					$(".view-detail").on('click', function(){
						ID = $(this).attr("data-id");
						$("#show_detail_"+ID).css('top', window.pageYOffset + 'px');
						$("#show_detail_"+ID).show();
						$("#show_detail_"+ID).css("position", "fixed");
						$("#show_detail_"+ID).css("top", "0");
						$("#show_detail_"+ID).css("bottom", "0");
						$("#show_detail_"+ID).css("height", $(document).height() + "px");
						$("#show_detail_"+ID+" .dyn_open_review_box").on('click', function(){
							$("#show_detail_"+ID+" .review_box ").css("display", "block");
						});
						$("#show_detail_"+ID+" .dyn_close_review_box").on("click", function(){
							$("#show_detail_"+ID+" .review_box ").hide();
						});
					});
					$(".close-lightbox").on("click", function(){
						$("#show_detail_"+ID).hide();
					});

				}
			});
		});

		$(".view-detail").on('click', function(){
			ID = $(this).attr("data-id");
			$("#show_detail_"+ID).css('top', window.pageYOffset + 'px');
			$("#show_detail_"+ID).show();
			$("#show_detail_"+ID).css("position", "fixed");
			$("#show_detail_"+ID).css("top", "0");
			$("#show_detail_"+ID).css("bottom", "0");
			$("#show_detail_"+ID).css("height", $(document).height() + "px");
		});
		$(".close-lightbox").on("click", function(){
			$("#show_detail_"+ID).hide();
		});
	});
</script>

<?php $_ajax = admin_url('admin-ajax.php'); ?>
<script>
	jQuery(document).ready(function(){
		var videoid = "<?php echo  $post->ID; ?>";
		var useripaddress = "<?php echo  get_ip_address(); ?>";
		//$ip = $_SERVER['REMOTE_ADDR'];
		var user_id = "<?php echo  get_current_user_id(); ?>";
		jQuery("#addplaylist_buttons").on("click",function(){
			//var bla = $('#item_id').val();
			// var blas = "engine_settingshttp://www.moris.dev/wp-content/plugins/oiopub-direct/modules/tracker/go.php?id=" + bla;
			// alert(blas);
			//alert("Successfully add on playlist");
			$.ajax(
				{
					url : '<?php echo $_ajax;?>',
					type: "POST",

					data: { videoid: videoid, action: 'save_myvideo_playlist', useripadd: useripaddress, user_id: user_id},
					success: function (result) {
						//alert("Successfully add on playlist");
						location.reload();

					}
				});

		});


	});
</script>
<?php get_footer(); ?>
