<?php /* Template Name: Home User Profile */ ?>
<?php get_header();?>

<!-- start homepage header -->
<!--<div class="homepage-header">
  <div class='homepage_main_video'><?php //get_template_part('includes/home-page-videos/home-page-video-1'); ?></div>-->
   <!--
    <div class='homepage_videos_wrapper'>
    <div class='homepage_video'><?php //get_template_part('includes/home-page-videos/home-page-video-2'); ?></div>
    <div class='homepage_video'><?php //get_template_part('includes/home-page-videos/home-page-video-3'); ?></div>
    <div class='homepage_video'><?php //get_template_part('includes/home-page-videos/home-page-video-2'); ?></div>
    <div class='homepage_video'><?php //get_template_part('includes/home-page-videos/home-page-video-3'); ?></div>
  </div>

</div>
-->


<!-- end homepage header -->


<div class="container p_90" ><!-- start 90 precent wrapper -->
 <?php
 /* ================================================================== */
 /* Start of Loop */
 /* ================================================================== */
   //while (have_posts()) : the_post();
 ?>

 <!-- Start Video Player -->
 <!--<?php //get_template_part('includes/videoplayer'); ?> -->
 <!-- End Video Player -->

 <!-- Start Video Heading -->
 <!--<div class="video-heading">-->
 <!-- Start Sharer -->
 <!--<?php //get_template_part('includes/sharer'); ?> -->
 <!-- End Sharer -->
    <!--<h6 class="video-title"><?php //the_title(); ?></h6>
    <div class="clear"></div>
  </div>-->
  <!-- End Video Heading -->

  <!-- Start Content -->
  <!-- <ul class="stats"> -->
  <!-- <li><?php //videopress_countviews( get_the_ID() ); ?></li> -->
  <!-- <li><?php //comments_number() ?></li> -->
  <!-- <li><?php //echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li> -->
  <!-- </ul>-->
  <div class="clear"></div>


  <?php //if (1 == 0){//if( vp_option('vpt_option.show_user') == '1' ){ ?>
<!--<div class="post-by">
    <a href="<?php //echo get_author_posts_url( $post->post_author ); ?>"><?php //echo get_avatar( $post->post_author, 50 ); ?></a>
    <a href="<?php //echo get_author_posts_url( $post->post_author ); ?>" class="post-by-link"><?php the_author(); ?></a>
    <?php //cho '<div class="post-by-vid-count">'.count_user_posts( $post->post_author ).' Videos Uploaded</div>'; ?>
    <div class="clear"></div>
</div>
-->

<?php //} ?>

<!-- Start Content -->
<!-- <div class="entry"
    <?php
    // Display The Content
    //if($post->post_content==""){
     // echo '<div class="content-empty">No Description</div>';
  //}else{
     //the_content();
 //}
 ?>
 <div class="clear"></div>
</div> -->
<!-- End Content -->


<?php
  // Link Pages Navigation
/*
$args = array( 'before' => '<div class="wp_link_pages">Pages:&nbsp; ', 'after' => '</div><div class="clear"></div>' );
wp_link_pages( $args );

  // Display the Tags
$before = '<div class="spacer-20"></div>
<div class="post-tags">
  <span><i class="fa fa-tags"></i>Tags</span>';

  $after = '</div>';
  the_tags( $before,', ',$after );
  */
  ?>

    <!--<div class="post-categories">
    <span><i class="fa fa-folder"></i>Categories</span>-->
    <!--<?php //the_category(', '); ?>-->
    <!--</div>-->


    <div class="clear"></div>
    <!-- End Content -->

    <!-- Start Related Videos -->
    <div class="spacing-40"></div>
    <!--<?php //get_template_part('includes/related-videos'); ?>-->
    <!-- End Related Videos -->


    <?php
    /* ================================================================== */
    /* End of Loop */
    /* ================================================================== */
    //endwhile;
    ?>

    <!-- Start Content -->

    <!-- Start Entries -->
    <div class="grid-3-4 centre-block homepage-layout" style="background-color:rgb(248, 248, 248) !important; padding: 15px 25px !important;">
     <!-- RANDOM CATEGORIES -->
     <div>
       <h6 class="title-bar-home"><i class="fa fa-fire" aria-hidden="true" style="color:#337ab7; font-size:32px"></i><span >Hot Content</span></h6>
       <div class="spacer-dark"></div>

       <!--<?php //get_template_part('includes/random-category-videos'); ?>-->
       <!--<?php //get_template_part('includes/random-category-videos'); ?>-->
       <!--<?php //get_template_part('includes/random-category-videos'); ?>-->
       <!--<?php //get_template_part('includes/random-category-videos'); ?>-->



       <?php get_template_part('includes/hot-content-videos'); ?>
       <?php get_template_part('includes/hot-content-files'); ?>
       <?php get_template_part('includes/hot-content-books'); ?>
         <?php get_template_part('includes/hot-content-products'); ?>

     </div>

     <div>
       <h6 class="title-bar-home" ><span>Recommended Content</span></h6>
       <div class="spacer-dark"></div>

       <?php get_template_part('includes/recommend-videos'); ?>
       <?php get_template_part('includes/recommend-files'); ?>
       <?php get_template_part('includes/recommend-books'); ?>
       <?php get_template_part('includes/recommend-products'); ?>

     </div>
     <div>
       <h6  class="title-bar-home"><span>New Content</span></h6>
       <div class="spacer-dark"></div>


       <?php get_template_part('includes/hot-videos'); ?>
       <?php get_template_part('includes/hot-files'); ?>
       <?php get_template_part('includes/hot-books'); ?>
       <?php get_template_part('includes/hot-product'); ?>

     </div>


     <div class="clear"></div>

     <!-- end random categories section -->




     <!-- RELATED VIDEOS -->
   <!--<div id="related_videos_container">
   <?php //get_template_part('includes/related-videos'); ?>
 </div>-->
 <!-- <hr/>-->

 <!-- POPULAR CHANNELS -->
   <!-- <div id="popular_channels_container">
   <?php //get_template_part('includes/popular-channels'); ?>
 </div>-->
 <!--<hr/>-->



 <!-- Content -->
    <!--<div class="entry">

    <div class="clear"></div>
  </div>-->
  <!-- End Content -->

  <?php get_template_part('layouts/layout-builder'); ?>

</div>
<!-- End Entries -->

<!-- Widgets -->

<!-- End Widgets -->


<div class="clear"></div>

<div class="spacing-40"></div>

<!-- bottom section -->

<!-- End Content -->

<div class="clear"></div>
</div>
<div class="spacing-40"></div>

<!-- end 90 percent wrapper -->

<div class="sign_up_area">
  <div class="sign_up_title">Join the Nation Today</div>
  <div class="sign_up_text"><span class="icon-quote-left"></span> Where Anything is Possible - Just Press Play ! <span class="icon-quote-right"></span></div>

  <div class="fifty-percent alignleft">
   <a href="<?php echo home_url(); ?>/sign-in/#login"><div class="login_button">LOG IN</div></a>
 </div>

 <div class="fifty-percent alignright">
   <a href="<?php echo home_url(); ?>/sign-in/#signup"><div class="sign_up_button">SIGN UP</div></a>
 </div>

</div>

<script language="javascript" type="text/javascript">
/*
    jquery('#categories_container').jscroll({
    loadingHtml: '<img src="<?php //echo get_template_directory_uri();?>/images/loading.gif" alt="Loading" /> Loading...',
    //padding: 20,
    nextSelector: '#nav-below li a',
    contentSelector: '#categories_container.post'
  }); */

</script>
<script type="text/javascript">
  $(function(){

    var appendthis =  ("<div class='modal-overlayblockdiv'></div>");

//custom pagination

$(".page-numbers").each(function(){
  var href=$(this).attr("href");
  if(href){
    var pos=href.indexOf("?");
    if(pos>0){
      href=href.substring(0,pos);
    }
    var anchor=$(this).parents(".row").eq(0).data("anchor");
    $(this).attr("href", href + "?cat_type=" + anchor + "#" + anchor );
  }
});

$(".list-group").css({"background-color":"rgb(248, 248, 248)", "padding" : "padding:15px 0px !important;"});

var my_blue={'background-color': '#3498DB'};
var my_grey={'background-color': '#c9c9c9'};
/* REF:  from http://www.doityourselfnation.org/bit_bucket/wp-content/plugins/msh_custom_css_and_js/js/1scripts.js?ver=4.2.4 */
$('.title-bar-home').mouseover(function(){
  var el=$(this).nextAll('.spacer-dark');

  $(this).nextAll('.spacer-dark').animate({
    width: '+=100%'
  },{duration:400,
   step: function( now, fx ) {
    if(now<20)
     $(fx.elem).css({'background-color' : 'rgb(227,227,227)'});

 },
 progress: function(promise, remaining){
  var percent = (remaining * 100).toFixed(2);
  if(percent>20)
    $(el).css(my_blue);
},
easing:'linear'

});

});
$('.title-bar-home').mouseout(function(){
  var el=$(this).nextAll('.spacer-dark');

  $(this).nextAll('.spacer-dark').animate({
    width: '-=100%'
  },{duration:400,
    step: function( now, fx ) {
      if(now<50)
       $(fx.elem).css({'background-color' : 'rgb(227,227,227)'});

   },
   progress: function(promise, remaining){
    var percent = (remaining * 100).toFixed(2);
    if(percent>50)
      $(el).css(my_grey);
  }
  ,
  easing:'linear'
});

});

$(".title-bar-home span").css({'font-size': "22px", 'padding': "6px", 'font-weight': '700', 'font-family': 'Open Sans', 'vertical-align': 'baseline', 'display' : 'inline-block', 'text-transform': 'uppercase'});

$('a[data-modal-id]').click(function(e) {

  e.preventDefault();
  $("body").append(appendthis);
  $(".modal-overlay").fadeTo(500, 0.7);
    //$(".js-modalbox").fadeIn(500);
    var modalBox = $(this).attr('data-modal-id');

    $('#'+modalBox + " .layout-title").html('<a href="'+ $(this).data('permalink') + '" title="' + $(this).data('title') + '" style="color:rgb(75, 75, 75)!important; font-size:16px !important; font-weight:600 !important">' + $(this).data('title') + '</a>');
    $('#'+modalBox + " .image-holder a").attr('href', $(this).data('permalink'));
    $('#'+modalBox + " img").attr('src', $(this).data('image'));

    if(modalBox=='products') {
        if($(this).data('image')!='http://www.doityourselfnation.org/bit_bucket/wp-includes/images/media/default.png'){
            $('#'+modalBox + " img").attr('src', $(this).data('image'));
        }else{
            $('#'+modalBox + " img").attr('src','http://www.doityourselfnation.org/bit_bucket/wp-content/themes/videopress/images/placeholder.jpg');
        }
        $("#"+modalBox + " .product-regular_price").html($(this).data('regularprice').replace( /<p>/g, '' ).replace( /<\/p>/g, '' ));
        $('#'+modalBox + " .imgall").html($(this).data('allimg').replace( /<p>/g, '' ).replace( /<\/p>/g, '' ));
        $('#'+modalBox + " .product-sold-take").text($(this).data('sold'));
        $("#"+modalBox + " .stock-span-grid").text($(this).data('stock'));
    }else{ }

    var thecontent=$(this).data('content').replace( /<p>/g, '' ).replace( /<\/p>/g, '' );

    $("#"+modalBox + " .inline-blockright:eq(0)").html(thecontent);
    $("#"+modalBox + " .inline-blockright:eq(2)").text($(this).data('author'));
    $("#"+modalBox + " .inline-blockright:eq(1)").html($(this).data('ref').replace( /<p>/g, '' ).replace( /<\/p>/g, '' ));
    $('#'+modalBox + " .stats li:eq(0)").text($(this).data('time'));
    $('#'+modalBox + " .stats li:eq(1)").text($(this).data('views'));
    $('#'+modalBox + " .stats li:eq(2)").text($(this).data('endorse') + ' Endorsments');
    $('#'+modalBox + " .stats li:eq(3)").text($(this).data('favr') + ' Favorites');
    $('#'+modalBox + " .stats li:eq(4)").text($(this).data('revs'));

    var comments=$(this).data("comments");
    if( (comments==0) || (comments=='No Comments') ){
      ncomments="No Comments";
    }
    else{
      ncomments=comments + " Comments";
    }
    $("#"+modalBox + " .stats li:eq(5)").text(ncomments);

    if($(this).data("ftype")){

      $('#'+modalBox + " #img-hld").attr("class", "image-holder filesz dyn-file-sprite " + $(this).data("fclass"));
      $('#'+modalBox + " #ftype").text("File Type: " + $(this).data("ftype"));
      if($(this).data("download")!="")
        $('#'+modalBox + " .stats li:last-child").text( $(this).data("download") + " downloads");
      else
        $('#'+modalBox + " .stats li:last-child").text("0 downloads");
    }

    $('#'+modalBox).fadeIn();

  });


$(".js-modal-close, .modal-overlay").click(function() {
  $(".modal-box, .modal-overlay").fadeOut(500, function() {
    $(".modal-overlay").remove();
  });
});

$(window).resize(function() {
  $(".modal-box").css({
    top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
    left: ($(window).width() - $(".modal-box").outerWidth()) / 2
  });
});

$(window).resize();

});


</script>
<?php ?>
<div class="modal-box" id="videos">
 <div class="modal-body">
  <a class="js-modal-close close">×</a>
  <div class="layout-2-details">
    <div class="image-holder" >
      <a href="">
        <div class="hover-item"></div>
      </a>
      <img src="" class="layout-2-thumb wp-post-image" alt="" style="max-height:100%!important;">
    </div>
    <h6 class="layout-title"></h6>
    <div class="dyn_rating_bar dyn_open_review_box" data-post_id="" data-rating_type="post"><div class="dyn_rating" style="width:0%"></div></div><div class="reviews review_box dyn_all_reviews search.php" style="display:none;">

    <div class="btn btn-danger dyn_close_review_box">Close</div></div><span>(clickable)</span> <div class="video_excerpt_text one">
          <h4 class="tittle-h4">Description</h4><div class="inline-blockright">okay</div>
      </div>
    <div class="video_excerpt_text two">
        <h4 class="tittle-h4">Reference</h4>
        <div class="inline-blockright">reference</div>
    </div>
      <div class="video_excerpt_text two">
          <h4 class="tittle-h4">Author </h4>
          <div class="inline-blockright">reference</div>
      </div>

          <h4 class="tittle-h4" id="price-home-modal"> </h4>

    <ul class="stats">
      <li></li>
      <li></li>
      <li><i class="fa fa-wrench"></i> 0 Endorsments</li>
      <!-- favourite section -->
      <li><i class="fa fa-heart"></i>
        0 Favorites</li>
        <!-- end favorite -->
        <li><i class="fa fa-star-o"></i> 0 Reviews</li>
        <li></li>
      </ul>
      <div class="clear"></div>
      <p></p>
    </div>
  </div>
</div>


    <div class="modal-box" id="products">
 <div class="modal-body">
  <a class="js-modal-close close">×</a>
  <div class="layout-2-details">
    <div class="image-holder" >
      <a href="">
        <div class="hover-item"></div>
      </a>
      <img src="" class="layout-2-thumb wp-post-image" alt="" style="max-height:100%!important;">
    </div>

      <div class="imgall">

      </div>

    <h6 class="layout-title"></h6>
    <div class="dyn_rating_bar dyn_open_review_box" data-post_id="" data-rating_type="post"><div class="dyn_rating" style="width:0%"></div></div><div class="reviews review_box dyn_all_reviews search.php" style="display:none;">

    <div class="btn btn-danger dyn_close_review_box">Close</div></div><span>(clickable)</span>

      <h4 class="tittle-h4">Price:<span class="price-product-modal">
              <span class="product-regular_price">99.99$</span>
          <span class="fgjieryhfe"> <span class="onsale" style="      font-weight: bold;">Sale!</span></span>
      </h4>


      <div class="stock-grid">
          <h4 class="tittle-h4"><i class="fa fa-smile-o" aria-hidden="true"></i><span class="stock-span-grid">  </span><span style="color:black;font-weight: 400;"> in Stock</span></h4>
      </div>

      <div class="video_excerpt_text one">
          <h4 class="tittle-h4">Description</h4>
          <div class="inline-blockright">okay</div>
      </div>
   <!-- <div class="video_excerpt_text two">
        <h4 class="tittle-h4">Reference</h4>
        <div class="inline-blockright">reference</div>
    </div> -->
      <div class="video_excerpt_text two-ref-del"  ><h4 class="tittle-h4"></h4><div class="inline-blockright"></div></div>
      <div class="video_excerpt_text two"><h4 class="tittle-h4">Sold By</h4><div class="inline-blockright">reference</div></div>


      <h4 class="tittle-h4" id="price-home-modal"> </h4>

    <ul class="stats">
      <li></li>
      <li></li>
      <li><i class="fa fa-wrench"></i> 0 Endorsments</li>
      <!-- favourite section -->
      <li><i class="fa fa-heart"></i>
        0 Favorites</li>
        <!-- end favorite -->
        <li><i class="fa fa-star-o"></i> 0 Reviews</li>
        <li></li>
        <li class="product-sold-take"> </li>
      </ul>
      <div class="clear"></div>
      <p></p>
    </div>
  </div>
</div>

<div  class="modal-box" id="docs">
 <div class="modal-body">
   <a class="js-modal-close close">×</a>
   <div class="layout-2-details">
    <div id="img-hld" class="">
      <a href="">
        <div class="hover-item dyn-file">
          <i class="fa fa-download"></i>
        </div>
      </a>
    </div>
    <h6 class="layout-title"><a href="" title="wow"></a></h6>
    <div id="ftype"></div>
    <div class="dyn_rating_bar dyn_open_review_box" data-post_id="" data-rating_type="post"><div class="dyn_rating" style="width:0%"></div></div><div class="reviews review_box dyn_all_reviews template-user-profile.php" style="display:none;">

    <div class="btn btn-danger dyn_close_review_box">Close</div></div><span>(clickable)</span>
    <div class="video_excerpt_text one"><h4 class="tittle-h4">Description 3</h4><div class="inline-blockright">No Description available</div></div>
    <div class="video_excerpt_text two"><h4 class="tittle-h4">Reference</h4><div class="inline-blockright">No referance </div></div>
    <div class="video_excerpt_text two"><h4 class="tittle-h4">Author</h4><div class="inline-blockright"></div></div>
    <ul class="stats">
      <li></li>
      <li></li>
      <li><i class="fa fa-wrench"></i>
        0 Endorsments</li>
        <li><i class="fa  fa-heart"></i>
          0 Favorites</li>
          <!--end favorite-->
          <li><i class="fa fa-star-o"></i> 0 Reviews</li>
          <li>No Comments</li>
          <li><i class="fa fa-download"></i> </li>
        </ul>
        <div class="clear"></div>
        <p></p>
      </div></div></div>

      <?php user_insert_product_hot_recomend(); get_footer(); ?>