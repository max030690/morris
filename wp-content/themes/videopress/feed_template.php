<?php 
/* Template Name: Feed Template */
get_header(); 

?>

    <!-- Start Content -->
    <div class="container">
		
<?php

if(!is_user_logged_in()){

	$btquery = "SELECT ID as post_id, post_type FROM ".$wpdb->prefix."posts WHERE post_type IN ('dyn_book','dyn_file') AND post_status = 'publish' ORDER BY post_date DESC LIMIT 50";
	
}
else if(is_user_logged_in()){
	
	$user_id = get_current_user_id();
	
	$friends_subscribers_qry = 'SELECT a.from_friend_id as id from '.$wpdb->prefix.'authors_friends_list a WHERE a.to_friend_id = '.$user_id.' AND a.status = 2 UNION SELECT b.to_friend_id from '.$wpdb->prefix.'authors_friends_list b WHERE b.from_friend_id = '.$user_id.' AND b.status = 2 UNION SELECT c.author_id from '.$wpdb->prefix.'authors_suscribers c WHERE c.suscriber_id = '.$user_id;
	
	
	$friends_subscribers_ids = $wpdb->get_results( $friends_subscribers_qry );
	
	$fs_ids = '';
	
	$count = 0;
	foreach($friends_subscribers_ids as $key=>$value){
		
		if($count == 0)
			$fs_ids .= $value->id;
		else
			$fs_ids .= ','.$value->id;
		
		$count++;	
	}

	if($fs_ids == ''){
		
		$btquery = "SELECT ID as post_id, post_type FROM ".$wpdb->prefix."posts WHERE post_type IN ('dyn_book','dyn_file') AND post_status = 'publish' ORDER BY post_date DESC LIMIT 50";
		
	}else{
		
		//~ $btquery = "SELECT ID as post_id, post_type FROM ".$wpdb->prefix."posts WHERE post_type IN ('dyn_book','dyn_file') AND post_status = 'publish' ORDER BY post_date LIMIT 50";
		
		$btquery = "SELECT ID as post_id, post_type FROM ".$wpdb->prefix."posts WHERE post_type IN ('dyn_book','dyn_file') AND post_author IN ($fs_ids) AND post_author != $user_id AND post_status = 'publish' ORDER BY post_date DESC LIMIT 50";
		
	}
	
	
}

$bresult = $wpdb->get_results( $btquery );
?>
<?php if($bresult){

	foreach ($bresult as $bdata){
		
		if($bdata->post_type == 'dyn_book'){
			show_book($bdata); // display book
		}else{
			show_file($bdata); // display file
		}

	}
	   
}else{		   
		echo '<div style="width:100%;float:left;" class="main_feed"><div style="width: 60%; background: rgb(242, 242, 242) none repeat scroll 0% 0%; border: 1px solid rgb(225, 225, 225); padding: 10px; margin: 20px auto;" class="box"><p><center>No Recent Activity.</center></p></div></div>';
} ?>

</div>

<?php

function show_file($fhdata){
	global $wpdb;
	
	$postID = $fhdata->post_id; 
	$post=get_post($postID);

	$privacyOption   = get_post_meta( $postID, 'privacy-option' ); 
	$post_author     = $post->post_author; 
	 
	$book_author = get_the_author_meta( 'display_name', $post_author); 
	$userId = get_post_field( 'post_author', $postID );
	 
	 
	$theflContent=$post->post_content; 
	$file_author = get_the_author_meta( 'display_name', $post->post_author );

	 $refr = get_post_meta($postID,'video_options_refr',true);
		if($refr){}
		else{$refr= 'No reference given'; }

	$ftype=apply_filters( 'dyn_file_type_by_mime', get_the_ID() );

	$imgClass = apply_filters('dyn_class_by_mime', $postID);

	$download_count=get_post_meta($postID, 'dyn_download_count', true);

	$revnum =apply_filters( 'dyn_number_of_post_review', $postID, "post"); 

	$endors=$wpdb->get_results( 'SELECT user_id FROM '.$wpdb->prefix.'endorsements WHERE post_id = ' . $postID);
	$endorsnum=count($endors);

	$favs=$wpdb->get_results('SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id='. $postID);
	$favsnum=count($favs);
 				 
	if((isset($privacyOption[0]) and ($privacyOption[0] != "private")) || empty($privacyOption))
	  {
		$display_template .= '<div style="width:100%;float:left;" class="main_feed">';
		$display_template .= '<div style="width:60%;background:#f2f2f2;min-height: 470px;margin:15px auto; border:1px solid #e1e1e1;" class="box">';
		$display_template .= '<div style="width:100%;background:#f2f2f2;float:left;padding:16px" class="feed_profile">';

		preg_match_all('/(\w+\s)/', $post->post_title, $matches);

		if ( count($matches[0]) && count($matches[1]) >4){

		$title = $matches[1][0] .  $matches[1][1] . $matches[1][2] . '[...]';
		}
		else if(strlen($post->post_title)>30){

		$title = substr($post->post_title, 0, 30) .' [...]';
		}
		else {

		$title = $post->post_title;
		}
		
		$profile_img = get_user_meta( $userId, 'profile_pic_meta', true );
		if($profile_img){
			$user_img_src = site_url() . '/profile_pic/' . $profile_img;
		}else{
			$user_img_src= get_template_directory_uri().'/images/no-image.png';
		}
		
		$display_template .= "<img style='width:60px;height:60px;float:left;margin:0 10px 0 0;' src='". $user_img_src."' class='layout-3-thumb' >";
		
		$display_template .= "<h4 style='padding-left:10px; color: steelblue;font-size: 15px; font-weight: 600; margin-top:0;margin-bottom:2px;'><span style='color:#222;'>".$book_author." </span><span style='color:#606060;font-weight: 600;'>added</span> <a href='". get_permalink( $postID) ."'> ".$title." </a></h4>";


		$display_template .= "<p style='padding-left:10px;font-size: 15px;font-weight:500;'>". human_time_diff(get_the_time('U', $postID), current_time('timestamp') ) . " ago</p>";

		$display_template .= "</div>";
		
		$display_template .= "<div class='image-holder filesz dyn-file-sprite ". $imgClass ."' style='width:96%;height:250px;margin:0 2%;'><a href=". get_permalink($postID) ."><div class='hover-item dyn-file'><i class='fa fa-download' style='left:50%;'></i></div></a></div>";
		

		$display_template .= '<div class="img_bottom" style="width:100%; padding: 0 6px 5px 5px;">';
		$display_template .= '<div class="feed_details" style="width:100%;float:left;">';
		$display_template .= '<div class="detail" style="width:100%;float:left;">';
		$display_template .= '<ul style="padding:10px; margin:0; width:100%; float:left;">';

		$videopress_meta_key = 'popularity_count';
		$videopress_meta_value = get_post_meta( $postID, $videopress_meta_key, true );

		if( $videopress_meta_value == '' ){
		$views = 'No Views';
		}else{
		$views = number_format($videopress_meta_value) . ' Views';
		}


		$comments = ($post->comment_count == 0) ? 'No' : $post->comment_count;


		$display_template .= "<li style='display:inline-block;width:120px;height:36px;border-radius:2px;line-height:36px; border:1px solid #b6b6b6; background:#ddd;text-align:center;margin-left:!0px;margin: 0 6px;'><span style='margin-right:6px;'>". $views ."</span></li>";

		$display_template .= "<li style='display:inline-block;width:120px;height:36px;border-radius:2px;line-height:36px; border:1px solid #b6b6b6; background:#ddd;text-align:center;margin-left:!0px;margin: 0 6px;'><span style='margin-right:6px;'>". $revnum ."</span></li>";

		$display_template .= "<li style='display:inline-block;width:120px;height:36px;border-radius:2px;line-height:36px; border:1px solid #b6b6b6; background:#ddd;text-align:center;margin-left:!0px;margin: 0 6px;'><span style='margin-right:6px;'>". $comments ."</span>Comments</li>";

		$display_template .= "<li style='display:inline-block;width:120px;height:36px;border-radius:2px;line-height:36px; border:1px solid #b6b6b6; background:#ddd;text-align:center;margin-left:!0px;margin: 0 6px;'><span style='margin-right:6px;'>". $endorsnum ."</span>Endorsments</li>";

		$display_template .= "<li style='display:inline-block;width:120px;height:36px;border-radius:2px;line-height:36px; border:1px solid #b6b6b6; background:#ddd;text-align:center;margin-left:!0px;margin: 0 6px;'><span style='margin-right:6px;'>". $favsnum ."</span>Favourites</li>";
		
		$display_template .= "<li style='display:inline-block;width:120px;height:36px;border-radius:2px;line-height:36px; border:1px solid #b6b6b6; background:#ddd;text-align:center;margin-left:!0px;margin: 0 6px;'><span style='margin-right:6px;'>". $download_count ."</span>Downloads</li>";


		$display_template .= '</ul></div></div>';
		$display_template .= "<p style='font-size:14px;padding:4px 0 5px 10px; font-weight:500;width: 100%;'>". strip_tags($post->post_content) ."</p>";
		$display_template .= "</div></div></div>";

		echo $display_template; 
	  }
				 
		 ?>
	
<?php
	
}




function show_book($bdata){
	global $wpdb;
	$bpost=get_post($bdata->post_id);
	$postID = $bdata->post_id;

	$privacyOption   = get_post_meta( $postID, 'privacy-option' );
	$post_author     = $bpost->post_author;
	

	$book_author = get_the_author_meta( 'display_name', $post_author);
	$userId = get_post_field( 'post_author', $postID );
	
	$refr = get_post_meta($postID,'video_options_refr',true);
	if($refr){}
		else{$refr= 'No reference given'; }

	$thumbnail_id=get_post_thumbnail_id($postID);
	$thumbnail_url=wp_get_attachment_image_src($thumbnail_id, 'full');
	$imgsrc= $thumbnail_url [0];


	$revnum =apply_filters( 'dyn_number_of_post_review', $postID, "post");



	$endors = $wpdb->get_results("SELECT user_id FROM ".$wpdb->prefix."endorsements WHERE post_id = " . $postID);
	$endorsnum=count($endors);
	
	
	
	$favs=$wpdb->get_results('SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id='. $postID);
	$favsnum=count($favs);
	
	
	//~ echo '<pre>';print_r($privacyOption);die;
	
	$display_template = '';
	
	if((isset($privacyOption[0]) and ($privacyOption[0] != "private")) || empty($privacyOption))
	{

		$display_template .= '<div style="width:100%;float:left;" class="main_feed">';
		$display_template .= '<div style="width:60%;background:#f2f2f2;min-height: 470px;margin:15px auto; border:1px solid #e1e1e1;" class="box">';
		$display_template .= '<div style="width:100%;background:#f2f2f2;float:left;padding:16px" class="feed_profile">';
		
		preg_match_all('/(\w+\s)/', $bpost->post_title, $matches);

		if ( count($matches[0]) && count($matches[1]) >4){
		
			$title = $matches[1][0] .  $matches[1][1] . $matches[1][2] . '[...]';
		}
		else if(strlen($bpost->post_title)>30){
			
			$title = substr($bpost->post_title, 0, 30) .' [...]';
		}
		else {
			
			$title = $bpost->post_title;
		}
		
		
		$profile_img = get_user_meta( $userId, 'profile_pic_meta', true );
		if($profile_img){
			$user_img_src = site_url() . '/profile_pic/' . $profile_img;
		}else{
			$user_img_src = get_template_directory_uri().'/images/no-image.png';
		}
			
		$display_template .= "<img style='width:60px;height:60px;float:left;margin:0 10px 0 0;' src='". $user_img_src."' class='layout-3-thumb' >";
		
		$display_template .= "<h4 style='padding-left:10px; color: steelblue;font-size: 15px; font-weight: 600; margin-top:0;margin-bottom:2px;'><span style='color:#222;'>".$book_author." </span><span style='color:#606060;font-weight: 600;'> added</span> <a href='". get_permalink( $postID) ."'> ".$title." </a></h4>";
		
		
		$display_template .= "<p style='padding-left:10px;font-size: 15px;font-weight:500;'>". human_time_diff(get_the_time('U', $postID), current_time('timestamp') ) . " ago</p>";
		
		$display_template .= "</div>";
		
		
		if( has_post_thumbnail($postID) ){
			$display_template .= '<div class="imgbox">';
			$display_template .=  get_the_post_thumbnail($postID, 'medium-thumb', array( 'class' => 'layout-3-thumb imgbox' ) );
			$display_template .= '</div>';
		}else{
			$display_template .=  '<div class="imgbox"><img src="'.get_template_directory_uri().'/images/no-image.png"></div>';
		}
		
		$display_template .= '<div class="img_bottom" style="width:100%;padding: 0 6px 5px 5px;">';
		$display_template .= '<div class="feed_details" style="width:100%;">';
		$display_template .= '<div class="detail" style="width:100%;">';
		$display_template .= '<ul style="padding:10px; margin:0; width:100%;">';
		
		$videopress_meta_key = 'popularity_count';
		$videopress_meta_value = get_post_meta( $postID, $videopress_meta_key, true );

		if( $videopress_meta_value == '' ){
			$views = 'No Views';
		}else{
			$views = number_format($videopress_meta_value) . ' Views';
		}
		
		
		$comments = ($bpost->comment_count == 0) ? 'No' : $bpost->comment_count;
		
		
		$display_template .= "<li style='display:inline-block;width:120px;height:36px;border-radius:2px;line-height:36px; border:1px solid #b6b6b6; background:#ddd;text-align:center;margin-left:!0px;margin: 0 6px;'><span style='margin-right:6px;'>". $views ."</span></li>";
		
		$display_template .= "<li style='display:inline-block;width:120px;height:36px;border-radius:2px;line-height:36px; border:1px solid #b6b6b6; background:#ddd;text-align:center;margin-left:!0px;margin: 0 6px;'><span style='margin-right:6px;'>". $revnum ."</span></li>";
		
		$display_template .= "<li style='display:inline-block;width:120px;height:36px;border-radius:2px;line-height:36px; border:1px solid #b6b6b6; background:#ddd;text-align:center;margin-left:!0px;margin: 0 6px;'><span style='margin-right:6px;'>". $comments ."</span>Comments</li>";
		
		$display_template .= "<li style='display:inline-block;width:120px;height:36px;border-radius:2px;line-height:36px; border:1px solid #b6b6b6; background:#ddd;text-align:center;margin-left:!0px;margin: 0 6px;'><span style='margin-right:6px;'>". $endorsnum ."</span>Endorsments</li>";
		
		$display_template .= "<li style='display:inline-block;width:120px;height:36px;border-radius:2px;line-height:36px; border:1px solid #b6b6b6; background:#ddd;text-align:center;margin-left:!0px;margin: 0 6px;'><span style='margin-right:6px;'>". $favsnum ."</span>Favourites</li>";
		
		
		$display_template .= '</ul></div></div>';
		$display_template .= "<p style='font-size:14px;padding:4px 0 5px 10px; font-weight:500;width: 100%;'>". strip_tags($bpost->post_content) ."</p>";
		$display_template .= "</div></div></div>";
		
		echo $display_template;
	}
}

?>


<?php get_footer(); ?>
