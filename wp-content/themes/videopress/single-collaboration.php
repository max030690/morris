<?php get_header(); $postId=get_the_ID(); ?>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/jquery.tokenize.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/jquery.tokenize.css" />
<div class="container p_90">

	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/font-awsome/css/font-awesome.min.css">

	<div class="grid-3-4 centre-block">

		<div class="grid-4-4-sidebar single-page">

			<?php while (have_posts()) : the_post(); ?>

				<?php

				$collaboration_id = get_the_ID();

				$result = $wpdb->get_results("SELECT *FROM ".$wpdb->prefix."authors_collaboration_list WHERE to_collaboration_id='".get_current_user_id()."' AND collaboration_post_id='".$collaboration_id."' ");

				if(get_the_author_meta( 'ID' ) == get_current_user_id() || count($result) > 0){

				?>
				<div class="top_title">
				<h4 class="video-title" style="display: block;"><?php the_title(); ?></h4>

				<button data-toggle="collapse" class="btn btn-success btn-sm collaboration_share_box_action" data-target="#collaboration_share_box_action">Share</button>
				</div>
				<div id="collaboration_share_box_action" class="collapse">

				<form method="post" class="collaboration_share_request_form">
					<h6><b>Share to other user</b></h6>
					<div class="select-box">
						<?php
						echo '<span>Please select the User you want to give access</span>
							<select id="tokenize" class="tokenize-sample to_collaboration_id" name="to_collaboration_id[]" multiple="multiple">';
							$args1 = array(
								'role'		=> 'free_user',
								'orderby'	=> 'id',
								'order'		=> 'desc'
							);
							$subscribers = get_users($args1);
							foreach ($subscribers as $user) {
								if(get_current_user_id() != $user->id)
								{
									echo '<option value="'.$user->id.'">' . $user->display_name.'</option>';
								}
							}
						echo '</select>';
						?>
					</div>
					<input type="hidden" name="collaboration_post_id" value="<?php echo get_the_ID();?>" />
					<input type="hidden" name="from_collaboration_id" value="<?php echo get_the_author_meta( 'ID' ); ?>" />
					<input type="button" name="collaboration_share_request" class="btn btn-success collaboration_share_request" value="Share" />

				</form>

				</div>


				<script type="text/javascript">
					jQuery('#tokenize').tokenize();

					jQuery(document).ready(function($){

						/* $(".collaboration_share_box_action").on("click",function(){
							$(".collaboration_share_request_form").trigger('reset');
						});	 */

						$(".collaboration_share_request").on("click",function(){

							var $aSelected = $('.to_collaboration_id .TokensContainer').find('li');

							if( $aSelected.hasClass('Token') )
							{
								var collaboration_record = $(".collaboration_share_request_form").serialize();
								$.ajax({
									type: "POST",
									url: "<?php echo plugins_url(); ?>/dyn-collaboration/dyn-collaboration-ajax.php",
									data: collaboration_record,
									success: function(data){
										setTimeout(function(){ window.location.assign(window.location.href); }, 1000);
									},
									error: function(xhr, textStatus, error){
										$(".collaboration_share_request").trigger("click");
									}
								});
							}else{
								alert("User not selected");
								return false;
							}
						});

					});

				</script>
				<style>.tokenize-sample { width: 300px }</style>

				

				<?php// get_the_content();?>


<script>
$(function(){
	
	var appendthis =  ("<div class='modal-overlay js-modal-close'></div>");

	  $('a[data-modal-id]').click(function(e) {
		e.preventDefault();
		$("body").append(appendthis);
		$(".modal-overlay").fadeTo(500, 0.7);
		//$(".js-modalbox").fadeIn(500);
		var modalBox = $(this).attr('data-modal-id');
		$('#'+modalBox).fadeIn($(this).data());
	  });  
	  
	  
	$(".js-modal-close, .modal-overlay").click(function() {
	  $(".modal-box, .modal-overlay").fadeOut(500, function() {
		$(".modal-overlay").remove();
	  });
	});
	 
	$(window).resize(function() {
	  $(".modal-box").css({
		top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
		left: ($(window).width() - $(".modal-box").outerWidth()) / 2
	  });
	});
	 
	$(window).resize();
 
});
</script>

				<?php
					//show the list of collaboration videos from this user
					$user_ID = get_current_user_id();
					//get all the authors where this user is suscribed
					$dyn_file = array();
					$dyn_book = array();
					$dyn_video = array();
					$product = array();
					//$data_result = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."authors_collaboration_list WHERE from_collaboration_id =".$user_ID);
					$data_result = $wpdb->get_results(
						"SELECT * FROM ".$wpdb->prefix."postmeta
						WHERE meta_key = 'collaboration_id'
						AND meta_value = '".$collaboration_id."'
						ORDER BY post_id ASC");

					if($data_result){
						foreach($data_result as $row){
							if($row->post_id != '0'){
								if(get_post_type($row->post_id) == 'dyn_file'){
									$dyn_file[] = $row->post_id;
								}else if(get_post_type($row->post_id) == 'dyn_book'){
									$dyn_book[] = $row->post_id;
								}else if(get_post_type($row->post_id) == 'product'){
									$product[] = $row->post_id;
								}else{
									$dyn_video[] = $row->post_id;
								}
							}
						}
					}
					$dyn_file 		= array_unique($dyn_file);
					$dyn_book 		= array_unique($dyn_book);
					$dyn_video 		= array_unique($dyn_video);
					$product		= array_unique($product);
					/* print_r($dyn_file);echo "<br>";
					print_r($dyn_book);echo "<br>";
					print_r($dyn_video);echo "<br>"; */
					?>



				<div class="video_main_section">
			<div class="thumb_cnt">
				<div class="iner_title"><label>Videos</label>

					<a class="js-open-modal btn btn-info btn-sm pull-right" href="" data-modal-id="popup1">Add Video</a>
					<div id="popup1" class="modal-box">  
						<div class="modal-body collaboration">
							<header>
								<a href="#" class="js-modal-close close">×</a>
								<h3>Add Video</h3>
							</header>
							<?php echo do_shortcode('[add_video_collaboration collaboration_id="'.$collaboration_id.'"]'); ?>
						</div>
					</div>

				</div>
			<div class="cover-twoblocks">
				<!--<div class="sortvideo-div">
					<div class="dyn-book-sort">
						<label></label>
					</div>
				</div>-->
				<div class="well well-sm">
					<strong>Views</strong>
					<div class="btn-group">
						<a href="#" id="list" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th-list">
						</span>List</a> <a href="#" id="grid" class="btn btn-default btn-sm"><span
							class="glyphicon glyphicon-th"></span>Grid</a>
					</div>
				</div>
			</div>
			</div>
				

				<div id="favoriteVideos">
			

			<script type="text/javascript">
			$(document).ready(function() {
				$('#favoriteVideos #products .item').addClass('grid-group-item');
				$('#favoriteVideos #list').click(function(event){event.preventDefault();$('#favoriteVideos #products .item').addClass('list-group-item');});
				$('#favoriteVideos #grid').click(function(event){event.preventDefault();$('#favoriteVideos #products .item').removeClass('list-group-item');$('#favoriteVideos #products .item').addClass('grid-group-item');});
			});
			</script>
			<?php
			$paged = ( get_query_var( 'page' ) ) ? absint( get_query_var( 'page' ) ) : 1;
			if(count($dyn_video)>0){


				$args = array(
					'post__in' => $dyn_video,
					'paged' => $paged,
					'post_status' => 'publish',
					'posts_per_page' => 10
				);
				$the_query = new WP_Query( $args );

				if ( $the_query->have_posts() ) :
				echo '<div id="products" class="list-group">';
				while ( $the_query->have_posts() ) : $the_query->the_post();
				?>
				<div class="item col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 post-<?php the_ID(); ?>">
			<div class="thumbnail layout-2-wrapper solid-bg">

			<div class="grid-6 first">
							<div class="image-holder">
								<a href="<?php echo get_permalink(); ?>">
									<div class="hover-item"></div>
								</a>
								<?php
								if( has_post_thumbnail() ){
									the_post_thumbnail('small-thumb', array( 'class' => 'layout-2-thumb' ) );
								}else{
									echo '<img src="'.get_template_directory_uri().'/images/no-image.png" class="layout-2-thumb">';
								}
								?>
							</div>
							<div class="layout-title-box layout-title-box-<?php the_ID(); ?>" style="display:block;">
								<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php echo ctpreview( $content = get_the_title() , $length = 30 , $message = 'No Title available' , $html = ' [...]' );
								?></a></h6>
								<ul class="bottom-detailsul">
									<?php if(get_the_author_id() == get_current_user_id()){ ?>
									<li><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>"><i class="fa fa-trash-o"></i> Delete</a></li><?php } ?>
									<li><a class="detailsblock-btn" data-modal-id="myModal-<?php the_ID(); ?>"><i class="fa fa-info-circle" aria-hidden="true"></i> Details</a></li>
									<?php if(get_the_author_id() == get_current_user_id()){ ?>
									<li><a class="changesettingsblock-btn" data-modal-id="myModalPrivacy-<?php the_ID(); ?>"><i class="fa fa-info-circle" aria-hidden="true"></i>Privacy</a></li>
									<?php } ?>
								</ul>
							</div>
					    </div>
						<script type="text/javascript">
$(function(){

var appendthis =  ("<div class='modal-overlayblockdiv'></div>");

	$('a[data-modal-id]').click(function(e) {
		e.preventDefault();
    $("body").append(appendthis);
    $(".modal-overlay").fadeTo(500, 0.7);
    //$(".js-modalbox").fadeIn(500);
		var modalBox = $(this).attr('data-modal-id');
		$('#'+modalBox).fadeIn($(this).data());
	});


$(".js-modal-close, .modal-overlay").click(function() {
    $(".modal-box, .modal-overlay").fadeOut(500, function() {
        $(".modal-overlay").remove();
    });

});

$(window).resize(function() {
    $(".modal-box").css({
        top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
        left: ($(window).width() - $(".modal-box").outerWidth()) / 2
    });
});

$(window).resize();

});
</script>

<div class="modal-box" id="myModalPrivacy-<?php the_ID(); ?>">
			<div class="modal-body">
				<a class="js-modal-close close privacyCloseI-<?php the_ID(); ?>">×</a>
				<div class="layout-2-details layout-2-details-<?php the_ID(); ?>">

						<div class="form-group">
							<label for="dyn-tags">Privacy Options:</label>
							<div class="checkbox">
								  <?php
								  //$selectUsersListArray = explode(",", $selectUsersList[0]);
								    $selectUsersListArray = $selectUsersList;
								     if(isset($privacyOption[0]) && $privacyOption[0] != "")
									 {
								       //echo $privacyOption[0]."<br>";
										 ?>
										    <label>Private
											  <?php
											     if($privacyOption[0] == "private")
												 {
											        ?>
													   <input type="radio" id="privacy-radio-<?php the_ID(); ?>" name="privacy-option-<?php the_ID(); ?>" class="dyn-select-file" class="form-control" value="private" checked="checked">
													<?php
												 }
												 else
												 {
													 ?>
													   <input type="radio" id="privacy-radio-<?php the_ID(); ?>" name="privacy-option-<?php the_ID(); ?>" class="dyn-select-file" class="form-control" value="private">
													<?php
												 }
											  ?>
											</label>
											<label>Public
											<input type="radio" id="privacy-radio1-<?php the_ID(); ?>" class="dyn-select-file" class="form-control" value="public" <?php if (isset($privacyOption[0]) && $privacyOption[0]=="public") echo "checked";?> name="privacy-option2-<?php the_ID(); ?>">
											</label>
										 <?php
									 }
									 else
									 {
										 ?>
										    <label>Private
											  <input type="radio" id="privacy-radio-<?php the_ID(); ?>" name="privacy-option-<?php the_ID(); ?>" class="dyn-select-file" class="form-control" value="private">
											</label>
											<label>Public
											 <input type="radio" id="privacy-radio1-<?php the_ID(); ?>" name="privacy-option2-<?php the_ID(); ?>" class="dyn-select-file" class="form-control" value="public">
											</label>
										 <?php
									 }
								  ?>
							</div>
							 <?php
								 if(isset($privacyOption[0]) and $privacyOption[0] != "")
								 {
									   if($privacyOption[0] == "private")
									   {
										    ?>
											<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/jquery.tokenize.js"></script>
												<div class="select-box-<?php the_ID(); ?>">
													<?php
													  echo '<select id="tokenize-'.get_the_ID().'" class="tokenize-sample" name="select_users_list[]" style="width: 350px; height: 150px;" multiple="multiple">';
														  $args1 = array(
															  'role' => 'free_user',
															  'orderby' => 'id',
															  'order' => 'desc'
														   );
														  $subscribers = get_users($args1);
														  foreach ($subscribers as $user) {
															  if(get_current_user_id() == $user->id)
															  { }
															  else
															  {
														          if( in_array($user->id, $selectUsersListArray) )
																  {
																	   echo '<option value="'.$user->id.'" selected="selected">' . $user->display_name.'</option>';
																  }
                                                                  else{
																	   echo '<option value="'.$user->id.'">' . $user->display_name.'</option>';
																  }
															  }
														  }
													  echo '</select>';
													?>
													<script type="text/javascript">
											          $(document).ready(function(){
														  $("#privacy-radio").click(function(){
															 $(".select-box").slideDown();
														  });
														  $("#privacy-radio1").click(function(){
															 $(".select-box").slideUp();
															 $('select#select_users_list option').removeAttr("selected");
														  });

													  });
													  $('#tokenize-<?php echo get_the_ID(); ?>').tokenize();
				                                         datas: "bower.json.php"
												  </script>
												  <style> .tokenize-sample { width: 350px;; }</style>
												</div>
										   <?php
									   }
									   else
									   {
                                            ?>
											<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/jquery.tokenize.js"></script>
												<div style="display:none;" class="select-box-<?php the_ID(); ?>">
													<?php
													  echo '<select id="tokenize-'.get_the_ID().'" class="tokenize-sample" name="select_users_list[]" style="width: 350px; height: 150px;" multiple="multiple">';
														  $args1 = array(
															  'role' => 'free_user',
															  'orderby' => 'id',
															  'order' => 'desc'
														   );
														  $subscribers = get_users($args1);
														  foreach ($subscribers as $user) {
															  if(get_current_user_id() == $user->id)
															  { }
															  else
															  {
																  echo '<option value="'.$user->id.'">' . $user->display_name.'</option>';
															  }
														  }
													  echo '</select>';
													?>
													<script type="text/javascript">
											          $(document).ready(function(){
														  $("#privacy-radio").click(function(){
															 $(".select-box").slideDown();
														  });
														  $("#privacy-radio1").click(function(){
															 $(".select-box").slideUp();
															 $('select#select_users_list option').removeAttr("selected");
														  });

													  });
													  $('#tokenize-<?php echo get_the_ID(); ?>').tokenize();
				                                         datas: "bower.json.php"
												  </script>
												  <style> .tokenize-sample { width: 350px;; }</style>
												</div>
										   <?php
									   }
								 }
								 else
								 {
									   ?>
									   <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/jquery.tokenize.js"></script>
									        <div style="display:none;" class="select-box-<?php the_ID(); ?>">
												<?php
												  echo '<select id="tokenize-'.get_the_ID().'" class="tokenize-sample" name="select_users_list[]" style="width: 350px; height: 150px;" multiple="multiple">';
													  $args1 = array(
														  'role' => 'free_user',
														  'orderby' => 'id',
														  'order' => 'desc'
													   );
													  $subscribers = get_users($args1);
													  foreach ($subscribers as $user) {
														  if(get_current_user_id() == $user->id)
														  { }
														  else
														  {
															  echo '<option value="'.$user->id.'">' . $user->display_name.'</option>';
														  }
													  }
												  echo '</select>';
												?>
												<script type="text/javascript">
											          $(document).ready(function(){
														  $("#privacy-radio").click(function(){
															 $(".select-box").slideDown();
														  });
														  $("#privacy-radio1").click(function(){
															 $(".select-box").slideUp();
															 $('select#select_users_list option').removeAttr("selected");
														  });

													  });
													  $('#tokenize-<?php echo get_the_ID(); ?>').tokenize();
				                                         datas: "bower.json.php"
												  </script>
												  <style> .tokenize-sample { width: 350px;; }</style>
											</div>
									   <?php
								 }
							 ?>
						
							 <div id="msgLoader-<?php the_ID(); ?>"></div>
                             <input type="button" id="save-Privacy-Option-<?php the_ID(); ?>" name="save-privacy-option-<?php the_ID(); ?>" value="Save Privacy Option" currentUserID="<?php echo get_current_user_id(); ?>" postID="<?php echo get_the_ID(); ?>">
						</div>
					<script>
                      $(document).ready(function(){
						  $("#privacy-radio-<?php the_ID(); ?>").click(function(){
							 $("#privacy-radio1-<?php the_ID(); ?>").prop( "checked", false );
						     $(".select-box-<?php the_ID(); ?>").slideDown();
						  });
						  $("#privacy-radio1-<?php the_ID(); ?>").click(function(){
							 $("#privacy-radio-<?php the_ID(); ?>").prop( "checked", false );
						     $(".select-box-<?php the_ID(); ?>").slideUp();
							 $('select#select_users_list-<?php the_ID(); ?> option').removeAttr("selected");
						  });
						  $(".privacyCloseI-<?php the_ID(); ?>").click(function(){
							 $('#msgLoader-<?php the_ID(); ?>').html('');
						  });
						  $("#save-Privacy-Option-<?php the_ID(); ?>").click(function(){
							   var userID = $(this).attr('currentUserID');
							   var postID = $(this).attr('postID');
							   var select_users_list = $('#tokenize-'+postID).val();
							   if ( typeof(select_users_list) !== "undefined" && select_users_list !== null )
							   { }
						       else
							   {
								   select_users_list = '';
							   }
							   var privacyOptionV = "";
							   if($('#privacy-radio-<?php the_ID(); ?>').is(':checked'))
							   {
								   var privateChecked = "yes";
							   }
							   else
							   {
								   var privateChecked = "no";
							   }
							   if($('#privacy-radio1-<?php the_ID(); ?>').is(':checked'))
							   {
								   var publicChecked = "yes";
							   }
							   else
							   {
								   var publicChecked = "no";
							   }
							   if(privateChecked == "yes")
							   {
								   privacyOptionV = "private";
							   }
							   else if(publicChecked == "yes")
							   {
								   privacyOptionV = "public";
							   }
							   else
							   {
								   privacyOptionV = "";
							   }
                               $.ajax({
									type: 'POST',
									url: "<?php echo site_url(); ?>/update-Privacy.php",
									data: { userID: userID, postID: postID, privacyOptionV : privacyOptionV,
									select_users_list : select_users_list},
									beforeSend: function(){
									  $('#msgLoader-<?php the_ID(); ?>').html('<img src="<?php echo site_url(); ?>/wp-content/themes/videopress/images/loading.gif" />');
									},
									success: function(data){
										 $('#msgLoader-<?php the_ID(); ?>').html(data);
									}
								});
						  });
                      });
					 // $('#tokenize').tokenize();
                    </script>
					<style> .tokenize-sample { width: 350px;; }</style>
					<style>
                      #privacy-radio1-<?php the_ID(); ?>, #privacy-radio-<?php the_ID(); ?> {
                        padding: 5px;
                        text-align: center;
                      }

                     #select-box-<?php the_ID(); ?> {
                       padding: 50px;
                       display: none;
                     }
                    </style>
				</div>
			</div>
</div>
			<div class="modal-box" id="myModal-<?php the_ID(); ?>">
			<div class="modal-body">
			<a class="js-modal-close close">×</a>
			<div class="layout-2-details layout-2-details-<?php the_ID(); ?>">
				<div class="image-holder">
					<a href="<?php echo get_permalink(); ?>">
						<div class="hover-item"></div>
					</a>

					<?php
					if( has_post_thumbnail() ){
						the_post_thumbnail('small-thumb', array( 'class' => 'layout-2-thumb' ) );
					}else{
						echo '<img src="'.get_template_directory_uri().'/images/no-image.png" class="layout-2-thumb">';
					}
					?>

				</div>
				<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h6>
				<?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
				<div class="video_excerpt_text one"><h4 class="tittle-h4">Description 2</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = '' , $message = 'No Description available' , $html = ' [...]' );?></div></div>
				<div class="video_excerpt_text two"><h4 class="tittle-h4">Reference</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_post_meta(get_the_ID(),'video_options_refr',true) , $length = '' , $message = 'No referance given' , $html = ' [...]' );?></div></div>
				<ul class="stats">
					<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
					<li><?php videopress_countviews( get_the_ID() ); ?></li>
					<li><i class="fa fa-wrench"></i> <?php $result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
						echo count($result); ?> Endorsments</li>
					<!-- favourite section -->
					<li><i class="fa fa-heart"></i>
					<?php
					  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
					  $result1 = $wpdb->get_results($sql_aux);
					  echo count($result1);
					?> Favorites</li>
					<!-- end favorite -->
					<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
					<li><?php comments_number() ?></li>
				</ul>
				<div class="clear"></div>
				<p><?php echo videopress_content('240'); ?></p>

					<?php if(get_the_author_id() == get_current_user_id()){ ?>
					<p><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>"><i class="fa fa-trash-o"></i> Delete</a></p>
					<?php } ?>


			</div></div></div>

			<div class="layout-2-details layout-2-details-<?php the_ID(); ?>" style="display:none;">
				<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h6>
				<?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
				<div class="video_excerpt_text one"><h4 class="tittle-h4">Description</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = 100 , $message = 'No Description available' , $html = '<a data-modal-id="myModal-'.get_the_ID().'"> [...]</a>' ); ?></div></div>
				<ul class="stats">
					<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
					<li><?php videopress_countviews( get_the_ID() ); ?></li>
					<li><i class="fa fa-wrench"></i> <?php $result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
						echo count($result); ?> Endorsments</li>
					<!-- favourite section -->
					<li><i class="fa fa-heart"></i>
					<?php
					  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
					  $result1 = $wpdb->get_results($sql_aux);
					  echo count($result1);
					?> Favorites</li>
					<!-- end favorite -->
					<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
					<li><?php comments_number() ?></li>
				</ul>
				<div class="clear"></div>
				<p><?php echo videopress_content('240'); ?></p>

					<p><?php if(get_the_author_id() == get_current_user_id()){ ?>
					      <a href="#" class="del-video" data-id="<?= get_the_ID(); ?>"><i class="fa fa-trash-o"></i> Delete</a>

					      <a class="privacy-video" data-modal-id="myModalPrivacy-<?php the_ID(); ?>"><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;Privacy</a>
						<?php } ?>
					</p>

			</div>

			<div class="clear"></div>
</div>
</div>
				<?php
				endwhile;
				echo "</div><div class='paginate_division'>".paginate_links(array('add_args' => array('display_page' => 'favorite_video')))."</div>";
				endif;
				wp_reset_postdata();
			}else{
				echo "<div>No Videos Available</div>";
			}?>

		</div>
		</div>


			
				<div class="file_main_section">
				<div class="thumb_cnt">
				<div class="iner_title">
				<label>Files</label>
					
					<a class="js-open-modal btn btn-info btn-sm pull-right" href="" data-modal-id="popup2">Add File</a>
					<div id="popup2" class="modal-box">  
						<div class="modal-body collaboration">
							<header>
								<a href="#" class="js-modal-close close">×</a>
								<h3>Add File</h3>
							</header>
							<?php echo do_shortcode('[add_file_collaboration collaboration_id="'.$collaboration_id.'"]'); ?>
						</div>
					</div>

				</div>
				<div class="cover-twoblocks">
				<!--<div class="sortvideo-div">
					<div class="dyn-book-sort">
						<label></label>
					</div>
				</div>-->
				<div class="well well-sm">
					<strong>Views</strong>
					<div class="btn-group">
						<a href="#" id="list" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th-list">
						</span>List</a> <a href="#" id="grid" class="btn btn-default btn-sm"><span
							class="glyphicon glyphicon-th"></span>Grid</a>
					</div>
				</div>
			</div>
			</div>
			


			<div id="favoriteFiles">
			

			<script type="text/javascript">
			$(document).ready(function() {
				$('#favoriteFiles #products .item').addClass('grid-group-item');
				$('#favoriteFiles #list').click(function(event){event.preventDefault();$('#favoriteFiles #products .item').addClass('list-group-item');});
				$('#favoriteFiles #grid').click(function(event){event.preventDefault();$('#favoriteFiles #products .item').removeClass('list-group-item');$('#favoriteFiles #products .item').addClass('grid-group-item');});
			});
			</script>
			<?php
			if(count($dyn_file)>0){
				$args = array(
					'post__in' => $dyn_file,
					'paged' => $paged,
					'post_status' => 'publish',
					'posts_per_page' => 10,
					'post_type' => 'dyn_file'
				);
				$the_query = new WP_Query( $args );

				if ( $the_query->have_posts() ) :
				echo '<div id="products" class="list-group">';
				while ( $the_query->have_posts() ) : $the_query->the_post();
				?>
				<div class="item col-xs-6 col-lg-6 post-<?php the_ID(); ?>">
				<div class="thumbnail layout-2-wrapper solid-bg">

					<div class="grid-6 first">
						<div class="image-holder filesz dyn-file-sprite <?php echo apply_filters('dyn_class_by_mime', get_the_id()); ?>">
							<a href="<?php echo get_permalink(); ?>">
								<div class="hover-item dyn-file">
			                        <i class="fa fa-download"></i>
			                    </div>
							</a>
						</div>
						<div class="layout-title-box layout-title-box-<?php the_ID(); ?>" style="display:block;">
							<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php echo ctpreview( $content = get_the_title() , $length = 30 , $message = 'No Title available' , $html = ' [...]' ); ?></a></h6>
							<ul class="bottom-detailsul">
							<?php if(get_the_author_id() == get_current_user_id()){ ?>
							<li><p><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>" ><i class="fa fa-trash-o"></i> Delete</a></p></li>
							<?php } ?>
							<li><a class="detailsblock-btn" data-modal-id="myModal-<?php the_ID(); ?>"><i class="fa fa-info-circle" aria-hidden="true"></i> Details</a></li>
							<?php if(get_the_author_id() == get_current_user_id()){ ?>
							<li><a class="changesettingsblock-btn" data-modal-id="myModalPrivacy-<?php the_ID(); ?>"><i class="fa fa-info-circle" aria-hidden="true"></i>Privacy</a></li><?php } ?>
							</ul>
						</div>
					</div>

<script type="text/javascript">
$(function(){

var appendthis =  ("<div class='modal-overlayblockdiv'></div>");

	$('a[data-modal-id]').click(function(e) {
		e.preventDefault();
    $("body").append(appendthis);
    $(".modal-overlay").fadeTo(500, 0.7);
    //$(".js-modalbox").fadeIn(500);
		var modalBox = $(this).attr('data-modal-id');
		$('#'+modalBox).fadeIn($(this).data());
	});


$(".js-modal-close, .modal-overlay").click(function() {
    $(".modal-box, .modal-overlay").fadeOut(500, function() {
        $(".modal-overlay").remove();
    });
});

$(window).resize(function() {
    $(".modal-box").css({
        top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
        left: ($(window).width() - $(".modal-box").outerWidth()) / 2
    });
});

$(window).resize();

});
</script>
<div class="modal-box" id="myModalPrivacy-<?php the_ID(); ?>">
			<div class="modal-body">
				<a class="js-modal-close close privacyCloseI-<?php the_ID(); ?>">×</a>
				<div class="layout-2-details layout-2-details-<?php the_ID(); ?>">

						<div class="form-group">
							<label for="dyn-tags">Privacy Options:</label>
							<div class="checkbox">
								  <?php
								  //$selectUsersListArray = explode(",", $selectUsersList[0]);
                                      $selectUsersListArray = $selectUsersList;
								     if(isset($privacyOption[0]) && $privacyOption[0] != "")
									 {
								       //echo $privacyOption[0]."<br>";
										 ?>
										    <label>Private
											  <?php
											     if($privacyOption[0] == "private")
												 {
											        ?>
													   <input type="radio" id="privacy-radio-<?php the_ID(); ?>" name="privacy-option-<?php the_ID(); ?>" class="dyn-select-file" class="form-control" value="private" checked="checked">
													<?php
												 }
												 else
												 {
													 ?>
													   <input type="radio" id="privacy-radio-<?php the_ID(); ?>" name="privacy-option-<?php the_ID(); ?>" class="dyn-select-file" class="form-control" value="private">
													<?php
												 }
											  ?>
											</label>
											<label>Public
											<input type="radio" id="privacy-radio1-<?php the_ID(); ?>" class="dyn-select-file" class="form-control" value="public" <?php if (isset($privacyOption[0]) && $privacyOption[0]=="public") echo "checked";?> name="privacy-option2-<?php the_ID(); ?>">
											</label>
										 <?php
									 }
									 else
									 {
										 ?>
										    <label>Private
											  <input type="radio" id="privacy-radio-<?php the_ID(); ?>" name="privacy-option-<?php the_ID(); ?>" class="dyn-select-file" class="form-control" value="private">
											</label>
											<label>Public
											 <input type="radio" id="privacy-radio1?>" name="privacy-option2-<?php the_ID(); ?>" class="dyn-select-file" class="form-control" value="public">
											</label>
										 <?php
									 }
								  ?>
							</div>
							 <?php
								 if(isset($privacyOption[0]) && $privacyOption[0] != "")
								 {
									   if($privacyOption[0] == "private")
									   {
										       $args1 = array(
												  'role' => 'free_user',
												  'orderby' => 'id',
												  'order' => 'desc'
											   );
											  $subscribers = get_users($args1);
										    ?>
											<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/jquery.tokenize.js"></script>
												<div class="select-box-<?php the_ID(); ?>">
													<?php
													  echo '<select class="tokenize-sample" id="tokenize-'.get_the_ID().'" name="select_users_list[]" style="width: 350px; height: 150px;" multiple="multiple">';
														  foreach ($subscribers as $user) {
															  if(get_current_user_id() == $user->id)
															  { }
															  else
															  {
														          if( in_array($user->id, $selectUsersListArray) )
																  {
																	   echo '<option value="'.$user->id.'" selected="selected">' . $user->display_name.'</option>';
																  }
                                                                  else{
																	   echo '<option value="'.$user->id.'">' . $user->display_name.'</option>';
																  }
															  }
														  }
													  echo '</select>';
													?>

													<script type="text/javascript">
																		  $(document).ready(function(){
												  $("#privacy-radio").click(function(){
													 $(".select-box").slideDown();
												  });
												  $("#privacy-radio1").click(function(){
													 $(".select-box").slideUp();
													 $('select#select_users_list option').removeAttr("selected");
												  });

											  });
											  $('#tokenize-<?php echo get_the_ID(); ?>').tokenize();
				                                datas: "bower.json.php"
										  </script>
										  <style> .tokenize-sample { width: 350px;; }</style>
												</div>

										   <?php
									   }
									   else
									   {
                                            ?>

												<div style="display:none;" class="select-box-<?php the_ID(); ?>">
													<?php
													  echo '<select class="tokenize-sample" id="tokenize-'.get_the_ID().'" name="select_users_list[]" style="width: 350px; height: 150px;" multiple="multiple">';
														  $args1 = array(
															  'role' => 'free_user',
															  'orderby' => 'id',
															  'order' => 'desc'
														   );
														  $subscribers = get_users($args1);
														  foreach ($subscribers as $user) {
															  if(get_current_user_id() == $user->id)
															  { }
															  else
															  {
																  echo '<option value="'.$user->id.'">' . $user->display_name.'</option>';
															  }
														  }
													  echo '</select>';
													?>

													<script type="text/javascript">
																		  $(document).ready(function(){
												  $("#privacy-radio").click(function(){
													 $(".select-box").slideDown();
												  });
												  $("#privacy-radio1").click(function(){
													 $(".select-box").slideUp();
													 //$('select#select_users_list option').removeAttr("selected");
												  });

											  });
											      $('#tokenize-<?php echo get_the_ID(); ?>').tokenize();
				                                         datas: "bower.json.php"
										  </script>
										  <style> .tokenize-sample { width: 350px; }</style>
												</div>

										   <?php
									   }
								 }
								 else
								 {
									   ?>
									        <div style="display:none;" class="select-box-<?php the_ID(); ?>">
												<?php
												  echo '<select class="tokenize-sample" id="tokenize-'.get_the_ID().'" name="select_users_list[]" style="width: 350px; height: 150px;" multiple="multiple">';
													  $args1 = array(
														  'role' => 'free_user',
														  'orderby' => 'id',
														  'order' => 'desc'
													   );
													  $subscribers = get_users($args1);
													  foreach ($subscribers as $user) {
														  if(get_current_user_id() == $user->id)
														  { }
														  else
														  {
															  echo '<option value="'.$user->id.'">' . $user->display_name.'</option>';
														  }
													  }
												  echo '</select>';
												?>

												<script type="text/javascript">
													 $(document).ready(function(){
												  $("#privacy-radio").click(function(){
													 $(".select-box").slideDown();
												  });
												  $("#privacy-radio1").click(function(){
													 $(".select-box").slideUp();
													 //$('select#select_users_list option').removeAttr("selected");
												  });

											  });
											     $('#tokenize-<?php echo get_the_ID(); ?>').tokenize();
				                                         datas: "bower.json.php"
										  </script>
										  <style> .tokenize-sample { width: 350px; }</style>
											</div>

									   <?php
								 }
							 ?>
					
							 <div id="msgLoader-<?php the_ID(); ?>"></div>
                             <input type="button" id="save-Privacy-Option-<?php the_ID(); ?>" name="save-privacy-option-<?php the_ID(); ?>" value="Save Privacy Option" currentUserID="<?php echo get_current_user_id(); ?>" postID="<?php echo get_the_ID(); ?>">
						</div>


					<script>
                      $(document).ready(function(){
						  $("#privacy-radio-<?php the_ID(); ?>").click(function(){
							 $("#privacy-radio1-<?php the_ID(); ?>").prop( "checked", false );
						     $(".select-box-<?php the_ID(); ?>").slideDown();
						  });
						  $("#privacy-radio1-<?php the_ID(); ?>").click(function(){
							 $("#privacy-radio-<?php the_ID(); ?>").prop( "checked", false );
						     $(".select-box-<?php the_ID(); ?>").slideUp();
							 $('select#select_users_list-<?php the_ID(); ?> option').removeAttr("selected");
						  });
						  $(".privacyCloseI-<?php the_ID(); ?>").click(function(){
							 $('#msgLoader-<?php the_ID(); ?>').html('');
						  });
						  $("#save-Privacy-Option-<?php the_ID(); ?>").click(function(){
							   var userID = $(this).attr('currentUserID');
							   var postID = $(this).attr('postID');
							   var select_users_list = $('#tokenize-'+postID).val();
							   if ( typeof(select_users_list) !== "undefined" && select_users_list !== null )
							   { }
						       else
							   {
								   select_users_list = '';
							   }
							   var privacyOptionV = "";
							   if($('#privacy-radio-<?php the_ID(); ?>').is(':checked'))
							   {
								   var privateChecked = "yes";
							   }
							   else
							   {
								   var privateChecked = "no";
							   }
							   if($('#privacy-radio1-<?php the_ID(); ?>').is(':checked'))
							   {
								   var publicChecked = "yes";
							   }
							   else
							   {
								   var publicChecked = "no";
							   }
							   if(privateChecked == "yes")
							   {
								   privacyOptionV = "private";
							   }
							   else if(publicChecked == "yes")
							   {
								   privacyOptionV = "public";
							   }
							   else
							   {
								   privacyOptionV = "";
							   }
                               $.ajax({
									type: 'POST',
									url: "<?php echo site_url(); ?>/update-Privacy.php",
									data: { userID: userID, postID: postID, privacyOptionV : privacyOptionV,
									select_users_list : select_users_list},
									beforeSend: function(){
									  $('#msgLoader-<?php the_ID(); ?>').html('<img src="<?php echo site_url(); ?>/wp-content/themes/videopress/images/loading.gif" />');
									},
									success: function(data){
										 $('#msgLoader-<?php the_ID(); ?>').html(data);

									}

								});
						  });


                      });

					 $('#tokenize').tokenize();
                    </script>
					<style> .tokenize-sample { width: 350px; }</style>
					<style>
                      #privacy-radio1-<?php the_ID(); ?>, #privacy-radio-<?php the_ID(); ?> {
                        padding: 5px;
                        text-align: center;
                      }

                     #select-box-<?php the_ID(); ?> {
                       padding: 50px;
                       display: none;
                     }
                    </style>
				</div>
			</div>
</div>
					<div class="modal-box" id="myModal-<?php the_ID(); ?>">
					 <div class="modal-body">
					 <a class="js-modal-close close">×</a>
					<div class="layout-2-details layout-2-details-<?php the_ID(); ?>">
						<div class="image-holder filesz dyn-file-sprite <?php echo apply_filters('dyn_class_by_mime', get_the_id()); ?>">
							<a href="<?php echo get_permalink(); ?>">
								<div class="hover-item dyn-file">
			                        <i class="fa fa-download"></i>
			                    </div>
							</a>
						</div>
						<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title();?></a></h6>
						<div>File Type: <?php echo apply_filters( 'dyn_file_type_by_mime', get_the_ID() ); ?></div>
						<?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
						<div class="video_excerpt_text one"><h4 class="tittle-h4">Description 3</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = '' , $message = 'No Description available' , $html = ' [...]' );?></div></div>
						<div class="video_excerpt_text two"><h4 class="tittle-h4">Reference</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_post_meta(get_the_ID(),'video_options_refr',true) , $length = '' , $message = 'No referance given' , $html = ' [...]' );?></div></div>
						<ul class="stats">
							<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
							<li><?php videopress_countviews( get_the_ID() ); ?></li>
							<li><i class="fa fa-wrench"></i>
							<?php
								$result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
								echo count($result);
							?> Endorsments</li>
							<li><i class="fa  fa-heart"></i>
							<?php
							  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
							  $result1 = $wpdb->get_results($sql_aux);
							  echo count($result1);
							?> Favorites</li>
							<!--end favorite-->
							<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
							<li><?php comments_number() ?></li>
							<li><i class="fa fa-download"></i> <?php echo apply_filters( 'dyn_number_of_download', get_the_id()); ?></li>
						</ul>
					
						<p><?php echo videopress_content('240'); ?></p>
						<?php if(get_the_author_id() == get_current_user_id()){ ?>
						<p><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>" ><i class="fa fa-trash-o"></i> Delete</a></p>
						<?php } ?>
					</div></div></div>



					<div class="layout-2-details layout-2-details-<?php the_ID(); ?>" style="display:none;">
						<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title();?></a></h6>
						<div>File Type: <?php echo apply_filters( 'dyn_file_type_by_mime', get_the_ID() ); ?></div>
						<?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
						<div class="video_excerpt_text one"><h4 class="tittle-h4">Description</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = 100 , $message = 'No Description available' , $html = '<a data-modal-id="myModal-'.get_the_ID().'"> [...]</a>' ); ?></div></div>
						<ul class="stats">
							<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
							<li><?php videopress_countviews( get_the_ID() ); ?></li>
							<li><i class="fa fa-wrench"></i>
							<?php
								$result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
								echo count($result);
							?> Endorsments</li>
							<li><i class="fa  fa-heart"></i>
							<?php
							  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
							  $result1 = $wpdb->get_results($sql_aux);
							  echo count($result1);
							?> Favorites</li>
							<!--end favorite-->
							<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
							<li><?php comments_number() ?></li>
							<li><i class="fa fa-download"></i> <?php echo apply_filters( 'dyn_number_of_download', get_the_id()); ?></li>
						</ul>
				
						<p><?php echo videopress_content('240'); ?>
						<?php if(get_the_author_id() == get_current_user_id()){ ?>
						        <a href="#" class="del-video" data-id="<?= get_the_ID(); ?>" ><i class="fa fa-trash-o"></i> Delete</a>
						        <a class="privacy-video" data-modal-id="myModalPrivacy-<?php the_ID(); ?>"><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;Privacy</a>
						<?php } ?>
						</p>
					</div>
				</div>
			</div>
				<?php
				endwhile;
				echo "</div><div class='paginate_division'>".paginate_links(array('add_args' => array('display_page' => 'favorite_file')))."</div>";
				endif;
				wp_reset_postdata();
			}else{
				echo "<div>No Files Available</div>";
			}?>


				<?php
			if(count($product)>0){
				$args = array(
					'post__in' => $product,
					'paged' => $paged,
					'post_status' => 'publish',
					'posts_per_page' => 10,
					'post_type' => 'product'
				);
				$the_query = new WP_Query( $args );

				if ( $the_query->have_posts() ) :
				echo '<div id="products" class="list-group">';
				while ( $the_query->have_posts() ) : $the_query->the_post();
				?>
				<div class="item col-xs-6 col-lg-6 post-<?php the_ID(); ?>">
				<div class="thumbnail layout-2-wrapper solid-bg">

					<div class="grid-6 first">
						<div class="image-holder filesz dyn-file-sprite <?php echo apply_filters('dyn_class_by_mime', get_the_id()); ?>">
							<a href="<?php echo get_permalink(); ?>">
								<div class="hover-item dyn-file">
			                        <i class="fa fa-download"></i>
			                    </div>
							</a>
						</div>
						<div class="layout-title-box layout-title-box-<?php the_ID(); ?>" style="display:block;">
							<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php echo ctpreview( $content = get_the_title() , $length = 30 , $message = 'No Title available' , $html = ' [...]' ); ?></a></h6>
							<ul class="bottom-detailsul">
							<?php if(get_the_author_id() == get_current_user_id()){ ?>
							<li><p><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>" ><i class="fa fa-trash-o"></i> Delete</a></p></li>
							<?php } ?>
							<li><a class="detailsblock-btn" data-modal-id="myModal-<?php the_ID(); ?>"><i class="fa fa-info-circle" aria-hidden="true"></i> Details</a></li>
							<?php if(get_the_author_id() == get_current_user_id()){ ?>
							<li><a class="changesettingsblock-btn" data-modal-id="myModalPrivacy-<?php the_ID(); ?>"><i class="fa fa-info-circle" aria-hidden="true"></i>Privacy</a></li><?php } ?>
							</ul>
						</div>
					</div>

<script type="text/javascript">
$(function(){

var appendthis =  ("<div class='modal-overlayblockdiv'></div>");

	$('a[data-modal-id]').click(function(e) {
		e.preventDefault();
    $("body").append(appendthis);
    $(".modal-overlay").fadeTo(500, 0.7);
    //$(".js-modalbox").fadeIn(500);
		var modalBox = $(this).attr('data-modal-id');
		$('#'+modalBox).fadeIn($(this).data());
	});


$(".js-modal-close, .modal-overlay").click(function() {
    $(".modal-box, .modal-overlay").fadeOut(500, function() {
        $(".modal-overlay").remove();
    });
});

$(window).resize(function() {
    $(".modal-box").css({
        top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
        left: ($(window).width() - $(".modal-box").outerWidth()) / 2
    });
});

$(window).resize();

});
</script>
<div class="modal-box" id="myModalPrivacy-<?php the_ID(); ?>">
			<div class="modal-body">
				<a class="js-modal-close close privacyCloseI-<?php the_ID(); ?>">×</a>
				<div class="layout-2-details layout-2-details-<?php the_ID(); ?>">

						<div class="form-group">
							<label for="dyn-tags">Privacy Options:</label>
							<div class="checkbox">
								  <?php
								  //$selectUsersListArray = explode(",", $selectUsersList[0]);
                                      $selectUsersListArray = $selectUsersList;
								     if(isset($privacyOption[0]) && $privacyOption[0] != "")
									 {
								       //echo $privacyOption[0]."<br>";
										 ?>
										    <label>Private
											  <?php
											     if($privacyOption[0] == "private")
												 {
											        ?>
													   <input type="radio" id="privacy-radio-<?php the_ID(); ?>" name="privacy-option-<?php the_ID(); ?>" class="dyn-select-file" class="form-control" value="private" checked="checked">
													<?php
												 }
												 else
												 {
													 ?>
													   <input type="radio" id="privacy-radio-<?php the_ID(); ?>" name="privacy-option-<?php the_ID(); ?>" class="dyn-select-file" class="form-control" value="private">
													<?php
												 }
											  ?>
											</label>
											<label>Public
											<input type="radio" id="privacy-radio1-<?php the_ID(); ?>" class="dyn-select-file" class="form-control" value="public" <?php if (isset($privacyOption[0]) && $privacyOption[0]=="public") echo "checked";?> name="privacy-option2-<?php the_ID(); ?>">
											</label>
										 <?php
									 }
									 else
									 {
										 ?>
										    <label>Private
											  <input type="radio" id="privacy-radio-<?php the_ID(); ?>" name="privacy-option-<?php the_ID(); ?>" class="dyn-select-file" class="form-control" value="private">
											</label>
											<label>Public
											 <input type="radio" id="privacy-radio1?>" name="privacy-option2-<?php the_ID(); ?>" class="dyn-select-file" class="form-control" value="public">
											</label>
										 <?php
									 }
								  ?>
							</div>
							 <?php
								 if(isset($privacyOption[0]) && $privacyOption[0] != "")
								 {
									   if($privacyOption[0] == "private")
									   {
										       $args1 = array(
												  'role' => 'free_user',
												  'orderby' => 'id',
												  'order' => 'desc'
											   );
											  $subscribers = get_users($args1);
										    ?>
											<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/jquery.tokenize.js"></script>
												<div class="select-box-<?php the_ID(); ?>">
													<?php
													  echo '<select class="tokenize-sample" id="tokenize-'.get_the_ID().'" name="select_users_list[]" style="width: 350px; height: 150px;" multiple="multiple">';
														  foreach ($subscribers as $user) {
															  if(get_current_user_id() == $user->id)
															  { }
															  else
															  {
														          if( in_array($user->id, $selectUsersListArray) )
																  {
																	   echo '<option value="'.$user->id.'" selected="selected">' . $user->display_name.'</option>';
																  }
                                                                  else{
																	   echo '<option value="'.$user->id.'">' . $user->display_name.'</option>';
																  }
															  }
														  }
													  echo '</select>';
													?>

													<script type="text/javascript">
																		  $(document).ready(function(){
												  $("#privacy-radio").click(function(){
													 $(".select-box").slideDown();
												  });
												  $("#privacy-radio1").click(function(){
													 $(".select-box").slideUp();
													 $('select#select_users_list option').removeAttr("selected");
												  });

											  });
											  $('#tokenize-<?php echo get_the_ID(); ?>').tokenize();
				                                datas: "bower.json.php"
										  </script>
										  <style> .tokenize-sample { width: 350px;; }</style>
												</div>

										   <?php
									   }
									   else
									   {
                                            ?>

												<div style="display:none;" class="select-box-<?php the_ID(); ?>">
													<?php
													  echo '<select class="tokenize-sample" id="tokenize-'.get_the_ID().'" name="select_users_list[]" style="width: 350px; height: 150px;" multiple="multiple">';
														  $args1 = array(
															  'role' => 'free_user',
															  'orderby' => 'id',
															  'order' => 'desc'
														   );
														  $subscribers = get_users($args1);
														  foreach ($subscribers as $user) {
															  if(get_current_user_id() == $user->id)
															  { }
															  else
															  {
																  echo '<option value="'.$user->id.'">' . $user->display_name.'</option>';
															  }
														  }
													  echo '</select>';
													?>

													<script type="text/javascript">
																		  $(document).ready(function(){
												  $("#privacy-radio").click(function(){
													 $(".select-box").slideDown();
												  });
												  $("#privacy-radio1").click(function(){
													 $(".select-box").slideUp();
													 //$('select#select_users_list option').removeAttr("selected");
												  });

											  });
											      $('#tokenize-<?php echo get_the_ID(); ?>').tokenize();
				                                         datas: "bower.json.php"
										  </script>
										  <style> .tokenize-sample { width: 350px; }</style>
												</div>

										   <?php
									   }
								 }
								 else
								 {
									   ?>
									        <div style="display:none;" class="select-box-<?php the_ID(); ?>">
												<?php
												  echo '<select class="tokenize-sample" id="tokenize-'.get_the_ID().'" name="select_users_list[]" style="width: 350px; height: 150px;" multiple="multiple">';
													  $args1 = array(
														  'role' => 'free_user',
														  'orderby' => 'id',
														  'order' => 'desc'
													   );
													  $subscribers = get_users($args1);
													  foreach ($subscribers as $user) {
														  if(get_current_user_id() == $user->id)
														  { }
														  else
														  {
															  echo '<option value="'.$user->id.'">' . $user->display_name.'</option>';
														  }
													  }
												  echo '</select>';
												?>

												<script type="text/javascript">
													 $(document).ready(function(){
												  $("#privacy-radio").click(function(){
													 $(".select-box").slideDown();
												  });
												  $("#privacy-radio1").click(function(){
													 $(".select-box").slideUp();
													 //$('select#select_users_list option').removeAttr("selected");
												  });

											  });
											     $('#tokenize-<?php echo get_the_ID(); ?>').tokenize();
				                                         datas: "bower.json.php"
										  </script>
										  <style> .tokenize-sample { width: 350px; }</style>
											</div>

									   <?php
								 }
							 ?>

							 <div id="msgLoader-<?php the_ID(); ?>"></div>
                             <input type="button" id="save-Privacy-Option-<?php the_ID(); ?>" name="save-privacy-option-<?php the_ID(); ?>" value="Save Privacy Option" currentUserID="<?php echo get_current_user_id(); ?>" postID="<?php echo get_the_ID(); ?>">
						</div>


					<script>
                      $(document).ready(function(){
						  $("#privacy-radio-<?php the_ID(); ?>").click(function(){
							 $("#privacy-radio1-<?php the_ID(); ?>").prop( "checked", false );
						     $(".select-box-<?php the_ID(); ?>").slideDown();
						  });
						  $("#privacy-radio1-<?php the_ID(); ?>").click(function(){
							 $("#privacy-radio-<?php the_ID(); ?>").prop( "checked", false );
						     $(".select-box-<?php the_ID(); ?>").slideUp();
							 $('select#select_users_list-<?php the_ID(); ?> option').removeAttr("selected");
						  });
						  $(".privacyCloseI-<?php the_ID(); ?>").click(function(){
							 $('#msgLoader-<?php the_ID(); ?>').html('');
						  });
						  $("#save-Privacy-Option-<?php the_ID(); ?>").click(function(){
							   var userID = $(this).attr('currentUserID');
							   var postID = $(this).attr('postID');
							   var select_users_list = $('#tokenize-'+postID).val();
							   if ( typeof(select_users_list) !== "undefined" && select_users_list !== null )
							   { }
						       else
							   {
								   select_users_list = '';
							   }
							   var privacyOptionV = "";
							   if($('#privacy-radio-<?php the_ID(); ?>').is(':checked'))
							   {
								   var privateChecked = "yes";
							   }
							   else
							   {
								   var privateChecked = "no";
							   }
							   if($('#privacy-radio1-<?php the_ID(); ?>').is(':checked'))
							   {
								   var publicChecked = "yes";
							   }
							   else
							   {
								   var publicChecked = "no";
							   }
							   if(privateChecked == "yes")
							   {
								   privacyOptionV = "private";
							   }
							   else if(publicChecked == "yes")
							   {
								   privacyOptionV = "public";
							   }
							   else
							   {
								   privacyOptionV = "";
							   }
                               $.ajax({
									type: 'POST',
									url: "<?php echo site_url(); ?>/update-Privacy.php",
									data: { userID: userID, postID: postID, privacyOptionV : privacyOptionV,
									select_users_list : select_users_list},
									beforeSend: function(){
									  $('#msgLoader-<?php the_ID(); ?>').html('<img src="<?php echo site_url(); ?>/wp-content/themes/videopress/images/loading.gif" />');
									},
									success: function(data){
										 $('#msgLoader-<?php the_ID(); ?>').html(data);

									}

								});
						  });


                      });

					 $('#tokenize').tokenize();
                    </script>
					<style> .tokenize-sample { width: 350px; }</style>
					<style>
                      #privacy-radio1-<?php the_ID(); ?>, #privacy-radio-<?php the_ID(); ?> {
                        padding: 5px;
                        text-align: center;
                      }

                     #select-box-<?php the_ID(); ?> {
                       padding: 50px;
                       display: none;
                     }
                    </style>
				</div>
			</div>
</div>
					<div class="modal-box" id="myModal-<?php the_ID(); ?>">
					 <div class="modal-body">
					 <a class="js-modal-close close">×</a>
					<div class="layout-2-details layout-2-details-<?php the_ID(); ?>">
						<div class="image-holder filesz dyn-file-sprite <?php echo apply_filters('dyn_class_by_mime', get_the_id()); ?>">
							<a href="<?php echo get_permalink(); ?>">
								<div class="hover-item dyn-file">
			                        <i class="fa fa-download"></i>
			                    </div>
							</a>
						</div>
						<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title();?></a></h6>
						<div>File Type: <?php echo apply_filters( 'dyn_file_type_by_mime', get_the_ID() ); ?></div>
						<?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
						<div class="video_excerpt_text one"><h4 class="tittle-h4">Description 3</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = '' , $message = 'No Description available' , $html = ' [...]' );?></div></div>
						<div class="video_excerpt_text two"><h4 class="tittle-h4">Reference</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_post_meta(get_the_ID(),'video_options_refr',true) , $length = '' , $message = 'No referance given' , $html = ' [...]' );?></div></div>
						<ul class="stats">
							<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
							<li><?php videopress_countviews( get_the_ID() ); ?></li>
							<li><i class="fa fa-wrench"></i>
							<?php
								$result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
								echo count($result);
							?> Endorsments</li>
							<li><i class="fa  fa-heart"></i>
							<?php
							  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
							  $result1 = $wpdb->get_results($sql_aux);
							  echo count($result1);
							?> Favorites</li>
							<!--end favorite-->
							<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
							<li><?php comments_number() ?></li>
							<li><i class="fa fa-download"></i> <?php echo apply_filters( 'dyn_number_of_download', get_the_id()); ?></li>
						</ul>

						<p><?php echo videopress_content('240'); ?></p>
						<?php if(get_the_author_id() == get_current_user_id()){ ?>
						<p><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>" ><i class="fa fa-trash-o"></i> Delete</a></p>
						<?php } ?>
					</div></div></div>



					<div class="layout-2-details layout-2-details-<?php the_ID(); ?>" style="display:none;">
						<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title();?></a></h6>
						<div>File Type: <?php echo apply_filters( 'dyn_file_type_by_mime', get_the_ID() ); ?></div>
						<?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
						<div class="video_excerpt_text one"><h4 class="tittle-h4">Description</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = 100 , $message = 'No Description available' , $html = '<a data-modal-id="myModal-'.get_the_ID().'"> [...]</a>' ); ?></div></div>
						<ul class="stats">
							<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
							<li><?php videopress_countviews( get_the_ID() ); ?></li>
							<li><i class="fa fa-wrench"></i>
							<?php
								$result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
								echo count($result);
							?> Endorsments</li>
							<li><i class="fa  fa-heart"></i>
							<?php
							  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
							  $result1 = $wpdb->get_results($sql_aux);
							  echo count($result1);
							?> Favorites</li>
							<!--end favorite-->
							<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
							<li><?php comments_number() ?></li>
							<li><i class="fa fa-download"></i> <?php echo apply_filters( 'dyn_number_of_download', get_the_id()); ?></li>
						</ul>

						<p><?php echo videopress_content('240'); ?>
						<?php if(get_the_author_id() == get_current_user_id()){ ?>
						        <a href="#" class="del-video" data-id="<?= get_the_ID(); ?>" ><i class="fa fa-trash-o"></i> Delete</a>
						        <a class="privacy-video" data-modal-id="myModalPrivacy-<?php the_ID(); ?>"><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;Privacy</a>
						<?php } ?>
						</p>
					</div>
				</div>
			</div>
				<?php
				endwhile;
				echo "</div><div class='paginate_division'>".paginate_links(array('add_args' => array('display_page' => 'favorite_file')))."</div>";
				endif;
				wp_reset_postdata();
			}else{
				echo "<div>No Files Available</div>";
			}?>
		</div>
		</div>
		<div class="book_main_section">
				<div class="thumb_cnt">
				<div class="iner_title">
				<label>Books</label>
				<a class="js-open-modal btn btn-info btn-sm pull-right" href="" data-modal-id="popup3">Add Book</a>
					<div id="popup3" class="modal-box">  
						<div class="modal-body collaboration">
							<header>
								<a href="#" class="js-modal-close close">×</a>
								<h3>Add Books</h3>
							</header>
							<?php echo do_shortcode('[dyn-book-upload-collaboration collaboration_id="'.$collaboration_id.'"]'); ?>
						</div>
					</div>

				</div>
				<div class="cover-twoblocks">
				<!--<div class="sortvideo-div">
					<div class="dyn-book-sort">
						<label></label>
					</div>
				</div>-->
				<div class="well well-sm">
					<strong>Views</strong>
					<div class="btn-group">
						<a href="#" id="list" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th-list">
						</span>List</a> <a href="#" id="grid" class="btn btn-default btn-sm"><span
							class="glyphicon glyphicon-th"></span>Grid</a>
					</div>
				</div>
			</div>
			</div>
				

				<div id="favoriteBooks">
			

			<script type="text/javascript">
			$(document).ready(function() {
				$('#favoriteBooks #products .item').addClass('grid-group-item');
				$('#favoriteBooks #list').click(function(event){event.preventDefault();$('#favoriteBooks #products .item').addClass('list-group-item');});
				$('#favoriteBooks #grid').click(function(event){event.preventDefault();$('#favoriteBooks #products .item').removeClass('list-group-item');$('#favoriteBooks #products .item').addClass('grid-group-item');});
			});
			</script>
			<?php
			if(count($dyn_book)>0){
				$args = array(
					'post__in' => $dyn_book,
					'paged' => $paged,
					'post_status' => 'publish',
					'posts_per_page' => 10,
					'post_type' => 'dyn_book'
				);
				$the_query = new WP_Query( $args );

				if ( $the_query->have_posts() ) :
				echo '<div id="products" class="list-group">';
				while ( $the_query->have_posts() ) : $the_query->the_post();
				?>
				<div class="item col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 post-<?php the_ID(); ?>">
					<div class="thumbnail layout-2-wrapper solid-bg">

						<div class="grid-6 first">


							<div class="image-holder">
								<a href="<?php the_permalink(); ?>">
								<div class="hover-item"></div>
								</a>
								<?php
									if (has_post_thumbnail()) {
										the_post_thumbnail('small-thumb', array( 'class' => 'layout-2-thumb' ) );
									} else {
										echo '<img class="layout-2-thumb wp-post-image" src="'. get_template_directory_uri() . '/images/no-image.png' .'" alt="" />';
									}
								?>

							</div>
							<div class="layout-title-box layout-title-box-<?php the_ID(); ?>" style="display:block;">
								<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php echo ctpreview( $content = get_the_title() , $length = 30 , $message = 'No Title available' , $html = ' [...]' ); ?></a></h6>
								<ul class="bottom-detailsul">
								<?php if($profile_id == get_current_user_id()){ ?>
								<li><p><a href="#" class="del-book" data-id="<?= get_the_ID(); ?>" ><i class="fa fa-trash-o"></i> Delete</a></p></li>
								<?php } ?>
								<li><a class="detailsblock-btn" data-modal-id="myModal-<?php the_ID(); ?>"><i class="fa fa-info-circle" aria-hidden="true"></i> Details</a></li>
								</ul>
							</div>
						</div>



						<div class="modal-box" id="myModal-<?php the_ID(); ?>">
						 <div class="modal-body">
						 <a class="js-modal-close close">×</a>
						<div class="layout-2-details layout-2-details-<?php the_ID(); ?>">
							<div class="image-holder dyn-book-file-sprite dyn-book-file-image">
								<a href="<?php the_permalink() ?>">
									<div class="hover-item">
										<i class="fa fa-download"></i>
									</div>
								</a>
							</div>
							<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title();?></a></h6>
							<div>File Type: Book</div>
							<?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
							<div class="video_excerpt_text one"><h4 class="tittle-h4">Description 4</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = '' , $message = 'No Description available' , $html = ' [...]' );?></div></div>
							<div class="video_excerpt_text two"><h4 class="tittle-h4">Reference</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_post_meta(get_the_ID(),'video_options_refr',true) , $length = '' , $message = 'No referance given' , $html = ' [...]' );?></div></div>
							<ul class="stats">
								<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
								<li><?php videopress_countviews( get_the_ID() ); ?></li>
								<li><i class="fa fa-wrench"></i>
								<?php
									$result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
									echo count($result);
								?> Endorsments</li>
								<li><i class="fa  fa-heart"></i>
								<?php
								  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
								  $result1 = $wpdb->get_results($sql_aux);
								  echo count($result1);
								?> Favorites</li>
								<!--end favorite-->
								<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
								<li><?php comments_number() ?></li>

							</ul>
							<div class="clear"></div>
							<p><?php echo videopress_content('240'); ?></p>
							<?php if($profile_id == get_current_user_id()){ ?>
							<p><a href="#" class="del-book" data-id="<?= get_the_ID(); ?>" ><i class="fa fa-trash-o"></i> Delete</a></p>
							<?php } ?>
						</div></div></div>



						<div class="layout-2-details layout-2-details-<?php the_ID(); ?>" style="display:none;">
							<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title();?></a></h6>
							<div>File Type: Book</div>
							<?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
							<div class="video_excerpt_text one"><h4 class="tittle-h4">Description</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = 100 , $message = 'No Description available' , $html = '<a data-modal-id="myModal-'.get_the_ID().'"> [...]</a>' ); ?></div></div>
							<ul class="stats">
								<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
								<li><?php videopress_countviews( get_the_ID() ); ?></li>
								<li><i class="fa fa-wrench"></i>
								<?php
									$result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
									echo count($result);
								?> Endorsments</li>
								<li><i class="fa  fa-heart"></i>
								<?php
								  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
								  $result1 = $wpdb->get_results($sql_aux);
								  echo count($result1);
								?> Favorites</li>
								<!--end favorite-->
								<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
								<li><?php comments_number() ?></li>

							</ul>
							
							<p><?php echo videopress_content('240'); ?></p>
							<?php if($profile_id == get_current_user_id()){ ?>
							<p><a href="#" class="del-book" data-id="<?= get_the_ID(); ?>" ><i class="fa fa-trash-o"></i> Delete</a></p>
							<?php } ?>
						</div>

						
					</div>
				</div>
				<?php
				endwhile;
				echo "</div><div class='paginate_division'>".paginate_links(array('add_args' => array('display_page' => 'favorite_book')))."<div>";
				endif;
				wp_reset_postdata();
			}else{
				echo "<div>No Books Available</div>";
			}
			?>
		</div>


			<?php }else{ ?>
				<h6><b>You have no permission!...</b></h6>
			<?php } ?>
			<?php endwhile; ?>

		





		</div>



	</div>
	</div>


</div>

<div class="spacing-40"></div>
<?php
function ctpreview( $content = '' , $length = '' , $message = '' , $html = '' ){
	if( $content == '' ){
		$content = $message ;
	}else{
		$content = strip_tags( $content );
		if( ( $length != '' ) && ( strlen( $content ) > $length ) ){
			$content = substr( $content , 0 , $length )." ".$html;
		}else{
			$content = $content ;
		}
	}
	return $content;
}
?>
<?php get_footer(); ?>