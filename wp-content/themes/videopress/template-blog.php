<?php /* Template Name: Blog Posts */ ?>
<?php get_header(); ?>

	<!-- Start Content -->
    <div class="container">

	<div class="grid-2-3">
    <?php
    // Set Args Parameters
		$args = array(
		'orderby'  => 'date',
		'order'    => 'DESC',
		'post_type' => 'newsblog',
		'paged'		=> $paged
		);
		
		// Start Loop
		query_posts($args);
		while ( have_posts() ) : the_post();
	?>
    
    <div class="blog-post">
    
    <div class="image-holder">
    <a href="<?php echo get_permalink(); ?>"><?php the_post_thumbnail('blog-post', array( 'class'=> 'blog-thumb' ) ); ?></a>
    </div>
    
    <h5><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h5>
    
    <ul class="stats">
      <li><?php videopress_countviews( get_the_ID() ); ?></li>
      <li><?php comments_number() ?></li>
      <li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
    </ul>
    <div class="clear"></div>
    
    <p><?php videopress_content('240'); ?></p>
    <a href="<?php echo get_permalink(); ?>" class="readmore">Read More <i class="fa fa-plus-circle"></i></a>
    </div>
    
    <?php endwhile; // End of Loop ?>
    
    <!-- Start Pagination -->
    <?php if( vp_option('vpt_option.pagination') == '1' ){
		videopress_pagination(); // Use Custom Pagination
	}else{
		echo '<div class="post_pagination">';
		posts_nav_link();
		echo'</div>';
	} ?>
    <!-- End Pagination -->
    </div>
    
    <div class="grid-3"><?php get_sidebar('blog'); ?></div>
	
    <div class="clear"></div>
	</div>
	<div class="spacing-40"></div>

<?php get_footer(); ?>