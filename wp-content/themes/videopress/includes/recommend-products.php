<?php
$paged = (get_query_var('page')) ? absint(get_query_var('page')) : 1;
$post = '';
$cat = isset($_GET['cat_type']) ? $_GET['cat_type'] : '';


if ($cat == 'recom-products')
    $rstart = ($paged == 1) ? 0 : intval($paged - 1) * 12;
else
    $rstart = 0;

if (!is_user_logged_in()) {
    $rtemp = 'SELECT a.post_id, user_id,
(SELECT Count(s1.post_id) FROM wp_favorite s1 WHERE s1.post_id = a.post_id) as countfavorite,
(SELECT Count(s1.post_id) FROM wp_endorsements s1 WHERE s1.post_id = a.post_id) as countendorsements,
(SELECT Count(comment_post_id) FROM wp_comments where comment_post_id=a.post_id) as countcoment,
(SELECT Count(g.post_id) FROM wp_dyn_review g inner join wp_dyn_rating m on g.review_id=m.review_id  where m.rating > 60 and g.post_id=a.post_id) as rating,
(SELECT Count(post_id) FROM wp_dyn_review where  post_id=a.post_id) as countreview,
(SELECT Count(post_id) FROM wp_user_views where  post_id=a.post_id) as countview,
(SELECT  b.meta_value FROM wp_postmeta b where  b.post_id=a.post_id and b.meta_value > 0  and b.meta_key="total_sales") as countsold
FROM ' . $wpdb->prefix . 'user_product_recommendation a, ' . $wpdb->prefix . 'posts
where ID=a.post_id
AND post_status="publish"
AND a.post_id NOT IN (SELECT post_id FROM ' . $wpdb->prefix . 'postmeta WHERE meta_key="privacy-option"
AND meta_value="private") GROUP BY a.post_id
ORDER BY countsold DESC,countfavorite DESC,rating DESC,countendorsements DESC,countcoment DESC,countview DESC,a.post_id DESC
';
    $rtquery = $rtemp . ' LIMIT ' . $rstart . ', 12';
} else if (is_user_logged_in()) {

    $rtemp = 'SELECT post_id, user_id,
 (SELECT Count(s1.post_id) FROM wp_favorite s1 WHERE s1.post_id = post_id) as countfavorite,
(SELECT Count(s1.post_id) FROM wp_endorsements s1 WHERE s1.post_id =post_id) as countendorsements,
(SELECT Count(g.comment_post_id) FROM wp_comments g where g.comment_post_id=post_id) as countcoment,
(SELECT  b.meta_value FROM wp_postmeta b where  b.post_id=post_id and b.meta_value > 0  and b.meta_key="total_sales") as countsold,
(SELECT Count(g.post_id) FROM wp_dyn_review g inner join wp_dyn_rating m on g.review_id=m.review_id  where m.rating > 60 and g.post_id=post_id) as rating,
(SELECT Count(o.post_id) FROM wp_dyn_review o where  o.post_id=post_id) as countreview,
(SELECT Count(h.post_id) FROM wp_user_views h where  h.post_id=post_id) as countview
 FROM '.$wpdb->prefix.'user_product_recommendation, '.$wpdb->prefix.'posts
    WHERE ID=post_id
AND post_status="publish"
AND post_id IN (SELECT post_id  FROM  ' . $wpdb->prefix . 'postmeta WHERE meta_key="_product_attributes")
AND user_id =' . get_current_user_id() . '
AND post_id NOT IN (SELECT post_id FROM ' . $wpdb->prefix . 'postmeta WHERE meta_key="privacy-option" AND meta_value="private")
UNION
SELECT post_id, user_id,
(SELECT Count(s1.post_id) FROM wp_favorite s1 WHERE s1.post_id = post_id) as countfavorite,
(SELECT Count(s1.post_id) FROM wp_endorsements s1 WHERE s1.post_id =post_id) as countendorsements,
(SELECT Count(g.comment_post_id) FROM wp_comments g where g.comment_post_id=post_id) as countcoment,
(SELECT  b.meta_value FROM wp_postmeta b where  b.post_id=post_id and b.meta_value > 0  and b.meta_key="total_sales") as countsold,
(SELECT Count(g.post_id) FROM wp_dyn_review g inner join wp_dyn_rating m on g.review_id=m.review_id  where m.rating > 60 and g.post_id=post_id) as rating,
(SELECT Count(o.post_id) FROM wp_dyn_review o where  o.post_id=post_id) as countreview,
(SELECT Count(h.post_id) FROM wp_user_views h where  h.post_id=post_id) as countview
FROM ' . $wpdb->prefix . 'user_product_recommendation, ' . $wpdb->prefix . 'posts WHERE ID=post_id
AND post_status="publish"
AND post_id IN (SELECT post_id  FROM  ' . $wpdb->prefix . 'postmeta WHERE meta_key="_product_attributes")
AND user_id =' . get_current_user_id() . '
AND post_id IN (SELECT post_id FROM ' . $wpdb->prefix . 'postmeta WHERE meta_key="privacy-option"
AND meta_value="private" AND post_id IN (SELECT post_id FROM ' . $wpdb->prefix . 'postmeta  WHERE meta_key = "select_users_list"
AND LENGTH(meta_value)>0  AND meta_value REGEXP "[[:<:]]' . get_current_user_id() . '[[:>:]]"))
ORDER BY countsold DESC,countfavorite DESC,rating DESC,countendorsements DESC,countcoment DESC,countview DESC,post_id DESC
';

    $rtquery = $rtemp . ' LIMIT ' . $rstart . ', 12';
    $rtotal = $wpdb->query($rtemp);

    if ($rtotal == 0) {
        $rtemp = 'SELECT post_id, user_id,
(SELECT Count(s1.post_id) FROM wp_favorite s1 WHERE s1.post_id = p.post_id) as countfavorite,
(SELECT Count(s1.post_id) FROM wp_endorsements s1 WHERE s1.post_id = p.post_id) as countendorsements,
(SELECT Count(comment_post_id) FROM wp_comments where comment_post_id=p.post_id) as countcoment,
(SELECT  b.meta_value FROM wp_postmeta b where  b.post_id=p.post_id and b.meta_value > 0  and b.meta_key="total_sales") as countsold,
(SELECT Count(g.post_id) FROM wp_dyn_review g inner join wp_dyn_rating m on g.review_id=m.review_id  where m.rating > 60 and g.post_id=p.post_id) as rating,
(SELECT Count(o.post_id) FROM wp_dyn_review o where  o.post_id=p.post_id) as countreview,
(SELECT Count(h.post_id) FROM wp_user_views h where  h.post_id=p.post_id) as countview
FROM ' . $wpdb->prefix . 'user_product_recommendation p, ' . $wpdb->prefix . 'posts
WHERE ID=post_id AND post_status="publish" AND post_id IN (SELECT post_id  FROM  ' . $wpdb->prefix . 'postmeta
WHERE meta_key="_product_attributes") AND post_id NOT IN (SELECT post_id FROM ' . $wpdb->prefix . 'postmeta
WHERE meta_key="privacy-option" AND meta_value="private") GROUP BY post_id
UNION
SELECT post_id, user_id,
(SELECT Count(s1.post_id) FROM wp_favorite s1 WHERE s1.post_id = p.post_id) as countfavorite,
(SELECT Count(s1.post_id) FROM wp_endorsements s1 WHERE s1.post_id = p.post_id) as countendorsements,
(SELECT Count(comment_post_id) FROM wp_comments where comment_post_id=p.post_id) as countcoment,
(SELECT  b.meta_value FROM wp_postmeta b where  b.post_id=p.post_id and b.meta_value > 0  and b.meta_key="total_sales") as countsold,
(SELECT Count(g.post_id) FROM wp_dyn_review g inner join wp_dyn_rating m on g.review_id=m.review_id  where m.rating > 60 and g.post_id=p.post_id) as rating,
(SELECT Count(o.post_id) FROM wp_dyn_review o where  o.post_id=p.post_id) as countreview,
(SELECT Count(h.post_id) FROM wp_user_views h where  h.post_id=p.post_id) as countview
FROM ' . $wpdb->prefix . 'user_product_recommendation p, ' . $wpdb->prefix . 'posts
WHERE ID=post_id AND post_status="publish" AND post_id IN (SELECT post_id  FROM  ' . $wpdb->prefix . 'postmeta
WHERE meta_key="_product_attributes") AND post_id IN (SELECT post_id FROM ' . $wpdb->prefix . 'postmeta
WHERE meta_key="privacy-option" AND meta_value="private" AND post_id IN (SELECT post_id FROM ' . $wpdb->prefix . 'postmeta
WHERE meta_key = "select_users_list" AND LENGTH(meta_value)>0  AND meta_value
REGEXP "[[:<:]]' . get_current_user_id() . '[[:>:]]")) GROUP BY post_id
ORDER BY countsold DESC,countfavorite DESC,rating DESC,countendorsements DESC,countcoment DESC,countview DESC,post_id DESC';
        $rtquery = $rtemp . ' LIMIT ' . $rstart . ', 12';
    }

}

//create custom loop for videos
$rresult = $wpdb->get_results($rtquery);
//echo  $wpdb->num_rows . 'Rows Found';

$rtotal = $wpdb->query($rtemp);


//echo  $wpdb->num_rows . 'Rows Found';

//$mime_types=array('video/mp4','video/x-ms-wmv');
//$args=array('numberposts' => 12, 'order'=> 'DESC', 'post_type'=>'attachment', 'post_mime_type'=> $mime_types, 'post_status'=>'inherit', 'posts_per_page'=>'12');
//$the_query= new WP_Query($args);

?>
<div class="list-group">
    <?php echo '<a href="#recom-products" id="recom-videos"><h5><b>RECOMMENDED PRODUCTS</b></h5></a>'; ?>

    <div class="row">
        <?php if ($rresult) {
            $ri = 0;


            foreach ($rresult as $rdata) {
                $rpost = get_post($rdata->post_id);
                $postID = $rdata->post_id;

                $thumbnail_id = get_post_thumbnail_id($postID);
                $thumbnail_url = wp_get_attachment_image_src($thumbnail_id, array(240, 240), true);
                $imgsrc = $thumbnail_url [0];
                $product_id = $postID;
                $_product = wc_get_product($product_id);
                $attachment_ids = $_product->get_gallery_attachment_ids();
                $units_sold = get_post_meta($postID, 'total_sales', true);


                $privacyOption = get_post_meta($postID, 'privacy-option');
                $selectUsersList = get_post_meta($postID, 'select_users_list');
                $post_author = $rpost->post_author;
                $user_ID = get_current_user_id();
                $selectUsersList = explode(",", $selectUsersList[0]);
//$post_id=$data->ID;
                $video_author = get_the_author_meta('display_name', $post_author);

                $refr = get_post_meta($postID, 'video_options_refr', true);
                if ($refr) {
                } else {
                    $refr = 'No reference given';
                }

                $thumbnail_id = get_post_thumbnail_id($postID);
                $thumbnail_url = wp_get_attachment_image_src($thumbnail_id, array(240, 240), true);
                $imgsrc = $thumbnail_url [0];

                $revnum = apply_filters('dyn_number_of_post_review', $postID, "post");

                $endors = $wpdb->get_results('SELECT user_id FROM ' . $wpdb->prefix . 'endorsements WHERE post_id = ' . $postID);
                $endorsnum = count($endors);

                $favs = $wpdb->get_results('SELECT user_id FROM ' . $wpdb->prefix . 'favorite WHERE post_id=' . $postID);
                $favsnum = count($favs);


                if (!is_user_logged_in()) {  // case where user is not logged in
                    if (isset($privacyOption[0]) and ($privacyOption[0] != "private")) {
//if( ($ri % 6) == 0){echo '</div><div class="row">'; }
                        ?>
                        <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 text-center"
                             style="padding: 5px 5px 20px 5px !important;  margin:0px !important; height:300px;">

                            <div class="thumbnail layout-2-wrapper solid-bg"
                                 style="height:100%; width:100% !important; box-shadow: 0 0 10px 1px #666; border-radius:5px; padding:5px">


                                <div class="image-holder">
                                    <a href="<?php echo get_permalink($postID); ?>">
                                        <div class="hover-item"></div>
                                    </a>

                                    <?php
                                    if (has_post_thumbnail($postID)) {
                                        echo get_the_post_thumbnail($postID, 'medium-thumb', array('class' => 'layout-3-thumb', 'style' => "height:140px; max-width:220px;"));
                                    } else {
                                        echo '<img src="http://www.doityourselfnation.org/bit_bucket/wp-content/themes/videopress/images/placeholder.jpg" class="layout-3-thumb" style="height:140px; max-width:220px;">';
                                    } ?>


                                </div>
                                <div class="layout-title-box text-center" style="display:block;">
                                    <h6 class="layout-title"
                                        style="max-width:100%; word-wrap: break-word; height:40px;">
                                        <?php
                                        preg_match_all('/(\w+\s)/', $rpost->post_title, $matches);

                                        if (count($matches[0]) && count($matches[1]) > 4) {
                                            ?>
                                            <a href="<?php echo get_permalink($postID) ?>"
                                               title="<?php echo $rpost->post_title; ?>"
                                               style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $matches[1][0] . $matches[1][1] . $matches[1][2] . '[...]'; ?></a>
                                            <?php
                                        } else if (strlen($rpost->post_title) > 30) {
                                            ?>
                                            <a href="<?php echo get_permalink($postID) ?>"
                                               title="<?php echo $rpost->post_title; ?>"
                                               style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo substr($rpost->post_title, 0, 30) . ' [...]'; ?></a>

                                            <?php
                                        } else {
                                            ?>
                                            <a href="<?php echo get_permalink($postID) ?>"
                                               title="<?php echo $rpost->post_title; ?>"
                                               style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $rpost->post_title; ?></a>

                                        <?php } ?>
                                    </h6>
                                    <!-- <ul class="stats">
        <li ><?php videopress_displayviews($postID); ?></li>
        <li><?php //echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago';
                                    ?></li>
        <li>Author: <font color="red"><?php //echo get_the_author($postID);
                                    ?></font></li>

    </ul>
    -->
                                    <ul class="bottom-detailsul" style="vertical-align:baseline;">
                                        <li><a class="detailsblock-btn" data-modal-id="products"
                                               data-author="<?php echo $video_author; ?>"
                                               data-regularprice='<?php echo $_product->get_price_html(); ?>'
                                               data-stock="<?php echo $_product->get_total_stock(); ?>"
                                               data-sold="<?php echo sprintf(__(' %s', 'woocommerce'), $units_sold); ?> Sold"
                                               data-allimg="<?php foreach ($attachment_ids as $attachment_id) {
                                                   echo '<img src=' . wp_get_attachment_url($attachment_id) . '>';
                                               } ?>"
                                               data-time="<?php echo human_time_diff(get_the_time('U', $postID), current_time('timestamp')) . ' ago'; ?>"
                                               data-views="<?php videopress_displayviews($postID); ?>"
                                               data-title="<?php echo $post->post_title; ?>"
                                               data-permalink="<?php echo get_permalink($postID); ?>"
                                               data-content="<?php echo strip_tags($post->post_content); ?>"
                                               data-image="<?php echo $imgsrc; ?>"
                                               data-comments="<?php echo $post->comment_count; ?>"
                                               data-ref="<?php echo $refr ?>" data-endorse="<?php echo $endorsnum ?>"
                                               data-favr="<?php echo $favsnum; ?> " data-revs="<?php echo $revnum ?>">
                                                <i class="fa fa-info-circle" aria-hidden="true"></i> Details
                                            </a>
                                        </li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                        <?php
                        $ri++;
                    } else {
                        if (!isset($privacyOption[0])) {
//if( ($ri % 6) == 0){echo '</div><div class="row">'; }
                            ?>
                            <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 text-center"
                                 style="padding: 5px 5px 20px 5px !important;  margin:0px !important; height:300px;">

                                <div class="thumbnail layout-2-wrapper solid-bg"
                                     style="height:100%; width:100% !important; box-shadow: 0 0 10px 1px #666; border-radius:5px; padding:5px">


                                    <div class="image-holder">
                                        <a href="<?php echo get_permalink($postID); ?>">
                                            <div class="hover-item"></div>
                                        </a>

                                        <?php
                                        if (has_post_thumbnail($postID)) {
                                            echo get_the_post_thumbnail($postID, 'medium-thumb', array('class' => 'layout-3-thumb', 'style' => "height:140px; max-width:220px;"));
                                        } else {
                                            echo '<img src="http://www.doityourselfnation.org/bit_bucket/wp-content/themes/videopress/images/placeholder.jpg" class="layout-3-thumb" style="height:140px; max-width:220px;">';
                                        } ?>


                                    </div>
                                    <div class="layout-title-box text-center" style="display:block;">
                                        <h6 class="layout-title"
                                            style="max-width:100%; word-wrap: break-word; height:40px;">
                                            <?php
                                            preg_match_all('/(\w+\s)/', $rpost->post_title, $matches);

                                            if (count($matches[0]) && count($matches[1]) > 4) {
                                                ?>
                                                <a href="<?php echo get_permalink($postID) ?>"
                                                   title="<?php echo $rpost->post_title; ?>"
                                                   style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $matches[1][0] . $matches[1][1] . $matches[1][2] . '[...]'; ?></a>
                                                <?php
                                            } else if (strlen($rpost->post_title) > 30) {
                                                ?>
                                                <a href="<?php echo get_permalink($postID) ?>"
                                                   title="<?php echo $rpost->post_title; ?>"
                                                   style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo substr($rpost->post_title, 0, 30) . ' [...]'; ?></a>

                                                <?php
                                            } else {
                                                ?>
                                                <a href="<?php echo get_permalink($postID) ?>"
                                                   title="<?php echo $rpost->post_title; ?>"
                                                   style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $rpost->post_title; ?></a>

                                            <?php } ?>
                                        </h6>
                                        <!-- <ul class="stats">
        <li ><?php videopress_displayviews($postID); ?></li>
        <li><?php //echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago';
                                        ?></li>
        <li>Author: <font color="red"><?php //echo get_the_author($postID);
                                        ?></font></li>

    </ul>
    -->
                                        <ul class="bottom-detailsul" style="vertical-align:baseline;">
                                            <li><a class="detailsblock-btn" data-modal-id="products"
                                                   data-author="<?php echo $video_author; ?>"
                                                   data-regularprice='<?php echo $_product->get_price_html(); ?>'
                                                   data-stock="<?php echo $_product->get_total_stock(); ?>"
                                                   data-sold="<?php echo sprintf(__(' %s', 'woocommerce'), $units_sold); ?> Sold"
                                                   data-allimg="<?php foreach ($attachment_ids as $attachment_id) {
                                                       echo '<img src=' . wp_get_attachment_url($attachment_id) . '>';
                                                   } ?>"
                                                   data-time="<?php echo human_time_diff(get_the_time('U', $postID), current_time('timestamp')) . ' ago'; ?>"
                                                   data-views="<?php videopress_displayviews($postID); ?>"
                                                   data-title="<?php echo $post->post_title; ?>"
                                                   data-permalink="<?php echo get_permalink($postID); ?>"
                                                   data-content="<?php echo strip_tags($post->post_content); ?>"
                                                   data-image="<?php echo $imgsrc; ?>"
                                                   data-comments="<?php echo $post->comment_count; ?>"
                                                   data-ref="<?php echo $refr ?>"
                                                   data-endorse="<?php echo $endorsnum ?>"
                                                   data-favr="<?php echo $favsnum; ?> "
                                                   data-revs="<?php echo $revnum ?>">
                                                    <i class="fa fa-info-circle" aria-hidden="true"></i> Details
                                                </a>
                                            </li>
                                        </ul>
                                    </div>

                                </div>
                            </div>
                            <?php
                            $ri++;
                        }
                    }
                } else {  // case where user is logged in
                    if ($post_author == $user_ID) {    // Case where logged in User is same as Video Author User
//if( ($ri % 6) == 0){echo '</div><div class="row">'; }
                        ?>
                        <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 text-center"
                             style="padding: 5px 5px 20px 5px !important;  margin:0px !important; height:300px;">

                            <div class="thumbnail layout-2-wrapper solid-bg"
                                 style="height:100%; width:100% !important; box-shadow: 0 0 10px 1px #666; border-radius:5px; padding:5px">


                                <div class="image-holder">
                                    <a href="<?php echo get_permalink($postID); ?>">
                                        <div class="hover-item"></div>
                                    </a>

                                    <?php
                                    if (has_post_thumbnail($postID)) {
                                        echo get_the_post_thumbnail($postID, 'medium-thumb', array('class' => 'layout-3-thumb', 'style' => "height:140px; max-width:220px;"));
                                    } else {
                                        echo '<img src="http://www.doityourselfnation.org/bit_bucket/wp-content/themes/videopress/images/placeholder.jpg" class="layout-3-thumb" style="height:140px; max-width:220px;">';
                                    } ?>


                                </div>
                                <div class="layout-title-box text-center" style="display:block;">
                                    <h6 class="layout-title"
                                        style="max-width:100%; word-wrap: break-word; height:40px;">
                                        <?php
                                        preg_match_all('/(\w+\s)/', $rpost->post_title, $matches);

                                        if (count($matches[0]) && count($matches[1]) > 4) {
                                            ?>
                                            <a href="<?php echo get_permalink($postID) ?>"
                                               title="<?php echo $rpost->post_title; ?>"
                                               style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $matches[1][0] . $matches[1][1] . $matches[1][2] . '[...]'; ?></a>
                                            <?php
                                        } else if (strlen($rpost->post_title) > 30) {
                                            ?>
                                            <a href="<?php echo get_permalink($postID) ?>"
                                               title="<?php echo $rpost->post_title; ?>"
                                               style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo substr($rpost->post_title, 0, 30) . ' [...]'; ?></a>

                                            <?php
                                        } else {
                                            ?>
                                            <a href="<?php echo get_permalink($postID) ?>"
                                               title="<?php echo $rpost->post_title; ?>"
                                               style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $rpost->post_title; ?></a>

                                        <?php } ?>
                                    </h6>
                                    <!-- <ul class="stats">
        <li ><?php videopress_displayviews($postID); ?></li>
        <li><?php //echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago';
                                    ?></li>
        <li>Author: <font color="red"><?php //echo get_the_author($postID);
                                    ?></font></li>

    </ul>
    -->
                                    <ul class="bottom-detailsul" style="vertical-align:baseline;">
                                        <li><a class="detailsblock-btn" data-modal-id="products"
                                               data-author="<?php echo $video_author; ?>"
                                               data-regularprice='<?php echo $_product->get_price_html(); ?>'
                                               data-stock="<?php echo $_product->get_total_stock(); ?>"
                                               data-sold="<?php echo sprintf(__(' %s', 'woocommerce'), $units_sold); ?> Sold"
                                               data-allimg="<?php foreach ($attachment_ids as $attachment_id) {
                                                   echo '<img src=' . wp_get_attachment_url($attachment_id) . '>';
                                               } ?>"
                                               data-time="<?php echo human_time_diff(get_the_time('U', $postID), current_time('timestamp')) . ' ago'; ?>"
                                               data-views="<?php videopress_displayviews($postID); ?>"
                                               data-title="<?php echo $post->post_title; ?>"
                                               data-permalink="<?php echo get_permalink($postID); ?>"
                                               data-content="<?php echo strip_tags($post->post_content); ?>"
                                               data-image="<?php echo $imgsrc; ?>"
                                               data-comments="<?php echo $post->comment_count; ?>"
                                               data-ref="<?php echo $refr ?>" data-endorse="<?php echo $endorsnum ?>"
                                               data-favr="<?php echo $favsnum; ?> " data-revs="<?php echo $revnum ?>">
                                                <i class="fa fa-info-circle" aria-hidden="true"></i> Details
                                            </a>
                                        </li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                        <?php
                        $ri++;
                    } else {    // Case where logged in User is not same as Video Author User
                        if (isset($privacyOption[0]) and $privacyOption[0] != "private") {
//if( ($ri % 6) == 0){echo '</div><div class="row">'; }
                            ?>
                            <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 text-center"
                                 style="padding: 5px 5px 20px 5px !important;  margin:0px !important; height:300px;">

                                <div class="thumbnail layout-2-wrapper solid-bg"
                                     style="height:100%; width:100% !important; box-shadow: 0 0 10px 1px #666; border-radius:5px; padding:5px">


                                    <div class="image-holder">
                                        <a href="<?php echo get_permalink($postID); ?>">
                                            <div class="hover-item"></div>
                                        </a>

                                        <?php
                                        if (has_post_thumbnail($postID)) {
                                            echo get_the_post_thumbnail($postID, 'medium-thumb', array('class' => 'layout-3-thumb', 'style' => "height:140px; max-width:220px;"));
                                        } else {
                                            echo '<img src="http://www.doityourselfnation.org/bit_bucket/wp-content/themes/videopress/images/placeholder.jpg" class="layout-3-thumb" style="height:140px; max-width:220px;">';
                                        } ?>


                                    </div>
                                    <div class="layout-title-box text-center" style="display:block;">
                                        <h6 class="layout-title"
                                            style="max-width:100%; word-wrap: break-word; height:40px;">
                                            <?php
                                            preg_match_all('/(\w+\s)/', $rpost->post_title, $matches);

                                            if (count($matches[0]) && count($matches[1]) > 4) {
                                                ?>
                                                <a href="<?php echo get_permalink($postID) ?>"
                                                   title="<?php echo $rpost->post_title; ?>"
                                                   style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $matches[1][0] . $matches[1][1] . $matches[1][2] . '[...]'; ?></a>
                                                <?php
                                            } else if (strlen($rpost->post_title) > 30) {
                                                ?>
                                                <a href="<?php echo get_permalink($postID) ?>"
                                                   title="<?php echo $rpost->post_title; ?>"
                                                   style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo substr($rpost->post_title, 0, 30) . ' [...]'; ?></a>
                                                <?php
                                            } else {
                                                ?>
                                                <a href="<?php echo get_permalink($postID) ?>"
                                                   title="<?php echo $rpost->post_title; ?>"
                                                   style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $rpost->post_title; ?></a>

                                            <?php } ?>
                                        </h6>
                                        <!-- <ul class="stats">
        <li ><?php videopress_displayviews($postID); ?></li>
        <li><?php //echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago';
                                        ?></li>
        <li>Author: <font color="red"><?php //echo get_the_author($postID);
                                        ?></font></li>

    </ul>
    -->
                                        <ul class="bottom-detailsul" style="vertical-align:baseline;">
                                            <li><a class="detailsblock-btn" data-modal-id="products"
                                                   data-author="<?php echo $video_author; ?>"
                                                   data-regularprice='<?php echo $_product->get_price_html(); ?>'
                                                   data-stock="<?php echo $_product->get_total_stock(); ?>"
                                                   data-sold="<?php echo sprintf(__(' %s', 'woocommerce'), $units_sold); ?> Sold"
                                                   data-allimg="<?php foreach ($attachment_ids as $attachment_id) {
                                                       echo '<img src=' . wp_get_attachment_url($attachment_id) . '>';
                                                   } ?>"
                                                   data-time="<?php echo human_time_diff(get_the_time('U', $postID), current_time('timestamp')) . ' ago'; ?>"
                                                   data-views="<?php videopress_displayviews($postID); ?>"
                                                   data-title="<?php echo $post->post_title; ?>"
                                                   data-permalink="<?php echo get_permalink($postID); ?>"
                                                   data-content="<?php echo strip_tags($post->post_content); ?>"
                                                   data-image="<?php echo $imgsrc; ?>"
                                                   data-comments="<?php echo $post->comment_count; ?>"
                                                   data-ref="<?php echo $refr ?>"
                                                   data-endorse="<?php echo $endorsnum ?>"
                                                   data-favr="<?php echo $favsnum; ?> "
                                                   data-revs="<?php echo $revnum ?>">
                                                    <i class="fa fa-info-circle" aria-hidden="true"></i> Details
                                                </a>
                                            </li>
                                        </ul>
                                    </div>

                                </div>
                            </div>
                            <?php
                            $ri++;
                        } else {
                            if (!isset($privacyOption[0])) {
//if( ($ri % 6) == 0){echo '</div><div class="row">'; }
                                ?>
                                <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 text-center"
                                     style="padding: 5px 5px 20px 5px !important;  margin:0px !important; height:300px;">

                                    <div class="thumbnail layout-2-wrapper solid-bg"
                                         style="height:100%; width:100% !important; box-shadow: 0 0 10px 1px #666; border-radius:5px; padding:5px">


                                        <div class="image-holder">
                                            <a href="<?php echo get_permalink($postID); ?>">
                                                <div class="hover-item"></div>
                                            </a>

                                            <?php
                                            if (has_post_thumbnail($postID)) {
                                                echo get_the_post_thumbnail($postID, 'medium-thumb', array('class' => 'layout-3-thumb', 'style' => "height:140px; max-width:220px;"));
                                            } else {
                                                echo '<img src="http://www.doityourselfnation.org/bit_bucket/wp-content/themes/videopress/images/placeholder.jpg" class="layout-3-thumb" style="height:140px; max-width:220px;">';
                                            } ?>


                                        </div>
                                        <div class="layout-title-box text-center" style="display:block;">
                                            <h6 class="layout-title"
                                                style="max-width:100%; word-wrap: break-word; height:40px;">
                                                <?php
                                                preg_match_all('/(\w+\s)/', $rpost->post_title, $matches);

                                                if (count($matches[0]) && count($matches[1]) > 4) {
                                                    ?>
                                                    <a href="<?php echo get_permalink($postID) ?>"
                                                       title="<?php echo $rpost->post_title; ?>"
                                                       style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $matches[1][0] . $matches[1][1] . $matches[1][2] . '[...]'; ?></a>
                                                    <?php
                                                } else if (strlen($rpost->post_title) > 30) {
                                                    ?>
                                                    <a href="<?php echo get_permalink($postID) ?>"
                                                       title="<?php echo $rpost->post_title; ?>"
                                                       style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo substr($rpost->post_title, 0, 30) . ' [...]'; ?></a>

                                                    <?php
                                                } else {
                                                    ?>
                                                    <a href="<?php echo get_permalink($postID) ?>"
                                                       title="<?php echo $rpost->post_title; ?>"
                                                       style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $rpost->post_title; ?></a>

                                                <?php } ?>
                                            </h6>
                                            <!-- <ul class="stats">
        <li ><?php videopress_displayviews($postID); ?></li>
        <li><?php //echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago';
                                            ?></li>
        <li>Author: <font color="red"><?php //echo get_the_author($postID);
                                            ?></font></li>

    </ul>
    -->
                                            <ul class="bottom-detailsul" style="vertical-align:baseline;">
                                                <li><a class="detailsblock-btn" data-modal-id="products"
                                                       data-author="<?php echo $video_author; ?>"
                                                       data-regularprice='<?php echo $_product->get_price_html(); ?>'
                                                       data-stock="<?php echo $_product->get_total_stock(); ?>"
                                                       data-sold="<?php echo sprintf(__(' %s', 'woocommerce'), $units_sold); ?> Sold"
                                                       data-allimg="<?php foreach ($attachment_ids as $attachment_id) {
                                                           echo '<img src=' . wp_get_attachment_url($attachment_id) . '>';
                                                       } ?>"
                                                       data-time="<?php echo human_time_diff(get_the_time('U', $postID), current_time('timestamp')) . ' ago'; ?>"
                                                       data-views="<?php videopress_displayviews($postID); ?>"
                                                       data-title="<?php echo $post->post_title; ?>"
                                                       data-permalink="<?php echo get_permalink($postID); ?>"
                                                       data-content="<?php echo strip_tags($post->post_content); ?>"
                                                       data-image="<?php echo $imgsrc; ?>"
                                                       data-comments="<?php echo $post->comment_count; ?>"
                                                       data-ref="<?php echo $refr ?>"
                                                       data-endorse="<?php echo $endorsnum ?>"
                                                       data-favr="<?php echo $favsnum; ?> "
                                                       data-revs="<?php echo $revnum ?>">
                                                        <i class="fa fa-info-circle" aria-hidden="true"></i> Details
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>

                                    </div>
                                </div>
                                <?php
                                $ri++;
                            } else {
                                if (is_array($selectUsersList) and count($selectUsersList) > 0) {   // case where user access list is available
                                    if (in_array($user_ID, $selectUsersList)) {
                                        //f( ($ri % 6) == 0){echo '</div><div class="row">'; }
                                        ?>
                                        <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 text-center"
                                             style="padding: 5px 5px 20px 5px !important;  margin:0px !important; height:300px;">

                                            <div class="thumbnail layout-2-wrapper solid-bg"
                                                 style="height:100%; width:100% !important; box-shadow: 0 0 10px 1px #666; border-radius:5px; padding:5px">


                                                <div class="image-holder">
                                                    <a href="<?php echo get_permalink($postID); ?>">
                                                        <div class="hover-item"></div>
                                                    </a>

                                                    <?php
                                                    if (has_post_thumbnail($postID)) {
                                                        echo get_the_post_thumbnail($postID, 'medium-thumb', array('class' => 'layout-3-thumb', 'style' => "height:140px; max-width:220px;"));
                                                    } else {
                                                        echo '<img src="http://www.doityourselfnation.org/bit_bucket/wp-content/themes/videopress/images/placeholder.jpg" class="layout-3-thumb" style="height:140px; max-width:220px;">';
                                                    } ?>


                                                </div>
                                                <div class="layout-title-box text-center" style="display:block;">
                                                    <h6 class="layout-title"
                                                        style="max-width:100%; word-wrap: break-word; height:40px;">
                                                        <?php
                                                        preg_match_all('/(\w+\s)/', $rpost->post_title, $matches);

                                                        if (count($matches[0]) && count($matches[1]) > 4) {
                                                            ?>
                                                            <a href="<?php echo get_permalink($postID) ?>"
                                                               title="<?php echo $rpost->post_title; ?>"
                                                               style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $matches[1][0] . $matches[1][1] . $matches[1][2] . '[...]'; ?></a>
                                                            <?php
                                                        } else if (strlen($rpost->post_title) > 30) {
                                                            ?>
                                                            <a href="<?php echo get_permalink($postID) ?>"
                                                               title="<?php echo $rpost->post_title; ?>"
                                                               style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo substr($rpost->post_title, 0, 30) . ' [...]'; ?></a>

                                                            <?php
                                                        } else {
                                                            ?>
                                                            <a href="<?php echo get_permalink($postID) ?>"
                                                               title="<?php echo $rpost->post_title; ?>"
                                                               style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $rpost->post_title; ?></a>

                                                        <?php } ?>
                                                    </h6>
                                                    <!-- <ul class="stats">
        <li ><?php videopress_displayviews($postID); ?></li>
        <li><?php //echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago';
                                                    ?></li>
        <li>Author: <font color="red"><?php //echo get_the_author($postID);
                                                    ?></font></li>

    </ul>
    -->
                                                    <ul class="bottom-detailsul" style="vertical-align:baseline;">
                                                        <li><a class="detailsblock-btn" data-modal-id="products"
                                                               data-author="<?php echo $video_author; ?>"
                                                               data-regularprice='<?php echo $_product->get_price_html(); ?>'
                                                               data-stock="<?php echo $_product->get_total_stock(); ?>"
                                                               data-sold="<?php echo sprintf(__(' %s', 'woocommerce'), $units_sold); ?> Sold"
                                                               data-allimg="<?php foreach ($attachment_ids as $attachment_id) {
                                                                   echo '<img src=' . wp_get_attachment_url($attachment_id) . '>';
                                                               } ?>"
                                                               data-time="<?php echo human_time_diff(get_the_time('U', $postID), current_time('timestamp')) . ' ago'; ?>"
                                                               data-views="<?php videopress_displayviews($postID); ?>"
                                                               data-title="<?php echo $post->post_title; ?>"
                                                               data-permalink="<?php echo get_permalink($postID); ?>"
                                                               data-content="<?php echo strip_tags($post->post_content); ?>"
                                                               data-image="<?php echo $imgsrc; ?>"
                                                               data-comments="<?php echo $post->comment_count; ?>"
                                                               data-ref="<?php echo $refr ?>"
                                                               data-endorse="<?php echo $endorsnum ?>"
                                                               data-favr="<?php echo $favsnum; ?> "
                                                               data-revs="<?php echo $revnum ?>">
                                                                <i class="fa fa-info-circle" aria-hidden="true"></i>
                                                                Details
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>

                                            </div>
                                        </div>
                                        <?php
                                        $ri++;
                                    } else {  // case where user is not in access list

                                    }
                                } else {
                                }
                            }
                        }
                    }
                }


            } // foreach
        } //if mysql result

        ?>


    </div>
    <div class="row" data-anchor="recom-products">
        <div class="col-sm-12">
            <nav class="pull-right">
                <?php

                if ($cat == 'recom-videos') {
                    $rvcurrent = max(1, get_query_var('page'));
                } else {
                    $rvcurrent = 1;
                }


                $paginate = paginate_links(array(
                    'format' => 'page/%#%/',
                    'current' => $rvcurrent,
                    'type' => 'array',
                    'mid_size' => 4,
                    'end_size' => 1,
                    'total' => ceil($rtotal / 12)
                ));

                $pcount = count($paginate);


                $maxp = ceil(($rtotal / 12) / 2);

                if ($rvcurrent <= ceil($rtotal / 12)) {
                    if (ceil($rtotal / 12) < 5) {
                        foreach ($paginate as $plnk) {
                            echo $plnk . '&nbsp;';
                        }
                    } else if (ceil($rtotal / 12) >= 5) {


                        if ($rvcurrent <= 6) {
                            if ($rvcurrent > 2) {
                                echo $paginate[0] . '&nbsp;';
                            }
                            if ($paginate[$rvcurrent - 2]) {
                                echo $paginate[$rvcurrent - 2] . '&nbsp;';
                            }
                            echo $paginate[$rvcurrent - 1] . '&nbsp;';
                            echo $paginate[$rvcurrent] . '&nbsp;';
                            if (isset($paginate[$rvcurrent + 1]) && (($pcount - 1) > $rvcurrent + 1)) {
                                echo $paginate[$rvcurrent + 1] . '&nbsp;';
                            }
                            if (isset($paginate[$rvcurrent + 2]) && (($pcount - 1) > $rvcurrent + 2)) {
                                echo $paginate[$rvcurrent + 2] . '&nbsp;';
                            }

                        } else if ($rvcurrent >= 7) {
                            echo $paginate[0] . '&nbsp;';
                            echo $paginate[5] . '&nbsp;';
                            echo $paginate[6] . '&nbsp;';
                            echo $paginate[7] . '&nbsp;';
                            if (isset($paginate[8]) && (($pcount - 1) > 8)) {
                                echo $paginate[8] . '&nbsp;';
                            }
                            if (isset($paginate[9]) && (($pcount - 1) > 9)) {
                                echo $paginate[9] . '&nbsp;';
                            }

                        }


                        if ($rvcurrent <= (ceil($rtotal / 12) - 1)) {
                            echo $paginate[$pcount - 1];
                        }

                    }

                }


                ?>
            </nav>
        </div>
    </div>
</div>



