<h6 class="title-bar"><span>Related File/Videos</span></h6>
<div class="spacer-dark"></div>
<?php // set a cookie from the last category they were on, and then use that as the category id ?>

    <!-- Start Categorized post -->
    <div class="layout-3 related-videos">
    <div class="homepage-video-holder" data-slick='{"slidesToShow": 5, "slidesToScroll": 1}'>
    <?php
	// Set Variable
	$videopress_gridcounter = 0;
	$videopress_category = get_the_category(); 
	
	if( isset( $videopress_category[0]->term_taxonomy_id ) == '' ){
		// Set Default category to 1 if none
		$videopress_category_id = $videopress_category[0]->term_taxonomy_id;
	}else{
		// else assign value to a category
		$videopress_category_id = $_COOKIE['diynationrelatedvideo'];
		//$videopress_category_id = $videopress_category[0]->term_taxonomy_id;
	}
	$args = array( 'post_type'=> array('post', 'dyn_file','product'), 'numberposts' => 4, 'order'=> 'DESC', 'category__and' => $postCats, 'tag__and' => $tagsNameArray );
	/* $args = array( 'numberposts' => 4, 'order'=> 'DESC', 'category'=> $videopress_category_id, 'exclude'=> $post->ID ); */
	$postslist = query_posts( $args );
	foreach($postslist as $post) :  setup_postdata($post);
	$videopress_gridcounter++;
	?>
    
    <!--<div class="grid-one-fourth <?php if( $videopress_gridcounter == '1' ){ echo ' first'; }elseif( $videopress_gridcounter == '4' ){ echo ' last'; } ?>">-->
    <div class="home-page-scroller <?php if( $videopress_gridcounter == '1' ){ echo ' first-homepage-video'; }elseif( $videopress_gridcounter == '6' ){ echo ' last'; } ?>">
    
    <?php get_template_part( 'related', get_post_type() ); ?>



    </div>
    
    <?php endforeach; wp_reset_query(); ?>
    <div class="clear"></div>
    </div>
    <!-- End Categorized Post -->
    
    </div> <!-- end -->
    <div class="scroll-left scroll-left_3"><span class="icon-arrow-left"> </span></div>
    <div class="scroll-right scroll-right_3"><span class="icon-arrow-right"> </span></div>
    <div class="spacing-40"></div>