<?php
$paged = ( get_query_var( 'page' ) ) ? absint( get_query_var( 'page' ) ) : 1;
$post='';

$cat=isset($_GET['cat_type']) ? $_GET['cat_type'] : '';


if($cat=='new-books')
$bkstart = ($paged==1) ? 0 : intval($paged-1) * 12;
else
$bkstart=0;


//create custom loop for books
if(!is_user_logged_in()){
$bktemp= 'SELECT * FROM ' .$wpdb->prefix.'posts WHERE post_type="dyn_book" AND post_status="publish" AND ID NOT IN (SELECT post_id FROM '.$wpdb->prefix.'postmeta WHERE meta_key="privacy-option" AND meta_value="private")
 ORDER BY   STR_TO_DATE( '.$wpdb->prefix. 'posts.post_date_gmt , "%Y-%m-%d %H:%i:%s")  DESC' ;

$bkquery = $bktemp.' LIMIT  ' . $bkstart .', 12';
}
else if(is_user_logged_in()){
$bktemp = 'SELECT * FROM ' .$wpdb->prefix.'posts WHERE post_type="dyn_book" AND post_status="publish" AND ID NOT IN (SELECT post_id FROM '.$wpdb->prefix.'postmeta WHERE meta_key="privacy-option" AND meta_value="private")
UNION
SELECT * FROM '.$wpdb->prefix.'posts WHERE post_type="dyn_book" AND post_status="publish" AND ID IN (SELECT post_id FROM '.$wpdb->prefix.'postmeta WHERE meta_key="privacy-option" AND meta_value="private") AND post_author='.get_current_user_id().'
UNION
SELECT * FROM '.$wpdb->prefix.'posts WHERE post_type="dyn_book"  AND post_status="publish" AND ID IN (SELECT post_id
FROM '.$wpdb->prefix.'postmeta WHERE meta_key = "select_users_list" AND LENGTH(meta_value)>0  AND meta_value REGEXP "[[:<:]]'.get_current_user_id().'[[:>:]]") ORDER BY  STR_TO_DATE(post_date_gmt,"%Y-%m-%d %H:%i:%s")  DESC ';

$bkquery = $bktemp.' LIMIT  ' . $bkstart .', 12';
}

$bkresult = $wpdb->get_results( $bkquery ); 
	 //echo  $wpdb->num_rows . 'Rows Found'; 
//echo "<pre>";
//print_r ($bkresult);
//echo "</pre>";
$bktotal=$wpdb->query($bktemp);

?>

<div class="list-group">
<?php echo '<a href="#new-books" id="new-books"><h5><b>NEW BOOKS</b></h5></a>'; ?>
	
	<div class="row">				 
<?php 
 if($bkresult)
 {
  $bkri=0;

foreach ($bkresult as $bkdata){
$postID = $bkdata->ID; 
$post = get_post($postID);
$privacyOption   = get_post_meta( $postID, 'privacy-option' ); 
//echo "<pre>";

//print_r ($privacyOption);

//echo "</pre>";

$selectUsersList = get_post_meta( $postID, 'select_users_list' ); 
$post_author     = $bkdata->post_author;
$user_ID         = get_current_user_id(); 
$selectUsersList = explode( ",", $selectUsersList[0] ); 

	$refr = get_post_meta($postID,'video_options_refr',true);
	if($refr){}
	else{$refr= 'No reference given'; }

$thumbnail_id=get_post_thumbnail_id($postID);
$thumbnail_url=wp_get_attachment_image_src($thumbnail_id, array(240,240), true);
$imgsrc= $thumbnail_url [0];

$revnum =apply_filters( 'dyn_number_of_post_review', $postID, "post"); 

$endors=$wpdb->get_results( 'SELECT user_id FROM '.$wpdb->prefix.'endorsements WHERE post_id = ' . $postID);
$endorsnum=count($endors);


$favs=$wpdb->get_results('SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id='. $postID);
$favsnum=count($favs);

$book_author=get_the_author_meta('display_name', $bkdata->post_author);

if(!is_user_logged_in()) {  // case where user is not logged in 
	             if(isset($privacyOption[0]) and ($privacyOption[0] != "private")) {
					  //if( ($bkri % 6) == 0){echo '</div><div class="row">'; }
					    ?>
							<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 text-center" style="padding: 5px 5px 20px 5px !important; margin:0px !important; height:300px;">

    			              <div class="thumbnail layout-2-wrapper solid-bg" style="height:100%; width:100% !important; box-shadow: 0 0 10px 1px #666; border-radius:5px; padding:5px">

								  
							   	   <div class="image-holder">
								   <a href="<?php echo get_permalink($postID); ?>">
								   <div class="hover-item"></div>
								   </a>            
												
											<?php							
										   if( has_post_thumbnail($postID) ){
												echo get_the_post_thumbnail($postID, 'medium-thumb', array( 'class' => 'layout-3-thumb', 'style'=>"height:140px; max-width:220px;" ) );
											}else{
													echo '<img src="'.get_template_directory_uri().'/images/no-image.png" class="layout-3-thumb" style="height:140px; max-width:220px;">';
											}?>

											
								   </div>
										<div class="layout-title-box text-center" style="display:block;">
										<h6 class="layout-title" style="max-width:100%; word-wrap: break-word; height:40px;">
										<?php
										preg_match_all('/(\w+\s)/', $bkdata->post_title, $matches);

										if ( count($matches[0]) && count($matches[1]) >4){
										?>
										<a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $matches[1][0] .  $matches[1][1] . $matches[1][2] . '[...]'; ?></a>
										<?php 
											}
										else if(strlen($bkdata->post_title)>30){ 
									   	?>
									    <a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo substr($bkdata->post_title, 0, 30) .' [...]'; ?></a>

									   <?php
									    }
										else {
												?>
										<a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $bkdata->post_title; ?></a>

										<?php } ?>
										</h6>
										<!-- <ul class="stats">
											<li ><?php videopress_displayviews( $postID ); ?></li>
											<li><?php //echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
											<li>Author: <font color="red"><?php //echo get_the_author($postID); ?></font></li>
											
										</ul>
										-->
										<ul class="bottom-detailsul" style="vertical-align:baseline;">
										<li><a class="detailsblock-btn" data-modal-id="videos" data-author="<?php echo $book_author; ?>" data-time="<?php echo human_time_diff(get_the_time('U', $postID), current_time('timestamp') ) . ' ago'; ?>" data-views="<?php videopress_displayviews( $postID ); ?>" data-title="<?php echo $bkdata->post_title; ?>" data-permalink="<?php echo get_permalink($postID); ?>" data-content="<?php echo strip_tags($bkdata->post_content); ?>" data-image="<?php echo $imgsrc; ?>" data-comments="<?php echo $bkdata->comment_count; ?>" data-ref="<?php echo $refr ?>" data-endorse="<?php echo $endorsnum?>" data-favr="<?php echo $favsnum; ?> " data-revs="<?php echo $revnum ?>">
										<i class="fa fa-info-circle" aria-hidden="true"></i> Details
										</a>
										</li>
									   </ul>
									   </div>
									   
								 </div>
							   </div>		
					   <?php
					    $bkri++;
				  }
				  else
				  {
					    if(!isset($privacyOption[0]))
						{
							 //if( ($bkri % 6) == 0){echo '</div><div class="row">'; }
					    ?>
							<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 text-center" style="padding: 5px 5px 20px 5px !important; margin:0px !important; height:300px;">

    			              <div class="thumbnail layout-2-wrapper solid-bg" style="height:100%; width:100% !important; box-shadow: 0 0 10px 1px #666; border-radius:5px; padding:5px">

								  
							   	   <div class="image-holder">
								   <a href="<?php echo get_permalink($postID); ?>">
								   <div class="hover-item"></div>
								   </a>            
												
											<?php							
										   if( has_post_thumbnail($postID) ){
												echo get_the_post_thumbnail($postID, 'medium-thumb', array( 'class' => 'layout-3-thumb', 'style'=>"height:140px; max-width:220px;" ) );
											}else{
													echo '<img src="'.get_template_directory_uri().'/images/no-image.png" class="layout-3-thumb" style="height:140px; max-width:220px;">';
											}?>

											
								   </div>
										<div class="layout-title-box text-center" style="display:block;">
										<h6 class="layout-title" style="max-width:100%; word-wrap: break-word; height:40px;">
										<?php
										preg_match_all('/(\w+\s)/', $bkdata->post_title, $matches);

										if ( count($matches[0]) && count($matches[1]) >4){;
										?>
										<a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $matches[1][0] .  $matches[1][1] . $matches[1][2] . '[...]'; ?></a>
										<?php 
											}
										else if(strlen($bkdata->post_title)>30){ 
									   	?>
									    <a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo substr($bkdata->post_title, 0, 30) .' [...]'; ?></a>

									   <?php
									    }
										else {
												?>
										<a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $bkdata->post_title; ?></a>

										<?php } ?>
										</h6>
										<!-- <ul class="stats">
											<li ><?php videopress_displayviews( $postID ); ?></li>
											<li><?php //echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
											<li>Author: <font color="red"><?php //echo get_the_author($postID); ?></font></li>
											
										</ul>
										-->
										<ul class="bottom-detailsul" style="vertical-align:baseline;">
										<li><a class="detailsblock-btn" data-modal-id="videos" data-author="<?php echo $book_author; ?>" data-time="<?php echo human_time_diff(get_the_time('U', $postID), current_time('timestamp') ) . ' ago'; ?>" data-views="<?php videopress_displayviews( $postID ); ?>" data-title="<?php echo $bkdata->post_title; ?>" data-permalink="<?php echo get_permalink($postID); ?>" data-content="<?php echo strip_tags($bkdata->post_content); ?>" data-image="<?php echo $imgsrc; ?>" data-comments="<?php echo $bkdata->comment_count; ?>" data-ref="<?php echo $refr ?>" data-endorse="<?php echo $endorsnum?>" data-favr="<?php echo $favsnum; ?> " data-revs="<?php echo $revnum ?>">
										<i class="fa fa-info-circle" aria-hidden="true"></i> Details
										</a>
										</li>
									   </ul>
									   </div>
									   
								 </div>
							   </div>		
					   <?php
					    $bkri++;
						}
				  }
		} 
		else
		{  // case where user is logged in
			 if($post_author == $user_ID)	
			 {    // Case where logged in User is same as Video Author User
		          //if( ($bkri % 6) == 0){echo '</div><div class="row">'; }
					    ?>
							<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 text-center" style="padding: 5px 5px 20px 5px !important; margin:0px !important; height:300px;">

    			              <div class="thumbnail layout-2-wrapper solid-bg" style="height:100%; width:100% !important; box-shadow: 0 0 10px 1px #666; border-radius:5px; padding:5px">

								  
							   	   <div class="image-holder">
								   <a href="<?php echo get_permalink($postID); ?>">
								   <div class="hover-item"></div>
								   </a>            
												
											<?php							
										   if( has_post_thumbnail($postID) ){
												echo get_the_post_thumbnail($postID, 'medium-thumb', array( 'class' => 'layout-3-thumb', 'style'=>"height:140px; max-width:220px;" ) );
											}else{
													echo '<img src="'.get_template_directory_uri().'/images/no-image.png" class="layout-3-thumb" style="height:140px; max-width:220px;">';
											}?>

											
								   </div>
										<div class="layout-title-box text-center" style="display:block;">
										<h6 class="layout-title" style="max-width:100%; word-wrap: break-word; height:40px;">
										<?php
										preg_match_all('/(\w+\s)/', $bkdata->post_title, $matches);

										if ( count($matches[0]) && count($matches[1]) >4){;
										?>
										<a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $matches[1][0] .  $matches[1][1] . $matches[1][2] . '[...]'; ?></a>
										<?php 
											}
										else if(strlen($bkdata->post_title)>30){ 
									   	?>
									    <a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo substr($bkdata->post_title, 0, 30) .' [...]'; ?></a>

									   <?php
									    }
										else {
												?>
										<a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $bkdata->post_title; ?></a>

										<?php } ?>
										</h6>
										<!-- <ul class="stats">
											<li ><?php videopress_displayviews( $postID ); ?></li>
											<li><?php //echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
											<li>Author: <font color="red"><?php //echo get_the_author($postID); ?></font></li>
											
										</ul>
										-->
										<ul class="bottom-detailsul" style="vertical-align:baseline;">
										<li><a class="detailsblock-btn" data-modal-id="videos" data-author="<?php echo $book_author; ?>" data-time="<?php echo human_time_diff(get_the_time('U', $postID), current_time('timestamp') ) . ' ago'; ?>" data-views="<?php videopress_displayviews( $postID ); ?>" data-title="<?php echo $bkdata->post_title; ?>" data-permalink="<?php echo get_permalink($postID); ?>" data-content="<?php echo strip_tags($bkdata->post_content); ?>" data-image="<?php echo $imgsrc; ?>" data-comments="<?php echo $bkdata->comment_count; ?>" data-ref="<?php echo $refr ?>" data-endorse="<?php echo $endorsnum?>" data-favr="<?php echo $favsnum; ?> " data-revs="<?php echo $revnum ?>">
										<i class="fa fa-info-circle" aria-hidden="true"></i> Details
										</a>
										</li>
									   </ul>
									   </div>
									   
								 </div>
							   </div>		
					   <?php
					    $bkri++;
			 }	
			 else
			 {    // Case where logged in User is not same as Video Author User
				  if(isset($privacyOption[0]) and $privacyOption[0] == "public")
				  {
					  //if( ($bkri % 6) == 0){echo '</div><div class="row">'; }
					    ?>
							<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 text-center" style="padding: 5px 5px 20px 5px !important; margin:0px !important; height:300px;">

    			              <div class="thumbnail layout-2-wrapper solid-bg" style="height:100%; width:100% !important; box-shadow: 0 0 10px 1px #666; border-radius:5px; padding:5px">

								  
							   	   <div class="image-holder">
								   <a href="<?php echo get_permalink($postID); ?>">
								   <div class="hover-item"></div>
								   </a>            
												
											<?php							
										   if( has_post_thumbnail($postID) ){
												echo get_the_post_thumbnail($postID, 'medium-thumb', array( 'class' => 'layout-3-thumb', 'style'=>"height:140px; max-width:220px;" ) );
											}else{
													echo '<img src="'.get_template_directory_uri().'/images/no-image.png" class="layout-3-thumb" style="height:140px; max-width:220px;">';
											}?>

											
								   </div>
										<div class="layout-title-box text-center" style="display:block;">
										<h6 class="layout-title" style="max-width:100%; word-wrap: break-word; height:40px;">
										<?php
										preg_match_all('/(\w+\s)/', $bkdata->post_title, $matches);

										if ( count($matches[0]) && count($matches[1]) >4){;
										?>
										<a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $matches[1][0] .  $matches[1][1] . $matches[1][2] . '[...]'; ?></a>
										<?php 
											}
										else if(strlen($bkdata->post_title)>30){ 
									   	?>
									    <a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo substr($bkdata->post_title, 0, 30) .' [...]'; ?></a>
									   <?php
									    }
										else {
												?>
										<a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $bkdata->post_title; ?></a>

										<?php } ?>
										</h6>
										<!-- <ul class="stats">
											<li ><?php videopress_displayviews( $postID ); ?></li>
											<li><?php //echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
											<li>Author: <font color="red"><?php //echo get_the_author($postID); ?></font></li>
											
										</ul>
										-->
										<ul class="bottom-detailsul" style="vertical-align:baseline;">
										<li><a class="detailsblock-btn" data-modal-id="videos" data-author="<?php echo $book_author; ?>" data-time="<?php echo human_time_diff(get_the_time('U', $postID), current_time('timestamp') ) . ' ago'; ?>" data-views="<?php videopress_displayviews( $postID ); ?>" data-title="<?php echo $bkdata->post_title; ?>" data-permalink="<?php echo get_permalink($postID); ?>" data-content="<?php echo strip_tags($bkdata->post_content); ?>" data-image="<?php echo $imgsrc; ?>" data-comments="<?php echo $bkdata->comment_count; ?>" data-ref="<?php echo $refr ?>" data-endorse="<?php echo $endorsnum?>" data-favr="<?php echo $favsnum; ?> " data-revs="<?php echo $revnum ?>">
										<i class="fa fa-info-circle" aria-hidden="true"></i> Details
										</a>
										</li>
									   </ul>
									   </div>
									   
								 </div>
							   </div>		
					   <?php
					    $bkri++;
				  }
				  else
				  {
					  if(!isset($privacyOption[0])) {
						   //if( ($bkri % 6) == 0){echo '</div><div class="row">'; }
					    ?>
							<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 text-center" style="padding: 5px 5px 20px 5px !important; margin:0px !important; height:300px;">

    			              <div class="thumbnail layout-2-wrapper solid-bg" style="height:100%; width:100% !important; box-shadow: 0 0 10px 1px #666; border-radius:5px; padding:5px">

								  
							   	   <div class="image-holder">
								   <a href="<?php echo get_permalink($postID); ?>">
								   <div class="hover-item"></div>
								   </a>            
												
											<?php							
										   if( has_post_thumbnail($postID) ){
												echo get_the_post_thumbnail($postID, 'medium-thumb', array( 'class' => 'layout-3-thumb', 'style'=>"height:140px; max-width:220px;" ) );
											}else{
													echo '<img src="'.get_template_directory_uri().'/images/no-image.png" class="layout-3-thumb" style="height:140px; max-width:220px;">';
											}?>

											
								   </div>
										<div class="layout-title-box text-center" style="display:block;">
										<h6 class="layout-title" style="max-width:100%; word-wrap: break-word; height:40px;">
										<?php
										preg_match_all('/(\w+\s)/', $bkdata->post_title, $matches);

										if ( count($matches[0]) && count($matches[1]) >4){;
										?>
										<a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $matches[1][0] .  $matches[1][1] . $matches[1][2] . '[...]'; ?></a>
										<?php 
											}
										else if(strlen($bkdata->post_title)>30){ 
									   	?>
									    <a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo substr($bkdata->post_title, 0, 30) .' [...]'; ?></a>

									   <?php
									    }
										else {
												?>
										<a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $bkdata->post_title; ?></a>

										<?php } ?>
										</h6>
										<!-- <ul class="stats">
											<li ><?php videopress_displayviews( $postID ); ?></li>
											<li><?php //echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
											<li>Author: <font color="red"><?php //echo get_the_author($postID); ?></font></li>
											
										</ul>
										-->
										<ul class="bottom-detailsul" style="vertical-align:baseline;">
										<li><a class="detailsblock-btn" data-modal-id="videos" data-author="<?php echo $book_author; ?>" data-time="<?php echo human_time_diff(get_the_time('U', $postID), current_time('timestamp') ) . ' ago'; ?>" data-views="<?php videopress_displayviews( $postID ); ?>" data-title="<?php echo $bkdata->post_title; ?>" data-permalink="<?php echo get_permalink($postID); ?>" data-content="<?php echo strip_tags($bkdata->post_content); ?>" data-image="<?php echo $imgsrc; ?>" data-comments="<?php echo $bkdata->comment_count; ?>" data-ref="<?php echo $refr ?>" data-endorse="<?php echo $endorsnum?>" data-favr="<?php echo $favsnum; ?> " data-revs="<?php echo $revnum ?>">
										<i class="fa fa-info-circle" aria-hidden="true"></i> Details
										</a>
										</li>
									   </ul>
									   </div>
									   
								 </div>
							   </div>		
					   <?php
					    $bkri++;
					  } else  {
						    if( is_array($selectUsersList) and count($selectUsersList) > 0 )
							   {   // case where user access list is available
									 if( in_array($user_ID, $selectUsersList) )
									 {
										 //if( ($bkri % 6) == 0){echo '</div><div class="row">'; }
					    ?>
							<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 text-center" style="padding: 5px 5px 20px 5px !important; margin:0px !important; height:300px;">

    			              <div class="thumbnail layout-2-wrapper solid-bg" style="height:100%; width:100% !important; box-shadow: 0 0 10px 1px #666; border-radius:5px; padding:5px">

								  
							   	   <div class="image-holder">
								   <a href="<?php echo get_permalink($postID); ?>">
								   <div class="hover-item"></div>
								   </a>            
												
											<?php							
										   if( has_post_thumbnail($postID) ){
												echo get_the_post_thumbnail($postID, 'medium-thumb', array( 'class' => 'layout-3-thumb', 'style'=>"height:140px; max-width:220px;" ) );
											}else{
													echo '<img src="'.get_template_directory_uri().'/images/no-image.png" class="layout-3-thumb" style="height:140px; max-width:220px;">';
											}?>

											
								   </div>
										<div class="layout-title-box text-center" style="display:block;">
										<h6 class="layout-title" style="max-width:100%; word-wrap: break-word; height:40px;">
										<?php
										preg_match_all('/(\w+\s)/', $bkdata->post_title, $matches);

										if ( count($matches[0]) && count($matches[1]) >4){;
										?>
										<a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $matches[1][0] .  $matches[1][1] . $matches[1][2] . '[...]'; ?></a>
										<?php 
										}
										else if(strlen($bkdata->post_title)>30){ 
									   	?>
									    <a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo substr($bkdata->post_title, 0, 30) .' [...]'; ?></a>

									   <?php
									    }
										else {
												?>
										<a href="<?php echo get_permalink( $postID) ?>" title="<?php echo $bkdata->post_title; ?>" style="color: #000; font-size: 14px; text-transform: capitalize;"><?php echo $bkdata->post_title; ?></a>

										<?php } ?>
										</h6>
										<!-- <ul class="stats">
											<li ><?php videopress_displayviews( $postID ); ?></li>
											<li><?php //echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
											<li>Author: <font color="red"><?php //echo get_the_author($postID); ?></font></li>
											
										</ul>
										-->
										<ul class="bottom-detailsul" style="vertical-align:baseline;">
										<li><a class="detailsblock-btn" data-modal-id="videos" data-author="<?php echo $book_author; ?>" data-time="<?php echo human_time_diff(get_the_time('U', $postID), current_time('timestamp') ) . ' ago'; ?>" data-views="<?php videopress_displayviews( $postID ); ?>" data-title="<?php echo $bkdata->post_title; ?>" data-permalink="<?php echo get_permalink($postID); ?>" data-content="<?php echo strip_tags($bkdata->post_content); ?>" data-image="<?php echo $imgsrc; ?>" data-comments="<?php echo $bkdata->comment_count; ?>" data-ref="<?php echo $refr ?>" data-endorse="<?php echo $endorsnum?>" data-favr="<?php echo $favsnum; ?> " data-revs="<?php echo $revnum ?>">
										<i class="fa fa-info-circle" aria-hidden="true"></i> Details
										</a>
										</li>
									   </ul>
									   </div>
									   
								 </div>
							   </div>		
					   <?php
					    $bkri++;
									 }
									 else
									 {  // case where user is not in access list
									 	 
									 }
							   }
							   else
							   {  }
					  }	 
				  }
			 }	
		}  	
 }}?>



		</div>
		<div class="row" data-anchor="new-books">
		<div class="col-sm-12">
		<nav class="pull-right">
        
   <?php

   if($cat=='new-books'){
         $pcurrent=max( 1, get_query_var('page') );
        }
   else{
         $pcurrent=1;
        }

 $paginate=paginate_links( array(
	'format' => 'page/%#%/',
	'current' => $pcurrent,
	'type' => 'array',
	'end_size' =>1,
	'mid_size'=>4,
	'total' => ($bktotal/12)
           ) );

$pcount=count($paginate);


 $maxp=ceil(($bktotal/12)/2);

if( $pcurrent<= ceil($bktotal / 12)  )
{
 if( ceil($bktotal / 12) <5 ) {
 foreach($paginate as $plnk){echo $plnk.'&nbsp;';}
 }

else if( ceil($bktotal / 12) >= 5 ){


 if ($pcurrent<=6)
 {
 	if ($pcurrent>2)
 	{echo $paginate[0].'&nbsp;';}
      if($paginate[$pcurrent-2]){
 	echo $paginate[$pcurrent-2].'&nbsp;';
       }
 	echo $paginate[$pcurrent-1].'&nbsp;';
 	echo $paginate[$pcurrent].'&nbsp;';
if( isset($paginate[$pcurrent+1]) && (($pcount-1) > $pcurrent+1) ){echo $paginate[$pcurrent+1].'&nbsp;';}
if( isset($paginate[$pcurrent+2]) && (($pcount-1) > $pcurrent+2) ){echo $paginate[$pcurrent+2].'&nbsp;';}

 }

else if ($pcurrent>=7)
 {   echo $paginate[0].'&nbsp;';
 	echo $paginate[5].'&nbsp;';
 	echo $paginate[6].'&nbsp;';
 	echo $paginate[7].'&nbsp;';
if( isset($paginate[8]) && (($pcount-1) > 8) ){echo $paginate[8].'&nbsp;';}
if( isset($paginate[9]) && (($pcount-1) > 9) ){echo $paginate[9].'&nbsp;';}

 }


if( $pcurrent <= (ceil($bktotal/12)-1) ){
echo $paginate[$pcount-1];
  }

 }

}

  ?>
     </nav>
    </div>
   </div>
</div>

