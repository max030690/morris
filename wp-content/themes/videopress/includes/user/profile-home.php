<?php
// This is called in homepage template under "Popular Channels"
// Set Author ID
$author = get_user_by( 'id', $popular_user['post_author'] );

?>

<!-- Start User info -->
    <div class="user-profile">
    <h6 class="title-bar"><span><?php echo get_the_author_meta('nickname', $author->ID ); ?></span></h6>

    	<?php echo get_avatar( $author->ID, '70' ); ?>

        <div class="user-info">
        <?php if( get_the_author_meta('user_url', $author->ID) != '' ){
        echo '<p><strong>Website:</strong><br> <a href="'.get_the_author_meta('user_url', $author->ID).'" rel="nofollow">'.get_the_author_meta('user_url', $author->ID).'</a></p>';
        } // End If Description
		?>

		<?php if( get_the_author_meta('description', $author->ID) != '' ){
        echo '<p><strong>About Me</strong><br>'.get_the_author_meta('description', $author->ID).'</p>';
        } // End If Description
		?>

        <!-- Social Icons -->
        <!--
        <ul class="user-social">
        <?php if( get_the_author_meta('gplus') != '' ){ echo '<li><a href="'.get_the_author_meta('gplus', $author->ID).'" target="_blank"><i class="fa fa-google-plus-square"></i></a></li>'; } ?>

        <?php
		if( get_the_author_meta('facebook', $author->ID) != '' ){ echo '<li><a href="'.get_the_author_meta('facebook', $author->ID).'" target="_blank"><i class="fa fa-facebook-square"></i></a></li>'; } ?>

        <?php
		if( get_the_author_meta('twitter', $author->ID) != '' ){ echo '<li><a href="'.get_the_author_meta('twitter', $author->ID).'" target="_blank"><i class="fa fa-twitter-square"></i></a></li>'; }?>

        <?php
		if( get_the_author_meta('instagram', $author->ID) != '' ){ echo '<li><a href="'.get_the_author_meta('instagram', $author->ID).'" target="_blank"><i class="fa fa-instagram"></i></a></li>'; }?>

        <?php
		if( get_the_author_meta('pinterest', $author->ID) != '' ){ echo '<li><a href="'.get_the_author_meta('pinterest', $author->ID).'" target="_blank"><i class="fa fa-pinterest-square"></i></a></li>'; }?>

        <?php
		if( get_the_author_meta('linkedin', $author->ID) != '' ){ echo '<li><a href="'.get_the_author_meta('linkedin', $author->ID).'" target="_blank"><i class="fa fa-linkedin-square"></i></a></li>'; }?>

        <?php
		if( get_the_author_meta('youtube', $author->ID) != '' ){ echo '<li><a href="'.get_the_author_meta('youtube', $author->ID).'" target="_blank"><i class="fa fa-youtube-square"></i></a></li>'; }?>
        </ul>
        <!-- Social Icons -->

        </div>


    <div class="clear"></div>
    </div>
    <!-- End User info -->


    <!-- Start Uploaded Videos by user -->
    <div class="layout-2">
    <h6 class="title-bar"><span>Videos By <?php echo get_the_author_meta('nickname', $author->ID ); ?></span></h6>
    <?php
	/* ================================================================== */
	/* Start of loop */
	/* ================================================================== */
	$x = 0;


	// query posts by user
	$query = new WP_Query( 'author=' . $popular_user['post_author'] );

	if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post();
	$x++;
	if ($x < 2){ // START (IF X IS LESS THAN 2)
	?>

    <!-- Start Layout Wrapper -->
    <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    	<div class="layout-2-wrapper<?php if( $x%2 == 0 ){ echo ' solid-bg'; } ?>">

        <div class="grid-6 first">
            <div class="image-holder">
            <a href="<?php echo get_permalink(); ?>">
            <div class="hover-item"></div>
            </a>

            <?php
			if( has_post_thumbnail() ){
            	the_post_thumbnail('small-thumb', array( 'class' => 'layout-2-thumb' ) );
			}else{
				echo '<img src="'.get_template_directory_uri().'/images/no-image.png" class="layout-2-thumb">';
			}
			?>

            </div>
        </div>

        <div class="layout-2-details">
     <h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h6>
        <ul class="stats">
            <li><?php videopress_countviews( get_the_ID() ); ?></li>
            <li><?php comments_number() ?></li>
            <li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
        </ul>
        <div class="clear"></div>
        <p><?php echo videopress_content('240'); ?></p>
        </div>

    <div class="clear"></div>
    	</div>
    </div>
    <!-- End Layout Wrapper -->

    <?php
	} // END (IF X IS LESS THAN 2)
	/* ================================================================== */
	/* End of Loop */
	/* ================================================================== */
	endwhile;

    /* ================================================================== */
    /* Else if Nothing Found */
    /* ================================================================== */
    else :
    ?>

    <h6>NO VIDEOS FOUND!</h6>
    <p>The user has not yet uploaded any videos yet.</p>

    <?php
	/* ================================================================== */
	/* End If */
	/* ================================================================== */
	endif;
	?>
    </div>
    <!-- End Uploaded Videos by user -->

    <!-- Start Pagination -->
    <?php if( vp_option('vpt_option.pagination') == '1' ){
		videopress_pagination(); // Use Custom Pagination
	}else{
		echo '<div class="post_pagination">';
		posts_nav_link();
		echo'</div>';
	} ?>
    <!-- End Pagination -->