
<?php
session_start();
ob_start();

$requestData = explode("/", $_SERVER['REQUEST_URI']);
$folderName = $requestData[1];
if(isset($_REQUEST['editChannel'])){ ?>
<script>
	$(window).load(function(){
		jQuery(".show_live_channel_tab").trigger("click");
	});
</script>
<?php }

// Profile Photo
//profile id from author two pictures
$author = get_user_by( 'slug', get_query_var( 'author_name' ) );
$profile_id = $author->ID;
$_SESSION["post_id_comment"] = $profile_id;
$_SESSION["author_ids"] = $profile_id;
global $paged;
$limit = false;

//include datepicker jqueryui core
//wp_enqueue_script('jquery-js','//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js');
wp_enqueue_script('jquery-ui-datepicker');
wp_enqueue_style('jquery-style', get_stylesheet_directory_uri().'/css/jquery-ui.css');
wp_enqueue_script('js',get_stylesheet_directory_uri().'/js/jstz.min.js');
wp_enqueue_script('timezone_detect_profile', get_stylesheet_directory_uri().'/js/timezonedetect_profile.js');



/***********************************************************
	0 - Remove The Temp image if it exists
	***********************************************************/
	/* if (!isset($_POST['x']) && !isset($_FILES['image']['name']) ){
		//Delete users temp image
			$temppath = '/var/www/html/bitbucket/profile_pic/'.$profile_id.'_temp.jpeg';
			if (file_exists ($temppath)){ @unlink($temppath); }
		}  */

//// Upload User Files From Files Tab /////////////////

// user file save
		$file_active = false;
		$msg = false;
		$upload_dir = wp_upload_dir();

//print_r($upload_dir);die;
		$msg = false;
		$msg_type = false;

		$home_active = 'active';


		if(isset($_GET['sortvideo']) && $_GET['sortvideo']!=''){
			$home_active = false;
			$vid_active = 'active';
		}


		if(isset($_GET['tabo']) && $_GET['tabo']!=''){
			$home_active = false;
			$file_active = 'active';
		}



	    if(isset($_GET['store']) && $_GET['store']!=''){
			$home_active = false;
			$new_store_active = 'active';
		}


		if(isset($_FILES) && $_POST['u_f_n']!==''){

			$path = $_FILES['u_f']['name'];
			$ext = pathinfo($path, PATHINFO_EXTENSION);
			$exts = array('gif','GIF','png','PNG','jpg','JPG','jpeg','JPEG','doc','DOC','docx','DOCX','pdf','PDF','txt','TXT');

			if(in_array($ext, $exts )){
				$upload_dir = wp_upload_dir();
		//echo '<pre>';print_r($upload_dir['basedir']);echo '</pre>';
				$file = uniqid().'.'.$ext;
				$save_path = $upload_dir['basedir'].'/files/'.$file;
				$download_path = $upload_dir['url'].'/files/'.$file;

				global $wpdb;
				$table = $wpdb->prefix."user_files";
				$insert = $wpdb->insert($table,array(
					'file_name'	=> $_POST['u_f_n'],
					'user_id'	=> get_current_user_id(),
					'file_type'	=> $ext,
					'downloads'	=> '0',
					'file'=>$file
					));
				if($insert){
					$lastid = $wpdb->insert_id;

					move_uploaded_file($_FILES['u_f']['tmp_name'],$save_path);
					$data = array();
					$data['file_active'] = 'active';
					$data['msg_type'] = 'success';
					$data['msg'] = '<div class="alert alert-success">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong>Success!</strong> File Saved !
				</div>';
				$data['result'] = '';


			//////////////////////////////////////////////
				$data['result'] .= '<tr>';
				$filetype_image = array('png','PNG','jpg','JPG','jpeg','JPEG','gif','GIF');
				$filetype_word = array('doc','DOC','docx','DOCX');
				$filtype_icon = (in_array($ext, $filetype_image))?'file-image-o':
				((in_array($ext, $filetype_word))?'file-word-o':
					(($ext=='txt')?'file-text-o':
						(($ext=='pdf')?'file-pdf-o':'file')));

				$download_link = $upload_dir['url'].'/files/'.$fil->file;
				$data['result'] .= '<td>
				<a class="file-download" href="'. $download_path.'" target="_blank" download data-id="'.$lastid.'">
					<i class="fa fa-'.$filtype_icon.'"></i>
				</a>
			</td>
			<td>
				<a class="file-download" href="'. $download_path.'" target="_blank" download data-id="'.$lastid.'">
					<b>'.$_POST['u_f_n'].'</b>
				</a>
			</td>
			<td class="type">'.$ext.'</td>
			<td class="download-c">0</td>';

					//if($profile_id == get_current_user_id()) {
			$data['result'] .= '<td><a class="u_f_del" href="?delete_file='. $lastid.'"><button type="button" class="btn btn-info">Delete</button></a></td>';
					//}
			$data['result'] .= '</tr>';
			//////////////////////////////////////////////
			$home_active = false;
			$vid_active = false;
			$file_active = 'active';

			//echo json_encode($data);
			//echo 'uploaded';
		}
	}
	?>
	<script>
		$("#u_f_form").trigger('reset');


	</script>
	<?php
}

function ctpreview( $content = '' , $length = '' , $message = '' , $html = '' ){
	if( $content == '' ){
		$content = $message ;
	}else{
		$content = strip_tags( $content );
		if( ( $length != '' ) && ( strlen( $content ) > $length ) ){
			$content = substr( $content , 0 , $length )." ".$html;
		}else{
			$content = $content ;
		}
	}
	return $content;
}


//////////End file UPload File Tab///////////////////////////////

/////////// Start file Delete //////////////////////////////

if(isset($_GET['delete_file'])&& $_GET['delete_file']!==''){
	if(is_user_logged_in()){
		global $wpdb;
		$delete_file = $_GET['delete_file'];
		$table = $wpdb->prefix."user_files";
		$logged_user = get_current_user_id();
		if(current_user_can( 'manage_options' )){
			$data = $wpdb->get_row("SELECT * FROM $table WHERE id=$delete_file",ARRAY_A);
		}else{
			$data = $wpdb->get_row("SELECT * FROM $table WHERE id=$delete_file AND user_id= $logged_user",ARRAY_A);
		}
		if($data){
				//$unlink = unlink($upload_dir['path'].'/files/'.$data['file']);
			unlink($upload_dir['basedir'].'/files/'.$data['file']);
			$msg = '<strong>Success!</strong> File Deleted !';
			$msg_type = 'success';
			$file_active = 'active';
			$home_active = false;
			$delete = $wpdb->delete( $table, array( 'id' => $delete_file ) );
		}

	}
}

/////////////////// End file Delete //////////////////////

$social_active = false;

if(isset($_POST['media_setting']) && $_POST['media_setting']!==''){
	$social_array = array(
		'facebook_enable' => ($_POST['fb_enable']) ? $_POST['fb_enable'] : '',
		'facebook_url' => ($_POST['fb_s']) ? $_POST['fb_s'] : '',
		'twitter_enable'=> ($_POST['tw_enable']) ? $_POST['tw_enable'] : '',
		'twitter_url'=> ($_POST['tw_s']) ? $_POST['tw_s'] : '',
		'instagram_enable'=> ($_POST['ins_enable']) ? $_POST['ins_enable'] : '',
		'instagram_url'=>($_POST['ins_s']) ? $_POST['ins_s'] : '',
		'linkedin_enable'=> ($_POST['lin_enable']) ? $_POST['lin_enable'] : '',
		'linkedin_url'=>($_POST['lin5_s']) ? $_POST['lin5_s'] : '',
		'website_enable'=> ($_POST['ws_enable']) ? $_POST['ws_enable'] : '',
		'website_url'=>($_POST['ws_s']) ? $_POST['ws_s'] : '',
		'googleplus_enable'=> ($_POST['gplus_enable']) ? $_POST['gplus_enable'] : '',
		'googleplus_url'=>($_POST['gplus_s']) ? $_POST['gplus_s'] : '',
		);
	update_user_meta($profile_id,'user_socialm_setting',$social_array);

	$social_active = 'active';
	$home_active = false;
}

$social_media_values = get_user_meta($profile_id,'user_socialm_setting',true);

if (isset($_POST['profileType']) && $_POST['profileType'] == '111' ){
	if(isset($_FILES['image']['name'])){
		/***********************************************************
			1 - Upload Original Image To Server
			***********************************************************/
			//Get Name | Size | Temp Location
			$ImageName = $_FILES['image']['name'];
			$ImageSize = $_FILES['image']['size'];
			$ImageTempName = $_FILES['image']['tmp_name'];
			//Get File Ext
			$ImageType = @explode('/', $_FILES['image']['type']);
				$type = $ImageType[1]; //file type
			//Set Upload directory
				$uploaddir = $_SERVER['DOCUMENT_ROOT'].'/'.$folderName.'/profile_pic/';
			//Set File name
				$file_temp_name = $profile_id.'_original.'.md5(time()).'n'.$type; //the temp file name
				$fullpath = "$uploaddir/".$file_temp_name; // the temp file path
				$file_name = $profile_id.'_temp.jpeg'; //$profile_id.'_temp.'.$type; // for the final resized image
				$fullpath_2 = "$uploaddir/".$file_name; //for the final resized image
			//Move the file to correct location
				$move = move_uploaded_file($ImageTempName ,$fullpath);
				chmod($fullpath, 0777);
			//Check for valid uplaod
				if (!$move) {
					die ('File didnt upload');
				} else {
				     $imgSrcProfile= site_url().'/profile_pic/'.$file_name.'?x='.rand();
					//$imgSrcProfile= 'http://182.75.35.84:83/bitbucket/profile_pic/'.$file_name.'?x='.rand(); // the image to display in crop area
					$msg= "Upload Complete!";  	//message to page
					$src = $file_name;	 		//the file name to post from cropping form to the resize
				}

		/***********************************************************
			2  - Resize The Image To Fit In Cropping Area
			***********************************************************/
				//get the uploaded image size
			clearstatcache();
			$original_size = getimagesize($fullpath);
			$original_width = $original_size[0];
			$original_height = $original_size[1];
				// Specify The new size
					$main_width = 500; // set the width of the image
					$main_height = $original_height / ($original_width / $main_width);	// this sets the height in ratio

				//create new image using correct php func
					if($_FILES["image"]["type"] == "image/gif"){
						$src2 = imagecreatefromgif($fullpath);
					}elseif($_FILES["image"]["type"] == "image/jpeg" || $_FILES["image"]["type"] == "image/pjpeg"){
						$src2 = imagecreatefromjpeg($fullpath);
					}elseif($_FILES["image"]["type"] == "image/png"){
						$src2 = imagecreatefrompng($fullpath);
					}else{
						$msg .= "There was an error uploading the file. Please upload a .jpg, .gif or .png file. <br />";
					}
				//create the new resized image
					$main = imagecreatetruecolor($main_width,$main_height);
					imagecopyresampled($main,$src2,0, 0, 0, 0,$main_width,$main_height,$original_width,$original_height);
				//upload new version
					$main_temp = $fullpath_2;
					imagejpeg($main, $main_temp, 90);
					chmod($main_temp,0777);
				//free up memory
					imagedestroy($src2);
					imagedestroy($main);
					@imagedestroy($fullpath);
					@unlink($fullpath); // delete the original upload
	}//ADD Image
	$_POST['profileType'] = "";
}

/***********************************************************
	3- Cropping & Converting The Image To Jpg
	***********************************************************/
	if (isset($_POST['profileCropType']) && $_POST['profileCropType'] == '222' ){
		if (isset($_POST['x']) && $_POST['x']>=0){

		//the file type posted
			$type = $_POST['typeProfile'];
		//the image src

			$src = $_SERVER['DOCUMENT_ROOT'].'/'.$folderName.'/profile_pic/'.$_POST['srcProfile'];

			$finalname = md5(time());

				$xx = $_POST['x']+10;
				$yy = $_POST['y']+10;
			//the target dimensions 150x150
				$targ_w = $targ_h = 250;
			//quality of the output
				$jpeg_quality = 90;

				$user_id = get_current_user_id();
				$meta_key = 'profile_pic_meta';

			if($type == 'jpg' || $type == 'jpeg' || $type == 'JPG' || $type == 'JPEG'){

			//create a cropped copy of the image
				$img_r = imagecreatefromjpeg($src);
				$dst_r = imagecreatetruecolor( $targ_w, $targ_h );
				imagecopyresampled($dst_r,$img_r,0,0,$xx,$yy,$targ_w,$targ_h,$_POST['w'],$_POST['h']);

				update_user_meta( $user_id, $meta_key, $finalname.'n.jpeg');
			//save the new cropped version
				imagejpeg($dst_r, $_SERVER['DOCUMENT_ROOT']."/".$folderName."/profile_pic/".$finalname."n.jpeg", 90);

			}else if($type == 'png' || $type == 'PNG'){

			//create a cropped copy of the image
				$img_r = imagecreatefrompng($src);
				$dst_r = imagecreatetruecolor( $targ_w, $targ_h );
				imagecopyresampled($dst_r,$img_r,0,0,$xx,$yy,$targ_w,$targ_h,$_POST['w'],$_POST['h']);

				update_user_meta( $user_id, $meta_key, $finalname.'n.jpeg');
			//save the new cropped version
				imagejpeg($dst_r, $_SERVER['DOCUMENT_ROOT']."/".$folderName."/profile_pic/".$finalname."n.jpeg", 90);


			}else if($type == 'gif' || $type == 'GIF'){

			//create a cropped copy of the image
				$img_r = imagecreatefromgif($src);
				$dst_r = imagecreatetruecolor( $targ_w, $targ_h );
				imagecopyresampled($dst_r,$img_r,0,0,$xx,$yy,$targ_w,$targ_h,$_POST['w'],$_POST['h']);

				update_user_meta( $user_id, $meta_key, $finalname.'n.jpeg');
			//save the new cropped version
				imagejpeg($dst_r, $_SERVER['DOCUMENT_ROOT']."/".$folderName."/profile_pic/".$finalname."n.jpeg", 90);
			}
			//free up memory
				imagedestroy($img_r); // free up memory
				imagedestroy($dst_r); //free up memory
				@unlink($src); // delete the original upload

			//return cropped image to page
			//$displayname ="http://182.75.35.84:83/bitbucket/profile_pic/".$finalname."n.jpeg";

		} // post x
		$_POST['profileCropType'] = "";
		echo '<script>window.location.assign(window.location.href);</script>';
	}
?>

<script>

	var profile_id = '<?= $profile_id;?>';

	//JCrop Bits  Profile Photo
	jquery(function(){

		jquery('#jcrop_target_profile').Jcrop({
			aspectRatio: 1,
			setSelect:   [ 200,200,37,49 ],
			onSelect: updateCoords
		});
	});

	function updateCoords(c)
	{
		jquery('#x').val(c.x);
		jquery('#y').val(c.y);
		jquery('#w').val(c.w);
		jquery('#h').val(c.h);
	};

	function checkCoords()
	{
		if (parseInt(jquery('#w').val())) return true;
		alert('Please select a crop region then press submit.');
		return false;
	};
	//End JCrop Bits

	function cancelCrop(){
		//Refresh page
		top.location = 'index.php?cancel';
		return false;
	}

	jQuery(document).ready(function(){
		jQuery("#profileImage").on("change", function() {
			jQuery("#profileForm").submit();
		});

		jQuery('#profile_click').click(function(e){
			e.preventDefault();
			jQuery('#profileImage').click();
		});

		jQuery('#MyDate').datepicker({
			dateFormat : 'yy-mm-dd',
			changeMonth: true,
			changeYear: true
		});


	});

</script>

<!-- Profile Photo-->
<!--<div id="Overlay" style=" width:100%; height:100%; background-color:rgba(0,0,0,.5); border:0px #990000 solid; position:absolute; top:0px; left:0px; z-index:2000; display:none;"></div>-->

<div id="wrapper" style="text-area:center; position:relative; margin:0px auto; border:1px #444 solid; background-color:#3498db;">

	<div id="formExample">

		<form action="" method="post" id="profileForm" enctype="multipart/form-data" style="display:none;">

			<input type="file" id="profileImage" name="image" accept='.jpeg,.jpg,.gif,.png,.JPEG,.JPG,.GIF,.PNG' /><br />
			<input type="hidden" name="profileType" value="111" />
			<input type="submit" value="submit" />

		</form>

	</div> <!-- Form-->


	<?php  if(isset($imgSrcProfile)){ //if an image has been uploaded display cropping area?>
	<script>
		jquery('#Overlay').show();
		jquery('#formExample').hide();
	</script>
	<div id="CroppingContainer" style="width:800px; max-height:600px; background-color:#FFF; position:relative; overflow:hidden; border:2px #666 solid; margin:50px auto; z-index:2001; padding-bottom:0px;">

		<div id="CroppingArea" style="width:500px; max-height:400px; position:relative; overflow:hidden; margin:40px 0px 40px 40px; border:2px #666 solid; float:left;">
			<img src="<?php echo $imgSrcProfile;?>" border="0" id="jcrop_target_profile" style="border:0px #990000 solid; position:relative; margin:0px 0px 0px 0px; padding:0px; " />
		</div>

		<div id="CropImageForm" style="float:left; margin:175px 0px 0px 40px;" >
			<form method="post" onsubmit="return checkCoords();">
				<input type="hidden" id="x" name="x" />
				<input type="hidden" id="y" name="y" />
				<input type="hidden" id="w" name="w" />
				<input type="hidden" id="h" name="h" />
				<input type="hidden" name="typeProfile" value="jpeg"/>
				<input type="hidden" name="srcProfile" value="<?php echo $src; ?>"/>
				<input type="hidden" name="profileCropType" value="222" />
				<input type="submit" value="Crop Image" class="btn btn-success" style="margin:5px;" />
			</form>
		</div>
		<div id="CropImageForm2" style="float:left; margin:0px 0px 0px 40px;" >
			<form action="" method="post" onsubmit="return cancelCrop();">
				<input type="submit" value="Cancel Crop" class="btn btn-danger" style="margin:5px;" />
			</form>
		</div>

	</div><!-- CroppingContainer -->
	<?php } ?>


</div>


<!-- Cover Photo -->


<?php

 //profile id
$profile_id1 = 123456;

/***********************************************************
	0 - Remove The Temp image if it exists
	***********************************************************/
	/* if (!isset($_POST['x']) && !isset($_FILES['image']['name']) ){
		//Delete users temp image
			$temppath = '/var/www/html/bitbucket/profile_pic/'.$profile_id.'_temp.jpeg';
			if (file_exists ($temppath)){ @unlink($temppath); }
		}  */

	if (isset($_POST['coverType']) && $_POST['coverType'] == '333' ){
		if(isset($_FILES['image']['name'])){
	/***********************************************************
		1 - Upload Original Image To Server
		***********************************************************/
		//Get Name | Size | Temp Location
		$ImageName = $_FILES['image']['name'];
		$ImageSize = $_FILES['image']['size'];
		$ImageTempName = $_FILES['image']['tmp_name'];
		//Get File Ext
		$ImageType = @explode('/', $_FILES['image']['type']);
			$type = $ImageType[1]; //file type
		//Set Upload directory
			$uploaddir = $_SERVER['DOCUMENT_ROOT'].'/'.$folderName.'/cover_pic/';
		//Set File name
			$file_temp_name = $profile_id1.'_original.'.md5(time()).'n'.$type; //the temp file name
			$fullpath = "$uploaddir/".$file_temp_name; // the temp file path
			$file_name = $profile_id1.'_temp.jpeg'; //$profile_id.'_temp.'.$type; // for the final resized image
			$fullpath_2 = "$uploaddir/".$file_name; //for the final resized image
		//Move the file to correct location
			$move = move_uploaded_file($ImageTempName ,$fullpath) ;
			chmod($fullpath, 0777);
   		//Check for valid uplaod
			if (!$move) {
				die ('File didnt upload');
			} else {
				$imgSrcCover= site_url().'/cover_pic/'.$file_name.'?x='.rand(); // the image to display in crop area
				$msg= "Upload Complete!";  	//message to page
				$src = $file_name;	 		//the file name to post from cropping form to the resize
			}

	/***********************************************************
		2  - Resize The Image To Fit In Cropping Area
		***********************************************************/
			//get the uploaded image size
		clearstatcache();
		$original_size = getimagesize($fullpath);
		$original_width = $original_size[0];
		$original_height = $original_size[1];
			// Specify The new size
				$main_width = 950; // set the width of the image
				$main_height = $original_height / ($original_width / $main_width);	// this sets the height in ratio
			//create new image using correct php func
				if($_FILES["image"]["type"] == "image/gif"){
					$src2 = imagecreatefromgif($fullpath);
				}elseif($_FILES["image"]["type"] == "image/jpeg" || $_FILES["image"]["type"] == "image/jpg"){
					$src2 = imagecreatefromjpeg($fullpath);
				}elseif($_FILES["image"]["type"] == "image/png"){
					$src2 = imagecreatefrompng($fullpath);
				}else{
					$msg .= "There was an error uploading the file. Please upload a .jpg, .gif or .png file. <br />";
				}
			//create the new resized image
				$main = imagecreatetruecolor($main_width,$main_height);
				imagecopyresampled($main,$src2,0, 0, 0, 0,$main_width,$main_height,$original_width,$original_height);
			//upload new version
				$main_temp = $fullpath_2;
				imagejpeg($main, $main_temp, 90);
				chmod($main_temp,0777);
			//free up memory
				imagedestroy($src2);
				imagedestroy($main);
				@imagedestroy($fullpath);
				@ unlink($fullpath); // delete the original upload

		}//ADD Image
		$_POST['coverType'] = "";
	}


/***********************************************************
	3- Cropping & Converting The Image To Jpg
	***********************************************************/
	if (isset($_POST['coverCropType']) && $_POST['coverCropType'] == '444' ){
		if (isset($_POST['x']) && $_POST['x']>=0){

		//the file type posted
			$type = $_POST['typeCover'];
		//the image src

			$src = $_SERVER['DOCUMENT_ROOT'].'/'.$folderName.'/cover_pic/'.$_POST['srcCover'];

			$finalname = md5(time());
				$xx = $_POST['x']+10;
				$yy = $_POST['y']+10;
			//the target dimensions 150x150
				$targ_w = 950;
				$targ_h = 300;
			//quality of the output
				$jpeg_quality = 90;

				$user_id = get_current_user_id();
				$meta_key = 'cover_pic_meta';
			//echo '<script>alert("hi file uploaded!.'.$type.'.");</script>';
			if($type == 'jpg' || $type == 'jpeg' || $type == 'JPG' || $type == 'JPEG'){

			//create a cropped copy of the image
				$img_r = imagecreatefromjpeg($src);
				$dst_r = imagecreatetruecolor( $targ_w, $targ_h );
				imagecopyresampled($dst_r,$img_r,0,0,$xx,$yy,$targ_w,$targ_h,$_POST['w'],$_POST['h']);

				update_user_meta( $user_id, $meta_key, $finalname.'n.jpeg');
			//save the new cropped version
				imagejpeg($dst_r, $_SERVER['DOCUMENT_ROOT']."/".$folderName."/cover_pic/".$finalname."n.jpeg", 90);

			}else if($type == 'png' || $type == 'PNG'){

			//create a cropped copy of the image
				$img_r = imagecreatefrompng($src);
				$dst_r = imagecreatetruecolor( $targ_w, $targ_h );
				imagecopyresampled($dst_r,$img_r,0,0,$xx,$yy,$targ_w,$targ_h,$_POST['w'],$_POST['h']);

				update_user_meta( $user_id, $meta_key, $finalname.'n.jpeg');
			//save the new cropped version
				imagejpeg($dst_r, $_SERVER['DOCUMENT_ROOT']."/".$folderName."/cover_pic/".$finalname."n.jpeg", 90);

			}else if($type == 'gif' || $type == 'GIF'){

			//create a cropped copy of the image
				$img_r = imagecreatefromgif($src);
				$dst_r = imagecreatetruecolor( $targ_w, $targ_h );
				imagecopyresampled($dst_r,$img_r,0,0,$xx,$yy,$targ_w,$targ_h,$_POST['w'],$_POST['h']);

				update_user_meta( $user_id, $meta_key, $finalname.'n.jpeg');
			//save the new cropped version
				imagejpeg($dst_r, $_SERVER['DOCUMENT_ROOT']."/".$folderName."/cover_pic/".$finalname."n.jpeg", 90);

			}

			//free up memory
				imagedestroy($img_r); // free up memory
				imagedestroy($dst_r); //free up memory
				@ unlink($src); // delete the original upload

		} // post x
		$_POST['coverCropType'] = "";
		echo '<script>window.location.assign(window.location.href);</script>';
	}
?>



<script>

	//JCrop Bits  Cover Photo
	jquery(function(){

		jquery('#jcrop_target_cover').Jcrop({
			setSelect:   [ 800,250,40,60 ],
			bgColor:     'black',
			bgOpacity:   .4,
			onSelect: updateCoords
		});

	});

	function updateCoords(c)
	{
		jquery('#x').val(c.x);
		jquery('#y').val(c.y);
		jquery('#w').val(c.w);
		jquery('#h').val(c.h);
	};

	function checkCoords()
	{
		if (parseInt(jquery('#w').val())) return true;
		alert('Please select a crop region then press submit.');
		return false;
	};
	//End JCrop Bits

	function cancelCrop(){
		//Refresh page
		top.location = 'index.php?cancel';
		return false;
	}


	jQuery(document).ready(function(){

		jQuery("#coverImage").on("change", function() {
			jQuery("#coverForm").submit();
		});

		jQuery('#cover_click').click(function(e){
			e.preventDefault();
			jQuery('#coverImage').click();

		});

		jQuery('#formExample').hide();

		jQuery( ".opener" ).click(function() {

			if(jQuery(this).find('.futurestreamings')) {
				var objdlg = jQuery(this).find('.futurestreamings');
				if (!objdlg.is(":visible")){
					jQuery(".arrow").css("display","none");
					objdlg.show();
				} else {
					jQuery(".arrow").css("display","block");
					objdlg.hide();
				}
			}
		});
	});
</script>
<!-- Cover Photo-->
<!-- <div id="Overlay" style=" width:100%; height:100%; background-color:rgba(0,0,0,.5); border:0px #990000 solid; position:absolute; top:0px; left:0px; z-index:2000; display:none;"></div>-->

<div id="wrapper" style="width:100%;text-area:center; position:relative;background-color:#3498db;">

	<div id="formExample">
		<form action="" method="post" id="coverForm" enctype="multipart/form-data" style="display:none" >

			<input type="file" id="coverImage" name="image" accept='.jpeg,.jpg,.gif,.png,.JPEG,.JPG,.GIF,.PNG' />
			<input type="hidden" name="coverType" value="333" />
			<input type="submit" value="submit"/>

		</form>

	</div>

	<?php  if(isset($imgSrcCover)){ //if an image has been uploaded display cropping area?>
	<script>
		jquery('#Overlay').show();
		jquery('#formExample').hide();
	</script>
	<div id="CroppingContainer" style="width: 100%; position:relative; overflow:hidden; border:2px #666 solid; z-index:888;">

		<div id="CroppingArea" style="width: 100%;max-height:300px; position:relative; overflow:hidden; float:left;">
			<img src="<?php echo $imgSrcCover;?>" border="0" id="jcrop_target_cover" style="border:0px #990000 solid; position:relative; margin:0px 0px 0px 0px; padding:0px; " />
		</div>
		<div id="CropImageForm" style="float:left;" >
			<form method="post" onsubmit="return checkCoords();">
				<input type="hidden" id="x" name="x" />
				<input type="hidden" id="y" name="y" />
				<input type="hidden" id="w" name="w" />
				<input type="hidden" id="h" name="h" />
				<input type="hidden" value="jpeg" name="typeCover"/>
				<input type="hidden" value="<?php echo $src; ?>" name="srcCover"/>
				<input type="hidden" name="coverCropType" value="444" />
				<input type="submit" value="Crop Image" class="btn btn-success" style="margin:5px;" />
			</form>
		</div>
		<div id="CropImageForm2" style="float:left;" >
			<form action="" method="post" onsubmit="return cancelCrop();">
				<input type="submit" value="Cancel Crop" class="btn btn-danger" style="margin:5px;" />
			</form>
		</div>


	</div><!-- CroppingContainer -->
	<?php } ?>

</div>

<?php
// Set Author ID
$author = get_user_by( 'slug', get_query_var( 'author_name' ) );
$author_info = get_userdata( $author->ID );
?>

<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/font-awsome/css/font-awesome.min.css">

<div class="user-profile">


	<?php
	$user_id2 = $profile_id;
	$meta_key2 = 'cover_pic_meta';
	$fileName2 = get_user_meta( $user_id2, $meta_key2, true );
	if($fileName2){
		$fileName3 = site_url().'/cover_pic/'.$fileName2;
	}else{

		$fileName3 = 'http://2.gravatar.com/avatar/2e31f18d72d237a9f4821b52398337ab?s=14&d=mm&r=g';
	}


	?>

	<div class="cover_image" style="black none repeat scroll 0 0; height:261px; width:100%; background:url(<?php echo $fileName3;?>) no-repeat 0px 0px;  > ">

	</div>
	<?php if(get_current_user_id() == $profile_id){ ?>
	<i class="fa fa-pencil-square"  id="cover_click" style="right: 18.5%; top: 205px;"></i>
	<?php } ?>
	<h6 class="title-bar"><span><?php echo get_the_author_meta('nickname', $author->ID ); ?></span></h6>

	<?php

	$user_id = $profile_id;
	$meta_key = 'profile_pic_meta';
	$fileName = get_user_meta( $user_id, $meta_key, true );

	if($fileName){
		$fileName1 = site_url().'/profile_pic/'.$fileName;
	}
	else{
		$fileName1 = 'http://2.gravatar.com/avatar/2e31f18d72d237a9f4821b52398337ab?s=14&d=mm&r=g';
	}

	?>
	<div class="profile_b">
		<img src="<?php echo $fileName1; ?>" class="profile_pic"/>
		<?php if(get_current_user_id() == $profile_id){ ?>
		<i class="fa fa-pencil-square" id="profile_click"></i>
		<?php } ?>
	</div>

	<div class="clear"></div>


</div>
<!-- End User info -->


<!-- Start Uploaded Videos by user ??-Arun what is this? -->
<div class="layout-2">

	<!-- start tabs -->

	<div>
		<?php
   //check if the paypal account is posted
		if(isset($_POST['operation']) && $_POST['operation']=='addpaypalaccount'){
	 //check if the user have provided paypal email in the past
			$datapaypal1 = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."users_merchant_accounts WHERE user_id =".$_POST['user_id']." and status=1");
			if($datapaypal1 && $wpdb->num_rows>0){
				$wpdb->update(
					$wpdb->prefix."users_merchant_accounts",
					array(
		     'paypal_email' => $_POST['paypal_email']	// string
		     ),
					array( 'user_id' => $_POST['user_id'] )
					);
			} else {
	    //insert a new one
				$insert = $wpdb->insert($wpdb->prefix."users_merchant_accounts",array(
					'user_id'	    => $_POST['user_id'],
					'paypal_email'	=> $_POST['paypal_email'],
					));
			}

			$datarevshare = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."users_revenue_status WHERE user_id =".$_POST['user_id']);
			if($datarevshare && $wpdb->num_rows>0){
				$wpdb->update(
					$wpdb->prefix."users_revenue_status",
					array(
					 'revenue_status' => $_POST['rev-share']	// string
					 ),
					array( 'user_id' => $_POST['user_id'] )
					);
			}else{
				$wpdb->insert($wpdb->prefix."users_revenue_status",array(
					'user_id'	    => $_POST['user_id'],
					'revenue_status'=> 1
				));
			}

		}

   //check if the user has defined a paypal  account into the system
		$datapaypal = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."users_merchant_accounts WHERE user_id =".$author->ID." and status=1");
		$paypalacc='';
		if($datapaypal){
			foreach ($datapaypal as $reg_datagen){
				$paypalacc=$reg_datagen->paypal_email;
			}
		}
		?>

		<?php
  	//Check whether to display review page

		if( get_query_var( 'display_page' ) == 'review' ){
			$review_active = 'active';
			$new_store_active = false;
			$home_active = false;
			$vid_active = false;
			$forum_active = false;
			$new_file_active = false;
			$new_book_active = false;
			$fav_active = false;
		}
		else if( get_query_var( 'display_page' ) == 'file' ){
			$new_file_active = 'active';
			$new_store_active = false;
			$home_active = false;
			$vid_active = false;
			$forum_active = false;
			$review_active = false;
			$new_book_active = false;
			$fav_active = false;
		}
		else if( get_query_var( 'display_page' ) == 'video' ||  get_query_var( 'display_tab' ) == 'video'){
			$vid_active = 'active';
			$new_store_active = false;
			$new_file_active = false;
			$home_active = false;
			$forum_active = false;
			$review_active = false;
			$new_book_active = false;
			$fav_active = false;
		}
		else if( get_query_var( 'display_page' ) == 'book' ){
			$new_book_active = 'active';
			$new_store_active = false;
			$vid_active = false;
			$new_file_active = false;
			$home_active = false;
			$forum_active = false;
			$review_active = false;
			$fav_active = false;

		}		else if( get_query_var( 'display_page' ) == 'store' ){
			$new_store_active = 'active';
			$new_book_active=false;
			$vid_active = false;
			$new_file_active = false;
			$home_active = false;
			$forum_active = false;
			$review_active = false;
			$fav_active = false;

		}
		?>

		<script type="text/javascript">
 function ChangeUrl(page, url) {
  if (typeof (history.pushState) != "undefined") {
   var obj = { Page: page, Url: url };
   history.pushState(obj, obj.Page, obj.Url);
  }else{
   $( location ).attr("href", url);
  }
 }
 //window.location.assign(window.location.href);
jQuery(document).ready(function($) {
	<?php if(isset($_GET['display_page'])){ ?>
		$( "a[href='#wptabsy-content-1']" ).trigger( "click" );
		$( "#wptabsy-content-1" ).trigger( "click" );
		var url     = window.location.href;
		var split = url.split("?");
		//FA: comment this out: ChangeUrl(split[0], split[0]);
	<?php } ?>

	<?php if(isset($_GET['noti'])){ ?>
		$( ".notifications-section a" ).trigger( "click" );
		var url     = window.location.href;
		var split = url.split("?");
		ChangeUrl(split[0], split[0]);
	<?php } ?>
});
/*jQuery(document).ready(function($) {

 	$(".videostab").live("click", function(){
		//$("a:contains('display_page=file')").html($("a:contains('display_page=file')").html().replace('display_page=file', 'display_page=video'));
		//$('.page-numbers').html($('.page-numbers').html().replace('display_page=file','display_page=video'));
		alert($(".page-numbers").attr("href"));
	});

	$(".filestab").live("click", function(){
		//$("a:contains('display_page=video')").html($("a:contains('display_page=video')").html().replace('display_page=video', 'display_page=file'));
		//$('.page-numbers').html($('.page-numbers').html().replace('display_page=video','display_page=file'));
		alert($(".page-numbers").attr("href"));
	});

});*/
</script>
		<!-- Nav tabs -->
		<ul class="nav nav-tabs" role="tablist">
			<li role="presentation" class="<?= $home_active;?>"><a href="#home" class="hometab" aria-controls="home" role="tab" data-toggle="tab">Home</a></li>
			<?php if($author->ID == get_current_user_id()) { ?>
			<li role="presentation"  class="<?= $forum_active;?>"><a href="#forum" class="forumtab" aria-controls="forum" role="tab" data-toggle="tab">Profile Info</a> </li>
			<?php } ?>
			<li role="presentation"  class="<?= $vid_active;?>"><a href="#videos" class="videostab" aria-controls="videos" role="tab" data-toggle="tab">Videos</a> </li>
            <li role="presentation" class="<?= $new_file_active;?>"><a href="#new_files" class="filestab" aria-controls="new_files" role="tab" data-toggle="tab">Files</a></li>
			<li role="presentation" class="<?= $new_book_active;?>"><a href="#new_books" aria-controls="new_books" role="tab" data-toggle="tab" class="bookstab" >Books</a></li>
			<li role="presentation" class="<?= $new_store_active;?>"><a href="#store" aria-controls="new_store" role="tab" data-toggle="tab" class="store" >Store</a></li>
			<li role="presentation"><a href="#playlists" aria-controls="playlists" role="tab" class="playlisttab" data-toggle="tab">Playlists</a></li>
			<li role="presentation" class="<?= $fav_active;?>"><a href="#favoriteinf" aria-controls="favorite" role="tab" data-toggle="tab" class="favouritetab">Favorites</a></li>
			<li style="display:none;" role="presentation" class="<?= $review_active;?>"><a href="#reviews" aria-controls="reviews" role="tab" class="reviewtab" data-toggle="tab">Reviews</a> </li>
			<li role="presentation"><a href="#discussion" aria-controls="discussion" role="tab" data-toggle="tab" class="discusstab">Discussion</a></li>
			<?php if($author->ID != get_current_user_id() && $paypalacc!='') { ?>
			<li role="presentation"><a href="#donate" aria-controls="donate" role="tab" class="donatetab" data-toggle="tab">Donate</a></li>
			<?php } else { ?>
			<?php if($author->ID == get_current_user_id()){ ?>
			<li role="presentation"><a href="#donate" aria-controls="donate" class="donatetab" role="tab" data-toggle="tab">Donate</a></li>
			<?php } ?>
			<?php } ?>
			<?php if($author->ID == get_current_user_id()) { ?>

			<li role="presentation"><a href="#suscriptioninf" aria-controls="suscription" role="tab" data-toggle="tab">Suscriptions</a></li>
			<?php } ?>
			<?php if($author->ID == get_current_user_id()) { ?>
			<!--<li role="presentation"><a href="#analytics" aria-controls="analytics" role="tab" data-toggle="tab">Analytics</a></li>-->
			<?php } ?>
			<?php if($author->ID == get_current_user_id()) { ?>
			<li role="presentation"><a href="#friends" aria-controls="friends" role="tab" data-toggle="tab">Friends</a></li>
			<?php } ?>
			<?php if($author->ID == get_current_user_id()) { ?>
			<li role="presentation" class="notifications-section"><a href="#notifications" aria-controls="notifications" role="tab" data-toggle="tab">Notifications</a></li>
			<?php } ?>
			<?php if($author->ID == get_current_user_id()) { ?>
			<li role="presentation"><a href="#collaboration" aria-controls="collaboration" role="tab" data-toggle="tab">Collaboration</a></li>
			<?php } ?>

		</ul>

		<!-- Tab panes -->
		<div class="tab-content">

          	<!-- start  store list -->
			<div role="tabpanel" class="tab-pane <?= $new_store_active; ?>" id="store">
	<h6 class="title-bar"><span>Products By <?php echo get_the_author_meta('nickname', $author->ID ); ?></span></h6>

	<div class="alert alert-success v-d" style="display:none;">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<strong>Success!</strong> Store is Deleted.
	</div>

	<?php
	$nothing = true;
	$selected = false;
	$current = true;
	$views=false;
	$endorsements=false;
	$favourites=false;
	$comments=false;

	$category = get_category( get_query_var( 'cat' ) );
	$catid = $category->cat_ID;
	if(isset($_GET['store']) && $_GET['store']!=''){
		$nothing = false;
		$store = filter_input(INPUT_GET, 'store', FILTER_SANITIZE_SPECIAL_CHARS);
		echo '<br>';
		if($store=='views'):
			$views=true;
		$my_query = new WP_Query( array(
						//'posts_per_page' => 10,
			'post_type' => 'product',
			'post_author' => $profile_id,
						//'cat'=>$catid,
			'meta_key' => 'popularity_count',
			'orderby'	=> 'meta_value_num',
			'order'		=> 'DESC',
			'meta_query' => array(
				array(
					'key' => 'popularity_count',
					)
				),
			'paged' => $paged,
			));


		elseif($store=='endorsements'):
			$endorsements=true;

		$limit = 'limit ';
		$limit .= $paged*10 ;
		$limit .= ', 10';

				//$querystr = "SELECT p.*,endocount FROM wp_posts p LEFT join (select *,count(*) as endocount from wp_endorsements group by post_id ) endors on p.ID = endors.post_id where p.post_type = 'post' AND p.post_status = 'publish' AND p.post_author = ' ".$profile_id."' order by endors.endocount desc $limit";

		$querystr = "SELECT p.*,endocount FROM wp_posts p left join (select *,count(*) as endocount from wp_endorsements group by post_id ) endors on p.ID = endors.post_id where p.post_type = 'product' AND p.post_status = 'publish' AND p.post_author = '".$profile_id."' order by endors.endocount desc ". $limit;

		$pageposts = $wpdb->get_results($querystr, OBJECT);

		elseif($store=='favourites'):
			$favourites=true;

		$limit = 'limit ';
		$limit .= $paged*10 ;
		$limit .= ', 10';

		$querystr = "SELECT p.*,favcount FROM wp_posts p left join (select *,count(*) as favcount from wp_favorite group by post_id ) favorite on p.ID = favorite.post_id where p.post_type = 'product' AND p.post_status = 'publish' AND p.post_author = '".$profile_id."' order by favorite.favcount desc $limit";

		$pageposts = $wpdb->get_results($querystr, OBJECT);

				//$selected = true;
		elseif($store=='comments'):
			$my_query = new WP_Query( array(
				'post_type' => 'product',
				'post_author' => $profile_id,
				'cat'=>$catid,
				'orderby'=> 'comment_count',
				'order'		=> 'DESC',
				'paged' => $paged,
				));

		$comments=true;
		else:
			$selected = true;
		endif;
		$current = false;
	}

	?>
	<div class="cover-twoblocks">
		<div class="store-div">
		<form method="get" id="store-form">
			<p><label>Currently Sorted By : </label>
				<select class="store" name="store">
					<option value="recent" <?= ($selected)?'selected':'';?>>Recent</option>
					<option value="views" <?= ($views)?'selected':'';?>>Views</option>
					<option value="endorsements" <?= ($endorsements)?'selected':'';?>>Endorsements</option>
					<option value="favourites" <?= ($favourites)?'selected':'';?>>Favorites</option>
					<option value="comments" <?= ($comments)?'selected':'';?>>comments</option>
				</select>
			</p>
		</form>
		</div>

		<div class="well well-sm">
			<strong>Views</strong>
			<div class="btn-group">
				<a href="#" id="liststore" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th-list">
				</span>List</a> <a href="#" id="gridstore" class="btn btn-default btn-sm"><span
					class="glyphicon glyphicon-th"></span>Grid</a>
			</div>
		</div>
    </div>
	<script>
		jQuery('.store').change(function(){
			this.form.submit();
		})
	</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#products .item').addClass('grid-group-item');
    $('#liststore').click(function(event){
    	event.preventDefault();
    	$('#products .item').addClass('list-group-item changeStyleList');

    });
    $('#gridstore').click(function(event){
    	event.preventDefault();
    	$('#products .item').removeClass('list-group-item changeStyleList');
    	$('#products .item').addClass('grid-group-item changeStyleGrid');
    });
});
</script>

	<?php

	if($current || $selected || $nothing):

		/* ================================================================== */
	/* Start of loop */
	/* ================================================================== */

$paged = ( get_query_var( 'page' ) ) ? absint( get_query_var( 'page' ) ) : 1;

$cat=isset($_GET['store']) ? $_GET['store'] : '';

if($cat=='store')
$start = ($paged==1) ? 0 : intval($paged-1) * 12;
else
$start=0;


$temp= 'SELECT * FROM  '.$wpdb->prefix.'posts WHERE post_type="product" AND post_author='.$profile_id.' ORDER BY STR_TO_DATE( '.$wpdb->prefix. 'posts.post_date_gmt , "%Y-%m-%d %H:%i:%s")  DESC';

$tquery = $temp.' LIMIT  ' . $start .', 12';
//create custom loop for videos
$result = $wpdb->get_results( $tquery );
	 //echo  $wpdb->num_rows . 'Rows Found';


$total=$wpdb->query($temp);

	$x = 0;
	if ($result){
	echo '<div id="products" class="list-group">';
	foreach ($result as $data){
	$x++;
       $postID = $data->ID;
	   $post=get_post($postID);
	   $privacyOption   = get_post_meta( $postID, 'privacy-option' );
	   $selectUsersList = get_post_meta( $postID, 'select_users_list' );
	   $post_author     = $post->post_author;
	   $user_ID         = get_current_user_id();
	   $selectUsersList = explode( ",", $selectUsersList[0] );
	?>
	<!-- Start Layout Wrapper -->

<?php
     if(!is_user_logged_in())
		{  // case where user is not logged in
	             if(isset($privacyOption[0]) && ($privacyOption[0] != "private"))
				  {
					     ?>
						    <div class="item col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 post-<?php the_ID(); ?>">
						      <div class="thumbnail layout-2-wrapper solid-bg">
						 <?php
				  }
				  else
				  {
					    if(!isset($privacyOption[0]))
						{
							 ?>
							  <div class="item col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 post-<?php the_ID(); ?>">
								  <div class="thumbnail layout-2-wrapper solid-bg">
							 <?php
						}
				  }
		}
		else
		{  // case where user is logged in
			 if($post_author == $user_ID)
			 {    // Case where logged in User is same as Video Author User
				     ?>
					     <div class="item col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 post-<?php the_ID(); ?>">
						  <div class="thumbnail layout-2-wrapper solid-bg">
					 <?php
			 }
			 else
			 {    // Case where logged in User is not same as Video Author User
				  if(isset($privacyOption[0]) && $privacyOption[0] != "private")
				  {
					    ?>
						    <div class="item col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 post-<?php the_ID(); ?>">
						      <div class="thumbnail layout-2-wrapper solid-bg">
						 <?php
				  }
				  else
				  {
					  if(!isset($privacyOption[0]))
					  {
						    ?>
							 <div class="item col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 post-<?php the_ID(); ?>">
						      <div class="thumbnail layout-2-wrapper solid-bg">
						 <?php
					  }
                      else
					  {
						    if( is_array($selectUsersList) and count($selectUsersList) > 0 )
							   {   // case where user access list is available
									 if( in_array($user_ID, $selectUsersList) )
									 {
										   ?>
										   <div class="item col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 post-<?php the_ID(); ?>">
											  <div class="thumbnail layout-2-wrapper solid-bg">
										 <?php
									 }
									 else
									 {
									 }
							   }
							   else
							   {  }
					  }
				  }
			 }
		}
	    if(!is_user_logged_in())
		{  // case where user is not logged in

	             if(isset($privacyOption[0]) && ($privacyOption[0] != "private"))
				  {


					   ?>
						   <div class="grid-6 first">
								<div class="image-holder">
									<a href="<?php echo get_permalink(); ?>">
										<div class="hover-item"></div>
									</a>
									<?php
									if( has_post_thumbnail() ){
										the_post_thumbnail('small-thumb', array( 'class' => 'layout-2-thumb' ) );
									}else{
										echo '<img src="'.get_template_directory_uri().'/images/placeholder.jpg" class="layout-2-thumb">';
									}
									?>
								</div>
								<div class="layout-title-box layout-title-box-<?php the_ID(); ?>" style="display:block;">
									<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php echo ctpreview( $content = get_the_title() , $length = 39 , $message = 'No Title available' , $html = ' [...]' ); ?></a></h6>
									<ul class="bottom-detailsul">
										<?php if($profile_id == get_current_user_id()){ ?>
										<li><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>"><i class="fa fa-trash-o"></i> Delete</a></li>
										<?php } ?>
										<li><a class="detailsblock-btn" data-modal-id="myModal-<?php the_ID(); ?>"><i class="fa fa-info-circle" aria-hidden="true"></i> Details</a></li>
									</ul>
								</div>
							</div>
							<script type="text/javascript">
$(function(){

var appendthis =  ("<div class='modal-overlayblockdiv'></div>");

	$('a[data-modal-id]').click(function(e) {
		e.preventDefault();
    $("body").append(appendthis);
    $(".modal-overlay").fadeTo(500, 0.7);
    //$(".js-modalbox").fadeIn(500);
		var modalBox = $(this).attr('data-modal-id');
		$('#'+modalBox).fadeIn($(this).data());
	});


$(".js-modal-close, .modal-overlay").click(function() {
    $(".modal-box, .modal-overlay").fadeOut(500, function() {
        $(".modal-overlay").remove();
    });

});

$(window).resize(function() {
    $(".modal-box").css({
        top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
        left: ($(window).width() - $(".modal-box").outerWidth()) / 2
    });
});

$(window).resize();

});
</script>

			<div class="modal-box" id="myModal-<?php the_ID(); ?>">
			<div class="modal-body">
			<a class="js-modal-close close">×</a>
			<div class="layout-2-details layout-2-details-<?php the_ID(); ?>">
				<div class="image-holder">
					<a href="<?php echo get_permalink(); ?>">
						<div class="hover-item"></div>
					</a>

					<?php
					if( has_post_thumbnail() ){
						the_post_thumbnail('small-thumb', array( 'class' => 'layout-2-thumb' ) );
					}else{
						echo '<img src="'.get_template_directory_uri().'/images/placeholder.jpg" class="layout-2-thumb">';
					}
					?>

				</div>
				<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h6>
				<?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
				<div class="video_excerpt_text one"><h4 class="tittle-h4">Description 2</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = '' , $message = 'No Description available' , $html = ' [...]' );?></div></div>
				<div class="video_excerpt_text two"><h4 class="tittle-h4">Reference</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_post_meta(get_the_ID(),'video_options_refr',true) , $length = '' , $message = 'No referance given' , $html = ' [...]' );?></div></div>
				<ul class="stats">
					<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
					<li><?php videopress_countviews( get_the_ID() ); ?></li>
					<li><i class="fa fa-wrench"></i> <?php $result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
						echo count($result); ?> Endorsments</li>
					<!-- favourite section -->
					<li><i class="fa fa-heart"></i>
					<?php
					  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
					  $result1 = $wpdb->get_results($sql_aux);
					  echo count($result1);
					?> Favorites</li>
					<!-- end favorite -->
					<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
					<li><?php comments_number() ?></li>

				</ul>
				<div class="clear"></div>
				<p><?php echo videopress_content('240'); ?></p>

					<?php if($profile_id == get_current_user_id()){ ?>
					<p><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>"><i class="fa fa-trash-o"></i> Delete</a></p>
					<?php } ?>


			</div></div></div>

			<div class="layout-2-details layout-2-details-<?php the_ID(); ?>" style="display:none;">
				<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h6>
				<?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
				<div class="video_excerpt_text one"><h4 class="tittle-h4">Description</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = 100 , $message = 'No Description available' , $html = '<a data-modal-id="myModal-'.get_the_ID().'"> [...]</a>' ); ?></div></div>
				<ul class="stats">
					<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
					<li><?php videopress_countviews( get_the_ID() ); ?></li>
					<li><i class="fa fa-wrench"></i> <?php $result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
						echo count($result); ?> Endorsments</li>
					<!-- favourite section -->
					<li><i class="fa fa-heart"></i>
					<?php
					  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
					  $result1 = $wpdb->get_results($sql_aux);
					  echo count($result1);
					?> Favorites</li>
					<!-- end favorite -->
					<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
					<li><?php comments_number() ?></li>

				</ul>
				<div class="clear"></div>
				<p><?php echo videopress_content('240'); ?></p>

					<?php if($profile_id == get_current_user_id()){ ?>
					<p><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>"><i class="fa fa-trash-o"></i> Delete</a></p>
					<?php } ?>


			</div>

			<div class="clear"></div>
					   <?php
				  }
				  else
				  {
					    if(!isset($privacyOption[0]))
						{
							?>
							   <div class="grid-6 first">
									<div class="image-holder">
										<a href="<?php echo get_permalink(); ?>">
											<div class="hover-item"></div>
										</a>
										<?php
										if( has_post_thumbnail() ){
											the_post_thumbnail('small-thumb', array( 'class' => 'layout-2-thumb' ) );
										}else{
											echo '<img src="'.get_template_directory_uri().'/images/placeholder.jpg" class="layout-2-thumb">';
										}
										?>
									</div>
									<div class="layout-title-box layout-title-box-<?php the_ID(); ?>" style="display:block;">
										<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php echo ctpreview( $content = get_the_title() , $length = 39 , $message = 'No Title available' , $html = ' [...]' ); ?></a></h6>
										<ul class="bottom-detailsul">
											<?php if($profile_id == get_current_user_id()){ ?>
											<li><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>"><i class="fa fa-trash-o"></i> Delete</a></li>
											<?php } ?>
											<li><a class="detailsblock-btn" data-modal-id="myModal-<?php the_ID(); ?>"><i class="fa fa-info-circle" aria-hidden="true"></i> Details</a></li>
										</ul>
									</div>
								</div>
								<script type="text/javascript">
$(function(){

var appendthis =  ("<div class='modal-overlayblockdiv'></div>");

	$('a[data-modal-id]').click(function(e) {
		e.preventDefault();
    $("body").append(appendthis);
    $(".modal-overlay").fadeTo(500, 0.7);
    //$(".js-modalbox").fadeIn(500);
		var modalBox = $(this).attr('data-modal-id');
		$('#'+modalBox).fadeIn($(this).data());
	});


$(".js-modal-close, .modal-overlay").click(function() {
    $(".modal-box, .modal-overlay").fadeOut(500, function() {
        $(".modal-overlay").remove();
    });

});

$(window).resize(function() {
    $(".modal-box").css({
        top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
        left: ($(window).width() - $(".modal-box").outerWidth()) / 2
    });
});

$(window).resize();

});
</script>

			<div class="modal-box" id="myModal-<?php the_ID(); ?>">
			<div class="modal-body">
			<a class="js-modal-close close">×</a>
			<div class="layout-2-details layout-2-details-<?php the_ID(); ?>">
				<div class="image-holder">
					<a href="<?php echo get_permalink(); ?>">
						<div class="hover-item"></div>
					</a>

					<?php
					if( has_post_thumbnail() ){
						the_post_thumbnail('small-thumb', array( 'class' => 'layout-2-thumb' ) );
					}else{
						echo '<img src="'.get_template_directory_uri().'/images/placeholder.jpg" class="layout-2-thumb">';
					}
					?>

				</div>
				<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h6>
				<?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
				<div class="video_excerpt_text one"><h4 class="tittle-h4">Description 2</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = '' , $message = 'No Description available' , $html = ' [...]' );?></div></div>
				<div class="video_excerpt_text two"><h4 class="tittle-h4">Reference</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_post_meta(get_the_ID(),'video_options_refr',true) , $length = '' , $message = 'No referance given' , $html = ' [...]' );?></div></div>
				<ul class="stats">
					<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
					<li><?php videopress_countviews( get_the_ID() ); ?></li>
					<li><i class="fa fa-wrench"></i> <?php $result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
						echo count($result); ?> Endorsments</li>
					<!-- favourite section -->
					<li><i class="fa fa-heart"></i>
					<?php
					  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
					  $result1 = $wpdb->get_results($sql_aux);
					  echo count($result1);
					?> Favorites</li>
					<!-- end favorite -->
					<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
					<li><?php comments_number() ?></li>
				</ul>
				<div class="clear"></div>
				<p><?php echo videopress_content('240'); ?></p>

					<?php if($profile_id == get_current_user_id()){ ?>
					<p><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>"><i class="fa fa-trash-o"></i> Delete</a></p>
					<?php } ?>


			</div></div></div>

			<div class="layout-2-details layout-2-details-<?php the_ID(); ?>" style="display:none;">
				<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h6>
				<?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
				<div class="video_excerpt_text one"><h4 class="tittle-h4">Description</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = 100 , $message = 'No Description available' , $html = '<a data-modal-id="myModal-'.get_the_ID().'"> [...]</a>' ); ?></div></div>
				<ul class="stats">
					<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
					<li><?php videopress_countviews( get_the_ID() ); ?></li>
					<li><i class="fa fa-wrench"></i> <?php $result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
						echo count($result); ?> Endorsments</li>
					<!-- favourite section -->
					<li><i class="fa fa-heart"></i>
					<?php
					  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
					  $result1 = $wpdb->get_results($sql_aux);
					  echo count($result1);
					?> Favorites</li>
					<!-- end favorite -->
					<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
					<li><?php comments_number() ?></li>
				</ul>
				<div class="clear"></div>
				<p><?php echo videopress_content('240'); ?></p>

					<?php if($profile_id == get_current_user_id()){ ?>
					<p><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>"><i class="fa fa-trash-o"></i> Delete</a></p>
					<?php } ?>


			</div>

			<div class="clear"></div>
						   <?php
						}
				  }
		}
		else
		{  // case where user is logged in
			 if($post_author == $user_ID)
			 {    // Case where logged in User is same as Video Author User
				   ?>
				       <div class="grid-6 first">
							<div class="image-holder">
								<a href="<?php echo get_permalink(); ?>">
									<div class="hover-item"></div>
								</a>
								<?php
								if( has_post_thumbnail() ){
									the_post_thumbnail('small-thumb', array( 'class' => 'layout-2-thumb' ) );
								}else{
									echo '<img src="'.get_template_directory_uri().'/images/placeholder.jpg" class="layout-2-thumb">';
								}
								?>
							</div>
							<div class="layout-title-box layout-title-box-<?php the_ID(); ?>" style="display:block;">
								<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php echo ctpreview( $content = get_the_title() , $length = 39 , $message = 'No Title available' , $html = ' [...]' ); ?></a></h6>
								<ul class="bottom-detailsul">
									<?php if($profile_id == get_current_user_id()){ ?>
									<li><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>"><i class="fa fa-trash-o"></i> Delete</a></li>
									<?php } ?>
									<li><a class="detailsblock-btn" data-modal-id="myModal-<?php the_ID(); ?>"><i class="fa fa-info-circle" aria-hidden="true"></i> Details</a></li>
									<li><a class="changesettingsblock-btn" data-modal-id="myModalPrivacy-<?php the_ID(); ?>"><i class="fa fa-info-circle" aria-hidden="true"></i>Privacy</a></li>
								</ul>
							</div>
					    </div>
						<script type="text/javascript">
$(function(){

var appendthis =  ("<div class='modal-overlayblockdiv'></div>");

	$('a[data-modal-id]').click(function(e) {
		e.preventDefault();
    $("body").append(appendthis);
    $(".modal-overlay").fadeTo(500, 0.7);
    //$(".js-modalbox").fadeIn(500);
		var modalBox = $(this).attr('data-modal-id');
		$('#'+modalBox).fadeIn($(this).data());
	});


$(".js-modal-close, .modal-overlay").click(function() {
    $(".modal-box, .modal-overlay").fadeOut(500, function() {
        $(".modal-overlay").remove();
    });

});

$(window).resize(function() {
    $(".modal-box").css({
        top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
        left: ($(window).width() - $(".modal-box").outerWidth()) / 2
    });
});

$(window).resize();

});
</script>

<div class="modal-box" id="myModalPrivacy-<?php the_ID(); ?>">
			<div class="modal-body">
				<a class="js-modal-close close privacyCloseI-<?php the_ID(); ?>">×</a>
				<div class="layout-2-details layout-2-details-<?php the_ID(); ?>">

						<div class="form-group">
							<label for="dyn-tags"><br/>Privacy Options:</label>
							<div class="checkbox">
								  <?php
								  //$selectUsersListArray = explode(",", $selectUsersList[0]);
								    $selectUsersListArray = $selectUsersList;
								     if(isset($privacyOption[0]) and $privacyOption[0] != "")
									 {
								       //echo $privacyOption[0]."<br>";
										 ?>
										    <label>Private
											  <?php
											     if($privacyOption[0] == "private")
												 {
											        ?>
													   <input type="radio" id="privacy-radio-<?php the_ID(); ?>" name="privacy-option-<?php the_ID(); ?>" class="dyn-select-file" class="form-control" value="private" checked="checked">
													<?php
												 }
												 else
												 {
													 ?>
													   <input type="radio" id="privacy-radio-<?php the_ID(); ?>" name="privacy-option-<?php the_ID(); ?>" class="dyn-select-file" class="form-control" value="private">
													<?php
												 }
											  ?>
											</label>
											<label>Public
											<input type="radio" id="privacy-radio1-<?php the_ID(); ?>" class="dyn-select-file" class="form-control" value="public" <?php if (isset($privacyOption[0]) && $privacyOption[0]=="public") echo "checked";?> name="privacy-option2-<?php the_ID(); ?>">
											</label>
										 <?php
									 }
									 else
									 {
										 ?>
										    <label>Private
											  <input type="radio" id="privacy-radio-<?php the_ID(); ?>" name="privacy-option-<?php the_ID(); ?>" class="dyn-select-file" class="form-control" value="private">
											</label>
											<label>Public
											 <input type="radio" id="privacy-radio1-<?php the_ID(); ?>" name="privacy-option2-<?php the_ID(); ?>" class="dyn-select-file" class="form-control" value="public">
											</label>
										 <?php
									 }
								  ?>
							</div>
							 <?php
								 if(isset($privacyOption[0]) && $privacyOption[0] != "")
								 {
									   if($privacyOption[0] == "private")
									   {
										    ?>
											<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/jquery.tokenize.js"></script>
												<div class="select-box-<?php the_ID(); ?>">
													<?php
													  echo '<select id="tokenize-'.get_the_ID().'" class="tokenize-sample" name="select_users_list[]" style="width: 350px; height: 150px;" multiple="multiple">';
														  $args1 = array(
															  'role' => 'free_user',
															  'orderby' => 'id',
															  'order' => 'desc'
														   );
														  $subscribers = get_users($args1);
														  foreach ($subscribers as $user) {
															  if(get_current_user_id() == $user->id)
															  { }
															  else
															  {
														          if( in_array($user->id, $selectUsersListArray) )
																  {
																	   echo '<option value="'.$user->id.'" selected="selected">' . $user->display_name.'</option>';
																  }
                                                                  else{
																	   echo '<option value="'.$user->id.'">' . $user->display_name.'</option>';
																  }
															  }
														  }
													  echo '</select>';
													?>
													<script type="text/javascript">
											          $(document).ready(function(){
														  $("#privacy-radio").click(function(){
															 $(".select-box").slideDown();
														  });
														  $("#privacy-radio1").click(function(){
															 $(".select-box").slideUp();
															 $('select#select_users_list option').removeAttr("selected");
														  });

													  });
													  $('#tokenize-<?php echo get_the_ID(); ?>').tokenize();
				                                         datas: "bower.json.php"
												  </script>
												  <style> .tokenize-sample { width: 350px;; }</style>
												</div>
										   <?php
									   }
									   else
									   {
                                            ?>
											<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/jquery.tokenize.js"></script>
												<div style="display:none;" class="select-box-<?php the_ID(); ?>">
													<?php
													  echo '<select id="tokenize-'.get_the_ID().'" class="tokenize-sample" name="select_users_list[]" style="width: 350px; height: 150px;" multiple="multiple">';
														  $args1 = array(
															  'role' => 'free_user',
															  'orderby' => 'id',
															  'order' => 'desc'
														   );
														  $subscribers = get_users($args1);
														  foreach ($subscribers as $user) {
															  if(get_current_user_id() == $user->id)
															  { }
															  else
															  {
																  echo '<option value="'.$user->id.'">' . $user->display_name.'</option>';
															  }
														  }
													  echo '</select>';
													?>
													<script type="text/javascript">
											          $(document).ready(function(){
														  $("#privacy-radio").click(function(){
															 $(".select-box").slideDown();
														  });
														  $("#privacy-radio1").click(function(){
															 $(".select-box").slideUp();
															 $('select#select_users_list option').removeAttr("selected");
														  });

													  });
													  $('#tokenize-<?php echo get_the_ID(); ?>').tokenize();
				                                         datas: "bower.json.php"
												  </script>
												  <style> .tokenize-sample { width: 350px;; }</style>
												</div>
										   <?php
									   }
								 }
								 else
								 {
									   ?>
									   <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/jquery.tokenize.js"></script>
									        <div style="display:none;" class="select-box-<?php the_ID(); ?>">
												<?php
												  echo '<select id="tokenize-'.get_the_ID().'" class="tokenize-sample" name="select_users_list[]" style="width: 350px; height: 150px;" multiple="multiple">';
													  $args1 = array(
														  'role' => 'free_user',
														  'orderby' => 'id',
														  'order' => 'desc'
													   );
													  $subscribers = get_users($args1);
													  foreach ($subscribers as $user) {
														  if(get_current_user_id() == $user->id)
														  { }
														  else
														  {
															  echo '<option value="'.$user->id.'">' . $user->display_name.'</option>';
														  }
													  }
												  echo '</select>';
												?>
												<script type="text/javascript">
											          $(document).ready(function(){
														  $("#privacy-radio").click(function(){
															 $(".select-box").slideDown();
														  });
														  $("#privacy-radio1").click(function(){
															 $(".select-box").slideUp();
															 $('select#select_users_list option').removeAttr("selected");
														  });

													  });
													  $('#tokenize-<?php echo get_the_ID(); ?>').tokenize();
				                                         datas: "bower.json.php"
												  </script>
												  <style> .tokenize-sample { width: 350px;; }</style>
											</div>
									   <?php
								 }
							 ?>
							 <br>
							 <div id="msgLoader-<?php the_ID(); ?>"></div>
                             <input type="button" id="save-Privacy-Option-<?php the_ID(); ?>" name="save-privacy-option-<?php the_ID(); ?>" value="Save Privacy Option" currentUserID="<?php echo get_current_user_id(); ?>" postID="<?php echo get_the_ID(); ?>">
						</div>
					<script>
                      $(document).ready(function(){
						  $("#privacy-radio-<?php the_ID(); ?>").click(function(){
							 $("#privacy-radio1-<?php the_ID(); ?>").prop( "checked", false );
						     $(".select-box-<?php the_ID(); ?>").slideDown();
						  });
						  $("#privacy-radio1-<?php the_ID(); ?>").click(function(){
							 $("#privacy-radio-<?php the_ID(); ?>").prop( "checked", false );
						     $(".select-box-<?php the_ID(); ?>").slideUp();
							 $('select#select_users_list-<?php the_ID(); ?> option').removeAttr("selected");
						  });
						  $(".privacyCloseI-<?php the_ID(); ?>").click(function(){
							 $('#msgLoader-<?php the_ID(); ?>').html('');
						  });
						  $("#save-Privacy-Option-<?php the_ID(); ?>").click(function(){
							   var userID = $(this).attr('currentUserID');
							   var postID = $(this).attr('postID');
							   var select_users_list = $('#tokenize-'+postID).val();
							   if ( typeof(select_users_list) !== "undefined" && select_users_list !== null )
							   { }
						       else
							   {
								   select_users_list = '';
							   }
							   var privacyOptionV = "";
							   if($('#privacy-radio-<?php the_ID(); ?>').is(':checked'))
							   {
								   var privateChecked = "yes";
							   }
							   else
							   {
								   var privateChecked = "no";
							   }
							   if($('#privacy-radio1-<?php the_ID(); ?>').is(':checked'))
							   {
								   var publicChecked = "yes";
							   }
							   else
							   {
								   var publicChecked = "no";
							   }
							   if(privateChecked == "yes")
							   {
								   privacyOptionV = "private";
							   }
							   else if(publicChecked == "yes")
							   {
								   privacyOptionV = "public";
							   }
							   else
							   {
								   privacyOptionV = "";
							   }
                               $.ajax({
									type: 'POST',
									url: "<?php echo site_url(); ?>/update-Privacy.php",
									data: { userID: userID, postID: postID, privacyOptionV : privacyOptionV,
									select_users_list : select_users_list},
									beforeSend: function(){
									  $('#msgLoader-<?php the_ID(); ?>').html('<img src="<?php echo site_url(); ?>/wp-content/themes/videopress/images/loading.gif" />');
									},
									success: function(data){
										 $('#msgLoader-<?php the_ID(); ?>').html(data);
									}
								});
						  });
                      });
					 // $('#tokenize').tokenize();
                    </script>
					<style> .tokenize-sample { width: 350px;; }</style>
					<style>
                      #privacy-radio1-<?php the_ID(); ?>, #privacy-radio-<?php the_ID(); ?> {
                        padding: 5px;
                        text-align: center;
                      }

                     #select-box-<?php the_ID(); ?> {
                       padding: 50px;
                       display: none;
                     }
                    </style>
				</div>
			</div>
</div>





			<div class="modal-box" id="myModal-<?php the_ID(); ?>">
			<div class="modal-body">
			<a class="js-modal-close close">×</a>
			<div class="layout-2-details layout-2-details-<?php the_ID(); ?>">
				<div class="image-holder">
					<a href="<?php echo get_permalink(); ?>">
						<div class="hover-item"></div>
					</a>

					<?php
					if( has_post_thumbnail() ){
						the_post_thumbnail('small-thumb', array( 'class' => 'layout-2-thumb' ) );
					}else{
						echo '<img src="'.get_template_directory_uri().'/images/placeholder.jpg" class="layout-2-thumb">';
					}
					?>

				</div>

				<div class='other-image-product-first'>
				 <div class="thumbnails columns-3">
					<?php
   $product_id=get_the_ID();
   $_product = wc_get_product( $product_id );
 $attachment_ids = $_product->get_gallery_attachment_ids();foreach( $attachment_ids as $attachment_id ){  ?>
 <img width="180" height="180" src="<?php echo wp_get_attachment_url( $attachment_id ); ?>" class="other-image-product attachment-shop_thumbnail" alt="Chrysanthemum" title="Chrysanthemum">
 <?php } ?>
				</div></div>
				<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h6>
				<?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
					<h4 class="tittle-h4">Price:<?php
				$product_id=get_the_ID();
				$_product = wc_get_product( $product_id );
				?><span class="price-product-modal">
				<?php if($_product->get_regular_price()!== ''){ if ($_product->get_sale_price() !=='') { ?> <del><span class="amount"><?php echo $_product->get_regular_price(); ?>$</span></del> <?php }else{?>
				<span class="amount"><?php echo $_product->get_regular_price(); ?>$</span>  <?php }} ?>


<?php if ($_product->get_sale_price() !=='') {
?>
<ins><span class="amount"><?php echo $_product->get_sale_price(); ?>$</span></ins> <?php } ?></span>
 <span class="fgjieryhfe"> <span class="onsale" style="left: 75em; top: 0em;">Sale!</span></span>
 </h4>

<div class="stock-grid">
<h4 class="tittle-h4"><i class="fa fa-smile-o" aria-hidden="true"></i><span class="stock-span-grid"><?php
echo $_product->get_total_stock();
?> in Stock</span></h4>
</div>




				<div class="video_excerpt_text one"><h4 class="tittle-h4">Description 2</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = '' , $message = 'No Description available' , $html = ' [...]' );?></div></div>


                <h4 class="tittle-h4">Sold by: <span class="stock-span-grid"><?php
	                echo get_the_author();
				?></span></h4>




					<!-- <div class="video_excerpt_text two"><h4 class="tittle-h4">Reference</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_post_meta(get_the_ID(),'video_options_refr',true) , $length = '' , $message = 'No referance given' , $html = ' [...]' );?></div></div>
				-->
				<ul class="stats">
					<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
					<li><?php videopress_countviews( get_the_ID() ); ?></li>
					<li><i class="fa fa-wrench"></i> <?php $result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
						echo count($result); ?> Endorsments</li>
					<!-- favourite section -->
					<li><i class="fa fa-heart"></i>
					<?php
					  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
					  $result1 = $wpdb->get_results($sql_aux);
					  echo count($result1);
					?> Favorites</li>
					<!-- end favorite -->
					<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
					<li><?php comments_number() ?></li>
					 <li>
					  <?php
	                  $units_sold = get_post_meta( get_the_ID(), 'total_sales', true );
	                  if($units_sold){
                   	  echo '<span>' . sprintf( __( ' %s', 'woocommerce' ), $units_sold ) . ' Sold</span>';}
	                  else{echo '<span>0 Sold</span>';}
				      ?>
                     </li>
				</ul>
				<div class="clear"></div>
				<p><?php echo videopress_content('240'); ?></p>

					<?php if($profile_id == get_current_user_id()){ ?>
					<p><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>"><i class="fa fa-trash-o"></i> Delete</a></p>
					<?php } ?>


			</div></div></div>

			<div class="layout-2-details layout-2-details-<?php the_ID(); ?>" style="display:none;">
				<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h6>
							<h6 class="tittle-h4">Price:<?php
				$product_id=get_the_ID();
				$_product = wc_get_product( $product_id );
				?><span class="price-product-modal">
				<?php if($_product->get_regular_price()!== ''){ if ($_product->get_sale_price() !=='') { ?> <del><span class="amount"><?php echo $_product->get_regular_price(); ?>$</span></del> <?php }else{?>
				<span class="amount"><?php echo $_product->get_regular_price(); ?>$</span>  <?php }} ?>


<?php if ($_product->get_sale_price() !=='') {
?>
<ins><span class="amount"><?php echo $_product->get_sale_price(); ?>$</span></ins> <?php } ?></span>

 </h6>

				<?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
				<div class="video_excerpt_text one"><h4 class="tittle-h4">Description</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = 100 , $message = 'No Description available' , $html = '<a data-modal-id="myModal-'.get_the_ID().'"> [...]</a>' ); ?></div></div>
				<ul class="stats">
					<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
					<li><?php videopress_countviews( get_the_ID() ); ?></li>
					<li><i class="fa fa-wrench"></i> <?php $result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
						echo count($result); ?> Endorsments</li>
					<!-- favourite section -->
					<li><i class="fa fa-heart"></i>
					<?php
					  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
					  $result1 = $wpdb->get_results($sql_aux);
					  echo count($result1);
					?> Favorites</li>
					<!-- end favorite -->
					<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
					<li><?php comments_number() ?></li>
					<li> <?php
	$units_sold = get_post_meta( get_the_ID(), 'total_sales', true );
	if($units_sold){
	echo '<span class="units_sold">' . sprintf( __( ' %s', 'woocommerce' ), $units_sold ) . ' Sold</span>';}
	else{echo '<span class="units_sold">0 Sold</span>';}
					 ?>
					</li>
				</ul>
				<div class="clear"></div>
				<p><?php echo videopress_content('240'); ?></p>

					<p><?php if($profile_id == get_current_user_id()){ ?>
					      <a href="#" class="del-video" data-id="<?= get_the_ID(); ?>"><i class="fa fa-trash-o"></i> Delete</a>
					    <?php } ?>
					      <a class="privacy-video" data-modal-id="myModalPrivacy-<?php the_ID(); ?>"><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;Privacy</a>
					</p>

			</div>

			<div class="clear"></div>
				   <?php
			 }
			 else
			 {    // Case where logged in User is not same as Video Author User
				  if(isset($privacyOption[0]) && $privacyOption[0] != "private")
				  {
					   ?>
						   <div class="grid-6 first">
								<div class="image-holder">
									<a href="<?php echo get_permalink(); ?>">
										<div class="hover-item"></div>
									</a>
									<?php
									if( has_post_thumbnail() ){
										the_post_thumbnail('small-thumb', array( 'class' => 'layout-2-thumb' ) );
									}else{
										echo '<img src="'.get_template_directory_uri().'/images/placeholder.jpg" class="layout-2-thumb">';
									}
									?>
								</div>
								<div class="layout-title-box layout-title-box-<?php the_ID(); ?>" style="display:block;">
									<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php echo ctpreview( $content = get_the_title() , $length = 39 , $message = 'No Title available' , $html = ' [...]' ); ?></a></h6>
									<ul class="bottom-detailsul">
										<?php if($profile_id == get_current_user_id()){ ?>
										<li><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>"><i class="fa fa-trash-o"></i> Delete</a></li>
										<?php } ?>
										<li><a class="detailsblock-btn" data-modal-id="myModal-<?php the_ID(); ?>"><i class="fa fa-info-circle" aria-hidden="true"></i> Details</a></li>
									</ul>
								</div>
							</div>
							<script type="text/javascript">
$(function(){

var appendthis =  ("<div class='modal-overlayblockdiv'></div>");

	$('a[data-modal-id]').click(function(e) {
		e.preventDefault();
    $("body").append(appendthis);
    $(".modal-overlay").fadeTo(500, 0.7);
    //$(".js-modalbox").fadeIn(500);
		var modalBox = $(this).attr('data-modal-id');
		$('#'+modalBox).fadeIn($(this).data());
	});


$(".js-modal-close, .modal-overlay").click(function() {
    $(".modal-box, .modal-overlay").fadeOut(500, function() {
        $(".modal-overlay").remove();
    });

});

$(window).resize(function() {
    $(".modal-box").css({
        top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
        left: ($(window).width() - $(".modal-box").outerWidth()) / 2
    });
});

$(window).resize();

});
</script>

			<div class="modal-box" id="myModal-<?php the_ID(); ?>">
			<div class="modal-body">
			<a class="js-modal-close close">×</a>
			<div class="layout-2-details layout-2-details-<?php the_ID(); ?>">
				<div class="image-holder">
					<a href="<?php echo get_permalink(); ?>">
						<div class="hover-item"></div>
					</a>

					<?php
					if( has_post_thumbnail() ){
						the_post_thumbnail('small-thumb', array( 'class' => 'layout-2-thumb' ) );
					}else{
						echo '<img src="'.get_template_directory_uri().'/images/placeholder.jpg" class="layout-2-thumb">';
					}
					?>

				</div>
				<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h6>
				<?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
				<div class="video_excerpt_text one"><h4 class="tittle-h4">Description 2</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = '' , $message = 'No Description available' , $html = ' [...]' );?></div></div>
				<div class="video_excerpt_text two"><h4 class="tittle-h4">Reference</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_post_meta(get_the_ID(),'video_options_refr',true) , $length = '' , $message = 'No referance given' , $html = ' [...]' );?></div></div>
				<ul class="stats">
					<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
					<li><?php videopress_countviews( get_the_ID() ); ?></li>
					<li><i class="fa fa-wrench"></i> <?php $result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
						echo count($result); ?> Endorsments</li>
					<!-- favourite section -->
					<li><i class="fa fa-heart"></i>
					<?php
					  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
					  $result1 = $wpdb->get_results($sql_aux);
					  echo count($result1);
					?> Favorites</li>
					<!-- end favorite -->
					<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
					<li><?php comments_number() ?></li>
				</ul>
				<div class="clear"></div>
				<p><?php echo videopress_content('240'); ?></p>

					<?php if($profile_id == get_current_user_id()){ ?>
					<p><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>"><i class="fa fa-trash-o"></i> Delete</a></p>
					<?php } ?>


			</div></div></div>

			<div class="layout-2-details layout-2-details-<?php the_ID(); ?>" style="display:none;">
				<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h6>
				<?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
				<div class="video_excerpt_text one"><h4 class="tittle-h4">Description</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = 100 , $message = 'No Description available' , $html = '<a data-modal-id="myModal-'.get_the_ID().'"> [...]</a>' ); ?></div></div>
				<ul class="stats">
					<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
					<li><?php videopress_countviews( get_the_ID() ); ?></li>
					<li><i class="fa fa-wrench"></i> <?php $result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
						echo count($result); ?> Endorsments</li>
					<!-- favourite section -->
					<li><i class="fa fa-heart"></i>
					<?php
					  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
					  $result1 = $wpdb->get_results($sql_aux);
					  echo count($result1);
					?> Favorites</li>
					<!-- end favorite -->
					<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
					<li><?php comments_number() ?></li>
				</ul>
				<div class="clear"></div>
				<p><?php echo videopress_content('240'); ?></p>

					<?php if($profile_id == get_current_user_id()){ ?>
					<p><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>"><i class="fa fa-trash-o"></i> Delete</a></p>
					<?php } ?>


			</div>

			<div class="clear"></div>
					   <?php
				  }
				  else
				  {
					  if(!isset($privacyOption[0]))
					  {
						    ?>
							   <div class="grid-6 first">
									<div class="image-holder">
										<a href="<?php echo get_permalink(); ?>">
											<div class="hover-item"></div>
										</a>
										<?php
										if( has_post_thumbnail() ){
											the_post_thumbnail('small-thumb', array( 'class' => 'layout-2-thumb' ) );
										}else{
											echo '<img src="'.get_template_directory_uri().'/images/placeholder.jpg" class="layout-2-thumb">';
										}
										?>
									</div>
									<div class="layout-title-box layout-title-box-<?php the_ID(); ?>" style="display:block;">
										<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php echo ctpreview( $content = get_the_title() , $length = 39 , $message = 'No Title available' , $html = ' [...]' ); ?></a></h6>
										<ul class="bottom-detailsul">
											<?php if($profile_id == get_current_user_id()){ ?>
											<li><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>"><i class="fa fa-trash-o"></i> Delete</a></li>
											<?php } ?>
											<li><a class="detailsblock-btn" data-modal-id="myModal-<?php the_ID(); ?>"><i class="fa fa-info-circle" aria-hidden="true"></i> Details</a></li>
										</ul>
									</div>
								</div>
								<script type="text/javascript">
$(function(){

var appendthis =  ("<div class='modal-overlayblockdiv'></div>");

	$('a[data-modal-id]').click(function(e) {
		e.preventDefault();
    $("body").append(appendthis);
    $(".modal-overlay").fadeTo(500, 0.7);
    //$(".js-modalbox").fadeIn(500);
		var modalBox = $(this).attr('data-modal-id');
		$('#'+modalBox).fadeIn($(this).data());
	});


$(".js-modal-close, .modal-overlay").click(function() {
    $(".modal-box, .modal-overlay").fadeOut(500, function() {
        $(".modal-overlay").remove();
    });

});

$(window).resize(function() {
    $(".modal-box").css({
        top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
        left: ($(window).width() - $(".modal-box").outerWidth()) / 2
    });
});

$(window).resize();

});
</script>

			<div class="modal-box" id="myModal-<?php the_ID(); ?>">
			<div class="modal-body">
			<a class="js-modal-close close">×</a>
			<div class="layout-2-details layout-2-details-<?php the_ID(); ?>">
				<div class="image-holder">
					<a href="<?php echo get_permalink(); ?>">
						<div class="hover-item"></div>
					</a>

					<?php
					if( has_post_thumbnail() ){
						the_post_thumbnail('small-thumb', array( 'class' => 'layout-2-thumb' ) );
					}else{
						echo '<img src="'.get_template_directory_uri().'/images/placeholder.jpg" class="layout-2-thumb">';
					}
					?>

				</div>
				<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h6>
				<?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
				<div class="video_excerpt_text one"><h4 class="tittle-h4">Description 2</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = '' , $message = 'No Description available' , $html = ' [...]' );?></div></div>
				<div class="video_excerpt_text two"><h4 class="tittle-h4">Reference</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_post_meta(get_the_ID(),'video_options_refr',true) , $length = '' , $message = 'No referance given' , $html = ' [...]' );?></div></div>
				<ul class="stats">
					<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
					<li><?php videopress_countviews( get_the_ID() ); ?></li>
					<li><i class="fa fa-wrench"></i> <?php $result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
						echo count($result); ?> Endorsments</li>
					<!-- favourite section -->
					<li><i class="fa fa-heart"></i>
					<?php
					  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
					  $result1 = $wpdb->get_results($sql_aux);
					  echo count($result1);
					?> Favorites</li>
					<!-- end favorite -->
					<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
					<li><?php comments_number() ?></li>
				</ul>
				<div class="clear"></div>
				<p><?php echo videopress_content('240'); ?></p>

					<?php if($profile_id == get_current_user_id()){ ?>
					<p><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>"><i class="fa fa-trash-o"></i> Delete</a></p>
					<?php } ?>


			</div></div></div>

			<div class="layout-2-details layout-2-details-<?php the_ID(); ?>" style="display:none;">
				<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h6>
				<?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
				<div class="video_excerpt_text one"><h4 class="tittle-h4">Description</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = 100 , $message = 'No Description available' , $html = '<a data-modal-id="myModal-'.get_the_ID().'"> [...]</a>' ); ?></div></div>
				<ul class="stats">
					<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
					<li><?php videopress_countviews( get_the_ID() ); ?></li>
					<li><i class="fa fa-wrench"></i> <?php $result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
						echo count($result); ?> Endorsments</li>
					<!-- favourite section -->
					<li><i class="fa fa-heart"></i>
					<?php
					  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
					  $result1 = $wpdb->get_results($sql_aux);
					  echo count($result1);
					?> Favorites</li>
					<!-- end favorite -->
					<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
					<li><?php comments_number() ?></li>
				</ul>
				<div class="clear"></div>
				<p><?php echo videopress_content('240'); ?></p>

					<?php if($profile_id == get_current_user_id()){ ?>
					<p><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>"><i class="fa fa-trash-o"></i> Delete</a></p>
					<?php } ?>


			</div>

			<div class="clear"></div>
						   <?php
					  }
                      else
					  {
						    if( is_array($selectUsersList) and count($selectUsersList) > 0 )
							   {   // case where user access list is available
									 if( in_array($user_ID, $selectUsersList) )
									 {
										   ?>
											   <div class="grid-6 first">
													<div class="image-holder">
														<a href="<?php echo get_permalink(); ?>">
															<div class="hover-item"></div>
														</a>
														<?php
														if( has_post_thumbnail() ){
															the_post_thumbnail('small-thumb', array( 'class' => 'layout-2-thumb' ) );
														}else{
															echo '<img src="'.get_template_directory_uri().'/images/placeholder.jpg" class="layout-2-thumb">';
														}
														?>
													</div>
													<div class="layout-title-box layout-title-box-<?php the_ID(); ?>" style="display:block;">
														<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php echo ctpreview( $content = get_the_title() , $length = 39 , $message = 'No Title available' , $html = ' [...]' ); ?></a></h6>
														<ul class="bottom-detailsul">
															<?php if($profile_id == get_current_user_id()){ ?>
															<li><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>"><i class="fa fa-trash-o"></i> Delete</a></li>
															<?php } ?>
															<li><a class="detailsblock-btn" data-modal-id="myModal-<?php the_ID(); ?>"><i class="fa fa-info-circle" aria-hidden="true"></i> Details</a></li>
														</ul>
													</div>
												</div>
												<script type="text/javascript">
$(function(){

var appendthis =  ("<div class='modal-overlayblockdiv'></div>");

	$('a[data-modal-id]').click(function(e) {
		e.preventDefault();
    $("body").append(appendthis);
    $(".modal-overlay").fadeTo(500, 0.7);
    //$(".js-modalbox").fadeIn(500);
		var modalBox = $(this).attr('data-modal-id');
		$('#'+modalBox).fadeIn($(this).data());
	});


$(".js-modal-close, .modal-overlay").click(function() {
    $(".modal-box, .modal-overlay").fadeOut(500, function() {
        $(".modal-overlay").remove();
    });

});

$(window).resize(function() {
    $(".modal-box").css({
        top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
        left: ($(window).width() - $(".modal-box").outerWidth()) / 2
    });
});

$(window).resize();

});
</script>

			<div class="modal-box" id="myModal-<?php the_ID(); ?>">
			<div class="modal-body">
			<a class="js-modal-close close">×</a>
			<div class="layout-2-details layout-2-details-<?php the_ID(); ?>">
				<div class="image-holder">
					<a href="<?php echo get_permalink(); ?>">
						<div class="hover-item"></div>
					</a>

					<?php
					if( has_post_thumbnail() ){
						the_post_thumbnail('small-thumb', array( 'class' => 'layout-2-thumb' ) );
					}else{
						echo '<img src="'.get_template_directory_uri().'/images/placeholder.jpg" class="layout-2-thumb">';
					}
					?>

				</div>
				<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h6>
				<?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
				<div class="video_excerpt_text one"><h4 class="tittle-h4">Description 2</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = '' , $message = 'No Description available' , $html = ' [...]' );?></div></div>
				<div class="video_excerpt_text two"><h4 class="tittle-h4">Reference</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_post_meta(get_the_ID(),'video_options_refr',true) , $length = '' , $message = 'No referance given' , $html = ' [...]' );?></div></div>
				<ul class="stats">
					<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
					<li><?php videopress_countviews( get_the_ID() ); ?></li>
					<li><i class="fa fa-wrench"></i> <?php $result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
						echo count($result); ?> Endorsments</li>
					<!-- favourite section -->
					<li><i class="fa fa-heart"></i>
					<?php
					  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
					  $result1 = $wpdb->get_results($sql_aux);
					  echo count($result1);
					?> Favorites</li>
					<!-- end favorite -->
					<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
					<li><?php comments_number() ?></li>
				</ul>
				<div class="clear"></div>
				<p><?php echo videopress_content('240'); ?></p>

					<?php if($profile_id == get_current_user_id()){ ?>
					<p><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>"><i class="fa fa-trash-o"></i> Delete</a></p>
					<?php } ?>


			</div></div></div>

			<div class="layout-2-details layout-2-details-<?php the_ID(); ?>" style="display:none;">
				<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h6>
				<?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
				<div class="video_excerpt_text one"><h4 class="tittle-h4">Description</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = 100 , $message = 'No Description available' , $html = '<a data-modal-id="myModal-'.get_the_ID().'"> [...]</a>' ); ?></div></div>
				<ul class="stats">
					<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
					<li><?php videopress_countviews( get_the_ID() ); ?></li>
					<li><i class="fa fa-wrench"></i> <?php $result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
						echo count($result); ?> Endorsments</li>
					<!-- favourite section -->
					<li><i class="fa fa-heart"></i>
					<?php
					  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
					  $result1 = $wpdb->get_results($sql_aux);
					  echo count($result1);
					?> Favorites</li>
					<!-- end favorite -->
					<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
					<li><?php comments_number() ?></li>
				</ul>
				<div class="clear"></div>
				<p><?php echo videopress_content('240'); ?></p>

					<?php if($profile_id == get_current_user_id()){ ?>
					<p><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>"><i class="fa fa-trash-o"></i> Delete</a></p>
					<?php } ?>


			</div>

			<div class="clear"></div>
										   <?php
									 }
									 else
									 {
									 }
							   }
							   else
							   {  }
					  }
				  }
			 }
		}

     if(!is_user_logged_in())
		{  // case where user is not logged in
	             if(isset($privacyOption[0]) && ($privacyOption[0] != "private"))
				  {
					     ?>
						     </div>
							 </div>
						 <?php
				  }
				  else
				  {
					    if(!isset($privacyOption[0]))
						{
							 ?>
								 </div>
								 </div>
							 <?php
						}
				  }
		}
		else
		{  // case where user is logged in
			 if($post_author == $user_ID)
			 {    // Case where logged in User is same as Video Author User
				   ?>
					   </div>
					   </div>
				   <?php
			 }
			 else
			 {    // Case where logged in User is not same as Video Author User
				  if(isset($privacyOption[0]) && $privacyOption[0] != "private")
				  {
					     ?>
						     </div>
							 </div>
						 <?php
				  }
				  else
				  {
					  if(!isset($privacyOption[0]))
					  {
						     ?>
								 </div>
								 </div>
							 <?php
					  }
                      else
					  {
						    if( is_array($selectUsersList) and count($selectUsersList) > 0 )
							   {   // case where user access list is available
									 if( in_array($user_ID, $selectUsersList) )
									 {
										     ?>
												 </div>
												 </div>
											 <?php
									 }
									 else
									 {
									 }
							   }
							   else
							   {  }
					  }
				  }
			 }
		}
?>
	<!-- End Layout Wrapper -->

<?php
/* ================================================================== */
/* End of Loop */
/* ================================================================== */
}// end for each
echo '</div>';
/* ================================================================== */
/* Else if Nothing Found */
/* ================================================================== */
}else {
	?>

<h6>NO PRODUCT FOUND!</h6>
<p>The user has not yet uploaded any product yet.</p>

<?php
/* ================================================================== */
/* End If */
/* ================================================================== */
}

elseif($endorsements || $favourites):
	if($pageposts):
		global $post;
echo '<div id="products" class="list-group">';
	foreach($pageposts as $post):
		setup_postdata($post);?>

	<!-- Start Layout Wrapper -->
<div class="item col-xs-6 col-lg-6 post-<?php the_ID(); ?>">
    <div class="thumbnail layout-2-wrapper solid-bg">

			<div class="grid-6 first">
				<div class="image-holder">
					<a href="<?php echo get_permalink(); ?>">
						<div class="hover-item"></div>
					</a>

					<?php
					if( has_post_thumbnail() ){
						the_post_thumbnail('small-thumb', array( 'class' => 'layout-2-thumb' ) );
					}else{
						echo '<img src="'.get_template_directory_uri().'/images/placeholder.jpg" class="layout-2-thumb">';
					}
					?>

				</div>
				<div class="layout-title-box layout-title-box-<?php the_ID(); ?>" style="display:block;">
					<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php echo ctpreview( $content = get_the_title() , $length = 39 , $message = 'No Title available' , $html = ' [...]' ); ?></a></h6>
					<ul class="bottom-detailsul">
						<?php if($profile_id == get_current_user_id()){ ?>
						<li><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>"><i class="fa fa-trash-o"></i> Delete</a></li>
						<?php } ?>
						<li><a class="detailsblock-btn" data-modal-id="myModal-<?php the_ID(); ?>"><i class="fa fa-info-circle" aria-hidden="true"></i> Details</a></li>
					</ul>
				</div>
			</div>

<script type="text/javascript">
$(function(){

var appendthis =  ("<div class='modal-overlayblockdiv'></div>");

	$('a[data-modal-id]').click(function(e) {
		e.preventDefault();
    $("body").append(appendthis);
    $(".modal-overlay").fadeTo(500, 0.7);
    //$(".js-modalbox").fadeIn(500);
		var modalBox = $(this).attr('data-modal-id');
		$('#'+modalBox).fadeIn($(this).data());
	});


$(".js-modal-close, .modal-overlay").click(function() {
    $(".modal-box, .modal-overlay").fadeOut(500, function() {
        $(".modal-overlay").remove();
    });

});

$(window).resize(function() {
    $(".modal-box").css({
        top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
        left: ($(window).width() - $(".modal-box").outerWidth()) / 2
    });
});

$(window).resize();

});
</script>

			<div class="modal-box" id="myModal-<?php the_ID(); ?>">
			<div class="modal-body">
			<a class="js-modal-close close">×</a>
			<div class="layout-2-details layout-2-details-<?php the_ID(); ?>">
				<div class="image-holder">
					<a href="<?php echo get_permalink(); ?>">
						<div class="hover-item"></div>
					</a>

					<?php
					if( has_post_thumbnail() ){
						the_post_thumbnail('small-thumb', array( 'class' => 'layout-2-thumb' ) );
					}else{
						echo '<img src="'.get_template_directory_uri().'/images/placeholder.jpg" class="layout-2-thumb">';
					}
					?>

				</div>
				<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h6>
				<?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
				<div class="video_excerpt_text one"><h4 class="tittle-h4">Description</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = '' , $message = 'No Description available' , $html = ' [...]' );?></div></div>
				<div class="video_excerpt_text two"><h4 class="tittle-h4">Reference</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_post_meta(get_the_ID(),'video_options_refr',true) , $length = '' , $message = 'No referance given' , $html = ' [...]' );?></div></div>
				<ul class="stats">
					<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
					<li><?php videopress_countviews( get_the_ID() ); ?></li>
					<li><i class="fa fa-wrench"></i> <?php $result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
						echo count($result); ?> Endorsments</li>
					<!--favourite section -->
					<li><i class="fa fa-heart"></i>
					<?php
					  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
					  $result1 = $wpdb->get_results($sql_aux);
					  echo count($result1);
					?> Favorites</li>
					<!-- end favorite -->
					<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
					<li><?php comments_number() ?></li>

				</ul>
				<div class="clear"></div>
				<p><?php echo videopress_content('120'); ?></p>
				<?php if($profile_id == get_current_user_id()){ ?>
				<p><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>"><i class="fa fa-trash-o"></i> Delete</a></p>
				<?php } ?>
			</div></div></div>

			<div class="layout-2-details layout-2-details-<?php the_ID(); ?>" style="display:none;">
				<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h6>
				<?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
				<div class="video_excerpt_text one"><h4 class="tittle-h4">Description</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = 100 , $message = 'No Description available' , $html = '<a data-modal-id="myModal-'.get_the_ID().'"> [...]</a>' ); ?></div></div>
				<ul class="stats">
					<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
					<li><?php videopress_countviews( get_the_ID() ); ?></li>
					<li><i class="fa fa-wrench"></i> <?php $result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
						echo count($result); ?> Endorsments</li>
					<!--favourite section -->
					<li><i class="fa fa-heart"></i>
					<?php
					  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
					  $result1 = $wpdb->get_results($sql_aux);
					  echo count($result1);
					?> Favorites</li>
					<!-- end favorite -->
					<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
					<li><?php comments_number() ?></li>
				</ul>
				<div class="clear"></div>
				<p><?php echo videopress_content('120'); ?></p>
				<?php if($profile_id == get_current_user_id()){ ?>
				<p><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>"><i class="fa fa-trash-o"></i> Delete</a></p>
				<?php } ?>
			</div>

	<div class="clear"></div>
</div>
</div>
<!-- End Layout Wrapper -->

<?php endforeach;
	echo '</div>';
else:
	echo '<h3 class="text-center">Nothing Found Here !</h3>';
endif;
else:

echo '<div id="products" class="list-group">';
	while ( $my_query->have_posts() ) : $my_query->the_post(); ?>

<!-- Start Layout Wrapper -->
<div class="item  col-xs-6 col-lg-6 post-<?php the_ID(); ?>">
    <div class="thumbnail layout-2-wrapper solid-bg">

		<div class="grid-6 first">
			<div class="image-holder">
				<a href="<?php echo get_permalink(); ?>">
					<div class="hover-item"></div>
				</a>

				<?php
				if( has_post_thumbnail() ){
					the_post_thumbnail('small-thumb', array( 'class' => 'layout-2-thumb' ) );
				}else{
					echo '<img src="'.get_template_directory_uri().'/images/.png" class="layout-2-thumb">';
				}
				?>

			</div>
			<div class="layout-title-box layout-title-box-<?php the_ID(); ?>" style="display:block;">
					<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php echo ctpreview( $content = get_the_title() , $length = 39 , $message = 'No Title available' , $html = ' [...]' ); ?></a></h6>
					<ul class="bottom-detailsul">
						<?php if($profile_id == get_current_user_id()){ ?>
						<li><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>"><i class="fa fa-trash-o"></i> Delete</a></li>
						<?php } ?>
						<li><a class="detailsblock-btn" data-modal-id="myModal-<?php the_ID(); ?>"><i class="fa fa-info-circle" aria-hidden="true"></i> Details</a></li>
					</ul>
				</div>
			</div>

<script type="text/javascript">
$(function(){

var appendthis =  ("<div class='modal-overlayblockdiv'></div>");

	$('a[data-modal-id]').click(function(e) {
		e.preventDefault();
    $("body").append(appendthis);
    $(".modal-overlay").fadeTo(500, 0.7);
    //$(".js-modalbox").fadeIn(500);
		var modalBox = $(this).attr('data-modal-id');
		$('#'+modalBox).fadeIn($(this).data());
	});


$(".js-modal-close, .modal-overlay").click(function() {
    $(".modal-box, .modal-overlay").fadeOut(500, function() {
        $(".modal-overlay").remove();
    });

});

$(window).resize(function() {
    $(".modal-box").css({
        top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
        left: ($(window).width() - $(".modal-box").outerWidth()) / 2
    });
});

$(window).resize();

});
</script>

			<div class="modal-box" id="myModal-<?php the_ID(); ?>">
			<div class="modal-body">
			<a class="js-modal-close close">×</a>
			<div class="layout-2-details layout-2-details-<?php the_ID(); ?>">
			<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h6>
			<?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
			<div class="video_excerpt_text one"><h4 class="tittle-h4">Description</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = '' , $message = 'No Description available' , $html = ' [...]' );?></div></div>
			<div class="video_excerpt_text two"><h4 class="tittle-h4">Reference</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_post_meta(get_the_ID(),'video_options_refr',true) , $length = '' , $message = 'No referance given' , $html = ' [...]' );?></div></div>
			<ul class="stats">
				<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
				<li><?php videopress_countviews( get_the_ID() ); ?></li>
				<li><i class="fa fa-wrench"></i> <?php $result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
					echo count($result); ?> Endorsments</li>
				<!--favourite section -->
				<li><i class="fa fa-heart"></i>
				<?php
				  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
				  $result1 = $wpdb->get_results($sql_aux);
				  echo count($result1);
				?> Favorites</li>
				<!-- end favorite -->
				<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
				<li><?php comments_number(); ?></li>
			</ul>
			<div class="clear"></div>
			<p><?php echo videopress_content('240'); ?></p>
			<?php if($profile_id == get_current_user_id()){ ?>
			<p><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>" ><i class="fa fa-trash-o"></i> Delete</a></p>
			<?php } ?>
		</div></div></div>

		<div class="layout-2-details layout-2-details-<?php the_ID(); ?>" style="display:none;">
			<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h6>
			<?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
			<div class="video_excerpt_text one"><h4 class="tittle-h4">Description</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = 100 , $message = 'No Description available' , $html = '<a data-modal-id="myModal-'.get_the_ID().'"> [...]</a>' ); ?></div></div>
			<ul class="stats">
				<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
				<li><?php videopress_countviews( get_the_ID() ); ?></li>
				<li><i class="fa fa-wrench"></i> <?php $result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
					echo count($result); ?> Endorsments</li>
				<!--favourite section -->
				<li><i class="fa fa-heart"></i>
				<?php
				  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
				  $result1 = $wpdb->get_results($sql_aux);
				  echo count($result1);
				?> Favorites</li>
				<!-- end favorite -->
				<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
				<li><?php comments_number(); ?></li>
			</ul>
			<div class="clear"></div>
			<p><?php echo videopress_content('240'); ?></p>
			<?php if($profile_id == get_current_user_id()){ ?>
			<p><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>" ><i class="fa fa-trash-o"></i> Delete</a></p>
			<?php } ?>
		</div>

		<div class="clear"></div>
	</div>
</div>
<!-- End Layout Wrapper -->
<?php
endwhile;
echo "</div>";
endif;
?>

<?php /*if( vp_option('vpt_option.pagination') == '1' ){
		videopress_pagination(); // Use Custom Pagination
	}else{
		echo '<div class="post_pagination vdfgdfgddf">';
		posts_nav_link();
		echo'</div>';
	}*/
 if($cat=='store'){
         $pcurrent=max( 1, get_query_var('page') );
        }
       else{
         $pcurrent=1;
        }

     $paginate = paginate_links( array(
	'format' => '?page/%#%/',
	'current' => $pcurrent,
	'type'=>'array',
	'end_size'=>1,
	'mid_size'=>4,
	'total' => ceil($total / 12)
           ) );

$pcount=count($paginate);


 $maxp=ceil(($total/12)/2);

if( $pcurrent<= ceil($total / 12)  )
{
 if( ceil($total / 12) <5 ) {
 foreach($paginate as $plnk){echo $plnk.'&nbsp;';}
 }

else if( ceil($total / 12) >= 5 ){


 if ($pcurrent<=6)
 {
 	if ($pcurrent>2)
 	{echo $paginate[0].'&nbsp;';}
       if($paginate[$pcurrent-2]){
 	echo $paginate[$pcurrent-2].'&nbsp;';
      }
 	echo $paginate[$pcurrent-1].'&nbsp;';
 	echo $paginate[$pcurrent].'&nbsp;';
if( isset($paginate[$pcurrent+1]) && (($pcount-1) > $pcurrent+1) ){echo $paginate[$pcurrent+1].'&nbsp;';}
if( isset($paginate[$pcurrent+2]) && (($pcount-1) > $pcurrent+2) ){echo $paginate[$pcurrent+2].'&nbsp;';}

 }

else if ($pcurrent>=7)
 {   echo $paginate[0].'&nbsp;';
 	echo $paginate[5].'&nbsp;';
 	echo $paginate[6].'&nbsp;';
 	echo $paginate[7].'&nbsp;';
if( isset($paginate[8]) && (($pcount-1) > 8) ){echo $paginate[8].'&nbsp;';}
if( isset($paginate[9]) && (($pcount-1) > 9) ){echo $paginate[9].'&nbsp;';}

 }


if( $pcurrent <= (ceil($total/12)-1) ){
echo $paginate[$pcount-1];
  }

 }

}

?>
	<!-- End Pagination -->
			</div>
			<!-- end store list -->




















































			<?php // HOME TAB // ?>
			<div role="tabpanel" class="tab-pane <?= $home_active;?>" id="home">

<?php
$user_ID = get_current_user_id();
if ($user_ID == $author->ID){
?>
<script type="text/javascript">
	jQuery(document).ready(function($){
		$(".change_user_status").on("change", function(){
			var user_id		= parseInt($(this).attr('user_id'));
			$.ajax({
				type: 'POST',
				url: "<?php echo bloginfo('template_url'); ?>/videopress_ajax_controll.php",
				data: {
					user_id : user_id
				},
				success: function(data){
					//alert(data);
				},
				error: function(xhr, textStatus, error){
					$(".change_user_status").trigger("click");
				}
			});
		});
	});
</script>
<style>
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input {display:none;}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: rgb(92, 184, 92);
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>
<?php
$user_status = get_user_meta($user_id, '_user_status', true);
if($user_status){
	$user_status_report = "";
}else{
	$user_status_report = "checked";
}
?>
<p><strong>User Status:</strong></p>
<label class="switch">
  <input class="change_user_status" user_id="<?php echo $user_ID;?>" type="checkbox" <?php echo $user_status_report;?>>
  <div class="slider"></div>
</label>
<?php } ?>
				<div class="user-info">
					<?php if( get_the_author_meta('user_url', $author->ID) != '' ){
						echo '<p><strong>Website:</strong><br> <a href="'.get_the_author_meta('user_url', $author->ID).'" rel="nofollow">'.get_the_author_meta('user_url', $author->ID).'</a></p>';
        } // End If Description
        ?>

        <?php if( get_the_author_meta('description', $author->ID) != '' ){
        	echo '<p><strong>About Me</strong><br>'.get_the_author_meta('description', $author->ID).'</p>';
        } // End If Description
        ?>

        <!-- Suscribe channel button -->
        <?php
          //check if the post variables for the suscribe button is enabled
        if(isset($_POST['operation']) && $_POST['operation']=='suscribe'){
        	//var_dump("acccccc");
			//insert a new one
        	$insert = $wpdb->insert($wpdb->prefix."authors_suscribers",array(
        		'suscriber_id'	=> $_POST['suscriber_id'],
        		'author_id'		=> $_POST['author_id'],
        		));
        }

		  //check if the operation is unsuscribe
        if(isset($_POST['operation']) && $_POST['operation']=='unsuscribe'){
		  	//remove all current records from the suscribers table
        	$delete = $wpdb->delete($wpdb->prefix."authors_suscribers",array(
        		'suscriber_id'	=> $_POST['suscriber_id'],
        		'author_id'		=> $_POST['author_id'],
        		));
        }

        $user_ID = get_current_user_id();
        if ($user_ID != $author->ID){
		  	//check if the current user is suscribed to the list
        	$data1 = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."authors_suscribers WHERE author_id =".$author->ID." and suscriber_id =".$user_ID);
        	$suscribed=0;
        	$exists=0;
        	if($data1){
        		foreach($data1 as $single_data1){
        			$suscribed=$single_data1->status;
        			$exists=1;
        		}
        	}
        	if($suscribed==0){
        		echo '<form method="post">
        		<input type="hidden" name="suscriber_id" class="form-control" id="suscriber_id" value="'.$user_ID.'">
        		<input type="hidden" name="author_id" class="form-control" id="suscriber_id" value="'.$author->ID.'">
        		<input type="hidden" name="operation" class="form-control" id="operation" value="suscribe">';
				if ( is_user_logged_in() )
        		  echo  '<button type="submit" name="submit" class="btn btn-success" id="susription-submit">Suscribe to this user channel</button>';
				else
				  echo  '<button type="submit" onclick="alert(\'You must logged in for suscribe this user channel\'); return false;" name="reset" class="btn btn-success" id="susription-submit">Suscribe to this user channel</button>';
        	echo '</form>';
        }else{
        	echo '<form method="post">
        	<input type="hidden" name="suscriber_id" class="form-control" id="suscriber_id" value="'.$user_ID.'">
        	<input type="hidden" name="author_id" class="form-control" id="suscriber_id" value="'.$author->ID.'">
        	<input type="hidden" name="operation" class="form-control" id="operation" value="unsuscribe">
        	<button type="submit" name="unsubmit" class="btn btn-success" id="unsusription-submit">Unsuscribe to this user channel</button>
        </form>';
    }


	if(!empty($_POST['add_friend_submit']) and $_POST['add_friend_submit'] == "Add Friend"){
		$from_friend_id		= $_POST['from_friend_id'];
		$to_friend_id		= $_POST['to_friend_id'];

		$wpdb->insert(
			$wpdb->prefix."authors_friends_list",
			array(
				'from_friend_id' 	=> $from_friend_id,
				'to_friend_id'		=> $to_friend_id
			)
		);

		$_POST['add_friend_submit'] = "";
	}



	if ( is_user_logged_in() ) {

		$result = $wpdb->get_results("SELECT *FROM ".$wpdb->prefix."authors_friends_list WHERE (from_friend_id='".$user_ID."' AND to_friend_id='".$author->ID."' AND status=1) OR (from_friend_id='".$author->ID."' AND to_friend_id='".$user_ID."' AND status=1)");

		$result2 = $wpdb->get_results("SELECT *FROM ".$wpdb->prefix."authors_friends_list WHERE (from_friend_id='".$user_ID."' AND to_friend_id='".$author->ID."' AND status=2) OR (from_friend_id='".$author->ID."' AND to_friend_id='".$user_ID."' AND status=2)");

	echo '<form method="post" class="subscribers-inner-form">
		<input type="hidden" name="from_friend_id" value="'.$user_ID.'">
		<input type="hidden" name="to_friend_id" value="'.$author->ID.'">';

	if(count($result) > 0){
		echo '<h4>Friend request already sent.</h4>';
	}else if(count($result2) > 0){
		echo '<h4>You are already Friend.</h4>';
	}else{
		echo '<input type="submit" name="add_friend_submit" value="Add Friend" class="btn btn-success">';
	}
	echo '</form>';
	/*


	if(!empty($_POST['add_collaboration_submit']) and $_POST['add_collaboration_submit'] == "Collaborate"){
		$from_collaboration_id		= $_POST['from_collaboration_id'];
		$to_collaboration_id		= $_POST['to_collaboration_id'];

		$wpdb->insert(
			$wpdb->prefix."authors_collaboration_list",
			array(
				'from_collaboration_id' 	=> $from_collaboration_id,
				'to_collaboration_id'		=> $to_collaboration_id
			)
		);

		$_POST['add_collaboration_submit'] = "";
	}

		$result = $wpdb->get_results("SELECT *FROM ".$wpdb->prefix."authors_collaboration_list WHERE (from_collaboration_id='".$user_ID."' AND to_collaboration_id='".$author->ID."' AND status=1) OR (from_collaboration_id='".$author->ID."' AND to_collaboration_id='".$user_ID."' AND status=1)");

		$result2 = $wpdb->get_results("SELECT *FROM ".$wpdb->prefix."authors_collaboration_list WHERE (from_collaboration_id='".$user_ID."' AND to_collaboration_id='".$author->ID."' AND status=2) OR (from_collaboration_id='".$author->ID."' AND to_collaboration_id='".$user_ID."' AND status=2)");

	echo '<form method="post" class="subscribers-inner-form">
		<input type="hidden" name="from_collaboration_id" value="'.$user_ID.'">
		<input type="hidden" name="to_collaboration_id" value="'.$author->ID.'">';

	if(count($result) > 0){
		//echo '<h4>Collaboration request already sent.</h4>';
	}else if(count($result2) > 0){
		//echo '<h4>You are already Collaborate.</h4>';
	}else{
		echo '<input type="submit" name="add_collaboration_submit" value="Collaborate" class="btn btn-success">';
	}
	echo '</form>'; */
	}

	do_action('become_affiliate', $author->ID);


			//show the list of notfied live streaming sessions from this user
    $sql_data2="SELECT * FROM ".$wpdb->prefix."authors_suscribers_notifications WHERE author_id =".$author->ID." and suscriber_id =".$user_ID." and streaming_time>=NOW()";
    $data2 = $wpdb->get_results($sql_data2);
	$streaming1_list= '';
    if($data2){
    	foreach($data2 as $single_data2){
    		$data3 = $wpdb->get_results("SELECT l.*,t.* FROM ".$wpdb->prefix."authors_livestreaming l left join ".$wpdb->prefix."tz t on l.timezone_streaming=t.id WHERE l.id =".$single_data2->streaming_id);
    		if($data3){
    			foreach($data3 as $single_data3){
    				$streaming1_list .='<li>Date:'.$single_data3->date_livestreaming.'.<br>Time Zone:'.$single_data3->name.'.<br>Title:'.$single_data3->title.'.<br>Resume:'.$single_data3->intro.'.</li><br>';
		           }//end foreach data  3
		         }//end if data 3
			   }//end foreach data2
			}//end if data2
			$streaming1_list .='</ul>';
			echo $streaming1_list;
		}
		?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
<style>
#affiliate-referral-link{
text-decoration: none !important;
    color: #565656;
}

</style>

		<!-- Social Icons -->
		<ul class="user-social">
			<?php if( get_the_author_meta('gplus') != '' ){ echo '<li><a href="'.get_the_author_meta('gplus', $author->ID).'" target="_blank"><i class="fa fa-google-plus-square"></i></a></li>'; } ?>

			<?php
			if( get_the_author_meta('facebook', $author->ID) != '' ){ echo '<li><a href="'.get_the_author_meta('facebook', $author->ID).'" target="_blank"><i class="fa fa-facebook-square"></i></a></li>'; } ?>

			<?php
			if( get_the_author_meta('twitter', $author->ID) != '' ){ echo '<li><a href="'.get_the_author_meta('twitter', $author->ID).'" target="_blank"><i class="fa fa-twitter-square"></i></a></li>'; }?>

			<?php
			if( get_the_author_meta('instagram', $author->ID) != '' ){ echo '<li><a href="'.get_the_author_meta('instagram', $author->ID).'" target="_blank"><i class="fa fa-instagram"></i></a></li>'; }?>

			<?php
			if( get_the_author_meta('pinterest', $author->ID) != '' ){ echo '<li><a href="'.get_the_author_meta('pinterest', $author->ID).'" target="_blank"><i class="fa fa-pinterest-square"></i></a></li>'; }?>

			<?php
			if( get_the_author_meta('linkedin', $author->ID) != '' ){ echo '<li><a href="'.get_the_author_meta('linkedin', $author->ID).'" target="_blank"><i class="fa fa-linkedin-square"></i></a></li>'; }?>

			<?php
			if( get_the_author_meta('youtube', $author->ID) != '' ){ echo '<li><a href="'.get_the_author_meta('youtube', $author->ID).'" target="_blank"><i class="fa fa-youtube-square"></i></a></li>'; }?>
		</ul>
		<!-- Social Icons -->
	</div>

	<?php
	$error = false;
	$int_success = false;



	if(isset($_POST['int_submit'])&& $_POST['int_submit']!=''){

		$path = $_FILES['int_vid']['name'];
		$intro_path = $upload_dir['basedir'];
		$file_tmp  = $_FILES['int_vid']['tmp_name'];

		$upload_dir2 = wp_upload_dir();
		$pathDir = $upload_dir2['path'];

		$int_ext = pathinfo($path, PATHINFO_EXTENSION);
		$int_exts = array('mp4','MP4','mp3','MP3');

		if(in_array($int_ext, $int_exts )){
			$uniqID = uniqid();
			$int_file  = $uniqID.'.'.$int_ext;
			$imageName = $uniqID;
			$current_user =  get_current_user_id();
			$table = $wpdb->prefix."user_intro";

			$intro_data = $wpdb->get_row('SELECT * FROM '.$table.' WHERE user_id='.$current_user);

			if($intro_data){
				$insert = $wpdb->update($table,array(
					'intro_video'	=> $int_file,
					'intro_video_Img' => $imageName.'.png',
					),array( 'id' => $intro_data->id ));
			}else{
				$insert = $wpdb->insert($table,array(
					'user_id'		=> $current_user,
					'intro_video'	=> $int_file,
					'intro_video_Img' => $imageName.'.png',
					));
			}

			$uplaod = move_uploaded_file($file_tmp,$upload_dir['basedir'].'/introvideos/'.$int_file);
			if($uplaod){
				$int_success = true;
                  /*  echo "ffmpeg -y -i ".$upload_dir['basedir'].'/introvideos/'.$int_file." -ss 00:00:14 -vframes 1 -an -vcodec png -f rawvideo -s 500x270 ".$upload_dir['basedir'].'/introvideos/'.$imageName.".png"; */
					exec("ffmpeg -y -i ".$upload_dir['basedir'].'/introvideos/'.$int_file." -ss 00:00:14 -vframes 1 -an -vcodec png -f rawvideo -s 500x270 ".$upload_dir['basedir'].'/introvideos/'.$imageName.".png", $output);
				$msg = 'file Upload Successful';
			}
		}
		else{
			$videos_str = implode(',',$int_exts);
			$error = true;
			$msg = 'Only file type '.$videos_str.' are allowed.';
		}
	}
	?>

	<?php

	if($error){
		$videos_str = implode(',',$int_exts);
		echo '<div class="alert alert-danger">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		'.$msg.'
	</div>';
}

if($int_success){
	echo '<div class="alert alert-success">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	'.$msg.'
</div>';
}

$table = $wpdb->prefix."user_intro";
$intro_data = $wpdb->get_row('SELECT * FROM '.$table.' WHERE user_id='.$profile_id);

$upload_title = ($intro_data)?'Change Introduction Video':'Add Introduction Video';

if(isset($_POST['test'])&& $_POST['test']!=''){
	$table = $wpdb->prefix."user_intro";
	$current_user =  get_current_user_id();
	$delete = $wpdb->delete( $table, array( 'user_id' => $current_user ) );
	$upload_title = 'Add Introduction Video';
	$intro_data = false;
}

if(is_author()){

	$curauth = (get_query_var('author_name')) ? get_user_by('slug', get_query_var('author_name')) : get_userdata(get_query_var('author'));
	if(get_current_user_id() == $curauth->ID || is_super_admin( get_current_user_id() )){
		echo "
		<div class='background_uploader col-md-12 col-sm-full' style='margin-bottom:20px'>
		<h3>Upload Background Here</h3>
		<form class='form-horizontals form-horizontalstwo' method='post' enctype='multipart/form-data'>
			<div class='form-group'>
				<label class='control-label col-md-3 label-class' for='bgfile'>Background Image:</label>
				<div class='col-md-3 tmesutker-clsahss'>
					<input type='file' accept='.jpeg,.jpg,.gif,.png,.JPEG,.JPG,.GIF,.PNG' name='bgfile' id='bgfile'>
					<p id='bg_image_errors'>Please upload only images of type jpeg, jpg, gif or png</p>
				</div>
				<div class='col-md-3 right-colsm'>
					<input type='submit' class='btn btn-success custom_btn_style' name='background_uploader' id='background_uploader' value='Upload'/>";
		if(get_user_meta( get_current_user_id(), 'background_image', true )){
			echo "&nbsp;&nbsp;&nbsp;<input type='submit' class='btn btn-danger' name='background_delete' id='background_delete' value='Delete'/>";
		}
		echo "</div>
			</div>

		</form>
		</div>";
	}
}

if(isset($_POST['background_uploader'])){
	if(($_FILES['bgfile']['type'] == 'image/jpg') || ($_FILES['bgfile']['type'] == 'image/jpeg') || ($_FILES['bgfile']['type'] == 'image/png') || ($_FILES['bgfile']['type'] == 'image/gif')){
		$user_id = get_current_user_id();
		$file = 'wp-content/themes/videopress/images/'.rand($user_id, 99999999).$_FILES['bgfile']['name'];
		move_uploaded_file($_FILES['bgfile']['tmp_name'], $file);

		$posts_array = get_posts(array(
			'posts_per_page' 	=> -1,
			'post_type' 		=> array('post', 'dyn_file'),
			'post_status' 		=> 'publish'
		));
		$followingCategoryArray = array();
		if(isset($posts_array) and is_array($posts_array) and count($posts_array)>0)
		{
			foreach($posts_array as $key=>$val)
			{
				global $post; $post = $val; setup_postdata( $post );
				if(get_the_author_ID() == get_current_user_id()){
					if(get_post_meta( get_the_ID(), 'dyn_background_src', true )){
						$imgData1 = get_post_meta( get_the_ID(), 'dyn_background_src');
						$imgData2 = get_user_meta( $user_id, 'background_image');
						if($imgData1[0] == $imgData2[0]){
							update_post_meta( get_the_ID(), 'dyn_background_src', $file );
						}
					}
				}
		}	}

		update_user_meta($user_id, 'background_image', $file);
		echo '<script>window.location.assign(window.location.href);</script>';
	}else{
		echo '<script>alert("Please upload only images of type jpeg, jpg, gif or png");</script>';
	}
}
if(isset($_POST['background_delete'])){
	$user_id = get_current_user_id();
	if ( ! delete_user_meta($user_id, 'background_image') ) {}
	echo '<script>window.location.assign(window.location.href);</script>';
}

//if($intro_data){
	//echo '<h3>Introduction Video</h3><hr>';
//}
?>

<?php

			$nepfData = json_decode( get_the_author_meta('ne_member_pfdata', get_current_user_id()));
		    
		if(count($nepfData) > 0) {
			echo '<div class="col-md-12 col-sm-full" style="margin-bottom:20px">
		<h3 style="float: left;width: 100%;">Notable Member\'s</h3>
		<div class="entry">';
			
			foreach($nepfData as $value)
			{
				$fileName1 = site_url().'/nm_pic/'.$value->ne_contact_photo;
				echo '<div style="width:110px;height:150px;border: 1px solid #ccc; text-align:center; float:left;margin-right:5px;clear:right;"><img src="'.$fileName1.'">
		
		<br> <a href="'.$value->ne_contact_link.'">'.$value->ne_contact_name.'</a><br> '.$value->ne_contact_title.'

		</div>';
			}
			echo '</div>
</div>		';
		}
				?>

<?php
if($profile_id == get_current_user_id()) {
	?>
	<div class="col-md-12 col-sm-full" style="margin-bottom:20px">
		<h3 style="float: left;width: 100%;">Introduction Video</h3>
		<div class="form-group">
			<form class="form-horizontals form-horizontalsthree" enctype="multipart/form-data" role="form" method="post">
				<label for="int_vid" class="control-label col-md-3 label-class"><?= $upload_title;?>:</label>
				<div class="col-md-3 tmesutker-clsahss">
					<input type="file" id="int_vid" name="int_vid" required>
				</div>
				<div class="col-md-2 sm-2-block">
					<button type="submit" class="btn btn-success custom_btn_style" name="int_submit" value="int_submit">Upload</button>
				</div>
			</form>
	<?php if($intro_data){ ?>
		<form class="form-horizontals" method="post" action="" id="intv_del">
			<input type="hidden" name="test" value="test">
			<button type="submit" class="btn btn-danger" id="int_delete" name="int_delete" value="int_deletes">Delete</button>
		</form>

	<script>
		jQuery('#int_delete').click(function(e){
			e.preventDefault();

			var r = confirm("Are you sure you want to continue !");
			if (r == true) {
				jQuery('#intv_del').submit();
			}

		});
	</script>
	<?php } ?></div></div>
	<div class="clear"></div>
	<?php } ?>

	<?php $intro_data = $wpdb->get_row('SELECT * FROM '.$table.' WHERE user_id='.$profile_id);
	if($intro_data){
		?>
		<div class="videocontainer">
			<div id="VideoPlayer"><img src="<?php echo get_template_directory_uri(); ?>/images/ajax-loader.gif" alt="loading" /></div>
			<div class="clear"></div>
			<script type="text/javascript">
				jQuery(document).ready(function(){
					jwplayer("VideoPlayer").setup({
						file: "<?php echo $upload_dir['url'].'/introvideos/'.$intro_data->intro_video;?>",
					image: "<?php echo $upload_dir['url'].'/introvideos/'.$intro_data->intro_video_Img; ?>",
					width: '100%',
					height: '400px',
					aspectratio: "16:9",
					skin: '<?php videopress_playerskin(); ?>',
					<?php videopress_player_logo(); ?>
				});
					jwplayer("VideoPlayer").stop();

				});
			</script>
			
			<?php 
			/****Removed bg logo
			<div class="video-logo">
				<?php $user_info = get_userdata($profile_id); ?>
				<a href="<?= home_url().'/user/'.$user_info->user_nicename;?>">
					<img src="<?= vp_option('vpt_option.player_logo')?>">
				</a>
			</div>
			**********/?>
		</div>
		<?php } ?>

<?php
				if(isset($_POST['metaname']) && !empty($_POST['metaname'])){
					$metaname = $_POST['metaname'];
					$metaval = $_POST['metaval'];

					update_user_meta(get_current_user_id(),$metaname,$metaval);
				}
				?>
				
			<div class="col-md-12 col-sm-full" style="margin-bottom:20px">	
			<div style="padding-top:30px;">	
				<?php if( get_the_author_meta('additionalinfog', $author->ID) != '' ){
        	echo '<p style="margin-bottom:10px"><strong>Additional Information</strong><br>'.get_the_author_meta('additionalinfog', $author->ID).'</p>';
        } 
        ?>
		
		<?php if( get_the_author_meta('cdescriptiong', $author->ID) != '' || get_the_author_meta('conphoneg', $author->ID) != '' ){
        	echo '<p><strong>Contact Us</strong><br>'.get_the_author_meta('cdescriptiong', $author->ID).'</p>';
			$phoneData = json_decode( get_the_author_meta('conphoneg', get_current_user_id()));
			
			if(count($phoneData) > 0) {

				foreach($phoneData as $value)
				{
					if($value->type == 'phone')
						echo '<i class="fa fa-phone" aria-hidden="true"></i>';
					elseif($value->type == 'email')
						echo '<i class="fa fa-envelope" aria-hidden="true"></i>';
					elseif($value->type == 'fax')
						echo '<i class="fa fa-fax" aria-hidden="true"></i>';	
						
					echo '&nbsp;'.$value->number.'<br>';	
				}
			}
			
			echo '<p>&nbsp;</p>';
        } 
        ?>

				</div>
				</div>
				<?php
			/*********************Commented section to be removed
		<div class="entry">
			<div class="panel-group">
				
				<!-- START About us home page -->
			
			<div class="panel panel-default">
				<div class="panel-heading">
					<a data-toggle="collapse" href="#panelabout">
						<h4 class="panel-title">About <span class="pull-right panel-btn">show</span></h4>
					</a>
				</div>
				<div id="panelabout" class="panel-collapse collapse">
					<div class="panel-body">
					<?php if($profile_id == get_current_user_id()) { ?>
						<span class="pull-right sec-form"  id="e_1"><i class="fa fa-pencil-square-o"></i></span>
					<?php } ?>

					<div id="about-des"><?php echo $user_three_about = get_user_meta($profile_id,'user_three_about',true);?></div>
					<?php
					if(!$user_three_about){
						echo 'No descritption provided';
					}
					?>
					</div>
				</div>
			</div> <?php 
			<!-- END About us home page -->

			<!-- START Credential/Rewards home page -->
			
			<div class="panel panel-default">
				<div class="panel-heading">
					<a data-toggle="collapse" href="#panelCredential">
						<h4 class="panel-title">Credentials <span class="pull-right panel-btn">show</span></h4>
					</a>
				</div>
				<div id="panelCredential" class="panel-collapse collapse">
					<div class="panel-body">
						<?php if($profile_id == get_current_user_id()) { ?>
						<span class="pull-right sec-form"  id="e_2"><i class="fa fa-pencil-square-o"></i></span>
						<?php } ?>

						<div id="reward-des"><?php echo $user_three_reward = get_user_meta($profile_id,'user_three_reward',true);?></div>
						<?php
						if(!$user_three_reward){
							echo 'No descritption provided';
						}
						?>
					</div>
				</div>
			</div>
			<!-- END Credential/Rewards home page -->

			<!-- START Contact us home page -->
			<div class="panel panel-default">
				<div class="panel-heading">
					<a data-toggle="collapse" href="#panelcontact">
						<h4 class="panel-title">Contact <span class="pull-right panel-btn">show</span></h4>
					</a>
				</div>
				<div id="panelcontact" class="panel-collapse collapse">
					<div class="panel-body">
						<?php if($profile_id == get_current_user_id()) { ?>
						<span class="pull-right sec-form" id="e_3"><i class="fa fa-pencil-square-o"></i></span>
						<?php } ?>

						<div id="contact-des"><?php echo $user_three_contact = get_user_meta($profile_id,'user_three_contact',true);?></div>
						<?php
						if(!$user_three_contact){
							echo 'No descritption provided';
						}
						?>
					</div>
					<!--<div class="panel-footer">Panel Footer</div>-->
				</div>
			</div>
			
			
	
			
			<!-- END Contact us home page -->

		</div>
	</div>
	END COMMENTED SECTION	************************/
	?>
	<!-- Start Threee section form submit -->

	<!-- Modal -->
	<div class="modal fade" id="modelthree_section" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">Add Description</h4>
				</div>
				<div class="modal-body">
					<form method="post" id="three-form">
						<input type="hidden" name="metaname" id="metaname">
						<div id="">
							<textarea rows="10" placeholder="Add Description Here" id="metaval" name="metaval"></textarea>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" id="three-submit" class="btn btn-primary">Submit changes</button>
				</div>
			</div>
		</div>
	</div>

	<!-- End Threee section form submit -->
	<script src="//cdn.ckeditor.com/4.5.5/basic/ckeditor.js"></script>
	<script>
		var $ = jQuery.noConflict();
		jQuery(document).ready(function(){
			var $ = jQuery.noConflict();
			$(".panel-btn").html('<span class="glyphicon glyphicon-collapse-down"></span>');

			$("#panelcontact,#panelCredential,#panelabout").on("hide.bs.collapse", function(){
				$(this).parent().find(".panel-btn").html('<span class="glyphicon glyphicon-collapse-down"></span>');
			});
			$("#panelcontact,#panelCredential,#panelabout").on("show.bs.collapse", function(){
				$(this).parent().find(".panel-btn").html('<span class="glyphicon glyphicon-collapse-up"></span>');
			});

			$(".sec-form").click(function(){
				var t_section = $(this).attr('id');

				if(t_section=='e_1'){
					$("#metaname").val('user_three_about');
					//$("#metaval").val($("#about-des").html());
					$(".cke_textarea_inline").html($("#about-des").html());

					$("#myModalLabel").html('write About yourself');
				}else if(t_section=='e_2'){
					$("#metaname").val('user_three_reward');
					//$("#metaval").val($("#reward-des").html());
					$(".cke_textarea_inline").html($("#reward-des").html());
					$("#myModalLabel").html('Write Credential/Rewards text');
				}else{
					$("#metaname").val('user_three_contact');
					//$("#metaval").val($("#contact-des").html());
					$(".cke_textarea_inline").html($("#contact-des").html());
					$("#myModalLabel").html('Contact information');
				}

				$('#modelthree_section').modal('show');
			})

			$("#three-submit").click(function(){
				$("#three-form").submit();
			})
		});

		//CKEDITOR.replace( 'metaval' );
		//CKEDITOR.disableAutoInline = true;

		CKEDITOR.inline( 'metaval' ,{font_defaultLabel : 'Arial',fontSize_defaultLabel : '44px'});
		CKEDITOR.editorConfig = function( config ) {
			config.font_defaultLabel = 'Arial';
			config.fontSize_defaultLabel = '44px';
		};

	</script>
	<div class="total-sec">
		<?php echo apply_filters( 'dyn_channel_home_review', $profile_id ); ?>

		<div class="col-md-3">
			<p>Total Endersment</p>
			<div class="icon-inner">
				<i class="fa fa-wrench"></i>
				<?php global $wpdb;
				$endersment_c = $wpdb->get_results('SELECT count(*) as counts FROM '.$wpdb->prefix.'endorsements endors join wp_posts posts on posts.ID = endors.post_id where posts.post_author = '.$profile_id); ?>
				<p><?= $endersment_c['0']->counts; ?></p>
			</div>
		</div>

		<div class="col-md-3">
			<p>Total Subscribers</p>
			<div class="icon-inner">
				<i class="fa fa-user"></i>
				<?php
				$subs_c = $wpdb->get_results('SELECT count(author_id) as counts FROM '.$wpdb->prefix.'authors_suscribers WHERE author_id = '.$profile_id);
				?>
				<p><?= $subs_c['0']->counts; ?></p>
			</div>
		</div>


		<div class="col-md-3">
			<p>Total Views</p>
			<div class="icon-inner">
				<i class="fa fa-eye"></i>
				<?php
				$view_c = $wpdb->get_results('select sum(m.meta_value) as view_count from '.$wpdb->prefix.'postmeta m join (SELECT * FROM '.$wpdb->prefix.'posts where post_type = "post" AND post_status = "publish" AND post_author = '.$profile_id.' ) p on p.ID = m.post_id where m.meta_key = "popularity_count"');

				?>
				<p><?php if($view_c['0']->view_count){ echo $view_c['0']->view_count;}else{ echo '0';} ?></p>
			</div>
		</div>

		<div class="col-md-3 download">
			<p>Total Downloads</p>
			<div class="icon-inner">
				<i class="fa fa-download"></i>
				<?php
				$download = $wpdb->get_results('select sum(m.meta_value) as downtotal from  '.$wpdb->prefix.'postmeta m join (SELECT * FROM '.$wpdb->prefix.'posts where post_type = "dyn_file" AND post_status = "publish" AND post_author = '.$profile_id.' ) p on p.ID = m.post_id where m.meta_key = "dyn_download_count"');
				?>
				<p><?php if(empty($download['0']->downtotal)){echo 0;}else{echo $download['0']->downtotal;} ?></p>
			</div>
		</div>


		<div class="col-md-3">
			<p>Favorited</p>
			<div class="icon-inner">
				<i class="fa fa-heart"></i>
				<?php global $wpdb;
				$favorite_c = $wpdb->get_results('SELECT count(*) as counts FROM '.$wpdb->prefix.'favorite fav join wp_posts posts on posts.ID = fav.post_id where posts.post_author = '.$profile_id.' and fav.user_id='.$profile_id);
				?>

				<p><?= intval($favorite_c['0']->counts + 1); ?></p>
			</div>
		</div>
	</div>


</div>

<?php // Forum Profile TAB // ?>
<div role="tabpanel" class="tab-pane <?= $forum_active;?> " id="forum">
	<style>
		.avtarimg .avatar{width:150px;height:150px;}
	</style>
	<?php
		//echo bbp_get_current_user_id();
	$user_id 		= bbp_current_user_id_custom();
	echo do_shortcode('[userForum user_id='.$user_id.']');
	?>
</div>


<?php

  if(isset($_GET['store']) && $_GET['store']!=''){
			$home_active = false;
			$new_store_active = 'active'
?>

 <script>
 $('.nav.nav-tabs li').removeClass

</script>

 <?php } ?>































































<?php // VIDEO TAB // ?>
<div role="tabpanel" class="tab-pane <?= $vid_active;?> " id="videos">
	<h6 class="title-bar"><span>Videos By <?php echo get_the_author_meta('nickname', $author->ID ); ?></span></h6>

	<div class="alert alert-success v-d" style="display:none;">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<strong>Success!</strong> Video is Deleted.
	</div>

	<?php
	$nothing = true;
	$selected = false;
	$current = true;
	$views=false;
	$endorsements=false;
	$favourites=false;
	$comments=false;

	$category = get_category( get_query_var( 'cat' ) );
	$catid = $category->cat_ID;
	if(isset($_GET['sortvideo']) && $_GET['sortvideo']!=''){
		$nothing = false;
		$sortvideo = filter_input(INPUT_GET, 'sortvideo', FILTER_SANITIZE_SPECIAL_CHARS);
		echo '<br>';
		if($sortvideo=='views'):
			$views=true;
		$my_query = new WP_Query( array(
						//'posts_per_page' => 10,
			'post_type' => 'post',
			'post_author' => $profile_id,
						//'cat'=>$catid,
			'meta_key' => 'popularity_count',
			'orderby'	=> 'meta_value_num',
			'order'		=> 'DESC',
			'meta_query' => array(
				array(
					'key' => 'popularity_count',
					)
				),
			'paged' => $paged,
			));


		elseif($sortvideo=='endorsements'):
			$endorsements=true;

		$limit = 'limit ';
		$limit .= $paged*10 ;
		$limit .= ', 10';

				//$querystr = "SELECT p.*,endocount FROM wp_posts p LEFT join (select *,count(*) as endocount from wp_endorsements group by post_id ) endors on p.ID = endors.post_id where p.post_type = 'post' AND p.post_status = 'publish' AND p.post_author = ' ".$profile_id."' order by endors.endocount desc $limit";

		$querystr = "SELECT p.*,endocount FROM wp_posts p left join (select *,count(*) as endocount from wp_endorsements group by post_id ) endors on p.ID = endors.post_id where p.post_type = 'post' AND p.post_status = 'publish' AND p.post_author = '".$profile_id."' order by endors.endocount desc ". $limit;

		$pageposts = $wpdb->get_results($querystr, OBJECT);

		elseif($sortvideo=='favourites'):
			$favourites=true;

		$limit = 'limit ';
		$limit .= $paged*10 ;
		$limit .= ', 10';

		$querystr = "SELECT p.*,favcount FROM wp_posts p left join (select *,count(*) as favcount from wp_favorite group by post_id ) favorite on p.ID = favorite.post_id where p.post_type = 'post' AND p.post_status = 'publish' AND p.post_author = '".$profile_id."' order by favorite.favcount desc $limit";

		$pageposts = $wpdb->get_results($querystr, OBJECT);

				//$selected = true;
		elseif($sortvideo=='comments'):
			$my_query = new WP_Query( array(
				'post_type' => 'post',
				'post_author' => $profile_id,
				'cat'=>$catid,
				'orderby'=> 'comment_count',
				'order'		=> 'DESC',
				'paged' => $paged,
				));

		$comments=true;
		else:
			$selected = true;
		endif;
		$current = false;
	}

	?>
	<div class="cover-twoblocks">
		<div class="sortvideo-div">
		<form method="get" id="sortvideo-form">
			<p><label>Currently Sorted By : </label>
				<select class="sortvideo" name="sortvideo">
					<option value="recent" <?= ($selected)?'selected':'';?>>Recent</option>
					<option value="views" <?= ($views)?'selected':'';?>>Views</option>
					<option value="endorsements" <?= ($endorsements)?'selected':'';?>>Endorsements</option>
					<option value="favourites" <?= ($favourites)?'selected':'';?>>Favorites</option>
					<option value="comments" <?= ($comments)?'selected':'';?>>comments</option>
				</select>
			</p>
		</form>
		</div>

		<div class="well well-sm">
			<strong>Views</strong>
			<div class="btn-group">
				<a href="#" id="list" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th-list">
				</span>List</a> <a href="#" id="grid" class="btn btn-default btn-sm"><span
					class="glyphicon glyphicon-th"></span>Grid</a>
			</div>
		</div>
    </div>
	<script>
		jQuery('.sortvideo').change(function(){
			this.form.submit();
		})
	</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#products .item').addClass('grid-group-item');
    $('#list').click(function(event){
    	event.preventDefault();
    	$('#products .item').addClass('list-group-item changeStyleList');

    });
    $('#grid').click(function(event){
    	event.preventDefault();
    	$('#products .item').removeClass('list-group-item changeStyleList');
    	$('#products .item').addClass('grid-group-item changeStyleGrid');
    });
});
</script>

	<?php

	if($current || $selected || $nothing):

		/* ================================================================== */
	/* Start of loop */
	/* ================================================================== */

$paged = ( get_query_var( 'page' ) ) ? absint( get_query_var( 'page' ) ) : 1;

$cat=isset($_GET['display_tab']) ? $_GET['display_tab'] : '';

if($cat=='video')
$start = ($paged==1) ? 0 : intval($paged-1) * 12;
else
$start=0;


$temp= 'SELECT * FROM  '.$wpdb->prefix.'posts WHERE post_type="post" AND post_author='.$profile_id.' ORDER BY STR_TO_DATE( '.$wpdb->prefix. 'posts.post_date_gmt , "%Y-%m-%d %H:%i:%s")  DESC';

$tquery = $temp.' LIMIT  ' . $start .', 12';
//create custom loop for videos
$result = $wpdb->get_results( $tquery );
	 //echo  $wpdb->num_rows . 'Rows Found';

$total=$wpdb->query($temp);

	$x = 0;
	if ($result){
	echo '<div id="products" class="list-group">';
	foreach ($result as $data){
	$x++;
       $postID = $data->ID;
	   $post=get_post($postID);
	   $privacyOption   = get_post_meta( $postID, 'privacy-option' );
	   $selectUsersList = get_post_meta( $postID, 'select_users_list' );
	   $post_author     = $post->post_author;
	   $user_ID         = get_current_user_id();
	   $selectUsersList = explode( ",", $selectUsersList[0] );
	?>
	<!-- Start Layout Wrapper -->

<?php
     if(!is_user_logged_in())
		{  // case where user is not logged in
	             if(isset($privacyOption[0]) && ($privacyOption[0] != "private"))
				  {
					     ?>
						    <div class="item col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 post-<?php the_ID(); ?>">
						      <div class="thumbnail layout-2-wrapper solid-bg">
						 <?php
				  }
				  else
				  {
					    if(!isset($privacyOption[0]))
						{
							 ?>
							  <div class="item col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 post-<?php the_ID(); ?>">
								  <div class="thumbnail layout-2-wrapper solid-bg">
							 <?php
						}
				  }
		}
		else
		{  // case where user is logged in
			 if($post_author == $user_ID)
			 {    // Case where logged in User is same as Video Author User
				     ?>
					     <div class="item col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 post-<?php the_ID(); ?>">
						  <div class="thumbnail layout-2-wrapper solid-bg">
					 <?php
			 }
			 else
			 {    // Case where logged in User is not same as Video Author User
				  if(isset($privacyOption[0]) && $privacyOption[0] != "private")
				  {
					    ?>
						    <div class="item col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 post-<?php the_ID(); ?>">
						      <div class="thumbnail layout-2-wrapper solid-bg">
						 <?php
				  }
				  else
				  {
					  if(!isset($privacyOption[0]))
					  {
						    ?>
							 <div class="item col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 post-<?php the_ID(); ?>">
						      <div class="thumbnail layout-2-wrapper solid-bg">
						 <?php
					  }
                      else
					  {
						    if( is_array($selectUsersList) and count($selectUsersList) > 0 )
							   {   // case where user access list is available
									 if( in_array($user_ID, $selectUsersList) )
									 {
										   ?>
										   <div class="item col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2 post-<?php the_ID(); ?>">
											  <div class="thumbnail layout-2-wrapper solid-bg">
										 <?php
									 }
									 else
									 {
									 }
							   }
							   else
							   {  }
					  }
				  }
			 }
		}
	    if(!is_user_logged_in())
		{  // case where user is not logged in

	             if(isset($privacyOption[0]) && ($privacyOption[0] != "private"))
				  {


					   ?>
						   <div class="grid-6 first">
								<div class="image-holder">
									<a href="<?php echo get_permalink(); ?>">
										<div class="hover-item"></div>
									</a>
									<?php
									if( has_post_thumbnail() ){
										the_post_thumbnail('small-thumb', array( 'class' => 'layout-2-thumb' ) );
									}else{
										echo '<img src="'.get_template_directory_uri().'/images/no-image.png" class="layout-2-thumb">';
									}
									?>
								</div>
								<div class="layout-title-box layout-title-box-<?php the_ID(); ?>" style="display:block;">
									<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php echo ctpreview( $content = get_the_title() , $length = 39 , $message = 'No Title available' , $html = ' [...]' ); ?></a></h6>
									<ul class="bottom-detailsul">
										<?php if($profile_id == get_current_user_id()){ ?>
										<li><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>"><i class="fa fa-trash-o"></i> Delete</a></li>
										<?php } ?>
										<li><a class="detailsblock-btn" data-modal-id="myModal-<?php the_ID(); ?>"><i class="fa fa-info-circle" aria-hidden="true"></i> Details</a></li>
									</ul>
								</div>
							</div>
							<script type="text/javascript">
$(function(){

var appendthis =  ("<div class='modal-overlayblockdiv'></div>");

	$('a[data-modal-id]').click(function(e) {
		e.preventDefault();
    $("body").append(appendthis);
    $(".modal-overlay").fadeTo(500, 0.7);
    //$(".js-modalbox").fadeIn(500);
		var modalBox = $(this).attr('data-modal-id');
		$('#'+modalBox).fadeIn($(this).data());
	});


$(".js-modal-close, .modal-overlay").click(function() {
    $(".modal-box, .modal-overlay").fadeOut(500, function() {
        $(".modal-overlay").remove();
    });

});

$(window).resize(function() {
    $(".modal-box").css({
        top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
        left: ($(window).width() - $(".modal-box").outerWidth()) / 2
    });
});

$(window).resize();

});
</script>

			<div class="modal-box" id="myModal-<?php the_ID(); ?>">
			<div class="modal-body">
			<a class="js-modal-close close">×</a>
			<div class="layout-2-details layout-2-details-<?php the_ID(); ?>">
				<div class="image-holder">
					<a href="<?php echo get_permalink(); ?>">
						<div class="hover-item"></div>
					</a>

					<?php
					if( has_post_thumbnail() ){
						the_post_thumbnail('small-thumb', array( 'class' => 'layout-2-thumb' ) );
					}else{
						echo '<img src="'.get_template_directory_uri().'/images/no-image.png" class="layout-2-thumb">';
					}
					?>

				</div>
				<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h6>
				<?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
				<div class="video_excerpt_text one"><h4 class="tittle-h4">Description 2</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = '' , $message = 'No Description available' , $html = ' [...]' );?></div></div>
				<div class="video_excerpt_text two"><h4 class="tittle-h4">Reference</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_post_meta(get_the_ID(),'video_options_refr',true) , $length = '' , $message = 'No referance given' , $html = ' [...]' );?></div></div>
				<ul class="stats">
					<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
					<li><?php videopress_countviews( get_the_ID() ); ?></li>
					<li><i class="fa fa-wrench"></i> <?php $result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
						echo count($result); ?> Endorsments</li>
					<!-- favourite section -->
					<li><i class="fa fa-heart"></i>
					<?php
					  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
					  $result1 = $wpdb->get_results($sql_aux);
					  echo count($result1);
					?> Favorites</li>
					<!-- end favorite -->
					<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
					<li><?php comments_number() ?></li>
				</ul>
				<div class="clear"></div>
				<p><?php echo videopress_content('240'); ?></p>

					<?php if($profile_id == get_current_user_id()){ ?>
					<p><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>"><i class="fa fa-trash-o"></i> Delete</a></p>
					<?php } ?>


			</div></div></div>

			<div class="layout-2-details layout-2-details-<?php the_ID(); ?>" style="display:none;">
				<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h6>
				<?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
				<div class="video_excerpt_text one"><h4 class="tittle-h4">Description</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = 100 , $message = 'No Description available' , $html = '<a data-modal-id="myModal-'.get_the_ID().'"> [...]</a>' ); ?></div></div>
				<ul class="stats">
					<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
					<li><?php videopress_countviews( get_the_ID() ); ?></li>
					<li><i class="fa fa-wrench"></i> <?php $result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
						echo count($result); ?> Endorsments</li>
					<!-- favourite section -->
					<li><i class="fa fa-heart"></i>
					<?php
					  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
					  $result1 = $wpdb->get_results($sql_aux);
					  echo count($result1);
					?> Favorites</li>
					<!-- end favorite -->
					<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
					<li><?php comments_number() ?></li>
				</ul>
				<div class="clear"></div>
				<p><?php echo videopress_content('240'); ?></p>

					<?php if($profile_id == get_current_user_id()){ ?>
					<p><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>"><i class="fa fa-trash-o"></i> Delete</a></p>
					<?php } ?>


			</div>

			<div class="clear"></div>
					   <?php
				  }
				  else
				  {
					    if(!isset($privacyOption[0]))
						{
							?>
							   <div class="grid-6 first">
									<div class="image-holder">
										<a href="<?php echo get_permalink(); ?>">
											<div class="hover-item"></div>
										</a>
										<?php
										if( has_post_thumbnail() ){
											the_post_thumbnail('small-thumb', array( 'class' => 'layout-2-thumb' ) );
										}else{
											echo '<img src="'.get_template_directory_uri().'/images/no-image.png" class="layout-2-thumb">';
										}
										?>
									</div>
									<div class="layout-title-box layout-title-box-<?php the_ID(); ?>" style="display:block;">
										<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php echo ctpreview( $content = get_the_title() , $length = 39 , $message = 'No Title available' , $html = ' [...]' ); ?></a></h6>
										<ul class="bottom-detailsul">
											<?php if($profile_id == get_current_user_id()){ ?>
											<li><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>"><i class="fa fa-trash-o"></i> Delete</a></li>
											<?php } ?>
											<li><a class="detailsblock-btn" data-modal-id="myModal-<?php the_ID(); ?>"><i class="fa fa-info-circle" aria-hidden="true"></i> Details</a></li>
										</ul>
									</div>
								</div>
								<script type="text/javascript">
$(function(){

var appendthis =  ("<div class='modal-overlayblockdiv'></div>");

	$('a[data-modal-id]').click(function(e) {
		e.preventDefault();
    $("body").append(appendthis);
    $(".modal-overlay").fadeTo(500, 0.7);
    //$(".js-modalbox").fadeIn(500);
		var modalBox = $(this).attr('data-modal-id');
		$('#'+modalBox).fadeIn($(this).data());
	});


$(".js-modal-close, .modal-overlay").click(function() {
    $(".modal-box, .modal-overlay").fadeOut(500, function() {
        $(".modal-overlay").remove();
    });

});

$(window).resize(function() {
    $(".modal-box").css({
        top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
        left: ($(window).width() - $(".modal-box").outerWidth()) / 2
    });
});

$(window).resize();

});
</script>

			<div class="modal-box" id="myModal-<?php the_ID(); ?>">
			<div class="modal-body">
			<a class="js-modal-close close">×</a>
			<div class="layout-2-details layout-2-details-<?php the_ID(); ?>">
				<div class="image-holder">
					<a href="<?php echo get_permalink(); ?>">
						<div class="hover-item"></div>
					</a>

					<?php
					if( has_post_thumbnail() ){
						the_post_thumbnail('small-thumb', array( 'class' => 'layout-2-thumb' ) );
					}else{
						echo '<img src="'.get_template_directory_uri().'/images/no-image.png" class="layout-2-thumb">';
					}
					?>

				</div>
				<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h6>
				<?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
				<div class="video_excerpt_text one"><h4 class="tittle-h4">Description 2</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = '' , $message = 'No Description available' , $html = ' [...]' );?></div></div>
				<div class="video_excerpt_text two"><h4 class="tittle-h4">Reference</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_post_meta(get_the_ID(),'video_options_refr',true) , $length = '' , $message = 'No referance given' , $html = ' [...]' );?></div></div>
				<ul class="stats">
					<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
					<li><?php videopress_countviews( get_the_ID() ); ?></li>
					<li><i class="fa fa-wrench"></i> <?php $result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
						echo count($result); ?> Endorsments</li>
					<!-- favourite section -->
					<li><i class="fa fa-heart"></i>
					<?php
					  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
					  $result1 = $wpdb->get_results($sql_aux);
					  echo count($result1);
					?> Favorites</li>
					<!-- end favorite -->
					<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
					<li><?php comments_number() ?></li>
				</ul>
				<div class="clear"></div>
				<p><?php echo videopress_content('240'); ?></p>

					<?php if($profile_id == get_current_user_id()){ ?>
					<p><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>"><i class="fa fa-trash-o"></i> Delete</a></p>
					<?php } ?>


			</div></div></div>

			<div class="layout-2-details layout-2-details-<?php the_ID(); ?>" style="display:none;">
				<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h6>
				<?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
				<div class="video_excerpt_text one"><h4 class="tittle-h4">Description</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = 100 , $message = 'No Description available' , $html = '<a data-modal-id="myModal-'.get_the_ID().'"> [...]</a>' ); ?></div></div>
				<ul class="stats">
					<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
					<li><?php videopress_countviews( get_the_ID() ); ?></li>
					<li><i class="fa fa-wrench"></i> <?php $result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
						echo count($result); ?> Endorsments</li>
					<!-- favourite section -->
					<li><i class="fa fa-heart"></i>
					<?php
					  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
					  $result1 = $wpdb->get_results($sql_aux);
					  echo count($result1);
					?> Favorites</li>
					<!-- end favorite -->
					<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
					<li><?php comments_number() ?></li>
				</ul>
				<div class="clear"></div>
				<p><?php echo videopress_content('240'); ?></p>

					<?php if($profile_id == get_current_user_id()){ ?>
					<p><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>"><i class="fa fa-trash-o"></i> Delete</a></p>
					<?php } ?>


			</div>

			<div class="clear"></div>
						   <?php
						}
				  }
		}
		else
		{  // case where user is logged in
			 if($post_author == $user_ID)
			 {    // Case where logged in User is same as Video Author User
				   ?>
				       <div class="grid-6 first">
							<div class="image-holder">
								<a href="<?php echo get_permalink(); ?>">
									<div class="hover-item"></div>
								</a>
								<?php
								if( has_post_thumbnail() ){
									the_post_thumbnail('small-thumb', array( 'class' => 'layout-2-thumb' ) );
								}else{
									echo '<img src="'.get_template_directory_uri().'/images/no-image.png" class="layout-2-thumb">';
								}
								?>
							</div>
							<div class="layout-title-box layout-title-box-<?php the_ID(); ?>" style="display:block;">
								<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php echo ctpreview( $content = get_the_title() , $length = 39 , $message = 'No Title available' , $html = ' [...]' ); ?></a></h6>
								<ul class="bottom-detailsul">
									<?php if($profile_id == get_current_user_id()){ ?>
									<li><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>"><i class="fa fa-trash-o"></i> Delete</a></li>
									<?php } ?>
									<li><a class="detailsblock-btn" data-modal-id="myModal-<?php the_ID(); ?>"><i class="fa fa-info-circle" aria-hidden="true"></i> Details</a></li>
									<li><a class="changesettingsblock-btn" data-modal-id="myModalPrivacy-<?php the_ID(); ?>"><i class="fa fa-info-circle" aria-hidden="true"></i>Privacy</a></li>
								</ul>
							</div>
					    </div>
						<script type="text/javascript">
$(function(){

var appendthis =  ("<div class='modal-overlayblockdiv'></div>");

	$('a[data-modal-id]').click(function(e) {
		e.preventDefault();
    $("body").append(appendthis);
    $(".modal-overlay").fadeTo(500, 0.7);
    //$(".js-modalbox").fadeIn(500);
		var modalBox = $(this).attr('data-modal-id');
		$('#'+modalBox).fadeIn($(this).data());
	});


$(".js-modal-close, .modal-overlay").click(function() {
    $(".modal-box, .modal-overlay").fadeOut(500, function() {
        $(".modal-overlay").remove();
    });

});

$(window).resize(function() {
    $(".modal-box").css({
        top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
        left: ($(window).width() - $(".modal-box").outerWidth()) / 2
    });
});

$(window).resize();

});
</script>

<div class="modal-box" id="myModalPrivacy-<?php the_ID(); ?>">
			<div class="modal-body">
				<a class="js-modal-close close privacyCloseI-<?php the_ID(); ?>">×</a>
				<div class="layout-2-details layout-2-details-<?php the_ID(); ?>">

						<div class="form-group">
							<label for="dyn-tags"><br/>Privacy Options:</label>
							<div class="checkbox">
								  <?php
								  //$selectUsersListArray = explode(",", $selectUsersList[0]);
								    $selectUsersListArray = $selectUsersList;
								     if(isset($privacyOption[0]) and $privacyOption[0] != "")
									 {
								       //echo $privacyOption[0]."<br>";
										 ?>
										    <label>Private
											  <?php
											     if($privacyOption[0] == "private")
												 {
											        ?>
													   <input type="radio" id="privacy-radio-<?php the_ID(); ?>" name="privacy-option-<?php the_ID(); ?>" class="dyn-select-file" class="form-control" value="private" checked="checked">
													<?php
												 }
												 else
												 {
													 ?>
													   <input type="radio" id="privacy-radio-<?php the_ID(); ?>" name="privacy-option-<?php the_ID(); ?>" class="dyn-select-file" class="form-control" value="private">
													<?php
												 }
											  ?>
											</label>
											<label>Public
											<input type="radio" id="privacy-radio1-<?php the_ID(); ?>" class="dyn-select-file" class="form-control" value="public" <?php if (isset($privacyOption[0]) && $privacyOption[0]=="public") echo "checked";?> name="privacy-option2-<?php the_ID(); ?>">
											</label>
										 <?php
									 }
									 else
									 {
										 ?>
										    <label>Private
											  <input type="radio" id="privacy-radio-<?php the_ID(); ?>" name="privacy-option-<?php the_ID(); ?>" class="dyn-select-file" class="form-control" value="private">
											</label>
											<label>Public
											 <input type="radio" id="privacy-radio1-<?php the_ID(); ?>" name="privacy-option2-<?php the_ID(); ?>" class="dyn-select-file" class="form-control" value="public">
											</label>
										 <?php
									 }
								  ?>
							</div>
							 <?php
								 if(isset($privacyOption[0]) && $privacyOption[0] != "")
								 {
									   if($privacyOption[0] == "private")
									   {
										    ?>
											<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/jquery.tokenize.js"></script>
												<div class="select-box-<?php the_ID(); ?>">
													<?php
													  echo '<select id="tokenize-'.get_the_ID().'" class="tokenize-sample" name="select_users_list[]" style="width: 350px; height: 150px;" multiple="multiple">';
														  $args1 = array(
															  'role' => 'free_user',
															  'orderby' => 'id',
															  'order' => 'desc'
														   );
														  $subscribers = get_users($args1);
														  foreach ($subscribers as $user) {
															  if(get_current_user_id() == $user->id)
															  { }
															  else
															  {
														          if( in_array($user->id, $selectUsersListArray) )
																  {
																	   echo '<option value="'.$user->id.'" selected="selected">' . $user->display_name.'</option>';
																  }
                                                                  else{
																	   echo '<option value="'.$user->id.'">' . $user->display_name.'</option>';
																  }
															  }
														  }
													  echo '</select>';
													?>
													<script type="text/javascript">
											          $(document).ready(function(){
														  $("#privacy-radio").click(function(){
															 $(".select-box").slideDown();
														  });
														  $("#privacy-radio1").click(function(){
															 $(".select-box").slideUp();
															 $('select#select_users_list option').removeAttr("selected");
														  });

													  });
													  $('#tokenize-<?php echo get_the_ID(); ?>').tokenize();
				                                         datas: "bower.json.php"
												  </script>
												  <style> .tokenize-sample { width: 350px;; }</style>
												</div>
										   <?php
									   }
									   else
									   {
                                            ?>
											<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/jquery.tokenize.js"></script>
												<div style="display:none;" class="select-box-<?php the_ID(); ?>">
													<?php
													  echo '<select id="tokenize-'.get_the_ID().'" class="tokenize-sample" name="select_users_list[]" style="width: 350px; height: 150px;" multiple="multiple">';
														  $args1 = array(
															  'role' => 'free_user',
															  'orderby' => 'id',
															  'order' => 'desc'
														   );
														  $subscribers = get_users($args1);
														  foreach ($subscribers as $user) {
															  if(get_current_user_id() == $user->id)
															  { }
															  else
															  {
																  echo '<option value="'.$user->id.'">' . $user->display_name.'</option>';
															  }
														  }
													  echo '</select>';
													?>
													<script type="text/javascript">
											          $(document).ready(function(){
														  $("#privacy-radio").click(function(){
															 $(".select-box").slideDown();
														  });
														  $("#privacy-radio1").click(function(){
															 $(".select-box").slideUp();
															 $('select#select_users_list option').removeAttr("selected");
														  });

													  });
													  $('#tokenize-<?php echo get_the_ID(); ?>').tokenize();
				                                         datas: "bower.json.php"
												  </script>
												  <style> .tokenize-sample { width: 350px;; }</style>
												</div>
										   <?php
									   }
								 }
								 else
								 {
									   ?>
									   <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/jquery.tokenize.js"></script>
									        <div style="display:none;" class="select-box-<?php the_ID(); ?>">
												<?php
												  echo '<select id="tokenize-'.get_the_ID().'" class="tokenize-sample" name="select_users_list[]" style="width: 350px; height: 150px;" multiple="multiple">';
													  $args1 = array(
														  'role' => 'free_user',
														  'orderby' => 'id',
														  'order' => 'desc'
													   );
													  $subscribers = get_users($args1);
													  foreach ($subscribers as $user) {
														  if(get_current_user_id() == $user->id)
														  { }
														  else
														  {
															  echo '<option value="'.$user->id.'">' . $user->display_name.'</option>';
														  }
													  }
												  echo '</select>';
												?>
												<script type="text/javascript">
											          $(document).ready(function(){
														  $("#privacy-radio").click(function(){
															 $(".select-box").slideDown();
														  });
														  $("#privacy-radio1").click(function(){
															 $(".select-box").slideUp();
															 $('select#select_users_list option').removeAttr("selected");
														  });

													  });
													  $('#tokenize-<?php echo get_the_ID(); ?>').tokenize();
				                                         datas: "bower.json.php"
												  </script>
												  <style> .tokenize-sample { width: 350px;; }</style>
											</div>
									   <?php
								 }
							 ?>
							 <br>
							 <div id="msgLoader-<?php the_ID(); ?>"></div>
                             <input type="button" id="save-Privacy-Option-<?php the_ID(); ?>" name="save-privacy-option-<?php the_ID(); ?>" value="Save Privacy Option" currentUserID="<?php echo get_current_user_id(); ?>" postID="<?php echo get_the_ID(); ?>">
						</div>
					<script>
                      $(document).ready(function(){
						  $("#privacy-radio-<?php the_ID(); ?>").click(function(){
							 $("#privacy-radio1-<?php the_ID(); ?>").prop( "checked", false );
						     $(".select-box-<?php the_ID(); ?>").slideDown();
						  });
						  $("#privacy-radio1-<?php the_ID(); ?>").click(function(){
							 $("#privacy-radio-<?php the_ID(); ?>").prop( "checked", false );
						     $(".select-box-<?php the_ID(); ?>").slideUp();
							 $('select#select_users_list-<?php the_ID(); ?> option').removeAttr("selected");
						  });
						  $(".privacyCloseI-<?php the_ID(); ?>").click(function(){
							 $('#msgLoader-<?php the_ID(); ?>').html('');
						  });
						  $("#save-Privacy-Option-<?php the_ID(); ?>").click(function(){
							   var userID = $(this).attr('currentUserID');
							   var postID = $(this).attr('postID');
							   var select_users_list = $('#tokenize-'+postID).val();
							   if ( typeof(select_users_list) !== "undefined" && select_users_list !== null )
							   { }
						       else
							   {
								   select_users_list = '';
							   }
							   var privacyOptionV = "";
							   if($('#privacy-radio-<?php the_ID(); ?>').is(':checked'))
							   {
								   var privateChecked = "yes";
							   }
							   else
							   {
								   var privateChecked = "no";
							   }
							   if($('#privacy-radio1-<?php the_ID(); ?>').is(':checked'))
							   {
								   var publicChecked = "yes";
							   }
							   else
							   {
								   var publicChecked = "no";
							   }
							   if(privateChecked == "yes")
							   {
								   privacyOptionV = "private";
							   }
							   else if(publicChecked == "yes")
							   {
								   privacyOptionV = "public";
							   }
							   else
							   {
								   privacyOptionV = "";
							   }
                               $.ajax({
									type: 'POST',
									url: "<?php echo site_url(); ?>/update-Privacy.php",
									data: { userID: userID, postID: postID, privacyOptionV : privacyOptionV,
									select_users_list : select_users_list},
									beforeSend: function(){
									  $('#msgLoader-<?php the_ID(); ?>').html('<img src="<?php echo site_url(); ?>/wp-content/themes/videopress/images/loading.gif" />');
									},
									success: function(data){
										 $('#msgLoader-<?php the_ID(); ?>').html(data);
									}
								});
						  });
                      });
					 // $('#tokenize').tokenize();
                    </script>
					<style> .tokenize-sample { width: 350px;; }</style>
					<style>
                      #privacy-radio1-<?php the_ID(); ?>, #privacy-radio-<?php the_ID(); ?> {
                        padding: 5px;
                        text-align: center;
                      }

                     #select-box-<?php the_ID(); ?> {
                       padding: 50px;
                       display: none;
                     }
                    </style>
				</div>
			</div>
</div>
			<div class="modal-box" id="myModal-<?php the_ID(); ?>">
			<div class="modal-body">
			<a class="js-modal-close close">×</a>
			<div class="layout-2-details layout-2-details-<?php the_ID(); ?>">
				<div class="image-holder">
					<a href="<?php echo get_permalink(); ?>">
						<div class="hover-item"></div>
					</a>

					<?php
					if( has_post_thumbnail() ){
						the_post_thumbnail('small-thumb', array( 'class' => 'layout-2-thumb' ) );
					}else{
						echo '<img src="'.get_template_directory_uri().'/images/no-image.png" class="layout-2-thumb">';
					}
					?>

				</div>
				<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h6>
				<?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
				<div class="video_excerpt_text one"><h4 class="tittle-h4">Description 2</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = '' , $message = 'No Description available' , $html = ' [...]' );?></div></div>
				<div class="video_excerpt_text two"><h4 class="tittle-h4">Reference</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_post_meta(get_the_ID(),'video_options_refr',true) , $length = '' , $message = 'No referance given' , $html = ' [...]' );?></div></div>
				<ul class="stats">
					<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
					<li><?php videopress_countviews( get_the_ID() ); ?></li>
					<li><i class="fa fa-wrench"></i> <?php $result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
						echo count($result); ?> Endorsments</li>
					<!-- favourite section -->
					<li><i class="fa fa-heart"></i>
					<?php
					  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
					  $result1 = $wpdb->get_results($sql_aux);
					  echo count($result1);
					?> Favorites</li>
					<!-- end favorite -->
					<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
					<li><?php comments_number() ?></li>
				</ul>
				<div class="clear"></div>
				<p><?php echo videopress_content('240'); ?></p>

					<?php if($profile_id == get_current_user_id()){ ?>
					<p><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>"><i class="fa fa-trash-o"></i> Delete</a></p>
					<?php } ?>


			</div></div></div>

			<div class="layout-2-details layout-2-details-<?php the_ID(); ?>" style="display:none;">
				<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h6>
				<?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
				<div class="video_excerpt_text one"><h4 class="tittle-h4">Description</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = 100 , $message = 'No Description available' , $html = '<a data-modal-id="myModal-'.get_the_ID().'"> [...]</a>' ); ?></div></div>
				<ul class="stats">
					<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
					<li><?php videopress_countviews( get_the_ID() ); ?></li>
					<li><i class="fa fa-wrench"></i> <?php $result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
						echo count($result); ?> Endorsments</li>
					<!-- favourite section -->
					<li><i class="fa fa-heart"></i>
					<?php
					  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
					  $result1 = $wpdb->get_results($sql_aux);
					  echo count($result1);
					?> Favorites</li>
					<!-- end favorite -->
					<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
					<li><?php comments_number() ?></li>
				</ul>
				<div class="clear"></div>
				<p><?php echo videopress_content('240'); ?></p>

					<p><?php if($profile_id == get_current_user_id()){ ?>
					      <a href="#" class="del-video" data-id="<?= get_the_ID(); ?>"><i class="fa fa-trash-o"></i> Delete</a>
					    <?php } ?>
					      <a class="privacy-video" data-modal-id="myModalPrivacy-<?php the_ID(); ?>"><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;Privacy</a>
					</p>

			</div>

			<div class="clear"></div>
				   <?php
			 }
			 else
			 {    // Case where logged in User is not same as Video Author User
				  if(isset($privacyOption[0]) && $privacyOption[0] != "private")
				  {
					   ?>
						   <div class="grid-6 first">
								<div class="image-holder">
									<a href="<?php echo get_permalink(); ?>">
										<div class="hover-item"></div>
									</a>
									<?php
									if( has_post_thumbnail() ){
										the_post_thumbnail('small-thumb', array( 'class' => 'layout-2-thumb' ) );
									}else{
										echo '<img src="'.get_template_directory_uri().'/images/no-image.png" class="layout-2-thumb">';
									}
									?>
								</div>
								<div class="layout-title-box layout-title-box-<?php the_ID(); ?>" style="display:block;">
									<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php echo ctpreview( $content = get_the_title() , $length = 39 , $message = 'No Title available' , $html = ' [...]' ); ?></a></h6>
									<ul class="bottom-detailsul">
										<?php if($profile_id == get_current_user_id()){ ?>
										<li><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>"><i class="fa fa-trash-o"></i> Delete</a></li>
										<?php } ?>
										<li><a class="detailsblock-btn" data-modal-id="myModal-<?php the_ID(); ?>"><i class="fa fa-info-circle" aria-hidden="true"></i> Details</a></li>
									</ul>
								</div>
							</div>
							<script type="text/javascript">
$(function(){

var appendthis =  ("<div class='modal-overlayblockdiv'></div>");

	$('a[data-modal-id]').click(function(e) {
		e.preventDefault();
    $("body").append(appendthis);
    $(".modal-overlay").fadeTo(500, 0.7);
    //$(".js-modalbox").fadeIn(500);
		var modalBox = $(this).attr('data-modal-id');
		$('#'+modalBox).fadeIn($(this).data());
	});


$(".js-modal-close, .modal-overlay").click(function() {
    $(".modal-box, .modal-overlay").fadeOut(500, function() {
        $(".modal-overlay").remove();
    });

});

$(window).resize(function() {
    $(".modal-box").css({
        top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
        left: ($(window).width() - $(".modal-box").outerWidth()) / 2
    });
});

$(window).resize();

});
</script>

			<div class="modal-box" id="myModal-<?php the_ID(); ?>">
			<div class="modal-body">
			<a class="js-modal-close close">×</a>
			<div class="layout-2-details layout-2-details-<?php the_ID(); ?>">
				<div class="image-holder">
					<a href="<?php echo get_permalink(); ?>">
						<div class="hover-item"></div>
					</a>

					<?php
					if( has_post_thumbnail() ){
						the_post_thumbnail('small-thumb', array( 'class' => 'layout-2-thumb' ) );
					}else{
						echo '<img src="'.get_template_directory_uri().'/images/no-image.png" class="layout-2-thumb">';
					}
					?>

				</div>
				<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h6>
				<?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
				<div class="video_excerpt_text one"><h4 class="tittle-h4">Description 2</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = '' , $message = 'No Description available' , $html = ' [...]' );?></div></div>
				<div class="video_excerpt_text two"><h4 class="tittle-h4">Reference</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_post_meta(get_the_ID(),'video_options_refr',true) , $length = '' , $message = 'No referance given' , $html = ' [...]' );?></div></div>
				<ul class="stats">
					<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
					<li><?php videopress_countviews( get_the_ID() ); ?></li>
					<li><i class="fa fa-wrench"></i> <?php $result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
						echo count($result); ?> Endorsments</li>
					<!-- favourite section -->
					<li><i class="fa fa-heart"></i>
					<?php
					  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
					  $result1 = $wpdb->get_results($sql_aux);
					  echo count($result1);
					?> Favorites</li>
					<!-- end favorite -->
					<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
					<li><?php comments_number() ?></li>
				</ul>
				<div class="clear"></div>
				<p><?php echo videopress_content('240'); ?></p>

					<?php if($profile_id == get_current_user_id()){ ?>
					<p><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>"><i class="fa fa-trash-o"></i> Delete</a></p>
					<?php } ?>


			</div></div></div>

			<div class="layout-2-details layout-2-details-<?php the_ID(); ?>" style="display:none;">
				<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h6>
				<?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
				<div class="video_excerpt_text one"><h4 class="tittle-h4">Description</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = 100 , $message = 'No Description available' , $html = '<a data-modal-id="myModal-'.get_the_ID().'"> [...]</a>' ); ?></div></div>
				<ul class="stats">
					<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
					<li><?php videopress_countviews( get_the_ID() ); ?></li>
					<li><i class="fa fa-wrench"></i> <?php $result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
						echo count($result); ?> Endorsments</li>
					<!-- favourite section -->
					<li><i class="fa fa-heart"></i>
					<?php
					  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
					  $result1 = $wpdb->get_results($sql_aux);
					  echo count($result1);
					?> Favorites</li>
					<!-- end favorite -->
					<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
					<li><?php comments_number() ?></li>
				</ul>
				<div class="clear"></div>
				<p><?php echo videopress_content('240'); ?></p>

					<?php if($profile_id == get_current_user_id()){ ?>
					<p><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>"><i class="fa fa-trash-o"></i> Delete</a></p>
					<?php } ?>


			</div>

			<div class="clear"></div>
					   <?php
				  }
				  else
				  {
					  if(!isset($privacyOption[0]))
					  {
						    ?>
							   <div class="grid-6 first">
									<div class="image-holder">
										<a href="<?php echo get_permalink(); ?>">
											<div class="hover-item"></div>
										</a>
										<?php
										if( has_post_thumbnail() ){
											the_post_thumbnail('small-thumb', array( 'class' => 'layout-2-thumb' ) );
										}else{
											echo '<img src="'.get_template_directory_uri().'/images/no-image.png" class="layout-2-thumb">';
										}
										?>
									</div>
									<div class="layout-title-box layout-title-box-<?php the_ID(); ?>" style="display:block;">
										<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php echo ctpreview( $content = get_the_title() , $length = 39 , $message = 'No Title available' , $html = ' [...]' ); ?></a></h6>
										<ul class="bottom-detailsul">
											<?php if($profile_id == get_current_user_id()){ ?>
											<li><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>"><i class="fa fa-trash-o"></i> Delete</a></li>
											<?php } ?>
											<li><a class="detailsblock-btn" data-modal-id="myModal-<?php the_ID(); ?>"><i class="fa fa-info-circle" aria-hidden="true"></i> Details</a></li>
										</ul>
									</div>
								</div>
								<script type="text/javascript">
$(function(){

var appendthis =  ("<div class='modal-overlayblockdiv'></div>");

	$('a[data-modal-id]').click(function(e) {
		e.preventDefault();
    $("body").append(appendthis);
    $(".modal-overlay").fadeTo(500, 0.7);
    //$(".js-modalbox").fadeIn(500);
		var modalBox = $(this).attr('data-modal-id');
		$('#'+modalBox).fadeIn($(this).data());
	});


$(".js-modal-close, .modal-overlay").click(function() {
    $(".modal-box, .modal-overlay").fadeOut(500, function() {
        $(".modal-overlay").remove();
    });

});

$(window).resize(function() {
    $(".modal-box").css({
        top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
        left: ($(window).width() - $(".modal-box").outerWidth()) / 2
    });
});

$(window).resize();

});
</script>

			<div class="modal-box" id="myModal-<?php the_ID(); ?>">
			<div class="modal-body">
			<a class="js-modal-close close">×</a>
			<div class="layout-2-details layout-2-details-<?php the_ID(); ?>">
				<div class="image-holder">
					<a href="<?php echo get_permalink(); ?>">
						<div class="hover-item"></div>
					</a>

					<?php
					if( has_post_thumbnail() ){
						the_post_thumbnail('small-thumb', array( 'class' => 'layout-2-thumb' ) );
					}else{
						echo '<img src="'.get_template_directory_uri().'/images/no-image.png" class="layout-2-thumb">';
					}
					?>

				</div>
				<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h6>
				<?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
				<div class="video_excerpt_text one"><h4 class="tittle-h4">Description 2</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = '' , $message = 'No Description available' , $html = ' [...]' );?></div></div>
				<div class="video_excerpt_text two"><h4 class="tittle-h4">Reference</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_post_meta(get_the_ID(),'video_options_refr',true) , $length = '' , $message = 'No referance given' , $html = ' [...]' );?></div></div>
				<ul class="stats">
					<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
					<li><?php videopress_countviews( get_the_ID() ); ?></li>
					<li><i class="fa fa-wrench"></i> <?php $result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
						echo count($result); ?> Endorsments</li>
					<!-- favourite section -->
					<li><i class="fa fa-heart"></i>
					<?php
					  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
					  $result1 = $wpdb->get_results($sql_aux);
					  echo count($result1);
					?> Favorites</li>
					<!-- end favorite -->
					<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
					<li><?php comments_number() ?></li>
				</ul>
				<div class="clear"></div>
				<p><?php echo videopress_content('240'); ?></p>

					<?php if($profile_id == get_current_user_id()){ ?>
					<p><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>"><i class="fa fa-trash-o"></i> Delete</a></p>
					<?php } ?>


			</div></div></div>

			<div class="layout-2-details layout-2-details-<?php the_ID(); ?>" style="display:none;">
				<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h6>
				<?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
				<div class="video_excerpt_text one"><h4 class="tittle-h4">Description</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = 100 , $message = 'No Description available' , $html = '<a data-modal-id="myModal-'.get_the_ID().'"> [...]</a>' ); ?></div></div>
				<ul class="stats">
					<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
					<li><?php videopress_countviews( get_the_ID() ); ?></li>
					<li><i class="fa fa-wrench"></i> <?php $result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
						echo count($result); ?> Endorsments</li>
					<!-- favourite section -->
					<li><i class="fa fa-heart"></i>
					<?php
					  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
					  $result1 = $wpdb->get_results($sql_aux);
					  echo count($result1);
					?> Favorites</li>
					<!-- end favorite -->
					<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
					<li><?php comments_number() ?></li>
				</ul>
				<div class="clear"></div>
				<p><?php echo videopress_content('240'); ?></p>

					<?php if($profile_id == get_current_user_id()){ ?>
					<p><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>"><i class="fa fa-trash-o"></i> Delete</a></p>
					<?php } ?>


			</div>

			<div class="clear"></div>
						   <?php
					  }
                      else
					  {
						    if( is_array($selectUsersList) and count($selectUsersList) > 0 )
							   {   // case where user access list is available
									 if( in_array($user_ID, $selectUsersList) )
									 {
										   ?>
											   <div class="grid-6 first">
													<div class="image-holder">
														<a href="<?php echo get_permalink(); ?>">
															<div class="hover-item"></div>
														</a>
														<?php
														if( has_post_thumbnail() ){
															the_post_thumbnail('small-thumb', array( 'class' => 'layout-2-thumb' ) );
														}else{
															echo '<img src="'.get_template_directory_uri().'/images/no-image.png" class="layout-2-thumb">';
														}
														?>
													</div>
													<div class="layout-title-box layout-title-box-<?php the_ID(); ?>" style="display:block;">
														<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php echo ctpreview( $content = get_the_title() , $length = 39 , $message = 'No Title available' , $html = ' [...]' ); ?></a></h6>
														<ul class="bottom-detailsul">
															<?php if($profile_id == get_current_user_id()){ ?>
															<li><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>"><i class="fa fa-trash-o"></i> Delete</a></li>
															<?php } ?>
															<li><a class="detailsblock-btn" data-modal-id="myModal-<?php the_ID(); ?>"><i class="fa fa-info-circle" aria-hidden="true"></i> Details</a></li>
														</ul>
													</div>
												</div>
												<script type="text/javascript">
$(function(){

var appendthis =  ("<div class='modal-overlayblockdiv'></div>");

	$('a[data-modal-id]').click(function(e) {
		e.preventDefault();
    $("body").append(appendthis);
    $(".modal-overlay").fadeTo(500, 0.7);
    //$(".js-modalbox").fadeIn(500);
		var modalBox = $(this).attr('data-modal-id');
		$('#'+modalBox).fadeIn($(this).data());
	});


$(".js-modal-close, .modal-overlay").click(function() {
    $(".modal-box, .modal-overlay").fadeOut(500, function() {
        $(".modal-overlay").remove();
    });

});

$(window).resize(function() {
    $(".modal-box").css({
        top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
        left: ($(window).width() - $(".modal-box").outerWidth()) / 2
    });
});

$(window).resize();

});
</script>

			<div class="modal-box" id="myModal-<?php the_ID(); ?>">
			<div class="modal-body">
			<a class="js-modal-close close">×</a>
			<div class="layout-2-details layout-2-details-<?php the_ID(); ?>">
				<div class="image-holder">
					<a href="<?php echo get_permalink(); ?>">
						<div class="hover-item"></div>
					</a>

					<?php
					if( has_post_thumbnail() ){
						the_post_thumbnail('small-thumb', array( 'class' => 'layout-2-thumb' ) );
					}else{
						echo '<img src="'.get_template_directory_uri().'/images/no-image.png" class="layout-2-thumb">';
					}
					?>

				</div>
				<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h6>
				<?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
				<div class="video_excerpt_text one"><h4 class="tittle-h4">Description 2</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = '' , $message = 'No Description available' , $html = ' [...]' );?></div></div>
				<div class="video_excerpt_text two"><h4 class="tittle-h4">Reference</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_post_meta(get_the_ID(),'video_options_refr',true) , $length = '' , $message = 'No referance given' , $html = ' [...]' );?></div></div>
				<ul class="stats">
					<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
					<li><?php videopress_countviews( get_the_ID() ); ?></li>
					<li><i class="fa fa-wrench"></i> <?php $result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
						echo count($result); ?> Endorsments</li>
					<!-- favourite section -->
					<li><i class="fa fa-heart"></i>
					<?php
					  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
					  $result1 = $wpdb->get_results($sql_aux);
					  echo count($result1);
					?> Favorites</li>
					<!-- end favorite -->
					<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
					<li><?php comments_number() ?></li>
				</ul>
				<div class="clear"></div>
				<p><?php echo videopress_content('240'); ?></p>

					<?php if($profile_id == get_current_user_id()){ ?>
					<p><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>"><i class="fa fa-trash-o"></i> Delete</a></p>
					<?php } ?>


			</div></div></div>

			<div class="layout-2-details layout-2-details-<?php the_ID(); ?>" style="display:none;">
				<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h6>
				<?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
				<div class="video_excerpt_text one"><h4 class="tittle-h4">Description</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = 100 , $message = 'No Description available' , $html = '<a data-modal-id="myModal-'.get_the_ID().'"> [...]</a>' ); ?></div></div>
				<ul class="stats">
					<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
					<li><?php videopress_countviews( get_the_ID() ); ?></li>
					<li><i class="fa fa-wrench"></i> <?php $result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
						echo count($result); ?> Endorsments</li>
					<!-- favourite section -->
					<li><i class="fa fa-heart"></i>
					<?php
					  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
					  $result1 = $wpdb->get_results($sql_aux);
					  echo count($result1);
					?> Favorites</li>
					<!-- end favorite -->
					<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
					<li><?php comments_number() ?></li>
				</ul>
				<div class="clear"></div>
				<p><?php echo videopress_content('240'); ?></p>

					<?php if($profile_id == get_current_user_id()){ ?>
					<p><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>"><i class="fa fa-trash-o"></i> Delete</a></p>
					<?php } ?>


			</div>

			<div class="clear"></div>
										   <?php
									 }
									 else
									 {
									 }
							   }
							   else
							   {  }
					  }
				  }
			 }
		}

     if(!is_user_logged_in())
		{  // case where user is not logged in
	             if(isset($privacyOption[0]) && ($privacyOption[0] != "private"))
				  {
					     ?>
						     </div>
							 </div>
						 <?php
				  }
				  else
				  {
					    if(!isset($privacyOption[0]))
						{
							 ?>
								 </div>
								 </div>
							 <?php
						}
				  }
		}
		else
		{  // case where user is logged in
			 if($post_author == $user_ID)
			 {    // Case where logged in User is same as Video Author User
				   ?>
					   </div>
					   </div>
				   <?php
			 }
			 else
			 {    // Case where logged in User is not same as Video Author User
				  if(isset($privacyOption[0]) && $privacyOption[0] != "private")
				  {
					     ?>
						     </div>
							 </div>
						 <?php
				  }
				  else
				  {
					  if(!isset($privacyOption[0]))
					  {
						     ?>
								 </div>
								 </div>
							 <?php
					  }
                      else
					  {
						    if( is_array($selectUsersList) and count($selectUsersList) > 0 )
							   {   // case where user access list is available
									 if( in_array($user_ID, $selectUsersList) )
									 {
										     ?>
												 </div>
												 </div>
											 <?php
									 }
									 else
									 {
									 }
							   }
							   else
							   {  }
					  }
				  }
			 }
		}
?>
	<!-- End Layout Wrapper -->

<?php
/* ================================================================== */
/* End of Loop */
/* ================================================================== */
}// end for each
echo '</div>';
/* ================================================================== */
/* Else if Nothing Found */
/* ================================================================== */
}else {
	?>

<h6>NO VIDEOS FOUND!</h6>
<p>The user has not yet uploaded any videos yet.</p>

<?php
/* ================================================================== */
/* End If */
/* ================================================================== */
}

elseif($endorsements || $favourites):
	if($pageposts):
		global $post;
echo '<div id="products" class="list-group">';
	foreach($pageposts as $post):
		setup_postdata($post);?>

	<!-- Start Layout Wrapper -->
<div class="item col-xs-6 col-lg-6 post-<?php the_ID(); ?>">
    <div class="thumbnail layout-2-wrapper solid-bg">

			<div class="grid-6 first">
				<div class="image-holder">
					<a href="<?php echo get_permalink(); ?>">
						<div class="hover-item"></div>
					</a>

					<?php
					if( has_post_thumbnail() ){
						the_post_thumbnail('small-thumb', array( 'class' => 'layout-2-thumb' ) );
					}else{
						echo '<img src="'.get_template_directory_uri().'/images/no-image.png" class="layout-2-thumb">';
					}
					?>

				</div>
				<div class="layout-title-box layout-title-box-<?php the_ID(); ?>" style="display:block;">
					<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php echo ctpreview( $content = get_the_title() , $length = 39 , $message = 'No Title available' , $html = ' [...]' ); ?></a></h6>
					<ul class="bottom-detailsul">
						<?php if($profile_id == get_current_user_id()){ ?>
						<li><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>"><i class="fa fa-trash-o"></i> Delete</a></li>
						<?php } ?>
						<li><a class="detailsblock-btn" data-modal-id="myModal-<?php the_ID(); ?>"><i class="fa fa-info-circle" aria-hidden="true"></i> Details</a></li>
					</ul>
				</div>
			</div>

<script type="text/javascript">
$(function(){

var appendthis =  ("<div class='modal-overlayblockdiv'></div>");

	$('a[data-modal-id]').click(function(e) {
		e.preventDefault();
    $("body").append(appendthis);
    $(".modal-overlay").fadeTo(500, 0.7);
    //$(".js-modalbox").fadeIn(500);
		var modalBox = $(this).attr('data-modal-id');
		$('#'+modalBox).fadeIn($(this).data());
	});


$(".js-modal-close, .modal-overlay").click(function() {
    $(".modal-box, .modal-overlay").fadeOut(500, function() {
        $(".modal-overlay").remove();
    });

});

$(window).resize(function() {
    $(".modal-box").css({
        top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
        left: ($(window).width() - $(".modal-box").outerWidth()) / 2
    });
});

$(window).resize();

});
</script>

			<div class="modal-box" id="myModal-<?php the_ID(); ?>">
			<div class="modal-body">
			<a class="js-modal-close close">×</a>
			<div class="layout-2-details layout-2-details-<?php the_ID(); ?>">
				<div class="image-holder">
					<a href="<?php echo get_permalink(); ?>">
						<div class="hover-item"></div>
					</a>

					<?php
					if( has_post_thumbnail() ){
						the_post_thumbnail('small-thumb', array( 'class' => 'layout-2-thumb' ) );
					}else{
						echo '<img src="'.get_template_directory_uri().'/images/no-image.png" class="layout-2-thumb">';
					}
					?>

				</div>
				<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h6>
				<?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
				<div class="video_excerpt_text one"><h4 class="tittle-h4">Description</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = '' , $message = 'No Description available' , $html = ' [...]' );?></div></div>
				<div class="video_excerpt_text two"><h4 class="tittle-h4">Reference</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_post_meta(get_the_ID(),'video_options_refr',true) , $length = '' , $message = 'No referance given' , $html = ' [...]' );?></div></div>
				<ul class="stats">
					<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
					<li><?php videopress_countviews( get_the_ID() ); ?></li>
					<li><i class="fa fa-wrench"></i> <?php $result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
						echo count($result); ?> Endorsments</li>
					<!--favourite section -->
					<li><i class="fa fa-heart"></i>
					<?php
					  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
					  $result1 = $wpdb->get_results($sql_aux);
					  echo count($result1);
					?> Favorites</li>
					<!-- end favorite -->
					<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
					<li><?php comments_number() ?></li>
				</ul>
				<div class="clear"></div>
				<p><?php echo videopress_content('120'); ?></p>
				<?php if($profile_id == get_current_user_id()){ ?>
				<p><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>"><i class="fa fa-trash-o"></i> Delete</a></p>
				<?php } ?>
			</div></div></div>

			<div class="layout-2-details layout-2-details-<?php the_ID(); ?>" style="display:none;">
				<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h6>
				<?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
				<div class="video_excerpt_text one"><h4 class="tittle-h4">Description</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = 100 , $message = 'No Description available' , $html = '<a data-modal-id="myModal-'.get_the_ID().'"> [...]</a>' ); ?></div></div>
				<ul class="stats">
					<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
					<li><?php videopress_countviews( get_the_ID() ); ?></li>
					<li><i class="fa fa-wrench"></i> <?php $result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
						echo count($result); ?> Endorsments</li>
					<!--favourite section -->
					<li><i class="fa fa-heart"></i>
					<?php
					  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
					  $result1 = $wpdb->get_results($sql_aux);
					  echo count($result1);
					?> Favorites</li>
					<!-- end favorite -->
					<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
					<li><?php comments_number() ?></li>
				</ul>
				<div class="clear"></div>
				<p><?php echo videopress_content('120'); ?></p>
				<?php if($profile_id == get_current_user_id()){ ?>
				<p><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>"><i class="fa fa-trash-o"></i> Delete</a></p>
				<?php } ?>
			</div>

	<div class="clear"></div>
</div>
</div>
<!-- End Layout Wrapper -->

<?php endforeach;
	echo '</div>';
else:
	echo '<h3 class="text-center">Nothing Found Here !</h3>';
endif;
else:

echo '<div id="products" class="list-group">';
	while ( $my_query->have_posts() ) : $my_query->the_post(); ?>

<!-- Start Layout Wrapper -->
<div class="item  col-xs-6 col-lg-6 post-<?php the_ID(); ?>">
    <div class="thumbnail layout-2-wrapper solid-bg">

		<div class="grid-6 first">
			<div class="image-holder">
				<a href="<?php echo get_permalink(); ?>">
					<div class="hover-item"></div>
				</a>

				<?php
				if( has_post_thumbnail() ){
					the_post_thumbnail('small-thumb', array( 'class' => 'layout-2-thumb' ) );
				}else{
					echo '<img src="'.get_template_directory_uri().'/images/no-image.png" class="layout-2-thumb">';
				}
				?>

			</div>
			<div class="layout-title-box layout-title-box-<?php the_ID(); ?>" style="display:block;">
					<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php echo ctpreview( $content = get_the_title() , $length = 39 , $message = 'No Title available' , $html = ' [...]' ); ?></a></h6>
					<ul class="bottom-detailsul">
						<?php if($profile_id == get_current_user_id()){ ?>
						<li><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>"><i class="fa fa-trash-o"></i> Delete</a></li>
						<?php } ?>
						<li><a class="detailsblock-btn" data-modal-id="myModal-<?php the_ID(); ?>"><i class="fa fa-info-circle" aria-hidden="true"></i> Details</a></li>
					</ul>
				</div>
			</div>

<script type="text/javascript">
$(function(){

var appendthis =  ("<div class='modal-overlayblockdiv'></div>");

	$('a[data-modal-id]').click(function(e) {
		e.preventDefault();
    $("body").append(appendthis);
    $(".modal-overlay").fadeTo(500, 0.7);
    //$(".js-modalbox").fadeIn(500);
		var modalBox = $(this).attr('data-modal-id');
		$('#'+modalBox).fadeIn($(this).data());
	});


$(".js-modal-close, .modal-overlay").click(function() {
    $(".modal-box, .modal-overlay").fadeOut(500, function() {
        $(".modal-overlay").remove();
    });

});

$(window).resize(function() {
    $(".modal-box").css({
        top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
        left: ($(window).width() - $(".modal-box").outerWidth()) / 2
    });
});

$(window).resize();

});
</script>

			<div class="modal-box" id="myModal-<?php the_ID(); ?>">
			<div class="modal-body">
			<a class="js-modal-close close">×</a>
			<div class="layout-2-details layout-2-details-<?php the_ID(); ?>">
			<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h6>
			<?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
			<div class="video_excerpt_text one"><h4 class="tittle-h4">Description</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = '' , $message = 'No Description available' , $html = ' [...]' );?></div></div>
			<div class="video_excerpt_text two"><h4 class="tittle-h4">Reference</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_post_meta(get_the_ID(),'video_options_refr',true) , $length = '' , $message = 'No referance given' , $html = ' [...]' );?></div></div>
			<ul class="stats">
				<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
				<li><?php videopress_countviews( get_the_ID() ); ?></li>
				<li><i class="fa fa-wrench"></i> <?php $result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
					echo count($result); ?> Endorsments</li>
				<!--favourite section -->
				<li><i class="fa fa-heart"></i>
				<?php
				  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
				  $result1 = $wpdb->get_results($sql_aux);
				  echo count($result1);
				?> Favorites</li>
				<!-- end favorite -->
				<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
				<li><?php comments_number(); ?></li>
			</ul>
			<div class="clear"></div>
			<p><?php echo videopress_content('240'); ?></p>
			<?php if($profile_id == get_current_user_id()){ ?>
			<p><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>" ><i class="fa fa-trash-o"></i> Delete</a></p>
			<?php } ?>
		</div></div></div>

		<div class="layout-2-details layout-2-details-<?php the_ID(); ?>" style="display:none;">
			<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h6>
			<?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
			<div class="video_excerpt_text one"><h4 class="tittle-h4">Description</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = 100 , $message = 'No Description available' , $html = '<a data-modal-id="myModal-'.get_the_ID().'"> [...]</a>' ); ?></div></div>
			<ul class="stats">
				<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
				<li><?php videopress_countviews( get_the_ID() ); ?></li>
				<li><i class="fa fa-wrench"></i> <?php $result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
					echo count($result); ?> Endorsments</li>
				<!--favourite section -->
				<li><i class="fa fa-heart"></i>
				<?php
				  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
				  $result1 = $wpdb->get_results($sql_aux);
				  echo count($result1);
				?> Favorites</li>
				<!-- end favorite -->
				<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
				<li><?php comments_number(); ?></li>
			</ul>
			<div class="clear"></div>
			<p><?php echo videopress_content('240'); ?></p>
			<?php if($profile_id == get_current_user_id()){ ?>
			<p><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>" ><i class="fa fa-trash-o"></i> Delete</a></p>
			<?php } ?>
		</div>

		<div class="clear"></div>
	</div>
</div>
<!-- End Layout Wrapper -->
<?php
endwhile;
echo "</div>";
endif;
?>

<?php /*if( vp_option('vpt_option.pagination') == '1' ){
		videopress_pagination(); // Use Custom Pagination
	}else{
		echo '<div class="post_pagination vdfgdfgddf">';
		posts_nav_link();
		echo'</div>';
	}*/
 if($cat=='video'){
         $pcurrent=max( 1, get_query_var('page') );
        }
       else{
         $pcurrent=1;
        }

     $paginate = paginate_links( array(
	'format' => '?page/%#%/',
	'current' => $pcurrent,
	'type'=>'array',
	'end_size'=>1,
	'mid_size'=>4,
	'total' => ceil($total / 12)
           ) );

$pcount=count($paginate);


 $maxp=ceil(($total/12)/2);

if( $pcurrent<= ceil($total / 12)  )
{
 if( ceil($total / 12) <5 ) {
 foreach($paginate as $plnk){echo $plnk.'&nbsp;';}
 }

else if( ceil($total / 12) >= 5 ){


 if ($pcurrent<=6)
 {
 	if ($pcurrent>2)
 	{echo $paginate[0].'&nbsp;';}
       if($paginate[$pcurrent-2]){
 	echo $paginate[$pcurrent-2].'&nbsp;';
      }
 	echo $paginate[$pcurrent-1].'&nbsp;';
 	echo $paginate[$pcurrent].'&nbsp;';
if( isset($paginate[$pcurrent+1]) && (($pcount-1) > $pcurrent+1) ){echo $paginate[$pcurrent+1].'&nbsp;';}
if( isset($paginate[$pcurrent+2]) && (($pcount-1) > $pcurrent+2) ){echo $paginate[$pcurrent+2].'&nbsp;';}

 }

else if ($pcurrent>=7)
 {   echo $paginate[0].'&nbsp;';
 	echo $paginate[5].'&nbsp;';
 	echo $paginate[6].'&nbsp;';
 	echo $paginate[7].'&nbsp;';
if( isset($paginate[8]) && (($pcount-1) > 8) ){echo $paginate[8].'&nbsp;';}
if( isset($paginate[9]) && (($pcount-1) > 9) ){echo $paginate[9].'&nbsp;';}

 }


if( $pcurrent <= (ceil($total/12)-1) ){
echo $paginate[$pcount-1];
  }

 }

}

?>
	<!-- End Pagination -->
</div>
<!-- End Uploaded Videos by user -->

<?php // PLAYLIST TAB CONTENT // ?>
<div role="tabpanel" class="tab-pane" id="playlists">
	<div>
		Created Playlists
		<?php if(get_current_user_id() == $profile_id){?>
		<span class="pull-right" id="playlist_create">
			<button class="btn btn-primary">Create New
				<i class="fa fa-plus"></i>
			</button>
		</span>
		<?php } ?>
	</div>
	<div class="clear"></div>
	<br/>



	<div class="playlist-inner" style="margin: 0 auto">
		<?php
		if(is_user_logged_in()){
			if(isset($_POST['submit']) && !empty($_POST['playlist_title']) && !empty($_POST['playlist_type']) &&  $_POST['playlist_type']!=0 ){
				global $wpdb;
				$table = $wpdb->prefix."user_playlists";
				$insert = $wpdb->insert($table,array(
					'user_id'	=> get_current_user_id(),
					'title'		=> $_POST['playlist_title'],
					'type'		=> $_POST['playlist_type'],
					'created'	=> date('Y-m-d h:i:s'),
									//'updated'	=> time(),
					));
					if($insert){ ?>
					<script> window.location.href = location.href; </script>
					<?php  }
				}
			}
			?>

			<form class="form-inline" id="playlist-form" method="post" style="display:none;">
			     <div class="form-group">
					<label for="playlist_type">Type</label>
					<select name="playlist_type" required>
					  <option value="0">Select Any One</option>
					  <option value="1">Videos</option>
					  <option value="2">Files</option>
					  <option value="3">Books</option>
					</select>

				</div>
				<div class="form-group">
					<label for="playlist_title">Title</label>
					<input type="text" name="playlist_title" class="playlist_title" class="form-control" id="playlist_title" placeholder="title" required>
				</div>

				<button type="submit" name="submit" class="btn btn-success" id="palylistform-submit">Submit</button>
				<button type="button" class="btn btn-primary" id="palylistform-cancel">Cancel</button>
			</form>
			<!-- Video playlist -->
			<div class="row" style="margin: 0 auto">
			<div class="cover-twoblocks">
				<div class="sortvideo-div">
					<div class="dyn-book-sort">
						<label>Playlist Videos</label>
					</div>
				</div>
				<!--<div class="well well-sm">
					<strong>Views</strong>
					<div class="btn-group">
						<a href="#" id="list" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th-list">
						</span>List</a> <a href="#" id="grid" class="btn btn-default btn-sm"><span
							class="glyphicon glyphicon-th"></span>Grid</a>
					</div>
				</div>-->
			</div>
			<?php
			$data = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."user_playlists WHERE user_id = $profile_id AND type = 1  ORDER BY id DESC");
			?>
            <?php  if ( wp_is_mobile() ) {
                    /* Display and echo mobile specific stuff here */
					  $mobi = "auto";
                 }
				 else{
					 $mobi = "23.794%";
				 }

				 ?>
			<?php if($data): ?>
				<div class="playlistdivs">

					<?php foreach($data as $single_data): ?>
						<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2" id="lay-<?= $single_data->id;?>" >
							<div class="thumbnail layout-2-wrapper solid-bg" style="height:100%; width:100% !important; box-shadow: 0 0 10px 1px #666; border-radius:5px; padding:5px">

								<?php
								$data = $wpdb->get_results("SELECT song_id, count(*) as c FROM ".$wpdb->prefix."playlist_songs WHERE playlist_id = ".$single_data->id);

								?>

								 <?php
					           if( has_post_thumbnail($data[0]->song_id) ){ ?>
							   <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($data[0]->song_id) ); ?>" class="playlistview" width="100%" >

							   <?php

					            } else { ?>
								<img src="<?php echo get_template_directory_uri()?>/images/no-image.png" class="playlistview" width="100%" >

								<?php

					              } ?>


								<p><?php echo $data[0]->c;?> videos
									<?php if($profile_id == get_current_user_id()){ ?>
									<span class="pull-right">
										<a href="#" title="Delete Playlist" data-id="<?= $single_data->id;?>" class="button_p_d"><i class="fa fa-trash-o"></i></a>
									</span>
									<?php } ?>
								</p>
								<p>
								<?php
								$ts = ($data[0]->c)?$single_data->id:false;
								$t = str_replace("=",'',base64_encode($ts));
								//echo base64_decode($t);
								?>
									<a href="<?php echo get_site_url(); ?>/playlist?opt=<?php echo $t; ?>&ih=<?php echo $data[0]->song_id; ?>&ptype=videos" class="playlist-url" data-playlist="<?= ($data[0]->c)?$single_data->id:false;?>" >
										<?php echo $single_data->title; ?>
									</a>
								</p>
								<p>updated : <?php echo dateDiff($single_data->updated); ?></p>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			<?php else: ?>
				<!-- No Playlist -->
				<div class="alert alert-warning">
					<!--<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>-->
					<strong>Warning!</strong> Nothing found here.
				</div>
			<?php endif; ?>
			</div>
			<!-- End playlist -->
			<br>
			<br>
			<!-- File playlist -->
			<div class="row" style="margin: 0 auto">
			<div class="cover-twoblocks">
				<div class="sortvideo-div">
					<div class="dyn-book-sort">
						<label>Playlist Files</label>
					</div>
				</div>
				<!--<div class="well well-sm">
					<strong>Views</strong>
					<div class="btn-group">
						<a href="#" id="list" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th-list">
						</span>List</a> <a href="#" id="grid" class="btn btn-default btn-sm"><span
							class="glyphicon glyphicon-th"></span>Grid</a>
					</div>
				</div>-->
			</div>
			<?php
			$datafile = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."user_playlists WHERE user_id = $profile_id AND type = 2  ORDER BY id DESC");
			?>

			<?php if($datafile): ?>
				<div class="playlistdivs">

					<?php foreach($datafile as $single_data_file): ?>
						<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2" id="lay-<?= $single_data_file->id;?>" >
							<div class="thumbnail layout-2-wrapper solid-bg" style="height:100%; width:100% !important; box-shadow: 0 0 10px 1px #666; border-radius:5px; padding:5px">

								<?php
								$data_file = $wpdb->get_results("SELECT song_id, count(*) as c FROM ".$wpdb->prefix."playlist_songs WHERE playlist_id = ".$single_data_file->id);

								?>



								<?php $output = ''; ?>
				                <?php echo apply_filters( 'dyn_file_image', $output, $data_file[0]->song_id ); ?>

								<p><?php echo $data_file[0]->c;?> files
									<?php if($profile_id == get_current_user_id()){ ?>
									<span class="pull-right">
										<a href="#" title="Delete Playlist" data-id="<?= $single_data_file->id;?>" class="button_p_d"><i class="fa fa-trash-o"></i></a>
									</span>
									<?php } ?>
								</p>
								<p>
								<?php
								$ts = ($data_file[0]->c)?$single_data_file->id:false;
								$t = str_replace("=",'',base64_encode($ts));
								//echo base64_decode($t);
								?>
									<a href="<?php echo get_site_url(); ?>/playlist?opt=<?php echo $t; ?>&ih=<?php echo $data_file[0]->song_id; ?>&ptype=files" class="playlist-url" data-playlist="<?= ($data_file[0]->c)?$single_data_file->id:false;?>" >
										<?php echo $single_data_file->title; ?>
									</a>
								</p>
								<p>updated : <?php echo dateDiff($single_data_file->updated); ?></p>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			<?php else: ?>
				<!-- No Playlist -->
				<div class="alert alert-warning">
					<!--<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>-->
					<strong>Warning!</strong> Nothing found here.
				</div>
			<?php endif; ?>
			</div>
			<!-- End File playlist -->
			<br>
			<br>
			<!-- Book playlist -->
			<div class="row" style="margin: 0 auto">
			<div class="cover-twoblocks">
				<div class="sortvideo-div">
					<div class="dyn-book-sort">
						<label>Playlist Books</label>
					</div>
				</div>
				<!--<div class="well well-sm">
					<strong>Views</strong>
					<div class="btn-group">
						<a href="#" id="list" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th-list">
						</span>List</a> <a href="#" id="grid" class="btn btn-default btn-sm"><span
							class="glyphicon glyphicon-th"></span>Grid</a>
					</div>
				</div>-->
			</div>
			<?php
			$databook = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."user_playlists WHERE user_id = $profile_id AND type = 3  ORDER BY id DESC");
			?>

			<?php if($databook): ?>
				<div class="playlistdivs">

					<?php foreach($databook as $single_data_book): ?>
						<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 col-xl-2" id="lay-<?= $single_data_book->id;?>" >
							<div class="thumbnail layout-2-wrapper solid-bg" style="height:100%; width:100% !important; box-shadow: 0 0 10px 1px #666; border-radius:5px; padding:5px">

								<?php
								$data_book = $wpdb->get_results("SELECT song_id, count(*) as c FROM ".$wpdb->prefix."playlist_songs WHERE playlist_id = ".$single_data_book->id);

								?>

								 <?php
					           if( has_post_thumbnail($data_book[0]->song_id) ){ ?>
							   <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($data_book[0]->song_id) ); ?>" class="playlistview" width="100%" >

							   <?php

					            } else { ?>
								<img src="<?php echo get_template_directory_uri()?>/images/no-image.png" class="playlistview" width="100%" >

								<?php

					              } ?>

								<p><?php echo $data_book[0]->c;?> books
									<?php if($profile_id == get_current_user_id()){ ?>
									<span class="pull-right">
										<a href="#" title="Delete Playlist" data-id="<?= $single_data_book->id;?>" class="button_p_d"><i class="fa fa-trash-o"></i></a>
									</span>
									<?php } ?>
								</p>
								<p>
								<?php
								$ts = ($data_book[0]->c)?$single_data_book->id:false;
								$t = str_replace("=",'',base64_encode($ts));
								//echo base64_decode($t);
								?>
									<a href="<?php echo get_site_url(); ?>/playlist?opt=<?php echo $t; ?>&ih=<?php echo $data_book[0]->song_id; ?>&ptype=books" class="playlist-url" data-playlist="<?= ($data_book[0]->c)?$single_data_book->id:false;?>" >
										<?php echo $single_data_book->title; ?>
									</a>
								</p>
								<p>updated : <?php echo dateDiff($single_data_book->updated); ?></p>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			<?php else: ?>
				<!-- No Playlist -->
				<div class="alert alert-warning">
					<!--<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>-->
					<strong>Warning!</strong> Nothing found here.
				</div>
			<?php endif; ?>
			</div>
			<!-- End Book playlist -->


		</div>




		<script>
			jQuery(document).ready(function(){
				var $ = jQuery.noConflict();

				jQuery('#playlist_create').click(function(e){
					e.preventDefault();
					jQuery(this).hide();
					jQuery('#playlist-form').show();
				} )

				jQuery('#palylistform-cancel').click(function(e){
					e.preventDefault();
					jQuery('.playlist_title').val('');
					jQuery('#playlist-form').hide();
					jQuery('#playlist_create').show();
				} )

				jQuery('.playlist-url').click(
					function(e){
						e.preventDefault();
						var playlist_id = $(this).attr('data-playlist');
						if(playlist_id){
							var hrefs =  $(this).attr('href');
							jQuery.ajax({
								type: "POST",
								url: "<?php echo admin_url('admin-ajax.php'); ?>",
								data: { action:'session_id_playlist',playlist_id: playlist_id,},
								success : function(data){
								//localStorage["playlist_id"] = playlist_id;
								window.location.href = hrefs;
							}
						});
						}
					}
					)

			})
		</script>

		<div class="clear"></div>
	</div>

	<?php // DISCUSSION TAB // ?>
	<div role="tabpanel" class="tab-pane" id="discussion">
		<?php if(function_exists("display_saic")) { echo display_saic();} ?>
	</div>
	<?php // END DISCUSSION TAB // ?>

	<?php // DONATE TAB // ?>
	<div role="tabpanel" class="tab-pane" id="donate">
		<?php if($author->ID != get_current_user_id() && $paypalacc!='') { ?>
		<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
			<input type="hidden" name="cmd" value="_xclick">
			<input type="hidden" name="business" value="<?php echo $paypalacc ?>">
			<input type="hidden" name="address_ override" value="<?php echo $paypalacc ?>">
			<input type="hidden" name="lc" value="US">
			<input type="hidden" name="item_name" value="Do It Yourself Nation">
			<input type="hidden" name="item_number" value="<?php echo get_the_author_meta('nickname', $author->ID ); ?>">
			<input type="hidden" name="currency_code" value="USD">
			<input type="hidden" name="button_subtype" value="services">
			<input type="hidden" name="bn" value="PP-BuyNowBF:btn_paynowCC_LG.gif:NonHosted">
			<div>
				<br/>Amount<br/><input type="text" name="amount" value="">
				<br/><input type="hidden" name="on0" value="Email">Your Paypal email<br/><input type="text" name="os0" maxlength="300" size="40">
				<br/><input type="hidden" name="on1" value="Message">Message to this author<br/><input type="text" name="os1" maxlength="300" size="40">
			</div>
			<!--<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_paynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">-->
			<button type="submit" class="donate-button">DONATE WITH PAYPAL</button>
			<!--<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">-->
		</form>
		<?php } else { ?>
		<?php if($author->ID== get_current_user_id()) { ?>
		<form action="#" method="post" target="_top">
			<input type="hidden" name="operation" value="addpaypalaccount">
			<input type="hidden" name="user_id" value="<?php echo $author->ID; ?>">
			<input type="text"   name="paypal_email" value="<?php echo $paypalacc; ?>" maxlength="300" size="40">
			<?php 
				global $wpdb;
				$table_name = $wpdb->prefix . "users_merchant_accounts";
				$wpdb->get_row("SELECT paypal_email from $table_name where user_id='$author->ID'");
				$rows = $wpdb->num_rows;
				if($rows>0)$checked="checked";
				else $checked=" ";
			?>
			<div class="checkbox">
			  <label><input type="checkbox" name="donate" <?php echo $checked;?> value="">Donate</label>
			</div>
			<?php 
				$wpdb->get_row("SELECT revenue_status from ".$wpdb->prefix . "users_revenue_status where user_id='$author->ID' and revenue_status=1");
				//$rows = $wpdb->num_rows;
				if($wpdb->num_rows>0)$checkedrev = "checked";				
				else $checkedrev=" ";
				
			?>
			<div class="checkbox">
			  <label><input type="checkbox" id="rev-share" name="rev-share" <?php echo $checkedrev;?> value="">Revnue Share</label>
			</div>
			<input type="submit" value="Save Paypal Account" name="submit" class="donate-button"/>
		</form>
		<?php } ?>
		<?php } ?>
	</div>

	<?php // END DONATE TAB // ?>

	<?php if($author->ID== get_current_user_id()) { ?>

	<?php // livestreamig channel configuration(videowhisper) TAB // ?>
	<?php // END livestreamig channel configuration(videowhisper) TAB // ?>
	<!--Start Pagination -->

	<?php // My suscriptions TAB // ?>
	<div role="tabpanel" class="tab-pane" id="suscriptioninf">
		<?php
        //show the list of notfied live streaming sessions from this user
		$user_ID = get_current_user_id();
		if(isset($_SESSION["timezone_profile"])){
			$timezone = $_SESSION["timezone_profile"];
			$date = new DateTime('now', new DateTimeZone($timezone));
			$localtime = $date->format('Y-m-d H:i:s');
		}

		//get all the authors where this user is suscribed
		$sql_data_gen="SELECT * FROM ".$wpdb->prefix."authors_suscribers WHERE suscriber_id =".$user_ID;
		$data_gen = $wpdb->get_results($sql_data_gen);
		$newrow=0;

		$suscription_list='<div style="display: table;width: 100%;table-layout: fixed;border-spacing: 10px;">';
		if($data_gen){
			$lines=0;
			foreach($data_gen as $single_data_gen){
				if($newrow==0) $suscription_list .='<div style="display: table;width: 100%;table-layout: fixed;border-spacing: 10px;">';
				$newrow=-1;
				$meta_key = 'profile_pic_meta';
				$fileName = get_user_meta($single_data_gen->author_id, $meta_key, true );
				$userdata = get_userdata($single_data_gen->author_id);
				$linkprof = site_url().'/user/'.$userdata->user_nicename;

	         //for evauate name length
				if(strlen($userdata->display_name)>25) $dsplayna = substr($userdata->display_name,0,22).'...';
				else $dsplayna = $userdata->display_name;
				if($fileName != ''){
					$fileNameImg = site_url().'/profile_pic/'.$fileName;
				}else{
					$fileNameImg = get_stylesheet_directory_uri().'/images/no_avatar.jpg';
				}


				$suscription_list .='<div style="display:table-cell;" class="live-stream-block" ><a href="'.$linkprof.'">
				<img src="'.$fileNameImg.'" height="100px" width="100px"/></a><br>
				<a href="'.$linkprof.'">'.$dsplayna.'</a>';

				$sql_data2="SELECT * FROM ".$wpdb->prefix."authors_suscribers_notifications WHERE author_id =".$user_ID." and suscriber_id =".$single_data_gen->suscriber_id." GROUP BY streaming_time ";
				$data2 = $wpdb->get_results($sql_data2);

				$cad_style="display:none;position:fixed;top:50%;left:50%;width:40em;max-height:30em;overflow-y:auto; margin-top:-9em;margin-left:-15em;border:1px solid #ccc;background-color:#f3f3f3;";
				/*
				$suscription_list .='<div class="opener" style="cursor: pointer; position:relative"><strong><label>Streaming List</label></strong><div class="img-arrow"><img src="'.get_template_directory_uri().'/images/right_3.gif" class="arrow"/></div>';

				?>
				<?php
				$suscription_list .= '<div class="futurestreamings" style="'.$cad_style.'">
				<strong>Click on any place of the box for close</strong><hr><ul class="channel-detail">';

				if($data2){
					foreach($data2 as $single_data2){
						$data3 = $wpdb->get_results("SELECT l.*,t.* FROM ".$wpdb->prefix."authors_livestreaming l left join ".$wpdb->prefix."tz t on l.timezone_streaming=t.id WHERE l.id =".$single_data2->streaming_id);
						if($data3){
							foreach($data3 as $single_data3){
								if($localtime <= $single_data2->streaming_time){
									$datetimefull = $single_data3->date_livestreaming .' '.$single_data3->name;
									$suscription_list .='<li>Date Time:<span>'.$datetimefull.'</span><br>Title:<span>'.$single_data3->title.'</span><br>Resume:<span>'.$single_data3->intro.'.</span></li><br>';
								}
							}
						}
					}
				}
		     $suscription_list .='</ul></div></div>';
		     */
		     $suscription_list .='</div>';

		     $lines++;

		     //for define a new row
		     $lines++;
		     if(($lines % 3)==0){
		     	$newrow=1;
		     }
		     if($newrow==1){
		     	$suscription_list .='</div>';
		     	$newrow=0;
		     }
		   }//end foreach datagen

		   //complete the missing columns for the last row
		   $putdiv=0;
		   while(($lines % 3)!=0 ){
		   	$suscription_list .='<div style="display: table-cell;"></div>';
		   	$lines++;
		   	$putdiv=1;
		   }
		   if($putdiv==1) $suscription_list .='</div>';

		   $suscription_list .='</div>';
		   echo $suscription_list;
		 }//end if data_gen

		echo '<script type="text/javascript">
		    var tz = jstz.determine();
		    var timezone = tz.name();
		    url = "'.get_template_directory_uri().'/includes/user/save_session.php?action=get_timezone&timezone="+timezone;
			$.ajax({
				url: url,
				type: "GET",
				data: "text",
				cache: false,
				success: function(data){

				}
			});
		</script>';
		 ?>
		</div>
		<?php // END suscriptions tabs // ?>



		<div role="tabpanel" class="tab-pane" id="friends">

		<?php

		if(!empty($_POST['add_friend_request_submit']) and $_POST['add_friend_request_submit'] == "Accept"){
			$to_friend_id		= $_POST['to_friend_id'];
			$from_friend_id		= $_POST['from_friend_id'];

			$wpdb->get_results("UPDATE ".$wpdb->prefix."authors_friends_list SET status=2 WHERE from_friend_id='".$from_friend_id."' AND to_friend_id='".$to_friend_id."'");

			$_POST['add_friend_request_submit'] = "";
		}

		if(!empty($_POST['remove_friend_submit']) and ($_POST['remove_friend_submit'] == "Reject" OR $_POST['remove_friend_submit'] == "Remove Friend")){
			$from_friend_id		= $_POST['from_friend_id'];
			$to_friend_id		= $_POST['to_friend_id'];

			$wpdb->get_results("DELETE FROM ".$wpdb->prefix."authors_friends_list WHERE (from_friend_id='".$from_friend_id."' AND to_friend_id='".$to_friend_id."') OR (from_friend_id='".$to_friend_id."' AND to_friend_id='".$from_friend_id."')");

			$_POST['remove_friend_submit'] = "";
		}

		$result = $wpdb->get_results("SELECT *FROM ".$wpdb->prefix."authors_friends_list WHERE (to_friend_id='".$user_ID."' AND status=1)");

		echo '<div style="display: table;width: 100%;table-layout: fixed;border-spacing: 10px;clear:both;">';
		echo "<h4>Pending friend requests.</h4>";
		if(count($result) > 0){
			foreach($result as $row){

				if($row->to_friend_id == $user_ID){

					$meta_key = 'profile_pic_meta';
					$fileName = get_user_meta($row->from_friend_id, $meta_key, true );
					$userdata = get_userdata($row->from_friend_id);
					$linkprof = site_url().'/user/'.$userdata->user_nicename;

					if(strlen($userdata->display_name)>25) $dsplayna = substr($userdata->display_name,0,22).'...';
					else $dsplayna = $userdata->display_name;
					if($fileName != ''){
						$fileNameImg = site_url().'/profile_pic/'.$fileName;
					}else{
						$fileNameImg = get_stylesheet_directory_uri().'/images/no_avatar.jpg';
					}

					echo '<div style="display:table-cell;" class="live-stream-block" >
					<a href="'.$linkprof.'"><img src="'.$fileNameImg.'" height="100px" width="100px"/></a><a href="'.$linkprof.'">'.$dsplayna.'</a><a>';

					echo '<form method="post" class="subscribers-inner-form">
						<input type="hidden" name="to_friend_id" value="'.$row->to_friend_id.'">
						<input type="hidden" name="from_friend_id" value="'.$row->from_friend_id.'">
						<input type="submit" name="add_friend_request_submit" value="Accept" class="btn btn-success btn-xs">
						</form>';

					echo '<form method="post" class="subscribers-inner-form">
						<input type="hidden" name="from_friend_id" value="'.$row->to_friend_id.'">
						<input type="hidden" name="to_friend_id" value="'.$row->from_friend_id.'">
						<input type="submit" name="remove_friend_submit" value="Reject" class="btn btn-success btn-xs">
						</form>';

					echo '</a></div>';
				}
			}
		}else{
			echo "<h4><small>No friend requests.</small></h4>";
		}
		echo '</div>';

		$result = $wpdb->get_results("SELECT *FROM ".$wpdb->prefix."authors_friends_list WHERE (from_friend_id='".$user_ID."' AND status=2) OR (to_friend_id='".$user_ID."' AND status=2)");

		echo '<div style="display: table;width: 100%;table-layout: fixed;border-spacing: 10px;clear:both;">';
		echo '<h4>Current Friends.</h4>';
		if(count($result) > 0){

			foreach($result as $row){

				if($row->to_friend_id == $user_ID){

					$meta_key = 'profile_pic_meta';
					$fileName = get_user_meta($row->from_friend_id, $meta_key, true );
					$userdata = get_userdata($row->from_friend_id);
					$linkprof = site_url().'/user/'.$userdata->user_nicename;

					if(strlen($userdata->display_name)>25) $dsplayna = substr($userdata->display_name,0,22).'...';
					else $dsplayna = $userdata->display_name;
					if($fileName != ''){
						$fileNameImg = site_url().'/profile_pic/'.$fileName;
					}else{
						$fileNameImg = get_stylesheet_directory_uri().'/images/no_avatar.jpg';
					}

					echo '<div style="display:table-cell;" class="live-stream-block" >
					<a href="'.$linkprof.'"><img src="'.$fileNameImg.'" height="100px" width="100px"/></a><a href="'.$linkprof.'">'.$dsplayna.'</a><a>';

					echo '<form method="post" class="subscribers-inner-form">
						<input type="hidden" name="from_friend_id" value="'.$row->to_friend_id.'">
						<input type="hidden" name="to_friend_id" value="'.$row->from_friend_id.'">
						<input type="submit" name="remove_friend_submit" value="Remove Friend" class="btn btn-success btn-sm">
						</form>';

					echo '</a></div>';
				}
				if($row->from_friend_id == $user_ID){

					$meta_key = 'profile_pic_meta';
					$fileName = get_user_meta($row->to_friend_id, $meta_key, true );
					$userdata = get_userdata($row->to_friend_id);
					$linkprof = site_url().'/user/'.$userdata->user_nicename;

					if(strlen($userdata->display_name)>25) $dsplayna = substr($userdata->display_name,0,22).'...';
					else $dsplayna = $userdata->display_name;
					if($fileName != ''){
						$fileNameImg = site_url().'/profile_pic/'.$fileName;
					}else{
						$fileNameImg = get_stylesheet_directory_uri().'/images/no_avatar.jpg';
					}

					echo '<div style="display:table-cell;" class="live-stream-block" >
					<a href="'.$linkprof.'"><img src="'.$fileNameImg.'" height="100px" width="100px"/></a><a href="'.$linkprof.'">'.$dsplayna.'</a><a>';

					echo '<form method="post" class="subscribers-inner-form">
						<input type="hidden" name="from_friend_id" value="'.$row->from_friend_id.'">
						<input type="hidden" name="to_friend_id" value="'.$row->to_friend_id.'">
						<input type="submit" name="remove_friend_submit" value="Remove Friend" class="btn btn-success btn-sm">
						</form>';

					echo '</a></div>';
				}
			}
		}else{
			echo '<h4><small>No Friends.</small></h4>';
		}
		echo '</div>';


		?>



		</div>

		<div role="tabpanel" class="tab-pane" id="notifications">

		<?php

		$result = $wpdb->get_results("SELECT *FROM ".$wpdb->prefix."authors_friends_list WHERE ((to_friend_id='".$user_ID."' OR from_friend_id='".$user_ID."') AND (status=1 OR status=2)) ORDER BY id DESC");

		echo '<div style="display: table;width: 100%;table-layout: fixed;border-spacing: 10px;clear:both;">';

		echo '<h4>Most recent nofitications.</h4>';

		if(count($result) > 0){
			foreach($result as $row){

				if($row->to_friend_id == $user_ID){

					$userdata = get_userdata($row->from_friend_id);
					$linkprof = site_url().'/user/'.$userdata->user_nicename;

					if(strlen($userdata->display_name)>25)
						$dsplayna = substr($userdata->display_name,0,22).'...';
					else
						$dsplayna = $userdata->display_name;

					if($row->status == 1){
						echo '<p><i class="fa fa-fighter-jet" aria-hidden="true"></i> <small>Friend request received from <a href="'.$linkprof.'">'.$dsplayna.'</a> to you.</small></p>';
					}
					if($row->status == 2){
						echo '<p><i class="fa fa-fighter-jet" aria-hidden="true"></i> <small><a href="'.$linkprof.'">'.$dsplayna.'</a> and you are now friend.</small></p>';
					}

				}
				if($row->from_friend_id == $user_ID){

					$userdata = get_userdata($row->to_friend_id);
					$linkprof = site_url().'/user/'.$userdata->user_nicename;

					if(strlen($userdata->display_name)>25)
						$dsplayna = substr($userdata->display_name,0,22).'...';
					else
						$dsplayna = $userdata->display_name;

					if($row->status == 2){
						echo '<p><i class="fa fa-fighter-jet" aria-hidden="true"></i> <small><a href="'.$linkprof.'">'.$dsplayna.'</a> and you are now friend.</small></p>';
					}

				}
			}
		}else{
			echo '<h4><small>Notifications not available.</small></h4>';
		}
		echo '</div>';

		?>

		</div>


		<div role="tabpanel" class="tab-pane" id="collaboration">

		<style>
		.view_collaboration_class .live-stream-block .attachment-full.wp-post-image{
			width: 100px;
		}
		.view_collaboration_class .live-stream-block a{
			text-transform: capitalize;
		}
		.view_collaboration_class .live-stream-block .btn.btn-success.btn-sm{
			margin: 0 1px;
		}
		</style>

		<?php

		echo do_shortcode('[view_pending_collaboration]');

		echo do_shortcode('[add_collaboration]');

		echo do_shortcode('[view_collaboration]');

		?>

		</div>



		<?php // Favorite tab // ?>
		<div role="tabpanel" class="tab-pane" id="favoriteinf">
		<?php
		//show the list of favorites videos from this user
		$user_ID = get_current_user_id();
		//get all the authors where this user is suscribed
		$dyn_file = array();
		$dyn_book = array();
		$dyn_video = array();
		$data_result = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."favorite WHERE user_id =".$user_ID);
		if($data_result){
			foreach($data_result as $row){
				if($row->post_id != '0'){
					//echo $row->post_id."-------------------".get_post_type($row->post_id)."<br>";
					if(get_post_type($row->post_id) == 'dyn_file'){
						$dyn_file[] = $row->post_id;
					}else if(get_post_type($row->post_id) == 'dyn_book'){
						$dyn_book[] = $row->post_id;
					}else{
						$dyn_video[] = $row->post_id;
					}
				}
			}
		}
		/* print_r($dyn_file);echo "<br>";
		print_r($dyn_book);echo "<br>";
		print_r($dyn_video);echo "<br>"; */
		?>
		<div id="favoriteVideos">
			<div class="cover-twoblocks">
				<div class="sortvideo-div">
					<div class="dyn-book-sort">
						<label>Favorite Videos</label>
					</div>
				</div>
				<div class="well well-sm">
					<strong>Views</strong>
					<div class="btn-group">
						<a href="#" id="list" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th-list">
						</span>List</a> <a href="#" id="grid" class="btn btn-default btn-sm"><span
							class="glyphicon glyphicon-th"></span>Grid</a>
					</div>
				</div>
			</div>

			<script type="text/javascript">
			$(document).ready(function() {
				$('#favoriteVideos #products .item').addClass('grid-group-item');
				$('#favoriteVideos #list').click(function(event){event.preventDefault();$('#favoriteVideos #products .item').addClass('list-group-item');});
				$('#favoriteVideos #grid').click(function(event){event.preventDefault();$('#favoriteVideos #products .item').removeClass('list-group-item');$('#favoriteVideos #products .item').addClass('grid-group-item');});
			});
			</script>
			<?php
			if(count($dyn_video)>0){
				$args = array(
					'post__in' => $dyn_video,
					'paged' => $paged,
					'post_status' => 'publish',
					'posts_per_page' => 10
				);
				$the_query = new WP_Query( $args );

				if ( $the_query->have_posts() ) :
				echo '<div id="products" class="list-group">';
				while ( $the_query->have_posts() ) : $the_query->the_post();
				?>
				<div class="item col-xs-6 col-lg-6 post-<?php the_ID(); ?>">
    <div class="thumbnail layout-2-wrapper solid-bg">

			<div class="grid-6 first">
							<div class="image-holder">
								<a href="<?php echo get_permalink(); ?>">
									<div class="hover-item"></div>
								</a>
								<?php
								if( has_post_thumbnail() ){
									the_post_thumbnail('small-thumb', array( 'class' => 'layout-2-thumb' ) );
								}else{
									echo '<img src="'.get_template_directory_uri().'/images/no-image.png" class="layout-2-thumb">';
								}
								?>
							</div>
							<div class="layout-title-box layout-title-box-<?php the_ID(); ?>" style="display:block;">
								<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php echo ctpreview( $content = get_the_title() , $length = 30 , $message = 'No Title available' , $html = ' [...]' ); ?></a></h6>
								<ul class="bottom-detailsul">
									<?php if(get_the_author_id() == get_current_user_id()){ ?>
									<li><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>"><i class="fa fa-trash-o"></i> Delete</a></li><?php } ?>
									<li><a class="detailsblock-btn" data-modal-id="myModal-<?php the_ID(); ?>"><i class="fa fa-info-circle" aria-hidden="true"></i> Details</a></li>
									<?php if(get_the_author_id() == get_current_user_id()){ ?>
									<li><a class="changesettingsblock-btn" data-modal-id="myModalPrivacy-<?php the_ID(); ?>"><i class="fa fa-info-circle" aria-hidden="true"></i>Privacy</a></li>
									<?php } ?>
								</ul>
							</div>
					    </div>
						<script type="text/javascript">
$(function(){

var appendthis =  ("<div class='modal-overlayblockdiv'></div>");

	$('a[data-modal-id]').click(function(e) {
		e.preventDefault();
    $("body").append(appendthis);
    $(".modal-overlay").fadeTo(500, 0.7);
    //$(".js-modalbox").fadeIn(500);
		var modalBox = $(this).attr('data-modal-id');
		$('#'+modalBox).fadeIn($(this).data());
	});


$(".js-modal-close, .modal-overlay").click(function() {
    $(".modal-box, .modal-overlay").fadeOut(500, function() {
        $(".modal-overlay").remove();
    });

});

$(window).resize(function() {
    $(".modal-box").css({
        top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
        left: ($(window).width() - $(".modal-box").outerWidth()) / 2
    });
});

$(window).resize();

});
</script>

<div class="modal-box" id="myModalPrivacy-<?php the_ID(); ?>">
			<div class="modal-body">
				<a class="js-modal-close close privacyCloseI-<?php the_ID(); ?>">×</a>
				<div class="layout-2-details layout-2-details-<?php the_ID(); ?>">

						<div class="form-group">
							<label for="dyn-tags"><br/>Privacy Options:</label>
							<div class="checkbox">
								  <?php
								  //$selectUsersListArray = explode(",", $selectUsersList[0]);
								    $selectUsersListArray = $selectUsersList;
								     if(isset($privacyOption[0]) && $privacyOption[0] != "")
									 {
								       //echo $privacyOption[0]."<br>";
										 ?>
										    <label>Private
											  <?php
											     if($privacyOption[0] == "private")
												 {
											        ?>
													   <input type="radio" id="privacy-radio-<?php the_ID(); ?>" name="privacy-option-<?php the_ID(); ?>" class="dyn-select-file" class="form-control" value="private" checked="checked">
													<?php
												 }
												 else
												 {
													 ?>
													   <input type="radio" id="privacy-radio-<?php the_ID(); ?>" name="privacy-option-<?php the_ID(); ?>" class="dyn-select-file" class="form-control" value="private">
													<?php
												 }
											  ?>
											</label>
											<label>Public
											<input type="radio" id="privacy-radio1-<?php the_ID(); ?>" class="dyn-select-file" class="form-control" value="public" <?php if (isset($privacyOption[0]) && $privacyOption[0]=="public") echo "checked";?> name="privacy-option2-<?php the_ID(); ?>">
											</label>
										 <?php
									 }
									 else
									 {
										 ?>
										    <label>Private
											  <input type="radio" id="privacy-radio-<?php the_ID(); ?>" name="privacy-option-<?php the_ID(); ?>" class="dyn-select-file" class="form-control" value="private">
											</label>
											<label>Public
											 <input type="radio" id="privacy-radio1-<?php the_ID(); ?>" name="privacy-option2-<?php the_ID(); ?>" class="dyn-select-file" class="form-control" value="public">
											</label>
										 <?php
									 }
								  ?>
							</div>
							 <?php
								 if(isset($privacyOption[0]) and $privacyOption[0] != "")
								 {
									   if($privacyOption[0] == "private")
									   {
										    ?>
											<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/jquery.tokenize.js"></script>
												<div class="select-box-<?php the_ID(); ?>">
													<?php
													  echo '<select id="tokenize-'.get_the_ID().'" class="tokenize-sample" name="select_users_list[]" style="width: 350px; height: 150px;" multiple="multiple">';
														  $args1 = array(
															  'role' => 'free_user',
															  'orderby' => 'id',
															  'order' => 'desc'
														   );
														  $subscribers = get_users($args1);
														  foreach ($subscribers as $user) {
															  if(get_current_user_id() == $user->id)
															  { }
															  else
															  {
														          if( in_array($user->id, $selectUsersListArray) )
																  {
																	   echo '<option value="'.$user->id.'" selected="selected">' . $user->display_name.'</option>';
																  }
                                                                  else{
																	   echo '<option value="'.$user->id.'">' . $user->display_name.'</option>';
																  }
															  }
														  }
													  echo '</select>';
													?>
													<script type="text/javascript">
											          $(document).ready(function(){
														  $("#privacy-radio").click(function(){
															 $(".select-box").slideDown();
														  });
														  $("#privacy-radio1").click(function(){
															 $(".select-box").slideUp();
															 $('select#select_users_list option').removeAttr("selected");
														  });

													  });
													  $('#tokenize-<?php echo get_the_ID(); ?>').tokenize();
				                                         datas: "bower.json.php"
												  </script>
												  <style> .tokenize-sample { width: 350px;; }</style>
												</div>
										   <?php
									   }
									   else
									   {
                                            ?>
											<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/jquery.tokenize.js"></script>
												<div style="display:none;" class="select-box-<?php the_ID(); ?>">
													<?php
													  echo '<select id="tokenize-'.get_the_ID().'" class="tokenize-sample" name="select_users_list[]" style="width: 350px; height: 150px;" multiple="multiple">';
														  $args1 = array(
															  'role' => 'free_user',
															  'orderby' => 'id',
															  'order' => 'desc'
														   );
														  $subscribers = get_users($args1);
														  foreach ($subscribers as $user) {
															  if(get_current_user_id() == $user->id)
															  { }
															  else
															  {
																  echo '<option value="'.$user->id.'">' . $user->display_name.'</option>';
															  }
														  }
													  echo '</select>';
													?>
													<script type="text/javascript">
											          $(document).ready(function(){
														  $("#privacy-radio").click(function(){
															 $(".select-box").slideDown();
														  });
														  $("#privacy-radio1").click(function(){
															 $(".select-box").slideUp();
															 $('select#select_users_list option').removeAttr("selected");
														  });

													  });
													  $('#tokenize-<?php echo get_the_ID(); ?>').tokenize();
				                                         datas: "bower.json.php"
												  </script>
												  <style> .tokenize-sample { width: 350px;; }</style>
												</div>
										   <?php
									   }
								 }
								 else
								 {
									   ?>
									   <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/jquery.tokenize.js"></script>
									        <div style="display:none;" class="select-box-<?php the_ID(); ?>">
												<?php
												  echo '<select id="tokenize-'.get_the_ID().'" class="tokenize-sample" name="select_users_list[]" style="width: 350px; height: 150px;" multiple="multiple">';
													  $args1 = array(
														  'role' => 'free_user',
														  'orderby' => 'id',
														  'order' => 'desc'
													   );
													  $subscribers = get_users($args1);
													  foreach ($subscribers as $user) {
														  if(get_current_user_id() == $user->id)
														  { }
														  else
														  {
															  echo '<option value="'.$user->id.'">' . $user->display_name.'</option>';
														  }
													  }
												  echo '</select>';
												?>
												<script type="text/javascript">
											          $(document).ready(function(){
														  $("#privacy-radio").click(function(){
															 $(".select-box").slideDown();
														  });
														  $("#privacy-radio1").click(function(){
															 $(".select-box").slideUp();
															 $('select#select_users_list option').removeAttr("selected");
														  });

													  });
													  $('#tokenize-<?php echo get_the_ID(); ?>').tokenize();
				                                         datas: "bower.json.php"
												  </script>
												  <style> .tokenize-sample { width: 350px;; }</style>
											</div>
									   <?php
								 }
							 ?>
							 <br>
							 <div id="msgLoader-<?php the_ID(); ?>"></div>
                             <input type="button" id="save-Privacy-Option-<?php the_ID(); ?>" name="save-privacy-option-<?php the_ID(); ?>" value="Save Privacy Option" currentUserID="<?php echo get_current_user_id(); ?>" postID="<?php echo get_the_ID(); ?>">
						</div>
					<script>
                      $(document).ready(function(){
						  $("#privacy-radio-<?php the_ID(); ?>").click(function(){
							 $("#privacy-radio1-<?php the_ID(); ?>").prop( "checked", false );
						     $(".select-box-<?php the_ID(); ?>").slideDown();
						  });
						  $("#privacy-radio1-<?php the_ID(); ?>").click(function(){
							 $("#privacy-radio-<?php the_ID(); ?>").prop( "checked", false );
						     $(".select-box-<?php the_ID(); ?>").slideUp();
							 $('select#select_users_list-<?php the_ID(); ?> option').removeAttr("selected");
						  });
						  $(".privacyCloseI-<?php the_ID(); ?>").click(function(){
							 $('#msgLoader-<?php the_ID(); ?>').html('');
						  });
						  $("#save-Privacy-Option-<?php the_ID(); ?>").click(function(){
							   var userID = $(this).attr('currentUserID');
							   var postID = $(this).attr('postID');
							   var select_users_list = $('#tokenize-'+postID).val();
							   if ( typeof(select_users_list) !== "undefined" && select_users_list !== null )
							   { }
						       else
							   {
								   select_users_list = '';
							   }
							   var privacyOptionV = "";
							   if($('#privacy-radio-<?php the_ID(); ?>').is(':checked'))
							   {
								   var privateChecked = "yes";
							   }
							   else
							   {
								   var privateChecked = "no";
							   }
							   if($('#privacy-radio1-<?php the_ID(); ?>').is(':checked'))
							   {
								   var publicChecked = "yes";
							   }
							   else
							   {
								   var publicChecked = "no";
							   }
							   if(privateChecked == "yes")
							   {
								   privacyOptionV = "private";
							   }
							   else if(publicChecked == "yes")
							   {
								   privacyOptionV = "public";
							   }
							   else
							   {
								   privacyOptionV = "";
							   }
                               $.ajax({
									type: 'POST',
									url: "<?php echo site_url(); ?>/update-Privacy.php",
									data: { userID: userID, postID: postID, privacyOptionV : privacyOptionV,
									select_users_list : select_users_list},
									beforeSend: function(){
									  $('#msgLoader-<?php the_ID(); ?>').html('<img src="<?php echo site_url(); ?>/wp-content/themes/videopress/images/loading.gif" />');
									},
									success: function(data){
										 $('#msgLoader-<?php the_ID(); ?>').html(data);
									}
								});
						  });
                      });
					 // $('#tokenize').tokenize();
                    </script>
					<style> .tokenize-sample { width: 350px;; }</style>
					<style>
                      #privacy-radio1-<?php the_ID(); ?>, #privacy-radio-<?php the_ID(); ?> {
                        padding: 5px;
                        text-align: center;
                      }

                     #select-box-<?php the_ID(); ?> {
                       padding: 50px;
                       display: none;
                     }
                    </style>
				</div>
			</div>
</div>
			<div class="modal-box" id="myModal-<?php the_ID(); ?>">
			<div class="modal-body">
			<a class="js-modal-close close">×</a>
			<div class="layout-2-details layout-2-details-<?php the_ID(); ?>">
				<div class="image-holder">
					<a href="<?php echo get_permalink(); ?>">
						<div class="hover-item"></div>
					</a>

					<?php
					if( has_post_thumbnail() ){
						the_post_thumbnail('small-thumb', array( 'class' => 'layout-2-thumb' ) );
					}else{
						echo '<img src="'.get_template_directory_uri().'/images/no-image.png" class="layout-2-thumb">';
					}
					?>

				</div>
				<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h6>
				<?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
				<div class="video_excerpt_text one"><h4 class="tittle-h4">Description 2</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = '' , $message = 'No Description available' , $html = ' [...]' );?></div></div>
				<div class="video_excerpt_text two"><h4 class="tittle-h4">Reference</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_post_meta(get_the_ID(),'video_options_refr',true) , $length = '' , $message = 'No referance given' , $html = ' [...]' );?></div></div>
				<ul class="stats">
					<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
					<li><?php videopress_countviews( get_the_ID() ); ?></li>
					<li><i class="fa fa-wrench"></i> <?php $result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
						echo count($result); ?> Endorsments</li>
					<!-- favourite section -->
					<li><i class="fa fa-heart"></i>
					<?php
					  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
					  $result1 = $wpdb->get_results($sql_aux);
					  echo count($result1);
					?> Favorites</li>
					<!-- end favorite -->
					<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
					<li><?php comments_number() ?></li>
				</ul>
				<div class="clear"></div>
				<p><?php echo videopress_content('240'); ?></p>

					<?php if(get_the_author_id() == get_current_user_id()){ ?>
					<p><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>"><i class="fa fa-trash-o"></i> Delete</a></p>
					<?php } ?>


			</div></div></div>

			<div class="layout-2-details layout-2-details-<?php the_ID(); ?>" style="display:none;">
				<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h6>
				<?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
				<div class="video_excerpt_text one"><h4 class="tittle-h4">Description</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = 100 , $message = 'No Description available' , $html = '<a data-modal-id="myModal-'.get_the_ID().'"> [...]</a>' ); ?></div></div>
				<ul class="stats">
					<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
					<li><?php videopress_countviews( get_the_ID() ); ?></li>
					<li><i class="fa fa-wrench"></i> <?php $result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
						echo count($result); ?> Endorsments</li>
					<!-- favourite section -->
					<li><i class="fa fa-heart"></i>
					<?php
					  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
					  $result1 = $wpdb->get_results($sql_aux);
					  echo count($result1);
					?> Favorites</li>
					<!-- end favorite -->
					<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
					<li><?php comments_number() ?></li>
				</ul>
				<div class="clear"></div>
				<p><?php echo videopress_content('240'); ?></p>

					<p><?php if(get_the_author_id() == get_current_user_id()){ ?>
					      <a href="#" class="del-video" data-id="<?= get_the_ID(); ?>"><i class="fa fa-trash-o"></i> Delete</a>

					      <a class="privacy-video" data-modal-id="myModalPrivacy-<?php the_ID(); ?>"><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;Privacy</a>
						<?php } ?>
					</p>

			</div>

			<div class="clear"></div>
</div>
</div>
				<?php
				endwhile;
				echo "</div><div class='paginate_division'>".paginate_links(array('add_args' => array('display_page' => 'favorite_video')))."</div>";
				endif;
				wp_reset_postdata();
			}else{
				echo "<div>No Videos Available</div>";
			}?>

		</div>
		<div id="favoriteFiles">
			<div class="cover-twoblocks">
				<div class="sortvideo-div">
					<div class="dyn-book-sort">
						<label>Favorite Files</label>
					</div>
				</div>
				<div class="well well-sm">
					<strong>Views</strong>
					<div class="btn-group">
						<a href="#" id="list" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th-list">
						</span>List</a> <a href="#" id="grid" class="btn btn-default btn-sm"><span
							class="glyphicon glyphicon-th"></span>Grid</a>
					</div>
				</div>
			</div>

			<script type="text/javascript">
			$(document).ready(function() {
				$('#favoriteFiles #products .item').addClass('grid-group-item');
				$('#favoriteFiles #list').click(function(event){event.preventDefault();$('#favoriteFiles #products .item').addClass('list-group-item');});
				$('#favoriteFiles #grid').click(function(event){event.preventDefault();$('#favoriteFiles #products .item').removeClass('list-group-item');$('#favoriteFiles #products .item').addClass('grid-group-item');});
			});
			</script>
			<?php
			if(count($dyn_file)>0){
				$args = array(
					'post__in' => $dyn_file,
					'paged' => $paged,
					'post_status' => 'publish',
					'posts_per_page' => 10,
					'post_type' => 'dyn_file'
				);
				$the_query = new WP_Query( $args );

				if ( $the_query->have_posts() ) :
				echo '<div id="products" class="list-group">';
				while ( $the_query->have_posts() ) : $the_query->the_post();
				?>
				<div class="item col-xs-6 col-lg-6 post-<?php the_ID(); ?>">
				<div class="thumbnail layout-2-wrapper solid-bg">

					<div class="grid-6 first">
						<div class="image-holder filesz dyn-file-sprite <?php echo apply_filters('dyn_class_by_mime', get_the_id()); ?>">
							<a href="<?php echo get_permalink(); ?>">
								<div class="hover-item dyn-file">
			                        <i class="fa fa-download"></i>
			                    </div>
							</a>
						</div>
						<div class="layout-title-box layout-title-box-<?php the_ID(); ?>" style="display:block;">
							<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php echo ctpreview( $content = get_the_title() , $length = 30 , $message = 'No Title available' , $html = ' [...]' ); ?></a></h6>
							<ul class="bottom-detailsul">
							<?php if(get_the_author_id() == get_current_user_id()){ ?>
							<li><p><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>" ><i class="fa fa-trash-o"></i> Delete</a></p></li>
							<?php } ?>
							<li><a class="detailsblock-btn" data-modal-id="myModal-<?php the_ID(); ?>"><i class="fa fa-info-circle" aria-hidden="true"></i> Details</a></li>
							<?php if(get_the_author_id() == get_current_user_id()){ ?>
							<li><a class="changesettingsblock-btn" data-modal-id="myModalPrivacy-<?php the_ID(); ?>"><i class="fa fa-info-circle" aria-hidden="true"></i>Privacy</a></li><?php } ?>
							</ul>
						</div>
					</div>

<script type="text/javascript">
$(function(){

var appendthis =  ("<div class='modal-overlayblockdiv'></div>");

	$('a[data-modal-id]').click(function(e) {
		e.preventDefault();
    $("body").append(appendthis);
    $(".modal-overlay").fadeTo(500, 0.7);
    //$(".js-modalbox").fadeIn(500);
		var modalBox = $(this).attr('data-modal-id');
		$('#'+modalBox).fadeIn($(this).data());
	});


$(".js-modal-close, .modal-overlay").click(function() {
    $(".modal-box, .modal-overlay").fadeOut(500, function() {
        $(".modal-overlay").remove();
    });
});

$(window).resize(function() {
    $(".modal-box").css({
        top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
        left: ($(window).width() - $(".modal-box").outerWidth()) / 2
    });
});

$(window).resize();

});
</script>
<div class="modal-box" id="myModalPrivacy-<?php the_ID(); ?>">
			<div class="modal-body">
				<a class="js-modal-close close privacyCloseI-<?php the_ID(); ?>">×</a>
				<div class="layout-2-details layout-2-details-<?php the_ID(); ?>">

						<div class="form-group">
							<label for="dyn-tags"><br/>Privacy Options:</label>
							<div class="checkbox">
								  <?php
								  //$selectUsersListArray = explode(",", $selectUsersList[0]);
                                      $selectUsersListArray = $selectUsersList;
								     if(isset($privacyOption[0]) && $privacyOption[0] != "")
									 {
								       //echo $privacyOption[0]."<br>";
										 ?>
										    <label>Private
											  <?php
											     if($privacyOption[0] == "private")
												 {
											        ?>
													   <input type="radio" id="privacy-radio-<?php the_ID(); ?>" name="privacy-option-<?php the_ID(); ?>" class="dyn-select-file" class="form-control" value="private" checked="checked">
													<?php
												 }
												 else
												 {
													 ?>
													   <input type="radio" id="privacy-radio-<?php the_ID(); ?>" name="privacy-option-<?php the_ID(); ?>" class="dyn-select-file" class="form-control" value="private">
													<?php
												 }
											  ?>
											</label>
											<label>Public
											<input type="radio" id="privacy-radio1-<?php the_ID(); ?>" class="dyn-select-file" class="form-control" value="public" <?php if (isset($privacyOption[0]) && $privacyOption[0]=="public") echo "checked";?> name="privacy-option2-<?php the_ID(); ?>">
											</label>
										 <?php
									 }
									 else
									 {
										 ?>
										    <label>Private
											  <input type="radio" id="privacy-radio-<?php the_ID(); ?>" name="privacy-option-<?php the_ID(); ?>" class="dyn-select-file" class="form-control" value="private">
											</label>
											<label>Public
											 <input type="radio" id="privacy-radio1?>" name="privacy-option2-<?php the_ID(); ?>" class="dyn-select-file" class="form-control" value="public">
											</label>
										 <?php
									 }
								  ?>
							</div>
							 <?php
								 if(isset($privacyOption[0]) && $privacyOption[0] != "")
								 {
									   if($privacyOption[0] == "private")
									   {
										       $args1 = array(
												  'role' => 'free_user',
												  'orderby' => 'id',
												  'order' => 'desc'
											   );
											  $subscribers = get_users($args1);
										    ?>
											<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/jquery.tokenize.js"></script>
												<div class="select-box-<?php the_ID(); ?>">
													<?php
													  echo '<select class="tokenize-sample" id="tokenize-'.get_the_ID().'" name="select_users_list[]" style="width: 350px; height: 150px;" multiple="multiple">';
														  foreach ($subscribers as $user) {
															  if(get_current_user_id() == $user->id)
															  { }
															  else
															  {
														          if( in_array($user->id, $selectUsersListArray) )
																  {
																	   echo '<option value="'.$user->id.'" selected="selected">' . $user->display_name.'</option>';
																  }
                                                                  else{
																	   echo '<option value="'.$user->id.'">' . $user->display_name.'</option>';
																  }
															  }
														  }
													  echo '</select>';
													?>

													<script type="text/javascript">
																		  $(document).ready(function(){
												  $("#privacy-radio").click(function(){
													 $(".select-box").slideDown();
												  });
												  $("#privacy-radio1").click(function(){
													 $(".select-box").slideUp();
													 $('select#select_users_list option').removeAttr("selected");
												  });

											  });
											  $('#tokenize-<?php echo get_the_ID(); ?>').tokenize();
				                                datas: "bower.json.php"
										  </script>
										  <style> .tokenize-sample { width: 350px;; }</style>
												</div>

										   <?php
									   }
									   else
									   {
                                            ?>

												<div style="display:none;" class="select-box-<?php the_ID(); ?>">
													<?php
													  echo '<select class="tokenize-sample" id="tokenize-'.get_the_ID().'" name="select_users_list[]" style="width: 350px; height: 150px;" multiple="multiple">';
														  $args1 = array(
															  'role' => 'free_user',
															  'orderby' => 'id',
															  'order' => 'desc'
														   );
														  $subscribers = get_users($args1);
														  foreach ($subscribers as $user) {
															  if(get_current_user_id() == $user->id)
															  { }
															  else
															  {
																  echo '<option value="'.$user->id.'">' . $user->display_name.'</option>';
															  }
														  }
													  echo '</select>';
													?>

													<script type="text/javascript">
																		  $(document).ready(function(){
												  $("#privacy-radio").click(function(){
													 $(".select-box").slideDown();
												  });
												  $("#privacy-radio1").click(function(){
													 $(".select-box").slideUp();
													 //$('select#select_users_list option').removeAttr("selected");
												  });

											  });
											      $('#tokenize-<?php echo get_the_ID(); ?>').tokenize();
				                                         datas: "bower.json.php"
										  </script>
										  <style> .tokenize-sample { width: 350px; }</style>
												</div>

										   <?php
									   }
								 }
								 else
								 {
									   ?>
									        <div style="display:none;" class="select-box-<?php the_ID(); ?>">
												<?php
												  echo '<select class="tokenize-sample" id="tokenize-'.get_the_ID().'" name="select_users_list[]" style="width: 350px; height: 150px;" multiple="multiple">';
													  $args1 = array(
														  'role' => 'free_user',
														  'orderby' => 'id',
														  'order' => 'desc'
													   );
													  $subscribers = get_users($args1);
													  foreach ($subscribers as $user) {
														  if(get_current_user_id() == $user->id)
														  { }
														  else
														  {
															  echo '<option value="'.$user->id.'">' . $user->display_name.'</option>';
														  }
													  }
												  echo '</select>';
												?>

												<script type="text/javascript">
													 $(document).ready(function(){
												  $("#privacy-radio").click(function(){
													 $(".select-box").slideDown();
												  });
												  $("#privacy-radio1").click(function(){
													 $(".select-box").slideUp();
													 //$('select#select_users_list option').removeAttr("selected");
												  });

											  });
											     $('#tokenize-<?php echo get_the_ID(); ?>').tokenize();
				                                         datas: "bower.json.php"
										  </script>
										  <style> .tokenize-sample { width: 350px; }</style>
											</div>

									   <?php
								 }
							 ?>
							 <br>
							 <div id="msgLoader-<?php the_ID(); ?>"></div>
                             <input type="button" id="save-Privacy-Option-<?php the_ID(); ?>" name="save-privacy-option-<?php the_ID(); ?>" value="Save Privacy Option" currentUserID="<?php echo get_current_user_id(); ?>" postID="<?php echo get_the_ID(); ?>">
						</div>


					<script>
                      $(document).ready(function(){
						  $("#privacy-radio-<?php the_ID(); ?>").click(function(){
							 $("#privacy-radio1-<?php the_ID(); ?>").prop( "checked", false );
						     $(".select-box-<?php the_ID(); ?>").slideDown();
						  });
						  $("#privacy-radio1-<?php the_ID(); ?>").click(function(){
							 $("#privacy-radio-<?php the_ID(); ?>").prop( "checked", false );
						     $(".select-box-<?php the_ID(); ?>").slideUp();
							 $('select#select_users_list-<?php the_ID(); ?> option').removeAttr("selected");
						  });
						  $(".privacyCloseI-<?php the_ID(); ?>").click(function(){
							 $('#msgLoader-<?php the_ID(); ?>').html('');
						  });
						  $("#save-Privacy-Option-<?php the_ID(); ?>").click(function(){
							   var userID = $(this).attr('currentUserID');
							   var postID = $(this).attr('postID');
							   var select_users_list = $('#tokenize-'+postID).val();
							   if ( typeof(select_users_list) !== "undefined" && select_users_list !== null )
							   { }
						       else
							   {
								   select_users_list = '';
							   }
							   var privacyOptionV = "";
							   if($('#privacy-radio-<?php the_ID(); ?>').is(':checked'))
							   {
								   var privateChecked = "yes";
							   }
							   else
							   {
								   var privateChecked = "no";
							   }
							   if($('#privacy-radio1-<?php the_ID(); ?>').is(':checked'))
							   {
								   var publicChecked = "yes";
							   }
							   else
							   {
								   var publicChecked = "no";
							   }
							   if(privateChecked == "yes")
							   {
								   privacyOptionV = "private";
							   }
							   else if(publicChecked == "yes")
							   {
								   privacyOptionV = "public";
							   }
							   else
							   {
								   privacyOptionV = "";
							   }
                               $.ajax({
									type: 'POST',
									url: "<?php echo site_url(); ?>/update-Privacy.php",
									data: { userID: userID, postID: postID, privacyOptionV : privacyOptionV,
									select_users_list : select_users_list},
									beforeSend: function(){
									  $('#msgLoader-<?php the_ID(); ?>').html('<img src="<?php echo site_url(); ?>/wp-content/themes/videopress/images/loading.gif" />');
									},
									success: function(data){
										 $('#msgLoader-<?php the_ID(); ?>').html(data);

									}

								});
						  });


                      });

					 $('#tokenize').tokenize();
                    </script>
					<style> .tokenize-sample { width: 350px; }</style>
					<style>
                      #privacy-radio1-<?php the_ID(); ?>, #privacy-radio-<?php the_ID(); ?> {
                        padding: 5px;
                        text-align: center;
                      }

                     #select-box-<?php the_ID(); ?> {
                       padding: 50px;
                       display: none;
                     }
                    </style>
				</div>
			</div>
</div>
					<div class="modal-box" id="myModal-<?php the_ID(); ?>">
					 <div class="modal-body">
					 <a class="js-modal-close close">×</a>
					<div class="layout-2-details layout-2-details-<?php the_ID(); ?>">
						<div class="image-holder filesz dyn-file-sprite <?php echo apply_filters('dyn_class_by_mime', get_the_id()); ?>">
							<a href="<?php echo get_permalink(); ?>">
								<div class="hover-item dyn-file">
			                        <i class="fa fa-download"></i>
			                    </div>
							</a>
						</div>
						<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title();?></a></h6>
						<div>File Type: <?php echo apply_filters( 'dyn_file_type_by_mime', get_the_ID() ); ?></div>
						<?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
						<div class="video_excerpt_text one"><h4 class="tittle-h4">Description 3</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = '' , $message = 'No Description available' , $html = ' [...]' );?></div></div>
						<div class="video_excerpt_text two"><h4 class="tittle-h4">Reference</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_post_meta(get_the_ID(),'video_options_refr',true) , $length = '' , $message = 'No referance given' , $html = ' [...]' );?></div></div>
						<ul class="stats">
							<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
							<li><?php videopress_countviews( get_the_ID() ); ?></li>
							<li><i class="fa fa-wrench"></i>
							<?php
								$result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
								echo count($result);
							?> Endorsments</li>
							<li><i class="fa  fa-heart"></i>
							<?php
							  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
							  $result1 = $wpdb->get_results($sql_aux);
							  echo count($result1);
							?> Favorites</li>
							<!--end favorite-->
							<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
							<li><?php comments_number() ?></li>
							<li><i class="fa fa-download"></i> <?php echo apply_filters( 'dyn_number_of_download', get_the_id()); ?></li>
						</ul>
						<div class="clear"></div>
						<p><?php echo videopress_content('240'); ?></p>
						<?php if(get_the_author_id() == get_current_user_id()){ ?>
						<p><a href="#" class="del-video" data-id="<?= get_the_ID(); ?>" ><i class="fa fa-trash-o"></i> Delete</a></p>
						<?php } ?>
					</div></div></div>



					<div class="layout-2-details layout-2-details-<?php the_ID(); ?>" style="display:none;">
						<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title();?></a></h6>
						<div>File Type: <?php echo apply_filters( 'dyn_file_type_by_mime', get_the_ID() ); ?></div>
						<?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
						<div class="video_excerpt_text one"><h4 class="tittle-h4">Description</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = 100 , $message = 'No Description available' , $html = '<a data-modal-id="myModal-'.get_the_ID().'"> [...]</a>' ); ?></div></div>
						<ul class="stats">
							<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
							<li><?php videopress_countviews( get_the_ID() ); ?></li>
							<li><i class="fa fa-wrench"></i>
							<?php
								$result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
								echo count($result);
							?> Endorsments</li>
							<li><i class="fa  fa-heart"></i>
							<?php
							  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
							  $result1 = $wpdb->get_results($sql_aux);
							  echo count($result1);
							?> Favorites</li>
							<!--end favorite-->
							<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
							<li><?php comments_number() ?></li>
							<li><i class="fa fa-download"></i> <?php echo apply_filters( 'dyn_number_of_download', get_the_id()); ?></li>
						</ul>
						<div class="clear"></div>
						<p><?php echo videopress_content('240'); ?>
						<?php if(get_the_author_id() == get_current_user_id()){ ?>
						        <a href="#" class="del-video" data-id="<?= get_the_ID(); ?>" ><i class="fa fa-trash-o"></i> Delete</a>
						        <a class="privacy-video" data-modal-id="myModalPrivacy-<?php the_ID(); ?>"><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;Privacy</a>
						<?php } ?>
						</p>
					</div>

					<div class="clear"></div>
				</div>
			</div>
				<?php
				endwhile;
				echo "</div><div class='paginate_division'>".paginate_links(array('add_args' => array('display_page' => 'favorite_file')))."</div>";
				endif;
				wp_reset_postdata();
			}else{
				echo "<div>No Files Available</div>";
			}?>
		</div>
		<div id="favoriteBooks">
			<div class="cover-twoblocks">
				<div class="sortvideo-div">
					<div class="dyn-book-sort">
						<label>Favorite Books</label>
					</div>
				</div>
				<div class="well well-sm">
					<strong>Views</strong>
					<div class="btn-group">
						<a href="#" id="list" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th-list">
						</span>List</a> <a href="#" id="grid" class="btn btn-default btn-sm"><span
							class="glyphicon glyphicon-th"></span>Grid</a>
					</div>
				</div>
			</div>

			<script type="text/javascript">
			$(document).ready(function() {
				$('#favoriteBooks #products .item').addClass('grid-group-item');
				$('#favoriteBooks #list').click(function(event){event.preventDefault();$('#favoriteBooks #products .item').addClass('list-group-item');});
				$('#favoriteBooks #grid').click(function(event){event.preventDefault();$('#favoriteBooks #products .item').removeClass('list-group-item');$('#favoriteBooks #products .item').addClass('grid-group-item');});
			});
			</script>
			<?php
			if(count($dyn_book)>0){
				$args = array(
					'post__in' => $dyn_book,
					'paged' => $paged,
					'post_status' => 'publish',
					'posts_per_page' => 10,
					'post_type' => 'dyn_book'
				);
				$the_query = new WP_Query( $args );

				if ( $the_query->have_posts() ) :
				echo '<div id="products" class="list-group">';
				while ( $the_query->have_posts() ) : $the_query->the_post();
				?>
				<div class="item col-xs-6 col-lg-6 post-<?php the_ID(); ?>">
					<div class="thumbnail layout-2-wrapper solid-bg">

						<div class="grid-6 first">


							<div class="image-holder">
								<a href="<?php the_permalink(); ?>">
								<div class="hover-item"></div>
								</a>
								<?php
									if (has_post_thumbnail()) {
										the_post_thumbnail('small-thumb', array( 'class' => 'layout-2-thumb' ) );
									} else {
										echo '<img class="layout-2-thumb wp-post-image" src="'. get_template_directory_uri() . '/images/no-image.png' .'" alt="" />';
									}
								?>

							</div>
							<div class="layout-title-box layout-title-box-<?php the_ID(); ?>" style="display:block;">
								<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php echo ctpreview( $content = get_the_title() , $length = 30 , $message = 'No Title available' , $html = ' [...]' ); ?></a></h6>
								<ul class="bottom-detailsul">
								<?php if($profile_id == get_current_user_id()){ ?>
								<li><p><a href="#" class="del-book" data-id="<?= get_the_ID(); ?>" ><i class="fa fa-trash-o"></i> Delete</a></p></li>
								<?php } ?>
								<li><a class="detailsblock-btn" data-modal-id="myModal-<?php the_ID(); ?>"><i class="fa fa-info-circle" aria-hidden="true"></i> Details</a></li>
								</ul>
							</div>
						</div>



						<div class="modal-box" id="myModal-<?php the_ID(); ?>">
						 <div class="modal-body">
						 <a class="js-modal-close close">×</a>
						<div class="layout-2-details layout-2-details-<?php the_ID(); ?>">
							<div class="image-holder dyn-book-file-sprite dyn-book-file-image">
								<a href="<?php the_permalink() ?>">
									<div class="hover-item">
										<i class="fa fa-download"></i>
									</div>
								</a>
							</div>
							<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title();?></a></h6>
							<div>File Type: Book</div>
							<?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
							<div class="video_excerpt_text one"><h4 class="tittle-h4">Description 4</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = '' , $message = 'No Description available' , $html = ' [...]' );?></div></div>
							<div class="video_excerpt_text two"><h4 class="tittle-h4">Reference</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_post_meta(get_the_ID(),'video_options_refr',true) , $length = '' , $message = 'No referance given' , $html = ' [...]' );?></div></div>
							<ul class="stats">
								<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
								<li><?php videopress_countviews( get_the_ID() ); ?></li>
								<li><i class="fa fa-wrench"></i>
								<?php
									$result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
									echo count($result);
								?> Endorsments</li>
								<li><i class="fa  fa-heart"></i>
								<?php
								  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
								  $result1 = $wpdb->get_results($sql_aux);
								  echo count($result1);
								?> Favorites</li>
								<!--end favorite-->
								<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
								<li><?php comments_number() ?></li>

							</ul>
							<div class="clear"></div>
							<p><?php echo videopress_content('240'); ?></p>
							<?php if($profile_id == get_current_user_id()){ ?>
							<p><a href="#" class="del-book" data-id="<?= get_the_ID(); ?>" ><i class="fa fa-trash-o"></i> Delete</a></p>
							<?php } ?>
						</div></div></div>



						<div class="layout-2-details layout-2-details-<?php the_ID(); ?>" style="display:none;">
							<h6 class="layout-title"><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title();?></a></h6>
							<div>File Type: Book</div>
							<?php echo apply_filters( 'dyn_display_review', $output, get_the_ID(), "post" ); ?>
							<div class="video_excerpt_text one"><h4 class="tittle-h4">Description</h4><div class="inline-blockright"><?php echo ctpreview( $content = get_the_content() , $length = 100 , $message = 'No Description available' , $html = '<a data-modal-id="myModal-'.get_the_ID().'"> [...]</a>' ); ?></div></div>
							<ul class="stats">
								<li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
								<li><?php videopress_countviews( get_the_ID() ); ?></li>
								<li><i class="fa fa-wrench"></i>
								<?php
									$result = $wpdb->get_results( 'SELECT user_id FROM `wp_endorsements` WHERE post_id = ' . get_the_ID());
									echo count($result);
								?> Endorsments</li>
								<li><i class="fa  fa-heart"></i>
								<?php
								  $sql_aux = 'SELECT user_id FROM '.$wpdb->prefix.'favorite WHERE post_id = ' . get_the_ID();
								  $result1 = $wpdb->get_results($sql_aux);
								  echo count($result1);
								?> Favorites</li>
								<!--end favorite-->
								<li><i class="fa fa-star-o"></i> <?php echo apply_filters( 'dyn_number_of_post_review', get_the_ID(), "post"); ?></li>
								<li><?php comments_number() ?></li>

							</ul>
							<div class="clear"></div>
							<p><?php echo videopress_content('240'); ?></p>
							<?php if($profile_id == get_current_user_id()){ ?>
							<p><a href="#" class="del-book" data-id="<?= get_the_ID(); ?>" ><i class="fa fa-trash-o"></i> Delete</a></p>
							<?php } ?>
						</div>

						<div class="clear"></div>
					</div>
				</div>
				<?php
				endwhile;
				echo "</div><div class='paginate_division'>".paginate_links(array('add_args' => array('display_page' => 'favorite_book')))."<div>";
				endif;
				wp_reset_postdata();
			}else{
				echo "<div>No Books Available</div>";
			}
			?>
		</div>
		</div>
		<?php // END favorite // ?>
		<?php } ?>

		<div role="tabpanel" class="tab-pane <?= $review_active;?>" id="reviews">
			<?php
			$sort = "recent"; //recent, top_rated
			$output = "";
			echo apply_filters( 'dyn_review_tab', $output, $profile_id, "file" );
			?>
		</div>
		<?php if($author->ID== get_current_user_id()) { ?>
		<?php // Start Analytics // ?>
	<!--<div role="tabpanel" class="tab-pane" id="analytics">
	  <div class="CSSTableGenerator" >
	    <table width="80%" cellpadding="0" align="center" style="border:1">
          <tbody>
            <tr>
              <td>&nbsp;</td>
              <td>Today</td>
              <td>Past&nbsp;7&nbsp;Days&nbsp;</td>
              <td>Past&nbsp;30&nbsp;Days</td>
              <td>&nbsp;All&nbsp;Time&nbsp;</td>
            </tr>
            <?php
	         //get all the videos from the post list from this author
	         $sql_data_gen="SELECT * FROM ".$wpdb->prefix."posts WHERE post_author =".$author->ID;
	         $data_post = $wpdb->get_results($sql_data_gen);
		     if($data_post){
		     	foreach($data_post as $single_data_post){
	        ?>
            <tr>
              <td><?php echo $single_data_post->post_title; ?></td>
              <td>10</td>
              <td>20</td>
              <td>30</td>
              <td>40</td>
            </tr>
            <?php
               }
             }
            ?>
          </tbody>
        </table>
      </div>
  </div>	-->
  <?php // END Analytics // ?>

  <?php } ?>

  <?php // START FILES TAB // ?>
  <style>
  	th{font-weight:bold;}
  </style>
  <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.dataTables.min.js"></script>
  <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/dataTables.bootstrap.min.js"></script>

				<div role="tabpanel" class="tab-pane <?= $new_file_active;?>" id="new_files">
					<?php
					echo apply_filters( 'dyn_files_list', $profile_id);
					?>
				</div>


	<?php // START FILES TAB // ?>
  <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.dataTables.min.js"></script>
  <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/dataTables.bootstrap.min.js"></script>

				<div role="tabpanel" class="tab-pane <?= $new_book_active;?>" id="new_books">
					<?php
//					if (has_filter('profile_new_book_list')) {
//					   echo apply_filters( 'profile_new_book_list', $profile_id);
//					}
                                        //please use this code for showing books
                                           echo do_shortcode( '[rfpgetbooks]' );
                                         ?>
				</div>

	<!-- End Uploaded Videos by user ??-Arun what is this? -->

	<script>
		jQuery(document).ready(function(){
			var $ = jQuery.noConflict();

			$('.button_p_d').click(function(e){
				e.preventDefault();

				console.log($(this).attr('data-id'));
				var confrm = confirm("Are you sure you want to continue!");
				var playlist_id = $(this).attr('data-id');
				var user_id = '<?= get_current_user_id;?>';

				if(confrm){
					$.ajax({
						type: "POST",
						url: "<?php echo admin_url('admin-ajax.php'); ?>",
						data: { action:'user_playlist_delete',playlist_id: playlist_id},
						success : function(data){
							$('#lay-'+playlist_id).remove();
						}
					});
				}
			})

		})

	</script>
	<style>
		.newclassAmt
		{
			margin-left: 0px;
		}
		.newclass
		{
			margin-left: 65px;
		}
		.newclass2
		{
			margin-left: 0px;
		}
		.newclasspaypalBox
		{
			margin-left: 8px;
		}
		.newclassmsgBox
		{
			margin-left: 0px;
		}
		#ui-datepicker-div{
			width: 240px !important;
		}
		.ui-datepicker-header .ui-datepicker-prev, .ui-datepicker-header .ui-datepicker-next{
			display: none;
		}
		.futurestreamings ul.channel-detail{
			padding: 10px 20px;
			font-size: 16px;
		}
		.futurestreamings ul.channel-detail li p{
			font-size: 15px;
		}
		.futurestreamings ul.channel-detail li{
			list-style: none;
			line-height: 25px;
		}
		.futurestreamings ul.channel-detail li span{
			padding-left: 10px;
		}
		.opener .img-arrow{
			position:absolute;
			left:150px;
			bottom:-33px
		}
		.opener .arrow{
			padding: 10px;
			padding-top: 0px;
			-webkit-transform: rotate(180deg);
			-webkit-transform-origin: 0 0;
		}

		@-moz-document url-prefix() {
			.opener .arrow{
				padding: 20px 0px;
				-webkit-transform: rotate(180deg);
				-webkit-transform-origin: 0 0;
				margin-left: 55%;
				behavior:url(-ms-transform.htc);
			    /* Firefox */
			    -moz-transform:rotate(180deg);
			    margin-top: -50px;
			}
			.opener .img-arrow{
				left:70px;
				bottom:-20px;
			}
		}
		@media (max-width: 768px) {
			.live-stream-block{
				width: 50%;
			}
			.arrow{
				text-align: right;
				padding: 0px 20px !important;
			}
		}

	}

.glyphicon { margin-right:5px; }
.thumbnail
{
    margin-bottom: 20px;
    padding: 0px;
    -webkit-border-radius: 0px;
    -moz-border-radius: 0px;
    border-radius: 0px;
}

.item.list-group-item
{
    float: none;
    width: 100%;
    background-color: #fff;
    margin-bottom: 10px;
}
.item.list-group-item:nth-of-type(odd):hover,.item.list-group-item:hover
{
   // background: #428bca;
}

.item.list-group-item .list-group-image
{
    margin-right: 10px;
}
.item.list-group-item .thumbnail
{
    margin-bottom: 0px;
}
.item.list-group-item .caption
{
    padding: 9px 9px 0px 9px;
}
.item.list-group-item:nth-of-type(odd)
{
    background: #eeeeee;
}

.item.list-group-item:before, .item.list-group-item:after
{
    display: table;
    content: " ";
}

.item.list-group-item img
{
    float: left;
}
.item.list-group-item:after
{
    clear: both;
}
.list-group-item-text
{
    margin: 0 0 11px;
}
.item2.list-group-item
{
    float: none;
    width: 100%;
    background-color: #fff;
    margin-bottom: 10px;
}
.item2.list-group-item .list-group-image
{
    margin-right: 10px;
}
.item2.list-group-item .thumbnail
{
    margin-bottom: 0px;
}
.item2.list-group-item .caption
{
    padding: 9px 9px 0px 9px;
}
.item2.list-group-item:nth-of-type(odd)
{
    background: #eeeeee;
}

.item2.list-group-item:before, .item2.list-group-item:after
{
    display: table;
    content: " ";
}

.item2.list-group-item img
{
    float: left;
}
.item2.list-group-item:after
{
    clear: both;
}
ul.pagination{
	clear: both;
	width: 100%;
}
.clear_view_type{
	padding-left: 0 !important;
	padding-right: 0 !important;
}
.clear_view_right_type{
	padding-right: 0 !important;
}
.list-group-item{
	padding: 0 !important;
}
@media (min-width: 992px)
{
.playlistdivs>.col-md-3 {
 /*width: 23.794%;*/
}
.playlistdivs>.col-sm-4 img {
	height: 104px;
}
}

@media (min-width: 768px)
{
.playlistdivs>.col-sm-4 {
   /* width: 30.433333%;*/
}
.playlistdivs>.col-sm-4 img {
	height: 104px;
}
}


</style>
<?php
function sort_box_videos($profile_id){
		$sort_type = array(
			"recent" => "Recent",
			"views"	=> "Views",
			"comment" => "Comment",
			"download" => "Download"
		);
		$url = add_query_arg('display_page', 'file');

		if(isset($_GET['file-sort']))
			$sort = $_GET['file-sort'];
		else
			$sort = 'recent';
		ob_start();
		?>
		<div class="cover-twoblocks">
			<div class="sortvideo-div">
			<div class="dyn-file-sort">
				<label>Sort By: </label>
				<select name="dyn-file-sort-select-fav" class="dyn-file-sort-select">
				<?php foreach ($sort_type as $key => $value): ?>
					<option data-url="<?php echo add_query_arg('file-sort', $key, $url); ?>" value="<?php echo $key; ?>" <?php selected( $sort, $key ) ?>><?php echo $value; ?></option>
				<?php endforeach; ?>
				</select>
			</div>

			</div>
			<div class="well well-sm">
				<strong>Views</strong>

				<div class="btn-group">
					<a href="#" id="list-fav2" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th-list">
					</span>List</a> <a href="#" id="grid-fav2" class="btn btn-default btn-sm"><span
						class="glyphicon glyphicon-th"></span>Grid</a>
				</div>
			</div>
		</div>
		<script type="text/javascript">
		$(document).ready(function() {
			$('#products2 .item2').addClass('grid-group-item');
			$('#list-fav2').click(function(event){event.preventDefault();$('#products2 .item2').addClass('list-group-item');});
			$('#grid-fav2').click(function(event){event.preventDefault();$('#products2 .item2').removeClass('list-group-item');$('#products2 .item2').addClass('grid-group-item');});
		});
		</script>
		<?php
		return ob_get_clean();

	}





function files_sort_box($profile_id){
		$sort_type = array(
			"recent" => "Recent",
			"views"	=> "Views",
			"comment" => "Comment",
			"download" => "Download"
		);
		$url = add_query_arg('display_page', 'file');

		if(isset($_GET['file-sort']))
			$sort = $_GET['file-sort'];
		else
			$sort = 'recent';
		ob_start();
		?>
		<div class="cover-twoblocks">
			<div class="sortvideo-div">
			<div class="dyn-file-sort">
				<label>Sort By: </label>
				<select name="dyn-file-sort-select-fav-files" class="dyn-file-sort-select">
				<?php foreach ($sort_type as $key => $value): ?>
					<option data-url="<?php echo add_query_arg('file-sort', $key, $url); ?>" value="<?php echo $key; ?>" <?php selected( $sort, $key ) ?>><?php echo $value; ?></option>
				<?php endforeach; ?>
				</select>
			</div>

			</div>
			<div class="well well-sm">
				<strong>Views</strong>
				<div class="btn-group">
					<a href="#" id="list-fav-files2" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th-list">
					</span>List</a> <a href="#" id="grid-fav-files2" class="btn btn-default btn-sm"><span
						class="glyphicon glyphicon-th"></span>Grid</a>
				</div>
			</div>
		</div>
		<script type="text/javascript">
		$(document).ready(function() {
			$('#products3 .item3').addClass('grid-group-item');
			$('#list-fav-files2').click(function(event){event.preventDefault();$('#products3 .item3').addClass('list-group-item');});
			$('#grid-fav-files2').click(function(event){event.preventDefault();$('#products3 .item3').removeClass('list-group-item');$('#products3 .item3').addClass('grid-group-item');});
		});
		</script>
		<?php
		return ob_get_clean();

	}
?>

<?php
function books_sort_box($profile_id){
		$sort_type = array(
			"recent" => "Recent",
			"views"	=> "Views",
			"comment" => "Comment",
			"download" => "Download"
		);
		$url = add_query_arg('display_page', 'file');

		if(isset($_GET['file-sort']))
			$sort = $_GET['file-sort'];
		else
			$sort = 'recent';
		ob_start();
		?>
		<div class="cover-twoblocks">
			<div class="sortvideo-div">
			<div class="dyn-file-sort">
				<label>Sort By: </label>
				<select name="dyn-file-sort-select-fav-files" class="dyn-file-sort-select">
				<?php foreach ($sort_type as $key => $value): ?>
					<option data-url="<?php echo add_query_arg('file-sort', $key, $url); ?>" value="<?php echo $key; ?>" <?php selected( $sort, $key ) ?>><?php echo $value; ?></option>
				<?php endforeach; ?>
				</select>
			</div>

			</div>
			<div class="well well-sm">
				<strong>Views</strong>
				<div class="btn-group">
					<a href="#" id="book-fav-files-list" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th-list">
					</span>List</a> <a href="#" id="book-fav-files-grid" class="btn btn-default btn-sm"><span
						class="glyphicon glyphicon-th"></span>Grid</a>
				</div>
			</div>
		</div>
		<script type="text/javascript">
		$(document).ready(function() {
			$('#products4 .item4').addClass('grid-group-item');
			$('#book-fav-files-list').click(function(event){event.preventDefault();$('#products4 .item4').addClass('list-group-item');});
			$('#book-fav-files-grid').click(function(event){event.preventDefault();$('#products4 .item4').removeClass('list-group-item');$('#products4 .item4').addClass('grid-group-item');});
		});
		</script>
		<?php
		return ob_get_clean();

	}
?>