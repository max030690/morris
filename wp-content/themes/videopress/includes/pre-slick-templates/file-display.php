<?php
$uploaded_type = get_post_meta( get_the_ID(), 'dyn_upload_type', true );
$postiddd = $_GET['ih'];
?>
<div class="vid-single filehightnow">
     <?php if(get_the_author_id() == get_current_user_id()){ ?>
		 <h6 class="video-title"><?php echo get_the_title($postiddd); ?>&nbsp;&nbsp;<a href="#" id="title-edit"><i class="fa fa-pencil-square-o"></i>Edit</a></h6>
	<?php } else {
	?>
	     <h6 class="video-title"><?php echo get_the_title($postiddd); ?></h6>
	<?php
	} ?>
	<div id="title-div" style="display:none;">
		<input type="text" name="title-val" value="<?= the_title();?>">
		<button type="button" class="btn btn-info" id="title-save">Save</button>
		<button type="button" class="btn btn-primary" id="title-cncl">Cancel</button>
		<br>
	</div>
	<div class="file-container">
		<div class="file-logo">
			<a href="<?php echo apply_filters( 'dyn_file_download_link', $postiddd ); ?>">
				<?php $output = ''; ?>
				<?php echo apply_filters( 'dyn_file_image', $output, $postiddd ); ?>
			</a>
		</div>

	</div></div>