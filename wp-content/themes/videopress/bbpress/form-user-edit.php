<?php

/**
 * bbPress User Profile Edit Part
 *
 * @package bbPress
 * @subpackage Theme
 */

?>
<script>
	$(function(){

			$(document.body).on('click', '.changeType' ,function(){
				$(this).closest('.phone-input').find('.type-text').text("");
				if($(this).data('type-value') == "phone")
					$(this).closest('.phone-input').find('.type-text').append('<i class="fa fa-phone" aria-hidden="true"></i>');
				else if($(this).data('type-value') == "email")
					$(this).closest('.phone-input').find('.type-text').append('<i class="fa fa-envelope" aria-hidden="true"></i>');
				else if($(this).data('type-value') == "fax")
					$(this).closest('.phone-input').find('.type-text').append('<i class="fa fa-fax" aria-hidden="true"></i>');
				
				if($(this).data('type-value') == "email")
					$(this).closest('.phone-input').find('.placeholdere').attr('placeholder','test@example.com');
				else
					$(this).closest('.phone-input').find('.placeholdere').attr('placeholder','+1 (999) 999 9999');
				
				$(this).closest('.phone-input').find('.type-input').val($(this).data('type-value'));
			});
			
			$(document.body).on('click', '.btn-remove-phone' ,function(){
				$(this).closest('.phone-input').remove();
			});
			
			$(document.body).on('click', '.btn-remove-ne-member' ,function(){
				$(this).closest('.nemember-input').remove();
			});
			
			$('.btn-add-phone').click(function(){

				var index = $('.phone-input').length + 1;
				
				$('.phone-list').append(''+
						'<div class="input-group phone-input">'+
							'<span class="input-group-btn">'+
								'<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="type-text"><i class="fa fa-phone" aria-hidden="true"></i></span> <span class="caret"></span></button>'+
								'<ul class="dropdown-menu" role="menu" style="background-color:#fff">'+
									'<li><a class="changeType" href="javascript:;" data-type-value="phone"><i class="fa fa-phone" aria-hidden="true"></i></a></li>'+
									'<li><a class="changeType" href="javascript:;" data-type-value="email"><i class="fa fa-envelope" aria-hidden="true"></i></a></li>'+
									'<li><a class="changeType" href="javascript:;" data-type-value="fax"><i class="fa fa-fax" aria-hidden="true"></i></a></li>'+
								'</ul>'+
							'</span>'+
							'<input type="text" name="conphoneg['+index+'][number]" class="form-control placeholdere" placeholder="+1 (999) 999 9999" />'+
							'<input type="hidden" name="conphoneg['+index+'][type]" class="type-input" value="phone" />'+
							'<span class="input-group-btn" style="float:left">'+
								'<button class="btn btn-danger btn-remove-phone" type="button"><span class="glyphicon glyphicon-remove"></span></button>'+
							'</span>'+
						'</div>'
				);

			});
			
			$(document.body).on('click', '.btn-cancel-ne-member' ,function(){
				$('.notable-member-box').html('');
				});
			
			$('.btn-add-notable-member').click(function(){
				
				
				$('.notable-member-box').html(''+
						'<div><label for="ne_contact_photo"><?php _e( 'Member Photo', 'bbpress' ); ?></label>'+
						'<input type="file" name="ne_contact_photo" id="ne_contact_photo" class="regular-text" tabindex="<?php bbp_tab_index(); ?>" '  +
						'accept=".jpeg,.jpg,.gif,.png,.JPEG,.JPG,.GIF,.PNG"   />'+
			            '</div>'+
						'<div>'+
								'	<label for="ne_contact_name"><?php _e( 'Member Name', 'bbpress' ); ?></label>'+
									'<input type="text" name="ne_contact_name" id="ne_contact_name" class="regular-text" tabindex="<?php bbp_tab_index(); ?>" />' +
								'</div>'+
								'<div>'+
									'<label for="ne_contact_title"><?php _e( 'Member Title', 'bbpress' ); ?></label>'+
									'<input type="text" name="ne_contact_title" id="ne_contact_title" class="regular-text" tabindex="<?php bbp_tab_index(); ?>" />'+
								'</div>'+
								'<div>'+
									'<label for="ne_contact_link"><?php _e( 'Member Link', 'bbpress' ); ?></label>' +
									'<input type="text" name="ne_contact_link" id="ne_contact_link" class="regular-text" placeholder="http://www.example.com"  tabindex="<?php bbp_tab_index(); ?>" />'+
								'</div>' +
								'<div style="float:right;width:80%"><button type="submit" class="btn btn-success custom_btn_style" name="int_submit" value="int_submit">Upload</button>' +
								'<button type="submit" class="btn btn-danger btn-cancel-ne-member" id="ne_int_delete" name="int_delete" value="int_deletes" style="margin-left:10px;">Cancel</button></div>'
				);

			});
			
		});
</script>

<form id="bbp-your-profile" action="<?php bbp_user_profile_edit_url( bbp_get_displayed_user_id() ); ?>" method="post" enctype="multipart/form-data">

	<h2 class="entry-title"><?php _e( 'Name', 'bbpress' ) ?></h2>

	<?php do_action( 'bbp_user_edit_before' ); ?>

	<fieldset class="bbp-form">
		<legend><?php _e( 'Name', 'bbpress' ) ?></legend>

		<?php do_action( 'bbp_user_edit_before_name' ); ?>

		<div>
			<label for="first_name"><?php _e( 'First Name', 'bbpress' ) ?></label>
			<input type="text" name="first_name" id="first_name" value="<?php bbp_displayed_user_field( 'first_name', 'edit' ); ?>" class="regular-text" tabindex="<?php bbp_tab_index(); ?>" />
		</div>

		<div>
			<label for="last_name"><?php _e( 'Last Name', 'bbpress' ) ?></label>
			<input type="text" name="last_name" id="last_name" value="<?php bbp_displayed_user_field( 'last_name', 'edit' ); ?>" class="regular-text" tabindex="<?php bbp_tab_index(); ?>" />
		</div>

		<div>
			<label for="nickname"><?php _e( 'Nickname', 'bbpress' ); ?></label>
			<input type="text" name="nickname" id="nickname" value="<?php bbp_displayed_user_field( 'nickname', 'edit' ); ?>" class="regular-text" tabindex="<?php bbp_tab_index(); ?>" />
		</div>

		<div>
			<label for="display_name"><?php _e( 'Display Name', 'bbpress' ) ?></label>

			<?php bbp_edit_user_display_name(); ?>

		</div>

		<?php do_action( 'bbp_user_edit_after_name' ); ?>

	</fieldset>

	<h2 class="entry-title"><?php _e( 'Contact Info', 'bbpress' ) ?></h2>

	<fieldset class="bbp-form">
		<legend><?php _e( 'Contact Info', 'bbpress' ) ?></legend>

		<?php do_action( 'bbp_user_edit_before_contact' ); ?>

		<div>
			<label for="url"><?php _e( 'Website', 'bbpress' ) ?></label>
			<input type="text" name="url" id="url" value="<?php bbp_displayed_user_field( 'user_url', 'edit' ); ?>" class="regular-text code" tabindex="<?php bbp_tab_index(); ?>" />
		</div>

		<?php foreach ( bbp_edit_user_contact_methods() as $name => $desc ) : ?>

			<div>
				<label for="<?php echo esc_attr( $name ); ?>"><?php echo apply_filters( 'user_' . $name . '_label', $desc ); ?></label>
				<input type="text" name="<?php echo esc_attr( $name ); ?>" id="<?php echo esc_attr( $name ); ?>" value="<?php bbp_displayed_user_field( $name, 'edit' ); ?>" class="regular-text" tabindex="<?php bbp_tab_index(); ?>" />
			</div>

		<?php endforeach; ?>

		<?php do_action( 'bbp_user_edit_after_contact' ); ?>

	</fieldset>

	<h2 class="entry-title"><?php bbp_is_user_home_edit() ? _e( 'About Yourself', 'bbpress' ) : _e( 'About the user', 'bbpress' ); ?></h2>

	<fieldset class="bbp-form">
		<legend><?php bbp_is_user_home_edit() ? _e( 'About Yourself', 'bbpress' ) : _e( 'About the user', 'bbpress' ); ?></legend>

		<?php do_action( 'bbp_user_edit_before_about' ); ?>

		<div>
			<label for="description"><?php _e( 'Biographical Info', 'bbpress' ); ?></label>
			<textarea name="description" id="description" rows="5" cols="30" tabindex="<?php bbp_tab_index(); ?>"><?php bbp_displayed_user_field( 'description', 'edit' ); ?></textarea>
		</div>
		
			<div>
				<label for="additionalinfog"><?php echo apply_filters( 'user_additionalinfog_label', 'Additional Information') ; ?></label>
				<textarea name="additionalinfog" id="additionalinfog"  rows="5" cols="30" tabindex="<?php bbp_tab_index(); ?>"><?php bbp_displayed_user_field( 'additionalinfog', 'edit' ); ?></textarea>
			</div>


		<?php do_action( 'bbp_user_edit_after_about' ); ?>

	</fieldset>
	
		<h2 class="entry-title"><?php bbp_is_user_home_edit() ? _e( 'Contact Us', 'bbpress' ) : _e( 'Contact User Information', 'bbpress' ); ?></h2>

	<fieldset class="bbp-form">
		<legend><?php bbp_is_user_home_edit() ? _e( 'About Yourself', 'bbpress' ) : _e( 'About the user', 'bbpress' ); ?></legend>

		<?php do_action( 'bbp_user_edit_before_about' ); ?>

		<div>
			<label for="description"><?php _e( 'Contact Description', 'bbpress' ); ?></label>
			<textarea name="cdescriptiong" id="cdescriptiong" rows="5" cols="30" tabindex="<?php bbp_tab_index(); ?>"><?php bbp_displayed_user_field( 'cdescriptiong', 'edit' ); ?></textarea>
		</div>
		<div>
		<label for="Contact Details"><?php _e( 'Contact Type', 'bbpress' ); ?></label>
		
		<div class="phone-list" style="float:right;width:80%">
		<?php 
		$counterPh = 1;
		if( get_the_author_meta('conphoneg', get_current_user_id()) != '' ){
			$phoneData = json_decode( get_the_author_meta('conphoneg', get_current_user_id()));
		    

			foreach($phoneData as $value)
			{
				
				echo '
				
					<div class="input-group phone-input">
						<span class="input-group-btn">
							<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="type-text">';
							if($value->type == 'phone')
								echo '<i class="fa fa-phone" aria-hidden="true"></i>';
							elseif($value->type == 'email')
								echo '<i class="fa fa-envelope" aria-hidden="true"></i>';
							elseif($value->type == 'fax')
								echo '<i class="fa fa-fax" aria-hidden="true"></i>';	
							
							echo '</span> <span class="caret"></span></button>
							<ul class="dropdown-menu" role="menu" style="background-color:#fff">
								<li><a class="changeType" href="javascript:;" data-type-value="phone"><i class="fa fa-phone" aria-hidden="true"></i>
</a></li>
								<li><a class="changeType" href="javascript:;" data-type-value="email"><i class="fa fa-envelope" aria-hidden="true"></i>
</a></li>
								<li><a class="changeType" href="javascript:;" data-type-value="fax"><i class="fa fa-fax" aria-hidden="true"></i>
</a></li>
							</ul>
						</span>
						<input type="hidden" name="conphoneg['.$counterPh.'][type]" class="type-input" value="'.$value->type.'" />
						<input type="text" name="conphoneg['.$counterPh.'][number]" value="'.$value->number.'" class="form-control placeholdere" placeholder="+1 (999) 999 9999" />
						<span class="input-group-btn" style="float:left">
								<button class="btn btn-danger btn-remove-phone" type="button"><span class="glyphicon glyphicon-remove"></span></button>
							</span>
					</div>
					
				';
				
				$counterPh++;
			}
		}
		
		//////Show empty box if no contact info available
		if($counterPh == 1) {
					echo '<div class="input-group phone-input">
						<span class="input-group-btn">
							<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="type-text"><i class="fa fa-phone" aria-hidden="true"></i></span> <span class="caret"></span></button>
							<ul class="dropdown-menu" role="menu" style="background-color:#fff">
								<li><a class="changeType" href="javascript:;" data-type-value="phone"><i class="fa fa-phone" aria-hidden="true"></i>
</a></li>
								<li><a class="changeType" href="javascript:;" data-type-value="email"><i class="fa fa-envelope" aria-hidden="true"></i>
</a></li>
								<li><a class="changeType" href="javascript:;" data-type-value="fax"><i class="fa fa-fax" aria-hidden="true"></i>
</a></li>
							</ul>
						</span>
						<input type="hidden" name="conphoneg['.$counterPh.'][type]" class="type-input" value="phone" />
						<input type="text" name="conphoneg['.$counterPh.'][number]" class="form-control placeholdere" placeholder="+1 (999) 999 9999" />
						<span class="input-group-btn" style="float:left">
								<button class="btn btn-danger btn-remove-phone" type="button"><span class="glyphicon glyphicon-remove"></span></button>
							</span>
					</div>';
					
		}
			?>		
				</div>

				<div style="float:right;width:80%">
				<button type="button" class="btn btn-success btn-sm btn-add-phone"><span class="glyphicon glyphicon-plus"></span> Add Contact</button>
				</div>
		</div>
		<?php do_action( 'bbp_user_edit_after_about' ); ?>

	</fieldset>
	
		<h2 class="entry-title"><?php _e( 'Notable Members', 'bbpress' ) ?></h2>
	<fieldset class="bbp-form">
		<legend><?php bbp_is_user_home_edit() ? _e( 'Notable Members', 'bbpress' ) : _e( 'Notable Members section', 'bbpress' ); ?></legend>

		<?php do_action( 'bbp_user_edit_before_about' ); ?>
		<div >
		<?php
		$counterPh = 1;
		if( get_the_author_meta('ne_member_pfdata', get_current_user_id()) != '' ){
			$nepfData = json_decode( get_the_author_meta('ne_member_pfdata', get_current_user_id()));
		    

			foreach($nepfData as $value)
			{
				$fileName1 = site_url().'/nm_pic/'.$value->ne_contact_photo;
				echo '<div class="nemember-input" style="width:110px;height:150px;border: 1px solid #ccc; text-align:center; float:left;margin-right:5px;clear:right;"><img src="'.$fileName1.'">
		
		<br> <a href="'.$value->ne_contact_link.'">'.$value->ne_contact_name.'</a><br> '.$value->ne_contact_title.'
		<br>
		<a class="btn-remove-ne-member"   style="color:red">
<i class="fa fa-trash-o"></i> 
</a> 
<input type="hidden" name="ne_member_pfdata['.$counterPh.'][ne_contact_photo]" class="type-input" value="'.$value->ne_contact_photo.'" />
<input type="hidden" name="ne_member_pfdata['.$counterPh.'][ne_contact_name]" class="type-input" value="'.$value->ne_contact_name.'" />
<input type="hidden" name="ne_member_pfdata['.$counterPh.'][ne_contact_title]" class="type-input" value="'.$value->ne_contact_title.'" />
<input type="hidden" name="ne_member_pfdata['.$counterPh.'][ne_contact_link]" class="type-input" value="'.$value->ne_contact_link.'" />
		</div>';
		$counterPh++;
			}
		}	
				?>
		
		
		</div>
		<div class="notable-member-box"></div>

		
			<div style="float:right;width:80%">
				<button type="button" class="btn btn-success btn-sm btn-add-notable-member"><span class="glyphicon glyphicon-plus"></span> Add Notable Member</button>
				</div>
			
		<?php do_action( 'bbp_user_edit_after_about' ); ?>

	</fieldset>

	<h2 class="entry-title"><?php _e( 'Account', 'bbpress' ) ?></h2>

	<fieldset class="bbp-form">
		<legend><?php _e( 'Account', 'bbpress' ) ?></legend>

		<?php do_action( 'bbp_user_edit_before_account' ); ?>

		<div>
			<label for="user_login"><?php _e( 'Username', 'bbpress' ); ?></label>
			<input type="text" name="user_login" id="user_login" value="<?php bbp_displayed_user_field( 'user_login', 'edit' ); ?>" disabled="disabled" class="regular-text" tabindex="<?php bbp_tab_index(); ?>" />
		</div>

		<div>
			<label for="email"><?php _e( 'Email', 'bbpress' ); ?></label>

			<input type="text" name="email" id="email" value="<?php bbp_displayed_user_field( 'user_email', 'edit' ); ?>" class="regular-text" tabindex="<?php bbp_tab_index(); ?>" />

			<?php

			// Handle address change requests
			$new_email = get_option( bbp_get_displayed_user_id() . '_new_email' );
			if ( !empty( $new_email ) && $new_email !== bbp_get_displayed_user_field( 'user_email', 'edit' ) ) : ?>

				<span class="updated inline">

					<?php printf( __( 'There is a pending email address change to <code>%1$s</code>. <a href="%2$s">Cancel</a>', 'bbpress' ), $new_email['newemail'], esc_url( self_admin_url( 'user.php?dismiss=' . bbp_get_current_user_id()  . '_new_email' ) ) ); ?>

				</span>

			<?php endif; ?>

		</div>

		<div id="password">
			<label for="pass1"><?php _e( 'New Password', 'bbpress' ); ?></label>
			<fieldset class="bbp-form password">
				<input type="password" name="pass1" id="pass1" size="16" value="" autocomplete="off" tabindex="<?php bbp_tab_index(); ?>" />
				<span class="description"><?php _e( 'If you would like to change the password type a new one. Otherwise leave this blank.', 'bbpress' ); ?></span>

				<input type="password" name="pass2" id="pass2" size="16" value="" autocomplete="off" tabindex="<?php bbp_tab_index(); ?>" />
				<span class="description"><?php _e( 'Type your new password again.', 'bbpress' ); ?></span><br />

				<div id="pass-strength-result"></div>
				<span class="description indicator-hint"><?php _e( 'Your password should be at least ten characters long. Use upper and lower case letters, numbers, and symbols to make it even stronger.', 'bbpress' ); ?></span>
			</fieldset>
		</div>

		<?php do_action( 'bbp_user_edit_after_account' ); ?>

	</fieldset>

	<?php if ( current_user_can( 'edit_users' ) && ! bbp_is_user_home_edit() ) : ?>

		<h2 class="entry-title"><?php _e( 'User Role', 'bbpress' ) ?></h2>

		<fieldset class="bbp-form">
			<legend><?php _e( 'User Role', 'bbpress' ); ?></legend>

			<?php do_action( 'bbp_user_edit_before_role' ); ?>

			<?php if ( is_multisite() && is_super_admin() && current_user_can( 'manage_network_options' ) ) : ?>

				<div>
					<label for="super_admin"><?php _e( 'Network Role', 'bbpress' ); ?></label>
					<label>
						<input class="checkbox" type="checkbox" id="super_admin" name="super_admin"<?php checked( is_super_admin( bbp_get_displayed_user_id() ) ); ?> tabindex="<?php bbp_tab_index(); ?>" />
						<?php _e( 'Grant this user super admin privileges for the Network.', 'bbpress' ); ?>
					</label>
				</div>

			<?php endif; ?>

			<?php bbp_get_template_part( 'form', 'user-roles' ); ?>

			<?php do_action( 'bbp_user_edit_after_role' ); ?>

		</fieldset>

	<?php endif; ?>

	<?php do_action( 'bbp_user_edit_after' ); ?>

	<fieldset class="submit">
		<legend><?php _e( 'Save Changes', 'bbpress' ); ?></legend>
		<div>

			<?php bbp_edit_user_form_fields(); ?>

			<button type="submit" tabindex="<?php bbp_tab_index(); ?>" id="bbp_user_edit_submit" name="bbp_user_edit_submit" class="button submit user-submit"><?php bbp_is_user_home_edit() ? _e( 'Update Profile', 'bbpress' ) : _e( 'Update User', 'bbpress' ); ?></button>
		</div>
	</fieldset>

</form>