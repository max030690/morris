<!-- Start Layout -->
<?php
// Check if the layout builder is enabled
if( vp_metabox('layout_builder.enable_builder') == '1' ){

	// Layout Builder
	$videopress_count = 0; // Set Variable to 0
	$videopress_group =  vp_metabox('layout_builder.layout_group');
	foreach ($videopress_group as $value){
	
	// Assign metabox values to variables
	$videopress_layout_type = vp_metabox('layout_builder.layout_group.'.$videopress_count.'.layout_type');
	$videopress_layout_title = vp_metabox('layout_builder.layout_group.'.$videopress_count.'.layout_title');
	$videopress_layout_category = vp_metabox('layout_builder.layout_group.'.$videopress_count.'.layout_category');
	$videopress_items = vp_metabox('layout_builder.layout_group.'.$videopress_count.'.item_count');
	$videopress_banner = vp_metabox('layout_builder.layout_group.'.$videopress_count.'.banner_image');
	$videopress_banner_link = vp_metabox('layout_builder.layout_group.'.$videopress_count.'.banner_link');
	
	// Create the function
	$videopress_layout_type( $videopress_layout_title, $videopress_layout_category, $videopress_items, $videopress_banner, $videopress_banner_link );

	$videopress_count = $videopress_count+1; // Add +1 to each loop

	} // End For Each
	
} // End If
?>
<!-- End of Layout -->