<?php get_header(); ?>

    <!-- Start Content -->
    <div class="container">

    <!-- Start Entries -->
    <div class="grid-2-3 blog-post-single">

    <?php
	/* ================================================================== */
	/* Start of Loop */
	/* ================================================================== */
	while (have_posts()) : the_post();
	?>

    <?php the_post_thumbnail('blog-post', array( 'class'=> 'blog-thumb' ) ); ?>

    <!-- Start Video Heading -->
    <div class="video-heading">
    <!-- Start Sharer -->
    <?php get_template_part('includes/sharer'); ?>
    <!-- End Sharer -->
    <h6 class="video-title"><?php the_title(); ?></h6>
    <div class="clear"></div>
    </div>
    <!-- End Video Heading -->

    <!-- Start Content -->
    <ul class="stats">
      <li></i><?php videopress_countviews( get_the_ID() ); ?></li>
      <li><?php comments_number() ?></li>
      <li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
    </ul>
    <div class="clear"></div>

    <!-- Start Content -->
    <div class="entry">
    <?php
    // Display The Content
    if($post->post_content==""){
    		echo '<div class="content-empty">No Description</div>';
		}else{
			the_content();
		}
	?>

    <div class="clear"></div>
    </div>
    <div class="spacing-40"></div>
    <!-- End Content -->

	<?php
	// Link Pages Navigation
	$args = array( 'before' => '<div class="wp_link_pages">Pages:&nbsp; ', 'after' => '</div><div class="clear"></div>' );
    wp_link_pages( $args );

	// Display the Tags
	$before = '<div class="spacer-20"></div>
			   <div class="post-tags">
			   <span><i class="fa fa-tags"></i>Tags</span>';

	$after = '</div>';
	the_tags( $before,', ',$after );
	?>

    <div class="clear"></div>
    <!-- End Content -->


    <?php
	/* ================================================================== */
	/* End of Loop */
	/* ================================================================== */
	endwhile;
	?>

    <?php comments_template(); ?>

    </div>
    <!-- End Entries -->

    <!-- Widgets -->
    <div class="grid-3"><?php get_sidebar('blog'); ?></div>
    <!-- End Widgets -->

    <div class="clear"></div>
    </div>
    <div class="spacing-40"></div>
    <!-- End Content -->

<?php get_footer(); ?>